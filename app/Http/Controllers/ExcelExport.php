<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\BeforeExport;
use DB;
use Maatwebsite\Excel\Events\BeforeSheet;

use Maatwebsite\Excel\Events\BeforeWriting;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ExcelExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

	public function __construct($data, $head, $type){
		$this->head = $head;
		$this->child = $data[0];

		if(count($data) > 1)
		{
			foreach (array_slice($data,1) as $no => $value)
			{
				$this->child_c[$no] = $value;
			}
		}

		$this->type = $type;
	}

	public function collection(){
		if(!in_array($this->type, ['absensi_user']) )
		{
			return collect($this->child);
		}
		else
		{
			return collect([]);
		}
	}

	public function headings(): array{
		if(!in_array($this->type, ['absensi_user']) )
		{
			return $this->head;
		}
		else
		{
			return [];
		}
	}

	public function registerEvents(): array
	{
		// switch ($this->type) {
		// 	case 'absensi_user':
		// 		return [
		// 			BeforeExport::class => function(BeforeExport $event)
		// 			{
		// 				$border_Style = [
		// 					'borders' => [
		// 						'allBorders' => [
		// 							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
		// 						],
		// 					],
		// 					'alignment' => [
		// 						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
		// 							'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
		// 					]
		// 				];

		// 				$data_pekerjaan = $this->child['data_pekerjaan'];

		// 				$event->writer->getProperties()->setCreator('Tomman');
		// 				$worksheet1 = '';

		// 				$data_excel = ['data_hadir' => $this->child['data_hadir'], 'data_tidak_hadir' => $this->child['data_tidak_hadir']];

		// 				$data = DB::table('master_karyawan AS mk')
		// 				->leftJoin('master_unit AS mu', 'mk.id_unit', '=', 'mu.unit_id')
		// 				->leftJoin('master_witel AS mw', 'mk.id_witel', '=', 'mw.witel_id')
		// 				->leftJoin('master_regional AS mr', 'mw.id_regional', '=', 'mr.regional_id')
		// 				->leftJoin('master_mitra AS mm', 'mk.id_mitra', '=', 'mm.mitra_id')
		// 				->leftJoin('master_position AS mp', 'mk.id_position', '=', 'mp.position_id')
		// 				->leftJoin('master_level AS ml', 'mk.id_level', '=', 'ml.level_id')
		// 				->leftJoin('master_team AS mt', function($join) {
		// 					$join->on('mt.technician1', '=', 'mk.nik')
		// 					->OrOn('mt.technician2', '=', 'mk.nik');
		// 				})
		// 				->leftJoin('master_sector AS ms', 'mt.id_sector', '=', 'ms.sector_id')
		// 				->where([
		// 					['mt.is_team_active', 1],
		// 					['ms.is_sector_active', 1],
		// 				]);

		// 				$data = $data->get();

		// 				$combinedData = [];

		// 				foreach ($data as $entry)
		// 				{
		// 					$key = $entry->nik . "-" . $entry->name;

		// 					if (!isset($combinedData[$key]))
		// 					{
		// 						$combinedData[$key] = [
		// 							"nik"               => $entry->nik,
		// 							"name"              => $entry->name,
		// 							"team_name"         => $entry->team_name,
		// 							"sub_sector"        => $entry->sub_sector,
		// 							"sector_name"       => $entry->sector_name,
		// 							"myLastLatitude"    => $entry->myLastLatitude,
		// 							"myLastLongitude"   => $entry->myLastLongitude,
		// 							"myLastAccuracy"    => $entry->myLastAccuracy,
		// 							"myLastCoordUpdate" => $entry->myLastCoordUpdate
		// 						];
		// 					}
		// 					else
		// 					{
		// 						$combinedData[$key]["team_name"]    .= " , " . $entry->team_name;
		// 						$combinedData[$key]["sub_sector"]   .= " , " . $entry->sub_sector;
		// 						$combinedData[$key]["sector_name"]  .= " , " . $entry->sector_name;
		// 					}
		// 				}

		// 				$sektor_nik = array_values($combinedData);

		// 				for ($i = 'A'; $i !== 'ZZ'; $i++){
		// 					$index[] = $i;
		// 				}

		// 				if($worksheet1 == '')
		// 				{
		// 					$worksheet1 = $event->writer->createSheet();
		// 					$event->writer->createSheet();
		// 				}

		// 				$hadir = $not_hadir = $izin = 0;

		// 				$not_hadir = count($data_excel['data_tidak_hadir']);
		// 				$hadir = count(array_filter(array_map(function($vv2) {
		// 					return empty($vv2->catatan);
		// 				}, $data_excel['data_hadir']) ) );
		// 				$izin = count(array_filter(array_map(function($vv2) {
		// 					return !empty($vv2->catatan);
		// 				}, $data_excel['data_hadir']) ) );

		// 				$event->writer->setActiveSheetIndex(0)->setTitle("List Peserta (H $hadir) | (NH $not_hadir)");
		// 				$event->writer->setActiveSheetIndex(1)->setTitle("List Peserta Izin ($izin)");

		// 				$no_dh = 0;

		// 				foreach($data_excel as $k => $v)
		// 				{
		// 					if($k == 'data_hadir')
		// 					{
		// 						$title_izin = ['No', 'Nik', 'Nama', 'Unit', 'Posisi', 'Tim', 'Sektor', 'Mitra', 'Witel', 'Alasan'];
		// 						$event->writer->setActiveSheetIndex(1)->getCell("A1")->setValue("Judul Pekerjaan: ". $data_pekerjaan['judul']);

		// 						foreach($title_izin as $kk => $vv)
		// 						{
		// 							$event->writer->setActiveSheetIndex(1)->getCell($index[$kk] ."4")->setValue($vv);
		// 						}

		// 						$title = ['No', 'Nik', 'Nama', 'Unit', 'Posisi', 'Tim', 'Sektor', 'Mitra', 'Witel', 'Jam Absen', 'Status Kehadiran'];
		// 						$event->writer->setActiveSheetIndex(0)->getCell("A1")->setValue("Judul Pekerjaan: ". $data_pekerjaan['judul']);

		// 						foreach($title as $kk => $vv)
		// 						{
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[$kk] ."4")->setValue($vv);
		// 						}

		// 						$no_dh_izin = 0;

		// 						foreach($v as $kk1 => $vv2)
		// 						{
		// 							$get_sektor_nik_id = array_search($vv2->nik, array_column($sektor_nik, 'nik') );
		// 							$team_name = $sub_sector = $sector_name = '-';

		// 							if($get_sektor_nik_id !== false)
		// 							{
		// 								$get_sektor_nik_id = $sektor_nik[$get_sektor_nik_id];

		// 								$team_name = $get_sektor_nik_id['team_name'] ?: '-';
		// 								$sub_sector = $get_sektor_nik_id['sub_sector'] ?: '-';
		// 								$sector_name = $get_sektor_nik_id['sector_name'] ?: '-';
		// 							}

		// 							if(is_null($vv2->catatan) || $vv2->catatan == '')
		// 							{
		// 								++$no_dh;
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[0] . '' . (4 + $no_dh) )->setValue($no_dh);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[1] . '' . (4 + $no_dh) )->setValue($vv2->nik);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[2] . '' . (4 + $no_dh) )->setValue($vv2->name_only);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[3] . '' . (4 + $no_dh) )->setValue($vv2->unit_name);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[4] . '' . (4 + $no_dh) )->setValue($vv2->posisi_name);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[5] . '' . (4 + $no_dh) )->setValue($team_name);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[6] . '' . (4 + $no_dh) )->setValue($sector_name);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[7] . '' . (4 + $no_dh) )->setValue($vv2->mitra_name);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[8] . '' . (4 + $no_dh) )->setValue($vv2->witel_name);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[9] . '' . (4 + $no_dh) )->setValue($vv2->jam_absen);
		// 								$event->writer->setActiveSheetIndex(0)->getCell($index[10] . '' . (4 + $no_dh) )->setValue('Hadir');

		// 								$event->writer->getSheet(0)->getStyle('A4:'. $index[10] . '' . (4 + $no_dh) )->applyFromArray($border_Style);
		// 							}
		// 							else
		// 							{
		// 								++$no_dh_izin;
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[0] . '' . (4 + $no_dh_izin) )->setValue($vv2->no);
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[1] . '' . (4 + $no_dh_izin) )->setValue($vv2->nik);
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[2] . '' . (4 + $no_dh_izin) )->setValue($vv2->name_only);
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[3] . '' . (4 + $no_dh_izin) )->setValue($vv2->unit_name);
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[4] . '' . (4 + $no_dh_izin) )->setValue($vv2->posisi_name);
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[5] . '' . (4 + $no_dh_izin) )->setValue($team_name);
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[6] . '' . (4 + $no_dh_izin) )->setValue($sector_name);
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[7] . '' . (4 + $no_dh_izin) )->setValue($vv2->mitra_name);
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[8] . '' . (4 + $no_dh_izin) )->setValue($vv2->witel_name);
		// 								$event->writer->setActiveSheetIndex(1)->getCell($index[9] . '' . (4 + $no_dh_izin) )->setValue($vv2->catatan);

		// 								$event->writer->getSheet(1)->getStyle('A4:'. $index[9] . '' . (4 + $no_dh_izin) )->applyFromArray($border_Style);
		// 							}
		// 						}
		// 					}

		// 					if($k == 'data_tidak_hadir')
		// 					{
		// 						foreach($title as $kk => $vv)
		// 						{
		// 							$event->writer->setActiveSheetIndex(1)->getCell($index[$kk] ."4")->setValue($vv);
		// 						}

		// 						foreach($v as $kk => $vv)
		// 						{
		// 							++$no_dh;

		// 							$get_sektor_nik_id = array_search($vv->nik, array_column($sektor_nik, 'nik') );
		// 							$team_name = $sub_sector = $sector_name = '-';

		// 							if($get_sektor_nik_id !== false)
		// 							{
		// 								$get_sektor_nik_id = $sektor_nik[$get_sektor_nik_id];

		// 								$team_name = $get_sektor_nik_id['team_name'] ?: '-';
		// 								$sub_sector = $get_sektor_nik_id['sub_sector'] ?: '-';
		// 								$sector_name = $get_sektor_nik_id['sector_name'] ?: '-';
		// 							}

		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[0] . '' . (4 + $no_dh) )->setValue($no_dh);
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[1] . '' . (4 + $no_dh) )->setValue($vv->nik);
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[2] . '' . (4 + $no_dh) )->setValue($vv->name_only);
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[3] . '' . (4 + $no_dh) )->setValue($vv->unit_name);
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[4] . '' . (4 + $no_dh) )->setValue($vv->position_name);
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[5] . '' . (4 + $no_dh) )->setValue($team_name);
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[6] . '' . (4 + $no_dh) )->setValue($sector_name);
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[7] . '' . (4 + $no_dh) )->setValue($vv->mitra_name);
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[8] . '' . (4 + $no_dh) )->setValue($vv->witel_name);
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[9] . '' . (4 + $no_dh) )->setValue('NOK');
		// 							$event->writer->setActiveSheetIndex(0)->getCell($index[10] . '' . (4 + $no_dh) )->setValue('Tidak Hadir');
		// 						}

		// 						$event->writer->getSheet(0)->getStyle('A4:'. $index[10] . '' . (4 + $no_dh) )->applyFromArray($border_Style);
		// 					}
		// 				}
		// 			}
		// 		];
		// 		break;

		// 	default:
				return [];
		// 		break;
		// }
	}
}