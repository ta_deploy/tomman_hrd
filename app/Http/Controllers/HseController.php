<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DA\HseModel;
use Cookie;

class HseController extends Controller
{

	public $foto_hse = ['Foto_Jarak_Dekat', 'Foto_Jarak_Jauh', 'Foto_Terdekat', 'Foto_Tambahan'];
	public $foto_update = ['Foto_Sebelum', 'Foto_Progress', 'Foto_Sesudah', 'Foto_Tambahan'];

	public function input()
	{
		$list_foto = $this->foto_hse;
		return view('Hse.input', compact('list_foto') );
	}

	public function submit_teknisi(Request $req, $id)
	{
		$msg = HseModel::submit_teknisi($req, $this->foto_hse, $id);
		return redirect('/input_potensi_b/'.$id)->with('alerts', $msg);
	}

	public function check_potensi()
	{
		$data = HseModel::list_bahaya();
		return view('Hse.list_potensi', compact('data') );
	}

	public function list_potensi()
	{
		$data = HseModel::list_bahaya(1);
		return view('Hse.list_potensi', compact('data') );
	}

	public function detail_potensi($id)
	{
		$data             = HseModel::detail_potensi($id);
		$data_log         = HseModel::detail_potensi_log($id);
		$list_foto        = $this->foto_hse;
		$list_foto_update = $this->foto_update;

		return view('Hse.detail_potensi', compact('data', 'data_log', 'list_foto', 'list_foto_update', 'id') );
	}

	public function update_perbaikan(Request $req, $id)
	{
		$msg = HseModel::update_perbaikan($req, $id, $this->foto_update);

		if($msg['update_hse'] == 4)
		{
			return redirect('/get_potensi/reject')->with('alerts', $msg);
		}
		else
		{
			return redirect('/wadah_potensi')->with('alerts', $msg);
		}
	}

	public function delete_potensi($id)
	{
		$msg = HseModel::delete_potensi($id);
		return redirect('/wadah_potensi')->with('alerts', $msg);

	}

	public function potensi_finished()
	{
		$data = HseModel::list_bahaya(2);
		return view('Hse.done_potensi', compact('data') );
	}

	public function get_potensi($status_hse)
	{
		switch ($status_hse)
		{
			case 'approve':
				$status_hse = 3;
			break;
			default:
				$status_hse = 4;
			break;
		}

		$data = HseModel::list_bahaya($status_hse);
		return view('Hse.status_potensi', compact('data', 'status_hse') );

	}

	public function check_hse(Request $req, $id)
	{
		$msg = HseModel::update_hse_status($req, $id);

		if($msg['update_hse'] == 0)
		{
			return redirect('/check_data')->with('alerts', $msg);
		}
		elseif($msg['update_hse'] == 2)
		{
			return redirect('/potensi_finished')->with('alerts', $msg);
		}
		else
		{
			return redirect('/wadah_potensi')->with('alerts', $msg);
		}
	}

	public function map_rawan_bahaya()
	{
		$data = HseModel::get_all_data();
		return view('Hse.mapping_rawan', compact('data') );
	}
}