<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\DA\Perf;
use DB;
use Telegram;
class PerfController extends Controller
{
  public function get_perf_ioan()
  {
    // $data = [
    //   [
    //     'name' => 'Sahmikasari',
    //     'username' => 'SVXKHA4',
    //     'no_telp' => '081520473205',
    //     'email' => 'sahmikasaritelkom@gmail.com'
    //   ],
    //   [
    //     'name' => 'YOGI SAPUTRO',
    //     'username' => 'SVEDA90',
    //     'no_telp' => '082149707788',
    //     'email' => 'pintutobat@gmail.com'
    //   ],
    //   [
    //     'name' => 'Fitria',
    //     'username' => 'SV6KS16',
    //     'no_telp' => '082299149151',
    //     'email' => 'fitriaoppo1998@gmail.com'
    //   ],
    //   [
    //     'name' => 'MUHAMMAD WAHYU RIZQON',
    //     'username' => 'SVMWR92',
    //     'no_telp' => '081250825192',
    //     'email' => 'eckon.sugud@gmail.com'
    //   ],
    //   [
    //     'name' => 'MUHAMMAD RASIDI',
    //     'username' => 'SVIUH57',
    //     'no_telp' => '085157022625',
    //     'email' => 'k3chil2598boss@gmail.com'
    //   ],
    //   [
    //     'name' => 'MOCHAMAD NURFAIZAL ARDIANSYAH',
    //     'username' => 'SVXMN97',
    //     'no_telp' => '081348390250',
    //     'email' => 'faizalardiansyah023@gmail.com'
    //   ],
    //   [
    //     'name' => 'Agus Maulana',
    //     'username' => 'SPGUS99',
    //     'no_telp' => '085386822238',
    //     'email' => '"agusmaulana120@gmail.com	"'
    //   ],
    //   [
    //     'name' => 'Ahmad Mawardi',
    //     'username' => 'SVHDH72',
    //     'no_telp' => '085249434934',
    //     'email' => 'sambalacan17@gmail.com'
    //   ],
    //   [
    //     'name' => 'Yanda',
    //     'username' => 'SVANDA9',
    //     'no_telp' => '085787468673',
    //     'email' => 'yandi.digwai@gmail.com'
    //   ],
    //   [
    //     'name' => 'Suryadi',
    //     'username' => 'SVSYD87',
    //     'no_telp' => '082312416800',
    //     'email' => 'suryadibanjarmasin8@gmail.com'
    //   ],
    //   [
    //     'name' => 'IBERAHIM AMAN',
    //     'username' => 'SVIBE95',
    //     'no_telp' => '081254712665',
    //     'email' => 'iberahimaman@gmail.com'
    //   ],
    //   [
    //     'name' => 'MUHAMMAD ROSADI',
    //     'username' => 'SVROS25',
    //     'no_telp' => '08971705968',
    //     'email' => 'gmbkalimantan@gmail.com'
    //   ],
    //   [
    //     'name' => 'MARIYANA',
    //     'username' => 'SV6KS14',
    //     'no_telp' => '081239373427',
    //     'email' => 'riyanabjm2@gmail.com'
    //   ],
    //   [
    //     'name' => 'Muhammad Riduan',
    //     'username' => 'SV6KS19',
    //     'no_telp' => '082154295557',
    //     'email' => 'ayiedmr@gmail.com'
    //   ],
    //   [
    //     'name' => 'AULIYA RAHMAN',
    //     'username' => 'SVGRY88',
    //     'no_telp' => '08115584444',
    //     'email' => 'halyang.dream05@gmail.com'
    //   ],
    //   [
    //     'name' => 'Rendra Suriadi',
    //     'username' => 'SVSUR91',
    //     'no_telp' => '081277774477',
    //     'email' => 'rendra477mmm@gmail.com'
    //   ],
    //   [
    //     'name' => 'MUHAMAD SUTRISNA WULANNADI',
    //     'username' => 'SVSPV99',
    //     'no_telp' => '08971708961',
    //     'email' => 'muhammadwandi1993@gmail.com'
    //   ],
    //   [
    //     'name' =>'Abdul Basit',
    //     'username' => 'SV6KS21',
    //     'no_telp' => '0813-4500-1269',
    //     'email' => 'basith847@gmail.com'
    //   ],
    //   [
    //     'name' =>'Ainal Yaqin',
    //     'username' => 'SV6KS13',
    //     'no_telp' => '085348654305',
    //     'email' => 'inangbarbar@gmail.com'
    //   ],
    //   [
    //     'name' => 'Akhmad Sobari',
    //     'username' => 'SV6KS20',
    //     'no_telp' => '082256324616',
    //     'email' => 'sobariahmad1705@gmail.com'
    //   ],
    //   [
    //     'name' => 'STEPY ANGGRAINI',
    //     'username' => 'SV6KS15',
    //     'no_telp' => '082250519827',
    //     'email' => 'stepyanggraini99@gmail.com'
    //   ],
    // ];

    // foreach ($data as $k => $v)
    // {
    //   $check = DB::table('user')->Where('id_user', $v['username'])->first();

    //   if(is_null($check) )
    //   {
    //     DB::table('1_2_employee')->insert([
    //       'nik'               => $v['username'],
    //       'nik_amija'         => $v['username'],
    //       'nama'              => $v['name'],
    //       'sub_directorat'    => "Consumer Service",
    //       'sub_sub_unit'      => "MULTISKILL",
    //       'atasan_1'          => 940437,
    //       'no_telp'			      => $v['no_telp'],
    //       'email'				      => $v['email'],
    //       'status'            => "PKS",
    //       'status_konfirmasi' => "Valid",
    //       'CAT_JOB'           => "Consumer Service",
    //       'ACTIVE'            => 1,
    //       'Witel_New'         => 'KALSEL',
    //       'laborcode'         => preg_replace("/[^a-zA-Z0-9-]+/", '', $v['username'] ),
    //       'mitra_amija'       => 'TELKOM AKSES',
    //       'updateBy'          => 20981020,
    //       'last_update'       => date('Y-m-d H:i:s'),
    //       'dateUpdate'        => date('Y-m-d H:i:s')
    //     ]);

    //     $token = md5($v['username'] . microtime() );

    //     $save_user = [
    //       'id_user'				      => $v['username'],
    //       'password'				    => md5($v['username'].'@telkomakses'),
    //       'level'					      => 61,
    //       'id_karyawan'			    => $v['username'],
    //       'reg'					        => 6,
    //       'witel'					      => 1,
    //       'psb_reg'				      => 0,
    //       'psb_remember_token' 	=> $token,
    //       'updated_user'			  => 20981020,
    //       'datetime_updated'	  => date('Y-m-d H:i:s')
    //     ];

    //     DB::table('user')->insert($save_user);
    //   }
    // }
    Perf::get_perf_ioan_servo();
  }
}
