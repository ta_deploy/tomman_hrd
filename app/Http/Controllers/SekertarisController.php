<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Telegram;
class SekertarisController extends Controller
{
  private $poto = array('UNSPEC','VALINS','Q', 'SUGAR','QC_PSB', 'CABUTAN_NTE', 'MTTR', 'Mytracker', 'Excelent_SPBU','Rekap_Reason_Pelanggan_Cabut','Progress_Uji_Petik_IXSA','Dashboard_NONATERO','STOK_OTP_Prekso');
  private $group = [
      ['id'=>'PROVISIONING', 'text' => 'PROVISIONING'],
      ['id'=>'ASSURANCE', 'text' => 'ASSURANCE'],
      ['id'=>'CORPORATE SERVICE', 'text' => 'CORPORATE SERVICE'],
      ['id'=>'SUPPORT', 'text' => 'SUPPORT'],
      ['id'=>'DEPLOYMENT', 'text' => 'DEPLOYMENT']
    ];
  // public function list()
  // {
  //   $data = DB::select('select *,
  //     (select UNSPEC from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS UNSPEC,
  //     (select VALINS from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS VALINS,
  //     (select Q from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS Q,
  //     (select SUGAR from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS SUGAR,
  //     (select QC_PSB from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS QC_PSB,
  //     (select CABUTAN_NTE from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS CABUTAN_NTE,
  //     (select MTTR from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS MTTR,
  //     (select Mytracker from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS Mytracker,
  //     (select Excelent_SPBU from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS Excelent_SPBU,
  //     (select Rekap_Reason_Pelanggan_Cabut from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS Rekap_Reason_Pelanggan_Cabut,
  //     (select Progress_Uji_Petik_IXSA from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS Progress_Uji_Petik_IXSA,
  //     (select Dashboard_NONATERO from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS Dashboard_NONATERO,
  //     (select STOK_OTP_Prekso from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS STOK_OTP_Prekso,
  //     (select TGL_UPDATE from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS tgl_update
  //     from anggta_ring3_laporan where posision IN("Team Leader Provisioning & Migrasi","Team Leader Sektor IOAN","Site Manager Provisioning & Migration","Team Leader Helpdesk IOAN", "Team Leader BGES", "Team Leader MO SPBU", "Team Leader TSEL & OLO Services","Site Manager Helpdesk","Team Leader Inventory & Asset Management") order by -tgl_update desc');
  //   return view('sekertaris.list', compact('data') );
  // }
  public function listnew()
  {
    $laporan = $this->getLaporan();
    $rg = DB::select('select *,
      (select TGL_UPDATE from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS tgl_update,
      (select payload from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS jenis_laporan
      from anggta_ring3_laporan where payload is not null order by -tgl_update desc');
    // dd($rg);
    $data = [];
    foreach($rg as $r){
      if($r->grouping){
        $data[$r->grouping][] = $r;
      }
    }
    // dd($data);
    return view('sekertaris.listnew', compact('data','laporan') );
  }
  public function report($date)
  {
    // dd();
    $start = date('Y-m-01');
    $today = date('Y-m-d');

    $rg = DB::select('select *,
      (select SEC_TO_TIME(AVG(TIME_TO_SEC(DATE_FORMAT(TGL_UPDATE, "%H:%i") ) ) ) from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and (TGL_UPDATE between "'.$start.'" and "'.$today.'") ) AS avg_tgl_update,
      (select count(*) from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and (TGL_UPDATE between "'.$start.'" and "'.$today.'") ) AS count_lapor
      from anggta_ring3_laporan where grouping != "" and payload is not null
ORDER BY -avg_tgl_update DESC');
    // dd($rg);
    $data = [];
    foreach($rg as $r){
      if($r->grouping){
        $data[$r->grouping][] = $r;
      }
    }
    return view('sekertaris.report', compact('data') );
  }

  // public function form()
  // {
  //   $poto = $this->poto;
  //   $class = array('UNSPEC'=>['TL_Assurance','Team_Leader_BGES'],
  //     'VALINS'=>['TL_Assurance','Team_Leader_BGES'],
  //     'Q'=>['TL_Hdesk_Assurance','Team_Leader_TSEL'],
  //     'SUGAR'=>['TL_Hdesk_Assurance'],
  //     'QC_PSB'=>['TL_Provisioning'],
  //     'CABUTAN_NTE'=>['SM_Provisioning'],
  //     'Mytracker'=>['Team_Leader_MO_SPBU'],
  //     'Excelent_SPBU'=>['Team_Leader_MO_SPBU'],
  //     'MTTR'=>['Team_Leader_TSEL'],
  //     'Rekap_Reason_Pelanggan_Cabut'=>['Site_Manager_Helpdesk'],
  //     'Progress_Uji_Petik_IXSA'=>['Site_Manager_Helpdesk'],
  //     'Dashboard_NONATERO'=>['TL_Hdesk_Assurance'],
  //     'STOK_OTP_Prekso'=>['Team_Leader_Inventory_Asset_Management']
  //   );
  //   //
  //   return view('sekertaris.form', compact('poto','class') );
  // }

  // public function save(Request $req)
  // {

  //   $profile = DB::table('anggta_ring3_laporan')->where('nik', $req->nik)->first();
  //   if(count($profile) ){
  //     //update laporan
  //     $laporan = DB::table('ring3_laporan')->where('nik', $req->nik)->where('TGL_UPDATE', 'like', date('Y-m-d').'%')->first();
  //     if(count($laporan) ){
  //       DB::table('ring3_laporan')->where('id',$laporan->id)->update([
  //         "nik"       =>$profile->nik,
  //         "witel"     =>$profile->witel,
  //         "nama"      =>$profile->nama,
  //         "position"  =>$profile->posision
  //       ]);
  //       $id = $laporan->id;
  //     }else{
  //       //insert laporan
  //       $id = DB::table('ring3_laporan')->insertGetId([
  //         "nik"       =>$profile->nik,
  //         "witel"     =>$profile->witel,
  //         "nama"      =>$profile->nama,
  //         "position"  =>$profile->posision
  //       ]);

  //     }
  //     $poto = $this->poto;
  //     $update =[];
  //     foreach($poto as $foto){
  //       $input = $foto;
  //       if ($req->hasFile($input) ) {
  //         // dd($input);
  //         $path = public_path().'/upload/sekertaris/'.$id.'/';
  //         if (!file_exists($path) ) {
  //           if (!mkdir($path, 0770, true) )
  //             return 'gagal menyiapkan folder foto evidence';
  //         }
  //         $file = $req->file($input);
  //         $ext = 'jpg';
  //         //TODO: path, move, resize
  //         try {
  //           $moved = $file->move("$path", "$input.$ext");
  //           Telegram::sendPhoto([
  //             'chat_id' => '-527605446',
  //             'caption' => str_replace('_',' ',$input)."\n".($profile->sektor?:$profile->witel)."\n".$profile->nama."\n".$profile->nik,
  //             'photo'   => $path.$input.".".$ext,
  //             'parse_mode' =>'html'
  //           ]);
  //           $update["$foto"]=1;
  //         }
  //         catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
  //           return 'gagal menyimpan foto evidence '.$input;
  //         }
  //       }
  //     }
  //     DB::table('ring3_laporan')->where('id', $id)->update($update);
  //     $this->getGambar();
  //     return redirect('/formLaporanRing3')->with('alerts', [
  //       ['type' => 'success', 'text' => 'Laporan Berhasil']
  //     ]);

  //   }else{
  //     return redirect('/formLaporanRing3')->with('alerts', [
  //       ['type' => 'error', 'text' => 'nik tidak ditemukan']
  //     ]);
  //   }
  // }
  public function getGambar()
  {
    // dd(public_path().'/out.png');
    $puppetdir    = "/srv/htdocs/puppet_wand";
    passthru("node $puppetdir/screnshot.js");
    Telegram::sendPhoto([
      'chat_id' => '-1001713057666',
      'caption' => "Laporan harian Ring3.\nTgl ".date('d')." Jam ".date('H:i'),
      'photo'   => public_path().'/out.png',
      'parse_mode' =>'html'
    ]);
  }
  // public function getGambarNew()
  // {
  //   // dd(public_path().'/out.png');
  //   $puppetdir    = "/srv/htdocs/puppet_wand";
  //   passthru("node $puppetdir/screnshotnew.js");
  //   Telegram::sendPhoto([
  //     'chat_id' => '52369916',
  //     'caption' => "Laporan harian Provisioning, Assurance dan Corporate.\nTgl ".date('d')." Jam ".date('H:i'),
  //     'photo'   => public_path().'/outnew.png',
  //     'parse_mode' =>'html'
  //   ]);
  // }
  public function mttrGambar()
  {
    // dd(public_path().'/out.png');
    $puppetdir    = "/srv/htdocs/puppet_wand";
    passthru("node $puppetdir/screnshotmttr.js");
    Telegram::sendPhoto([
      'chat_id' => '-1001713057666',
      'caption' => "MTTR (Mean Time To Report) Leaders, Bulan ".date('F').".",
      'photo'   => public_path().'/outmttr.png',
      'parse_mode' =>'html'
    ]);
  }
  //trial -519429171
  // ring3 -527605446
  // me 52369916

  public function formnew($nik = false)
  {
    $data = DB::table('anggta_ring3_laporan')->select(DB::raw('anggta_ring3_laporan.*,(select TGL_UPDATE from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and TGL_UPDATE like "'.date("Y-m-d").'%") AS tgl_update') )->where('nik', $nik)->first();

    // $data->tgl_update=1;
    // dd($data,'asd');
    if($data){

      if($data->payload){
        $poto = json_decode($data->payload);
        $laporan = $this->getLaporan();
        return view('sekertaris.formnew', compact('poto','data','laporan') );
        // dd($poto);
      }else{
        return redirect()->back()
                    ->with('alerts', [
          ['type' => 'danger', 'text' => 'Tidak tersedia Laporan untuk nik '.$data->nik]
        ]);
      }
    }else{
      return redirect()->back()
                    ->with('alerts', [
          ['type' => 'danger', 'text' => 'Nik Tidak ada']
        ]);
    }
    // dd($nik);
  }
  public function savenew(Request $req)
  {

    $profile = DB::table('anggta_ring3_laporan')->where('nik', $req->nik)->first();
    if(count($profile) ){
      //update laporan
      $laporan = DB::table('ring3_laporan')->where('nik', $req->nik)->where('TGL_UPDATE', 'like', date('Y-m-d').'%')->first();
      $input = ['nik'=>$profile->nik,'witel'=>$profile->witel,'nama'=>$profile->nama,'grouping'=>$profile->grouping];
      $input['position']=$profile->posision;

      if(count($laporan) ){
        DB::table('ring3_laporan')->where('id',$laporan->id)->update($input);
        $id = $laporan->id;
      }else{
        //update start jam laporan set jam 7
        if( (1*date('H') ) < 7){
          $TGL_UPDATE = date('Y-m-d 07:00:00');
        }else{
          $TGL_UPDATE = date('Y-m-d H:i:s');
        }
        $input['TGL_UPDATE'] = $TGL_UPDATE;
        //insert laporan
        $id = DB::table('ring3_laporan')->insertGetId($input);
      }
      $jenis_laporan = $this->getLaporan();
      $poto = json_decode($profile->payload);
      $update =[];
      foreach($poto as $foto){
        $input = $foto;
        if ($req->hasFile($input) ) {
          // dd($input);
          $path = public_path().'/upload/sekertaris/'.$id.'/';
          if (!file_exists($path) ) {
            if (!mkdir($path, 0770, true) )
              return 'gagal menyiapkan folder foto evidence';
          }
          $file = $req->file($input);
          $ext = 'jpg';
          //TODO: path, move, resize
          try {
            $moved = $file->move("$path", "$input.$ext");
            Telegram::sendPhoto([
              'chat_id' => '-1001713057666',
              'caption' => str_replace('_',' ',$jenis_laporan[$input]['text'])."\n".($profile->sektor?:$profile->witel)."\n".$profile->nama."\n".$profile->nik,
              'photo'   => $path.$input.".".$ext,
              'parse_mode' =>'html'
            ]);
            if($profile->cc){
              Telegram::sendPhoto([
                'chat_id' => $profile->cc,
                'caption' => str_replace('_',' ',$jenis_laporan[$input]['text'])."\n".($profile->sektor?:$profile->witel)."\n".$profile->nama."\n".$profile->nik,
                'photo'   => $path.$input.".".$ext,
                'parse_mode' =>'html'
              ]);
            }
            $update[]=$foto;
          }
          catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto evidence '.$input;
          }
        }
      }
      DB::table('ring3_laporan')->where('id', $id)->update(['payload' => json_encode($update)]);
      $this->getGambar();
      return redirect()->back()->with('alerts', [
        ['type' => 'success', 'text' => 'Laporan Berhasil']
      ]);

    }else{
      return redirect()->back()->with('alerts', [
        ['type' => 'success', 'text' => 'nik tidak ditemukan']
      ]);
    }
  }
  private function getLaporan(){
    $jenis_laporan = DB::table('ring3_jenis_laporan')->select('id','jenis_laporan as text','required')->where('status', 1)->get();
    $laporan = [];
    foreach($jenis_laporan as $jl){
      $laporan[$jl->id] = ["text"=>$jl->text,"required"=>$jl->required];
    }
    return $laporan;
  }
  public function setuplist()
  {
    $laporan = $this->getLaporan();
    // dd($laporan);
    $reporter = DB::table('anggta_ring3_laporan')->get();
    $group = json_decode(json_encode($this->group) );
    $rg = [];
    foreach($reporter as $r){
      if($r->grouping){
        $rg[$r->grouping][] = $r;
      }
    }
    // dd($reporter,$rg);
    return view('sekertaris.setuplist', compact('rg','reporter', 'laporan') );
  }
  public function setupform($nik)
  {
    $cc = DB::table('group_telegram')->select('chat_id as id','title as text')->get();
    $jenis_laporan = DB::table('ring3_jenis_laporan')->select('id','jenis_laporan as text','required')->where('status', 1)->get();
    $data = DB::table('anggta_ring3_laporan')->where('nik', $nik)->first();
    $group = json_decode(json_encode($this->group) );
    // dd($jenis_laporan);
    return view('sekertaris.setupform', compact('group','jenis_laporan','data','cc') );
  }
  public function setupsave($nik,Request $req)
  {
    // dd($req->required);
    $data = $req->only('witel','nama','posision','sektor','grouping','cc');
    $data['payload'] = json_encode($req->jenis_laporan);
    if($nik == 'new'){
      //input
      if(DB::table('anggta_ring3_laporan')->where('nik', $req->nik)->first() ){
        return redirect()->back()
                    ->withInput($req->all() )
                    ->with('alerts', [
          ['type' => 'danger', 'text' => 'nik sudah ada']
        ]);
      }else{
        $data['nik']=$req->nik;
        DB::table('anggta_ring3_laporan')->insert($data);

        return redirect()->back()->with('alerts', [
          ['type' => 'success', 'text' => 'berhasil insert data']
        ]);
      }
    }else{
      //update
      if(DB::table('anggta_ring3_laporan')->where('nik', $nik)->first() ){
        //update

        DB::table('anggta_ring3_laporan')->where('nik', $nik)->update($data);

        return redirect('/listSetupLaporanRing3')->with('alerts', [
          ['type' => 'success', 'text' => 'berhasil update data']
        ]);
      }else{
        return redirect('/listSetupLaporanRing3')->with('alerts', [
          ['type' => 'danger', 'text' => 'nik tidak ditemukan']
        ]);
      }
    }
  }

  public function tematiksave(Request $req)
  {
    $exists = DB::table('ring3_jenis_laporan')->select('id','jenis_laporan as text','required')->where('jenis_laporan',$req->jenis_tematik)->first();
    if($exists){
      return json_encode($exists);
    }else{
      $id = DB::table('ring3_jenis_laporan')->insertGetId(["jenis_laporan"=>$req->jenis_tematik,"required"=>$req->required,"status"=>1]);
      $required = "Optional";
      if($req->required){
        $required = "Required";
      }
      return json_encode(DB::table('ring3_jenis_laporan')->select('id','jenis_laporan as text','required')->where('id',$id)->first() );
    }

  }
  public function hapusLaporan($nik)
  {
    $exists = DB::table('anggta_ring3_laporan')->where('nik',$nik)->first();
    if($exists){
      DB::table('anggta_ring3_laporan')->where('nik', $nik)->update([
        'grouping'=> '',
        'payload' =>  null
      ]);
      return redirect('/listSetupLaporanRing3')->with('alerts', [
          ['type' => 'success', 'text' => 'Berhasil Menghapus laporan dan grouping']
        ]);
    }else{
      return redirect('/listSetupLaporanRing3')->with('alerts', [
          ['type' => 'warning', 'text' => 'nik tidak ditemukan']
        ]);
    }

  }
  public function mvp($date = false)
  {
    if($date){
      $start = explode(':', $date)[0] ." 00:00:01";
      $today = explode(':', $date)[1] ." 23:59:59";
    }else{
      $start = date('Y-m-d 00:00:01',strtotime("monday last week") );
      $today = date('Y-m-d 23:59:59',strtotime("monday this week - 1 second") );
    }

    $data = DB::select('select *,
      (select SEC_TO_TIME(AVG(TIME_TO_SEC(DATE_FORMAT(TGL_UPDATE, "%H:%i") ) ) ) from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and (TGL_UPDATE between "'.$start.'" and "'.$today.'") ) AS avg_tgl_update,
      (select count(*) from ring3_laporan where ring3_laporan.nik=anggta_ring3_laporan.nik and (TGL_UPDATE between "'.$start.'" and "'.$today.'") ) AS count_lapor
      from anggta_ring3_laporan where grouping != "" and payload is not null
ORDER BY -avg_tgl_update DESC');
    // dd($data);
    return view('sekertaris.mvp', compact('data') );
  }

  //ioan
  public function leaderboard($tgl_param = null)
  {
    // $data = DB::Table('performance_ioan_average As pia')
    // ->LeftJoin('detail_performance_ioan As dpi', 'pia.id', '=', 'dpi.id_sektor')
    // ->select('pia.sektor', 'pia.average', 'pia.witel', 'dpi.*')
    // ->get();

    if(is_null($tgl_param) ) $tgl_param = date('Y-m');

    $data = DB::Select("SELECT * FROM performance_ioan_average pia WHERE sektor != 'NON SEKTOR' ORDER BY tgl_data DESC");

    $new_data = $witel = [];

    $tgl = '';

    foreach($data as $k => $v)
    {
      $tgl = $data[0]->created_at;

      $witel[$v->witel] = $v->witel;
    }

    return view('sekertaris.leaderboard', compact('data', 'witel', 'tgl', 'tgl_param') );
  }

  public function get_laporan_performance()
  {
    // dd(public_path().'/out.png');
		exec('cd /srv/htdocs/puppet_rend;nodejs ranked_leaderboard_ass.js');
		exec('cd /srv/htdocs/puppet_rend;nodejs ranked_leaderboard_prov.js');

    Telegram::sendPhoto([
      'chat_id' => '-519429171',
      'caption' => "Laporan Performansi Assurance.\nTgl ".date('d')." Jam ".date('H:i'),
      'photo'   => '/srv/htdocs/puppet_rend/ranked_performance_assurance.png',
      'parse_mode' =>'html'
    ]);

    Telegram::sendPhoto([
      'chat_id' => '-519429171',
      'caption' => "Laporan Performansi Provisioning.\nTgl ".date('d')." Jam ".date('H:i'),
      'photo'   => '/srv/htdocs/puppet_rend/ranked_performance_provisioning.png',
      'parse_mode' =>'html'
    ]);
  }

  public function leaderboard_ajax(Request $req)
  {
    $sql = '';

    if($req->witel != 'all')
    {
      $sql .= " AND witel = '$req->witel' ";
    }

    $sql .= " AND sektor != 'NON SEKTOR' ";

    $data = DB::Select("SELECT * FROM `performance_ioan_average` where 1 ".$sql." ORDER BY `performance_ioan_average`.`sektor` ASC,tgl_data asc");
    $arraysektor = array();

    $tgl_load = $req->tgl;

    if(strlen($tgl_load) < 5 )
    {
      $tgl_load = $tgl_load .'-12';
    }

    $bln = [];
    $year = date('Y', strtotime($tgl_load) );

    $start = $month = strtotime($year.'-01' );
    $end = strtotime(date('Y-m', strtotime($tgl_load) ) );

    while($month <= $end)
    {
      $bln[] = ['tgl_data' => date('Y-m', $month), 'result' => '-'];
      $month = strtotime("+1 month", $month);
    }

    foreach($data as $d)
    {
      if(!isset($arraysektor[$d->sektor .'-'. $d->witel]['result'][0]) )
      {
        $arraysektor[$d->sektor .'-'. $d->witel]['result'] = $bln;
        $arraysektor[$d->sektor .'-'. $d->witel]['last_nilai'] = '-';
      }

      $find_k = array_search($d->tgl_data, array_column($arraysektor[$d->sektor .'-'. $d->witel]['result'], 'tgl_data') );

      if($find_k !== FALSE)
      {
        $arraysektor[$d->sektor .'-'. $d->witel]['result'][$find_k]['result'] = $d->result;
        $arraysektor[$d->sektor .'-'. $d->witel]['last_nilai'] = $d->average;
      }

      $arraysektor[$d->sektor .'-'. $d->witel]['sektor'] = $d->sektor;
      $arraysektor[$d->sektor .'-'. $d->witel]['witel']  = $d->witel;
    }

    usort($arraysektor, function($a, $b) {
      return $b['last_nilai'] <=> $a['last_nilai'];
    });

    return view('sekertaris.ajaxleaderboard', compact('arraysektor', 'tgl_load') );
    // dd($arraysektor);
    // dd($data);
    // return \Response::json($data);
  }

  public function leaderboard_detail($sektor, $thn, $tgl_load)
  {
    $data = DB::Select("SELECT * FROM `performance_ioan_average` where sektor='".$sektor."' ORDER BY `performance_ioan_average`.`sektor` ASC, tgl_data ASC");

    $bln = $result = [];

    if(strlen($tgl_load) < 5 )
    {
      $tgl_load = $tgl_load .'-12';
    }

    $year = date('Y', strtotime($tgl_load) );

    $start = $month = strtotime($year.'-01' );
    $end = strtotime(date('Y-m', strtotime($tgl_load) ) );

    while($month <= $end)
    {
      $bln[date('Y-m', $month)] = '-';
      $result[date('Y-m', $month)] = 4;
      $month = strtotime("+1 month", $month);
    }


    // $kpi = [
    //   "a. Q Gangguan IOAN"                         => $bln,
    //   "b. Assurance Guarantee"                     => $bln,
    //   "c. SQM Proactive Tiket"                     => $bln,
    //   "d. TTR COMPLY - 3 JAM"                      => $bln,
    //   "e. TTR COMPLY - 3 JAM HVC Platinum & Gold"  => $bln,
    //   "f. TTR COMPLY - 12 JAM"                     => $bln,
    //   "g. TTR COMPLY - 12 JAM HVC Silver"          => $bln,
    //   "h. TTR COMPLY - 48 JAM"                     => $bln,
    //   "i. TTR COMPLY - 3 JAM MANJA"                => $bln,
    //   "l. Underspec All"                           => $bln,
    //   "m. Underspec HVC"                           => $bln,
    //   "n. Underspec IN%"                           => $bln,
    //   "o1. Tangible ODP (Produktivitas)"           => $bln,
    //   "o2. Tangible ODP (Kualitas)"                => $bln,
    //   "p1. Tangible ODC (Kuantitas)"               => $bln,
    //   "p2. Tangible ODC (Kualitas)"                => $bln,
    //   "j. Validasi Data (Qrcode, Valins Port ODP)" => $bln,
    //   "k. Validasi Port Assurance & Maintenance"   => $bln
    // ];

    $kpi = $final_kpi = $title_benar = [];

    $title_benar =[
       'a. Q Gangguan IOAN' => ['a. Q gangguan'],
       'b. Assurance Guarantee' => ['b. ASR Guarantee'],
       'c. SQM Proactive Tiket' => ['d. TTR Comply 1DS (12 Jam) dengan SQM'],
       'd. TTR COMPLY - 3 JAM' => ['c. TTR Comply 3 jam dengan SQM'],
       'j. Validasi Data (Qrcode, Valins Port ODP)' => ['e. Validasi Data'],
    ];

    foreach($data as $no => $d)
    {
      foreach($bln as $kk => $vv)
      {
        if($kk == $d->tgl_data)
        {
          $arr = json_decode(json_encode(json_decode($d->payload_kpi) ), TRUE);
          $check_field = array_column($arr, 'indikator');

          foreach($check_field as $v)
          {
            if(!isset($kpi[$v]) )
            {

              $collect = array_map(function($x) use($bln){
                return $bln;
              }, array_flip(array_column($arr, 'indikator') ) );

              $kpi = array_merge($kpi, $collect);
            }
          }

          foreach($arr as $i)
          {
            $kpi[$i['indikator'] ][$d->tgl_data] = $i['pencapaian'];
          }

          $result[$d->tgl_data] = $d->result;
        }
      }
    }

    foreach($kpi as $k => &$v)
    {
      foreach($title_benar as $kk => $vv)
      {
        if(in_array($k, $vv) )
        {
          foreach($v as $k3 => $v3)
          {
            if($v3 != '-')
            {
              $kpi[$kk][$k3] = $v3;
              unset($kpi[$k]);
            }
          }
        }
      }
    }

    unset($v);
    ksort($kpi);

    return view('sekertaris.ajaxdetilleaderboard',compact('kpi', 'bln', 'result') );
  }

  //prov
  public function leaderboardprov($tgl_param = null)
  {
    // $data = DB::Table('performance_ioan_average As pia')
    // ->LeftJoin('detail_performance_ioan As dpi', 'pia.id', '=', 'dpi.id_sektor')
    // ->select('pia.sektor', 'pia.average', 'pia.witel', 'dpi.*')
    // ->get();

    $data = DB::Select("SELECT * FROM performance_prov pia ORDER BY tgl_data DESC");

    $new_data = $witel = [];

    $tgl = '';

    if(is_null($tgl_param) ) $tgl_param = date('Y-m');

    foreach($data as $k => $v)
    {
      $tgl = $v->created_at;

      $witel[$v->witel] = $v->witel;
    }
    // dd("a");
    return view('sekertaris.raport.prov', compact('data', 'witel', 'tgl', 'tgl_param') );
  }

  public function leaderboard_ajax_prov(Request $req)
  {
    $sql = '';

    if($req->witel != 'all')
    {
      $sql .= " AND witel = '$req->witel' ";
    }

    $data = DB::Select("SELECT * FROM `performance_prov` where 1 ".$sql." ORDER BY `performance_prov`.`sektor` ASC,tgl_data asc");
    $arraysektor = array();

    $tgl_load = $req->tgl;

    if(strlen($tgl_load) < 5 )
    {
      $tgl_load = $tgl_load .'-12';
    }

    $bln = [];
    $year = date('Y', strtotime($tgl_load) );

    $start = $month = strtotime($year.'-01' );
    $end = strtotime(date('Y-m', strtotime($tgl_load) ) );

    while($month <= $end)
    {
      $bln[] = ['tgl_data' => date('Y-m', $month), 'result' => '-'];
      $month = strtotime("+1 month", $month);
    }

    foreach($data as $d)
    {
      if(!isset($arraysektor[$d->sektor]['result'][0]) )
      {
        $arraysektor[$d->sektor]['result'] = $bln;
        $arraysektor[$d->sektor]['last_nilai'] = '-';
        $arraysektor[$d->sektor]['ps_re']      = '-';
      }

      $find_k = array_search($d->tgl_data, array_column($arraysektor[$d->sektor]['result'], 'tgl_data') );

      if($find_k !== FALSE)
      {
        $arraysektor[$d->sektor]['result'][$find_k]['result'] = $d->result;
        $arraysektor[$d->sektor]['last_nilai'] = $d->average;
        $arraysektor[$d->sektor]['ps_re']      = $d->ps_re;
      }

      $arraysektor[$d->sektor]['sektor'] = $d->sektor;
      $arraysektor[$d->sektor]['witel'] = $d->witel;
    }

    @array_multisort(@array_column($arraysektor, 'last_nilai'),  SORT_DESC,
    @array_column($arraysektor, 'ps_re'), SORT_DESC,
    $arraysektor);

    return view('sekertaris.raport.ajaxprov', compact('arraysektor', 'tgl_load') );
  }

  public function leaderboard_detail_prov($sektor, $thn, $tgl_load)
  {
    $data = DB::Select("SELECT * FROM `performance_prov` where sektor='".$sektor."' ORDER BY `performance_prov`.`sektor` ASC, tgl_data asc");
    $bln = $result = [];

    if(strlen($tgl_load) < 5 )
    {
      $tgl_load = $tgl_load .'-12';
    }

    $year = date('Y', strtotime($tgl_load) );

    $start = $month = strtotime($year.'-01' );
    $end = strtotime(date('Y-m', strtotime($tgl_load) ) );

    while($month <= $end)
    {
      $bln[date('Y-m', $month)]    = '-';
      $result[date('Y-m', $month)] = 4;
      $total[date('Y-m', $month)]  = '-';

      $month = strtotime("+1 month", $month);
    }

    $kpi = [
      "PS/PI"                   => $bln,
      "TTI COMPLY"              => $bln,
      "GGN < 60 HARI PSB"       => $bln,
      "VALIDASI CORE PELANGGAN" => $bln,
      "TTR FFG"                 => $bln,
      "UJI PETIK COMPLIANCE"    => $bln,
      "QC RETURN"               => $bln
    ];

    foreach($data as $no => $d)
    {
      foreach($bln as $kk => $vv)
      {
        if($kk == $d->tgl_data)
        {
          foreach(json_decode($d->payload_kpi) as $i)
          {
            $kpi[$i->indikator][$d->tgl_data] = $i->pencapaian;
          }

          $result[$d->tgl_data] = $d->result;
          $total[$d->tgl_data] = $d->average;
        }
      }
    }

    return view('sekertaris.raport.ajaxdetil',compact('kpi','bln', 'result', 'total') );
  }
  public function inputkpiprov()
  {
    return view('sekertaris.raport.input');
  }
  public function saveinputkpiprov(Request $req)
  {
    $postrequest = trim($req->paste);
    $dirty = array("\n", "\t", "\r");
    $clean = array('<br>', ';', '');
    $coma = array(",", "%");
    $fl = array('.', '');
    $request = str_replace($dirty, $clean, $postrequest);
    $date=date('Y-m-d H:i:s');
    foreach(explode('<br>', $request) as $rows){
      $data = explode(';',$rows);
      $result = 0;
      if($data[24]>=100){
        $result = 1;
      }
      $payload='[{"indikator":"PS/PI","pencapaian":"'.$data[5].'","tgl_update":"'.$date.'"},{"indikator":"TTI COMPLY","pencapaian":"'.$data[8].'","tgl_update":"'.$date.'"},{"indikator":"GGN < 60 HARI PSB","pencapaian":"'.$data[11].'","tgl_update":"'.$date.'"},{"indikator":"VALIDASI CORE PELANGGAN","pencapaian":"'.$data[14].'","tgl_update":"'.$date.'"},{"indikator":"TTR FFG","pencapaian":"'.$data[17].'","tgl_update":"'.$date.'"},{"indikator":"UJI PETIK COMPLIANCE","pencapaian":"'.$data[20].'","tgl_update":"'.$date.'"},{"indikator":"QC RETURN","pencapaian":"'.$data[23].'","tgl_update":"'.$date.'"}]';
      $sektor[] = ["witel"=>$data[1],"sektor"=>"SEKTOR - ".$data[1]." - ".$data[2],"average"=>str_replace($coma, $fl, $data[24]),"result"=>$result,"tgl_data"=>$req->tgl_data,"payload_kpi"=>($payload)];
    }
    DB::table('performance_prov')->where('tgl_data', $req->tgl_data)->delete();
    DB::table('performance_prov')->insert($sektor);
    return redirect('/leaderboardprov');
  }
}
