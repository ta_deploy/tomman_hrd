<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Session;
use App\DA\HomeModel;
use App\DA\HrModel;
use App\DA\ApiModel;
use Validator;
use Telegram;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

class HomeController extends Controller
{
  public $fileRecruitment = [
		'Pakta Integritas MYI-Tech' => 'file_pakta_integritas',
    'KTP' => 'file_ktp',
		'Kartu Keluarga' => 'file_kk',
    'Kartu SIM C' => 'file_sim',
    'Kartu SIM B' => 'file_sim_b',
    'Sertifikat Multimedia' => 'sertifikat_multim',
		'CV' => 'file_cv',
		'Ijazah Terakhir' => 'file_ijazah',
		'Pengalaman Kerja / Sertifikat / Piagam' => 'file_sertifikat',
		'Sertifikat Vaksin Terakhir' => 'file_sertifikat_vaksin'
	];

  public function performance()
  {
    $startDate = Input::get('startDate');
    $endDate = Input::get('endDate');

    if(in_array(null, [$startDate, $endDate]) )
    {
      $startDate = date('Y-m-d', strtotime("first day of this month") );
      $endDate = date('Y-m-d', strtotime("last day of this month") );
    }

    $get_orders = HomeModel::get_orders(date('Y') );
    $get_performansi = HomeModel::get_performansi($startDate, $endDate);

    $get_karyawan = HomeModel::get_karyawan();

    $get_performansi = json_decode(json_encode($get_performansi), FALSE);
    // dd($get_karyawan);
    return view('home.performance', compact('startDate','endDate','get_orders','get_performansi','get_karyawan') );
  }

  public function home_employee(){
    $get_belum_brevert = HomeModel::brevert_dan_akun("BELUM_BREVET");
    $get_lulus_brevert = HomeModel::brevert_dan_akun("LULUS_BREVET");
    $get_belum_akun = HomeModel::brevert_dan_akun("BELUM_AKUN");
    $monolog_mytech = HomeModel::monolog_mytech();

    $total_pelamar = HomeModel::get_total_pelamar_front('pelamar');
    $total_rekrutmen = HomeModel::get_total_pelamar_front('rekrut');
    $total_rejected = HomeModel::get_total_pelamar_front('rejected');
    $total_diterima = HomeModel::get_total_pelamar_front('accept');
    return view('home.employee',compact('get_belum_brevert','get_lulus_brevert','get_belum_akun','monolog_mytech','total_pelamar',
    'total_rekrutmen','total_rejected','total_diterima') );
  }

  public function home_beranda(Request $req)
  {
    //redirect amun not penting you know
    if (!in_array(session('auth')->perwira_level,[1,69,30,46,71]) )
    {
      return redirect('/performance');
    }

    $tahun_rec  = "YEAR(created_at) = YEAR(NOW() )";
    $tahun_user  = "YEAR(dateUpdate) = YEAR(NOW() )";
    $data_pelamar = $data_rekrut = $data_rejected = $data_accept = [];
    $sql_jk = $total_u_rec = $total_u_user = $sql_daerah = $data_sub = $sql_mit = '';

    if($req->all() )
    {
      $data_sub = $req->all();

      if($req->jk)
      {
        $jk = "('".implode("','", $req->jk)."')";
        $sql_jk = "AND jk IN $jk";
      }

      if($req->tahun)
      {
        $tahun_rec = "YEAR(created_at) = $req->tahun";
        $tahun_user = "YEAR(dateUpdate) = $req->tahun";
      }

      if($req->umur)
      {
        if(in_array('18_25', $req->umur) )
        {
          $umur_rec[] ="YEAR(NOW() ) - YEAR(tgl_lahir) BETWEEN 18 AND 25";
          $umur_user[] ="YEAR(NOW() ) - YEAR(tgl_lahir) BETWEEN 18 AND 25";
        }

        if(in_array('25_', $req->umur) )
        {
          $umur_rec[] ="YEAR(NOW() ) - YEAR(tgl_lahir) > 25";
          $umur_user[] ="YEAR(NOW() ) - YEAR(tgl_lahir) > 25";
        }

        if($umur_rec)
        {
          $total_u_rec = "AND (". implode(' OR ',$umur_rec).")";
        }

        if($umur_user)
        {
          $total_u_user = "AND (". implode(' OR ',$umur_user).")";
        }
      }

      $daerah = [];
      if($req->provinsi)
      {
        $daerah[] = "provinsi = $req->provinsi";
      }

      if($req->kota)
      {
        $daerah[] = "kota = $req->kota";
      }

      if($req->kecamatan)
      {
        $daerah[] = "kecamatan = $req->kecamatan";
      }

      if($req->kelurahan)
      {
        $daerah[] = "kelurahan = $req->kelurahan";
      }

      if($daerah)
      {
        $sql_daerah = "AND ". implode(' AND ',$daerah);
      }
    }

    if(in_array(session('auth')->perwira_level, [30, 71]) )
    {
      $sql_mit = "AND user_perwira.mitra = ". session('auth')->mitra. " AND user_perwira.perwira_level IN (46, 22, 100)";
    }

    $get_data_pelamar = HomeModel::get_data_front('pelamar', $req);

    $get_data_rekrut = HomeModel::get_data_front('rekrut', $req);

    $get_data_rejected = HomeModel::get_data_front('rejected', $req);

    $get_data_accept = HomeModel::get_data_front('accept', $req);

    $get_data_tot_lamar = HomeModel::get_data_front('total_lamar', $req);

    $get_data_tot = HomeModel::get_total_front_all($req);

    $get_data_jk = HomeModel::data_jk($req);

    $c_month = [];

    foreach($get_data_pelamar as $val)
    {
      $c_month[$val->tgl] = $val->tgl;
    }

    foreach($get_data_rekrut as $val)
    {
      $c_month[$val->tgl] = $val->tgl;
    }

    foreach($get_data_accept as $val)
    {
      $c_month[$val->tgl] = $val->tgl;
    }

    foreach($get_data_pelamar as $key => $val)
    {
      $data_pelamar[$val->tgl] = $val;
    }

    foreach($get_data_rekrut as $key => $val)
    {
      $data_rekrut[$val->tgl] = $val;
    }

    foreach($get_data_rejected as $key => $val)
    {
      $data_rejected[$val->tgl] = $val;
    }

    foreach($get_data_accept as $key => $val)
    {
      $data_accept[$val->tgl] = $val;
    }

    $pelamar =  $rekrut = $rejected = $accept = [];

    if($c_month)
    {
      ksort($c_month);

      foreach($c_month as $key => $val)
      {
        if ($data_pelamar)
        {
          $pelamar[$key] = (object)['data' => 0, 'tgl' => $key];
          foreach($data_pelamar as $key_c1 => $val_c1)
          {
            $pelamar[$key_c1] = $val_c1;
          }
        }
      }

      foreach($c_month as $key => $val)
      {
        if ($data_rekrut)
        {
          $rekrut[$key] = (object)['data' => 0, 'tgl' => $key];
          foreach($data_rekrut as $key_c1 => $val_c1)
          {
            $rekrut[$key_c1] = $val_c1;
          }
        }
      }

      foreach($c_month as $key => $val)
      {
        if ($data_rejected)
        {
          $rejected[$key] = (object)['data' => 0, 'tgl' => $key];
          foreach($data_rejected as $key_c1 => $val_c1)
          {
            $rejected[$key_c1] = $val_c1;
          }
        }
      }

      foreach($c_month as $key => $val)
      {
        if ($data_accept)
        {
          $accept[$key] = (object)['data' => 0, 'tgl' => $key];
          foreach($data_accept as $key_c1 => $val_c1)
          {
            $accept[$key_c1] = $val_c1;
          }
        }
      }
    }

    $minim_year = HomeModel::get_minim_year();

    $data_prov = HomeModel::api_provinsi();
		// $data_ar = json_decode($data_prov, TRUE);

		foreach($data_prov as $val)
		{
      $get_area[$val->id] = $val->nama;
		}

    $get_pelamar = HomeModel::get_pelamar('all', 'NEW');

    $total_pelamar = HomeModel::get_total_pelamar_front('pelamar');
    $total_rekrutmen = HomeModel::get_total_pelamar_front('rekrut');
    $total_rekrutmen_per_mit = HomeModel::get_total_pelamar_front('rekrut_mit');
    $total_rejected = HomeModel::get_total_pelamar_front('rejected');
    $total_diterima = HomeModel::get_total_pelamar_front('accept');

    return view('home.beranda', compact('get_pelamar', 'pelamar', 'get_area', 'rekrut', 'rejected', 'accept', 'get_data_tot_lamar', 'get_data_jk', 'get_data_tot', 'minim_year', 'data_sub', 'get_data_pelamar', 'get_data_rekrut', 'get_data_rejected', 'get_data_accept', 'total_pelamar', 'total_rekrutmen', 'total_rejected', 'total_diterima', 'total_rekrutmen_per_mit') );
  }

  public function profile()
  {
    $auth = session('auth');
    // dd($auth);
    $accident = HomeModel::history_accident($auth);
    $resling = HomeModel::history_resling($auth);
    $reward = HomeModel::history_reward($auth);
    $fa = HomeModel::history_fa($auth);
    $mutasi = HomeModel::history_mutasi($auth);
    $cuti = HomeModel::history_cuti($auth);
    $file = $this->fileRecruitment;
    $data = HomeModel::myProfile();
    $data_prov = ApiModel::get_provinsi();
		$data_ar = json_decode($data_prov, TRUE);
    $get_direktorat = HomeModel::get_all_direct();
    $get_mitra = HomeModel::get_all_mitra();
    $get_sto = HrModel::get_alamat_d();
    $get_jobdes = HrModel::get_jobdesk();

		foreach($data_ar['provinsi'] as $val)
		{
      $get_area[$val['id']] = $val['nama'];
		}

    return view('hr.profile.profile', compact('auth', 'fa', 'mutasi', 'accident', 'resling', 'reward','file', 'data', 'cuti', 'get_area', 'get_direktorat', 'get_mitra', 'get_sto', 'get_jobdes') );
  }

  public function profile_save(Request $req)
  {
    HomeModel::save_profile($req);

    return redirect('/profile')->with('alerts', [
			['type' => 'success', 'text' => '<strong>SUKSES</strong> Memperbaharui Data Diri']
		]);
  }

  public function info_search($id)
  {
    // dd($auth);
    $data = HomeModel::get_data_search($id);
    $auth = HomeModel::get_up_ID($id);
    $accident = HomeModel::history_accident($auth);
    $resling = HomeModel::history_resling($auth);
    $reward = HomeModel::history_reward($auth);
    $fa = HomeModel::history_fa($auth);
    $mutasi = HomeModel::history_mutasi($auth);
    $cuti = HomeModel::history_cuti($auth);
    $file = $this->fileRecruitment;
    $data_prov = ApiModel::get_provinsi();
		$data_ar = json_decode($data_prov, TRUE);
    $get_direktorat = HomeModel::get_all_direct();
    $get_mitra = HomeModel::get_all_mitra();
    $get_sto = HrModel::get_alamat_d();
    $get_jobdes = HrModel::get_jobdesk();

		foreach($data_ar['provinsi'] as $val)
		{
      $get_area[$val['id']] = $val['nama'];
		}

    return view('hr.profile.profile', compact('auth', 'get_mitra', 'fa', 'mutasi', 'accident','resling','reward','file', 'data', 'cuti', 'get_area', 'get_direktorat', 'sto', 'get_sto', 'get_jobdes') );
  }

  public function view_profile(Request $req)
  {
    if ($req->has('nik') )
    {
      $nik = $req->input('nik');
      $data = HomeModel::view_profile_recruiter($nik);
    } else {
      $nik = session('auth')->id_karyawan;
      $data = HomeModel::view_profile_recruiter($nik);
    }

    $grafik = $req->input('grafik');

    $get_direktorat = DB::table('perwira_direktorat')->get();

    $get_subgroup = DB::table('perwira_subgroup')->get();

    $get_posisi_mitra = DB::table('perwira_posisi_mitra')->get();

    $get_perusahaan_mitra = DB::table('perwira_perusahaan_mitra')->get();

    $get_file = HomeModel::getdetail_ktp($nik);

    $file = $this->fileRecruitment;

    return view('hr.profile.view', compact('data','get_direktorat','get_posisi_mitra','get_perusahaan_mitra','get_file','file','grafik', 'get_subgroup') );
  }

  public function view_profile_save(Request $req)
  {
    $rules = array(
      'input_status' => 'required'
    );

    $messages = [
      'input_status.required' => 'Silahkan Pilih Status !'
    ];

    $input = $req->input();
    $validator = Validator::make($req->all(), $rules, $messages);
    $validator->sometimes('input_status', 'required', function ($input) { return true; });

    if ($validator->fails() ) {
			return redirect()->back()
				->withInput($req->input() )
				->withErrors($validator)
				->with('alerts', [
					['type' => 'danger', 'text' => '<strong>GAGAL</strong> Harap Pilih Status']
				]);
		}
    else
    {
      HomeModel::save_view_profile($req);

      $status = $req->input('input_status');

      if ($status == "REKRUT")
      {
        return redirect('/')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>BERHASIL</strong> Menyimpan Data Pelamar untuk di Rekrut']
        ]);
      }
      elseif (strpos($status, 'ACCEPTED') !== false)
      {
        return redirect('/hr/createNew_user/input')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>BERHASIL</strong> Menyimpan Data untuk Selanjutnya Pembuatan User'],
          ['type' => 'info', 'text' => 'NIK : <strong>'.$req->input('nik').'</strong>']
        ]);
      }
      elseif ($status == "REJECTED")
      {
        return redirect('/')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>BERHASIL</strong> Menyimpan Data Pelamar untuk di Kembalikan / Tidak Di Terima']
        ]);
      }
    }
  }

  public function result_search(Request $req)
  {
    $search = $req->data_search;
    if(is_numeric($search) )
    {
      if(strlen($search) >=10)
      {
        $data = HomeModel::search_data_result('ktp', $search);
      }
      else
      {
        $data = HomeModel::search_data_result('nik', $search);
      }
    }
    else
    {
      $data = HomeModel::search_data_result('nama', $search);
    }

    return view('searchResult', compact('data') );
  }

  public function request_url($method)
  {
    // fungsi untuk mengirim/meminta/memerintahkan sesuatu ke bot
    $TOKEN = "1866922472:AAHcESBKs4qoKiy6GD6_IrLPYEDUv1gx-i4";
    $data = "https://api.telegram.org/bot" . $TOKEN . "/". $method;

    return $data;
  }

  public function get_updates($offset)
  {
    // fungsi untuk meminta pesan
    // bagian ebook di sesi Meminta Pesan, polling: getUpdates
    $url = $this->request_url("getUpdates")."?offset=".$offset;
    $resp = file_get_contents($url);
    $result = json_decode($resp, true);
    if ($result["ok"]==1) return $result["result"];
    return array();
  }

  public function send_reply($chatid, $msgid, $text)
  {
    // fungsi untuk mebalas pesan,
    $keyboard = json_encode([
      "inline_keyboard" => [
        [
          [
            "text" => "Benar",
            "callback_data" => "yes"
          ],
          [
            "text" => "Salah",
            "callback_data" => "no"
          ]
        ]
      ]
    ]);

    $data = [
      'chat_id' => $chatid,
      'text'  => $text,
      'reply_to_message_id' => $msgid
    ];

    if(isset(session('tele')['step_verify']) )
    {
      $data['reply_markup'] = $keyboard;
    }

    $options = [
      'http' => [
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data),
      ],
    ];

    $context  = stream_context_create($options);
    $result = file_get_contents($this->request_url('sendMessage'), false, $context);
    print_r($result);
  }

  public function create_response($text, $message)
  {
    $hasil = '';

    $fromid = $message["from"]["id"]; // variable penampung id user
    $chatid = $message["chat"]["id"]; // variable penampung id chat
    $pesanid= $message['message_id']; // variable penampung id message

    // variable penampung username nya user
    if(isset($message["from"]["username"]) )
    {
      $chatuser = $message["from"]["username"];
    }
    else
    {
      $chatuser = '';
    }

    // variable penampung nama user
    if(isset($message["from"]["last_name"]) )
    {
      $namakedua = $message["from"]["last_name"];
    }
    else
    {
      $namakedua = '';
    }

    $namauser = $message["from"]["first_name"]. ' ' .$namakedua;

    $textur = preg_replace('/\s\s+/', ' ', $text);
    $command = explode(' ',$textur,2); //
    switch ($command[0])
    {
      case '/start':
        if(count($command) == 2)
        {
          $hasil = "Ok! Sekarang Masukkan Password Tomman!";
          Session::put('tele.chat_id', $chatid);
          session::put('step_login.step','pass');
          Session::put('step_login.nik', $command[1]);
        }
      break;

      default:
        if(isset(session('step_login')['step']) )
        {
          switch(session('step_login')['step']){
            case 'pass':
            Session::put('step_login.pass', $text);
            $check = HomeModel::check_tele_user(session('step_login')['nik'],  md5(session('step_login')['pass']) );

            if(!$check)
            {
              $get_data = HomeModel::get_session_id_nik(session('step_login')['nik']);
              if(empty($get_data->chat_id) )
              {
                Session::forget('step_login');
                $hasil .= "Pastikan Data Dibawah Ini Benar!\n";
                $hasil .= "Nama:          $get_data->nama \n";
                $hasil .= "Nik:           $get_data->id_nik \n";
                $hasil .= "User Telegram: $chatuser \n";
                $hasil .= "Nama User:     $namauser \n";
                Session::put('tele.chat_id', $chatid);
                Session::put('tele.nik', $get_data->id_nik);
                Session::put('tele.nama_tele', $chatuser);
                session::put('tele.step_verify','check');
              }
              else
              {
                $hasil = 'Anda Sudah Menambahkan Data Diri!';
              }
              Session::forget('step_login');
            }
            else
            {
              $hasil = 'Username Tidak Terdaftar dengan nik '. session('step_login')['user'] .' dan password '.session('step_login')['pass'];
              Session::forget('step_login');
            }
            break;
          }
        }
        elseif(isset(session('tele')['step_verify']) )
        {
          switch($command[0])
          {
            case 'yes':
              // HomeModel::save_id_chat(session('tele')['nik'], session('tele')['chat_id'], session('tele')['nama_tele']);
              $hasil = "Terima Kasih Telah Mendaftar!";
              Session::forget('tele');
              break;
            case 'no':
              $hasil = "Data tidak disimpan, silahkan cek kembali data sebelumnya !";
              Session::forget('tele');
            break;
          }
        }
        else
        {
          $hasil = 'Pesan tidak dikenal';
        }
      break;
    }

    return $hasil;
  }

  public function process_message($message)
  {
    // dd($message);
    $updateid = $message["update_id"];
    if(array_key_exists('callback_query', $message) )
    {
      $message_data = $message['callback_query']["message"];
      $chatid = $message_data["chat"]["id"];
      $message_id = $message_data["message_id"];

      if($message['callback_query']['data'])
      {
        $message_data["text"] = $message['callback_query']['data'];
        $text = $message['callback_query']['data'];
      }
      else
      {
        $message_data["text"] = 'foo_bar';
        $text = 'foo_bar';
      }
    }
    else
    {
      $message_data = $message["message"] ?? 0;
      $chatid = $message_data["chat"]["id"];
      $message_id = $message_data["message_id"];
      $text = $message_data["text"];
    }

    if (isset($text) )
    {
      $response = $this->create_response($text, $message_data);
      if (!empty($response) ) $this->send_reply($chatid, $message_id, $response);
    }

    return $updateid;
  }

  public function process_one($jenis)
  {
    if($jenis == 'webhook')
    {
      $this->process_message(json_decode(file_get_contents('php://input'), true) );
    }
    else
    {
      // fungsi untuk meminta pesan
      $update_id  = 0;
      echo "-";

      if (file_exists("last_update_id") ) $update_id = (int)file_get_contents("last_update_id");

      $updates = $this->get_updates($update_id);

      if ( (!empty($updates) ) )
      {
        echo "\r\n===== isi diterima \r\n";
        print_r($updates);
      }

      foreach ($updates as $message)
      {
        echo '+';
        $update_id = $this->process_message($message);
      }
      file_put_contents("last_update_id", $update_id + 1);
    }
  }

  public function running_me_baby($jenis = 'loop')
  {
    //https://api.telegram.org/bot1866922472:AAHcESBKs4qoKiy6GD6_IrLPYEDUv1gx-i4/setWebhook?url=https://perwira.tomman.app/enemy_spotted
    if($jenis != 'webhook')
    {
      while (true)
      {
        $this->process_one($jenis);
        sleep(1);
      }
    }
    else
    {
      $this->process_one($jenis);
    }
  }

  public function updateEmployee()
  {
    return HomeModel::update_Employy();
  }

  public static function updateAPILocate()
  {
    $get_provinsi = json_decode(ApiModel::get_provinsi() );

    foreach($get_provinsi as $provinsi)
    {
      foreach($provinsi as $prov)
      {
        print_r("$prov->nama ");
        HomeModel::update_api_loc('prov', $prov);

        $get_kota = json_decode(ApiModel::get_kota($prov->id) );
        foreach($get_kota as $kota)
        {
          foreach($kota as $kt)
          {
            print_r("$kt->nama ");

            HomeModel::update_api_loc('kota', $prov);

            $get_kecamatan = json_decode(ApiModel::get_kecamatan($kt->id) );
            foreach($get_kecamatan as $kecamatan)
            {
              foreach($kecamatan as $kcmt)
              {
                print_r("$kcmt->nama ");
                HomeModel::update_api_loc('kecamatan', $prov);

                $get_kelurahan = json_decode(ApiModel::get_kelurahan($kcmt->id) );
                foreach($get_kelurahan as $kelurahan)
                {
                  foreach($kelurahan as $klrh)
                  {
                    print("$klrh->nama \n");
                    HomeModel::update_api_loc('kelurahan', $prov);

                  }
                }
              }
            }
          }
        }
      }
    }
    print_r("Selesai");
    Telegram::sendMessage([
        'chat_id' => 401791818,
        'text' => "Selesai updateAPILocate"
    ]);
  }

	public function organisasi()
	{
    $data = DB::table('user_perwira As up')->select('id_nik', 'nama', 'waspang_nik', DB::RAW("(SELECT nama FROM user_perwira WHERE id_nik = up.waspang_nik) As nama_waspang") )->where('waspang_nik', '!=', '')->get();

    $org = [];

    foreach($data as $v)
    {
      $org[$v->id_nik]['parent_id'] = $v->waspang_nik;
      $org[$v->id_nik]['people_id'] = $v->id_nik;
      $org[$v->id_nik]['nama'] = $v->nama;
      $org[$v->id_nik]['nama_waspang'] = $v->nama_waspang;
    }

    $org = array_values($org);

    foreach($org as $v)
    {
      $search_data = array_search($v['parent_id'], array_column($org, 'people_id') );

      if($search_data === FALSE)
      {
        $org[$v['parent_id'] ]['parent_id'] = 0;
        $org[$v['parent_id'] ]['people_id'] = $v['parent_id'];
        $org[$v['parent_id'] ]['nama'] = $v['nama_waspang'];
        $org[$v['parent_id'] ]['nama_waspang'] = 'KOSONG';
      }
    }

    $org = array_values($org);
    // dd($org);
		return view('report.organisasi', compact('org') );
	}

	public function save_create(Request $req)
	{
		HomeModel::save_or_update_org($req);
    return redirect("/hr/list/organisasi");
	}

	public function update_data_search(Request $req)
	{
    HomeModel::save_profile($req);
    return back()->with('alerts', [
			['type' => 'success', 'text' => '<strong>SUKSES</strong> Memperbaharui Data Diri']
		]);
	}

	public function download_all_file($nik)
  {
      $data = HomeModel::view_profile_recruiter($nik);

      $rootPaths = [
          public_path() . '/upload4/perwira/recruitment/' . $nik,
          public_path() . '/upload5/perwira/recruitment/' . $nik
      ];
      
      $copyPath = public_path() . '/upload4/perwira/recruitment/' . $nik . '_copyan_';
      \File::copyDirectory($rootPaths[0], $copyPath);

      // Check and copy files from the second directory if they exist
      if (\File::exists($rootPaths[1])) {
          \File::copyDirectory($rootPaths[1], $copyPath);
      }

      $zipName = str_replace('/', '-', $data->nama . '-' . $data->nama_provinsi. '-' . $data->nama_kota) . '.zip';
      $zipPath = public_path() . '/' . $zipName;

      $zip = new ZipArchive();
      if ($zip->open($zipPath, ZipArchive::CREATE | ZipArchive::OVERWRITE) === true) {
          $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($copyPath), RecursiveIteratorIterator::LEAVES_ONLY);

          foreach ($files as $file) {
              if (!$file->isDir()) {
                  $filePath = $file->getRealPath();
                  $relativePath = substr($filePath, strlen($copyPath) + 1);
                  $zip->addFile($filePath, $relativePath);
              }
          }

          $zip->close();
      } else {
          return response()->json(['error' => 'Could not create zip file'], 500);
      }

      \File::deleteDirectory($copyPath);

      $headers = ['Content-Type' => 'application/zip'];
      $response = response()->download($zipPath, $zipName, $headers);

      register_shutdown_function('unlink', $zipPath);

      return $response;
  }

}
