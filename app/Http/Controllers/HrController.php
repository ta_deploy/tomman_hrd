<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\DA\HrModel;
use App\DA\ReportModel;
use Telegram;
use Excel;

date_default_timezone_set('Asia/Makassar');

class HrController extends Controller {

  public function list_nik()
  {
    $xada = "Ada"; $xrequest = "Request"; $xprocess = "Process";
    $get_ada = HrModel::statusNIK($xada);
    $get_request = HrModel::statusNIK($xrequest);
    $get_process = HrModel::statusNIK($xprocess);

    return view('hr.nik.listNik', compact('get_ada','get_request','get_process') );
  }

  public function submit_nik($id)
  {
    $data = HrModel::search_id_people($id);
    return view('hr.nik.formSubmissNik', compact('data') );
  }

  public function submit_karyawan(Request $req, $id)
  {
    return view('hr.karyawan.formKaryawan');
  }

  public function save_karyawan(Request $req, $id)
  {
    $msg = HrModel::save_karyawan($req, $id);
    return redirect('/hr/list/nik')->with('alerts', $msg);
  }

  public function save_aju_nik(Request $req, $id)
  {
    $msg = HrModel::save_aju_nik($req, $id, 'create');
    return redirect('/hr/list/nik')->with('alerts', $msg);
  }

  public function submit_mutasi()
  {
    return view('hr.mutasi.formMutasi');
  }

  public function save_mutasi(Request $req, $id)
  {
    $msg = HrModel::save_mutasi($req, $id);
    return redirect('/hr/list/mutasi')->with('alerts', $msg);
  }

  public function app_mutasi()
  {
    return view('hr.mutasi.formPersetujuanMutasi');
  }

    public function save_app_mutasi(Request $req, $id)
    {
      $msg = HrModel::save_app_mutasi($req, $id);
      return redirect('/hr/list/mutasi')->with('alerts', $msg);
    }

  public function listcreated_user()
  {
    $get_data = HrModel::get_list_createU();
    return view('hr.created.listCreated', compact('get_data') );
  }

  public function created_user($id)
  {
    $get_data = DB::table('perwira_level_user')->get();

    foreach($get_data as $v)
    {
      $only_tomman[$v->tomman] = $v->tomman;
    }
    return view('hr.created.formCreatedUser', compact('get_data', 'only_tomman') );
  }

  public function createdSave_user(Request $req, $id)
  {
    HrModel::createdSave_user($req);

    return redirect('/hr/list/created_user')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>BERHASIL</strong> Menyimpan Hak Akses User']
    ]);
  }

  public function get_user(Request $req)
  {
    $get_user = HrModel::get_user_ajax($req);
    $data = [];
    foreach ($get_user as $row) {
      $data[] = array("id" => $row->id, "text" => $row->text.' ('.$row->id_nik.')');
    }
    return \Response::json($data);
  }

  public function get_user_position(Request $req)
  {
    $get_user = HrModel::get_user_ajax($req);
    $data = [];
    foreach ($get_user as $row) {
      $data[] = array("id" => $row->id_nik, "text" => $row->text.' ('.$row->id_nik.')');
    }
    return \Response::json($data);
  }

  public function get_user_mutasi(Request $req)
  {
    $search = trim($req->data, '');
    $get_user = HrModel::get_detail_user($search);
    return \Response::json($get_user[0]);
  }

  public function list_level()
  {
    $data = HrModel::list_level();
    return view('hr.level.listLevel', compact('data') );
  }

  public function created_karyawn($id)
  {
    return view('hr.karyawan.formKaryawan');
  }

  public function list_mitra()
  {
    $get_mitra = HrModel::get_listM();
    return view('hr.mitra.listMitra', compact('get_mitra') );
  }

  public function add_mitra()
  {
    return view('hr.mitra.formMitra');
  }

  public function save_mitra(Request $req, $id)
  {
    $msg = HrModel::save_mitra($req);
    return redirect('/hr/list/mitra')->with('alerts', $msg);
  }

  public function list_witel()
  {
    $get_witel = HrModel::get_Witel();
    return view('hr.witel.listWitel', compact('get_witel') );
  }

  public function save_witel(Request $req)
  {
    $msg = HrModel::save_witel($req);
    return redirect('/hr/list/sto')->with('alerts', $msg);
  }

  public function list_sto()
  {
    $get_witel = HrModel::get_Witel();
    $get_sto = HrModel::get_alamat_d();
    return view('hr.witel.formSto', compact('get_witel','get_sto') );
  }

  public function save_sto(Request $req)
  {
    $msg = HrModel::save_sto($req);
    return redirect('/hr/list/sto')->with('alerts', $msg);
  }

  public function list_position()
  {
    $data = HrModel::show_data_position();
    return view('hr.jobPosition.listSummaryJobPosition', compact('data') );
  }

  public function created_position($id)
  {
    $get_witel = HrModel::get_Witel_13();
    $posisi_job_fungsi = HrModel::list_job_posisi('fungsi_grup');
    $posisi_job_sub = HrModel::list_job_posisi('sub_grup');
    $posisi_job_level = HrModel::list_job_posisi('level');
    $posisi_job_title = HrModel::list_job_posisi('posisi_title');
    // dd($data);
    return view('hr.jobPosition.formJobPosition', compact('get_witel', 'posisi_job_fungsi', 'posisi_job_sub', 'posisi_job_level', 'posisi_job_title') );
  }

  public function save_position(Request $req, $id)
  {
    $msg = HrModel::save_position($req);
    return redirect('/hr/list/position')->with('alerts', $msg);
  }

  public function list_direktorat()
  {
    $data = HrModel::list_direktorat();
    // dd($data);
    return view('hr.direktorat.listSumaryDirektorat', compact('data') );
  }

  public function save_direktorat(Request $req)
  {
    $msg = HrModel::save_direktorat($req);
    return redirect('/hr/list/direktorat')->with('alerts', $msg);
  }

  public function validate_direktorat(Request $req)
  {
    $data = HrModel::find_direktorat_nm($req->data);
    return \Response::json($data);
  }

  public function get_psa(Request $req)
  {
    $data = HrModel::find_psa($req->data);
    return \Response::json($data);
  }

  public function get_sto_mut(Request $req)
  {
    // dd($req->all() );
    $search = trim($req->searchTerm, '');
    $check_data = HrModel::find_direktorat_sto_base_witel($search, $req->witel);
    $data = [];
    foreach ($check_data as $row) {
      $data[] = array("id" => $row->id_area, "text" => $row->kode_area . ' ('. $row->area_alamat .')');
    }
    return \Response::json($data);
  }

  public function get_jabatan(Request $req)
  {
    // dd($req->all() );
    $search = trim($req->searchTerm, '');
    $check_data = HrModel::find_direktorat_jabatan($search, $req->witel);
    $data = [];
    foreach ($check_data as $row) {
      $data[] = array("id" => $row->id, "text" => $row->level . ' ('. $row->pos_name .')');
    }
    return \Response::json($data);
  }

  public function list_mutasi()
  {
    return view('hr.mutasi.listMutasi');
  }

  public function list_ttd()
  {
    $data = HrModel::list_ttd();
    return view('hr.ttd.listTtd', compact('data') );
  }

  public function save_ttd(Request $req, $id)
  {
    $msg = HrModel::save_ttd($req, $id);
    return redirect('/hr/list/ttd')->with('alerts', $msg);
  }

  public function update_ttd(Request $req, $id)
  {
    $msg = HrModel::update_ttd($req, $id);
    return redirect('/hr/list/ttd')->with('alerts', $msg);
  }

  public function created_ttd($id)
  {
    $data = HrModel::data_ttd($id);
    return view('hr.ttd.formTtd', compact('data') );
  }

  public function edit_ttd($id)
  {
    $data = HrModel::data_ttd($id);
    return view('hr.ttd.formTtd', compact('data') );
  }

  public function list_archivement()
  {
    $data = HrModel::list_archive();
    return view('hr.reward.listReward', compact('data') );
  }

  public function save_archivement(Request $req)
  {
    $msg = HrModel::save_archivement($req);
    return redirect('/hr/list/archivement')->with('alerts', $msg);
  }

  public function created_archivement()
  {
    return view('hr.reward.formReward');
  }

  public function calendar_cuti()
  {
    $data = HrModel::get_cuti_all(date('Y') );
    return view('hr.cuti.kalenderCuti', ['data' => $data]);
  }

  public function input_cuti()
  {
    return view('hr.cuti.formCuti');
  }

  public function get_jml_cuti(Request $req)
  {
    $data = HrModel::get_cuti_people($req->data);
    $jml = 0;

    if($data)
    {
      foreach($data as $d){
        $jml += $d->jml_cuti;
      }
    }

    $sisa = 12 - $jml;
    return $sisa;
  }

  public function save_cuti(Request $req)
  {
    $msg = HrModel::save_cuti($req);
    return redirect('/hr/calendar/cuti')->with('alerts', $msg);
  }

  public function get_direktorat(Request $req)
  {
    $search = trim($req->searchTerm, '');
    $check_data = HrModel::find_direktorat_nm_ajax($search);
    $data = [];
    foreach ($check_data as $row) {
      $data[] = array("id" => $row->id, "text" => $row->direktorat);
    }
    return \Response::json($data);
  }

  public function get_alker(Request $req)
  {
    $search = trim($req->searchTerm, '');
    $check_data = HrModel::find_alker($search);
    $data = [];
    foreach ($check_data as $row) {
      $data[] = array("id" => $row->id, "text" => $row->alker);
    }
    return \Response::json($data);
  }

  public function get_detail_jobs(Request $req)
  {
    $data = HrModel::get_detail_jobs($req->id_dir, $req->pos_name);
    return \Response::json($data);
  }

  public function get_posisi(Request $req)
  {
    $data = HrModel::get_posisi($req->data);
    return \Response::json($data);
  }

  public function get_witel(Request $req)
  {
    $search = trim($req->searchTerm, '');
    $check_data = HrModel::find_witel($search);
    $data = [];
    foreach ($check_data as $row) {
      $data[] = array("id" => $row->id_witel, "text" => $row->nama_witel);
    }
    return \Response::json($data);
  }

  public function get_mitra(Request $req)
  {
    $search = trim($req->searchTerm, '');
    $check_data = HrModel::find_mitra($search);
    $data = [];
    foreach ($check_data as $row) {
      $data[] = array("id" => $row->mitra_amija, "text" => $row->mitra_amija_pt);
    }
    return \Response::json($data);
  }
  public function get_user_rekrut()
  {
    $check_data = HrModel::find_user_rekrut();
    $data = [];
    foreach ($check_data as $row) {
      $data[] = array("id" => $row->noktp, "text" => $row->nama.' ('. $row->noktp .')');
    }
    return \Response::json($data);
  }

  public function get_table_beranda(Request $req)
  {
    $jenis = trim($req->data, '');
    $tahun = trim($req->tahun, '');
    $bulan = trim($req->bulan, '');
    $jk = $req->jk;
    $umur = $req->umur;
    $provinsi = $req->provinsi;
    $kota = $req->kota;
    $kecamatan = $req->kecamatan;
    $kelurahan = $req->kelurahan;
    $data = HrModel::get_table_rec($jenis, $tahun, $bulan, $jk, $umur, $provinsi, $kota, $kecamatan, $kelurahan);
    return \Response::json($data);
  }

	public function download_file($id, $people_id)
	{
		$check_path = public_path() . '/upload4/perwira/reward/' . $id . '/' . $people_id;
    $files = preg_grep('~^reward.*$~', scandir($check_path) );

    if(count($files) != 0)
    {
      $files = array_values($files);
      $file = $check_path.'/'.$files[0];
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename='.basename($file) );
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($file) );
      ob_clean();
      flush();
      readfile($file);
      exit;
    }
	}

  public function detail_cuti($id)
  {
    $data = HrModel::get_detail_cuti($id);
    return $data;
  }

  public function monolog_karyawan(Request $req)
  {
    // dd(Input::get('witel') );
    $input_witel = Input::get('witel');

    $get_witel = DB::SELECT('SELECT WITEL_HR as id, WITEL_HR as text FROM monolog_teknisi_tr6 GROUP BY WITEL_HR');

    $get_tidak_terdaftar = HrModel::monolog_karyawan('tidak_terdaftar',$input_witel);

    $get_karyawan_aktif = HrModel::monolog_karyawan('karyawan_aktif',$input_witel);

    $get_karyawan_nonaktif = HrModel::monolog_karyawan('karyawan_nonaktif',$input_witel);

    $get_mytech_scmt_active = HrModel::monolog_karyawan('mytech_scmt_active',$input_witel);

    $get_mytech_scmt_blanks = HrModel::monolog_karyawan('mytech_scmt_blanks',$input_witel);

    $get_scmt_mytech_blanks = HrModel::monolog_karyawan('scmt_mytech_blanks',$input_witel);

    $get_scmt_mytech_active = HrModel::monolog_karyawan('scmt_mytech_active',$input_witel);

    return view('hr.monolog.karyawan', compact('input_witel','get_witel','get_tidak_terdaftar','get_karyawan_aktif','get_karyawan_nonaktif','get_mytech_scmt_active','get_mytech_scmt_blanks','get_scmt_mytech_blanks','get_scmt_mytech_active') );
  }

  public function uploadData()
  {
    return view('hr.uploadData');
  }

  public function uploadDataSave(Request $req)
  {
    return HrModel::uploadDataSave($req);
  }

	public function list_pelamar_country($id)
	{
		$data = HrModel::get_pelamar_kota($id);
    return view('report.listPelamar_kota', compact('data') );
	}

	public function get_recruit_comp(Request $req)
	{
		$data = HrModel::get_recruit_comp($req->mitra);
    return \Response::json($data);
	}

	public function get_accepted_home(Request $req)
	{
		$data = HrModel::get_accepted_h($req->mitra);
    return \Response::json($data);
	}

	public function teknisi_list(Request $req)
	{
    $input_witel = $req->witel;
    $get_witel = DB::SELECT('SELECT WITEL_HR as id, WITEL_HR as text FROM monolog_teknisi_tr6 GROUP BY WITEL_HR');

    $get_tidak_terdaftar  = HrModel::list_reg_karyawan('tidak_terdaftar', $input_witel);
    $get_aktif_reg        = HrModel::list_reg_karyawan('aktif_reg', $input_witel);
    $get_non_aktif_reg    = HrModel::list_reg_karyawan('non_aktif_reg', $input_witel);
    $get_aktif_tomman     = HrModel::list_reg_karyawan('aktif_tomman', $input_witel);
    $get_non_aktif_tomman = HrModel::list_reg_karyawan('non_aktif_tomman', $input_witel);

    return view('hr.listTeknisi.karyawan', compact('input_witel', 'get_witel', 'get_tidak_terdaftar', 'get_aktif_reg', 'get_non_aktif_reg', 'get_has_tomman', 'get_aktif_tomman', 'get_non_aktif_tomman') );

	}

	public function teknisiTomman_list(Request $req)
	{
    $input_witel = $req->witel;
    $get_witel = DB::SELECT('SELECT WITEL_HR as id, WITEL_HR as text FROM monolog_teknisi_tr6 GROUP BY WITEL_HR');

    $get_aktif_tomman     = HrModel::list_tomman_teknisi('aktif_tomman', $input_witel);
    $get_non_aktif_reg    = HrModel::list_tomman_teknisi('non_aktif_tomman', $input_witel);
		$get_no_profil        = HrModel::list_tomman_teknisi('tidak_terdaftar', $input_witel);

    return view('hr.listTeknisi.list_tomman', compact('input_witel', 'get_witel', 'get_aktif_tomman', 'get_non_aktif_reg', 'get_no_profil') );

	}

  public function kehadiran_peserta_dow()
  {
    $data = DB::table('absen_kehadiran_dow')->whereDate('tgl_hadir', date('Y-m-d') )->get();

    return view('hr.kehadiran_peserta_dow', compact('data') );
  }

  public function absen_dow_save(Request $req)
  {
    $nik = $req->input('nik');
    $nama = $req->input('nama');

    $data = DB::table('absen_kehadiran_dow')->where('nik', $nik)->whereDate('tgl_hadir', date('Y-m-d') )->first();
    if ($data != null)
    {
      return back()->with('alerts', [
        ['type' => 'danger', 'text' => 'Rekan sudah terbaca Absen sebelumnya pada '.$data->dtm_approve]
      ]);
    }

    if ($req->hasFile('img_dow') == true)
    {
      $name = date('Ymd');
      $path = public_path().'/upload4/absen_dow/'.$nik.'/';

      if (!file_exists($path) )
      {
        if (!mkdir($path, 0777, true) )
        {
          return 'gagal menyiapkan folder foto absensi';
        }
      }

      $file = $req->file('img_dow');
      $ext = 'jpg';
      $filemime = @exif_read_data($file);

      if ($filemime == false)
      {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => 'Foto yang Dilaporkan Tidak Menggunakan TimeStamp!']
        ]);
      }

      if (@array_key_exists("Software", $filemime) )
      {
        if (!in_array(@$filemime['Software'] ,["Timestamp Camera","Timestamp Camera ENT","Timestamp Camera Enterprise"]) )
        {
          return back()->with('alerts', [
            ['type' => 'danger', 'text' => 'Foto yang Dilaporkan Tidak Menggunakan TimeStamp!']
          ]);
        } else {
          try {
            $moved = $file->move("$path", "$name.$ext");

            $img = new \Imagick($moved->getRealPath() );

            $img->scaleImage(100, 150, true);
            $img->writeImage("$path/$name-th.$ext");
          }
          catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto absensi '.$name;
          }
        }

        DB::table('absen_kehadiran_dow')->insert([
          'nik' => $nik,
          'nama' => $nama,
          'gps_lat' => $req->input('gps_lat'),
          'gps_long' => $req->input('gps_long'),
          'tgl_hadir' => date('Y-m-d'),
          'dtm_hadir' => date('Y-m-d H:i:s')
        ]);

        $name = date('Ymd');
        $path = public_path().'/upload4/absen_dow/'.$nik;
        $th = "$path/$name-th.jpg";

        if (file_exists(public_path().$th) )
        {
          $path = "$path/$name";
        }

        if($path)
        {
          $img    = "$path/$name.jpg";
          $th     = "$path/$name-th.jpg";
        } else {
          $img    = null;
          $th     = null;
        }

        if (date('H:i') < "08:00")
        {
          $jam = "HADIR";
        } else {
          $jam = "TELAT";
        }

        $msg_location = "http://maps.google.com/?q=".$req->input('gps_lat').",".$req->input('gps_long');

        $caption = $nama." / ".$nik."\n\n".$msg_location."\n\n<i>".date('Y-m-d H:i:s')." WITA (".$jam.")</i>";

        Telegram::sendPhoto([
          'chat_id' => '-1001854224134',
          'parse_mode' => 'html',
          'caption' => $caption,
          'photo' => $img
        ]);

        return back()->with('alerts', [
          ['type' => 'success', 'text' => 'SUKSES Menyimpan Laporan']
        ]);
      } else {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => 'Foto yang Dilaporkan Tidak Menggunakan TimeStamp!']
        ]);
      }

    } else {
      return back()->with('alerts', [
        ['type' => 'danger', 'text' => 'Foto Absen Belum Dilaporkan!']
      ]);
    }
  }

  public function event_list()
  {
    $data = HrModel::get_event();
    return view('hr.absensi.listevent',compact('data') );
  }
  public function event_form($id)
  {
    $data = HrModel::get_event_byid($id);
    return view('hr.absensi.formevent',compact('data') );
  }
  public function event_save(Request $req, $id)
  {
    $exists = HrModel::get_event_byid($id);
    $param = ["nama_event"=>$req->nama_event,"tgl"=>$req->tgl,"user"=>session('auth')->id_user];
    if($exists){
      //todo update
      HrModel::event_update($id,$param);
    }else{
      //todo insert
      HrModel::event_insert($param);
    }
    return redirect("/event_list");
  }
  public function event_delete($id)
  {
    HrModel::event_delete($id);
    return redirect("/event_list");
  }

  public function register_rfid_form($id,$event_id)
  {
    return view('hr.absensi.register_rfid');
  }
  public function register_rfid(Request $req)
  {
    HrModel::register_rfid(["rfid"=>$req->rfid,"nik"=>$req->nik,"card_type"=>$req->card_type]);

    return redirect('/scanrfid/'.$req->event_id);
  }


  public function scanrfid($id)
  {
    $html = HrModel::get_attended($id);
    // dd($html);
    $data = HrModel::get_event_byid($id);
    return view('hr.absensi.rfid', compact('data','html') );
  }
  public function scanned_rfid(Request $req)
  {
    $rfid = HrModel::get_rfid($req->rfid);
    if($rfid){
      //to do cek absensi
      $cekabsen = HrModel::get_absensi_byrfid($req->rfid, $req->event_id);
      if($cekabsen){
        return ["code"=>2,"msg"=>"sudah absen","html"=>null];
      }else{
        //to do absensi
        HrModel::set_attended(["event_id"=>$req->event_id,"rfid"=>$req->rfid,"nik"=>$rfid->nik,"gate"=>session('auth')->nama]);
        $html = HrModel::get_attended($req->event_id);
        return ["code"=>3,"msg"=>"attended","html"=>$html];
      }
    }else{
      return ["code"=>1,"msg"=>"kartu tidak dikenali","html"=>null];
    }
  }

	public function event_dp()
	{
		// return view('home.doorprize_list');
		return view('home.doorprize');
	}

	public function event_download($id)
	{
    $download_event = ReportModel::download_event($id);
    $data = HrModel::get_event_byid($id);
		$judul_pekerjaan = $data->nama_event;
    // dd($download_event);
		return Excel::download(new ExcelExport([$download_event], [], 'list_event') , "List Peserta $judul_pekerjaan.xlsx");
	}

}
