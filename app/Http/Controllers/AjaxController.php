<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Session;
use App\DA\AjaxModel;

class AjaxController extends Controller
{
	public function get_alker_teknisi(Request $req)
	{
    $data = AjaxModel::get_alker_teknisi($req);
    return response()->json($data);
	}

	public function jenis_select(Request $req, $jenis_select)
	{
    $search = trim($req->searchTerm);
    $data = AjaxModel::get_jenis_select($search, $jenis_select);
    return response()->json($data);
	}
}