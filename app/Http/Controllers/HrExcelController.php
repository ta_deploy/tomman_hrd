<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DA\Mutasi;
use App\DA\Karyawan;
use App\DA\Witel;
use DB;
use Excel;


class HrExcelController extends Controller {

  public function cetakKaryawan(Request $req){
    $kolom="";
    for ($x=0;$x<count($req->cek);$x++){
      $kolom.=$req->cek[$x];
      if ($x!=count($req->cek)-1){$kolom.=",";}
    }
    $psa=Witel::getLokerById($req->psa);
    $datakar= Karyawan::getKaryawanCetak($req->psa,$kolom);
    $data = json_decode(json_encode($datakar), True);

    Excel::create('Data Karyawan', function($excel) use($data,$req,$psa) {
    //sheet1
    $excel->sheet('Karyawan', function($sheet)  use($data,$req,$psa){
        $sheet->setStyle(array(
            'font' => array('name' =>  'Times New Roman')
        ) );
        $sheet->setAutoSize(true);
        $sheet->mergeCells('A1:D1');
        $sheet->cell('A1', function($cells) use($data,$psa){
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontWeight('bold');
            $cells->setValue('DATA KARYAWAN PSA '.$psa->nama_loker);
        });

        $head=array();
        for ($x = 0; $x < count($req->cek); $x++){
           $isi= explode(".", $req->cek[$x]);
           array_push($head, $isi[1]);
        }

        $sheet->row(3, $head);

        $y=4;
        for ($x = 0; $x < count($data); $x++){
            $sheet->row($y, $data[$x]);
            $y+=1;
        }

        $kolom="A";
        for ($x = 0; $x < count($req->cek)-1; $x++){
            $kolom++;
        }

        $sheet->setBorder('A3:'.$kolom.''.$y, 'thin');

        });
    })->export('xls');
    return back();
  }

  public function cetakMutasi($id){
    $data=Mutasi::getCetakMutasi($id);

     Excel::create('BA Pelurusan Job Position', function($excel) use($data) {

        function Terbilang($x)
        {
          $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
          if ($x < 12)
            return " " . $abil[$x];
          elseif ($x < 20)
            return Terbilang($x - 10) . " Belas";
          elseif ($x < 100)
            return Terbilang($x / 10) . " Puluh" . Terbilang($x % 10);
          elseif ($x < 200)
            return " seratus" . Terbilang($x - 100);
          elseif ($x < 1000)
            return Terbilang($x / 100) . " Ratus" . Terbilang($x % 100);
          elseif ($x < 2000)
            return " seribu" . Terbilang($x - 1000);
          elseif ($x < 1000000)
            return Terbilang($x / 1000) . " Ribu" . Terbilang($x % 1000);
          elseif ($x < 1000000000)
            return Terbilang($x / 1000000) . " Juta" . Terbilang($x % 1000000);
        }
        function TanggalIndo($date){
          $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

          $tahun = substr($date, 0, 4);
          $bulan = substr($date, 5, 2);
          $tgl   = substr($date, 8, 2);

          $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
          return($result);
        }
        function BulanIndo($date){
          $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
          $bulan = substr($date, 5, 2);
          $result = $BulanIndo[(int)$bulan-1];
          return($result);
        }
        function namahari($tanggal){

        $tgl=substr($tanggal,8,2);
        $bln=substr($tanggal,5,2);
        $thn=substr($tanggal,0,4);

        $info=date('w', mktime(0,0,0,$bln,$tgl,$thn) );

            switch($info){
                case '0': return "Minggu"; break;
                case '1': return "Senin"; break;
                case '2': return "Selasa"; break;
                case '3': return "Rabu"; break;
                case '4': return "Kamis"; break;
                case '5': return "Jumat"; break;
                case '6': return "Sabtu"; break;
            };

        }
        //sheet1
        $excel->sheet('BA', function($sheet)  use($data){

            $sheet->setStyle(array(
                'font' => array(
                    'name'      =>  'Times New Roman'
                )
            ) );

            $sheet->setWidth(array(
                'A'     =>  5,'B'     =>  15,'C'     =>  30,'D'     =>  30,
                'E'     =>  15,'F'     =>  20,'G'     =>  30,'H'     =>  15, 'I' =>20
            ) );
            $sheet->mergeCells('A1:I1');
                $sheet->cell('A1', function($cells) use($data){
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setValue('BERITA ACARA '.$data->jenis);
                });
            $tanggal=explode('-',$data->tgl_pengajuan);
            $sheet->mergeCells('A2:I2');
                $sheet->cell('A2', function($cells) use($tanggal) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setValue('PK.                   /PS000/TA.0104/'.$tanggal[1].'-'.$tanggal[0]);
                });

            $sheet->mergeCells('A4:I4');
                $sheet->cell('A4', function($cells) use($data,$tanggal){
                  $tanggalint=(int)$tanggal[2];
                  $tahunint=(int)$tanggal[0];
                    $cells->setValue('Pada hari ini '.namahari($data->tgl_pengajuan).' tanggal '.ucwords(Terbilang($tanggalint) ).
                                      ' bulan '.BulanIndo($data->tgl_pengajuan).' tahun '
                                      .ucwords(Terbilang($tahunint) ).
                                      ' telah dilakukan kesepakatan '.$data->jenis.' tenaga kerja sbb :');
                });

            $sheet->setBorder('A6:I8', 'thin');
            $sheet->setMergeColumn(array(
                'columns' => array('A','B','C'),
                'rows' => array(
                    array(6,7), )
            ) );
            $sheet->row(6, array('NO', 'NIK','NAMA' ) );
            $sheet->mergeCells('D6:F6');
                $sheet->cell('D6', function($cells) {
                    $cells->setValue('Posisi Lama');
                });
            $sheet->mergeCells('G6:I6');
                $sheet->cell('G6', function($cells) {
                    $cells->setValue('Posisi Baru');
                });

            $sheet->cell('D7', function($cells){
                    $cells->setValue('Jabatan');
                });
            $sheet->cell('E7', function($cells){
                    $cells->setValue('Status');
                });
            $sheet->cell('F7', function($cells){
                    $cells->setValue('PSA');
                });
            $sheet->cell('G7', function($cells){
                    $cells->setValue('Jabatan');
                });
            $sheet->cell('H7', function($cells){
                    $cells->setValue('Status');
                });
            $sheet->cell('I7', function($cells){
                    $cells->setValue('PSA');
                });
            //isi
            $sheet->cell('A8', function($cells) use($data){
                    $cells->setValue('1');
                });
            $sheet->cell('B8', function($cells) use($data){
                    $cells->setValue($data->nik);
                });
            $sheet->cell('C8', function($cells) use($data){
                    $cells->setValue($data->nama);
                });
            $sheet->getStyle('C8')->getAlignment()->setWrapText(true);
            $sheet->cell('D8', function($cells) use($data){
                    $cells->setValue($data->position_name_lama);
                });
            $sheet->getStyle('D8')->getAlignment()->setWrapText(true);
            $sheet->cell('E8', function($cells) use($data){
                    $cells->setValue('PKS');
                });
            $sheet->cell('F8', function($cells) use($data){
                    $cells->setValue($data->loker_lama);
                });
            $sheet->cell('G8', function($cells) use($data){
                    $cells->setValue($data->position_name_baru);
                });
            $sheet->getStyle('G8')->getAlignment()->setWrapText(true);
            $sheet->cell('H8', function($cells) use($data){
                    $cells->setValue('PKS');
                });
            $sheet->cell('I8', function($cells) use($data){
                    $cells->setValue($data->loker_baru);
                });

            //ttd
            $sheet->mergeCells('B10:C10');
                $sheet->cell('B10', function($cells){
                    $cells->setValue('Dibuat Oleh,');
                });
            $sheet->mergeCells('E10:F10');
                $sheet->cell('E10', function($cells){
                    $cells->setValue('Disetujui Oleh,');
                });
            $sheet->mergeCells('H10:I10');
                $sheet->cell('B10', function($cells){
                    $cells->setValue('Diketahui Oleh');
                });
            $sheet->mergeCells('A11:C11');
                $sheet->cell('A11', function($cells)  use($data){
                    $cells->setValue($data->jabatan_dibuat);
                });
            $sheet->mergeCells('E11:F11') ;
                $sheet->cell('E11', function($cells) use($data){
                    $cells->setValue($data->jabatan_disetujui);
                });
            $sheet->mergeCells('H11:I11') ;
                $sheet->cell('H11', function($cells) use($data){
                    $cells->setValue($data->jabatan_diketahui);
                });
            $sheet->mergeCells('B15:C15') ;
                $sheet->cell('B15', function($cells) use($data){
                    $cells->setValue($data->nama_dibuat);
                });
            $sheet->mergeCells('E15:F15') ;
                $sheet->cell('E15', function($cells) use($data){
                    $cells->setValue($data->nama_disetujui);
                });
            $sheet->mergeCells('H15:I15') ;
                $sheet->cell('H15', function($cells) use($data){
                    $cells->setValue($data->nama_diketahui);
                });
            $sheet->mergeCells('B16:C16') ;
                $sheet->cell('B16', function($cells) use($data){
                    $cells->setValue($data->nik_dibuat);
                });
            $sheet->mergeCells('E16:F16') ;
                $sheet->cell('E16', function($cells) use($data){
                    $cells->setValue($data->nik_disetujui);
                });
            $sheet->mergeCells('H16:I16') ;
                $sheet->cell('H16', function($cells) use($data){
                    $cells->setValue($data->nik_diketahui);
                });

            $sheet->cells('A6:I16', function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                    });
        });
    })->export('xls');
  return back();
 }

}
