<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\DA\MitraModel;
use DB;

class ApiController extends Controller
{
  //aset
  public function getAset(Request $req)
  {
    header('Content-type: application/json');
    echo json_encode(DB::table('asset_wtmta')->select("serial_number","kategori_asset")->where("nik_pemakai", $req->id_user)->get() );
    // return view('mitra.teknisi.listTeknisi');
  }
  public function saveLaporanAset(Request $req){
    // asset_laporan_teknisi
    DB::table('asset_laporan_teknisi')->insert([
        "sn"          => $req->sn,
        "status"      => $req->status,
        "keluhan"     => $req->keluhan,
        "tgl_laporan" => date('Y-m-d H:i:s'),
        "user_nik"    => $req->user_nik,
        "user_nama"   =>$req->user_nama,
        "odo_meter"   => $req->odo_meter,
        "jumlah_arc"  =>$req->jumlah_arc

    ]);
  }
  public function getLaporanAset(Request $req){
    // asset_laporan_teknisi
    header('Content-type: application/json');
    echo json_encode(DB::table('asset_laporan_teknisi')->whee('sn', $req->sn)->get() );
  }

  //HRD
  public function getProfil(Request $req)
  {
    header('Content-type: application/json');
    echo json_encode(DB::table('user_perwira')->where('id_user', $req->id_user)->first() );
    // return view('mitra.teknisi.listTeknisi');
  }

  //FA
  public function getPelatihanSelesai(Request $req)
  {
    header('Content-type: application/json');
    echo json_encode(DB::table('asset_wtmta')->select("serial_number as userId","no_po as id","serial_number as title","no_po as completed")->limit(10)->get() );
    // return view('mitra.teknisi.listTeknisi');
  }
}
