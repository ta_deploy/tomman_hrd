<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\DA\FAModel;
use App\DA\HomeModel;
use Excel;
use App\Http\Controllers\Controller;
class FAController extends Controller {

  public function list_breving()
  {
    $data_rec = HomeModel::brevert_dan_akun("BELUM_BREVET");
    $all_event = FAModel::get_list('avail');
    $event_rw = [];

    foreach($all_event as $val)
    {
      $event_rw[$val->id] = $val;
    }
    // dd($event_rw, $all_event);
    foreach($all_event as $val)
    {
      foreach($event_rw as $key => $val_c1)
      {
        if($val_c1->id == $val->id)
        {
          $event_rw[$key]->data_nama[] = $val->nama;
        }
      }
    }

    $current_event = FAModel::get_list('berlangsung');
    $done_event = FAModel::get_list('done');
    // dd($current_event);
    return view('fa.listBrevertTrain', compact('data_rec', 'current_event', 'done_event'), ['all_event' => $event_rw]);
  }

  public function input_breving()
  {
    return view('fa.formBrevertTrain');
  }

  public function submit_breving(Request $req)
  {
    $msg = FAModel::save_breving($req);
    return redirect('/FA/list/breving')->with('alerts', $msg);
  }

  public function detail_breving($id)
  {
    $data = FAModel::detail_brevert($id);
    $brevert = FAModel::get_brevert_title($id);
    // dd($data);
    return view('fa.detail_brevert', compact('data', 'brevert') );
  }

  public function check_absen_breving($id){
    $msg = FAModel::check_briefing($id, session('auth')->noktp);
    return $msg;
  }
}
