<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DA\ReportModel;
use Illuminate\Support\Facades\DB;
use Excel;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Session;
use Telegram;
class ReportController extends Controller {

  public function resling()
  {
    $resign_ter = ReportModel::get_resling(date('Y'), 'Resign', "Terhormat");
    $resign_black = ReportModel::get_resling(date('Y'), 'Resign', "Blacklist");
    $kons = ReportModel::get_resling(date('Y'), 'Konseling');

    $chart = ReportModel::get_resling_chart(date('Y') );

    $data_chart = [];

    foreach($chart as $val)
    {
      $month[$val->bulan] = $val->bulan;
    }

    foreach($chart as $key => $val)
    {
      $data_chart[$val->jenis][$val->jenis_jns] = [];
      foreach($month as $val_c1)
      {
        $jns = $val->jenis;
        $data_chart[$val->jenis][$val->jenis_jns][$val_c1] = 0;
      }
      $data_chart[$val->jenis][$val->jenis_jns][$val->bulan] = $val->jml;
    }
    // dd($data_chart);
    return view('report.reportResling', compact('resign_ter', 'resign_black', 'kons', 'data_chart') );
  }

  public function add_resling()
  {
    return view('hr.resign.formResign');
  }

  public function save_resling(Request $req)
  {
    $msg = ReportModel::save_resling($req);
    return redirect('/report/resling')->with('alerts', $msg);
  }

  public function absensi()
  {
    $belum_absen = ReportModel::get_absensi("belum_absen");
    $menunggu_approve = ReportModel::get_absensi("menunggu_approve");
    $sudah_absen = ReportModel::get_absensi("sudah_absen");
    $total = ReportModel::get_absensi("total");
    $p_ba = (@round( (count($total) - count($sudah_absen) ) / count($total) * 100) );
    $p_sa = (@round( (count($total) - count($belum_absen) ) / count($total) * 100) );
    return view('report.reportAbsensi', compact('belum_absen','menunggu_approve','sudah_absen','p_ba','p_sa') );
  }

  public function absen_approval($nik)
  {
		ReportModel::approve_absen($nik);
		return redirect()->back()->with('alerts',[
			['type'=>'success','text'=> $nik.' ABSEN APPROVED']
		]);
  }

  public function absen_approval_massal(Request $req)
  {
    $getData    = $req->approveMassal;

    if (count($getData) == 0 ){
      return back()->with('alerts',[['type' => 'danger', 'text' => 'NIK Belum Di Checklist']]);
    }

    ReportModel::save_absen_app_mass($getData);
    return redirect('/report/absensi')->with('alerts',[['type' => 'success', 'text' => 'Approve Massal Sukses']]);
  }

  public function absen_decline($id)
  {
      $data = ReportModel::absen_decline($id)[0];

      return view('report.absen_decline', compact('data') );
  }

  public function absen_declineSave(Request $req)
  {
    $update_absen = ReportModel::absen_declineSave($req->input('absen_id'),$req->input('keterangan') );
		if ($update_absen) {
			return redirect('/report/absensi')->with('alerts',[
				['type'=>'success','text'=> 'ABSEN DECLINE SUCCESS']
			]);
		} else {
			return redirect()->back()->with('alerts',[
				['type'=>'danger','text'=> 'ABSEN DECLINE FAILED. TRY AGAIN IN A MINUTE']
			]);
		}
  }

  public function list_accident()
  {
    $data = ReportModel::list_acc();

    $jenis = session('auth')->mitra;

    if(in_array(session('auth')->perwira_level, [1, 30, 69]) )
    {
      $jenis = 'all';
    }

    $data_hr = ReportModel::list_acc_mitra($jenis, date('Y') );
    $data_yearly = ReportModel::list_acc_mitra('tahunan', date('Y') );

    $get_month = [];

    foreach($data_yearly as $val)
    {
      $get_month[$val->bulan] =$val->bulan;
    }

    foreach($get_month as $val)
    {
      foreach($data_yearly as $val_c1)
      {
        $data_tahunan[$val_c1->mitra][$val] = 0;
        $data_tahunan[$val_c1->mitra][$val_c1->bulan] = intval($val_c1->jml_acc);
      }
    }

    // dd($data_hr, $get_month, $data_yearly, $data_tahunan);
    return view('hr.accident.listAccident', compact('data', 'jenis', 'data_hr', 'data_tahunan') );
  }

  public function created_accident($id)
  {
    return view('hr.accident.formAccident');
  }

  public function save_accident(Request $req, $id)
  {
    $msg = ReportModel::save_accident($req, $id);
    return redirect('/report/accident')->with('alerts', $msg);
  }

  public function save_alker(Request $req)
  {
    $msg = ReportModel::save_Alker($req);
    return redirect('/report/alker')->with('alerts', $msg);
  }

  public function save_Newalker(Request $req)
  {
    $msg = ReportModel::save_NAlker($req);
    return \Response::json($msg);
  }

  public function list_alker()
  {
    $alker = ReportModel::get_alker('alker');
    $alker_detail = ReportModel::get_alker('alker_detail');

    // $reader = new Xlsx();
    // \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
    // $spreadSheet = $reader->load(public_path() .'/tes2.xlsx');
    // $data = $spreadSheet->getSheet(0)->toArray();
    // unset($data[0]);

    // $updates = array_filter(array_map(function ($v1) use ($data) {
    //   $key = array_search([$v1->nama, $v1->unit, $v1->posisi, $v1->merk_brand, $v1->nama_brand], array_map(function ($v2)
    //   {
    //     return [$v2[24], $v2[22], $v2[23], $v2[3], $v2[4]];
    //   }, $data));

    //   if ($key !== false)
    //   {
    //     $date_obj = \DateTime::createFromFormat('d/m/Y', $data[$key][20]);
    //     return [
    //       'id' => $v1->id,
    //       'received_to_user_date' => $date_obj->format('Y-m-d'),
    //       'received_to_user_time' => date("H:i:s")
    //     ];
    //   }
    // }, $raw_data->toArray() ) );

    // if (!empty($updates) )
    // {
    //   $updateData = array_values($updates);
    //   // dd($updateData);
    //   $updateQuery = "UPDATE perwira_alkerList
    //   SET received_to_user_date =
    //     CASE id
    //       " . implode(" ", array_map(function ($data) {
    //         return "WHEN " . $data['id'] . " THEN '" . $data['received_to_user_date'] . "'";
    //       }, $updateData)) . "
    //     END,
    //   received_to_user_time =
    //     CASE id
    //       " . implode(" ", array_map(function ($data) {
    //         return "WHEN " . $data['id'] . " THEN '" . $data['received_to_user_time'] . "'";
    //       }, $updateData)) . "
    //     END
    //   WHERE id IN (" . implode(',', array_column($updateData, 'id')) . ")";

    //   DB::statement($updateQuery);
    // }

    // foreach($data as $k => $v)
    // {
    //   $remodel_data[$v[14] ]['nama']                    = $v['14'];
    //   $remodel_data[$v[14] ]['posisi']                  = $v['15'];
    //   $remodel_data[$v[14] ]['unit']                    = $v['16'];
    //   $remodel_data[$v[14] ]['witel']                   = $v['17'];
    //   $remodel_data[$v[14] ]['alker'][$k]['nama']       = $v['4'];
    //   $remodel_data[$v[14] ]['alker'][$k]['jenis']      = $v['5'];
    //   $remodel_data[$v[14] ]['alker'][$k]['merek']      = $v['6'];
    //   $remodel_data[$v[14] ]['alker'][$k]['barang']     = $v['7'];
    //   $remodel_data[$v[14] ]['alker'][$k]['aset']       = $v['8'];
    //   $remodel_data[$v[14] ]['alker'][$k]['jenis_aset'] = $v['9'];

    //   $formats = [
    //     'm/d/Y H:i',
    //     'm/d/Y H:i:s',
    //     'n/j/Y H:i',
    //     'n/j/Y H:i:s',
    //     'Y-m-d H:i',
    //     'Y-m-d H:i:s',
    //   ];

    //   $dateFormatted = $timeFormatted = null;

    //   foreach ($formats as $format)
    //   {
    //     try
    //     {
    //       $date = DateTime::createFromFormat($format, $v['18']);
    //       if ($date !== false)
    //       {
    //         $dateFormatted = $date->format('Y-m-d');
    //         $timeFormatted = $date->format('H:i:s');
    //         break;
    //       }
    //     }
    //     catch (Exception $e)
    //     {
    //         // Handle any exceptions thrown by DateTime::createFromFormat
    //     }
    //   }

    //   $remodel_data[$v[14] ]['alker'][$k]['diterima_date'] = $dateFormatted;
    //   $remodel_data[$v[14] ]['alker'][$k]['diterima_time'] = $timeFormatted;
    // }

    // foreach($remodel_data as $k => $v)
    // {
    //   foreach($v['alker'] as $v2)
    //   {
    //     $search = DB::table('perwira_alkerList As a')
    //     ->leftjoin('perwira_alker_brand As b', 'a.id_alker', '=', 'b.id')
    //     ->Where([
    //       ['a.nama' , $v['nama'] ],
    //       ['a.unit' , $v['unit'] ],
    //       ['a.posisi' , $v['posisi'] ],
    //       ['a.witel' , $v['witel'] ],
    //       ['b.nama' , $v2['barang'] ],
    //     ])
    //     ->first();

    //     if(!$search)
    //     {
    //       $get_id_alker = DB::table('perwira_alker_brand')->where('nama', $v2['barang'])->first();

    //       if(!$get_id_alker)
    //       {
    //         dd($v2);
    //       }

    //       DB::transaction(function () use($v, $v2, $get_id_alker){
    //         DB::table('perwira_alkerList')->insert([
    //           'nama'          => $v['nama'],
    //           'unit'          => $v['unit'],
    //           'posisi'        => $v['posisi'],
    //           'witel'         => $v['witel'],
    //           'id_alker'      => $get_id_alker->id,
    //           'received_date' => $v2['diterima_date'],
    //           'received_time' => $v2['diterima_time'],
    //         ]);
    //       });

    //     }
    //   }
    // }

    return view('hr.alker.listAlker', compact('alker', 'alker_detail') );
  }

  public function addNew_alker()
  {
    return view('hr.alker.formAlker');
  }

  public function list_cuti()
  {
    $data = ReportModel::get_jenis_cut();
    return view('hr.cuti.formJenisCuti', compact('data') );
  }

  public function jenis_cuti(Request $req)
  {
    $search = trim($req->searchTerm, '');
    $check_data = ReportModel::find_jenis_cut($search);
    $data = [];
    foreach ($check_data as $row) {
      $data[] = array("id" => $row->id, "text" => $row->cuti_nm);
    }
    return \Response::json($data);
  }

  public function save_jenis_cuti(Request $req)
  {
    $msg = ReportModel::save_jenis_cut($req);
    return redirect('/report/cuti')->with('alerts', $msg);
  }

  public function briefingList($date)
  {
    $group_telegram = ReportModel::group_telegram();
		$listData = ReportModel::getAllBriefingTgl($date);
		$getFoto = ReportModel::getFotoByNik();
		$dataNow = ReportModel::getDataByTglNik();

    return view('report.briefing.briefingList', compact('date','group_telegram','listData','getFoto','dataNow') );
  }

  public function briefingInput()
  {
    date_default_timezone_set("Asia/Makassar");
    $group_telegram = ReportModel::group_telegram();

    $exist = ReportModel::getDataByTglNik();
    if (!$exist) {
      $data = ReportModel::simpanData();
      $id   = $data;
    } else {
      $id = $exist->id;
      if ($exist->status == 0){
        $path=public_path().'/upload4/briefing/'.$id;
        \File::deleteDirectory($path);
        ReportModel::hapusSemuaFoto($id);
      }
    }

    $getData = ReportModel::getDataById($id);
    $sektor  = ReportModel::getSektor();

    return view('report.briefing.briefingInput',compact('group_telegram','nik', 'id', 'sektor', 'getData') );
  }

  private function briefingSaveFoto($req, $id)
  {
    if (count($req->file) <> 0)
    {
      foreach($req->file('file') as $file)
      {
        $path = public_path().'/upload4/briefing/'.$id.'/';
        if (!file_exists($path) ) {
          if (!mkdir($path, 0770, true) )
            return 'Gagal Menyiapkan Folder Foto Evidence';
        }

        try {
          $image = $file;
          $imageName = $image->getClientOriginalName();
          $image->move("$path", $imageName);

          ReportModel::simpanFoto($imageName, $id);
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'Gagal Menyimpan Foto Evidence ';
        }
      }
    }
	}

  public function briefingSave(Request $req)
  {
    // dd($req->all(), 'ini dede');
    date_default_timezone_set("Asia/Makassar");
		$this->validate($req,[
			'sektor'	=> 'required',
			'catatan'	=> 'required'
		],[
			'sektor.required'	=> 'Sektor Jangan Kosong',
			'catatan.required'	=> 'Catatan Diisi'
		]);

		ReportModel::updateData($req, $req->idLaporan);

    //upload photos
    $this->briefingSaveFoto($req,$req->idLaporan);

		$dataBriefing = ReportModel::getDataById($req->idLaporan);

		$pesan = "BRIEFING REPORT ".date('Y-m-d H:i:s'). "\n";
		$pesan .= "=================================\n";
		$pesan .= "Dilaporkan oleh : ".session('auth')->id_karyawan." [ ".session('auth')->nama." ]\n";
		$pesan .= "Catatan Briefing : ".$dataBriefing->catatan;
		$chatID = $req->sektor;

		Telegram::sendMessage([
        'chat_id' => $chatID,
        'text' => $pesan
    ]);

    	// kirim foto
    	$dataFoto = ReportModel::getFoto($req->idLaporan);
    	if (count($dataFoto)<>0){
    		foreach($dataFoto as $foto){
    			$file = public_path()."/upload4/briefing/".$req->idLaporan."/".$foto->foto;
				if (file_exists($file) ){
					Telegram::sendPhoto([
						'chat_id' => $chatID,
						'photo' => $file,
            'caption' => "Briefing ".$dataBriefing->title." ".$dataBriefing->TGL
					]);
				}
    		}
    	}

    	$pesan = "Kehadiran Teknisi ".$dataBriefing->TGL."\n";
    	$pesan .= "================================= \n";
    	$dataTeknisi = ReportModel::active_team_Briefing();
    	$num = 1;

    	foreach($dataTeknisi as $no=>$teknisi){
    		if($teknisi->approval=='1'){
    			$pesan .= $num++.'. '. $teknisi->nik." [ ".$teknisi->nama." ]"." [".$teknisi->mitra."] [".$teknisi->kesehatan."] \n";
    		};
    	};

    	Telegram::sendMessage([
      		'chat_id' => $chatID,
      		'text' => $pesan
    	]);

    $redirect = [
      'url' => '/report/briefingList/'.date('Y-m-d')
    ];

    Session::flash('alerts',[['type' => 'success', 'text' => 'Laporan Berhasil Disimpan']]);
    return $redirect;
  }

  public function list_recruit_inter()
	{
    $pelamar = $diterima = $blacklist = [];


    $sql = '';

    if(in_array(session('auth')->perwira_level, [71]) )
    {
      $sql .= 'AND up.mitra = '.session('auth')->mitra;
    }

		$pelamar = ReportModel::get_data_pelamar($sql);

    $diterima_mitra = ReportModel::get_data_terima_mitra($sql);

    $diterima_ta = ReportModel::get_data_terima_ta($sql);

    $blacklist = ReportModel::get_data_blacklist($sql);

    return view('report.reportRec_in', compact('pelamar', 'diterima_mitra', 'diterima_ta', 'blacklist') );
	}

	public function detail_cuti($id)
	{
		$cuti = ReportModel::detail_cuti($id);
    // dd($cuti);
    return view('hr.cuti.DetailCuti', compact('cuti') );
	}

	public function MonitorBrevert()
	{
    // dd(session('auth') );
		$data_main = ReportModel::get_brevert_me(session('auth')->id_mitra);
		$log = ReportModel::get_brevert_me(session('auth')->id_mitra, 'log');
    $log_data = $log_main = [];
    // dd($data_main, $log);
    if($data_main)
    {
      foreach($data_main as $key => $val)
      {
        $log_main[$val->noktp]['nama'] = $val->nama;
        $log_main[$val->noktp]['id_nik'] = $val->id_nik;
        $log_main[$val->noktp]['noktp'] = $val->noktp;
        $log_main[$val->noktp]['event'][] = (object) [
          'id' => $val->id,
          'title' => $val->title,
          'jenis' => $val->jenis,
          'tgl_start' => $val->tgl_start,
          'tgl_end' => $val->tgl_end,
          'hadir' => $val->hadir,
        ];
      }
    }

    if($log)
    {
      foreach($log as $key => $val)
      {
        $log_data[$val->noktp]['nama'] = $val->nama;
        $log_data[$val->noktp]['id_nik'] = $val->id_nik;
        $log_data[$val->noktp]['noktp'] = $val->noktp;
        $log_data[$val->noktp]['event'][] = (object) [
          'id' => $val->id,
          'title' => $val->title,
          'jenis' => $val->jenis,
          'tgl_start' => $val->tgl_start,
          'tgl_end' => $val->tgl_end,
          'hadir' => $val->hadir,
        ];
      }
    }
    // dd($log_main, $data_main);
    return view('mitra.brevert.monitorBrevert', compact('log_main', 'log_data') );
	}
}
