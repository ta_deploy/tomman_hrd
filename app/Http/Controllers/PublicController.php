<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use App\DA\ApiModel;
use App\DA\HomeModel;
use App\DA\PublicModel;
use DB;
use Validator;

date_default_timezone_set('Asia/Makassar');

class PublicController extends Controller
{
	public $fileRecruitment = [
		'file_ktp',
		'file_kk',
		'file_sim',
		'file_sim_b',
		'sertifikat_multim',
		'file_cv',
		'file_ijazah',
		'file_sertifikat',
		'file_sertifikat_vaksin',
		'file_surat_keterangan_sehat',
		'file_surat_berkelakuan_baik'
	];

	public function recruitment()
	{
		$data_prov = HomeModel::api_provinsi();
		// $data_ar = json_decode($data_prov, TRUE);
		foreach($data_prov as $val)
		{
			$get_area[$val->id] = $val->nama;
		}

		$get_golongan_darah = [
            (object)['id' => 'O', 'text'  => 'O'],
            (object)['id' => 'A', 'text' => 'A'],
            (object)['id' => 'B', 'text'  => 'B'],
            (object)['id' => 'AB', 'text'  => 'AB'],
        ];

		$get_lokasi_kerja = [
            (object)['id' => 'Kalsel - Banjarmasin', 'text'  => 'Kalsel - Banjarmasin'],
            (object)['id' => 'Kaltim - Balikpapan', 'text' => 'Kaltim - Balikpapan'],
            (object)['id' => 'Kalteng - Palangkaraya', 'text'  => 'Kalteng - Palangkaraya'],
            (object)['id' => 'Kaltara - Tarakan', 'text'  => 'Kaltara - Tarakan'],
            (object)['id' => 'Kalbar - Pontianak', 'text'  => 'Kalbar - Pontianak'],
        ];

		$get_level_pendidikan = DB::table('perwira_level_pendidikan')->get();

		return view('recruitment.index', compact('get_area','get_level_pendidikan', 'get_lokasi_kerja', 'get_golongan_darah') );
	}

	public function ajx_region(Request $req)
	{
		$data = [];

		if(!empty($req->data) )
		{
			if ($req->jenis == 'kota')
			{
				$data = HomeModel::api_kota($req->data);
			}
			elseif ($req->jenis == 'kecamatan')
			{
				$data = HomeModel::api_kecamatan($req->data);
			}
			elseif ($req->jenis == 'kelurahan')
			{
				$data = HomeModel::api_kelurahan($req->data);
			}

		}
		return $data;
	}

	public function recruitmentSave(Request $req)
	{
		$rules = array(
				'noktp'                       => 'required',
				'nama'                        => 'required',
				'jk'                          => 'required',
				'agama'                       => 'required',
				'tempat_lahir'                => 'required',
				'tgl_lahir'                   => 'required',
				'goldar'                      => 'required',
				'email'                       => 'required',
				'alamat'                      => 'required',
				'provinsi'                    => 'required',
				'kota'                        => 'required',
				'kecamatan'                   => 'required',
				'kelurahan'                   => 'required',
				'lokasi_kerja'                => 'required',
				'no_telp'                     => 'required',
				'no_wa'                       => 'required',
				'nokk'                        => 'required',
				'status_perkawinan'           => 'required',
				'ibu'                         => 'required',
				'telpon_2_nm'                 => 'required',
				'telpon_2'                    => 'required',
				'lvl_pendidikan'              => 'required',
				'last_study'                  => 'required',
				'jurusan'                     => 'required',
				'tgllulus'                    => 'required',
				'file_ktp'                    => 'required',
				'file_kk'                     => 'required',
				'file_surat_keterangan_sehat' => 'required',
				'file_surat_berkelakuan_baik' => 'required',
				'file_cv'                     => 'required',
				'file_ijazah'                 => 'required',
				'file_sertifikat_vaksin'      => 'required'
		);

		$messages = [
				'noktp.required'                       => 'Silahkan Isi Kolom "Nomor KTP" Karena Masih Kosong',
				'nama.required'                        => 'Silahkan Isi Kolom "Nama" Karena Masih Kosong',
				'jk.required'                          => 'Silahkan Isi Kolom "Jenis Kelamin" Karena Masih Kosong',
				'agama.required'                       => 'Silahkan Isi Kolom "Agama" Karena Masih Kosong',
				'tempat_lahir.required'                => 'Silahkan Isi Kolom "Tempat Lahir" Karena Masih Kosong',
				'tgl_lahir.required'                   => 'Silahkan Isi Kolom "Tanggal Lahir" Karena Masih Kosong',
				'goldar.required'                      => 'Silahkan Isi Kolom "Golongan Darah" Karena Masih Kosong',
				'email.required'                       => 'Silahkan Isi Kolom "Email" Karena Masih Kosong',
				'alamat.required'                      => 'Silahkan Isi Kolom "Alamat" Karena Masih Kosong',
				'provinsi.required'                    => 'Silahkan Isi Kolom "Provinsi" Karena Masih Kosong',
				'kota.required'                        => 'Silahkan Isi Kolom "Kabupaten/Kota" Karena Masih Kosong',
				'kecamatan.required'                   => 'Silahkan Isi Kolom "Kecamatan" Karena Masih Kosong',
				'kelurahan.required'                   => 'Silahkan Isi Kolom "Kelurahan" Karena Masih Kosong',
				'lokasi_kerja.required'                => 'Silahkan Isi Kolom "Lokasi Kerja" Karena Masih Kosong',
				'no_telp.required'                     => 'Silahkan Isi Kolom "No Handphone" Karena Masih Kosong',
				'no_wa.required'                       => 'Silahkan Isi Kolom "No WhatsApp" Karena Masih Kosong',
				'nokk.required'                        => 'Silahkan Isi Kolom "Nomor Kartu Keluarga" Karena Masih Kosong',
				'status_perkawinan.required'           => 'Silahkan Isi Kolom "Status Perkawinan" Karena Masih Kosong',
				'ibu.required'                         => 'Silahkan Isi Kolom "Nama Ibu Kandung" Karena Masih Kosong',
				'telpon_2_nm.required'                 => 'Silahkan Isi Kolom "Nama Keluarga yang Bisa Dihubungi" Karena Masih Kosong',
				'telpon_2.required'                    => 'Silahkan Isi Kolom "Kontak Keluarga yang Bisa Dihubungi" Karena Masih Kosong',
				'lvl_pendidikan.required'              => 'Silahkan Isi Kolom "Jenjang" Karena Masih Kosong',
				'last_study.required'                  => 'Silahkan Isi Kolom "Nama Institusi Pendidikan Terakhir" Karena Masih Kosong',
				'jurusan.required'                     => 'Silahkan Isi Kolom "Jurusan" Karena Masih Kosong',
				'tgllulus.required'                    => 'Silahkan Isi Kolom "Tanggal Lulus" Karena Masih Kosong',
				'file_ktp.required'                    => 'Silahkan Upload File "KTP" Karena Masih Kosong',
				'file_kk.required'                     => 'Silahkan Isi Kolom "Kartu Keluarga" Karena Masih Kosong',
				'file_surat_keterangan_sehat.required' => 'Silahkan Isi Kolom "Surat Keterangan Sehat" Karena Masih Kosong',
				'file_surat_berkelakuan_baik.required' => 'Silahkan Isi Kolom "Surat Berkelakuan Baik" Karena Masih Kosong',
				'file_cv.required'                     => 'Silahkan Isi Kolom "CV" Karena Masih Kosong',
				'file_ijazah.required'                 => 'Silahkan Isi Kolom "Ijazah Terakhir" Karena Masih Kosong',
				'file_sertifikat_vaksin.required'      => 'Silahkan Isi Kolom "Sertifikat Vaksin Terakhir" Karena Masih Kosong'
		];

		$input = $req->input();
		$validator = Validator::make($req->all(), $rules, $messages);

		$validator->sometimes('noktp', 'required', function ($input) { return true; });
		$validator->sometimes('nama', 'required', function ($input) { return true; });
		$validator->sometimes('jk', 'required', function ($input) { return true; });
		$validator->sometimes('agama', 'required', function ($input) { return true; });
		$validator->sometimes('tempat_lahir', 'required', function ($input) { return true; });
		$validator->sometimes('tgl_lahir', 'required', function ($input) { return true; });
		$validator->sometimes('goldar', 'required', function ($input) { return true; });
		$validator->sometimes('email', 'required', function ($input) { return true; });
		$validator->sometimes('alamat', 'required', function ($input) { return true; });
		$validator->sometimes('provinsi', 'required', function ($input) { return true; });
		$validator->sometimes('kota', 'required', function ($input) { return true; });
		$validator->sometimes('kecamatan', 'required', function ($input) { return true; });
		$validator->sometimes('kelurahan', 'required', function ($input) { return true; });
		$validator->sometimes('lokasi_kerja', 'required', function ($input) { return true; });
		$validator->sometimes('no_telp', 'required', function ($input) { return true; });
		$validator->sometimes('no_wa', 'required', function ($input) { return true; });
		$validator->sometimes('nokk', 'required', function ($input) { return true; });
		$validator->sometimes('status_perkawinan', 'required', function ($input) { return true; });
		$validator->sometimes('ibu', 'required', function ($input) { return true; });
		$validator->sometimes('telpon_2_nm', 'required', function ($input) { return true; });
		$validator->sometimes('telpon_2', 'required', function ($input) { return true; });
		$validator->sometimes('lvl_pendidikan', 'required', function ($input) { return true; });
		$validator->sometimes('last_study', 'required', function ($input) { return true; });
		$validator->sometimes('jurusan', 'required', function ($input) { return true; });
		$validator->sometimes('tgllulus', 'required', function ($input) { return true; });
		$validator->sometimes('file_ktp', 'required', function ($input) { return true; });
		$validator->sometimes('file_kk', 'required', function ($input) { return true; });
		$validator->sometimes('file_surat_keterangan_sehat', 'required', function ($input) { return true; });
		$validator->sometimes('file_surat_berkelakuan_baik', 'required', function ($input) { return true; });
		$validator->sometimes('file_cv', 'required', function ($input) { return true; });
		$validator->sometimes('file_ijazah', 'required', function ($input) { return true; });
		$validator->sometimes('file_sertifikat_vaksin', 'required', function ($input) { return true; });

		if ($validator->fails() )
		{
			return redirect()->back()
				->withInput($req->input() )
				->withErrors($validator)
				->with('alerts', [
					['type' => 'danger', 'text' => '<strong>GAGAL</strong> Harap Lengkapi Kolom yang Wajib']
				]);
		};

		$check_data = PublicModel::check_data($req);
		if (count($check_data)>0)
		{
			if ($check_data->status_pelamar == 'BLACKLIST')
			{
				return back()->with('alerts', [
					['type' => 'danger', 'text' => '<strong>GAGAL</strong> Nomor KTP Terdeteksi Dalam Daftar Blocklist!']
				]);
			}
			else
			{
				return back()->with('alerts', [
					['type' => 'danger', 'text' => '<strong>GAGAL</strong> Nomor KTP Sudah Pernah Terdaftar Sebelumnya Pada Tanggal'.$check_data->created_at]
				]);
			}
		}

		PublicModel::save_data($req);

		return redirect('/recruitment')->with('alerts', [
			['type' => 'success', 'text' => 'Terimakasih Lamaran Anda Berhasil Disimpan, Silahkan Tunggu Informasi Selanjutnya dari Kami']
		]);

	}
}