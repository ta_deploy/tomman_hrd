<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\DA\MitraModel;
use DB;

class MitraController extends Controller
{
  public function listKaryawan()
  {
    return view('mitra.teknisi.listTeknisi');
  }

  public function addNew_teknisi()
  {
    return view('mitra.teknisi.formAddTeknisi');
  }

  public function resign($id)
  {
    return view('hr.resign.formResign');
  }

  public function addNew_alker()
  {
    return view('mitra.alker.formAlker');
  }

  public function save_alker_mitra()
  {
    // $m
    return view('mitra.alker.formAlker');
  }

  public function submit_alker_mitra(Request $req)
  {
    $msg = MitraModel::save_alker($req);
    return redirect('/report/alker')->with('alerts', $msg);
  }

}
