<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\DA\LoginModel;
use DB;
use Telegram;
use Validator;
use Cookie;

class LoginController extends Controller
{
	public function index()
	{
		return view('login');
	}

	public function login(Request $req, LoginModel $Loginmodel)
	{
		$user = $req->input('user');
		$pass = $req->input('pass');
		$type = $req->input('type_login');
		// dd($user, $pass, $type);
		$result = $Loginmodel->login($user, $pass);
		$sso_login = $Loginmodel->sso_login($user, $pass);
		$login_mom = $Loginmodel->login_mom($user, $pass);
		// dd($login_mom, $result);
		switch ($type) {
			case 'lokal':
				if (count($result) > 0)
				{
					$pixeltheme = DB::table('user')->where('id_user', $user)->first()->pixeltheme;
					$result[0]->pixeltheme = $pixeltheme ?: 'frost';
					// dd($result[0]);
					$session = $req->session();
					$session->put('auth', $result[0]);
					$this->ensureLocalUserHasRememberToken($result[0], $Loginmodel);
					return $this->successResponse($result[0]->psb_remember_token);
				} else if (count($login_mom) > 0){
					$login_mom->pixeltheme = 'frost';
					$login_mom->nama = $login_mom->name;
					$login_mom->id_user = $login_mom->nik;
					$login_mom->psb_remember_token = $login_mom->token;

					if($login_mom->id_unit == 27)
					{
						$login_mom->perwira_level = 69;
					}
					else
					{
						$login_mom->perwira_level = 33;
					}

					$login_mom->id_people = 0;
					$login_mom->noktp = 0;

					$session = $req->session();
					$session->put('auth', $login_mom);
					$this->ensureLocalUserHasRememberToken($login_mom, $Loginmodel, 'login_mom');
					return $this->successResponse($login_mom->psb_remember_token);
				} else {
					return redirect('/login')
					->with('alerts', [['type' => 'danger', 'text' => 'Login Gagal']]);
				}
				break;

			case 'sso':
				if ($sso_login['auth'] == 'Yes')
				{
					$nik = $user;
					$nama = $sso_login['nama'];

					$data = DB::table('absen_kehadiran_dow')->where('nik', $nik)->whereDate('tgl_hadir', date('Y-m-d') )->first();

    				return view('hr.absensi.index', compact('data', 'nik', 'nama') );
				}
				break;
		}
	}

	public function register()
	{
		$get_mitra = DB::SELECT('SELECT mitra_amija as id, mitra_amija_pt as text, kat FROM mitra_amija WHERE kat IN ("DELTA", "NON", "ORGANIK") GROUP BY mitra_amija_pt ORDER BY kat ASC');
		$get_witel = DB::SELECT('SELECT reg_psb as id, nama_witel as text FROM witel WHERE active = 1 ORDER BY reg_psb ASC');
		return view('register', compact('get_mitra', 'get_witel') );
	}

	public function registerSave(Request $req)
	{
		// $check = DB::table('1_2_employee')->where('nik', $req->input('username') )->first();
		$witel = DB::table('witel')->where('reg_psb', $req->input('reg_psb') )->first();
		$user = DB::table('user')->where('id_user', $req->input('username') )->first();

		$rules = array(
			'username' => 'required|regex:/^[a-zA-Z0-9- ]+$/u',
		);

		$messages = [
			'username.required' => 'Silahkan isi Username!',
		];

		$validator = Validator::make($req->all(), $rules, $messages);

		if ($validator->fails() )
		{
			return redirect()->back()
			->withInput($req->all() )
			->withErrors($validator)
			->with('alerts', [
				['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan NIK']
			]);
		}

		DB::table('1_2_employee')->Where('nik', $req->input('username') )->delete();
		DB::table('user')->Where('id_karyawan', $req->input('username') )->delete();

		DB::table('1_2_employee')->insert([
			'nik'               => $req->input('username'),
			'nik_amija'         => $req->input('username'),
			'nama'              => $req->input('name'),
			'sub_directorat'    => "Consumer Service",
			'sub_sub_unit'      => "MULTISKILL",
			'atasan_1'          => preg_replace("/[^a-zA-Z0-9-]+/", '', $req->input('atasan1') ),
			'no_telp'			      => $req->input('no_telp'),
			'email'				      => $req->input('email'),
			'status'            => "PKS",
			'status_konfirmasi' => "Valid",
			'CAT_JOB'           => "Consumer Service",
			// 'ACTIVE'            => 1,
			'ACTIVE'            => 0,
			'Witel_New'         => $witel->nama_witel,
			'laborcode'         => preg_replace("/[^a-zA-Z0-9-]+/", '', $req->input('username') ),
			'mitra_amija'       => $req->input('mitra'),
			'updateBy'          => 20981020,
			'last_update'       => date('Y-m-d H:i:s'),
			'dateUpdate'        => date('Y-m-d H:i:s')
		]);

		$token = $this->generateRememberToken($req->input('username') );

		$save_user = [
			'id_user'				      => $req->input('username'),
			'password'				    => md5($req->input('pass') ),
			'level'					      => $req->input('level'),
			// 'maintenance_level'		=> 2,
			'id_karyawan'			    => $req->input('username'),
			'reg'					        => 6,
			'witel'					      => $witel->id_witel,
			'psb_reg'				      => $witel->reg_psb,
			'psb_remember_token' 	=> $token,
			'updated_user'			  => 20981020,
			'datetime_updated'	  => date('Y-m-d H:i:s')
		];

		if($req->input('level') == 9)
		{
			$save_user['is_jointer'] = 1;
			$save_user['pt2_level'] = 0;
		}

		if($req->input('level') == '5')
		{
			$save_user['level'] = 5;
			$save_user['pt2_level'] = 8;
		}

		DB::table('user')->insert($save_user);

		$nik = $req->input('username');
		$name = $req->input('name');
		$level_psb = DB::table('level')->where('id_level', $req->input('level') )->first();
		$relasi = $req->input('atasan1');
		$datetime = date('Y-m-d H:i:s');
		$IP = $_SERVER['REMOTE_ADDR'];
		$chatID = '-306306083';

		Telegram::sendMessage([
			'chat_id' => $chatID,
			'parse_mode' => 'html',
			'text' => "<b>REG SUCCESS</b>\n\nNIK : <i>$nik</i>\nNama : <i>$name</i>\nWitel : <i>$witel->nama_witel</i>\nLevel PSB : <i>$level_psb->nama</i>\nNIK Relasi : <i>$relasi</i>\nIP Address : <i>$IP</i>\n\n<i>$datetime</i>"
		]);

		return redirect('/login')->with('alerts', [
			['type' => 'success', 'text' => 'Registered Success, Please Contact Admin for Activate User']
		]);
	}
	public function getLoginResult(Request $req, LoginModel $Loginmodel)
	{
		$user = $req->input('user');
		$pass = $req->input('pass');
		$result = $Loginmodel->login($user, $pass);
		// dd($result);
		$response = new \stdClass;
		if (count($result) ){
			$response->success = 1;
			$response->message = "Selamat datang ".$result[0]->nama;
			$response->id = $result[0]->id_user;
			$response->username = $result[0]->id_user;
			$response->rememberToken = $result[0]->psb_remember_token;
			$response->version = 1;

		} else {
			$response = new \stdClass;
			$response->success = null;
			$response->message = "Username atau password salah";
			$response->id = 0;
			$response->username = 0;
			$response->rememberToken = 0;
			$response->version = 1;
		}
		header('Content-type: application/json');
		echo(json_encode($response) );
	}

	public function getLoginByToken(Request $req, LoginModel $Loginmodel)
	{
		$token = $req->input('remember_token');
		$result = $Loginmodel->loginByToken($token);
		// dd($result);
		$response = new \stdClass;
		if (count($result) ){
			$response->success = 1;
			$response->message = "Selamat datang ".$result[0]->nama;
			$response->id = $result[0]->id_user;
			$response->username = $result[0]->id_user;
			$response->rememberToken = $result[0]->psb_remember_token;
			$response->version = 1;

		} else {
			$response = new \stdClass;
			$response->success = 0;
			$response->message = "Username atau password salah";
			$response->id = 0;
			$response->username = 0;
			$response->rememberToken = 0;
			$response->version = 1;
		}
		header('Content-type: application/json');
		echo(json_encode($response) );
	}

	public function logout()
	{
		Session::forget('auth');
		Session::forget('badges');
		Cookie::queue(Cookie::forget('presistent-token') );
		// Cookie::queue(Cookie::forget('nik-tomman') );

		return redirect('/login');
	}

	private function ensureLocalUserHasRememberToken($localUser, LoginModel $Loginmodel, $jenis = 'normal')
	{
		$token = $localUser->psb_remember_token;
		if (!$localUser->psb_remember_token)
		{
			$token = $this->generateRememberToken($localUser->id_user);
			$Loginmodel->remembertoke($localUser, $token, $jenis);
			$localUser->psb_remember_token = $token;
		}

		return $token;
	}

	private function generateRememberToken($nik)
	{
		return md5($nik . microtime() );
	}

	private function successResponse($rememberToken)
	{
		if (Session::has('auth-originalUrl') )
		{
			$url = Session::pull('auth-originalUrl');
		}
		else
		{
			if (in_array(session('auth')->perwira_level,[1,69,30,46,71]) )
			{
				$url = '/';
			} elseif (session('auth')->perwira_level == 100) {
				$url = '/profile';
			} else {
				$url = '/performance';
			}
		}

		$response = redirect($url);
		if ($rememberToken)
		{
			$response->withCookie(cookie()->forever('presistent-token', $rememberToken) );
			// $response->withCookie(cookie()->forever('nik-tomman', session('auth')->id_karyawan) );
		}
		return $response;
	}

	public function themes()
  {

    return view('theme');
  }
  public function themeSave(Request $req )
  {

    session('auth')->pixeltheme=$req->theme;
    DB::table('user')->where('id_user', session('auth')->id_user)->update([
      'pixeltheme'  => $req->theme
    ]);
    return back();
  }

	public function survey_education()
	{
		$data_naker = DB::connection('data_center')
		->table('master_karyawan As a')
		->select('nik As id', DB::RAW("CONCAT(name, ' (', nik, ')') AS text") )
		->Where('is_status', 1)
		->OrderBy('name', 'ASC')
		->get();

		return view('survey_education', compact('data_naker') );
	}

	public function save_survey_education(Request $req)
	{
		if($req->select_opsi_avail == 'yes')
		{
			$nik = $req->nik_nama;

			$search_name = DB::connection('data_center')
			->table('master_karyawan As a')
			->Where('nik', $nik)
			->first();

			$name = $search_name->name;
		}
		else
		{
			$name = $req->nama;
			$nik = $req->nik;
		}

		DB::table('perwira_survey_pendidikan')->insert([
			'nik'              => $nik,
			'nama'             => $name,
			'level'            => $req->level,
			'level_pendidikan' => $req->level_pendidikan,
			'created_date'     => date('Y-m-d'),
			'created_time' 		 => date('H:i:s'),
		]);

		return redirect('survey_education')->with('alerts', [['type' => 'success', 'text' => 'Survey Berhasil Disimpan.']]);
	}
}