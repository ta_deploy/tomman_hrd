<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CheckRole
{
    public function handle($request, Closure $next, ...$roles)
    {
        $lvl = [
            1 => 1,
            30 => 30,
            46 => 46,
            22 => 22,
            69 => 69,
            12 => 12,
            71 => 71,
            72 => 72,
            100 => 100,
            44 => 44,
        ];

        $level = Session('auth')->perwira_level;
        // dd($roles, empty($roles), $level, $lvl);
        if(empty($roles) ){
            return redirect('login');
        }
        if (isset($lvl[$level]) && in_array($lvl[$level], $roles) ){
            return $next($request);
        }

        Session::put('auth-originalUrl', $request->fullUrl() );
        if ($request->ajax() ) {
            return response('UNAUTHORIZED', 401);
        } else {
            Session::put('auth-originalUrl', '');
            return redirect('login');
        }
    }

}
