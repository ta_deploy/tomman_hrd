<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;

class Witel
{
    const TABLE = 'witel';
    public static function get()
    {
         return DB::table(self::TABLE)
            ->get();
    }
    public static function save( $req)
        {
            DB::table(self::TABLE)->insert([
                'nama_witel' => $req->witel
            ]);
        }
    public static function update($req,$id)
        {
            DB::table(self::TABLE)->where('id', $id)->update([
                'nama_witel' => $req->witel
            ]);
        }
    public static function delete( $id)
        {
            DB::transaction(function() use($id) {

                DB::table(self::TABLE)
                   ->where('id', $id)
                   ->delete();
            });
        }
    public static function getWitelByIdLoker($id)
    {
         $idwitel= DB::table('loker')
            ->select('witel_id')
            ->where('id',$id)
            ->first();

         return DB::table(self::TABLE)
            ->select('id as id','nama_witel as witel')
            ->where('id',$idwitel->witel_id)
            ->first();
    }
    //loker
    public static function getLoker()
    {
     return DB::table('loker')
                ->select('loker.id as id','witel.nama_witel as witel','loker.nama_loker as nama_loker')
                ->leftJoin('witel','loker.witel_id', '=', 'witel.id')
                ->get();
    }
    public static function getLokerById($id)
    {
     return DB::table('loker')
        ->where('id',$id)
        ->first();
    }
    public static function saveLoker($req)
        {
         if (!empty($req->id) ){
                DB::table('loker')->where('id', $req->id)->update([
                    'witel_id' => $req->witel,
                    'nama_loker' => $req->loker
                ]);
         }else{
                DB::table('loker')->insert([
                    'witel_id' => $req->witel,
                    'nama_loker' => $req->loker
                ]);
         }
        }
    public static function deleteLoker( $id)
        {
            DB::table('loker')
               ->where('id', $id)
               ->delete();
        }
     public static function getLokerSelect2()
    {
     return DB::table('loker')
        ->select('id as id','nama_loker as text')
        ->get();
    }

    //STO
    public static function getSto()
    {
     return DB::table('sto')
                ->select('sto.id as id','witel.nama_witel as witel','loker.nama_loker as nama_loker',
                            'sto.sto as sto','sto.kode_sto as kode_sto')
                ->leftJoin('loker','sto.loker_id','=','loker.id')
                ->leftJoin('witel','loker.witel_id', '=', 'witel.id')
                ->get();
    }
    public static function getStoById($id)
    {
     return DB::table('sto')
        ->select('sto.id as id','witel.nama_witel as witel','loker.nama_loker as nama_loker',
                'sto.loker_id as loker_id','sto.sto as sto','sto.kode_sto as kode_sto')
        ->leftJoin('loker','sto.loker_id','=','loker.id')
        ->leftJoin('witel','loker.witel_id', '=', 'witel.id')
        ->where('sto.id',$id)
        ->first();
    }
    public static function saveSto($req)
        {
         if (!empty($req->id) ){
                DB::table('sto')->where('id', $req->id)->update([
                    'loker_id' => $req->psa,
                    'sto' => $req->sto,
                    'kode_sto'=> $req->kodesto
                ]);
         }else{
                DB::table('sto')->insert([
                    'loker_id' => $req->psa,
                    'sto' => $req->sto,
                    'kode_sto'=> $req->kodesto
                ]);
         }
        }
    public static function deleteSto( $id)
        {
            DB::table('sto')
               ->where('id', $id)
               ->delete();
        }
    public static function getStoSelect2()
    {
     return DB::table('sto')
        ->select('id as id','sto as text')
        ->get();
    }

    public static function getStoByLoker($id)
    {
     return DB::table('sto')
        ->select('id as id','sto as text')
        ->where('loker_id',$id)
        ->get();
    }
}