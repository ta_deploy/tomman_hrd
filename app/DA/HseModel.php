<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Cookie;

class HseModel
{
    public static function submit_teknisi($req, $foto, $auth)
    {
        return DB::transaction( function() use($req, $auth, $foto)
        {
            $id = DB::table('perwira_hse')->insertGetId([
                'koordinat'     => $req->koor,
                'odp_near'      => $req->odp_near,
                'alamat_manual' => $req->alamat_manual,
                'note'          => $req->note,
                'created_by'    => $auth
            ]);

            DB::Table('perwira_hse_log')->insert([
                'ph_id'      => $id,
                'detail'     => 'Input Potensi Bahaya',
                'status'     => 'Create',
                'created_by' => $auth
            ]);

            $that = new HseModel();
            $that->handeUpload_hse($req, $id, $foto);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Data Potensi Bahaya Berhasil Ditambahkan!'
            ];

            return $msg;
        });

    }

    private function handeUpload_hse($req, $id, $foto)
    {
        $path = public_path() . '/upload4/perwira/rawan_celaka/potensi_bahaya/'.$id;

        if (!file_exists($path) )
        {
            if (!mkdir($path, 0770, true) )
            {
                return 'gagal menyiapkan folder '.$path;
            }
        }

        foreach($foto as $v)
        {
            if ($req->hasFile($v) )
            {
                $file = $req->file($v);
                try
                {
                    $filename = $v .'.'. strtolower( $file->getClientOriginalExtension() );
                    $file->move("$path", "$filename");
                }
                catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
                {
                    return 'gagal menyimpan '. $v;
                }
            }
        }
    }

    public static function update_perbaikan($req, $id, $foto)
    {
        $auth = session('auth')->id_karyawan;

        return DB::transaction( function() use($req, $id, $auth, $foto)
        {
            $get_prev = DB::table('perwira_hse')->Where('id', $id)->first();

            switch ($get_prev->update_hse) {
                case 0:
                    $update_hse = 1;
                    $status_log  = 'Inbox Tiket';
                break;
                default:
                    $update_hse = 2;
                    $status_log  = 'Approval';
                break;
            }

            DB::table('perwira_hse')->Where('id', $id)->update([
                'note_perbaikan' => $req->note_perbaikan,
                'update_hse'     => $update_hse,
                'modified_by'    => $auth
            ]);

            DB::Table('perwira_hse_log')->insert([
                'ph_id'      => $id,
                'status'     => $status_log,
                'detail'     => $req->note_perbaikan,
                'created_by' => $auth
            ]);

            $that = new HseModel();
            $that->handeUpload_tindak_lanjut($req, $id, $foto);

            $msg['update_hse'] = $get_prev->update_hse;

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Data Hse Berhasil Di Update!'
            ];

            return $msg;
        });
    }

    public static function update_hse_status($req, $id)
    {
        $auth = session('auth')->id_karyawan;

        return DB::transaction( function() use($req, $id, $auth)
        {
            $get_prev = DB::table('perwira_hse')->Where('id', $id)->first();
            $isHapus = $submit_info = 0;

            switch ($req->submit_info)
            {
                case 'approve':
                    $submit_info = 1;

                    if($get_prev->update_hse == 2)
                    {
                        $submit_info = 3;
                    }

                    $status_log = 'Approved';
                    $type_msg = 'success';
                break;
                default:
                    if($get_prev->update_hse == 0)
                    {
                        $isHapus = 1;
                        $req->submit_info = 'Hapus';
                        $status_log = 'Deleted';
                    }
                    else
                    {
                        $submit_info = 4;
                        $type_msg = 'danger';
                        $status_log = 'Rejected';
                    }
                break;
            }

            DB::table('perwira_hse')->Where('id', $id)->update([
                'note_perbaikan' => $req->note_perbaikan,
                'update_hse'     => $submit_info,
                'isHapus'        => $isHapus,
                'modified_by'    => $auth
            ]);

            DB::Table('perwira_hse_log')->insert([
                'ph_id'      => $id,
                'detail'     => $req->note_perbaikan,
                'status'     => $status_log,
                'created_by' => $auth
            ]);

            $msg['update_hse'] = $get_prev->update_hse;

            $msg['msg'] = [
                'type' => $type_msg,
                'text' => 'Data Hse Berhasil Di ' . ucwords($req->submit_info)
            ];

            return $msg;
        });
    }

    public static function countMenu()
    {
        $data = new \stdClass();
        $data->check_first    = count(self::list_bahaya() );
        $data->pengaduan      = count(self::list_bahaya(1) );
        $data->sdh_dikerjakan = count(self::list_bahaya(2) );
        $data->reject         = count(self::list_bahaya(4) );

        return $data;
    }

    public static function delete_potensi($id)
    {
        $auth = session('auth')->id_karyawan;

        return DB::transaction( function() use($id, $auth)
        {
            DB::table('perwira_hse')->Where('id', $id)->update([
                'isHapus' => 1
            ]);

            DB::Table('perwira_hse_log')->insert([
                'ph_id'      => $id,
                'status'     => 'Deleted',
                'detail'     => 'Delete Potensi Bahaya',
                'created_by' => $auth
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Data Kerusakan Berhasil Di Hapus!'
            ];

            return $msg;
        });

    }

    private function handeUpload_tindak_lanjut($req, $id, $foto)
    {
        $path = public_path() . '/upload4/perwira/rawan_celaka/tindak_lanjut/'.$id;

        if (!file_exists($path) )
        {
            if (!mkdir($path, 0770, true) )
            {
                return 'gagal menyiapkan folder '.$path;
            }
        }

        foreach($foto as $v)
        {
            if ($req->hasFile($v) )
            {
                $file = $req->file($v);

                try
                {
                    $filename = $v .'.'. strtolower( $file->getClientOriginalExtension() );
                    $file->move("$path", "$filename");
                }
                catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
                {
                    return 'gagal menyimpan '. $v;
                }
            }
        }
    }

    public static function list_bahaya($jenis_hse = 0)
    {
        return DB::Table('perwira_hse As ph')
        ->Leftjoin('1_2_employee As emp', 'ph.created_by', '=', 'emp.nik')
        ->Leftjoin('1_2_employee As emp_update', 'ph.modified_by', '=', 'emp_update.nik')
        ->select('ph.*', 'emp.nama As nama_created', 'emp.no_telp As created_no_telp', 'emp_update.nama As nama_updated')
        ->WhereNotNull('ph.id')
        ->Where([
            ['isHapus', 0],
            ['update_hse', $jenis_hse],
        ])
        ->OrderBy('modified_at', 'DESC')
        ->get();
    }

    public static function detail_potensi($id)
    {
        return DB::Table('perwira_hse As ph')
        ->Leftjoin('1_2_employee As emp', 'ph.created_by', '=', 'emp.nik')
        ->Leftjoin('1_2_employee As emp_update', 'ph.modified_by', '=', 'emp_update.nik')
        ->select('ph.*', 'emp.nama As nama_created', 'emp.no_telp As created_no_telp', 'emp_update.nama As nama_updated')
        ->where('id', $id)
        ->first();
    }

    public static function detail_potensi_log($id)
    {
        return DB::Table('perwira_hse_log As phl')
        ->Leftjoin('1_2_employee As emp', 'phl.created_by', '=', 'emp.nik')
        ->select('phl.*', 'emp.nama As nama_created')
        ->where('ph_id', $id)
        ->OrderBy('id', 'DESC')
        ->get();
    }

    public static function get_all_data($isHapus = 0)
    {
        return DB::table('perwira_hse')
        ->select('perwira_hse.*', DB::RAW("(CASE
            WHEN update_hse = 0 THEN 'Baru Dibuat'
            WHEN update_hse = 1 THEN 'Sedang Dikerjakan'
            WHEN update_hse = 2 THEN 'Menunggu Approva'
            WHEN update_hse = 3 THEN 'Pekerjaan Selesai'
            ELSE 'Ditolak' END) As status_hse") )
        ->Where('isHapus', $isHapus)
        ->get();
    }
}