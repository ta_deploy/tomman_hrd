<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;

class Ttd
{
    const TABLE = 'ttd';
    public static function getAll()
    {
        return DB::table(self::TABLE)->orderBy('id','desc')->get();
    }
    public static function getTtd()
    {
        return DB::table(self::TABLE)
            ->select('id as id', 'nama as text', 'nik')
            ->where('status',1)
            ->get();
    }
    public static function getNikAtasan()
    {
        return DB::table(self::TABLE)
            ->select('nik as id', 'nama as text', 'nik')
            ->where('status',1)
            ->get();
    }
    public static function save($request)
    {

    if(!empty($request->id) ){
        //update
        DB::table(self::TABLE)->where('id', $request->id)->update([
                        "nik" => $request->nik,
                        "nama" => $request->nama,
                        "jabatan" => $request->jabatan,
                        "status" =>$request->status
                    ]);
        }else {
        DB::table(self::TABLE)->insertGetId([
                        "nik" => $request->nik,
                        "nama" => $request->nama,
                        "jabatan" => $request->jabatan,
                        "status" =>$request->status
                        ]);
        }
    }
    public static function delete($id)
    {
        DB::table(self::TABLE)->where('id', $id)->delete();
    }
    public static function getById($id)
    {
             return DB::table(self::TABLE)
                ->where('id',$id)
                ->first();
    }

}