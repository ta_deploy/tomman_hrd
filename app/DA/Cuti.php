<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;

class Cuti
{
    const TABLE = 'cuti';
    //cuti
    public static function getCutiById($id)
    {
     return DB::table(self::TABLE)
        ->where('id',$id)
        ->first();
    }
    public static function getDetailCutiByIdCuti($id)
    {
     return DB::table('detail_cuti')
        ->select('tgl')
        ->where('id_cuti',$id)
        ->get();
    }
    public static function getSisaCuti($id)
    {
         $tahun = (int)date('Y');
         $tahunakhir=$tahun+1;
         $awal =$tahun.'-04-01';
         $akhir =$tahunakhir.'-03-31';

     return DB::table('detail_cuti')
        ->leftJoin('cuti','detail_cuti.id_cuti','=','cuti.id')
        ->where('cuti.id_jenis_cuti',1)
        ->where('cuti.id_karyawan',$id)
        ->whereBetween('detail_cuti.tgl',[$awal, $akhir])
        ->get();
    }
    public static function getListCutiNaker($id)
    {
     return DB::table('detail_cuti')
        ->select('detail_cuti.tgl as tgl','jenis_cuti.jenis_cuti as jenis_cuti')
        ->leftJoin('cuti','detail_cuti.id_cuti','=','cuti.id')
        ->leftJoin('jenis_cuti','cuti.id_jenis_cuti','=','jenis_cuti.id')
        ->where('cuti.id_karyawan',$id)
        ->orderBy('detail_cuti.id','desc')
        ->get();
    }

    public static function getKalender()
    {
        $tahun = (int)date('Y');
        $tahunlalu=$tahun-1;
        $tahundepan=$tahun+1;
        $awal =$tahunlalu.'-01-01';
        $akhir =$tahundepan.'-01-01';

     return DB::table('detail_cuti')
        ->select('cuti.id as id','karyawan.nama as nama','detail_cuti.tgl as tgl')
        ->leftJoin('cuti','detail_cuti.id_cuti','=','cuti.id')
        ->leftJoin('karyawan','cuti.id_karyawan','=','karyawan.id')
        ->whereBetween('detail_cuti.tgl',[$awal, $akhir])
        ->get();
    }
    public static function save( $req)
    {
     $tgl=explode(",", $req->tgl);

     if (!empty($req->id) ){
        DB::transaction(function() use($req,$tgl) {
            DB::table(self::TABLE)->where('id', $req->id)->update([
                'id_karyawan' => $req->nama,
                'id_jenis_cuti' => $req->jenis,
                'alasan' => $req->alasan
            ]);
            //detail cuti
            DB::table('detail_cuti')
                   ->where('id_cuti', $req->id)
                   ->delete();

            for ($x=0;$x<count($tgl);$x++) {
                DB::table('detail_cuti')->insert(
                ['id_cuti' => $req->id,
                'tgl' =>$tgl[$x]
                ]);
            }
        });

     }else{
        DB::transaction(function() use($req,$tgl) {
            $idcuti= DB::table(self::TABLE)->insertGetId([
                'id_karyawan' => $req->nama,
                'id_jenis_cuti' => $req->jenis,
                'alasan' => $req->alasan
            ]);
            //detail cuti
            for ($x=0;$x<count($tgl);$x++) {
                DB::table('detail_cuti')->insert(
                ['id_cuti' => $idcuti,
                'tgl' =>$tgl[$x]
                ]);
            }
        });

    }
    }
    public static function delete( $id)
    {
        DB::transaction(function() use($id) {
            DB::table(self::TABLE)
                   ->where('id', $id)
                   ->delete();

            DB::table('detail_cuti')
                   ->where('id_cuti', $id)
                   ->delete();
        });
    }

    //jenis cuti
    public static function getJenisCuti()
    {
         return DB::table('jenis_cuti')
            ->get();
    }
    public static function getJenisCutiSelect2()
    {
        return DB::table('jenis_cuti')->select('id as id', 'jenis_cuti as text')->get();
    }
    public static function saveJenisCuti( $req)
        {
            DB::table('jenis_cuti')->insert([
                'jenis_cuti' => $req->jenis_cuti
            ]);
        }
    public static function updateJenisCuti($req,$id)
        {
            DB::table('jenis_cuti')->where('id', $id)->update([
                'jenis_cuti' => $req->jenis_cuti
            ]);
        }
    public static function deleteJenisCuti( $id)
        {
            DB::table('jenis_cuti')
               ->where('id', $id)
               ->delete();
        }

}