<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;

class Accident
{
    const TABLE = 'kecelakaan_kerja';
    public static function get()
    {
         return DB::table(self::TABLE)
            ->get();
    }
    public static function getById($id)
    {
     return DB::table(self::TABLE)
        ->where('id',$id)
        ->first();
    }
    public static function getList($req)
    {
    $tanggal1 = $req->tgl;
    $tanggal=explode("-",$tanggal1);
    if ($req->cari==1){
        return DB::table(self::TABLE)
        ->select('kecelakaan_kerja.id as id','kecelakaan_kerja.id_naker as id_naker','karyawan.nik as nik',
            'karyawan.nama as nama','karyawan.bpjs as bpjs','loker.nama_loker as psa',
            'job_position.position_name as position_name','kecelakaan_kerja.tgl as tgl',
            'kecelakaan_kerja.kronologi_kejadian as kronologi_kejadian',
            'kecelakaan_kerja.detail_cedera as detail_cedera','kecelakaan_kerja.alamat as alamat','kecelakaan_kerja.tindakan as tindakan')
        ->leftJoin('karyawan','kecelakaan_kerja.id_naker','=','karyawan.id')
        ->leftJoin('job_position', 'kecelakaan_kerja.id_naker', '=', 'job_position.id_naker')
        ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
        ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
        ->whereMonth('kecelakaan_kerja.tgl', $tanggal[1])
        ->whereYear('kecelakaan_kerja.tgl', $tanggal[0])
        ->get();
        }
     else if ($req->cari==2){
        return DB::table(self::TABLE)
        ->select('kecelakaan_kerja.id as id','kecelakaan_kerja.id_naker as id_naker','karyawan.nik as nik',
            'karyawan.nama as nama','karyawan.bpjs as bpjs','loker.nama_loker as psa',
            'job_position.position_name as position_name','kecelakaan_kerja.tgl as tgl',
            'kecelakaan_kerja.kronologi_kejadian as kronologi_kejadian',
            'kecelakaan_kerja.detail_cedera as detail_cedera','kecelakaan_kerja.alamat as alamat','kecelakaan_kerja.tindakan as tindakan')
        ->leftJoin('karyawan','kecelakaan_kerja.id_naker','=','karyawan.id')
        ->leftJoin('job_position', 'kecelakaan_kerja.id_naker', '=', 'job_position.id_naker')
        ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
        ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
        ->whereYear('kecelakaan_kerja.tgl', $tanggal[0])
        ->get();
        }
    }
    public static function getListByNaker($req)
    {
     return DB::table(self::TABLE)
        ->where('id_naker',$req->nama)
        ->get();
    }

    public static function save($req)
    {
       if(!empty($req->id) ){
    //update
            DB::transaction(function() use($req) {
            DB::table(self::TABLE)->where('id', $req->id)->update([
                            "id_naker" => $req->nama,
                            "tgl" => $req->tgl,
                            "alamat" => $req->alamat,
                            "detail_cedera" => $req->detail_cedera,
                            "kronologi_kejadian" => $req->kronologi,
                            "tindakan" => $req->tindakan
                        ]);
            });
        }else {
            DB::transaction(function() use($req) {
            DB::table(self::TABLE)->insert([
                            "id_naker" => $req->nama,
                            "tgl" => $req->tgl,
                            "alamat" => $req->alamat,
                            "detail_cedera" => $req->detail_cedera,
                            "kronologi_kejadian" => $req->kronologi,
                            "tindakan" => $req->tindakan
                            ]);

            });
        }
    }
    public static function delete($id)
    {
        DB::table(self::TABLE)
               ->where('id', $id)
               ->delete();
    }

}