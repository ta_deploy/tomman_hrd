<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Session;
use DateTime;

date_default_timezone_set("Asia/Makassar");
class AjaxModel{

  public static function get_alker_teknisi($req)
  {
    $witel  = $req->witel;
    $unit   = $req->unit;
    $posisi = $req->posisi;
    $umur   = $req->umur;

    $draw   = $req->draw;
    $start  = $req->start;
    $length = $req->length;
    $search = $req->input('search.value');

    $array_column = ['no', 'a.nama', 'a.unit', 'a.posisi', 'a.witel'];
    $order_column = $req->input('order.0.column');
    $order_dir    = $req->input('order.0.dir');

    $raw_alker_list = DB::table('perwira_alkerList AS a')
    ->select('a.*', DB::RAW("TIMESTAMPDIFF(MICROSECOND, a.received_to_user_date, NOW() ) AS microsecond_diff") )
    ->whereNotNull('a.received_to_user_date')
    ->groupBy('a.nama');

    $raw_data = DB::table('perwira_alker_brand As b')
    ->join('perwira_alkerList As a', 'b.id', '=', 'a.id_alker')
    ->join('perwira_alker As c', 'b.alker_id', '=', 'c.id')
    ->select('a.*', 'b.merk As merk_brand', 'b.nama As nama_brand', 'c.alker', 'c.aset', 'c.spek', DB::RAW("TIMESTAMPDIFF(MICROSECOND, a.received_to_user_date, NOW() ) AS microsecond_diff") )
    ->whereNotNull('a.received_to_user_date');

    if($witel)
    {
      $raw_data->Where('a.witel', $witel);
      $raw_alker_list->Where('a.witel', $witel);
    }

    if($unit)
    {
      $raw_data->WhereIn('a.unit', $unit);
      $raw_alker_list->WhereIn('a.unit', $unit);
    }

    if($posisi)
    {
      $raw_data->Where('a.posisi', $posisi);
      $raw_alker_list->Where('a.posisi', $posisi);
    }

    if($umur)
    {
      switch ($umur) {
        case '_6_bulan':
          $raw_data->whereRaw('DATEDIFF(CURDATE(), a.received_to_user_date) >= 6');
          $raw_alker_list->whereRaw('DATEDIFF(CURDATE(), a.received_to_user_date) >= 6');
          break;

        default:
          $raw_alker_list->whereRaw('DATEDIFF(CURDATE(), a.received_to_user_date) >= 12');
          $raw_data->whereRaw('DATEDIFF(CURDATE(), a.received_to_user_date) >= 12')
          ->WhereIn('c.id', [40, 41, 63, 82, 90, 93, 105, 107]);
          break;
      }
    }

    $temp_raw_alker = clone $raw_alker_list;

    $recordsTotal = $temp_raw_alker->get()->count();

    if($search)
    {
      $raw_alker_list->Where(function($join) use($search){
        $join->Where('a.nama', 'like', '%'.$search.'%')
        ->OrWhere('a.witel', 'like', '%'.$search.'%')
        ->OrWhere('a.unit', 'like', '%'.$search.'%')
        ->OrWhere('a.posisi', 'like', '%'.$search.'%');
      });
    }

    if($order_column != 0)
    {
      if($order_column == 5)
      {
        $raw_data->orderBy(DB::RAW("TIMESTAMPDIFF(MICROSECOND, a.received_to_user_date, NOW() )"), $order_dir);
        $raw_alker_list->orderBy(DB::RAW("TIMESTAMPDIFF(MICROSECOND, a.received_to_user_date, NOW() )"), $order_dir);
      }
      else
      {
        $raw_data->orderBy($array_column[$order_column], $order_dir);
        $raw_alker_list->orderBy($array_column[$order_column], $order_dir);
      }
    }
    else
    {
      $raw_data
      ->orderBy(DB::RAW("TIMESTAMPDIFF(MICROSECOND, a.received_to_user_date, NOW() )"), 'DESC');
      // ->orderBy('a.witel' ,'ASC')
      // ->orderBy('a.nama' ,'ASC')
      // ->orderBy('a.unit' ,'ASC')
      // ->orderBy('a.posisi' ,'ASC');

      $raw_alker_list
      ->orderBy(DB::RAW("TIMESTAMPDIFF(MICROSECOND, a.received_to_user_date, NOW() )"), 'DESC');
      // ->orderBy('a.witel' ,'ASC')
      // ->orderBy('a.nama' ,'ASC')
      // ->orderBy('a.unit' ,'ASC')
      // ->orderBy('a.posisi' ,'ASC');
    }

    $raw_data = $raw_data->get();

    $temp_raw_alker = clone $raw_alker_list;

    $recordsFiltered = $temp_raw_alker->GroupBy('nama')->get()->count();

    $raw_alker_list = $raw_alker_list
    ->skip($start)
    ->take($length)
    ->get();

    $final_data = $fresh_data = [];

    function processRawData($v2, &$final_data, &$start, $search = null)
    {
      $final_data[$v2->nama]['no']     = $start;
      $final_data[$v2->nama]['nama']   = $v2->nama;
      $final_data[$v2->nama]['unit']   = $v2->unit;
      $final_data[$v2->nama]['posisi'] = $v2->posisi;
      $final_data[$v2->nama]['witel']  = $v2->witel;

      $final_data[$v2->nama]['barang'][$v2->nama_brand]['nama_brand'] = $v2->nama_brand;
      $final_data[$v2->nama]['barang'][$v2->nama_brand]['merk_brand'] = $v2->merk_brand;
      $final_data[$v2->nama]['barang'][$v2->nama_brand]['alker']      = $v2->alker;
      $final_data[$v2->nama]['barang'][$v2->nama_brand]['aset']       = $v2->aset;
      $final_data[$v2->nama]['barang'][$v2->nama_brand]['spek']       = $v2->spek;
      $final_data[$v2->nama]['barang'][$v2->nama_brand]['tanggal']    = $v2->received_to_user_date;
      $final_data[$v2->nama]['barang'][$v2->nama_brand]['microtime']  = $v2->microsecond_diff;

      $formats = [
        'm/d/Y H:i',
        'm/d/Y H:i:s',
        'n/j/Y H:i',
        'n/j/Y H:i:s',
        'Y-m-d H:i',
        'Y-m-d H:i:s',
      ];

      $age = '0 Hari';

      foreach ($formats as $format)
      {
        try
        {
          $date = DateTime::createFromFormat($format, $v2->received_to_user_date . ' ' . $v2->received_time);

          if ($date !== false)
          {
            $age = '';

            $today = new DateTime();
            $interval = $today->diff($date);

            if ($interval->y > 0)
            {
              $age .= $interval->format('%y Tahun');
            }

            if ($interval->m > 0)
            {
              $age .= ($age !== '') ? ' ' : '';
                $age .= $interval->format('%m Bulan');
            }

            if ($interval->d > 0)
            {
              $age .= ($age !== '') ? ' ' : '';
              $age .= $interval->format('%d Hari');
            }

            break;
          }
        }
        catch (Exception $e)
        {
        }
      }

      $final_data[$v2->nama]['barang'][$v2->nama_brand]['umur'] = $age;
    }

    foreach ($raw_data as $v2)
    {
      $matchingElements = array_filter($raw_alker_list->toArray(), function ($v1) use ($v2)
      {
        return $v2->nama == $v1->nama && $v2->unit == $v1->unit && $v2->posisi == $v1->posisi && $v2->witel == $v1->witel;
      });

      if (!empty($matchingElements) )
      {
        $fresh_data[key($matchingElements)] = reset($matchingElements);
      }
    }

    foreach ($fresh_data as $v1)
    {
      ++$start;

      $filtered_data = array_filter($raw_data->toArray(), function ($v2) use ($v1, $search)
      {
        return ($v2->nama == $v1->nama && $v2->unit == $v1->unit && $v2->posisi == $v1->posisi && $v2->witel == $v1->witel) || ($search &&
        !empty(array_filter([$v2->merk_brand, $v2->nama_brand, $v2->alker, $v2->aset, $v2->spek], function ($val) use ($search)
          {
            return strpos($val, $search) !== false;
          }) ) && !isset($final_data[$v2->nama])
        );
      });

      foreach ($filtered_data as $v2)
      {
        processRawData($v2, $final_data, $start, $search);
      }
    }

    $final_data = array_values($final_data);

    $response = [
      'draw'            => $draw,
      'recordsTotal'    => $recordsTotal,
      'recordsFiltered' => $recordsFiltered,
      'data'            => $final_data,
    ];

    return $response;
  }

  public static function get_jenis_select($isi, $jenis)
  {
    $data = DB::table('perwira_alkerList');

    switch($jenis)
    {
      case 'witel':
          $re_jenis = 'witel';
        break;
      case 'unit':
          $re_jenis = 'unit';
        break;
      default:
          $re_jenis = 'posisi';
        break;
    }

    if($isi)
    {
      $data->Where($re_jenis, 'LIKE', "%$isi%");
    }

    return $data
    ->select("$re_jenis as id", "$re_jenis as text")
    ->GroupBy($re_jenis)
    ->get();
  }
}