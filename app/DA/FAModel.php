<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Session;

date_default_timezone_set("Asia/Makassar");
class FAModel{

  public static function detail_brevert($id)
  {
    return DB::table('perwira_brevert_user')
    ->leftjoin('user_perwira', 'perwira_brevert_user.noktp', '=', 'user_perwira.noktp')
    ->select('user_perwira.nama', 'user_perwira.nokk', 'user_perwira.noktp', 'perwira_brevert_user.*')
    ->where('perwira_brevert_user.id_brevert', $id)
    ->get();
  }

  public static function get_brevert_title($id)
  {
    return DB::table('perwira_breving')
    ->where('id', $id)->first();
  }

  public static function get_list($jenis)
  {
    if($jenis == 'avail')
    {
      $sql_main = DB::table('user_perwira')
      ->leftjoin('perwira_brevert_user', 'perwira_brevert_user.noktp', '=', 'user_perwira.noktp')
      ->leftjoin('perwira_breving', 'perwira_breving.id', 'perwira_brevert_user.id_brevert')
      ->select('perwira_breving.*', 'user_perwira.nama')
      ->where('tgl_start', '<', DB::raw("DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s')") );

      if(session('auth')->perwira_level == 12)
      {
        return $sql_main->where('perwira_brevert_user.noktp', session('auth')->noktp)->get();
      }
      else
      {
        return $sql_main->get();
      }
    }
    elseif($jenis == 'berlangsung')
    {
      $sql_main = DB::table('user_perwira')
      ->leftjoin('perwira_brevert_user', 'perwira_brevert_user.noktp', '=', 'user_perwira.noktp')
      ->leftjoin('perwira_breving', 'perwira_breving.id', 'perwira_brevert_user.id_brevert')
      ->select('perwira_breving.*')
      ->where('tgl_end', '>=', DB::raw("DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s')") )
      ->GroupBy('perwira_breving.id');

      if(session('auth')->perwira_level == 12)
      {
        return $sql_main->where('perwira_brevert_user.noktp', session('auth')->noktp)->get();
      }
      else
      {
        return $sql_main->get();
      }
    }
    elseif($jenis == 'done')
    {
      $sql_main = DB::table('user_perwira')
      ->leftjoin('perwira_brevert_user', 'perwira_brevert_user.noktp', '=', 'user_perwira.noktp')
      ->leftjoin('perwira_breving', 'perwira_breving.id', 'perwira_brevert_user.id_brevert')
      ->select('perwira_breving.*')
      ->where('tgl_end', '<', DB::raw("DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s')") )
      ->GroupBy('perwira_breving.id');

      if(session('auth')->perwira_level == 12)
      {
        return $sql_main->where('perwira_brevert_user.noktp', session('auth')->noktp)->get();
      }
      else
      {
        return $sql_main->get();
      }
    }
  }

  public static function save_breving($req)
  {
    $auth = Session::get('auth');

    return DB::transaction( function() use($req, $auth)
    {
      // dd($req->all() );
        $date = array_map('trim', explode('-', $req->tgl) );
        $id = DB::table('perwira_breving')->insertGetId([
            'jenis' => $req->jenis,
            'tgl_start' => $date[0],
            'tgl_end' => $date[1],
            'title' => $req->title,
            'created_by' => $auth->id_user
        ]);

        foreach($req->Nik as $val)
        {
          DB::table('perwira_brevert_user')->insert([
            'id_brevert' => $id,
            'noktp' => $val
        ]);
        }

        $msg['msg'] = [
            'type' => 'success',
            'text' => "Pembuatan Acara $req->jenis Dengan Judul $req->title Berhasil!"
        ];

        return $msg;
    });
  }

  public static function check_briefing($id, $ktp)
  {
    return DB::transaction( function() use($id, $ktp)
    {
      $check_data =DB::table('perwira_brevert_user')->where([
        ['id_brevert', $id],
        ['noktp', $ktp]
      ])->first();

      $get_title = DB::table('perwira_breving')->where('id', $id)->first();
      if($check_data)
      {
        if(!$check_data->hadir)
        {

          DB::table('perwira_brevert_user')->where([
            ['id_brevert', $id],
            ['noktp', $ktp]
          ])->update([
            'hadir' => date('Y-m-d H:i:s')
          ]);

          $msg['msg'] = [
            'type' => 'success',
            'text' => "Berhasil Absen $get_title->title !"
          ];
        }
        else
        {
          $msg['msg'] = [
            'type' => 'info',
            'text' => "Anda Sudah Masuk Pelatihan $get_title->title !"
          ];
        }
      }
      else
      {
        $msg['msg'] = [
          'type' => 'danger',
          'text' => "Anda Tidak Terdaftar di Pelatihan $get_title->title !"
        ];
      }

      return $msg;
    });
  }

}