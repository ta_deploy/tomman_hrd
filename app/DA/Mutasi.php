<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;

class Mutasi
{
    const TABLE = 'pelurusan_job_position';
    public static function savePengajuanMutasi($req)
    {
    if(!empty($req->id) ){
    //update
        DB::transaction(function() use($req) {
            DB::table(self::TABLE)->where('id', $req->id)->update([
                'id_naker' => $req->nama,
                'id_job_position' => $req->objectid,
                'tgl_pengajuan' => $req->tgl,
                'jenis' => $req->jenis,
                'id_sto'=>$req->sto2,
                'nik_atasan'=>$req->nik2,
                'status_pengajuan' => 1,
                'dibuat' =>$req->nik_dibuat,
                'disetujui' =>$req->nik_disetujui,
                'diketahui' => $req->nik_diketahui
            ]);
            //kembalikan  status job sebelumnya supaya bisa dipilih lagi
            DB::table('job_position')->where('id', $req->objectid_bantu)->update([
                'status' => 0
            ]);
            //update status job supaya tidak bisa dipilih lagi
            DB::table('job_position')->where('id', $req->objectid)->update([
                'status' => 1
            ]);
        });
    }else {
        DB::transaction(function() use($req) {
            DB::table(self::TABLE)->insert([
                'id_naker' => $req->nama,
                'id_job_position' => $req->objectid,
                'tgl_pengajuan' => $req->tgl,
                'jenis' => $req->jenis,
                'id_sto'=>$req->sto2,
                'nik_atasan'=>$req->nik2,
                'status_pengajuan' => 1,
                'dibuat' =>$req->nik_dibuat,
                'disetujui' =>$req->nik_disetujui,
                'diketahui' => $req->nik_diketahui
            ]);

            //update status job supaya tidak bisa dipilih lagi
            DB::table('job_position')->where('id', $req->objectid)->update([
                'status' => 1
            ]);
        });
    }
    }
    public static function deletePengajuan($id)
    {
        DB::transaction(function() use($id) {
            //cari id job
            $data= DB::table(self::TABLE)
                ->where('id',$id)
                ->first();
            //kembalikan status job jadi 1
            DB::table('job_position')->where('id', $data->id_job_position)->update([
                    'status' => 0
                ]);
            //hapus pengajuan
            DB::table(self::TABLE)
               ->where('id', $id)
               ->delete();
        });
    }

     public static function getListPengajuan()
    {
         return DB::select('select pelurusan_job_position.id as id, pelurusan_job_position.jenis as jenis,
            pelurusan_job_position.id_naker as id_naker,karyawan.nik as nik,karyawan.nama as nama,
            job_position_lama.regional as regional_lama,job_position_lama.bizpart_id as bizpart_id_lama,
            witel_lama.nama_witel as witel_lama,
            loker_lama.nama_loker as loker_lama,
            job_position_lama.object_id as object_id_lama,job_position_lama.position_name as position_name_lama,
            job_position_lama.direktorat as direktorat_lama, job_position_lama.unit as unit_lama,
            job_position_lama.sub_unit as sub_unit_lama,job_position_lama.group as group_lama,
            job_position_lama.sub_group as sub_group_lama, job_position_lama.group_fungsi as group_fungsi_lama,

            job_position_baru.regional as regional_baru,job_position_baru.bizpart_id as bizpart_id_baru,
            witel_baru.nama_witel as witel_baru,
            loker_baru.nama_loker as loker_baru,
            job_position_baru.object_id as object_id_baru,job_position_baru.position_name as position_name_baru,
            job_position_baru.direktorat as direktorat_baru, job_position_baru.unit as unit_baru,
            job_position_baru.sub_unit as sub_unit_baru,job_position_baru.group as group_baru,
            job_position_baru.sub_group as sub_group_baru, job_position_baru.group_fungsi as group_fungsi_baru

            from pelurusan_job_position join karyawan on pelurusan_job_position.id_naker=karyawan.id
            left join pelurusan_job_position pelurusan_job_position_lama
                on pelurusan_job_position.id_naker=pelurusan_job_position_lama.id_naker
                and pelurusan_job_position_lama.status=1
            left join job_position job_position_lama on pelurusan_job_position_lama.id_job_position=job_position_lama.id
            left join loker loker_lama on job_position_lama.id_loker=loker_lama.id
            left join witel witel_lama on loker_lama.witel_id=witel_lama.id

            left join job_position job_position_baru on pelurusan_job_position.id_job_position=job_position_baru.id
            left join loker loker_baru on job_position_baru.id_loker=loker_baru.id
            left join witel witel_baru on loker_baru.witel_id=witel_baru.id

            where pelurusan_job_position.status_pengajuan=1'
            );
    }

    public static function getListPersetujuan()
    {
        return DB::table(self::TABLE)
            ->select('pelurusan_job_position.id as id','pelurusan_job_position.jenis as jenis',
                'pelurusan_job_position.id_naker as id_naker',
                'karyawan.nik as nik','karyawan.nama as nama',
                'pelurusan_job_position.tgl_persetujuan as tgl_persetujuan',
                'job_position.regional as regional','job_position.bizpart_id as bizpart_id',
                'witel.nama_witel as witel', 'pelurusan_job_position.status_pengajuan as status_pengajuan',
                'loker.nama_loker as loker', 'job_position.id as idjob',
                'job_position.object_id as object_id','job_position.position_name as position_name',
                'job_position.direktorat as direktorat',
                'job_position.unit as unit','job_position.sub_unit as sub_unit','job_position.group as group',
                'job_position.sub_group as sub_group', 'job_position.group_fungsi as group_fungsi')
            ->leftJoin('karyawan','pelurusan_job_position.id_naker','=','karyawan.id')
            ->leftJoin('job_position', 'pelurusan_job_position.id_job_position', '=', 'job_position.id')
            ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
            ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
            ->whereIn('pelurusan_job_position.status_pengajuan',[2,3])
            ->where('pelurusan_job_position.status',1)
            ->orderBy('pelurusan_job_position.id','desc')
            ->get();
    }

    public static function getPengajuanById($id)
    {
        return DB::table(self::TABLE)
            ->select('pelurusan_job_position.id as id','pelurusan_job_position.jenis as jenis',
                'pelurusan_job_position.id_naker as id_naker','pelurusan_job_position.tgl_pengajuan as tgl_pengajuan',
                'pelurusan_job_position.id_sto as id_sto','sto.sto as sto',
                'pelurusan_job_position.nik_atasan as nik_atasan',
                'pelurusan_job_position.dibuat as dibuat',
                'pelurusan_job_position.disetujui as disetujui',
                'pelurusan_job_position.diketahui as diketahui',
                'karyawan.nik as nik','karyawan.nama as nama',
                'pelurusan_job_position.status_pengajuan as status_pengajuan',
                'job_position.regional as regional','job_position.bizpart_id as bizpart_id',
                'witel.nama_witel as witel','job_position.id_loker as id_loker',
                'loker.nama_loker as loker',
                'job_position.id as idjob','job_position.teritory as teritory',
                'job_position.object_id as object_id','job_position.position_name as position_name','job_position.direktorat as direktorat',
                'job_position.unit as unit','job_position.sub_unit as sub_unit','job_position.group as group',
                'job_position.sub_group as sub_group', 'job_position.group_fungsi as group_fungsi')
            ->leftJoin('karyawan','pelurusan_job_position.id_naker','=','karyawan.id')
            ->leftJoin('job_position', 'pelurusan_job_position.id_job_position', '=', 'job_position.id')
            ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
            ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
            ->leftJoin('sto','pelurusan_job_position.id_sto','=','sto.id')
            ->where('pelurusan_job_position.id',$id)
            ->first();
    }
    public static function getPengajuanByIdNaker($nik)
    {
        return DB::table(self::TABLE)
            ->select('pelurusan_job_position.id as id',
                'pelurusan_job_position.id_naker as id_naker',
                'karyawan.nik as nik','karyawan.nama as nama',
                'karyawan.nik_atasan as nik_atasan','karyawan.bpjs as bpjs',
                'karyawan.id_sto as id_sto','sto.sto as sto',
                'pelurusan_job_position.status_pengajuan as status_pengajuan',
                'job_position.regional as regional','job_position.bizpart_id as bizpart_id',
                'witel.nama_witel as witel','job_position.id_loker as id_loker',
                'loker.nama_loker as loker',
                'job_position.id as idjob','job_position.teritory as teritory',
                'job_position.object_id as object_id','job_position.position_name as position_name',
                'job_position.direktorat as direktorat',
                'job_position.unit as unit','job_position.sub_unit as sub_unit','job_position.group as group',
                'job_position.sub_group as sub_group', 'job_position.group_fungsi as group_fungsi')
            ->leftJoin('karyawan','pelurusan_job_position.id_naker','=','karyawan.id')
            ->leftJoin('sto','karyawan.id_sto','=','sto.id')
            ->leftJoin('job_position', 'pelurusan_job_position.id_job_position', '=', 'job_position.id')
            ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
            ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
            ->where('pelurusan_job_position.id_naker',$nik)
            ->where('pelurusan_job_position.status',1)
            ->first();
    }
     public static function savePersetujuanMutasi($req)
    {
        DB::transaction(function() use($req) {
        if ($req->status=='2'){

            $idstolama=0; if (!empty($req->idstolama) ){$idstolama=$req->idstolama;}
            $niklama=null; if (!empty($req->niklama) ){$niklama=$req->niklama;}

            //update pelurusan job position
            DB::table(self::TABLE)->where('id', $req->id)->update([
                    'tgl_persetujuan' => $req->tgl,
                    'status' => 1,
                    'status_pengajuan' => $req->status,
                    'id_sto'=>$idstolama,
                    'nik_atasan'=>$niklama
                ]);
            DB::table(self::TABLE)->where('id', $req->idjoblama)->update([
                    'status' => 0
                ]);
            //update, jika ada pelurusan unmap
            $unmap= DB::table('pelurusan_job_position')
                    ->select('id as id')
                    ->where('id_naker', $req->nik)
                    ->where('status_pengajuan',4)
                    ->orderBy('id','desc')
                    ->first();
            if (!empty($unmap) ) {
                DB::table(self::TABLE)->where('id', $unmap->id)->update([
                        'status_pengajuan' => 6
                    ]);
            }
            //karyawan, update nik atasan & id sto
            DB::table('karyawan')->where('id', $req->nik)->update([
                    'id_sto'=>$req->idstobaru,
                    'nik_atasan'=>$req->nikbaru
                ]);
            //job position, update status
            //jika naker lama & tidak unmap
            if (!empty($req->idjobpositionlama) && empty($unmap) ){
                DB::table('job_position')->where('id', $req->idjobpositionlama)->update([
                        'status' => 0,
                        'id_naker' =>0
                    ]);
            }
            //**status job baru sudah diubah saat pengajuan tetap diupdate disini
            DB::table('job_position')->where('id', $req->idjobpositionbaru)->update([
                    'status' => 1,
                    'id_naker' => $req->nik
                ]);

        }else if ($req->status=='3'){
            DB::table(self::TABLE)->where('id', $req->id)->update([
                    'tgl_persetujuan' => $req->tgl,
                    'status' => 0,
                    'status_pengajuan' => $req->status
                ]);
            //kembalikan status job baru jadi 0
            DB::table('job_position')->where('id', $req->idjobpositionbaru)->update([
                    'status' => 0
                ]);
        }
        if ($req->hasFile('scan') ) {
                //dd($input);
                $path = public_path().'/upload/PersetujuanMutasi/'.$req->id.'/';
                if (!file_exists($path) ) {
                  if (!mkdir($path, 0770, true) )
                    return 'gagal menyiapkan folder file';
                }
                $file = $req->file('scan');
                try {
                  $moved = $file->move("$path",$req->file('scan')->getClientOriginalName() );
                }
                catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                  return 'gagal menyimpan file ';
                }
            }
        });
    }

    public static function deletePersetujuanMutasi($id,$nik,$status_pengajuan,$file)
    {
        DB::transaction(function() use($id,$nik,$status_pengajuan,$file) {
        if ($status_pengajuan=='2'){
            //prepare
            $idlama= DB::table('pelurusan_job_position')
                    ->select('id as id','id_job_position as idjoblama')
                    ->where('id_naker', $nik)
                    ->whereIn('status_pengajuan',[0,2,5])
                    ->orderBy('id','desc')
                    ->offset(1)
                    ->limit(1)
                    ->first();
            $idbaru= DB::table('pelurusan_job_position')
                    ->select('id_job_position as idjobbaru','id_sto as id_sto','.nik_atasan as nik_atasan')
                    ->where('id',$id)
                    ->first();
            $karyawan = DB::table('karyawan')
                    ->select('id_sto as id_sto','nik_atasan as nik_atasan')
                    ->where('id',$nik)
                    ->first();
            //update pelurusan, kembalikan status_pengajuan ke pengajuan (1)
            DB::table(self::TABLE)->where('id', $id)->update([
                    'status' => 0,
                    'status_pengajuan' => 1,
                    'id_sto'=>$karyawan->id_sto,
                    'nik_atasan'=>$karyawan->nik_atasan
                ]);
            DB::table(self::TABLE)->where('id', $idlama->id)->update([
                    'status' => 1
                ]);
            //karyawan, update nik atasan & id sto
            DB::table('karyawan')->where('id', $nik)->update([
                    'id_sto'=>$idbaru->id_sto,
                    'nik_atasan'=>$idbaru->nik_atasan
                ]);
            //update if unmap
            $unmap= DB::table('pelurusan_job_position')
                    ->select('id as id')
                    ->where('id_naker', $nik)
                    ->where('status_pengajuan',6)
                    ->orderBy('id','desc')
                    ->first();
            if (!empty($unmap) ) {
                DB::table(self::TABLE)->where('id', $unmap->id)->update([
                        'status_pengajuan' => 4
                    ]);
            }
            //job position update status
            if ($idlama->idjoblama!=0 && empty($unmap) ){
                DB::table('job_position')->where('id', $idlama->idjoblama)->update([
                        'status' => 1,
                        'id_naker' => $nik
                    ]);
            }
             DB::table('job_position')->where('id', $idbaru->idjobbaru)->update([
                    'status' => 0,
                    'id_naker' => 0
                ]);
         }
         else if ($status_pengajuan=='3') {
            DB::table(self::TABLE)->where('id', $id)->update([
                    'status_pengajuan' => 1
                ]);
         }
          unlink(public_path()."/upload/PersetujuanMutasi/".$id."/".$file);
        });
     }
    public static function getListCv($req)
    {
        return DB::table(self::TABLE)
            ->select('pelurusan_job_position.id as id','pelurusan_job_position.jenis as jenis',
                'pelurusan_job_position.tgl_persetujuan as tgl',
                'job_position.regional as regional','job_position.bizpart_id as bizpart_id',
                'witel.nama_witel as witel','loker.nama_loker as psa',
                'job_position.object_id as object_id','job_position.position_name as position_name',
                'job_position.direktorat as direktorat','job_position.teritory as teritory',
                'job_position.unit as unit','job_position.sub_unit as sub_unit','job_position.group as group',
                'job_position.sub_group as sub_group', 'job_position.group_fungsi as group_fungsi')
            ->leftJoin('karyawan','pelurusan_job_position.id_naker','=','karyawan.id')
            ->leftJoin('job_position', 'pelurusan_job_position.id_job_position', '=', 'job_position.id')
            ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
            ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
            ->where('pelurusan_job_position.id_naker', $req->nama)
            ->whereIn('pelurusan_job_position.status_pengajuan',[0,2])
            ->orderBy('pelurusan_job_position.id','desc')
            ->get();
    }

     public static function getCetakMutasi($id)
    {
         return DB::table(self::TABLE)
            ->select('pelurusan_job_position.id as id', 'pelurusan_job_position.tgl_pengajuan as tgl_pengajuan',
                    'pelurusan_job_position.jenis as jenis',
                    'pelurusan_job_position.id_naker as id_naker','karyawan.nik as nik','karyawan.nama as nama',

                    'ttd_dibuat.nik as nik_dibuat','ttd_dibuat.nama as nama_dibuat','ttd_dibuat.jabatan as jabatan_dibuat',
                    'ttd_disetujui.nik as nik_disetujui','ttd_disetujui.nama as nama_disetujui','ttd_disetujui.jabatan as jabatan_disetujui',
                    'ttd_diketahui.nik as nik_diketahui','ttd_diketahui.nama as nama_diketahui','ttd_diketahui.jabatan as jabatan_diketahui',

                    'loker_lama.nama_loker as loker_lama',
                    'job_position_lama.object_id as object_id_lama',
                    'job_position_lama.position_name as position_name_lama',
                    'loker_baru.nama_loker as loker_baru',
                    'job_position_baru.object_id as object_id_baru','job_position_baru.position_name as position_name_baru')

            ->leftJoin('karyawan','pelurusan_job_position.id_naker','=','karyawan.id')
            ->leftJoin('ttd AS ttd_dibuat','pelurusan_job_position.dibuat','=','ttd_dibuat.id')
            ->leftJoin('ttd AS ttd_disetujui','pelurusan_job_position.disetujui','=','ttd_disetujui.id')
            ->leftJoin('ttd AS ttd_diketahui','pelurusan_job_position.diketahui','=','ttd_diketahui.id')

            ->leftJoin('pelurusan_job_position AS pelurusan_job_position_lama','pelurusan_job_position.id_naker','=','pelurusan_job_position_lama.id_naker')
            ->leftJoin('job_position AS job_position_lama','pelurusan_job_position_lama.id_job_position','=','job_position_lama.id')
            ->leftJoin('loker AS loker_lama','job_position_lama.id_loker','=','loker_lama.id')
            ->leftJoin('job_position AS job_position_baru','pelurusan_job_position.id_job_position','=','job_position_baru.id')
            ->leftJoin('loker AS loker_baru','job_position_baru.id_loker','=','loker_baru.id')

            ->where('pelurusan_job_position.id',$id)
            ->where('pelurusan_job_position_lama.status',1)
            ->first();
    }

    public static function unmap ($id,$nik,$id_loker)
    {
       DB::transaction(function() use($id,$nik,$id_loker) {
            DB::table('job_position')->where('id',$id)
                ->update([
                    'status' => 0
                ]);

            DB::table(self::TABLE)->insert([
                'id_naker' => $nik,
                'id_job_position' => $id,
                'id_sto'=>$id_loker,
                'status_pengajuan' => 4,
                'status' =>0
            ]);

         });
    }
     //cancel unmap
    public static function cancelUnmap ($idjob)
    {
        DB::table('job_position')->where('id',$idjob)
            ->update([
                'status' => 1
            ]);
    }

    public static function getlistUnmap($req)
    {
        if (!empty($req) ){ $witel=$req->witel; }
        else { $witel=1; }

        if (empty($req) || $req->cari==1){
            return DB::table(self::TABLE)
                ->select('pelurusan_job_position.id as id',
                    'pelurusan_job_position.id_naker as id_naker',
                    'karyawan.nik as nik','karyawan.nama as nama',
                    'witel.nama_witel as witel',
                    'loker.nama_loker as loker',
                    'job_position.id as idjob','job_position.id_loker as id_loker',
                    'job_position.status',
                    'job_position.object_id as object_id','job_position.position_name as position_name')
                ->leftJoin('job_position', 'pelurusan_job_position.id_job_position', '=', 'job_position.id')
                ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
                ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
                ->leftJoin('karyawan', 'pelurusan_job_position.id_naker', '=', 'karyawan.id')
                ->where('pelurusan_job_position.status_pengajuan',4)
                ->where('loker.witel_id',$witel)
                ->get();
         }
         else if ($req->cari==2){
            return DB::table(self::TABLE)
                ->select('pelurusan_job_position.id as id',
                    'pelurusan_job_position.id_naker as id_naker',
                    'karyawan.nik as nik','karyawan.nama as nama',
                    'witel.nama_witel as witel',
                    'loker.nama_loker as loker',
                    'job_position.id as idjob','job_position.id_loker as id_loker',
                    'job_position.status',
                    'job_position.object_id as object_id','job_position.position_name as position_name')
                ->leftJoin('job_position', 'pelurusan_job_position.id_job_position', '=', 'job_position.id')
                ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
                ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
                ->leftJoin('karyawan', 'pelurusan_job_position.id_naker', '=', 'karyawan.id')
                ->where('pelurusan_job_position.status_pengajuan',4)
                ->where('job_position.id_loker',$req->psa)
                ->get();
         }
    }
}