<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;

class Position
{
    const TABLE = 'position';
    public static function getlistPositionNaker()
    {  
        return DB::table(self::TABLE)
            ->select('position.id as id',
                    'position.id_naker as id_naker','karyawan.nik as nik','karyawan.nama as nama',
                    'loker.nama_loker as nama_loker','position.id_loker as id_loker', 
                    'position.posisi as posisi','position.group as group','position.status as status',
                    'position.loker as loker','position.teritory as teritory','position.sto as sto',
                    'position.keterangan as keterangan')
            ->leftJoin('loker', 'position.id_loker', '=', 'loker.id')
            ->leftJoin('karyawan','position.id_naker','=','karyawan.id')
            ->get();
    } 
    public static function editNakerPosition($id,$req)
        {
            DB::table(self::TABLE)->where('id', $id)->update([
                'id_naker' => $req->naker
            ]);  
        } 
     public static function getCountPosition()
    {
         return DB::select('select posisi,COUNT(*) as kuota,
                            count(case id_naker when 0 then null else 1 end) as isi, 
                            count(case id_naker when 0 then 1 else null end) as kekurangan 
                            from position GROUP BY posisi');     
    } 
     public static function getChart()
    {  
        return DB::table(self::TABLE)
            ->select('position.id as id',
                    'position.id_naker as id_naker','karyawan.nik as nik','karyawan.nama as nama',
                    'loker.nama_loker as nama_loker','position.id_loker as id_loker', 
                    'position.posisi as posisi','position.group as group','position.status as status',
                    'position.loker as loker','position.teritory as teritory','position.sto as sto',
                    'position.keterangan as keterangan','position.id_atasan as id_atasan')
            ->leftJoin('loker', 'position.id_loker', '=', 'loker.id')
            ->leftJoin('karyawan','position.id_naker','=','karyawan.id')
            ->limit(14)
            ->orderBy('position.id','asc')
            ->get();
    }     
}