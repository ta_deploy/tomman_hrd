<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\PublicController;
use Session;

date_default_timezone_set("Asia/Makassar");

class PublicModel
{
    public static function check_data($req)
    {
        return DB::table('perwira_recruitment')->where('noktp', $req->input('noktp'))->first();
    }

    public static function save_data($req)
    {
        //upload file
        DB::transaction(function () use ($req) {
            $id = $req->input('noktp');
            self::handleFileUpload($req, $id);

            $smartphone = $laptop = $sim = $motor = 0;
            if (@$req->ket['smartphone'] == 1)
            {
                $smartphone = 1;
            }
            if (@$req->ket['laptop'] == 1)
            {
                $laptop = 1;
            }
            if (@$req->ket['sim'] == 1)
            {
                $sim = 1;
            }
            if (@$req->ket['motor'] == 1)
            {
                $motor = 1;
            }

            $microsoft_office = $autocad = $web_dev = $design_grafis = 0;

            if (@$req->ket['microsoft_office'] == 1)
            {
                $microsoft_office = 1;
            }
            if (@$req->ket['autocad'] == 1)
            {
                $autocad = 1;
            }
            if (@$req->ket['web_dev'] == 1)
            {
                $web_dev = 1;
            }
            if (@$req->ket['design_grafis'] == 1)
            {
                $design_grafis = 1;
            }

            DB::table('perwira_recruitment')->insert([
                'nama'                 => $req->input('nama'),
                'jk'                   => $req->input('jk'),
                'agama'                => $req->input('agama'),
                'tempat_lahir'         => $req->input('tempat_lahir'),
                'tgl_lahir'            => $req->input('tgl_lahir'),
                'goldar'               => $req->input('goldar'),
                'email'                => $req->input('email'),
                'npwp'                 => $req->input('npwp'),
                'alamat'               => $req->input('alamat'),
                'provinsi'             => $req->input('provinsi'),
                'kota'                 => $req->input('kota'),
                'kecamatan'            => $req->input('kecamatan'),
                'kelurahan'            => $req->input('kelurahan'),
                'lokasi_kerja'         => $req->input('lokasi_kerja'),
                'no_telp'              => $req->input('no_telp'),
                'no_wa'                => $req->input('no_wa'),
                'noktp'                => $req->input('noktp'),
                'nokk'                 => $req->input('nokk'),
                'status_perkawinan'    => $req->input('status_perkawinan'),
                'ibu'                  => $req->input('ibu'),
                'telpon_2_nm'          => $req->input('telpon_2_nm'),
                'telpon_2'             => $req->input('telpon_2'),
                'relasi'               => $req->input('relasi'),
                'lvl_pendidikan'       => $req->input('lvl_pendidikan'),
                'last_study'           => $req->input('last_study'),
                'jurusan'              => $req->input('jurusan'),
                'tgllulus'             => $req->input('tgllulus'),
                'ket_smartphone'       => $smartphone,
                'ket_laptop'           => $laptop,
                'ket_sim'              => $sim,
                'ket_motor'            => $motor,
                'ket_microsoft_office' => $microsoft_office,
                'ket_autocad'          => $autocad,
                'ket_web_dev'          => $web_dev,
                'ket_design_grafis'    => $design_grafis,
                'created_at'           => date('Y-m-d H:i:s'),
                'status_pelamar'       => 'NEW'
            ]);

            $get_provinsi = DB::table('api_provinsi')->where('id', $req->input('provinsi'))->first();

            if ($get_provinsi->id != 0)
            {
                $get_kota = DB::table('api_kota')->where('id_kota', $req->input('kota'))->first();
                $get_kecamatan = DB::table('api_kecamatan')->where('id_kecamatan', $req->input('kecamatan'))->first();
                $get_kelurahan = DB::table('api_kelurahan')->where('id_kelurahan', $req->input('kelurahan'))->first();

                $get_count_today = DB::table('perwira_recruitment')->where('created_at', 'like', date('Y-m-d') . '%')->count();
                $get_count_year = DB::table('perwira_recruitment')->where('created_at', 'like', date('Y') . '%')->count();

                $msg = "Bertambah Pelamar\n";
                $msg .= "Nama: " . $req->input('nama') . "\n";
                $msg .= "Pendidikan Terakhir: " . $req->input('lvl_pendidikan') . "\n";
                $msg .= "Nomor Telpon: " . $req->input('no_telp') . "\n";
                $msg .= "Lokasi Kerja: " . $req->input('lokasi_kerja') . "\n";
                $msg .= "Provinsi: " . $get_provinsi->nama . "\n";
                $msg .= "Kota: " . $get_kota->nama . "\n";
                $msg .= "Kecamatan: " . $get_kecamatan->nama . "\n";
                $msg .= "Kelurahan: " . $get_kelurahan->nama . "\n";
                $msg .= "Total Pelamar Hari Ini: $get_count_today\n";
                $msg .= "Total Pelamar Tahun " . date("Y") . ": $get_count_year\n";

                $chat_id = ["-1002056875249", "-4115540980"];

                $botToken = '6942638646:AAHBsYXytwiZDDDG8OrSixHhLs6Ob4iiS3Y';
                $website = "https://api.telegram.org/bot" . $botToken;

                foreach ($chat_id as $k => $v)
                {
                    $params = [
                        'chat_id' => $v,
                        'parse_mode' => 'html',
                        'text' => "$msg",
                    ];

                    $ch = curl_init();
                    curl_setopt_array($ch, [
                        CURLOPT_URL => $website . '/sendMessage',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => $params,
                    ]);

                    $result = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);

                    if ($httpCode == 200)
                    {
                        $response = json_decode($result, true);
                        if ($response['ok'])
                        {
                            $id_success = 1;
                        }
                        else
                        {
                            $id_success = 0;
                        }
                    }
                    else
                    {
                        $id_success = 0;
                    }

                    DB::table('perwira_recruitment')
                    ->where('noktp', $req->input('noktp'))
                    ->update([
                        'success_send' => $id_success
                    ]);
                }

                if (in_array($req->input('lokasi_kerja'), ['Kaltim - Balikpapan', 'Kalsel - Banjarmasin']) &&
                    $req->input('jk') == 'Laki-Laki')
                {
                    self::sendWhatsapp($req->input('lokasi_kerja'), $req->input('nama'), $req->input('no_wa'));
                }
            }
        });
    }

    public static function check_notif_recruitment()
    {
        $data = DB::table('perwira_recruitment')->where('success_send', 0)->get();

        if ($data->isEmpty())
        {
            echo "No data found";
        }
        else
        {
            foreach ($data as $k => $v)
            {
                $get_provinsi = DB::table('api_provinsi')->where('id', $v->provinsi)->first();

                if (in_array($get_provinsi->id, [61, 62, 63, 64, 65]))
                {
                    $get_kota = DB::table('api_kota')->where('id_kota', $v->kota)->first();
                    $get_kecamatan = DB::table('api_kecamatan')->where('id_kecamatan', $v->kecamatan)->first();
                    $get_kelurahan = DB::table('api_kelurahan')->where('id_kelurahan', $v->kelurahan)->first();

                    $get_count_today = DB::table('perwira_recruitment')->where('created_at', 'like', date('Y-m-d') . '%')->count();
                    $get_count_year = DB::table('perwira_recruitment')->where('created_at', 'like', date('Y') . '%')->count();

                    $msg = "Bertambah Pelamar\n";
                    $msg .= "Nama: " . $v->nama . "\n";
                    $msg .= "Pendidikan Terakhir: " . $v->lvl_pendidikan . "\n";
                    $msg .= "Nomor Telpon: " . $v->no_telp . "\n";
                    $msg .= "Lokasi Kerja: " . $v->lokasi_kerja . "\n";
                    $msg .= "Provinsi: " . $get_provinsi->nama . "\n";
                    $msg .= "Kota: " . $get_kota->nama . "\n";
                    $msg .= "Kecamatan: " . $get_kecamatan->nama . "\n";
                    $msg .= "Kelurahan: " . $get_kelurahan->nama . "\n";
                    $msg .= "Total Pelamar Hari Ini: $get_count_today\n";
                    $msg .= "Total Pelamar Tahun " . date("Y") . ": $get_count_year\n";

                    $chat_id = ["-1002056875249", "-4115540980"];

                    $botToken = '6942638646:AAHBsYXytwiZDDDG8OrSixHhLs6Ob4iiS3Y';
                    $website = "https://api.telegram.org/bot" . $botToken;

                    $success = false;
                    $attempt = 0;

                    while (!$success && $attempt < 5) {
                        foreach ($chat_id as $k => $v2) {
                            $params = [
                                'chat_id' => $v2,
                                'parse_mode' => 'html',
                                'text' => "$msg",
                            ];

                            $ch = curl_init();

                            curl_setopt_array($ch, [
                                CURLOPT_URL => $website . '/sendMessage',
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POST => true,
                                CURLOPT_POSTFIELDS => $params,
                            ]);

                            $result = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            curl_close($ch);

                            if ($httpCode == 200)
                            {
                                $response = json_decode($result, true);
                                if ($response['ok'])
                                {
                                    $success = true;
                                    break;
                                }
                            }

                            $attempt++;
                        }
                    }

                    if ($success)
                    {
                        DB::table('perwira_recruitment')->where('noktp', $req->input('noktp'))->update([
                            'success_send' => 1
                        ]);
                    }
                    else
                    {
                        echo 'fail at 5 times';
                    }
                }
            }
        }
    }

    private static function handleFileUpload($req, $id)
    {
        $that = new PublicController();
        foreach ($that->fileRecruitment as $namefile) {
            if ($req->hasFile($namefile)) {
                $hdd = DB::table('hdd')->first();
                $path = public_path() . '/' . $hdd->public . '/perwira/recruitment/' . $id . '/';

                if (!file_exists($path))
                {
                    if (!mkdir($path, 0770, true))
                    {
                        return 'Gagal Menyimpan File';
                    }
                }

                if ($req->hasFile($namefile))
                {
                    $file = $req->file($namefile);
                    try {

                        //save log
                        DB::table('perwira_recruitment_fileUpload')
                        ->insert([
                            'originalName' => $file->getClientOriginalName(),
                            'originalExt'  => $file->getClientOriginalExtension(),
                            'fileName'     => $namefile,
                            'ktp_no'       => $id
                        ]);

                        $filename = $namefile . '.' . strtolower($file->getClientOriginalExtension());
                        $file->move("$path", "$filename");
                    } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                        return 'Gagal Menyimpan ' . $path;
                    }
                }
            }
        }
    }

    public static function sendWhatsapp($lokasi_kerja, $to_name, $number)
    {
        if ($lokasi_kerja == 'Kaltim - Balikpapan')
        {
            $channel_id  = 'a0c52722-4d3a-4df9-a853-bf7dcd92b118';
        }
        else if ($lokasi_kerja == 'Kalsel - Banjarmasin')
        {
            $channel_id  = 'ae44f9e0-21ea-4a50-bb10-9f65df0127f1';
        }

        $template_id = '1aa2a411-298d-4577-957d-55ae07782c6c';

        $mekari = DB::connection('data_center')->table('mekari_token')->first();

        $text = DB::connection('data_center')->table('mekari_template')->where('template_id', $template_id)->first();

        $placeholders = [];
        preg_match_all('/\{\{\d+\}\}/', $text->template_text, $placeholders);
        if (!empty($placeholders[0]))
        {
            $total_var = count($placeholders[0]);
        }

        $number = str_replace('.0', '', $number);

        if (preg_replace("/[^0-9]/", "", $number))
        {
            if (strlen($number) >= 10)
            {
                if (substr($number, 0, 1) == '0')
                {
                    $number = '62'.substr($number, 1);
                }
                else if (substr($number, 0, 1) == '8')
                {
                    $number = '62'.$number;
                }

                $fields['to_number']              = $number;
                $fields['to_name']                = $to_name;
                $fields['message_template_id']    = $template_id;
                $fields['channel_integration_id'] = $channel_id;
                $fields['language']               = ['code' => 'id'];

                $temp_body[] = [
                    "key"        => 1,
                    "value"      => "variable_1",
                    "value_text" => $to_name
                ];

                $temp_body[] = [
                    "key"        => 2,
                    "value"      => "variable_2",
                    "value_text" => $number
                ];

                $fields['parameters']['body'] = $temp_body;
                $postfields = json_encode($fields, JSON_PRETTY_PRINT);

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://service-chat.qontak.com/api/open/v1/broadcasts/whatsapp/direct',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $postfields,
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'Authorization: Bearer '.$mekari->access_token
                    ),
                ));

                $response = curl_exec($curl);
                curl_close($curl);

                if ($response != null)
                {
                    $result = json_decode($response);
                    if ($result->status == 'error' || $result->status == 'failed')
                    {
                        $status = "The Number in Not Registered WhatsApp";
                    }
                    else if ($result->status == 'success')
                    {
                        $status = "Successfully Sent";
                    }
                    else
                    {
                        $status = "Undefined";
                    }

                    return $status;
                }

                sleep(1);
            }
        }
    }
}