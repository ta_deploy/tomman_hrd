<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class Karyawan
{
    const TABLE = 'karyawan';
    public static function getAll()
    {
        return DB::table(self::TABLE)->select('id as id', 'nama as text', 'mitra', 'nik')->get();
    }
    public static function get()
    {
        return DB::table(self::TABLE)
        ->where('mitra','TA')
        ->get();
    }
    public static function getList($mitra,$psa)
    {
         return DB::table(self::TABLE)
                ->select('karyawan.id as id','karyawan.nik as nik',
                          'karyawan.nama as nama','karyawan.tgl_lahir as tgl_lahir',
                          'karyawan.kota_lahir as kota_lahir','karyawan.status as status',
                          'mitra.nama as nama_mitra')
                ->leftJoin('job_position', 'job_position.id_naker', '=', 'karyawan.id')
                ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
                ->leftJoin('mitra', 'karyawan.id_mitra','=','mitra.id' )
                ->where('job_position.id_loker', $psa)
                ->where('karyawan.mitra',$mitra)
                ->get();
    }
    public static function getKaryawanCetak($psa,$kolom)
    {
         return DB::table(self::TABLE)
                ->select(DB::raw($kolom) )
                ->leftJoin('job_position', 'job_position.id_naker', '=', 'karyawan.id')
                ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
                ->where('job_position.id_loker', $psa)
                ->where('karyawan.mitra','TA')
                ->get();
    }
    public static function getById($id)
    {
        return DB::table(self::TABLE)
          ->where('id', $id)
          ->first();
    }
    public static function saveKaryawan( $req)
      {
        DB::transaction(function() use($req) {
          $tgl=''; $tgl2='';
          if ($req->statuskerja=="1"){$tgl='ojt'; $tgl2='ojt_akhir';}
          else if ($req->statuskerja=="2"){$tgl='kontrak1'; $tgl2='kontrak1_akhir';}
          else if ($req->statuskerja=="3"){$tgl='kontrak2'; $tgl2='kontrak2_akhir';}
          else if ($req->statuskerja=="4"){$tgl='pembaharuan'; $tgl2='pembaharuan_akhir';}
          else if ($req->statuskerja=="5"){$tgl='pegawai_tetap'; $tgl2='pegawai_tetap_akhir';}
          else if ($req->statuskerja=="6"){$tgl='resign'; $tgl2='resign_akhir';}

          $tgl_lahir=null;
          if (!empty($req->tgllahir) ){$tgl_lahir=$req->tgllahir;}
           $tglmulaikerja=null;
          if (!empty($req->tglmulaikerja) ){$tglmulaikerja=$req->tglmulaikerja;}
          $tglstatuskerja=null;
          if (!empty($req->tgl)&&$req->tgl!="/"){$tglstatuskerja=$req->tgl;}
          $tglstatuskerjaakhir=null;
          if (!empty($req->tgl2)&&$req->tgl2!="/"){$tglstatuskerjaakhir=$req->tgl2;}

          $tgl_lulus=null;
          if (!empty($req->tgl_lulus)&&$req->tgl_jamsostek!="0000-00-00"){$tgl_lulus=$req->tgl_lulus;}
          $tgl_jamsostek=null;
          if (!empty($req->tgl_jamsostek)&&$req->tgl_jamsostek!="0000-00-00"){$tgl_jamsostek=$req->tgl_jamsostek;}
          $tgl_bpjs=null;
          if (!empty($req->tgl_bpjs)&&$req->tgl_jamsostek!="0000-00-00"){$tgl_bpjs=$req->tgl_bpjs;}
          $tgl_bpjs_istri=null;
          if (!empty($req->tgl_bpjs_istri)&&$req->tgl_jamsostek!="0000-00-00"){$tgl_bpjs_istri=$req->tgl_bpjs_istri;}
          $tgl_bpjs_anak1=null;
          if (!empty($req->tgl_bpjs_anak1)&&$req->tgl_jamsostek!="0000-00-00"){$tgl_bpjs_anak1=$req->tgl_bpjs_anak1;}
          $tgl_bpjs_anak2=null;
          if (!empty($req->tgl_bpjs_anak2)&&$req->tgl_jamsostek!="0000-00-00"){$tgl_bpjs_anak2=$req->tgl_bpjs_anak2;}
          $tgl_bpjs_anak3=null;
          if (!empty($req->tgl_bpjs_anak3)&&$req->tgl_jamsostek!="0000-00-00"){$tgl_bpjs_anak3=$req->tgl_bpjs_anak3;}


         if (!empty($req->id) ){
          //update
              DB::table(self::TABLE)->where('id', $req->id)->update([
                   'nik' => $req->nik,'nama' => $req->nama,'tgl_lahir'=>$tgl_lahir,
                  'kota_lahir'=>$req->kota_lahir,'agama'=>$req->agama,'jk'=>$req->jk,
                  'alamat'=>$req->alamat,'kota'=>$req->kota,'suku'=>$req->suku,
                  'status_nikah'=>$req->status,'maiden'=>$req->maiden,'goldar'=>$req->goldar,
                  'ktp'=>$req->noktp,'npwp'=>$req->npwp,'telpon'=>$req->telpon,'email'=>$req->email,

                  'tgl_mulai_kerja'=>$tglmulaikerja,'status'=>$req->statuskerja,
                  $tgl=>$tglstatuskerja,$tgl2=>$tglstatuskerjaakhir,

                  'lvl_pendidikan'=>$req->lvlpendidikan,'penyelenggara_pendidikan'=>$req->penyelenggara,
                  'tgl_lulus'=>$tgl_lulus,'jurusan'=>$req->jurusan,

                  'nokk'=>$req->nokk,'status_dalam_keluarga'=>$req->status_keluarga,'nama_pasangan'=>$req->namapasangan,
                  'nama_anak1'=>$req->anak1,'nama_anak2'=>$req->anak2,'nama_anak3'=>$req->anak3,

                  'no_rekening'=>$req->norek,'nama_rekening'=>$req->namarek,'bank'=>$req->bank,
                  'kantor_cabang_bank'=>$req->kantor_cabang,'alamat_bank'=>$req->alamat_bank,

                  'jamsostek'=>$req->jamsostek,'tgl_jamsostek'=>$tgl_jamsostek,
                  'bpjs'=>$req->bpjs,'tgl_bpjs'=>$tgl_bpjs,
                  'bpjs_istri'=>$req->bpjs_istri,'tgl_bpjs_istri'=>$tgl_bpjs_istri,
                  'bpjs_anak1'=>$req->bpjs_anak1,'tgl_bpjs_anak1'=>$tgl_bpjs_anak1,
                  'bpjs_anak2'=>$req->bpjs_anak2,'tgl_bpjs_anak2'=>$tgl_bpjs_anak2,
                  'bpjs_anak3'=>$req->bpjs_anak3,'tgl_bpjs_anak3'=>$tgl_bpjs_anak3,

                  'gadas'=>$req->gadas,'pelatihan_ala'=>$req->pelatihan,

                  'ukuran_baju'=>$req->ukuran, 'penerimaan_seragam'=>$req->seragam,
                  'rompi'=>$req->rompi,'helm'=>$req->helm,'kacamata'=>$req->kacamata,
                  'sepatu_safety'=>$req->sepatu_safety,'sarung_tangan'=>$req->sarung_tangan
              ]);
         }else{
              $id = DB::table(self::TABLE)->insertGetId([
                  'mitra' => 'TA',
                  'nik' => $req->nik,'nama' => $req->nama,'tgl_lahir'=>$tgl_lahir,
                  'kota_lahir'=>$req->kota_lahir,'agama'=>$req->agama,'jk'=>$req->jk,
                  'alamat'=>$req->alamat,'kota'=>$req->kota,'suku'=>$req->suku,
                  'status_nikah'=>$req->status,'maiden'=>$req->maiden,'goldar'=>$req->goldar,
                  'ktp'=>$req->noktp,'npwp'=>$req->npwp,'telpon'=>$req->telpon,'email'=>$req->email,

                  'tgl_mulai_kerja'=>$tglmulaikerja,'status'=>$req->statuskerja,
                  $tgl=>$tglstatuskerja,$tgl2=>$tglstatuskerjaakhir,

                  'lvl_pendidikan'=>$req->lvlpendidikan,'penyelenggara_pendidikan'=>$req->penyelenggara,
                  'tgl_lulus'=>$tgl_lulus,'jurusan'=>$req->jurusan,

                  'nokk'=>$req->nokk,'status_dalam_keluarga'=>$req->status_keluarga,'nama_pasangan'=>$req->namapasangan,
                  'nama_anak1'=>$req->anak1,'nama_anak2'=>$req->anak2,'nama_anak3'=>$req->anak3,

                  'no_rekening'=>$req->norek,'nama_rekening'=>$req->namarek,'bank'=>$req->bank,
                  'kantor_cabang_bank'=>$req->kantor_cabang,'alamat_bank'=>$req->alamat_bank,

                  'jamsostek'=>$req->jamsostek,'tgl_jamsostek'=>$tgl_jamsostek,
                  'bpjs'=>$req->bpjs,'tgl_bpjs'=>$tgl_bpjs,
                  'bpjs_istri'=>$req->bpjs_istri,'tgl_bpjs_istri'=>$tgl_bpjs_istri,
                  'bpjs_anak1'=>$req->bpjs_anak1,'tgl_bpjs_anak1'=>$tgl_bpjs_anak1,
                  'bpjs_anak2'=>$req->bpjs_anak2,'tgl_bpjs_anak2'=>$tgl_bpjs_anak2,
                  'bpjs_anak3'=>$req->bpjs_anak3,'tgl_bpjs_anak3'=>$tgl_bpjs_anak3,

                  'gadas'=>$req->gadas,'pelatihan_ala'=>$req->pelatihan,

                  'ukuran_baju'=>$req->ukuran, 'penerimaan_seragam'=>$req->seragam,
                  'rompi'=>$req->rompi,'helm'=>$req->helm,'kacamata'=>$req->kacamata,
                  'sepatu_safety'=>$req->sepatu_safety,'sarung_tangan'=>$req->sarung_tangan

              ]);
              DB::table('pelurusan_job_position')->insert([
                'id_naker' => $id,
                'status_pengajuan' => 5,
                'id_job_position' =>0,
                'status' =>1
              ]);
          }
        });
      }
    public static function deleteKaryawan($id)
    {
        DB::table(self::TABLE)->where('id', $id)->delete();
    }
    public static function getKaryawanById($id)
    {
        return DB::table('karyawan')
	        ->where('id_karyawan', $id)
	        ->first();
    }
    public static function getAnggotaReguByIdRegu($id)
    {
        $regu = DB::select('select id
        FROM karyawan
        WHERE regu_id=?',[
            $id
        ]);
        $no = 1;
        $regus = '';
        foreach($regu as $n){
          if($no == 1){
            $regus .= $n->id;
            $no = 2;
          }else{
            $regus .= ', '.$n->id;
          }
        }
        return $regus;
    }
    public static function insertRegu($req){
        $id = DB::table('regu')->insertGetId([
           'nama_regu'=> $req->nama_regu,
           'jenis_regu'=> 'ODP'
        ]);
        return $id;
    }
    public static function updateRegu($req)
    {
        DB::table('regu')
          ->where('id', $req->id)
          ->update([
            'nama_regu'=> $req->nama_regu
          ]);
    }
    public static function updateAnggota($req, $id)
    {
        DB::table('karyawan')
          ->where('regu_id', $id)
          ->update([
            'regu_id'=> 0
          ]);
        $karyawan = explode(',', $req->input('karyawan') );
        foreach($karyawan as $kar) {
            if($kar!=''){
                DB::table('karyawan')
                  ->where('id', $kar)
                  ->update([
                    'regu_id'=> $id
                  ]);
            }
        }

    }
    public static function deleteRegu($id)
    {
        DB::table('karyawan')
          ->where('regu_id', $id)
          ->update([
            'regu_id'=> 0
          ]);
        DB::table('regu')
          ->where('id', $id)->delete();
    }
    public static function getListRegu()
    {
        return DB::table('regu')
          ->get();
    }
    public static function getComboRegu()
    {
        return DB::table('regu')->select('id' , 'nama_regu as text')
          ->get();
    }
}