<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\HomeController;
date_default_timezone_set('Asia/Makassar');
class HomeModel{

    public static function get_orders($year)
    {
        $sql_mit = '';
        //MANAGER
        if(in_array(session('auth')->perwira_level, [30]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'"). " AND up.perwira_level IN (46, 22, 100)";
        }
        //MITRA
        if(in_array(session('auth')->perwira_level, [71]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'");
        }
        //SITE MANAGER
        if(in_array(session('auth')->perwira_level, [46]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'"). " AND up.perwira_level IN (22, 100)";
        }
        //TL
        if(in_array(session('auth')->perwira_level, [22]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'"). " AND up.perwira_level IN (100) AND r.TL = ".session('auth')->id_nik;
        }
        //TEKNISI
        if(in_array(session('auth')->perwira_level, [100]) )
        {
            $sql_mit = " AND up.id_nik = ". session('auth')->id_nik;
        }


        $performansi_psb_1 = DB::SELECT('SELECT
        MONTH(pl.modified_at) as month,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") AND pl.status_laporan IN (1,37,38,74,88,94,98) THEN 1 ELSE 0 END) as jml_close
        FROM dispatch_teknisi dt
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN user_perwira up ON r.nik1 = up.id_nik
        WHERE
        up.id_nik IS NOT NULL AND YEAR(pl.modified_at) = '. $year .' '. $sql_mit .' GROUP BY month');

        $performansi_psb_2 = DB::SELECT('SELECT
        MONTH(pl.modified_at) as month,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") AND pl.status_laporan IN (1,37,38,74,88,94,98) THEN 1 ELSE 0 END) as jml_close
        FROM dispatch_teknisi dt
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN user_perwira up ON r.nik2 = up.id_nik
        WHERE
        up.id_nik IS NOT NULL AND YEAR(pl.modified_at) = '. $year .' '. $sql_mit .' GROUP BY month');

        $performansi_marina_1 = DB::SELECT('SELECT
        (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN MONTH(created_at)
        WHEN m.status IN ("Up", "close") THEN MONTH(tgl_selesai) END) as month,
        SUM(CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN m.status IN ("Up", "close") THEN 1 ELSE 0 END) as jml_close
        FROM maintaince m
        LEFT JOIN regu r ON m.dispatch_regu_id = r.id_regu
        LEFT JOIN user_perwira up ON r.nik1 = up.id_nik
        WHERE up.id_nik IS NOT NULL AND (CASE
        WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN YEAR(created_at)
        WHEN m.status IN ("Up", "close") THEN YEAR(tgl_selesai) END) = '. $year .' '. $sql_mit .'
        GROUP BY MONTH( (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN created_at
        WHEN m.status IN ("Up", "close") THEN tgl_selesai END) ) ');

        $performansi_marina_2 = DB::SELECT('SELECT
        (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN MONTH(created_at)
        WHEN m.status IN ("Up", "close") THEN MONTH(tgl_selesai) END) as month,
        SUM(CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN m.status IN ("Up", "close") THEN 1 ELSE 0 END) as jml_close
        FROM maintaince m
        LEFT JOIN regu r ON m.dispatch_regu_id = r.id_regu
        LEFT JOIN user_perwira up ON r.nik2 = up.id_nik
        WHERE up.id_nik IS NOT NULL AND (CASE
        WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN YEAR(created_at)
        WHEN m.status IN ("Up", "close") THEN YEAR(tgl_selesai) END) = '. $year .' '. $sql_mit .'
        GROUP BY MONTH( (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN created_at
        WHEN m.status IN ("Up", "close") THEN tgl_selesai END) ) ');

        $performansi_pt2_1 = DB::SELECT('SELECT
        MONTH(pm.modified_at) as month,
        SUM(CASE WHEN pm.lt_status IN ("Berangkat", "Ogp", "Pending", "Tidak Sempat") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN pm.lt_status IN ("Selesai") THEN 1 ELSE 0 END) as jml_close
        FROM pt2_master pm
        LEFT JOIN regu r ON pm.regu_id = r.id_regu
        LEFT JOIN user_perwira up ON r.nik1 = up.id_nik
        WHERE
        up.id_nik IS NOT NULL AND YEAR(pm.modified_at) = '. $year .' '. $sql_mit .'
        GROUP BY month');

        $performansi_pt2_2 = DB::SELECT('SELECT
        MONTH(pm.modified_at) as month,
        SUM(CASE WHEN pm.lt_status IN ("Berangkat", "Ogp", "Pending", "Tidak Sempat") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN pm.lt_status IN ("Selesai") THEN 1 ELSE 0 END) as jml_close
        FROM pt2_master pm
        LEFT JOIN regu r ON pm.regu_id = r.id_regu
        LEFT JOIN user_perwira up ON r.nik2 = up.id_nik
        WHERE
        up.id_nik IS NOT NULL AND YEAR(pm.modified_at) = '. $year .' '. $sql_mit .'
        GROUP BY month');

        $all_psb = array_unique(array_merge($performansi_psb_1, $performansi_psb_2), SORT_REGULAR);
        $all_marina = array_unique(array_merge($performansi_marina_1, $performansi_marina_2), SORT_REGULAR);
        $all_pt2 = array_unique(array_merge($performansi_pt2_1, $performansi_pt2_2), SORT_REGULAR);
        $result = $final_result = [];

        foreach (array_merge_recursive($all_psb, $all_marina, $all_pt2) as $value)
        {
            $result[$value->month]['month'] = $value->month;
            $result[$value->month]['jml_order'][] = $value->jml_order;
            $result[$value->month]['jml_close'][] = $value->jml_close;
        }

        foreach($result as $k => $v)
        {
            $final_result[$k]['month'] = $v['month'];
            $final_result[$k]['jml_order'] = array_sum($v['jml_order']);
            $final_result[$k]['jml_close'] = array_sum($v['jml_close']);
        }
        // dd($final_result);
        return $final_result;
    }

    public static function get_performansi($startDate,$endDate)
    {
        //MANAGER
        $sql_mit = '';

        if(in_array(session('auth')->perwira_level, [30]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'"). " AND up.perwira_level IN (46, 22, 100)";
        }
        //MITRA
        if(in_array(session('auth')->perwira_level, [71]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'");
        }
        //SITE MANAGER
        if(in_array(session('auth')->perwira_level, [46]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'"). " AND up.perwira_level IN (22, 100)";
        }
        //TL
        if(in_array(session('auth')->perwira_level, [22]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'"). " AND up.perwira_level IN (100) AND r.TL = ".session('auth')->id_nik;
        }
        //TEKNISI
        if(in_array(session('auth')->perwira_level, [100]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'");
        }

        $performansi_psb_1 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik1 END) as nik,
        up.mitra,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") AND pl.status_laporan IN (1,37,38,74,88,94,98) THEN 1 ELSE 0 END) as jml_close
        FROM dispatch_teknisi dt
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN user_perwira up ON r.nik1 = up.id_nik
        WHERE
        (DATE(pl.modified_at) BETWEEN "'.$startDate.'" AND "'.$endDate.'")'. $sql_mit .'
        GROUP BY nik
        HAVING (jml_order != 0 OR jml_close != 0) AND nik IS NOT NULL
        ORDER BY jml_close DESC');

        $performansi_psb_2 = DB::SELECT('SELECT
        up.mitra,
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik2 END) as nik,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") AND pl.status_laporan IN (1,37,38,74,88,94,98) THEN 1 ELSE 0 END) as jml_close
        FROM dispatch_teknisi dt
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN user_perwira up ON r.nik2 = up.id_nik
        WHERE
        (DATE(pl.modified_at) BETWEEN "'.$startDate.'" AND "'.$endDate.'")'. $sql_mit .'
        GROUP BY nik
        HAVING (jml_order != 0 OR jml_close != 0) AND nik IS NOT NULL
        ORDER BY jml_close DESC');

        $performansi_marina_1 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik1 END) as nik,
        (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN MONTH(created_at)
        WHEN m.status IN ("Up", "close") THEN MONTH(tgl_selesai) END) as month,
        up.mitra,
        SUM(CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN m.status IN ("Up", "close") THEN 1 ELSE 0 END) as jml_close
        FROM maintaince m
        LEFT JOIN regu r ON m.dispatch_regu_id = r.id_regu
        LEFT JOIN user_perwira up ON r.nik1 = up.id_nik
        WHERE (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN created_at
        WHEN m.status IN ("Up", "close") THEN tgl_selesai END) BETWEEN "'.$startDate.'" AND "'.$endDate.'"'. $sql_mit .'
        GROUP BY nik
        HAVING (jml_order != 0 OR jml_close != 0) AND nik IS NOT NULL ORDER BY jml_close DESC');

        $performansi_marina_2 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik2 END) as nik,
        (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN MONTH(created_at)
        WHEN m.status IN ("Up", "close") THEN MONTH(tgl_selesai) END) as month,
        up.mitra,
        SUM(CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN m.status IN ("Up", "close") THEN 1 ELSE 0 END) as jml_close
        FROM maintaince m
        LEFT JOIN regu r ON m.dispatch_regu_id = r.id_regu
        LEFT JOIN user_perwira up ON r.nik2 = up.id_nik
        WHERE (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN created_at
        WHEN m.status IN ("Up", "close") THEN tgl_selesai END) BETWEEN "'.$startDate.'" AND "'.$endDate.'"'. $sql_mit .'
        GROUP BY nik
        HAVING (jml_order != 0 OR jml_close != 0) AND nik IS NOT NULL ORDER BY jml_close DESC');

        $performansi_pt2_1 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik1 END) as nik,
        up.mitra,
        SUM(CASE WHEN pm.lt_status IN ("Berangkat", "Ogp", "Pending", "Tidak Sempat") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN pm.lt_status IN ("Selesai") THEN 1 ELSE 0 END) as jml_close
        FROM pt2_master pm
        LEFT JOIN regu r ON pm.regu_id = r.id_regu
        LEFT JOIN user_perwira up ON r.nik1 = up.id_nik
        WHERE
        DATE(pm.modified_at) BETWEEN "'.$startDate.'" AND "'.$endDate.'"'. $sql_mit .'
        GROUP BY nik
        HAVING (jml_order != 0 OR jml_close != 0) AND nik IS NOT NULL
        ORDER BY jml_close DESC');

        $performansi_pt2_2 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik2 END) as nik,
        up.mitra,
        SUM(CASE WHEN pm.lt_status IN ("Berangkat", "Ogp", "Pending", "Tidak Sempat") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN pm.lt_status IN ("Selesai") THEN 1 ELSE 0 END) as jml_close
        FROM pt2_master pm
        LEFT JOIN regu r ON pm.regu_id = r.id_regu
        LEFT JOIN user_perwira up ON r.nik2 = up.id_nik
        WHERE
        DATE(pm.modified_at) BETWEEN "'.$startDate.'" AND "'.$endDate.'"'. $sql_mit .'
        GROUP BY nik
        HAVING (jml_order != 0 OR jml_close != 0) AND nik IS NOT NULL
        ORDER BY jml_close DESC');

        $all_psb = array_unique(array_merge($performansi_psb_1, $performansi_psb_2), SORT_REGULAR);
        $all_marina = array_unique(array_merge($performansi_marina_1, $performansi_marina_2), SORT_REGULAR);
        $all_pt2 = array_unique(array_merge($performansi_pt2_1, $performansi_pt2_2), SORT_REGULAR);
        $result = $final_result = [];

        foreach (array_merge_recursive($all_psb, $all_marina, $all_pt2) as $value)
        {
            $result[$value->nik]['nama'] = $value->nama;
            $result[$value->nik]['nik'] = $value->nik;
            $result[$value->nik]['mitra_amija_pt'] = $value->mitra;
            $result[$value->nik]['jml_order'][] = $value->jml_order;
            $result[$value->nik]['jml_close'][] = $value->jml_close;
        }

        foreach($result as $k => $v)
        {
            $final_result[$k]['nama'] = $v['nama'];
            $final_result[$k]['nik'] = $v['nik'];
            $final_result[$k]['nik'] = $v['nik'];
            $final_result[$k]['mitra_amija_pt'] = $v['mitra_amija_pt'];
            $final_result[$k]['jml_order'] = array_sum($v['jml_order']);
            $final_result[$k]['jml_close'] = array_sum($v['jml_close']);
        }

        if($final_result)
        {
            usort($final_result, function ($a, $b) {
                return $a['jml_close'] < $b['jml_close'];
            });
        }

        return $final_result;
    }

    public static function get_karyawan()
    {
        $sql_mit = '';

        if(in_array(session('auth')->perwira_level, [30]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'"). " AND up.perwira_level IN (46, 22, 100)";
        }
        //MITRA
        if(in_array(session('auth')->perwira_level, [71]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'");
        }
        //SITE MANAGER
        if(in_array(session('auth')->perwira_level, [46]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'"). " AND up.perwira_level IN (22, 100)";
        }
        //TL
        if(in_array(session('auth')->perwira_level, [22]) )
        {
            $sql_mit = " AND up.mitra = ". (session('auth')->mitra ?? "'kosong'"). " AND up.perwira_level IN (100) AND gt.TL_NIK = ".session('auth')->id_nik;
        }

        return DB::SELECT('SELECT
        CONCAT("Jumlah Karyawan Setiap Mitra") as title,
        (CASE WHEN up.mitra IS NULL THEN "Mitra Kosong" ELSE up.mitra END) as judul,
        SUM(CASE WHEN up.nama <> "" THEN 1 ELSE 0 END) as jml
        FROM user_perwira up
        LEFT JOIN regu r ON (up.id_user = r.nik1 OR up.id_user = r.nik2) AND r.ACTIVE = 1
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        WHERE
        up.witel = "KALSEL"'. $sql_mit .'
        GROUP BY up.mitra');
    }

    public static function history_accident($id)
    {
        return DB::table('user_perwira as up')
        ->leftjoin('perwira_accident as pa', 'pa.people_id', '=', 'up.id_people')
        ->select('pa.*')
        ->where('up.id_people', $id->id_people)
        ->whereNotNull('pa.id')
        ->get();
    }

    public static function history_resling($id)
    {
        return DB::table('user_perwira as up')
        ->leftjoin('perwira_resling as pr', 'pr.people_id', '=', 'up.id_people')
        ->select('pr.*')
        ->where('up.id_people', $id->id_people)
        ->whereNotNull('pr.id')
        ->get();
    }

    public static function history_reward($id)
    {
        return DB::table('user_perwira as up')
        ->leftjoin('perwira_archive as pa', 'pa.people_id', '=', 'up.id_people')
        ->select('pa.*')
        ->where('up.id_people', $id->id_people)
        ->whereNotNull('pa.id')
        ->get();
    }

    public static function history_fa($id)
    {
        return DB::table('user_perwira as up')
        ->leftjoin('perwira_brevert_user as pbu', 'pbu.noktp', '=', 'up.noktp')
        ->leftjoin('perwira_breving as pb', 'pb.id', 'pbu.id_brevert')
        ->select('pb.*')
        ->where('up.noktp', $id->noktp)
        ->whereNotNull('pb.id')
        ->GroupBy('pb.id')
        ->get();
    }

    public static function history_cuti($id)
    {
        return DB::table('user_perwira as up')
        ->leftjoin('perwira_cuti as pc', 'pc.people_id', '=', 'up.id_people')
        ->leftjoin('perwira_jenis_cuti as pjc', 'pc.jenis_id', '=', 'pjc.id')
        ->select('pc.*', 'pjc.cuti_nm')
        ->where('up.id_people', $id->id_people)
        ->whereNotNull('pc.id')
        ->get();
    }

    public static function history_mutasi($id)
    {
        return DB::SELECT("SELECT
        pmd.nik_atasan as nik_atasan,
        pmd.jenis as jenis,
        pmd.witel_id as witel_id,
        pmd.sto_id as sto_id,
        pmd.position_id as position_id,
        pmd.create_by as create_by,
        pmd.approve_by as approve_by,
        pmd.known_by as known_by,
        pp.id as id_p_pos,
        pp.pos_name as pos_name,
        pp.level as level,
        pp.unit as unit,
        pp.sub_unit as sub_unit,
        pp.grup as grup,
        pp.sub_grup as sub_grup,
        pp.fungsi_grup as fungsi_grup,
        pp.regional as regional,
        pp.teritory as teritory,
        pp.bizpart_id as bizpart_id,
        pp.status as status,
        pd.direktorat as direktorat,
        w.nama_witel as nama_witel,
        aa.kode_area as kode_area,
        pd.id as direktorat_id,
        w.id_witel as nama_witel_id,
        aa.id_area as kode_area_id,
        (SELECT CONCAT(up_s.nama, ' (', pmd.create_by, ')' ) FROM user_perwira up_s WHERE pmd.create_by = up_s.id_nik) as nm_create_by,
        (SELECT CONCAT(up_s.nama, ' (', pmd.approve_by, ')' ) FROM user_perwira up_s WHERE pmd.approve_by = up_s.id_nik) as nm_approve_by,
        (SELECT CONCAT(up_s.nama, ' (', pmd.known_by, ')' ) FROM user_perwira up_s WHERE pmd.known_by = up_s.id_nik) as nm_known_by,
        (SELECT CONCAT(up_s.nama, ' (', pmd.create_by, ')' ) FROM user_perwira up_s WHERE pmd.create_by = up_s.id_nik) as nm_p_create_by,
        (SELECT CONCAT(up_s.nama, ' (', pmd.approve_by, ')' ) FROM user_perwira up_s WHERE pmd.approve_by = up_s.id_nik) as nm_p_approve_by,
        (SELECT CONCAT(up_s.nama, ' (', pmd.known_by, ')' ) FROM user_perwira up_s WHERE pmd.known_by = up_s.id_nik) as nm_p_known_by
        FROM user_perwira up
        LEFT JOIN perwira_mutasi_data as pmd ON pmd.people_id = up.id_people
        LEFT JOIN witel w ON w.id_witel = pmd.witel_id
        LEFT JOIN area_alamat aa ON aa.id_area = pmd.sto_id
        LEFT JOIN perwira_posisition pp ON pp.id = pmd.position_id
        LEFT JOIN perwira_direktorat pd ON pd.id = pp.direktorat_id
        WHERE up.id_people = $id->id_people AND pmd.id IS NOT NULL");
}

    public static function view_profile_recruiter($id)
    {
        return DB::table('perwira_recruitment as pr')
        ->leftJoin('api_provinsi as ap','pr.provinsi','=','ap.id')
        ->leftJoin('api_kota as ak','pr.kota','=','ak.id_kota')
        ->leftJoin('api_kecamatan as akc','pr.kecamatan','=','akc.id_kecamatan')
        ->leftJoin('api_kelurahan as akl','pr.kelurahan','=','akl.id_kelurahan')
        ->select('pr.*','ap.nama as nama_provinsi','ak.nama as nama_kota','akc.nama as nama_kecamatan','akl.nama as nama_kelurahan')
        ->where('pr.noktp', $id)
        ->first();
    }

    public static function search_data_result($jenis, $search)
    {
        if($jenis == 'ktp')
        {
            $sql = "up.nokk LIKE '%$search%'";
        }

        if($jenis == 'nik')
        {
            $sql = "up.id_nik LIKE '%$search%'";
        }

        if($jenis == 'nama')
        {
            $sql = "up.nama LIKE '%$search%'";
        }
        // dd($sql);
        $performansi_psb_1 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik1 END) as nik,
        up.mitra,
        up.id_people,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") AND pl.status_laporan IN (1,37,38,74,88,94,98) THEN 1 ELSE 0 END) as jml_close
        FROM user_perwira up
        LEFT JOIN regu r ON r.nik1 = up.id_nik
        LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        WHERE
        '. $sql .'
        GROUP BY nik
        ORDER BY jml_close DESC');

        $performansi_psb_2 = DB::SELECT('SELECT
        up.mitra,
        up.id_people,
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik2 END) as nik,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN dt.jenis_order IN ("SC","MYIR","CABUT_NTE","ONT_PREMIUM","IN","INT","MIGRASI_CCAN") AND pl.status_laporan IN (1,37,38,74,88,94,98) THEN 1 ELSE 0 END) as jml_close
        FROM user_perwira up
        LEFT JOIN regu r ON r.nik1 = up.id_nik
        LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        WHERE
        '. $sql .'
        GROUP BY nik
        ORDER BY jml_close DESC');

        $performansi_marina_1 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik1 END) as nik,
        (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN MONTH(created_at)
        WHEN m.status IN ("Up", "close") THEN MONTH(tgl_selesai) END) as month,
        up.mitra,
        up.id_people,
        SUM(CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN m.status IN ("Up", "close") THEN 1 ELSE 0 END) as jml_close
        FROM user_perwira up
        LEFT JOIN regu r ON r.nik1 = up.id_nik
        LEFT JOIN maintaince m ON m.dispatch_regu_id = r.id_regu
        WHERE '. $sql .'
        GROUP BY nik');

        $performansi_marina_2 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik2 END) as nik,
        (CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN MONTH(created_at)
        WHEN m.status IN ("Up", "close") THEN MONTH(tgl_selesai) END) as month,
        up.mitra,
        up.id_people,
        SUM(CASE WHEN m.status IN ("ogp", "Rescedule") OR m.status = NULL THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN m.status IN ("Up", "close") THEN 1 ELSE 0 END) as jml_close
        FROM user_perwira up
        LEFT JOIN regu r ON r.nik1 = up.id_nik
        LEFT JOIN maintaince m ON m.dispatch_regu_id = r.id_regu
        WHERE '. $sql .'
        GROUP BY nik');

        $performansi_pt2_1 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik1 END) as nik,
        up.mitra,
        up.id_people,
        SUM(CASE WHEN pm.lt_status IN ("Berangkat", "Ogp", "Pending", "Tidak Sempat") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN pm.lt_status IN ("Selesai") THEN 1 ELSE 0 END) as jml_close
        FROM user_perwira up
        LEFT JOIN regu r ON r.nik1 = up.id_nik
        LEFT JOIN pt2_master pm ON pm.regu_id = r.id_regu
        WHERE
        '. $sql .'
        GROUP BY nik
        ORDER BY jml_close DESC');

        $performansi_pt2_2 = DB::SELECT('SELECT
        (CASE WHEN up.nama IS NOT NULL THEN up.nama ELSE "Nama Kosong!" END) as nama,
        (CASE WHEN up.id_nik IS NOT NULL THEN up.id_nik ELSE r.nik2 END) as nik,
        up.mitra,
        up.id_people,
        SUM(CASE WHEN pm.lt_status IN ("Berangkat", "Ogp", "Pending", "Tidak Sempat") THEN 1 ELSE 0 END) as jml_order,
        SUM(CASE WHEN pm.lt_status IN ("Selesai") THEN 1 ELSE 0 END) as jml_close
        FROM user_perwira up
        LEFT JOIN regu r ON r.nik1 = up.id_nik
        LEFT JOIN pt2_master pm ON pm.regu_id = r.id_regu
        WHERE
        '. $sql .'
        GROUP BY nik
        ORDER BY jml_close DESC');

        $all_psb = array_unique(array_merge($performansi_psb_1, $performansi_psb_2), SORT_REGULAR);
        $all_marina = array_unique(array_merge($performansi_marina_1, $performansi_marina_2), SORT_REGULAR);
        $all_pt2 = array_unique(array_merge($performansi_pt2_1, $performansi_pt2_2), SORT_REGULAR);
        $result = $final_result = [];
        foreach (array_merge_recursive($all_psb, $all_marina, $all_pt2) as $value)
        {
            $result[$value->nik]['nama'] = $value->nama;
            $result[$value->nik]['nik'] = $value->nik;
            $result[$value->nik]['id_people'] = $value->id_people;
            $result[$value->nik]['mitra_amija_pt'] = $value->mitra;
            $result[$value->nik]['jml_order'][] = $value->jml_order;
            $result[$value->nik]['jml_close'][] = $value->jml_close;
        }

        foreach($result as $k => $v)
        {
            $final_result[$k]['nama'] = $v['nama'];
            $final_result[$k]['nik'] = $v['nik'];
            $final_result[$k]['id_people'] = $v['id_people'];
            $final_result[$k]['mitra_amija_pt'] = $v['mitra_amija_pt'];
            $final_result[$k]['jml_order'] = array_sum($v['jml_order']);
            $final_result[$k]['jml_close'] = array_sum($v['jml_close']);
        }
        // dd($final_result);
        return $final_result;
    }

    public static function myProfile()
    {
        return DB::SELECT('SELECT
        emp.*,
        u.witel,
        ap.nama as nama_provinsi,
        ak.nama as nama_kota,
        akl.nama as nama_kelurahan,
        akc.nama as nama_kecamatan
        FROM 1_2_employee emp
        LEFT JOIN user u ON emp.nik = u.id_user
        LEFT JOIN mitra_amija ma ON emp.mitra_amija = ma.mitra_amija AND ma.witel = "'.session('auth')->witel.'"
        LEFT JOIN api_provinsi ap ON emp.provinsi = ap.id
        LEFT JOIN api_kota ak ON emp.kota = ak.id_kota
        LEFT JOIN api_kelurahan akl ON emp.kelurahan = akl.id_kelurahan
        LEFT JOIN api_kecamatan akc ON emp.kecamatan = akc.id_kecamatan
        WHERE
        emp.nik = "'.session('auth')->id_karyawan.'"
        ')[0] ?? DB::connection('data_center')
		->table('master_karyawan As a')
        ->select('NAME', 'a.*', 'position_name', 'level_name', 'unit_name', 'witel_name As witel', 'mitra_name', 'name As nama', DB::RAW("Null As jk, Null As goldar, Null As tgl_lahir, Null As tempat_lahir, Null As agama, Null As status_perkawinan, Null As alamat, Null As no_telp, Null As no_wa, Null As noktp, Null As email, Null As ibu, Null As telpon_2_nm, Null As telpon_2, Null As alamat_urgent, Null As npwp, Null As tanggal_aju, Null As tglmulaikerja, Null As status_kerja, Null As direktorat_id, Null As psa, Null As tglawalkerja, Null As tglakhirkerja, Null As last_study, Null As tgllulus, Null As jurusan, Null As nokk, Null As status_keluarga, Null As namapasangan, Null As anak1, Null As anak2, Null As anak3, Null As waspang_nik, Null As sepatu") )
        ->leftJoin('master_position As b', 'a.id_position', '=', 'b.position_id')
        ->leftJoin('master_level As c', 'a.id_level', '=', 'c.level_id')
        ->leftJoin('master_unit As d', 'd.unit_id', '=', 'a.id_unit')
        ->leftJoin('master_witel As e', 'a.id_witel', '=', 'e.witel_id')
        ->leftJoin('master_mitra As f', 'a.id_mitra', '=', 'f.mitra_id')
        ->where('nik', '=', session('auth')->nik)->first();
    }

    public static function brevert_dan_akun($kat)
    {
        if ($kat == "BELUM_BREVET") {
            $where_kat = 'emp.ket_brevet = 0';
        } elseif ($kat == "LULUS_BREVET") {
            $where_kat = 'emp.ket_brevet = 1';
        } elseif ($kat == "BELUM_AKUN") {
            $where_kat = 'emp.ket_akun = 0';
        } elseif ($kat == "PUNYA_AKUN") {
            $where_kat = 'emp.ket_akun = 1';
        }

        return DB::SELECT('SELECT
        emp.*,
        u.witel,
        ap.nama as nama_provinsi,
        ak.nama as nama_kota,
        akl.nama as nama_kelurahan,
        akc.nama as nama_kecamatan
        FROM 1_2_employee emp
        LEFT JOIN user u ON emp.nik = u.id_user
        LEFT JOIN mitra_amija ma ON emp.mitra_amija = ma.mitra_amija AND ma.witel = "'.session('auth')->witel.'"
        LEFT JOIN api_provinsi ap ON emp.provinsi = ap.id
        LEFT JOIN api_kota ak ON emp.kota = ak.id_kota
        LEFT JOIN api_kelurahan akl ON emp.kelurahan = akl.id_kelurahan
        LEFT JOIN api_kecamatan akc ON emp.kecamatan = akc.id_kecamatan
        WHERE
        emp.updateBy = "PERWIRA" AND
        '.$where_kat.'
        ORDER BY emp.dateUpdate DESC
        ');
    }

    public static function monolog_mytech()
    {
        return DB::SELECT('SELECT
            mtt.WITEL_HR,
            SUM(CASE WHEN up.id_nik IS NULL THEN 1 ELSE 0 END) as tidak_terdaftar,
            SUM(CASE WHEN mtt.STATUS_KARYAWAN LIKE "ACTIVE" THEN 1 ELSE 0 END) as karyawan_active,
            SUM(CASE WHEN mtt.STATUS_KARYAWAN LIKE "NON ACTIVE%" THEN 1 ELSE 0 END) as karyawan_nonactive,
            SUM(CASE WHEN mtt.MY_TECH LIKE "ACTIVE%" AND mtt.SCMT LIKE "Active%" AND mtt.STATUS_KARYAWAN = "ACTIVE" THEN 1 ELSE 0 END) as mytech_scmt_active,
            SUM(CASE WHEN mtt.MY_TECH LIKE "ACTIVE%" AND mtt.SCMT IS NULL AND mtt.STATUS_KARYAWAN = "ACTIVE" THEN 1 ELSE 0 END) as mytech_scmt_blanks,
            SUM(CASE WHEN mtt.SCMT LIKE "Inactive%" AND mtt.MY_TECH LIKE "ACTIVE%" AND mtt.STATUS_KARYAWAN = "ACTIVE" THEN 1 ELSE 0 END) as scmt_mytech_active,
            SUM(CASE WHEN mtt.SCMT LIKE "Active%" AND mtt.MY_TECH IS NULL AND mtt.STATUS_KARYAWAN = "ACTIVE" THEN 1 ELSE 0 END) as scmt_mytech_blanks
            FROM monolog_teknisi_tr6 mtt
            LEFT JOIN user_perwira up ON mtt.NIK = up.id_nik
            WHERE mtt.WITEL_HR = "WITEL BANJARMASIN"
            GROUP BY mtt.WITEL_HR
        ')[0];
    }

    public static function get_pelamar($id_prov, $status_pelamar){
        $sql = '';
        if($id_prov != 'all')
        {
            $sql = 'pr.provinsi = "'.$id_prov.'" AND';
        }

        return DB::SELECT('SELECT
        ak.nama, ak.id_kota, COUNT(*) AS jumlah
        FROM perwira_recruitment pr
        LEFT JOIN api_kota ak ON pr.kota = ak.id_kota
        WHERE
        ' . $sql . ' pr.status_pelamar = "'.$status_pelamar.'"
        GROUP BY pr.kota ORDER BY jumlah DESC');
    }

    // public static function pelamar_permitra($id_prov, $status_pelamar){
    //     $sql = '';
    //     if($id_prov != 'all')
    //     {
    //         $sql = 'pr.provinsi = "'.$id_prov.'" AND';
    //     }

    //     return DB::SELECT('SELECT
    //     pr.modified_by AS nama,
    //     up.mitra,
    //     count(*) AS jumlah
    //     FROM perwira_recruitment pr
    //     LEFT JOIN api_kota ak ON pr.kota = ak.id_kota
    //     LEFT JOIN user_perwira up ON pr.modified_by = up.id_nik
    //     WHERE
    //     ' . $sql . ' pr.status_pelamar = "'.$status_pelamar.'"
    //     GROUP BY pr.modified_by ORDER BY jumlah DESC');
    // }

    public static function get_data_front($jenis, $req)
    {
        $tahun_rec  = "YEAR(created_at) = YEAR(NOW() )";
        $sql_jk = $total_u_rec = $sql_daerah =  '';

        if($req->all() )
        {

            if($req->jk)
            {
                $jk = "('".implode("','", $req->jk)."')";
                $sql_jk = "AND jk IN $jk";
            }

            if($req->tahun)
            {
                $tahun_rec = "YEAR(created_at) = $req->tahun";
            }

            if($req->umur)
            {
                if(in_array('18_25', $req->umur) )
                {
                    $umur_rec[] ="YEAR(NOW() ) - YEAR(tgl_lahir) BETWEEN 18 AND 25";
                }

                if(in_array('25_', $req->umur) )
                {
                    $umur_rec[] ="YEAR(NOW() ) - YEAR(tgl_lahir) > 25";
                }

                if($umur_rec)
                {
                    $total_u_rec = "AND (". implode(' OR ',$umur_rec).")";
                }
            }

            $daerah = [];

            if($req->provinsi && $req->provinsi != 'all')
            {
                $daerah[] = "provinsi = $req->provinsi";
            }

            if($req->kota && $req->kota != 'all')
            {
                $daerah[] = "kota = $req->kota";
            }

            if($req->kecamatan && $req->kecamatan != 'all')
            {
                $daerah[] = "kecamatan = $req->kecamatan";
            }

            if($req->kelurahan && $req->kelurahan != 'all')
            {
                $daerah[] = "kelurahan = $req->kelurahan";
            }

            if($daerah)
            {
                $sql_daerah = "AND ". implode(' AND ', $daerah);
            }
        }

        switch ($jenis) {
            case 'pelamar':
                $data = DB::SELECT("SELECT
                COUNT(*) as data,
                MONTH(created_at) as tgl
                FROM perwira_recruitment
                WHERE status_pelamar = 'NEW' AND log_pelamar IS NULL AND $tahun_rec $sql_jk $total_u_rec $sql_daerah
                GROUP BY MONTH(created_at)
                ORDER BY MONTH(created_at) ASC
                ");
            break;
            case 'rekrut':
                $data = DB::SELECT("SELECT
                COUNT(*) as data,
                MONTH(created_at) as tgl
                FROM perwira_recruitment
                WHERE status_pelamar = 'REKRUT' AND $tahun_rec $sql_jk $total_u_rec $sql_daerah
                GROUP BY MONTH(created_at)
                ORDER BY MONTH(created_at) ASC
                ");
            break;
            case 'rejected':
                $data = DB::SELECT("SELECT
                COUNT(*) as data,
                MONTH(created_at) as tgl
                FROM perwira_recruitment
                WHERE status_pelamar = 'NEW' AND log_pelamar = 'REJECTED' AND $tahun_rec $sql_jk $total_u_rec $sql_daerah
                GROUP BY MONTH(created_at)
                ORDER BY MONTH(created_at) ASC
                ");
            break;
            case 'accept':
                $data = DB::SELECT("SELECT
                COUNT(*) as data,
                MONTH(created_at) as tgl
                FROM perwira_recruitment
                WHERE status_pelamar = 'ACCEPTED' AND $tahun_rec $sql_jk $total_u_rec $sql_daerah
                GROUP BY MONTH(created_at)
                ORDER BY MONTH(created_at) ASC
                ");
            break;
            case 'total_lamar':
                $data = DB::SELECT("SELECT
                COUNT(*) as data,
                MONTH(created_at) as tgl
                FROM perwira_recruitment
                WHERE $tahun_rec $sql_jk $total_u_rec $sql_daerah
                GROUP BY MONTH(created_at)
                ORDER BY MONTH(created_at) ASC
                ");
            break;
        }
        return $data;
    }

    public static function get_total_front_all($req)
    {
        $tahun_user  = "YEAR(dateUpdate) = YEAR(NOW() )";
        $sql_jk = $total_u_user = $sql_mit = '';

        if($req->all() )
        {

          if($req->jk)
          {
            $jk = "('".implode("','", $req->jk)."')";
            $sql_jk = "AND jk IN $jk";
          }

          if($req->tahun)
          {
            $tahun_user = "YEAR(dateUpdate) = $req->tahun";
          }

          if($req->umur)
          {
            if(in_array('18_25', $req->umur) )
            {
              $umur_user[] ="YEAR(NOW() ) - YEAR(tgl_lahir) BETWEEN 18 AND 25";
            }

            if(in_array('25_', $req->umur) )
            {
              $umur_user[] ="YEAR(NOW() ) - YEAR(tgl_lahir) > 25";
            }

            if($umur_user)
            {
              $total_u_user = "AND (". implode(' OR ',$umur_user).")";
            }
          }
        }

        if(in_array(session('auth')->perwira_level, [30, 71]) )
        {
            $sql_mit = "AND user_perwira.mitra = ". session('auth')->mitra. " AND user_perwira.perwira_level IN (46, 22, 100)";
        }

        return DB::SELECT("SELECT
        COUNT(*) as data,
        MONTH(dateUpdate) as tgl
        FROM user_perwira
        WHERE $tahun_user $sql_jk $total_u_user $sql_mit
        GROUP BY MONTH(dateUpdate)
        ORDER BY MONTH(dateUpdate) ASC
        ");
    }

    public static function data_jk($req)
    {
        $sql_jk = $total_u_user = $sql_mit = '';

        if($req->all() )
        {
          if($req->jk)
          {
            $jk = "('".implode("','", $req->jk)."')";
            $sql_jk = "AND jk IN $jk";
          }

          if($req->umur)
          {
            if(in_array('18_25', $req->umur) )
            {
              $umur_user[] ="YEAR(NOW() ) - YEAR(tgl_lahir) BETWEEN 18 AND 25";
            }

            if(in_array('25_', $req->umur) )
            {
              $umur_user[] ="YEAR(NOW() ) - YEAR(tgl_lahir) > 25";
            }

            if($umur_user)
            {
              $total_u_user = "AND (". implode(' OR ',$umur_user).")";
            }
          }
        }

        if(in_array(session('auth')->perwira_level, [30, 71]) )
        {
          $sql_mit = "AND user_perwira.mitra = ". session('auth')->mitra. " AND user_perwira.perwira_level IN (46, 22, 100)";
        }

        return DB::SELECT("SELECT
        SUM(CASE WHEN jk = 'Laki-Laki' THEN 1 ELSE 0 END) as L,
        SUM(CASE WHEN jk = 'Perempuan' THEN 1 ELSE 0 END) as P
        FROM user_perwira WHERE user_perwira.id_nik IS NOT NULL $sql_jk $total_u_user $sql_mit
        GROUP BY YEAR(dateUpdate)");
    }

    public static function get_minim_year()
    {
        return DB::table('user_perwira')
        ->select(DB::raw('YEAR(dateUpdate) as thn') )
        ->where(DB::raw('YEAR(dateUpdate)'), '!=', '0000')
        ->orderBy('dateUpdate', 'ASC')
        ->limit(1)
        ->first();
    }

    private static function handleFileUpload($req, $id)
	{
        $that = new HomeController();
		foreach($that->fileRecruitment as $namefile)
		{
			if ($req->hasFile($namefile) )
			{
				$hdd = DB::TABLE('hdd')->first();
				$path = public_path() . '/'.$hdd->public.'/perwira/recruitment/'.$id.'/';

				if (!file_exists($path) ) {
					if (!mkdir($path, 0770, true) ) {
					return 'Gagal Menyimpan File';
					}
				}

				if ($req->hasFile($namefile) ) {
					$file = $req->file($namefile);
					try {

					//save log
					DB::TABLE('perwira_recruitment_fileUpload')->insert([
						'originalName' => $file->getClientOriginalName(),
						'originalExt' => $file->getClientOriginalExtension(),
						'fileName' => $namefile,
						'ktp_no' => $id
					]);

					$filename = $namefile.'.'.strtolower( $file->getClientOriginalExtension() );
					$file->move("$path", "$filename");

					} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
					return 'Gagal Menyimpan '. $path;
					}
				}
			}
		}
	}

    public static function save_profile($req)
    {
        DB::transaction( function() use($req)
        {
            DB::table('1_2_employee')->where('nik', $req->input('nik_id') )->update([
            'nik'               => $req->input('nik'),
            'nik_amija'         => $req->input('nik'),
            'nama'              => $req->input('nama'),
            'jobdesk_hr'        => $req->jobdesk_hr,
            'jk'                => $req->input('jk'),
            'goldar'            => $req->input('goldar'),
            'tgl_lahir'         => $req->input('tgllahir'),
            'tempat_lahir'      => $req->input('kota_lahir'),
            'agama'             => $req->input('agama'),
            'status_perkawinan' => $req->input('status_perkawinan'),
            'alamat'            => $req->input('alamat'),
            'no_telp'           => $req->input('telpon'),
            'no_wa'             => $req->input('no_wa'),
            'sto'               => $req->input('sto'),
            'noktp'             => $req->input('noktp'),
            'provinsi'          => $req->input('provinsi'),
            'kota'              => $req->input('kota'),
            'kecamatan'         => $req->input('kecamatan'),
            'kelurahan'         => $req->input('kelurahan'),
            'email'             => $req->input('email'),
            'ibu'               => $req->input('ibu'),
            'telpon_2_nm'       => $req->input('telpon_2_nm'),
            'telpon_2'          => $req->input('telpon_2'),
            'npwp'              => $req->input('npwp'),
            'rhesus'            => $req->input('rhesus'),
            'alamat_urgent'     => $req->input('alamat_urgent'),
            'tanggal_aju'       => $req->input('tanggal_aju'),
            'tglmulaikerja'     => $req->input('tglmulaikerja'),
            'status_kerja'      => $req->input('status_kerja'),
            'alamat_koordinat'  => $req->input('koor_alamat'),
            'urgent_koordinat'  => $req->input('koor_urgent'),
            'psa'               => $req->input('psa'),
            // 'Witel_New'      => $req->input('witel'),
            'region'            => $req->input('region'),
            'tglawalkerja'      => $req->input('tglawalkerja'),
            'tglakhirkerja'     => $req->input('tglakhirkerja'),
            'lvl_pendidikan'    => $req->input('lvl_pendidikan'),
            'last_study'        => $req->input('last_study'),
            'tgllulus'          => $req->input('tgllulus'),
            'jurusan'           => $req->input('jurusan'),
            'nokk'              => $req->input('nokk'),
            'status_keluarga'   => $req->input('status_keluarga'),
            'namapasangan'      => $req->input('namapasangan'),
            'anak1'             => $req->input('anak1'),
            'anak2'             => $req->input('anak2'),
            'anak3'             => $req->input('anak3'),
            'norek'             => $req->input('norek'),
            'bank'              => $req->input('bank'),
            'kantor_cabang'     => $req->input('kantor_cabang'),
            'namarek'           => $req->input('namarek'),
            'link_aj'           => $req->input('link_aj'),
            'jamsostek'         => $req->input('jamsostek'),
            'tgl_jamsostek'     => $req->input('tgl_jamsostek'),
            'bpjs'              => $req->input('bpjs'),
            'tgl_bpjs'          => $req->input('tgl_bpjs'),
            'bpjs_istri'        => $req->input('bpjs_istri'),
            'tgl_bpjs_istri'    => $req->input('tgl_bpjs_istri'),
            'bpjs_anak1'        => $req->input('bpjs_anak1'),
            'tgl_bpjs_anak1'    => $req->input('tgl_bpjs_anak1'),
            'bpjs_anak2'        => $req->input('bpjs_anak2'),
            'tgl_bpjs_anak2'    => $req->input('tgl_bpjs_anak2'),
            'bpjs_anak3'        => $req->input('bpjs_anak3'),
            'tgl_bpjs_anak3'    => $req->input('tgl_bpjs_anak3'),
            'waspang_nik'       => $req->input('waspang_nik'),
            'mitra_pt'          => $req->input('mitra_name'),
            'position'          => $req->input('posisi'),
            'gadas'             => $req->input('gadas'),
            'pelatihan'         => $req->input('pelatihan'),
            'seragam'           => $req->input('seragam'),
            'celana'            => $req->input('celana'),
            'sepatu'            => $req->input('sepatu')
            ]);
            //periksa user jika perubahan nik
            $check_user = DB::table('user')->where('id_karyawan', $req->input('nik') )->first();

            if (count($check_user)>0)
            {
                DB::table('user')->where('id_karyawan', $req->input('nik') )->update([
                    'id_user' => $req->input('nik'),
                    'id_karyawan' => $req->input('nik'),
                    'psb_remember_token' => NULL,
                    'witel' => $req->input('witel'),
                    'updated_user' => session('auth')->id_karyawan
                ]);
            }

            $id = $req->input('nik');
            self::handleFileUpload($req, $id);
        });
    }

    public static function get_data_search($id_people)
    {
        return DB::TABLE('user_perwira as up')
        ->leftjoin('witel as w', 'w.nama_witel', '=', 'up.witel')
        ->select('up.*', 'up.id_nik As nik', 'up.mitra As mitra_pt', 'w.id_witel')
        ->where('up.id_people', $id_people)
        ->first();
    }

    public static function save_view_profile($req)
    {
        date_default_timezone_set('Asia/Makassar');
        $status = $req->input('input_status');

        if ($status == "REKRUT")
        {
            DB::transaction( function() use($req)
            {
                DB::table('perwira_recruitment')->where('id_recruitment', $req->input('id') )->update([
                    'status_pelamar' => 'REKRUT',
                    'modified_by' => session('auth')->id_nik
                ]);
            });
        }
        elseif (strpos($status, 'ACCEPTED') !== false )
        {

            DB::transaction( function() use($req, $status){
                DB::table('perwira_recruitment')->where('id_recruitment', $req->input('id') )->update([
                    'status_pelamar' => $status,
                    'log_pelamar' => 'REKRUT',
                    'modified_by' => session('auth')->id_nik
                ]);
            });

            $data = DB::table('perwira_recruitment')->where('id_recruitment', $req->input('id') )->first();

            if ($req->input('nik') <> "")
            {
                $nik = $req->input('nik');
            }
            else
            {
                $nik = $data->noktp;
            }

            if ($req->input('laborcode') <> "")
            {
                $ket_akun = '1';
            }
            else
            {
                $ket_akun = '0';
            }
            // insert employee
            DB::transaction( function() use($req, $nik, $data, $ket_akun)
            {
                $id = DB::table('1_2_employee')->insertGetId([
                    'nik'               => $nik,
                    'laborcode'         => $req->input('laborcode'),
                    'noktp'             => $data->noktp,
                    'nama'              => $data->nama,
                    'status_nik'        => 'Ada',
                    'jk'                => $data->jk,
                    'goldar'            => $data->goldar,
                    'agama'             => $data->agama,
                    'provinsi'          => $data->provinsi,
                    'kota'              => $data->kota,
                    'kecamatan'         => $data->kecamatan,
                    'kelurahan'         => $data->kelurahan,
                    'email'             => $data->email,
                    'ibu'               => $data->ibu,
                    'telpon_2'          => $data->telpon_2,
                    'telpon_2_nm'       => $data->telpon_2_nm,
                    'npwp'              => $data->npwp,
                    'last_study'        => $data->last_study,
                    'lvl_pendidikan'    => $data->lvl_pendidikan,
                    'tgllulus'          => $data->tgllulus,
                    'jurusan'           => $data->jurusan,
                    'nokk'              => $data->nokk,
                    'status_perkawinan' => $data->status_perkawinan,
                    'tgl_lahir'         => $data->tgl_lahir,
                    'tempat_lahir'      => $data->tempat_lahir,
                    'alamat'            => $data->alamat,
                    'no_telp'           => $data->no_telp,
                    'no_wa'             => $data->no_wa,
                    'tanggal_aju'       => date('Y-m-d H:i:s'),
                    'last_update'       => date('Y-m-d H:i:s'),
                    'dateUpdate'        => date('Y-m-d H:i:s'),
                    'ACTIVE'            => '1',
                    'ket_brevet'        => '0',
                    'ket_akun'          => $ket_akun,
                    'updateBy'          => 'PERWIRA'
                ]);

                $check = DB::table('perwira_insert_nik')->where('people_id', $id)->first();

                if (count($check)>0)
                {
                    DB::table('perwira_insert_nik')->where('people_id', $id)->update([
                        'people_id' => $id,
                        'tgl_usulan' => $req->input('tgl_usulan'),
                        'tgl_selesai' => $req->input('tgl_selesai'),
                        'id_direktorat' => $req->input('direktorat'),
                        'id_subgroup' => $req->input('subgroup'),
                        'id_posisi_mitra' => $req->input('posisi_mitra'),
                        'id_perusahaan_mitra' => $req->input('perusahaan_mitra'),
                        'nik_waspang' => $req->input('nik_waspang'),
                        'modified_by' => session('auth')->id_karyawan
                    ]);
                } else {
                    DB::table('perwira_insert_nik')->insert([
                        'people_id' => $id,
                        'tgl_usulan' => $req->input('tgl_usulan'),
                        'tgl_selesai' => $req->input('tgl_selesai'),
                        'id_direktorat' => $req->input('direktorat'),
                        'id_subgroup' => $req->input('subgroup'),
                        'id_posisi_mitra' => $req->input('posisi_mitra'),
                        'id_perusahaan_mitra' => $req->input('perusahaan_mitra'),
                        'nik_waspang' => $req->input('nik_waspang'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => session('auth')->id_karyawan
                    ]);
                }
            });
        }
        elseif ($status == "REJECTED")
        {
            DB::transaction( function() use($req){
                DB::table('perwira_recruitment')->where('id_recruitment', $req->input('id') )->update([
                    'status_pelamar' => 'NEW',
                    'log_pelamar' => 'REJECTED',
                    'modified_by' => session('auth')->id_nik
                ]);
            });
        }
    }

    public static function check_tele_user($user, $pwd)
    {
        return DB::table('user')->where([
            ['id_user', $user],
            ['password', $pwd]
        ])->first();
    }

    public static function get_session_id_nik($nik)
    {
        return DB::table('user_perwira')->where('id_nik', $nik)->first();
    }

    public static function save_id_chat($nik, $chat_id, $nama)
    {
        DB::table('user_perwira')->where('id_nik', $nik)
        ->update([
            'chat_id' => $chat_id,
            'username_telegram' => $nama
        ]);
    }

    public static function update_employy()
    {
        // data Mitra
        $get_data = DB::table('employee_mitra')->get();

        foreach($get_data as $data)
        {
            $check_data = DB::table('1_2_employee')->where('nik', $data->nik)->first();
            if(count($check_data)>0)
            {
                return DB::transaction( function() use($check_data, $data){
                    DB::table('1_2_employee')->where('id_people', $check_data->id_people)->update([
                    'nik' => $data->nik,
                    'nik_amija' => $data->nik,
                    'nama' => $data->nama,
                    'jk' => $data->jenis_kelamin,
                    'noktp' => str_replace("'","",$data->no_ktp),
                    'email' => $data->email,
                    'telpon_2' => str_replace("'","",$data->no_telepon_keluarga),
                    'telpon_2_nm' => $data->nama_keluarga,
                    'npwp' => str_replace("'","",$data->no_npwp),
                    'lvl_pendidikan' => $data->pendidikan_terakhir,
                    'bpjs' => str_replace("'","",$data->no_bpjs),
                    'tgl_lahir' => $data->tanggal_lahir,
                    'tempat_lahir' => $data->tempat_lahir,
                    'alamat' => $data->alamat,
                    'position' => $data->position_title,
                    'no_telp' => str_replace("'","",$data->no_telepon)
                    ]);

                    print_r("success \n");
                });
            }
            else
            {
                print_r("empty \n");
            }
        }

        // data TA
        $get_data = DB::table('employee_om_ta')->get();

        foreach($get_data as $data)
        {
            $nik = str_replace("'","",$data->nik);
            $check_data = DB::table('1_2_employee')->where('nik', $nik)->first();

            if(count($check_data)>0)
            {
                return DB::transaction( function() use($check_data, $data){
                    DB::table('1_2_employee')->where('id_people', $check_data->id_people)->update([
                    'position' => $data->position_title
                    ]);
                    print_r("success \n");
                });
            }
            else
            {
                print_r("empty \n");
            }
        }
    }

    public static function getdetail_ktp($nik)
    {
        return DB::table('perwira_recruitment_fileUpload')->where('ktp_no', $nik)->get();
    }

    public static function get_total_pelamar_front($jenis)
    {
        switch ($jenis) {
            case 'pelamar':
                $data= DB::table('perwira_recruitment')->where('status_pelamar','NEW')->whereNull('log_pelamar')->get();
            break;
            case 'rekrut':
                $data= DB::table('perwira_recruitment As pr')
                ->leftjoin('user_perwira As up', 'pr.modified_by', '=', 'up.id_nik')
                ->select('pr.*', 'up.mitra')
                ->where('status_pelamar', 'REKRUT')
                ->get();
            break;
            case 'rekrut_mit':
                $data= DB::table('perwira_recruitment As pr')
                ->leftjoin('user_perwira As up', 'pr.modified_by', '=', 'up.id_nik')
                ->select(DB::RAW('COUNT(pr.id_recruitment) as jml'), 'up.mitra')
                ->where('status_pelamar', 'REKRUT')
                ->GroupBy('up.mitra')
                ->get();
            break;
            case 'rejected':
                $data= DB::table('perwira_recruitment')->where('status_pelamar','NEW')->where('log_pelamar','REJECTED')->get();
            break;
            case 'accept':
                $data= DB::table('perwira_recruitment As pr')
                ->leftjoin('user_perwira As up', 'pr.modified_by', '=', 'up.id_nik')
                ->select(DB::RAW('COUNT(pr.id_recruitment) as jml'), 'up.mitra')
                ->where('status_pelamar','ACCEPTED')
                ->GroupBy('up.mitra')
                ->get();
            break;
        }

        return $data;
    }

    public static function get_all_direct()
    {
        return DB::SELECT('SELECT id, direktorat as text FROM perwira_direktorat');
    }

    public static function get_all_mitra()
    {
        return DB::SELECT('SELECT mitra_amija_pt as id, mitra_amija_pt as text FROM mitra_amija WHERE witel = "'.session('auth')->witel.'" GROUP BY mitra_amija_pt');
    }

    public static function get_up_ID($id)
    {
        return DB::table('user_perwira')->where('id_people', $id)->first();
    }

    public static function update_api_loc($jenis, $data)
    {
        DB::transaction( function() use($jenis, $data)
        {
            switch ($jenis) {
                case 'prov':
                    DB::table('api_provinsi')->insert([
                        'id' => $data->id,
                        'nama' => $data->nama,
                    ]);
                break;
                case 'kota':
                    DB::table('api_kota')->insert([
                        'id_kota' => $data->id,
                        'id_provinsi' => $data->id_provinsi,
                        'nama' => $data->nama,
                    ]);
                break;
                case 'kecamatan':
                    DB::table('api_kecamatan')->insert([
                        'id_kecamatan' => $data->id,
                        'id_kota' => $data->id_kota,
                        'nama' => $data->nama,
                    ]);
                break;
                case 'kelurahan':
                    DB::table('api_kelurahan')->insert([
                        'id_kelurahan' => $data->id,
                        'id_kecamatan' => $data->id_kecamatan,
                        'nama' => $data->nama,
                    ]);
                break;
            }
        });
    }

    public static function absen_tomman_submit()
    {
        //marina
        $auth = session('auth');
        User::absen($auth->id_karyawan);

        $chat_id = "-200657991";
        $msg = "User ".$auth->id_karyawan."/".$auth->nama." sudah absen, silahkan TL yg bersangkutan memverifikasi kehadiran nya.tks";

        $botToken = '207659566:AAFY7LKIrJ2vYyaGohyYDzuIOS3tOOwV3fE';
        $website="https://api.telegram.org/bot".$botToken;
        $params=[
          'chat_id' => $chat_id,
          'parse_mode' => 'html',
          'text' => "$msg",
        ];
        $ch = curl_init();
        $optArray = array(
          CURLOPT_URL => $website.'/sendMessage',
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_POST => 1,
          CURLOPT_POSTFIELDS => $params,
        );
        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        curl_close($ch);

    }

    public static function api_provinsi()
    {
        return DB::SELECT('SELECT * FROM api_provinsi');
    }

    public static function api_kota($id_provinsi)
    {
        return DB::SELECT('SELECT id_kota as id, nama as text FROM api_kota WHERE id_provinsi = "'.$id_provinsi.'"');
    }

    public static function api_kecamatan($id_kota)
    {
        return DB::SELECT('SELECT id_kecamatan as id, nama as text FROM api_kecamatan WHERE id_kota = "'.$id_kota. '"');
    }

    public static function api_kelurahan($id_kecamatan)
    {
        return DB::SELECT('SELECT id_kelurahan as id, nama as text FROM api_kelurahan WHERE id_kecamatan = "'.$id_kecamatan. '"');
    }

    public static function save_or_update_org($req)
    {
        $check = DB::Table('perwira_organisasi')->where('people_id', $req->nik_user)->first();
        $status = 0;

        // if(file_exists('https://apps.telkomakses.co.id/wimata/photo/crop_'.$req->nik_user.'.jpg') )
        // {
        //     $status = 1;
        // }

        if($check)
        {
            DB::Table('perwira_organisasi')->where('people_id', $req->nik_user)->update([
                'parent_id' => $req->nik_jabatan,
                'gambar'    => $status
            ]);
        }
        else
        {
            DB::Table('perwira_organisasi')->insert([
                'people_id' => $req->nik_user,
                'parent_id' => $req->nik_jabatan,
                'gambar'    => $status
            ]);
        }

        self::handle_foto_org($req);
    }

    private static function handle_foto_org($req)
	{
		if ($req->hasFile('photo-profile') )
        {
            $hdd = DB::TABLE('hdd')->first();
            $path = public_path() . '/'.$hdd->public.'/perwira/organisasi/'.$req->nik_user.'/';

            if (!file_exists($path) ) {
                if (!mkdir($path, 0770, true) ) {
                    return 'Gagal Menyimpan File';
                }
            }

            if ($req->hasFile('photo-profile') ) {
                $file = $req->file('photo-profile');
                try {
                    $filename = 'profile_'. $req->nik_user .'.jpg';
                    $file->move("$path", "$filename");
                } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                    return 'Gagal Menyimpan '. $path;
                }
            }
        }
	}
}