<?php
namespace App\DA;

use cURL;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

class LoginModel
{
	public function login($user, $pass)
	{
		return DB::select('
		SELECT up.* , pl.level_name, r.id_regu, r.mainsector
		FROM user_perwira up
		LEFT JOIN perwira_level pl ON up.perwira_level = pl.level_id
		LEFT JOIN regu r ON (r.nik1 = up.id_user OR r.nik2 = up.id_user) AND r.ACTIVE = 1
		LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
		WHERE up.id_user = ? AND up.password = MD5(?)
		GROUP BY up.id_user
		ORDER BY r.date_created DESC
		', [$user, $pass]);
	}
	public function loginByToken($token)
	{
		return DB::select('
		SELECT up.* , pl.level_name, r.id_regu, r.mainsector
		FROM user_perwira up
		LEFT JOIN perwira_level pl ON up.perwira_level = pl.level_id
		LEFT JOIN regu r ON (r.nik1 = up.id_user OR r.nik2 = up.id_user) AND r.ACTIVE = 1
		LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
		WHERE up.psb_remember_token = ?
		GROUP BY up.id_user
		ORDER BY r.date_created DESC
		', [$token]);
	}

	public function remembertoke($localUser, $token, $jenis)
	{
		if($jenis == 'login_mom')
		{
			DB::connection('data_center')->table('master_karyawan')->Where('id_karyawan', $localUser->id_karyawan)->update([
				'token' => $token
			]);
		}
		else
		{
			return DB::table('user')->where('id_user', $localUser->id_user)
			->update(['psb_remember_token' => $token]);
		}
	}

	public static function sso_login($username, $password)
	{

		$url = 'http://api.telkomakses.co.id/API/sso/auth_sso_post.php';
		$data = 'username=' . $username . '&password=' . $password;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		$hasil = json_decode($response, true);


		return $hasil;
	}

	public static function login_mom($username, $password)
	{
		return DB::connection('data_center')
		->table('master_karyawan As a')
    ->select('NAME', 'a.*', 'position_name', 'level_name', 'unit_name', 'witel_name As witel', 'mitra_name')
    ->leftJoin('master_position As b', 'a.id_position', '=', 'b.position_id')
    ->leftJoin('master_level As c', 'a.id_level', '=', 'c.level_id')
    ->leftJoin('master_unit As d', 'd.unit_id', '=', 'a.id_unit')
    ->leftJoin('master_witel As e', 'a.id_witel', '=', 'e.witel_id')
    ->leftJoin('master_mitra As f', 'a.id_mitra', '=', 'f.mitra_id')
    ->where([
			['nik', $username],
			['password', MD5($password)],
    ])
		->first();
	}
}