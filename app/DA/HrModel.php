<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Excel;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
class HrModel
{
    public static function save_aju_nik($req, $id, $kat)
    {
        //tes commit
        $auth = Session::get('auth');

        return DB::transaction( function() use($req, $id, $auth, $kat)
        {
            if ($kat == "create") {

                DB::table('perwira_insert_nik')->insert([
                    'people_id' => $id,
                    'tanggal_usulan' => $req->tgl_aju,
                    'created_by' => $auth->id_user
                ]);

                DB::table('1_2_employee')->where('id_people', $id)->update([
                    'status_nik' => "Process"
                ]);

                $msg['msg'] = [
                    'type' => 'success',
                    'text' => 'Pengajuan NIK Berhasil!'
                ];

                return $msg;

            } elseif ($kat == "update") {

                DB::table('perwira_insert_nik')->where('people_id', $id)->update([
                    'tanggal_selesai' => $req->tgl_selesai,
                    'modified_by' => $auth->id_user
                ]);

                DB::table('1_2_employee')->where('id_people', $id)->update([
                    'status_nik' => "Ada"
                ]);

                $msg['msg'] = [
                    'type' => 'success',
                    'text' => 'Data NIK Berhasil Diperbaharui!'
                ];

                return $msg;

            }

        });
    }

    public static function save_karyawan($req, $id)
    {
        $auth = Session::get('auth');

        return DB::transaction( function() use($req, $id, $auth)
        {
            DB::table('1_2_employee')->insert([
                'nik' => $req->nik,
                'status_nik' => $req->jenis_nik,
                'nik_amija' =>$req->nik,
                'nama' => $req->nama,
                'jk' => $req->jk,
                'goldar' => $req->goldar,
                'tgl_lahir' => $req->tgllahir,
                'tempat_lahir' => $req->kota_lahir,
                'agama' => $req->agama,
                'status' => $req->status,
                'alamat' => $req->alamat,
                'kota' => $req->kota,
                'noktp' => $req->noktp,
                'no_telp' => $req->telpon,
                'email' => $req->email,
                'ibu' => $req->maiden,
                'telpon_2' => $req->telpon_2,
                'telpon_2_nm' => $req->telpon_2_nm,
                'npwp' => $req->npwp,
                'waspang_nik' => $req->waspang_nik,
                'tanggal_aju' => $req->tanggal_aju,
                'tglmulaikerja' => $req->tglmulaikerja,
                'status_kerja' => $req->status_kerja,
                'psa' => $req->psa,
                'Witel_New' => $req->witel,
                'tglawalkerja' => $req->tglawalkerja,
                'tglakhirkerja' => $req->tglakhirkerja,
                'last_study' => $req->last_study,
                'lvl_pendidikan' => $req->lvl_pendidikan,
                'tgllulus' => $req->tgllulus,
                'jurusan' => $req->jurusan,
                'nokk' => $req->nokk,
                'status_keluarga' => $req->status_keluarga,
                'namapasangan' => $req->namapasangan,
                'anak1' => $req->anak1,
                'anak2' => $req->anak2,
                'anak3' => $req->anak3,
                'norek' => $req->norek,
                'bank' => $req->bank,
                'kantor_cabang' => $req->kantor_cabang,
                'namarek' => $req->namarek,
                'link_aj' => $req->link_aj,
                'jamsostek' => $req->jamsostek,
                'tgl_jamsostek' => $req->tgl_jamsostek,
                'bpjs' => $req->bpjs,
                'tgl_bpjs' => $req->tgl_bpjs,
                'bpjs_istri' => $req->bpjs_istri,
                'tgl_bpjs_istri' => $req->tgl_bpjs_istri,
                'bpjs_anak1' => $req->bpjs_anak1,
                'tgl_bpjs_anak1' => $req->tgl_bpjs_anak1,
                'bpjs_anak2' => $req->bpjs_anak2,
                'tgl_bpjs_anak2' => $req->tgl_bpjs_anak2,
                'bpjs_anak3' => $req->bpjs_anak3,
                'tgl_bpjs_anak3' => $req->tgl_bpjs_anak3,
                'gadas' => $req->gadas,
                'pelatihan' => $req->pelatihan,
                'seragam' => $req->seragam,
                'mitra_amija' => $req->mitra_name,
                'posisi_id' => $req->posisi
            ]);

            $files = ['file_ktp','file_pakta_integritas'];
            $that = new HrModel();

            foreach($files as $file)
            {
                $that->handleUploadData($req, $id, $file);
            }

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Pengajuan Nik Berhasil!'
            ];

            return $msg;
        });
    }

    public static function list_direktorat()
    {
        return DB::SELECT("SELECT pd.*,
        pp.pos_name,
        (SELECT COUNT(*) FROM user_perwira up WHERE up.posisi_id = pp.id) as jml
        FROM perwira_direktorat pd
        LEFT JOIN perwira_posisition pp ON pp.direktorat_id = pd.id");
    }

    public static function list_job_posisi($data)
    {
        return DB::table('perwira_posisiJob_listing')->where('jenis', $data)->get();
    }

    public static function get_Witel_13()
    {
        return DB::table('1_3_witel')->where('Reg_TA', session('auth')->region)->get();
    }

    public static function save_direktorat($req)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            DB::table('perwira_direktorat')->insert([
                'direktorat' => $req->direktorat_nm,
                'created_by' => $auth->id_user
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Direktorat Berhasil Ditambahkan!'
            ];

            return $msg;
        });
    }

    public static function show_data_position()
    {
        return DB::table('perwira_posisition As pp')
        ->leftjoin('perwira_direktorat As pd', 'pp.direktorat_id', '=', 'pd.id')
        ->select('pp.*', 'pd.direktorat', DB::RAW("count(pd.id) as total_pd") )
        ->GroupBy('pp.direktorat_id', 'pp.pos_name')
        ->get();
    }

    public static function get_detail_jobs($id_dir, $pos_nam)
    {
        return DB::table('perwira_posisition As pp')
        ->leftjoin('witel', 'witel.id_witel', 'pp.witel_id')
        ->leftjoin('perwira_direktorat As pd', 'pp.direktorat_id', '=', 'pd.id')
        ->select('pp.*', 'witel.nama_witel', 'witel.psa_witel', 'pd.direktorat')
        ->where([
            ['pp.direktorat_id', $id_dir],
            ['pp.pos_name', $pos_nam]
        ])
        ->get();
    }

    public static function save_position($req)
    {
        $auth = Session::get('auth');
        // dd($req->all() );
        return DB::transaction( function() use($req, $auth)
        {
            DB::table('perwira_posisition')->insert([
            'id_pos' => $req->posisi_id,
            'pos_name' => $req->nama_posisi,
            'level' => $req->level,
            'direktorat_id' => $req->direktorat,
            'unit' => $req->unit,
            'sub_unit' => $req->sub_unit,
            'grup' => $req->grup,
            'sub_grup' => $req->sub_grup,
            'fungsi_grup' => $req->grup_fungsi,
            'witel_id' => $req->witel,
            'regional' => $req->regional,
            'bizpart_id' => $req->bizpart_id,
            'teritory' => $req->teritory,
            'status' => $req->status,
            'created_by' => $auth->id_user
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Posisi Berhasil Ditambahkan!'
            ];

            return $msg;
        });
    }

    public static function save_sto($req)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            DB::table('area_alamat')->insert([
            'witel_id' => $req->witel_nm,
            'area_alamat' => $req->sto,
            'kode_area' => $req->kode_area,
            'created_by' => $auth->id_user,
            'created_at' => date('Y-m-d H:i:s')
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'STO Berhasil Ditambahkan!'
            ];

            return $msg;
        });
    }

    public static function list_mitra()
    {
        return DB::table('mitra_amija')
        ->where([
            [DB::RAW("SUBSTRING_INDEX(mitra_amija, ' ', '-1')"), '!=', 'DELTA'],
            ['witel', session('auth')->witel],
            ['mitra_status', 'ACTIVE']
        ])
        ->get();
    }

    public static function save_mitra($req)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            DB::table('mitra_amija')->insert([
                'mitra_amija' => $req->mitra_alias,
                'mitra_amija_pt' => $req->mitra_nm,
                'mitra_status' => $req->status,
                'ket' => $req->kategori,
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => "STO $req->mitra_nm Berhasil Ditambahkan!"
            ];

            return $msg;
        });
    }

    public static function save_witel($req)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            DB::table('witel')->insert([
            'nama_witel' => $req->witel_nm,
            'psa_witel' => $req->psa,
            'id_paket' => 1,
            'active' => 1,
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Witel Baru Berhasil Ditambahkan!'
            ];

            return $msg;
        });
    }

    public static function save_mutasi($req, $id)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            $id = DB::table('perwira_mutasi_data')->insertGetId([
                'tgl' => $req->tgl,
                'nik_atasan' => $req->nik_atasan,
                'people_id' => $req->nama,
                'jenis' => $req->jenis,
                'witel_id' => $req->witel,
                'sto_id' => $req->sto,
                'position_id' => $req->jabatan,
                'create_by' => $req->created_by,
                'approve_by' => $req->approve_by,
                'known_by' => $req->known_by,
                'created_by' => $auth->id_user
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Data Mutasi Berhasil Ditambahkan!'
            ];

            return $msg;
        });
    }

    public static function save_app_mutasi($req, $id)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $id, $auth)
        {
            DB::table('perwira_mutasi_data')->where('id', $id)->update([
                'status' => $req->status,
                'modified_by' => $auth->id_user
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Persetujuan Mutasi Berhasil Diperbaharui!'
            ];

            $that = new HrModel();
            $that->handleUploadData_ren($req, $req->nik, 'berkas_mutasi', 'berkas');
            return $msg;
        });

    }

    public static function list_ttd()
    {
        return DB::table('perwira_ttd')->get();
    }

    public static function save_ttd($req, $id)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            DB::table('perwira_ttd')->insert([
            'people_id' => $req->Nik,
            'jabatan' => $req->jabatan,
            'status' => $req->status,
            'created_by' => $auth->id_user
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Data Tanda Tangan Berhasil Ditambahkan!'
            ];

            return $msg;
        });
    }
    public static function update_ttd($req, $id)
    {
        $auth = Session::get('auth');

        return DB::transaction( function() use($req, $auth, $id)
        {
            DB::table('perwira_ttd')->where('id', $id)->insert([
            'people_id' => $req->Nik,
            'jabatan' => $req->jabatan,
            'status' => $req->status,
            'created_by' => $auth->id_user
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Data Tanda Tangan Berhasil Diperbaharui!'
            ];

            return $msg;
        });
    }

    public static function find_alker($data)
    {
        return DB::TABLE('perwira_alker')->where('alker', 'like', $data.'%')->get();
    }

    public static function save_archivement($req)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            foreach($req->nik as $people_id)
            {
                $id = DB::table('perwira_archive')->insertGetId([
                    'people_id' => $people_id,
                    'tgl' => $req->tgl,
                    'title_archive' => $req->title_archive,
                    'created_by' => $auth->id_user
                ]);

                $that = new HrModel();
                $that->handleUploadData_arc($req, $id, $people_id, 'reward', 'berkas');
            }

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Data Archivement Berhasil Ditambahkan!'
            ];

            return $msg;
        });

    }

    private function handleUploadData_arc($req, $id, $nik, $fldr, $input_nm)
    {
        $path = public_path() . '/upload/tomman_hrd/'. $fldr .'/' . $id . '/' . $nik .'/';

        if (!file_exists($path) ) {
            if (!mkdir($path, 0770, true) ) {
            return 'gagal menyiapkan folder '.$path;
            }
        }

        if ($req->hasFile($input_nm) ) {
            $file = $req->file($input_nm);
            try {
            $filename = $fldr .'.'. strtolower( $file->getClientOriginalExtension() );
            $file->move("$path", "$filename");
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan '. $path;
            }
        }
    }

    private function handleUploadData_ren($req, $id, $path, $input_nm)
    {
        $path = public_path() . '/upload/tomman_hrd/'. $path .'/' . $id . '/';

        if (!file_exists($path) ) {
            if (!mkdir($path, 0770, true) ) {
            return 'gagal menyiapkan folder '.$path;
            }
        }

        if ($req->hasFile($input_nm) ) {
            $file = $req->file($input_nm);
            try {
            $filename = $path.'.'. strtolower( $file->getClientOriginalExtension() );
            $file->move("$path", "$filename");
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan '. $path;
            }
        }
    }

    public static function get_cuti_people($id)
    {
        return DB::SELECT("SELECT *, DATEDIFF(tgl_cuti_akhir, tgl_cuti_awal) as jml_cuti FROM perwira_cuti WHERE people_id = $id AND YEAR(tgl_cuti_awal) = ".date('Y') );
    }

    public static function save_cuti($req)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            $date = array_map('trim', explode('-', $req->tgl_cuti) );
            DB::table('perwira_cuti')->insert([
                'people_id' => $req->nik,
                'jenis_id' => $req->jenis_cuti,
                'alasan_cuti' => $req->alasan_cuti,
                'tgl_cuti_awal' => $date[0],
                'tgl_cuti_akhir' => $date[1],
                'created_by' => $auth->id_user
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Data Cuti Berhasil Ditambahkan!'
            ];

            return $msg;
        });
    }

    public static function find_direktorat_nm($data)
    {
        return DB::table('perwira_direktorat')->where('direktorat', $data)->first();
    }

    public static function find_direktorat_nm_ajax($data)
    {
        return DB::table('perwira_direktorat')->where('direktorat', 'like', $data.'%')->get();
    }

    public static function find_witel($data)
    {
        return DB::table('witel')->where('nama_witel', 'like', $data.'%')->orderBy('urut','ASC')->get();
    }

    public static function find_mitra($data)
    {
        return DB::table('mitra_amija')->where('kat','DELTA')
        ->where([
            ['mitra_status','ACTIVE'],
            ['mitra_amija_pt', 'like', $data.'%'],
            ['witel', session('auth')->witel]
        ])
        ->get();
    }

    private function handleUploadData($req, $id, $namefile)
    {
        $path = public_path() . '/upload4/perwira/karyawan/'.$id.'/';

        if (!file_exists($path) ) {
            if (!mkdir($path, 0770, true) ) {
            return 'Gagal Menyimpan File';
            }
        }

        if ($req->hasFile($namefile) ) {
            $file = $req->file($namefile);
            try {
            $filename = $file->getClientOriginalName() .'.'. strtolower( $file->getClientOriginalExtension() );
            $file->move("$path", "$filename");
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'Gagal Menyimpan '. $path;
            }
        }
    }

    public static function statusNIK($kat)
    {
        return DB::SELECT('SELECT
        up.*,
        pin.tanggal_usulan
        FROM user_perwira up
        LEFT JOIN perwira_insert_nik pin ON up.id_people = pin.people_id
        WHERE
        up.status_nik = "'.$kat.'"
        GROUP BY up.id_people
        ORDER BY up.dateUpdate DESC
        ');
    }

    public static function find_psa($data)
    {
      return DB::Table('witel')->where('id_witel', $data)->first();
    }

    public static function find_direktorat_sto_base_witel($data, $witel)
    {
        // dd($data, $witel);
        return DB::table('area_alamat')->where([
            ['witel_id', $witel],
            ['kode_area', 'like', '%'.$data.'%']
        ])
        ->get();
    }

    public static function find_direktorat_jabatan($data, $witel)
    {
        // dd($data, $witel);
        return DB::table('perwira_posisition')
        ->where([
            ['witel_id', $witel],
            ['pos_name', 'like', '%'.$data.'%']
        ])
        ->get();
    }

    public static function get_posisi($data)
    {
        // dd($data, $witel);
        return DB::table('perwira_posisition')
        ->leftjoin('perwira_direktorat', 'perwira_direktorat.id', 'perwira_posisition.direktorat_id')
        ->select('perwira_posisition.*', 'perwira_direktorat.direktorat')
        ->where('perwira_posisition.id', $data)->first();
    }

    public static function list_level()
    {
        return DB::table('perwira_level')->get();
    }

    public static function get_cuti_all($y)
    {
        return DB::table('perwira_cuti')
        ->leftjoin('user_perwira', 'user_perwira.id_people', '=', 'perwira_cuti.people_id')
        ->leftjoin('perwira_jenis_cuti', 'perwira_jenis_cuti.id', '=', 'perwira_cuti.jenis_id')
        ->select('perwira_cuti.*', 'user_perwira.nama', 'user_perwira.id_nik', 'perwira_jenis_cuti.cuti_nm', 'user_perwira.jk')
        ->where(DB::RAW('YEAR(tgl_cuti_awal)'), $y)
        ->get();
    }

    public static function get_table_rec($jenis, $tahun, $bulan, $jk, $umur, $provinsi, $kota, $kecamatan, $kelurahan)
    {
        $query = '';
        switch ($jenis) {
            case 'pelamar':
                $query .= "AND status_pelamar = 'NEW' ";
            break;
            case 'rekrut':
                $query .= "AND status_pelamar = 'REKRUT' ";
            break;
            case 'rejected':
                $query .= "AND status_pelamar = 'NEW' AND log_pelamar = 'REJECTED' ";
            break;
            case 'accept':
                $query .= "AND status_pelamar = 'ACCEPTED' ";
            break;
        }

        $sql_jk = $total_u_rec = $sql_daerah =  $sql_bulan = '';

        if($jk)
        {
            $jk = "('".implode("','", $jk)."')";
            $sql_jk = "AND jk IN $jk";
        }

        if($umur)
        {
            if(in_array('18_25', $umur) )
            {
                $umur_rec[] ="YEAR(NOW() ) - YEAR(tgl_lahir) BETWEEN 18 AND 25";
            }

            if(in_array('25_', $umur) )
            {
                $umur_rec[] ="YEAR(NOW() ) - YEAR(tgl_lahir) > 25";
            }

            if($umur_rec)
            {
                $total_u_rec = "AND (". implode(' OR ',$umur_rec).")";
            }
        }

        $daerah = [];

        if($provinsi && $provinsi != 'all')
        {
            $daerah[] = "provinsi = $provinsi";
        }

        if($kota && $kota != 'all')
        {
            $daerah[] = "kota = $kota";
        }

        if($kecamatan && $kecamatan != 'all')
        {
            $daerah[] = "kecamatan = $kecamatan";
        }

        if($kelurahan && $kelurahan != 'all')
        {
            $daerah[] = "kelurahan = $kelurahan";
        }

        if($daerah)
        {
            $sql_daerah = "AND ". implode(' AND ',$daerah);
        }

        if($bulan == 'today')
        {
            $sql_bulan = "AND pr.created_at LIKE '" .date('Y-m-d'). "%'";
        }
        elseif(is_numeric($bulan) )
        {
            $sql_bulan = "AND MONTH(pr.created_at) = $bulan";
        }

        $sql = DB::SELECT("SELECT pr.*, CAST(noktp AS CHAR) as noktp, ak.nama as nama_kota
            FROM perwira_recruitment pr
            LEFT JOIN api_provinsi ap ON pr.provinsi = ap.id
            LEFT JOIN api_kota ak ON pr.kota = ak.id_kota
            LEFT JOIN api_kecamatan akc ON pr.kecamatan = akc.id_kecamatan
            LEFT JOIN api_kelurahan akl ON pr.kelurahan = akl.id_kelurahan
            WHERE YEAR(pr.created_at) = $tahun $query $sql_jk $sql_bulan $total_u_rec $sql_daerah
        ");

        return $sql;
    }

    public static function list_archive()
    {
        return DB::table('perwira_archive')
        ->leftjoin('user_perwira', 'user_perwira.id_people', '=', 'perwira_archive.people_id')
        ->select('perwira_archive.*', 'user_perwira.id_nik', 'user_perwira.nama')
        ->get();
    }

    public static function data_ttd($id)
    {
        return DB::table('perwira_ttd')
        ->leftjoin('user_perwira', 'user_perwira.id_people', '=', 'perwira_ttd.people_id')
        ->select('perwira_ttd.*', 'user_perwira.id_nik', 'user_perwira.nama')
        ->where('id', $id)->get();
    }

    public static function get_detail_user($data)
    {
        return DB::SELECT("SELECT up.*,
        c_pmd.c_nik_atasan as c_nik_atasan,
        c_pmd.c_jenis as c_jenis,
        c_pmd.c_witel_id as c_witel_id,
        c_pmd.c_sto_id as c_sto_id,
        c_pmd.c_position_id as c_position_id,
        c_pmd.c_create_by as c_create_by,
        c_pmd.c_approve_by as c_approve_by,
        c_pmd.c_known_by as c_known_by,
        p_pmd.p_nik_atasan as p_nik_atasan,
        p_pmd.p_jenis as p_jenis,
        p_pmd.p_witel_id as p_witel_id,
        p_pmd.p_sto_id as p_sto_id,
        p_pmd.p_position_id as p_position_id,
        p_pmd.p_create_by as p_create_by,
        p_pmd.p_approve_by as p_approve_by,
        p_pmd.p_known_by as p_known_by,
        c_pp.pos_name as c_pos_name,
        c_pp.level as c_level,
        c_pp.unit as c_unit,
        c_pp.sub_unit as c_sub_unit,
        c_pp.grup as c_grup,
        c_pp.sub_grup as c_sub_grup,
        c_pp.fungsi_grup as c_fungsi_grup,
        c_pp.regional as c_regional,
        c_pp.teritory as c_teritory,
        c_pp.bizpart_id as c_bizpart_id,
        c_pp.status as c_status,
        p_pp.pos_name as p_pos_name,
        p_pp.level as p_level,
        p_pp.unit as p_unit,
        p_pp.sub_unit as p_sub_unit,
        p_pp.grup as p_grup,
        p_pp.sub_grup as p_sub_grup,
        p_pp.fungsi_grup as p_fungsi_grup,
        p_pp.regional as p_regional,
        p_pp.teritory as p_teritory,
        p_pp.bizpart_id as p_bizpart_id,
        p_pp.status as p_status,
        p_pd.direktorat as p_direktorat,
        c_pd.direktorat as c_direktorat,
        c_w.nama_witel as c_nama_witel,
        p_w.nama_witel as p_nama_witel,
        c_aa.kode_area as c_kode_area,
        p_aa.kode_area as p_kode_area,
        p_pd.id as p_direktorat_id,
        c_pd.id as c_direktorat_id,
        c_w.id_witel as c_nama_witel_id,
        p_w.id_witel as p_nama_witel_id,
        c_aa.id_area as c_kode_area_id,
        p_aa.id_area as p_kode_area_id,
        (SELECT CONCAT(up_s.nama, ' (', c_pmd.c_create_by, ')' ) FROM user_perwira up_s WHERE c_pmd.c_create_by = up_s.id_nik) as nm_c_create_by,
        (SELECT CONCAT(up_s.nama, ' (', c_pmd.c_approve_by, ')' ) FROM user_perwira up_s WHERE c_pmd.c_approve_by = up_s.id_nik) as nm_c_approve_by,
        (SELECT CONCAT(up_s.nama, ' (', c_pmd.c_known_by, ')' ) FROM user_perwira up_s WHERE c_pmd.c_known_by = up_s.id_nik) as nm_c_known_by,
        (SELECT CONCAT(up_s.nama, ' (', p_pmd.p_create_by, ')' ) FROM user_perwira up_s WHERE p_pmd.p_create_by = up_s.id_nik) as nm_p_create_by,
        (SELECT CONCAT(up_s.nama, ' (', p_pmd.p_approve_by, ')' ) FROM user_perwira up_s WHERE p_pmd.p_approve_by = up_s.id_nik) as nm_p_approve_by,
        (SELECT CONCAT(up_s.nama, ' (', p_pmd.p_known_by, ')' ) FROM user_perwira up_s WHERE p_pmd.p_known_by = up_s.id_nik) as nm_p_known_by
        FROM user_perwira up
          LEFT JOIN (
            SELECT SUBSTRING_INDEX(GROUP_CONCAT(nik_atasan ORDER BY id DESC), ',', 1) as c_nik_atasan,
            SUBSTRING_INDEX(GROUP_CONCAT(jenis ORDER BY id DESC), ',', 1) as c_jenis,
            SUBSTRING_INDEX(GROUP_CONCAT(witel_id ORDER BY id DESC), ',', 1) as c_witel_id,
            SUBSTRING_INDEX(GROUP_CONCAT(sto_id ORDER BY id DESC), ',', 1) as c_sto_id,
            SUBSTRING_INDEX(GROUP_CONCAT(position_id ORDER BY id DESC), ',', 1) as c_position_id,
            SUBSTRING_INDEX(GROUP_CONCAT(create_by ORDER BY id DESC), ',', 1) as c_create_by,
            SUBSTRING_INDEX(GROUP_CONCAT(approve_by ORDER BY id DESC), ',', 1) as c_approve_by,
            SUBSTRING_INDEX(GROUP_CONCAT(known_by ORDER BY id DESC), ',', 1) as c_known_by,
            people_id
            FROM perwira_mutasi_data
            WHERE status = 1 GROUP BY people_id) as c_pmd ON c_pmd.people_id = up.id_people
          LEFT JOIN witel c_w ON c_w.id_witel = c_pmd.c_witel_id
          LEFT JOIN area_alamat c_aa ON c_aa.id_area = c_pmd.c_sto_id
          LEFT JOIN perwira_posisition c_pp ON c_pp.id = c_pmd.c_position_id
          LEFT JOIN perwira_direktorat c_pd ON c_pd.id = c_pp.direktorat_id
          LEFT JOIN (
            SELECT SUBSTRING_INDEX(GROUP_CONCAT(nik_atasan ORDER BY id DESC), ',', 1) as p_nik_atasan,
            SUBSTRING_INDEX(GROUP_CONCAT(jenis ORDER BY id DESC), ',', 1) as p_jenis,
            SUBSTRING_INDEX(GROUP_CONCAT(witel_id ORDER BY id DESC), ',', 1) as p_witel_id,
            SUBSTRING_INDEX(GROUP_CONCAT(sto_id ORDER BY id DESC), ',', 1) as p_sto_id,
            SUBSTRING_INDEX(GROUP_CONCAT(position_id ORDER BY id DESC), ',', 1) as p_position_id,
            SUBSTRING_INDEX(GROUP_CONCAT(create_by ORDER BY id DESC), ',', 1) as p_create_by,
            SUBSTRING_INDEX(GROUP_CONCAT(approve_by ORDER BY id DESC), ',', 1) as p_approve_by,
            SUBSTRING_INDEX(GROUP_CONCAT(known_by ORDER BY id DESC), ',', 1) as p_known_by,
            people_id
            FROM perwira_mutasi_data
            WHERE status = 1 GROUP BY people_id) as p_pmd ON p_pmd.people_id = up.id_people
          LEFT JOIN witel p_w ON p_w.id_witel = p_pmd.p_witel_id
          LEFT JOIN area_alamat p_aa ON p_aa.id_area = p_pmd.p_sto_id
          LEFT JOIN perwira_posisition p_pp ON p_pp.id = p_pmd.p_position_id
          LEFT JOIN perwira_direktorat p_pd ON p_pd.id = p_pp.direktorat_id
          WHERE up.id_people = $data");
    }

    public static function find_user_rekrut()
    {
        return DB::Table('user_perwira')
        ->leftjoin('perwira_brevert_user', 'user_perwira.noktp', '=', 'perwira_brevert_user.noktp')
        ->select('user_perwira.*')
        ->where('status_pelamar', '=', 'REKRUT')
        ->whereNull('perwira_brevert_user.noktp')
        ->get();
    }

    public static function search_id_people($id)
    {
        return DB::table('user_perwira')->where('id_people', $id)->first();
    }

    public static function get_list_createU()
    {
        return DB::TABLE('user_perwira')->select('nama','id_nik','witel','mitra','psb_level','maintenance_level','pt2_level','perwira_level','proc_level','promise_level')->orderBy('dateUpdate','DESC')->get();
    }

    public static function createdSave_user($req)
    {
        // dd($req->all() );
        return DB::transaction(function() use($req) {
            $check = DB::table('1_2_employee')->where('id_people', $req->nik)->first();
            if ($req->jenis == "Tomman")
            {
                $get_field = DB::table('perwira_level_user')->where('tomman', $req->tomman)->first();

                $update_user = [
                    $get_field->field_db => $req->level,
                    'datetime_updated'   => date('Y-m-d H:i:s')
                ];

                if($get_field->tomman == 'PSB' && $get_field->level == 9)
                {
                    $update_user['is_jointer'] = 1;
                    $update_user['pt2_level'] = 0;
                }

                DB::table('user')->where('id_user', $check->nik)->update($update_user);

            } else {
                dd("Maaf Belum Tersedia :)");
            }
        });
    }

    public static function get_user_ajax($req)
    {
        $search = trim($req->searchTerm, '');
        $sql = '';

        if(session('auth')->perwira_level != 1)
        {
            $sql .= 'AND up.witel = "'.session('auth')->witel.'" ';
        }

        if (preg_match("/^[a-zA-Z -]+$/",$search) ) {
            $get_user = DB::SELECT('SELECT up.id_people as id, UPPER(up.nama) as text, up.id_nik FROM user_perwira up WHERE up.nama LIKE "%'.$search.'%" '.$sql.' LIMIT 10');
        } else {
            $get_user = DB::SELECT('SELECT up.id_people as id, UPPER(up.nama) as text, up.id_nik FROM user_perwira up WHERE (up.id_nik LIKE "%'.$search.'%" OR up.noktp LIKE "%'.$search.'%") '.$sql.' LIMIT 10');
        }
        return $get_user;
    }

    public static function get_listM()
    {
        return DB::table('mitra_amija')
        ->where([
            ['mitra_status','ACTIVE'],
            ['witel', session('auth')->witel]
        ])
        ->groupBy('mitra_amija')->orderBy('kat','asc')->get();
    }

    public static function get_Witel()
    {
        return DB::table('witel')->orderBy('urut','ASC')->get();
    }

    public static function get_alamat_d()
    {
        return DB::table('area_alamat')->leftJoin('witel','area_alamat.witel_id','=','witel.id_witel')->groupBy('area_alamat.kode_area')->get();
    }

    public static function get_detail_cuti($id)
    {
        return DB::table('user_perwira as up')
        ->leftjoin('perwira_cuti as pc', 'pc.people_id', '=', 'up.id_people')
        ->select('pc.*', 'up.id_nik', 'up.nama')
        ->where('pc.id', $id)
        ->get();
    }

    public static function get_jobdesk()
    {
        return DB::table('perwira_posisi_mitra')->get();
    }

    public static function monolog_karyawan($status,$witel)
    {
        switch($status)
        {
            case 'tidak_terdaftar':
                $where = 'up.id_nik IS NULL';
            break;

            case 'karyawan_aktif':
                $where = 'mtt.STATUS_KARYAWAN LIKE "ACTIVE"';
            break;

            case 'karyawan_nonaktif':
                $where = 'mtt.STATUS_KARYAWAN LIKE "NON ACTIVE%"';
            break;

            case 'mytech_scmt_active':
                $where = 'mtt.MY_TECH LIKE "ACTIVE%" AND mtt.SCMT LIKE "Active%" AND mtt.STATUS_KARYAWAN = "ACTIVE"';
            break;

            case 'mytech_scmt_blanks':
                $where = 'mtt.MY_TECH LIKE "ACTIVE%" AND mtt.SCMT IS NULL AND mtt.STATUS_KARYAWAN = "ACTIVE"';
            break;

            case 'scmt_mytech_blanks':
                $where = 'mtt.SCMT LIKE "Active%" AND mtt.MY_TECH IS NULL AND mtt.STATUS_KARYAWAN = "ACTIVE"';
            break;

            case 'scmt_mytech_active':
                $where = 'mtt.SCMT LIKE "Inactive%" AND mtt.MY_TECH LIKE "ACTIVE%" AND mtt.STATUS_KARYAWAN = "ACTIVE"';
            break;
        }
        return DB::SELECT('SELECT
            up.id_nik,
            up.nama as up_nama,
            up.mitra,
            up.witel,
            mtt.*
        FROM monolog_teknisi_tr6 mtt
        LEFT JOIN user_perwira up ON mtt.NIK = up.id_nik
        WHERE '.$where.' AND mtt.WITEL_HR = "'.$witel.'" ORDER BY up.dateUpdate DESC
        ');
    }

    public static function uploadDataSave($req)
    {
        $ket = $req->input('keterangan');

        if ($ket == 'MONOLOG')
        {
            return DB::transaction(function() use($req) {
                DB::table('monolog_teknisi_tr6')->truncate();
                Excel::load($req->file('upload'), function($reader) {
                    $bulks = $reader->toArray();

                    foreach($bulks as $bulk)
                    {
                        if (count($bulk) == '11')
                        {
                            DB::table('monolog_teknisi_tr6')->insert([
                                'LABORCODE' => $bulk['laborcode'],
                                'NIK' => str_replace('.0','',$bulk['nik']),
                                'NAMA' => $bulk['nama'],
                                'NAMA_HR' => $bulk['nama_hr'],
                                'REGIONAL_HR' => $bulk['regional_hr'],
                                'WITEL_HR' => $bulk['witel_hr'],
                                'WH' => $bulk['wh'],
                                'STATUS_KARYAWAN' => $bulk['status_karyawan'],
                                'NTE' => str_replace('.0','',$bulk['nte']),
                                'MY_TECH' => $bulk['my_tech'],
                                'SCMT' => $bulk['scmt']
                            ]);
                        } else {
                            return back()->with('alerts', [
                                ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Format File Tidak Didukung']
                            ]);
                        }
                    }
                });

                return redirect('/hr/uploadData')->with('alerts', [
                    ['type' => 'success', 'text' => '<strong>BERHASIL</strong> Menyimpan Data Monolog']
                ]);
            });



        }
        elseif($ket == 'BREVERT_TEKNISI')
        {
            Excel::load($req->file('upload'), function($reader) use($req){
                $bulks = $reader->toArray();
                dd($req->input(),$bulks);
            });
        }
        elseif($ket == 'EMPLOYEE_TA')
        {
            Excel::load($req->file('upload'), function($reader) use($req){
                $bulks = $reader->toArray();
                dd($req->input(),$bulks);
            });
        }
        elseif(in_array($ket, ['EMPLOYEE_MITRA', 'EMPLOYEE_MITRA_NONAKTIF']) )
        {
            $status = 'NONAKTIF';

            if($ket == 'EMPLOYEE_MITRA')
            {
                $status = 'AKTIF';
            }

            if($req->file('upload')->getClientOriginalExtension() != 'xlsx')
            {
                return back()->with('alerts', [
                    ['type' => 'danger', 'text' => '<strong>GAGAL</strong> File Harus Berupa .Xlsx']
                ]);
            }

            $reader = new Xlsx();
            \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
            $spreadSheet = $reader->load($req->file('upload') );
            $data = $spreadSheet->getSheet(0)->toArray();
            unset($data[0]);

            return DB::transaction(function() use($data, $status) {
                $no = 0;

                DB::table('perwira_user_regional')->where('JENIS', $status)->truncate();

                foreach($data as $v)
                {
                    if ( (int)$v[0] != 0)
                    {
                        ++$no;
                        DB::table('perwira_user_regional')->insert([
                            'NIK'                        => (int)$v[0],
                            'NAMA'                       => $v[1],
                            'TEMPAT_LAHIR'               => $v[2],
                            'TANGGAL_LAHIR'              => $v[3],
                            'NO_KTP'                     => $v[4],
                            'NO_NPWP'                    => $v[5],
                            'NO_BPJS'                    => $v[6],
                            'ALAMAT'                     => $v[7],
                            'EMAIL'                      => $v[8],
                            'NO_TELEPON'                 => $v[9],
                            'NO_TELEPON_YANG_DI_HIBUNGI' => $v[10],
                            'NAMA_KELUARGA'              => $v[11],
                            'JENIS_KELAMIN'              => $v[12],
                            'PENDIDIKAN_TERAKHIR'        => $v[13],
                            'STATUS_NAKER'               => $v[14],
                            'DIREKTORAT'                 => $v[15],
                            'NAMA_PERUSAHAAN'            => $v[16],
                            'POSITION_TITLE'             => $v[17],
                            'REGIONAL'                   => $v[18],
                            'WITEL'                      => $v[19],
                            'JENIS'                      => $status,
                        ]);
                    }
                }
                return back()->with('alerts', [
                    ['type' => 'success', 'text' => '<strong>BERHASIL</strong> '. $no .' Data Sudah Disimpan!']
                ]);
            });
        }
    }

    public static function get_pelamar_kota($id)
    {
        $query = '';

        if($id != 'all')
        {
            $query = "ak.id_kota = $id AND";
        }
        return DB::SELECT("SELECT pr.*, ak.nama as nama_kota
        FROM perwira_recruitment pr
        LEFT JOIN api_kota ak ON pr.kota = ak.id_kota
        WHERE
        $query pr.status_pelamar = 'NEW' ");
    }

    public static function get_recruit_comp($id)
    {
        return DB::table('perwira_recruitment As pr')
        ->leftjoin('user_perwira As up', 'pr.modified_by', '=', 'up.id_nik')
        ->select('pr.*')
        ->where([
            ['status_pelamar', 'REKRUT'],
            ['up.mitra', $id]
        ])
        ->get();
    }

    public static function get_accepted_h($id)
    {
        return DB::table('perwira_recruitment As pr')
        ->leftjoin('user_perwira As up', 'pr.modified_by', '=', 'up.id_nik')
        ->select('pr.*')
        ->where([
            ['status_pelamar', 'ACCEPTED'],
            ['up.mitra', $id]
        ])
        ->get();
    }

    public static function get_monolog_list($id)
    {
        $sql = '';
        switch ($id) {
            case 'monolog_unre':
                $sql = "up.id_nik IS NULL";
            break;
            case 'kar_aktif':
                $sql = "mtt.STATUS_KARYAWAN LIKE 'ACTIVE'";
            break;
            case 'kar_not_aktif':
                $sql = "mtt.STATUS_KARYAWAN LIKE 'NON ACTIVE%'";
            break;
            case 'scmt_act':
                $sql = "mtt.MY_TECH LIKE 'ACTIVE%' AND mtt.SCMT LIKE 'Active%' AND mtt.STATUS_KARYAWAN = 'ACTIVE'";
            break;
            case 'scmt_blnk':
                $sql = "mtt.MY_TECH LIKE 'ACTIVE%' AND mtt.SCMT NOT LIKE 'Active%' AND mtt.STATUS_KARYAWAN = 'ACTIVE'";
            break;
          }
        return DB::SELECT('SELECT
            mtt.WITEL_HR,
            SUM(CASE WHEN mtt.SCMT LIKE "Active%" AND mtt.MY_TECH NOT LIKE "ACTIVE%" AND mtt.STATUS_KARYAWAN = "ACTIVE" THEN 1 ELSE 0 END) as scmt_mytech_blanks
            FROM monolog_teknisi_tr6 mtt
            LEFT JOIN user_perwira up ON mtt.NIK = up.id_nik
            WHERE mtt.WITEL_HR = "WITEL BANJARMASIN"
        ');
    }

    public static function list_reg_karyawan($id)
    {
        $sql = '';

        switch ($id) {
            case 'tidak_terdaftar':
                $sql = "emp.nik IS NULL";
            break;
            case 'aktif_reg':
                $sql = "emp.nik IS NOT NULL AND pur.JENIS = 'AKTIF'";
            break;
            case 'non_aktif_reg':
                $sql = "emp.nik IS NOT NULL AND pur.JENIS = 'NONAKTIF'";
            break;
            case 'has_tomman':
                $sql = "u.id_user IS NOT NULL";
            break;
            case 'aktif_tomman':
                $sql = "u.psb_remember_token IS NOT NULL";
            break;
            case 'non_aktif_tomman':
                $sql = "u.psb_remember_token IS NULL";
            break;
        }

        return DB::SELECT("SELECT pur.*, emp.id_people, u.id_user
        FROM perwira_user_regional pur
        LEFT JOIN 1_2_employee emp ON pur.NIk = emp.nik
        LEFT JOIN user u ON u.id_user = emp.nik
        WHERE $sql");
    }

    public static function list_tomman_teknisi($id)
    {
        $sql = '';

        switch ($id) {
            case 'aktif_tomman':
                $sql = "u.psb_remember_token IS NOT NULL AND emp.id_people IS NOT NULL";
            break;
            case 'non_aktif_tomman':
                $sql = "u.psb_remember_token IS NULL";
            break;
            case 'tidak_terdaftar':
                $sql = "emp.nik IS NULL";
            break;
        }

        return DB::SELECT("SELECT u.id_user, u.psb_remember_token, emp.* FROM
        user u
        LEFT JOIN 1_2_employee emp ON u.id_user = emp.nik
        WHERE $sql");
    }

    public static function get_attended($event_id)
    {
        // $att= DB::table('perwira_event_attendant As a')
        // ->leftjoin('1_2_employee As emp', 'a.nik', '=', 'emp.nik')
        // ->select(DB::RAW("(CASE WHEN emp.nama IS NOT NULL THEN CONCAT(emp.nik, ' (', emp.nama, ')') ELSE a.nik END) As nama_lengkap") )
        // ->where('event_id', $event_id)->get();
        // $att = DB::table('t1.perwira_event_attendant As a')
        // ->leftjoin('t1.1_2_employee As emp', 'a.nik', '=', 'emp.nik')
        // ->join('data_center.master_karyawan As mk', 'a.nik', '=', 'mk.nik')
        // // ->select('a.*',
        // //     DB::raw("(CASE
        // //         WHEN emp.nama IS NOT NULL THEN CONCAT(emp.nik, ' (', emp.nama, ')')
        // //         WHEN mk.nama IS NOT NULL THEN CONCAT(mk.nik, ' (', mk.nama, ')')
        // //         ELSE a.nik
        // //     END) AS nama_lengkap")
        // // )
        // ->where('event_id', $event_id)
        // ->get();

        $result1 = DB::table('perwira_event_attendant as a')
            ->leftJoin('1_2_employee as emp', 'a.nik', '=', 'emp.nik')
            ->select('a.*', DB::RAW('a.nik As nama_scan') )
            ->where('event_id', $event_id)
            ->get();

        $result2 = DB::connection('data_center')
            ->table('master_karyawan as mk')
            ->get();

        $combinedResult = [
            'result1' => $result1,
            'result2' => $result2,
        ];

        foreach($combinedResult['result1'] as $k2 => $v2)
        {
            $find = array_search($v2->nik, array_column($combinedResult['result2']->toArray(), 'nik') );

            if($find)
            {
                $get_find[$k2] = $combinedResult['result2'][$find];
                $result1[$k2]->nama_scan = $get_find[$k2]->name . ' ('. $get_find[$k2]->nik .')';
            }
        }


        $content = '
        <div class="row text-center">
            <div class="col-md-3 ">
                <div class="panel panel-success panel-dark">
                    <div class="panel-heading">
                        <span class="panel-title">Total Attendant</span>
                        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
                    </div>
                    <div class="panel-body m-x-auto">
                        <p>'.count($combinedResult['result1']).'</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">';

        foreach($combinedResult['result1'] as $at){
            $content .= '<div class="col-md-3 b-a-3 m-a-1">'.$at->nama_scan.'</div>';
        }
        $content .= '</div>';
        return $content;
    }
    public static function set_attended($param)
    {
        return DB::table('perwira_event_attendant')->insert($param);
    }
    public static function register_rfid($param)
    {
        return DB::table('perwira_rfid')->insert($param);
    }
    public static function get_absensi_byrfid($rfid,$event_id)
    {
        return DB::table('perwira_event_attendant')->where('rfid', $rfid)->where('event_id', $event_id)->first();
    }
    public static function get_event()
    {
        return DB::table('perwira_event_absensi')->get();
    }
    public static function get_event_byid($id)
    {
        return DB::table('perwira_event_absensi')->where('id', $id)->first();
    }
    public static function event_update($id,$param)
    {
        return DB::table('perwira_event_absensi')->where('id', $id)->update($param);
    }
    public static function event_insert($param)
    {
        return DB::table('perwira_event_absensi')->insert($param);
    }
    public static function event_delete($id)
    {
        return DB::table('perwira_event_absensi')->where('id', $id)->delete();
    }
    public static function get_rfid($id)
    {
        return DB::table('perwira_rfid')->where('rfid', $id)->first();
    }
}