<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Session;
use DateTime;

date_default_timezone_set("Asia/Makassar");
class ReportModel{

    public static function get_absensi($kat)
    {
        if ($kat == "belum_absen") {
            $kategori = '(d.date_created IS NULL OR DATE(d.date_created) <> "'.date('Y-m-d').'") AND';
        } elseif ($kat == "menunggu_approve") {
            $kategori = 'DATE(d.date_created) = "'.date('Y-m-d').'" AND d.approval = 0 AND';
        } elseif ($kat == "sudah_absen") {
            $kategori = 'DATE(d.date_created) = "'.date('Y-m-d').'" AND d.approval = 1 AND';
        } elseif ($kat == "total") {
            $kategori = '';
        }

        return DB::SELECT('SELECT
        d.status as status_kehadiran,
        d.date_created as waktu_absen,
        d.approval,
        d.date_approval,
        d.absen_id,
        c.nik,
        c.nama,
        ma.mitra_amija_pt
        FROM
        1_2_employee c
        LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
        LEFT JOIN mitra_amija ma ON a.mitra = ma.mitra_amija AND ma.witel = "'.session('auth')->witel.'"
        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
        LEFT JOIN (SELECT SUBSTRING_INDEX(GROUP_CONCAT(nik ORDER BY date_created DESC), ",", 1) as nik,
        SUBSTRING_INDEX(GROUP_CONCAT(date_created ORDER BY date_created DESC), ",", 1) as date_created,
        SUBSTRING_INDEX(GROUP_CONCAT(approval ORDER BY date_created DESC), ",", 1) as approval, SUBSTRING_INDEX(GROUP_CONCAT(date_approval ORDER BY date_created DESC), ",", 1) as date_approval,
        SUBSTRING_INDEX(GROUP_CONCAT(absen_id ORDER BY date_created DESC), ",", 1) as absen_id,
        SUBSTRING_INDEX(GROUP_CONCAT(status ORDER BY date_created DESC), ",", 1) as status
        FROM absen GROUP BY nik) as d ON d.nik = c.nik
        WHERE
        '.$kategori.'
        (b.TL_NIK = "'.session('auth')->id_karyawan.'" OR b.Nik_Atl = "'.session('auth')->id_karyawan.'" OR b.Nik_Atl2 = "'.session('auth')->id_karyawan.'" ) AND a.ACTIVE <> "0"
        GROUP BY c.nik
        ORDER BY d.date_created DESC
        ');
    }

    public static function approve_absen($nik)
    {
      DB::transaction( function() use($nik)
      {
        DB::table('absen')->where('nik',$nik)->where('date_created','LIKE',''.date('Y-m-d').'%')
        ->update([
          'approval' => 1,
          'date_approval' => date('Y-m-d H:n:s'),
          'absenEsok'=> 2,
          'approve_by' => session('auth')->id_karyawan
        ]);

        //absen marina
        $auth = session('auth');

        DB::table('karyawan')->where('id_karyawan', $auth->id_karyawan)->update([
          'mt_verif_absen' => DB::raw('now()')
        ]);

        DB::table('maintenance_absen_log')->insert([
          'id_user' => $nik,
          'status' => 2,
          'created_by' => $auth->id_karyawan,
          'created_at' => DB::raw('now()')
        ]);

        $chat_id = "-200657991";
        $msg = "User ".$auth->id_karyawan."/".$auth->nama." sudah di verifikasi kehadiran nya by ".$auth->id_karyawan."/".$auth->nama.".tks";
        $botToken = '207659566:AAFY7LKIrJ2vYyaGohyYDzuIOS3tOOwV3fE';
        $website="https://api.telegram.org/bot".$botToken;
        $params=[
          'chat_id' => $chat_id,
          'parse_mode' => 'html',
          'text' => "$msg",
        ];
        $ch = curl_init();
        $optArray = array(
          CURLOPT_URL => $website.'/sendMessage',
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_POST => 1,
          CURLOPT_POSTFIELDS => $params,
        );
        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        curl_close($ch);
      });
    }

    public static function absen_decline($id)
    {
        return DB::SELECT('
        SELECT
        *
        FROM user_perwira up
        LEFT JOIN absen a ON up.id_user = a.nik
        WHERE
        a.absen_id = "'.$id.'"
        ');
    }

    public static function absen_declineSave($absen_id,$keterangan)
    {
        $query = DB::table('absen')->where('absen_id',$absen_id)->update([
            'keterangan' => $keterangan,
            'approval' => 3,
            'date_approval' => date('Y-m-d h:n:s')
          ]);
        return $query;
    }

    public static function save_jenis_cut($req)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            DB::table('perwira_jenis_cuti')->insert([
            'cuti_nm' => $req->cuti_nm,
            'created_by' => $auth->id_user
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Jenis Cuti Berhasil Ditambahkan!'
            ];

            return $msg;
        });
    }

    public static function get_jenis_cut()
    {
      return DB::Table('perwira_jenis_cuti')->get();
    }

    public static function find_jenis_cut($data)
    {
      return DB::Table('perwira_jenis_cuti')->where('cuti_nm', 'like', $data.'%')->get();
    }

    public static function save_accident($req, $id)
    {
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            DB::table('perwira_accident')->insert([
                'tgl' => $req->tgl,
                'people_id' => $req->nama,
                'alamat_acc' => $req->alamat_acc,
                'detail_acc' => $req->detail_acc,
                'action' => $req->action,
                'kronolog' => $req->kronolog,
                'created_by' => $auth->id_user
            ]);

            $msg['msg'] = [
                'type' => 'success',
                'text' => 'Data Accident Berhasil Ditambahkan!'
            ];

            return $msg;
        });
    }

    public static function save_resling($req)
    {
      // dd($req->all() );
        $auth = Session::get('auth');
        return DB::transaction( function() use($req, $auth)
        {
            $data['tgl'] = $req->tgl;
            $data['people_id'] = $req->nama;
            $data['jenis'] = $req->jenis;
            $data['created_by'] = $auth->id_user;

            if($req->jenis == 'Konseling')
            {
              $data['jenis_jns'] = $req->jenis_kon;
              $data['detail_langgar'] = $req->detail_langgar;
              $data['tndk_lanjut'] = $req->tndk_lanjut;
            }
            else
            {
              $data['jenis_jns'] = $req->jenis_res;
              $data['detail_langgar'] = $req->detail_blacklst;
            }

            // dd($data);

            DB::table('perwira_resling')->insert($data);

            $msg['msg'] = [
                'type' => 'success',
                'text' => "Data $req->jenis Berhasil Ditambahkan!"
            ];

            $that = new ReportModel();
            $that->handleUploadData($req, $req->nik, 'reward', 'bukti');
            return $msg;
        });
    }

    private function handleUploadData($req, $id, $path, $input_nm)
    {
        $path = public_path() . '/upload/tomman_hrd/'. $path .'/' . $id . '/';

        if (!file_exists($path) ) {
            if (!mkdir($path, 0770, true) ) {
            return 'gagal menyiapkan folder '.$path;
            }
        }

        if ($req->hasFile($input_nm) ) {
            $file = $req->file($input_nm);
            try {
            $filename = $path .'.'. strtolower( $file->getClientOriginalExtension() );
            $file->move("$path", "$filename");
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan '. $path;
            }
        }
    }

    public static function save_Alker($req)
    {
      $auth = Session::get('auth');
      return DB::transaction( function() use($req, $auth)
      {
        $nama_alker = DB::Table('perwira_alker')->where('id', $req->alker)->first();
        DB::table('perwira_alker_brand')->insert([
            'merk' => $req->merk_alker,
            'jenis' => $req->alker,
            'created_by' => $auth->id_user
        ]);

        $msg['msg'] = [
            'type' => 'success',
            'text' => "Data $nama_alker->alker Dengan Merk $req->merk_alker Berhasil Ditambahkan!"
        ];

        return $msg;
      });
    }

    public static function get_alker($jenis)
    {
      if($jenis == 'alker')
      {
        return DB::table('perwira_alker')->get();
      }
      else
      {
        return DB::table('perwira_alker_brand')
        ->leftjoin('perwira_alker', 'perwira_alker_brand.alker_id', '=', 'perwira_alker.id')
        ->select('perwira_alker_brand.*', 'perwira_alker.alker As nama_alker', 'perwira_alker.aset', 'perwira_alker.spek')
        ->get();
      }
    }

    public static function save_NAlker($req)
    {
      $auth = Session::get('auth');
      return DB::transaction( function() use($req, $auth)
      {
          DB::table('perwira_alker')->insert([
              'alker' => $req->new_alker,
              'created_by' => $auth->id_user
          ]);

          $msg['msg'] = [
              'type' => 'success',
              'text' => "Alker $req->new_alker Berhasil Ditambahkan!"
          ];

        return $msg;
      });
    }

    public static function group_telegram()
    {
      return DB::table('group_telegram')->where('TL_NIK',session('auth')->id_karyawan)->orwhere('Nik_Atl',session('auth')->id_karyawan)->orwhere('Nik_Atl2',session('auth')->id_karyawan)->get();
    }

    public static function getAllBriefingTgl($date)
    {
      return DB::table('briefing')->whereDate('createdAt', 'LIKE', '%'.$date.'%')->where('createdBy',session('auth')->id_karyawan)->orderBy('createdAt', 'DESC')->get();
    }

    public static function getFotoByNik()
    {
      return DB::table('briefing')
      ->leftJoin('briefing_foto','briefing.id','=','briefing_foto.idFoto')
      ->select('briefing_foto.*')
      ->where('briefing.createdBy',session('auth')->id_karyawan)
      ->get();
    }

    public static function getDataByTglNik()
    {
      date_default_timezone_set("Asia/Makassar");
      return DB::table('briefing')->whereDate('createdAt', date('Y-m-d') )->where('createdBy',session('auth')->id_karyawan)->first();
    }

    public static function simpanData()
    {
      return DB::table('briefing')->insertGetId([
        'createdBy'	=> session('auth')->id_karyawan
      ]);
    }

    public static function hapusSemuaFoto($id)
    {
      return DB::table('briefing_foto')->where('idFoto',$id)->delete();
    }

    public static function getDataById($id)
    {
      return DB::SELECT('SELECT
      a.*,
      b.title,
      a.createdAt as TGL
      FROM
        briefing a
      LEFT JOIN group_telegram b ON a.sektor = b.chat_id
      WHERE a.id = '.$id.'
      ORDER BY a.createdAt DESC
      LIMIT 1
      ')[0];
    }

    public static function getSektor()
    {
      return DB::SELECT('SELECT
      a.chat_id as id,
      a.title as text
      FROM
        group_telegram a
      WHERE a.ket_posisi IN ("PROV","IOAN")
      ');
    }

    public static function updateData($req, $id)
    {
      $check = DB::table('briefing')->where('id',$id)->first();
      if (count($check)>0)
      {
        return DB::table('briefing')->where('id',$id)->update([
          'createdBy' => session('auth')->id_karyawan,
          'sektor'	=> $req->sektor,
          'catatan'	=> $req->catatan,
          'status'	=> 1
        ]);
      }
      else
      {
        return DB::table('briefing')->insert([
          'createdBy' => session('auth')->id_karyawan,
          'sektor'	=> $req->sektor,
          'catatan'	=> $req->catatan,
          'status'	=> 1
        ]);
      }
    }

    public static function getFoto($id)
    {
      return DB::table('briefing_foto')->where('idFoto',$id)->get();
    }

    public static function active_team_Briefing()
    {
      date_default_timezone_set('Asia/Makassar');
      return DB::SELECT('SELECT
          *,
          d.status as status_kehadiran,
          d.date_created as waktu_absen,
          c.nik as NIK,
          c.status as status_emp,
          e.squad,
          ma.mitra_amija_pt as mitra,
          a.sttsWo
        FROM
          1_2_employee c
        LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
        LEFT JOIN mitra_amija ma ON a.mitra = ma.mitra_amija AND ma.witel = "'.session('auth')->witel.'"
        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
        LEFT JOIN absen d ON (c.nik = d.nik AND DATE(d.date_created) = "'.date('Y-m-d').'")
        LEFT JOIN employee_squad e ON c.squad = e.squad_id
        WHERE
          (b.TL_NIK = "'.session('auth')->id_karyawan.'" OR b.Nik_Atl = "'.session('auth')->id_karyawan.'" OR b.Nik_Atl2 = "'.session('auth')->id_karyawan.'" ) AND a.ACTIVE <> 0
        GROUP BY c.nik
        ORDER BY d.status ASC
      ');
    }

    public static function simpanFoto($filename, $id)
    {
      return DB::table('briefing_foto')->insert([
        'idFoto'	=> $id,
        'foto'		=> $filename
      ]);
    }

    public static function get_resling($year, $jenis, $jenis_jns = null)
    {
      return DB::table('perwira_resling')
      ->leftjoin('user_perwira', 'user_perwira.id_people', '=', 'perwira_resling.people_id')
      ->leftjoin('perwira_posisition', 'perwira_posisition.id', '=', 'user_perwira.posisi_id')
      ->leftjoin('perwira_direktorat', 'perwira_direktorat.id', '=', 'perwira_posisition.direktorat_id')
      ->select('perwira_resling.*', 'user_perwira.id_people', 'user_perwira.id_nik', 'user_perwira.nama', 'perwira_posisition.pos_name')
      ->where([
        ['jenis', $jenis],
        ['jenis_jns', $jenis_jns],
        [DB::RAW("YEAR(perwira_resling.tgl)"), $year]
      ])->get();
    }

    public static function get_resling_chart($y)
    {
      return DB::SELECT("SELECT jenis, jenis_jns, COUNT(*) as jml, MONTH(tgl) as bulan FROM perwira_resling WHERE YEAR(tgl) = $y GROUP BY jenis, jenis_jns, MONTH(tgl) ORDER BY bulan ASC");
    }

    public static function list_acc()
    {
      return DB::table('perwira_accident')
      ->leftjoin('user_perwira', 'user_perwira.id_people', '=', 'perwira_accident.people_id')
      ->get();
    }

    public static function list_acc_mitra($jenis, $tahun)
    {
      if($jenis == 'all')
      {
        return DB::SELECT("SELECT
        (CASE WHEN up.mitra IS NOT NULL THEN up.mitra ELSE 'Tidak Mempunyai Mitra' END)as mitra,
        SUM(CASE WHEN pa.id IS NOT NULL THEN 1 ELSE 0 END) as jml_acc
        FROM user_perwira up
        LEFT JOIN perwira_accident pa ON pa.people_id = up.id_people
        WHERE YEAR(pa.tgl) = $tahun GROUP BY mitra ORDER BY jml_acc DESC");
      }
      elseif($jenis == 'tahunan')
      {
        return DB::SELECT("SELECT
        (CASE WHEN up.mitra IS NOT NULL THEN up.mitra ELSE 'Tidak Mempunyai Mitra' END) as mitra,
        MONTH(tgl) as bulan,
        SUM(CASE WHEN pa.id IS NOT NULL THEN 1 ELSE 0 END) as jml_acc
        FROM user_perwira up
        LEFT JOIN perwira_accident pa ON pa.people_id = up.id_people
        WHERE YEAR(pa.tgl) = $tahun GROUP BY MONTH(pa.tgl), up.mitra ORDER BY bulan ASC");
      }
      else
      {
        return DB::SELECT("SELECT
        (CASE WHEN up.mitra IS NOT NULL THEN up.mitra ELSE 'Tidak Mempunyai Mitra' END)as mitra,
        SUM(CASE WHEN pa.id IS NOT NULL THEN 1 ELSE 0 END) as jml_acc
        FROM user_perwira up
        LEFT JOIN perwira_accident pa ON pa.people_id = up.id_people
        WHERE YEAR(pa.tgl) = $tahun AND mitra = $jenis GROUP BY mitra ORDER BY jml_acc DESC");
      }
    }

    public static function detail_cuti($id)
    {
        return DB::table('user_perwira as up')
        ->leftjoin('perwira_cuti as pc', 'pc.people_id', '=', 'up.id_people')
        ->leftjoin('perwira_jenis_cuti as pjc', 'pc.jenis_id', '=', 'pjc.id')
        ->select('pc.*', 'pjc.cuti_nm', 'up.nama', 'up.id_nik')
        ->where('pc.id', $id)
        ->first();
    }

    public static function get_brevert_me($mitra, $jenis = 'avail')
    {
      if($jenis == 'avail')
      {
        // dd($mitra);
        return DB::table('user_perwira')
        ->leftjoin('perwira_brevert_user', 'perwira_brevert_user.noktp', '=', 'user_perwira.noktp')
        ->leftjoin('perwira_breving', 'perwira_breving.id', 'perwira_brevert_user.id_brevert')
        ->select('user_perwira.nama', 'user_perwira.id_nik', 'user_perwira.noktp', 'perwira_breving.*', 'perwira_brevert_user.hadir')
        ->where('user_perwira.id_mitra', $mitra)
        ->where([
          ['tgl_start', '<=', DB::raw("DATE_FORMAT(NOW(), '%Y-%m-%d')")],
          ['tgl_end', '>', DB::raw("DATE_FORMAT(NOW(), '%Y-%m-%d')")]
        ])
        // ->whereNotNull('perwira_breving.id')
        ->get();
      }
      else
      {
        return DB::table('user_perwira')
        ->leftjoin('perwira_brevert_user', 'perwira_brevert_user.noktp', '=', 'user_perwira.noktp')
        ->leftjoin('perwira_breving', 'perwira_breving.id', 'perwira_brevert_user.id_brevert')
        ->select('user_perwira.nama', 'user_perwira.id_nik', 'user_perwira.noktp', 'perwira_breving.*', 'perwira_brevert_user.hadir')
        ->where('user_perwira.id_mitra', $mitra)
        ->where('tgl_end', '<', DB::raw("DATE_FORMAT(NOW(), '%Y-%m-%d')") )
        ->get();
      }
    }

    public static function save_absen_app_mass($getData)
    {

      foreach($getData as $data)
      {
        DB::transaction( function() use($data){
          DB::table('absen')->where('nik',$data)->whereDate('date_created',date('Y-m-d') )->update([
            'approval'        => 1,
            'date_approval'   => DB::raw('now()'),
            'approve_by'      => session('auth')->id_karyawan
          ]);
        });
      }
    }

    public static function get_data_pelamar($sql = '')
    {
      return DB::SELECT("SELECT pr.*, ap.nama as nama_provinsi, ak.nama as nama_kota, akl.nama as nama_kelurahan, akc.nama as nama_kecamatan, YEAR(NOW() ) - YEAR(pr.tgl_lahir) as umur, prf.fileName, prf.originalExt
      FROM perwira_recruitment pr
      LEFT JOIN perwira_recruitment_fileUpload prf ON pr.noktp = prf.ktp_no
      LEFT JOIN api_provinsi ap ON pr.provinsi = ap.id
      LEFT JOIN api_kota ak ON pr.kota = ak.id_kota
      LEFT JOIN api_kelurahan akl ON pr.kelurahan = akl.id_kelurahan
      LEFT JOIN api_kecamatan akc ON pr.kecamatan = akc.id_kecamatan
      WHERE (pr.status_pelamar = 'NEW' AND prf.fileName = 'file_sertifikat_vaksin') OR pr.status_pelamar = 'REKRUT' $sql
      GROUP BY pr.noktp
      ORDER BY MONTH(pr.created_at) ASC
      ");
    }

    public static function get_data_terima_ta($sql)
    {
      return DB::SELECT("SELECT pr.*, up.mitra, ap.nama as nama_provinsi, ak.nama as nama_kota, akl.nama as nama_kelurahan, akc.nama as nama_kecamatan, YEAR(NOW() ) - YEAR(pr.tgl_lahir) as umur
      FROM perwira_recruitment pr
      LEFT JOIN user_perwira up ON up.id_nik = pr.modified_by
      LEFT JOIN api_provinsi ap ON pr.provinsi = ap.id
      LEFT JOIN api_kota ak ON pr.kota = ak.id_kota
      LEFT JOIN api_kelurahan akl ON pr.kelurahan = akl.id_kelurahan
      LEFT JOIN api_kecamatan akc ON pr.kecamatan = akc.id_kecamatan
      WHERE pr.status_pelamar = 'ACCEPTED_TA' $sql
      ORDER BY MONTH(pr.created_at) ASC
      ");
    }

    public static function get_data_terima_mitra($sql)
    {
      return DB::SELECT("SELECT pr.*, up.mitra, ap.nama as nama_provinsi, ak.nama as nama_kota, akl.nama as nama_kelurahan, akc.nama as nama_kecamatan, YEAR(NOW() ) - YEAR(pr.tgl_lahir) as umur
      FROM perwira_recruitment pr
      LEFT JOIN user_perwira up ON up.id_nik = pr.modified_by
      LEFT JOIN api_provinsi ap ON pr.provinsi = ap.id
      LEFT JOIN api_kota ak ON pr.kota = ak.id_kota
      LEFT JOIN api_kelurahan akl ON pr.kelurahan = akl.id_kelurahan
      LEFT JOIN api_kecamatan akc ON pr.kecamatan = akc.id_kecamatan
      WHERE pr.status_pelamar = 'ACCEPTED_MITRA' $sql
      ORDER BY MONTH(pr.created_at) ASC
      ");
    }

    public static function get_data_blacklist($sql)
    {
      return DB::SELECT("SELECT pr.*, up.mitra
      FROM perwira_recruitment pr
      LEFT JOIN user_perwira up ON up.id_nik = pr.modified_by
      WHERE pr.status_pelamar = 'BLACKLIST' $sql
      ORDER BY MONTH(pr.created_at) ASC
      ");
    }

    public static function download_event($id)
    {

      $result1 = DB::table('perwira_event_attendant as a')
      ->leftJoin('1_2_employee as emp', 'a.nik', '=', 'emp.nik')
      ->select('a.*', DB::RAW('a.nik As nama_scan') )
      ->where('event_id', $id)
      ->get();

      $result2 = DB::connection('data_center')
      ->table('master_karyawan as mk')
      ->get();

      $combinedResult = [
        'result1' => $result1,
        'result2' => $result2,
      ];

      $data_value = [];
      $no = 0;

      foreach($combinedResult['result1'] as $k2 => $v2)
      {
        $find = array_search($v2->nik, array_column($combinedResult['result2']->toArray(), 'nik') );

        if($find)
        {
          $get_find[$k2] = $combinedResult['result2'][$find];
          $result1[$k2]->nama_scan = $get_find[$k2]->name . ' ('. $get_find[$k2]->nik .')';

          $data_value[] = [
            'no'        => ++$no,
            'nik'       => $result1[$k2]->nik,
            'nama_scan' => $result1[$k2]->nama_scan,
            'ts'        => $result1[$k2]->ts,
            'gate'      => $result1[$k2]->gate,
          ];
        }
      }

      return ['head' => ['Nomor', 'NIK', 'Peserta', 'Tgl Scan', 'Penjaga Scan'], 'data' => $data_value];
    }
}