<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Session;

date_default_timezone_set("Asia/Makassar");
class MitraModel{

  public static function save_alker($req)
  {
      $auth = Session::get('auth');
      return DB::transaction( function() use($req, $auth)
      {
          DB::table('perwira_alkerMitra')->insert([
              'mitra_id' => session('auth')->id_mitra,
              'created_by' => $auth->id_user
          ]);

          $msg['msg'] = [
              'type' => 'success',
              'text' => 'Data Accident Berhasil Ditambahkan!'
          ];

          return $msg;
      });
  }
}