<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;

class JobPosition
{
    const TABLE = 'job_position';
    public static function getlist($req)
    {
        if (!empty($req) ){
            $status=explode(",", $req->status);
            if ($req->status=="0"||$req->status=="1"){$status=array($req->status,$req->status);}
            $status_tersedia=explode(",", $req->status_tersedia);
            if ($req->status_tersedia=="0"||$req->status_tersedia=="1")
                {$status_tersedia=array($req->status_tersedia,$req->status_tersedia);}
            $witel=$req->witel;
        }
        else if (empty($req) ){
            $status=array(0,1);
            $status_tersedia=array(0,1);
            $witel=1;
        }

        if (empty($req) || $req->cari==1){
            return DB::table(self::TABLE)
                ->select('job_position.id as id',
                        'job_position.id_naker as id_naker','karyawan.nik as nik','karyawan.nama as nama',
                        'job_position.regional as regional','job_position.bizpart_id as bizpart_id',
                        'witel.nama_witel as witel',
                        'loker.nama_loker as loker','job_position.id_loker as id_loker',
                        'job_position.object_id as object_id','job_position.position_name as position_name',
                        'job_position.level as level',
                        'job_position.direktorat as direktorat', 'job_position.teritory as teritory',
                        'job_position.unit as unit','job_position.sub_unit as sub_unit','job_position.group as group',
                        'job_position.sub_group as sub_group', 'job_position.group_fungsi as group_fungsi',
                        'job_position.status as status','job_position.status_tersedia as status_tersedia')
                ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
                ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
                ->leftJoin('karyawan','job_position.id_naker','=','karyawan.id')
                ->where('loker.witel_id',$witel)
                ->whereIn('job_position.status',[$status[0],$status[1]])
                ->whereIn('job_position.status_tersedia',[$status_tersedia[0],$status_tersedia[1]])
                ->get();
         }
         else if ($req->cari==2){
            return DB::table(self::TABLE)
                ->select('job_position.id as id',
                        'job_position.id_naker as id_naker','karyawan.nik as nik','karyawan.nama as nama',
                        'job_position.regional as regional','job_position.bizpart_id as bizpart_id',
                        'witel.nama_witel as witel',
                        'loker.nama_loker as loker','job_position.id_loker as id_loker',
                        'job_position.object_id as object_id','job_position.position_name as position_name',
                        'job_position.level as level',
                        'job_position.direktorat as direktorat','job_position.teritory as teritory',
                        'job_position.unit as unit','job_position.sub_unit as sub_unit','job_position.group as group',
                        'job_position.sub_group as sub_group', 'job_position.group_fungsi as group_fungsi',
                        'job_position.status as status','job_position.status_tersedia as status_tersedia')
                ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
                ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
                ->leftJoin('karyawan','job_position.id_naker','=','karyawan.id')
                ->where('job_position.id_loker',$req->psa)
                ->whereIn('job_position.status',[$status[0],$status[1]])
                ->whereIn('job_position.status_tersedia',[$status_tersedia[0],$status_tersedia[1]])
                ->get();
         }
    }
    public static function getByPsa($id)
    {
         return DB::table(self::TABLE)
            ->select(DB::raw('id, CONCAT(object_id," - ",position_name) as text, object_id as object_id') )
            ->where('id_loker',$id)
            ->where('status',0)
            ->where('status_tersedia',1)
            ->get();
    }
    public static function getById($id)
    {
         return DB::table(self::TABLE)
            ->where('id',$id)
            ->first();
    }
    public static function save( $req)
        {
         if (!empty($req->id) ){
            //update
            DB::table(self::TABLE)->where('id', $req->id)->update([
                'object_id' => $req->objectid,
                'position_name' => $req->position,
                'direktorat' => $req->direktorat,
                'level'=> $req->level,
                'unit' => $req->unit,
                'sub_unit' => $req->subunit,
                'group' => $req->group,
                'sub_group' => $req->subgroup,
                'group_fungsi' => $req->groupfungsi,
                'id_loker'=>$req->psa,
                'regional'=>$req->regional,
                'teritory'=>$req->teritory,
                'bizpart_id'=>$req->bizpart,
                'status_tersedia'=>$req->status
            ]);

         }else{
            DB::table(self::TABLE)->insert([
                'object_id' => $req->objectid,
                'position_name' => $req->position,
                'direktorat' => $req->direktorat,
                'level' => $req->level,
                'unit' => $req->unit,
                'sub_unit' => $req->subunit,
                'group' => $req->group,
                'sub_group' => $req->subgroup,
                'group_fungsi' => $req->groupfungsi,
                'id_loker'=>$req->psa,
                'regional'=>$req->regional,
                'teritory'=>$req->teritory,
                'bizpart_id'=>$req->bizpart,
                'status_tersedia'=>$req->status
            ]);

         }
        }
    public static function delete( $id)
        {
            DB::transaction(function() use($id) {

                DB::table(self::TABLE)
                   ->where('id', $id)
                   ->delete();
            });
        }
    public static function getKaryawanPosition($id)
    {
         return DB::table(self::TABLE)
                ->select('job_position.id as id',
                    'karyawan.id_sto as id_sto','sto.sto as sto',
                    'job_position.id_naker as id_naker','karyawan.nik as nik',
                    'karyawan.nik_atasan as nik_atasan','karyawan.bpjs as bpjs',
                    'witel.nama_witel as witel',
                    'loker.nama_loker as loker','job_position.id_loker as id_loker',
                    'job_position.id as idjob','job_position.teritory as teritory',
                    'job_position.regional as regional','job_position.bizpart_id as bizpart_id',
                    'job_position.object_id as object_id','job_position.position_name as position_name',
                    'job_position.direktorat as direktorat','job_position.level as level',
                    'job_position.unit as unit','job_position.sub_unit as sub_unit','job_position.group as group',
                    'job_position.sub_group as sub_group', 'job_position.group_fungsi as group_fungsi')
                ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
                ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
                ->leftJoin('karyawan', 'job_position.id_naker', '=', 'karyawan.id')
                ->leftJoin('sto','karyawan.id_sto','=','sto.id')
                ->where('job_position.id_naker', $id)
                ->orderBy('job_position.id', 'desc')
                ->first();
    }

    public static function getKaryawanPositionLama($id)
    {
         return DB::table('pelurusan_job_position')
                ->select('pelurusan_job_position.id as id',
                    'karyawan.id_sto as id_sto','sto.sto as sto',
                    'karyawan.nik_atasan as nik_atasan',
                    'witel.nama_witel as witel',
                    'loker.nama_loker as loker', 'job_position.id as idjob',
                    'job_position.regional as regional','job_position.bizpart_id as bizpart_id',
                    'job_position.teritory as teritory','job_position.level as level',
                    'job_position.object_id as object_id','job_position.position_name as position_name','job_position.direktorat as direktorat',
                    'job_position.unit as unit','job_position.sub_unit as sub_unit','job_position.group as group',
                    'job_position.sub_group as sub_group', 'job_position.group_fungsi as group_fungsi')
                ->leftJoin('job_position', 'pelurusan_job_position.id_job_position', '=', 'job_position.id')
                ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
                ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
                ->leftJoin('karyawan','pelurusan_job_position.nik','=','karyawan.nik')
                ->leftJoin('sto','karyawan.id_sto','=','sto.id')
                ->where('pelurusan_job_position.id_naker', $id)
                ->whereIn('pelurusan_job_position.status_pengajuan',[0,2,5])
                ->orderBy('pelurusan_job_position.id','desc')
                ->offset(1)
                ->limit(1)
                ->first();
    }

    public static function countDasboard($req)
    {
    if (empty($req) || $req->cari==1){
            $id=1;
            if (!empty($req) ){$id=$req->witel;}
        $semua= DB::table(self::TABLE)
                ->leftJoin('loker','job_position.id_loker','=','loker.id')
                ->where('loker.witel_id',$id)
                ->count() ;

        $terisi= DB::table(self::TABLE)
                ->leftJoin('loker','job_position.id_loker','=','loker.id')
                ->where('loker.witel_id',$id)
                ->where('status',1)
                ->count() ;

        $kosong= DB::table(self::TABLE)
                ->leftJoin('loker','job_position.id_loker','=','loker.id')
                ->where('loker.witel_id',$id)
                ->where('status',0)
                ->count() ;

        $aktif= DB::table(self::TABLE)
                ->leftJoin('loker','job_position.id_loker','=','loker.id')
                ->where('loker.witel_id',$id)
                ->where('status',0)
                ->where('status_tersedia',1)
                ->count() ;

        $unmap= DB::table(self::TABLE)
                ->leftJoin('loker','job_position.id_loker','=','loker.id')
                ->where('loker.witel_id',$id)
                ->where('status',0)
                ->where('id_naker','<>', 0)
                ->where('id_naker','<>', "")
                ->count() ;
          }
    else {

        $semua= DB::table(self::TABLE)
                ->where('job_position.id_loker',$req->psa)
                ->count() ;

        $terisi= DB::table(self::TABLE)
                ->where('job_position.id_loker',$req->psa)
                ->where('status',1)
                ->count() ;

        $kosong= DB::table(self::TABLE)
                ->where('job_position.id_loker',$req->psa)
                ->where('status',0)
                ->count() ;

        $aktif= DB::table(self::TABLE)
                ->where('job_position.id_loker',$req->psa)
                ->where('status',0)
                ->where('status_tersedia',1)
                ->count() ;

        $unmap= DB::table(self::TABLE)
                ->where('job_position.id_loker',$req->psa)
                ->where('status',0)
                ->where('id_naker','<>', 0)
                ->where('id_naker','<>', "")
                ->count() ;
         }

        return $all = array($semua,$terisi,$kosong,$aktif,$unmap );
    }

    public static function getCount($id)
    {
         return DB::select('select position_name,COUNT(*) as kuota,
                            count(case id_naker when 0 then null else 1 end) as isi,
                            count(case id_naker when 0 then 1 else null end) as kekurangan
                            from job_position
                            where id_loker = ? GROUP BY position_name',[$id]);
    }

    public static function tambah($posisi,$psa)
    {
        $data= DB::table(self::TABLE)
            ->where('position_name',$posisi)
            ->where('id_loker',$psa)
            ->first();

        DB::table(self::TABLE)->insert([
                'object_id' => $data->object_id,
                'position_name' => $data->position_name,
                'direktorat' => $data->direktorat,
                'level' => $data->level,
                'unit' => $data->unit,
                'sub_unit' => $data->sub_unit,
                'group' => $data->group,
                'sub_group' => $data->sub_group,
                'group_fungsi' => $data->group_fungsi,
                'id_loker'=>$data->id_loker,
                'regional'=>$data->regional,
                'teritory'=>$data->teritory,
                'bizpart_id'=>$data->bizpart_id,
                'status_tersedia'=>1
            ]);
    }

    public static function kurang($posisi,$psa)
    {
        $data= DB::table(self::TABLE)
            ->where('position_name',$posisi)
            ->where('id_loker',$psa)
            ->where('id_naker',0)
            ->orderBy('id','desc')
            ->first();

        DB::table(self::TABLE)
                   ->where('id', $data->id)
                   ->delete();
    }
}