<?php
namespace App\DA;

use DOMDocument;
use Illuminate\Support\Facades\DB;
use Telegram;
class Perf
{
    public static function get_perf_ioan()
    {
        $chat_id = '52369916';
        Telegram::sendMessage([
            'chat_id' => $chat_id,
            'text' => 'Start Sync get_perf_ioan',
            'parse_mode' =>'html'
        ]);
        $akun = DB::table('akun')->where('user','17920262')->first();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/tactical/index.php/login/act_login');
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
        curl_setopt($ch, CURLOPT_POSTFIELDS, "user_id=".$akun->user."&password=".$akun->pwd);
        $result = curl_exec($ch);
        $witel=["KALSEL","BALIKPAPAN"];
        foreach($witel as $w){
            curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/tactical/dashboard/get_kpi_ioan_sektor');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "periode=".date('Ym',strtotime('-1 month') )."&witel=".$w);
            $result = curl_exec($ch);
            dd($result);
        }
    }

    public static function get_perf_ioan_servo()
    {
        // $tes = DB::SELECT("SELECT pia.id as id_pia, pia.payload_kpi, dpi.*
        // FROM performance_ioan_average pia
        // LEFT JOIN detail_performance_ioan dpi ON pia.id = dpi.id_sektor");

        // foreach($tes as $k => $v)
        // {
        //     $final_data[$v->id_pia][$k]['indikator'] = $v->indikator;
        //     $final_data[$v->id_pia][$k]['tolak_ukur'] = $v->tolak_ukur;
        //     $final_data[$v->id_pia][$k]['satuan'] = $v->satuan;
        //     $final_data[$v->id_pia][$k]['target'] = $v->target;
        //     $final_data[$v->id_pia][$k]['bobot'] = $v->bobot;
        //     $final_data[$v->id_pia][$k]['relasi'] = $v->relasi;
        //     $final_data[$v->id_pia][$k]['result'] = $v->result;
        //     $final_data[$v->id_pia][$k]['score'] = $v->score;
        //     $final_data[$v->id_pia][$k]['pencapaian'] = $v->pencapaian;
        //     $final_data[$v->id_pia][$k]['tgl_update'] = $v->tgl_update;
        // }

        // $final_data = array_map(function($x){
        //     return json_encode($x);
        // }, $final_data);

        // foreach($final_data as $k => $v)
        // {
        //     DB::Table('performance_ioan_average')->where('id', $k)->update([
        //         'payload_kpi' => $v
        //     ]);
        // }

        $get_cookies = DB::Table('cookie_systems')->Where('application', 'servo')->first();
        $cookie = $get_cookies->cookies;
        // $date = date('Y-m');
        $date = '2023-01';
        $date_ = str_replace('-', '', $date);

        //KALSEL

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://newaccess-servo.telkom.co.id/servo/sector/kpisectorhasilrekon',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('PERIODE' => $date_,'TREG' => '6','WITEL' => '44'),
            CURLOPT_HTTPHEADER => array(
                'Cookie: accessservo='.$cookie.'; sidebar_closed=1'
            ),
        ) );
        $response_kalsel = curl_exec($ch);
        curl_close($ch);

        //KALBAR

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://newaccess-servo.telkom.co.id/servo/sector/kpisectorhasilrekon',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('PERIODE' => $date_,'TREG' => '6','WITEL' => '45'),
            CURLOPT_HTTPHEADER => array(
                'Cookie: accessservo='.$cookie.'; sidebar_closed=1'
            ),
        ) );
        $response_bpp = curl_exec($ch);
        curl_close($ch);

        $header_data = $body_data = [];

        $dom = new \DOMDocument();
        @$dom->loadHTML($response_kalsel);
        $xpath = new \DOMXPath($dom);

        $thead = $xpath->query('//table/thead/tr');

        foreach ($thead as $row)
        {
            $cells = $row->getElementsByTagName('th');

            foreach ($cells as $cell) {
                $header_data['KALSEL'][] = trim(preg_replace('/\xc2\xa0/', '', $cell->nodeValue) );
            }
        }

        $tbody = $xpath->query('//table/tbody/tr');

        $no_clm = $no_avg = $no_new = 0;
        $new_body_data = [];

        foreach ($tbody as $row)
        {
            $cells = $row->getElementsByTagName('td');

            if($cells[1] && $cells[1]->getAttribute('colspan') != '')
            {
                ++$no_avg;

                foreach ($cells as $cell)
                {
                    if(trim(preg_replace('/\xc2\xa0/', '', $cell->nodeValue) ) )
                    {
                        $body_data['KALSEL'][$no_avg]['rata_rata'][] = trim(preg_replace('/\xc2\xa0/', '', $cell->nodeValue) );
                    }
                }
            }
            else
            {
                if($cells[1])
                {
                    ++$no_clm;
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['indikator']  = trim(preg_replace('/\xc2\xa0/', '', $cells[1]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['tolak_ukur'] = trim(preg_replace('/\xc2\xa0/', '', $cells[2]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['satuan']     = trim(preg_replace('/\xc2\xa0/', '', $cells[3]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['target']     = trim(preg_replace('/\xc2\xa0/', '', $cells[4]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['bobot']      = trim(preg_replace('/\xc2\xa0/', '', $cells[5]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['relasi']     = trim(preg_replace('/\xc2\xa0/', '', $cells[6]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['result']     = trim(preg_replace('/\xc2\xa0/', '', $cells[7]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['score']      = trim(preg_replace('/\xc2\xa0/', '', $cells[8]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['pencapaian'] = trim(preg_replace('/\xc2\xa0/', '', $cells[9]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['keterangan'] = trim(preg_replace('/\xc2\xa0/', '', $cells[10]->nodeValue) );
                    $body_data['KALSEL'][$no_avg]['isi'][$no_clm]['tgl_update'] = trim(preg_replace('/\xc2\xa0/', '', $cells[11]->nodeValue) );
                }

            }
        }

        foreach($body_data['KALSEL'] as $k => &$v)
        {
            if(str_contains($v['rata_rata'][0], 'Parent') && $v['rata_rata'][0] != 'Parent 1')
            {
                unset($body_data['KALSEL'][$k]);
            }
        }

        unset($v);

        foreach($body_data['KALSEL'] as $k => $v)
        {
            if(count($v) == 1)
            {
                $new_body_data['KALSEL'][$no_new]['sektor'] = $v['rata_rata'][0];
            }

            if(count($v) == 2)
            {
                $new_body_data['KALSEL'][$no_new]['data']['rata_rata'] = $v['rata_rata'][2] ?? $v['rata_rata'][1];
                $new_body_data['KALSEL'][$no_new]['data']['isi'] = $v['isi'];
                $no_new++;
            }
        }

        $dom = new \DOMDocument();
        @$dom->loadHTML($response_bpp);
        $xpath = new \DOMXPath($dom);

        $thead = $xpath->query('//table/thead/tr');

        foreach ($thead as $row)
        {
            $cells = $row->getElementsByTagName('th');

            foreach ($cells as $cell) {
                $header_data['BALIKPAPAN'][] = trim(preg_replace('/\xc2\xa0/', '', $cell->nodeValue) );
            }
        }

        $tbody = $xpath->query('//table/tbody/tr');

        $no_clm = $no_avg = 0;

        foreach ($tbody as $row)
        {
            $cells = $row->getElementsByTagName('td');

            if($cells[1] && $cells[1]->getAttribute('colspan') != '' )
            {
                ++$no_avg;
                foreach ($cells as $cell)
                {
                    if(trim(preg_replace('/\xc2\xa0/', '', $cell->nodeValue) ) )
                    {
                        $body_data['BALIKPAPAN'][$no_avg]['rata_rata'][] = trim(preg_replace('/\xc2\xa0/', '', $cell->nodeValue) );
                    }
                }
            }
            else
            {
                if($cells[1])
                {
                    ++$no_clm;
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['indikator']  = trim(preg_replace('/\xc2\xa0/', '', $cells[1]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['tolak_ukur'] = trim(preg_replace('/\xc2\xa0/', '', $cells[2]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['satuan']     = trim(preg_replace('/\xc2\xa0/', '', $cells[3]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['target']     = trim(preg_replace('/\xc2\xa0/', '', $cells[4]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['bobot']      = trim(preg_replace('/\xc2\xa0/', '', $cells[5]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['relasi']     = trim(preg_replace('/\xc2\xa0/', '', $cells[6]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['result']     = trim(preg_replace('/\xc2\xa0/', '', $cells[7]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['score']      = trim(preg_replace('/\xc2\xa0/', '', $cells[8]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['pencapaian'] = trim(preg_replace('/\xc2\xa0/', '', $cells[9]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['keterangan'] = trim(preg_replace('/\xc2\xa0/', '', $cells[10]->nodeValue) );
                    $body_data['BALIKPAPAN'][$no_avg]['isi'][$no_clm]['tgl_update'] = trim(preg_replace('/\xc2\xa0/', '', $cells[11]->nodeValue) );
                }

            }
        }

        foreach($body_data['BALIKPAPAN'] as $k => &$v)
        {
            if(str_contains($v['rata_rata'][0], 'Parent') && $v['rata_rata'][0] != 'Parent 1')
            {
                unset($body_data['BALIKPAPAN'][$k]);
            }
        }

        unset($v);

        foreach($body_data['BALIKPAPAN'] as $k => $v)
        {
            if(count($v) == 1)
            {
                $new_body_data['BALIKPAPAN'][$no_new]['sektor'] = $v['rata_rata'][0];
            }

            if(count($v) == 2)
            {
                $new_body_data['BALIKPAPAN'][$no_new]['data']['rata_rata'] = $v['rata_rata'][2] ?? $v['rata_rata'][1];
                $new_body_data['BALIKPAPAN'][$no_new]['data']['isi'] = $v['isi'];
                $no_new++;
            }
        }

        foreach($new_body_data as $k => $v)
        {
            foreach($v as $kk => $vv)
            {
                if(@$vv['sektor'])
                {
                    $new_data_trophy[$k][$kk]['sektor'] = $vv['sektor'];
                    $new_data_trophy[$k][$kk]['average'] = $vv['data']['rata_rata'];
                }
            }

            usort($new_data_trophy[$k], function($a, $b) {
                return $b['average'] <=> $a['average'];
            });
        }

        $top_one = [];

        foreach($new_data_trophy as $k => $v)
        {
            $top_one[$k] = $v[0];
        }

        foreach($new_body_data as $k => $v)
        {
            // DB::SELECT("DELETE pia, dpi
            // FROM performance_ioan_average pia
            // LEFT JOIN detail_performance_ioan dpi ON pia.id = dpi.id_sektor
            // WHERE pia.witel = '$k' AND tgl_data = '$date' ");

            DB::SELECT("DELETE pia
            FROM performance_ioan_average pia
            WHERE witel = '$k' AND tgl_data = '$date' ");

            $insert = [];

            foreach($v as $kk => $vv)
            {
                if(@$vv['sektor'])
                {
                    $result = 0;
                    if( (float)$vv['data']['rata_rata'] >= 100)
                    {
                        $result = 1;
                    }

                    if(@$top_one[$k]['sektor'] == $vv['sektor'] && $vv['data']['rata_rata'] >= 100)
                    {
                        // $result = 2;
                        $result = 1;
                    }

                    $insert[] = [
                        'sektor'      => $vv['sektor'],
                        'average'     => $vv['data']['rata_rata'],
                        'result'      => $result,
                        'witel'       => $k,
                        'tgl_data'    => $date,
                        'payload_kpi' => json_encode($vv['data']['isi']),
                    ];
                    // foreach($vv['isi'] as $vvv)
                    // {
                    //     DB::Table('detail_performance_ioan')->insert([
                    //         'sektor'        => $vv['rata_rata'][0],
                    //         'average'       => $vv['rata_rata'][1],
                    //         'result_sektor' => $result,
                    //         'witel'         => $k,
                    //         'tgl_data'      => $date,
                    //         "id_sektor"     => $id_sektor,
                    //         "indikator"     => $vvv['indikator'],
                    //         "tolak_ukur"    => $vvv['tolak_ukur'],
                    //         "satuan"        => $vvv['satuan'],
                    //         "target"        => $vvv['target'],
                    //         "bobot"         => $vvv['bobot'],
                    //         "relasi"        => $vvv['relasi'],
                    //         "result"        => $vvv['result'],
                    //         "score"         => $vvv['score'],
                    //         "pencapaian"    => $vvv['pencapaian'],
                    //         "keterangan"    => $vvv['keterangan'],
                    //         "tgl_update"    => $vvv['tgl_update']
                    //     ]);
                    // }
                }
            }

            DB::Table('performance_ioan_average')->insert($insert);
        }

    }

}