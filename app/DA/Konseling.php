<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;

class Konseling
{
    const TABLE = 'konseling';
    public static function get()
    {
         return DB::table(self::TABLE)
            ->get();
    }
    public static function getById($id)
    {
     return DB::table(self::TABLE)
        ->where('id',$id)
        ->first();
    }
    public static function getList()
    {
     return DB::table(self::TABLE)
        ->select('konseling.id as id','konseling.id_naker as id_naker','konseling.tgl as tgl',
            'karyawan.nik as nik','karyawan.nama as nama','loker.nama_loker as psa','job_position.unit as unit',
            'konseling.pelanggaran as pelanggaran','konseling.tindak_lanjut as tindak_lanjut','konseling.sp as sp',
            'konseling.ket_sp as ket_sp')
        ->leftJoin('karyawan','konseling.id_naker','=','karyawan.id')
        ->leftJoin('job_position', 'job_position.id_naker', '=', 'konseling.id_naker')
        ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
        ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
        ->get();
    }
    public static function getListByNaker($req)
    {
     return DB::table(self::TABLE)
        ->select('konseling.id as id','konseling.id_naker as id_naker','konseling.tgl as tgl',
            'karyawan.nik as nik','karyawan.nama as nama','loker.nama_loker as psa','job_position.unit as unit',
            'konseling.pelanggaran as pelanggaran','konseling.tindak_lanjut as tindak_lanjut','konseling.sp as sp',
            'konseling.ket_sp as ket_sp')
        ->leftJoin('karyawan','konseling.id_naker','=','karyawan.id')
        ->leftJoin('job_position', 'job_position.id_naker', '=', 'konseling.id_naker')
        ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
        ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
        ->where('konseling.id_naker',$req->nama)
        ->get();
    }
    public static function getListBySp($req)
    {
     return DB::table(self::TABLE)
        ->select('konseling.id as id','konseling.id_naker as id_naker','konseling.tgl as tgl',
            'karyawan.nik as nik','karyawan.nama as nama','loker.nama_loker as psa','job_position.unit as unit',
            'konseling.pelanggaran as pelanggaran','konseling.tindak_lanjut as tindak_lanjut','konseling.sp as sp',
            'konseling.ket_sp as ket_sp')
        ->leftJoin('karyawan','konseling.id_naker','=','karyawan.id')
        ->leftJoin('job_position', 'job_position.id_naker', '=', 'konseling.id_naker')
        ->leftJoin('loker', 'job_position.id_loker', '=', 'loker.id')
        ->leftJoin('witel', 'loker.witel_id', '=', 'witel.id')
        ->where('konseling.sp',$req->sp)
        ->get();
    }
    public static function save($req)
    {
       if(!empty($req->id) ){
    //update
            DB::transaction(function() use($req) {
            DB::table(self::TABLE)->where('id', $req->id)->update([
                            "id_naker" => $req->nama,
                            "tgl" => $req->tgl,
                            "pelanggaran" => $req->pelanggaran,
                            "tindak_lanjut" => $req->tindak,
                            "sp" => $req->sp,
                            "ket_sp" => $req->ketsp
                        ]);
            if ($req->hasFile('scan') ) {
                    unlink(public_path()."/upload/Konseling/".$req->id."/".$req->file);
                    $path = public_path().'/upload/Konseling/'.$req->id.'/';
                    if (!file_exists($path) ) {
                      if (!mkdir($path, 0770, true) )
                        return 'gagal menyiapkan folder file';
                    }
                    $file = $req->file('scan');
                    try {
                      $moved = $file->move("$path",$req->file('scan')->getClientOriginalName() );
                    }
                    catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                      return 'gagal menyimpan file ';
                    }
                }
            });
        }else {
            DB::transaction(function() use($req) {
            $id = DB::table(self::TABLE)->insertGetId([
                            "id_naker" => $req->nama,
                            "tgl" => $req->tgl,
                            "pelanggaran" => $req->pelanggaran,
                            "tindak_lanjut" => $req->tindak,
                            "sp" => $req->sp,
                            "ket_sp" => $req->ketsp
                            ]);

              if ($req->hasFile('scan') ) {
                    //dd($input);
                    $path = public_path().'/upload/Konseling/'.$id.'/';
                    if (!file_exists($path) ) {
                      if (!mkdir($path, 0770, true) )
                        return 'gagal menyiapkan folder file';
                    }
                    $file = $req->file('scan');
                    try {
                      $moved = $file->move("$path",$req->file('scan')->getClientOriginalName() );
                    }
                    catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                      return 'gagal menyimpan file ';
                    }
                }

            });
        }
    }
    public static function delete($id,$file)
    {
        DB::transaction(function() use($id,$file) {
            DB::table(self::TABLE)
                   ->where('id', $id)
                   ->delete();
            unlink(public_path()."/upload/Konseling/".$id."/".$file);
        });
    }

}