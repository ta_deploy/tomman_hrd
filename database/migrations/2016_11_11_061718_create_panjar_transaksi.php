<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePanjarTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panjar_transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('panjar_id');
            $table->integer('nota_id');
            $table->integer('status');
            $table->double('nilai');
            $table->date('tgl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('panjar_transaksi');
    }
}
