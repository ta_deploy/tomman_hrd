<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanRemo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remo_laporan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status')->comment('1:UP;2:OGP;3:PENDING;0:BELUM SURVEY;');
            $table->longText('action');
            $table->longText('catatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('remo_laporan');
    }
}
