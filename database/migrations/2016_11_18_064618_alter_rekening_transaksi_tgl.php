<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRekeningTransaksiTgl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rekening_transaksi', function (Blueprint $table) {
            $table->date('tgl');
            $table->longText('catatan')->nullable();
            $table->integer('status_transaksi')->command('1=panjarkeluar;2=panjarkembali;3=pencairan;4=tambahsaldo;5=pembayarannota;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rekening_transaksi', function (Blueprint $table) {
            $table->dropColumn('tgl');
            $table->dropColumn('catatan');
            $table->dropColumn('status_transaksi');
        });
    }
}
