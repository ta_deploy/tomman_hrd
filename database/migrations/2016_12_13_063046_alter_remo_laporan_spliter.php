<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRemoLaporanSpliter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remo_laporan', function (Blueprint $table) {
            $table->integer('spliter')->nullable()->default(0)->comment("1=1:8;2=1:16;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remo_laporan', function (Blueprint $table) {
            $table->dropColumn('spliter');
        });
    }
}
