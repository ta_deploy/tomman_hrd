<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRincianKwitansi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rincian_kwitansi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kwitansi_id');
            $table->text('designator_id');
            $table->integer('qty');
            $table->double('nilai');
            $table->date('tgl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rincian_kwitansi');
    }
}
