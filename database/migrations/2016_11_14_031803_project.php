<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Project extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
        
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_site');
            $table->string('sto')->nullable();
            $table->string('witel')->nullable();
            $table->string('koordinat')->nullable();
            $table->longText('alamat')->nullable();
            $table->string('kategori')->nullable();
            $table->string('no_sp')->nullable();
            $table->string('sponsor')->nullable();
            $table->date('toc')->nullable();
            $table->integer('status_id');
            $table->longText('pic')->nullable();
            $table->integer('loker_project_id');
            $table->string('provisioning')->nullable();
            $table->string('proaktif_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project');
    }
}
