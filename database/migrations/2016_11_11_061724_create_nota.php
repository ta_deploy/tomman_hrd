<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('karyawan_id');
            $table->integer('project_id');
            $table->integer('program_id');
            $table->integer('kategori_id');
            $table->integer('pertanggungan_id');
            $table->date('tgl');
            $table->longText('keperluan');
            $table->double('nilai');
            $table->integer('status');
            $table->string('plat');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nota');
    }
}
