<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HargaDesignatorTelkom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harga_designator_telkom', function (Blueprint $table) {
            $table->increments('id');
            $table->string('designator_id');
            $table->string('witel_id');
            $table->string('harga_jasa');
            $table->string('harga_material');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('harga_designator_telkom');
    }
}
