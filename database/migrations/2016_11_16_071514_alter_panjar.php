<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPanjar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panjar', function (Blueprint $table) {
            $table->double('sisa')->nullable()->default(0);
            $table->longText('keperluan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panjar', function (Blueprint $table) {
            $table->dropColumn('sisa');
            $table->dropColumn('keperluan');
        });
    }
}
