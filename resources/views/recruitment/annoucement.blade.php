<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Beranda</title>
    <meta charset="utf-8" />
    <meta name="description" content="Company Official Page for Telkom Akses" />
    <meta name="keywords" content="telkom, akses" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Telkom Akses Official Page" />
    <meta property="og:url" content="https://telkomakses.co.id/" />
    <meta property="og:site_name" content="Telkom Akses Official Page" />
    <link rel="canonical" href="https://telkomakses.co.id/" />
    <link
      rel="shortcut icon"
      href="https://telkomakses.co.id/assets/media/logos/favicon.ico"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700"
    />
    <link
      href="https://telkomakses.co.id/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="https://telkomakses.co.id/assets/plugins/custom/datatables/datatables.bundle.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="https://telkomakses.co.id/assets/plugins/global/plugins.bundle.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="https://telkomakses.co.id/assets/css/style.bundle.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="https://telkomakses.co.id/assets/owl/owl.carousel.min.css"
      rel="stylesheet"
      type="text/css"
    />
    <style type="text/css">
      .table-col-bordered tr td:not(:last-child) {
        border-right-width: 1px;
        border-right-style: solid;
        border-right-color: var(--kt-border-color);
      }
      .cursor-grabbing td:not(.dtfc-fixed-left) {
        cursor: grabbing;
        cursor: -webkit-grabbing;
      }
      .cursor-grabbing td:not(.dtfc-fixed-left) > * {
        cursor: auto;
      }
      .active > .page-link,
      .page-link.active {
        z-index: 3;
        color: var(--bs-pagination-active-color);
        background-color: #e31b23 !important;
        border-color: var(--bs-pagination-active-border-color);
      }
      .bg-dark {
        --kt-bg-rgb-color: #202020 !important;
        background-color: #202020 !important;
      }
      .bg-primary {
        --kt-bg-rgb-color: #e31b23 !important;
        background-color: #e31b23 !important;
      }
      .text-primary {
        color: #e31b23 !important;
      }
      .text-hover-primary:hover {
        transition: color 0.2s ease;
        color: #e31b23 !important;
      }
      .carousel-custom .carousel-indicators-active-primary li.active:after {
        background-color: #e31b23 !important;
      }
      .menu-state-primary
        .menu-item.hover:not(.here)
        > .menu-link:not(.disabled):not(.active):not(.here),
      .menu-state-primary
        .menu-item:not(.here)
        .menu-link:hover:not(.disabled):not(.active):not(.here) {
        transition: color 0.2s ease;
        color: #e31b23 !important;
      }
      .menu-state-primary
        .menu-item:not(.here)
        .menu-link:hover:not(.disabled):not(.active):not(.here)
        .menu-arrow:after {
        background-color: #e31b23 !important;
      }
      .btn.btn-primary {
        color: var(--kt-primary-inverse);
        border-color: #e31b23 !important;
        background-color: #e31b23 !important;
      }
      .btn-check:active + .btn.btn-primary,
      .btn-check:checked + .btn.btn-primary,
      .btn.btn-primary.active,
      .btn.btn-primary.show,
      .btn.btn-primary:active:not(.btn-active),
      .btn.btn-primary:focus:not(.btn-active),
      .btn.btn-primary:hover:not(.btn-active),
      .show > .btn.btn-primary {
        color: var(--kt-primary-inverse);
        border-color: #ee6e73;
        background-color: #ee6e73 !important;
      }
      .menu-state-bg-light-primary
        .menu-item.hover:not(.here)
        > .menu-link:not(.disabled):not(.active):not(.here),
      .menu-state-bg-light-primary
        .menu-item:not(.here)
        .menu-link:hover:not(.disabled):not(.active):not(.here) {
        transition: color 0.2s ease;
        background-color: #fff6f5 !important;
        color: #e31b23 !important;
      }
      /*.modal-backdrop {
			background-color: rgba(0,0,0,0) !important;
		}*/
      .carousel-inner::after {
        content: "" !important;
      }
      .carousel-custom
        .carousel-indicators.carousel-indicators-bullet
        li:after {
        content: "" !important;
      }
    </style>
  </head>
  <body
    id="kt_body"
    class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled app-blank"
  >
    <script>
      var defaultThemeMode = "light";
      var themeMode;
      if (document.documentElement) {
        if (document.documentElement.hasAttribute("data-theme-mode")) {
          themeMode = document.documentElement.getAttribute("data-theme-mode");
        } else {
          if (localStorage.getItem("data-theme") !== null) {
            themeMode = localStorage.getItem("data-theme");
          } else {
            themeMode = defaultThemeMode;
          }
        }
        if (themeMode === "system") {
          themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches
            ? "dark"
            : "light";
        }
        document.documentElement.setAttribute("data-theme", themeMode);
      }
    </script>
    <div class="d-flex flex-column flex-root">
      <div class="page d-flex flex-row flex-column-fluid">
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
          <div
            id="kt_header"
            class="header align-items-stretch bg-white nav-oi"
            data-kt-sticky="true"
            data-kt-sticky-name="header"
            data-kt-sticky-offset="{default: '200px', lg: '300px'}"
          >
            <div class="container-lg d-flex align-items-center">
              <div
                class="d-flex topbar align-items-center d-lg-none ms-n2 me-3"
                title="Show aside menu"
              >
                <div
                  class="btn btn-icon btn-active-light-dark w-30px h-30px w-md-40px h-md-40px"
                  id="kt_header_menu_mobile_toggle"
                >
                  <span class="svg-icon svg-icon-1">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="https://www.w3.org/2000/svg"
                    >
                      <path
                        d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z"
                        fill="currentColor"
                      />
                      <path
                        opacity="0.3"
                        d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div class="header-logo me-5 me-md-10 flex-grow-1 flex-lg-grow-0">
                <a href="https://telkomakses.co.id/">
                  <img
                    alt="Logo"
                    src="https://telkomakses.co.id/assets/media/logos/logo2.png"
                    class="p-3 logo-default h-50px"
                  />
                  <img
                    alt="Logo"
                    src="https://telkomakses.co.id/assets/media/logos/logo2.png"
                    class="p-3 logo-sticky h-50px"
                  />
                </a>
              </div>
              <div
                class="d-flex align-items-stretch justify-content-between flex-lg-grow-1"
              >
                <div></div>
                <div class="d-flex align-items-stretch" id="kt_header_nav">
                  <div
                    class="header-menu align-items-stretch"
                    data-kt-drawer="true"
                    data-kt-drawer-name="header-menu"
                    data-kt-drawer-activate="{default: true, lg: false}"
                    data-kt-drawer-overlay="true"
                    data-kt-drawer-width="{default:'200px', '300px': '250px'}"
                    data-kt-drawer-direction="start"
                    data-kt-drawer-toggle="#kt_header_menu_mobile_toggle"
                    data-kt-swapper="true"
                    data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav'}"
                  ></div>
                </div>
              </div>
            </div>
          </div>
          <div
            id="kt_content_container"
            class="d-flex flex-column-fluid align-items-start"
          >
            <div class="content flex-row-fluid py-0" id="kt_content">
              <div>
                <div
                  id="carousel_hero"
                  class="carousel carousel-custom slide"
                  data-bs-ride="carousel"
                  data-bs-interval="5000"
                >
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img
                        class="img-fluid w-100 h-500px h-lg-700px"
                        src="https://telkomakses.co.id/upload/hero/Hero_230626_b91ff00b6f.jpg"
                        style="object-fit: cover; filter: brightness(60%)"
                      />
                      <div
                        class="w-100 h-500px h-lg-700px position-absolute"
                        style="top: 0"
                      >
                        <div
                          class="container d-flex flex-column align-items-center text-center"
                        >
                          <!-- TIDAK ADA BACKGROUND PUTIH PADA BANNER, MOHON CEK KEMBALI FORMAT YANG ANDA INPUTKAN PADA TEXT EDITOR -->
                          <span
                            class="fs-5hx fw-bold text-white mt-20 d-none d-lg-block"
                            ><div style="font-size: 32px">
                              Telkom Akses Untuk Broadband Indonesia
                            </div></span
                          >
                          <span
                            class="fs-2hx fw-bold text-white mt-20 d-block d-lg-none"
                            ><div style="font-size: 32px">
                              Telkom Akses Untuk Broadband Indonesia
                            </div></span
                          >
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img
                        class="img-fluid w-100 h-500px h-lg-700px"
                        src="https://telkomakses.co.id/upload/hero/Hero_230704_d7a7d28be0.jpg"
                        style="object-fit: cover; filter: brightness(60%)"
                      />
                      <div
                        class="w-100 h-500px h-lg-700px position-absolute"
                        style="top: 0"
                      >
                        <div
                          class="container d-flex flex-column align-items-center text-center"
                        >
                          <!-- TIDAK ADA BACKGROUND PUTIH PADA BANNER, MOHON CEK KEMBALI FORMAT YANG ANDA INPUTKAN PADA TEXT EDITOR -->
                          <span
                            class="fs-5hx fw-bold text-white mt-20 d-none d-lg-block"
                            ><div style="font-size: 32px">
                              To Serve Network Company in Providing Excellent
                              Vision Digital Infrastructure
                            </div></span
                          >
                          <span
                            class="fs-2hx fw-bold text-white mt-20 d-block d-lg-none"
                            ><div style="font-size: 32px">
                              To Serve Network Company in Providing Excellent
                              Vision Digital Infrastructure
                            </div></span
                          >
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    class="w-100 position-absolute pb-10 text-center"
                    style="bottom: 0"
                  >
                    <ol
                      class="p-0 m-0 carousel-indicators carousel-indicators-bullet carousel-indicators-active-white"
                    >
                      <li
                        data-bs-target="#carousel_hero"
                        data-bs-slide-to="0"
                        class="ms-1 active"
                      ></li>
                      <li
                        data-bs-target="#carousel_hero"
                        data-bs-slide-to="1"
                        class="ms-1"
                      ></li>
                    </ol>
                  </div>
                </div>

                <div
                  class="bg-primary"
                  style="
                    background-image: url('https://telkomakses.co.id/assets/background-1.png');
                    background-repeat: no-repeat;
                  "
                >
                  <div class="container py-10 py-lg-20">
                    <div class="text-white w-100 pt-20 fs-1 slide-up">
                      <span class="fw-bolder text-center"
                        ><p>
                          Terimakasih Lowongan Sudah Ditutup Karena Quota Sudah
                          Terpenuhi..
                        </p>
                      </span>
                    </div>
                    <br />
                    <br />
                    <div class="d-flex flex-center">
                      <div
                        class="text-white text-center text-lg-start w-100 pb-20 fs-1 slide-up"
                      >
                        <!-- TIDAK ADA BACKGROUND PUTIH PADA BANNER, MOHON CEK KEMBALI FORMAT YANG ANDA INPUTKAN PADA TEXT EDITOR -->
                        <span class="opacity-75"></span>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  class="bg-dark"
                  style="
                    background-image: url('https://telkomakses.co.id/assets/background-map.png');
                    background-repeat: no-repeat;
                    background-size: cover;
                  "
                >
                  <div class="container py-10 py-lg-20">
                    <!-- HATI - HATI DALAM MENGINPUTKAN LATITUDE DAN LONGITUDE, SEBAIKNYA AMBIL DARI GOOGLE MAP KARENA TITIKNYA AKURAT -->
                    <div
                      id="map_widget"
                      class="min-h-auto h-300px h-lg-500px mt-10 mt-lg-5"
                    ></div>
                    <div
                      class="d-flex align-items-center justify-content-between flex-column flex-lg-row text-center mb-10 mb-lg-5 gap-10"
                    >
                      <div>
                        <span
                          class="fs-4hx text-white"
                          data-kt-countup="true"
                          data-kt-countup-value="61"
                          >0</span
                        >
                        <br />
                        <span class="fs-3 text-white opacity-75"
                          >Area Tercover</span
                        >
                      </div>
                      <div>
                        <span
                          class="fs-4hx text-white"
                          data-kt-countup="true"
                          data-kt-countup-value="18"
                          data-kt-countup-suffix="k+"
                          >0</span
                        >
                        <br />
                        <span class="fs-3 text-white opacity-75">Karyawan</span>
                      </div>
                      <div>
                        <span
                          class="fs-4hx text-white"
                          data-kt-countup="true"
                          data-kt-countup-value="7"
                          >0</span
                        >
                        <br />
                        <span class="fs-3 text-white opacity-75">Regional</span>
                      </div>
                      <div>
                        <span
                          class="fs-4hx text-white"
                          data-kt-countup="true"
                          data-kt-countup-value="29"
                          >0</span
                        >
                        <br />
                        <span class="fs-3 text-white opacity-75">Teritori</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <style type="text/css">
                .gsap-marker-scroller-start {
                  display: none !important;
                }
                .gsap-marker-scroller-end {
                  display: none !important;
                }
                .gsap-marker-start {
                  display: none !important;
                }
                .gsap-marker-end {
                  display: none !important;
                }
                .gsap-marker-scroller-start {
                  display: none !important;
                }
                .gsap-marker-scroller-end {
                  display: none !important;
                }
                .gsap-marker-start {
                  display: none !important;
                }
                .gsap-marker-end {
                  display: none !important;
                }
              </style>

              <script src="https://telkomakses.co.id/assets/map.js"></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/amchart/index.js"
              ></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/amchart/xy.js"
              ></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/amchart/percent.js"
              ></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/amchart/radar.js"
              ></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/amchart/animated.js"
              ></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/amchart/map.js"
              ></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/amchart/indonesiaLow.js"
              ></script>

              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/lottie.min.js"
              ></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/gsap.min.js"
              ></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/scrolltrigger.js"
              ></script>
              <script
                type="text/javascript"
                src="https://telkomakses.co.id/assets/js/scrollreveal.js"
              ></script>

              <script type="text/javascript">
                var element = document.getElementById("map_widget");
                var root = am5.Root.new(element);
                root.setThemes([am5themes_Animated.new(root)]);
                var container1 = root.container.children.push(
                  am5.Container.new(root, {
                    layout: root.horizontalLayout,
                    width: am5.p100,
                    height: am5.p100,
                  })
                );
                root._logo.dispose();

                var map = container1.children.push(
                  am5map.MapChart.new(root, {
                    panX: "none",
                    panY: "none",
                    wheelY: "none",
                    projection: am5map.geoMercator(),
                    width: am5.percent(100),
                  })
                );

                var polygonSeries = map.series.push(
                  am5map.MapPolygonSeries.new(root, {
                    fill: am5.color("#808285"),
                    geoJSON: am5geodata_indonesiaLow,
                  })
                );

                polygonSeries.mapPolygons.template.setAll({
                  tooltipText: false,
                  interactive: true,
                });

                var pointSeries = map.series.push(
                  am5map.MapPointSeries.new(root, {})
                );
                var pointSeriesHQ = map.series.push(
                  am5map.MapPointSeries.new(root, {})
                );

                pointSeries.bullets.push(function () {
                  var container2 = am5.Container.new(root, {
                    tooltipHTML: `{title}{caption}`,
                    cursorOverStyle: "pointer",
                    fill: am5.color("#fff"),
                  });

                  var circle3 = container2.children.push(
                    am5.Circle.new(root, {
                      radius: 5,
                      tooltipY: 0,
                      fill: am5.color("#e31b23"),
                      interactive: true,
                      strokeOpacity: 0,
                    })
                  );

                  var circle2 = container2.children.push(
                    am5.Circle.new(root, {
                      radius: 3,
                      tooltipY: 0,
                      fill: am5.color("#e31b23"),
                      interactive: true,
                      strokeOpacity: 0,
                    })
                  );

                  var circle1 = container2.children.push(
                    am5.Circle.new(root, {
                      radius: 5,
                      tooltipY: 0,
                      fill: am5.color("#e31b23"),
                      interactive: true,
                      strokeOpacity: 0,
                    })
                  );

                  circle3.animate({
                    key: "scale",
                    from: 1,
                    to: 5,
                    duration: 2000,
                    easing: am5.ease.out(am5.ease.cubic),
                    loops: Infinity,
                  });

                  circle3.animate({
                    key: "opacity",
                    from: 1,
                    to: 0.1,
                    duration: 2000,
                    easing: am5.ease.out(am5.ease.cubic),
                    loops: Infinity,
                  });

                  circle2.animate({
                    key: "scale",
                    from: 1,
                    to: 5,
                    duration: 2000,
                    easing: am5.ease.out(am5.ease.cubic),
                    loops: Infinity,
                  });

                  circle2.animate({
                    key: "opacity",
                    from: 1,
                    to: 0.3,
                    duration: 2000,
                    easing: am5.ease.out(am5.ease.cubic),
                    loops: Infinity,
                  });

                  container2.events.on("click", (e) => {
                    changeBg(e.target.dataItem.dataContext.wilayah);
                  });

                  return am5.Bullet.new(root, {
                    sprite: container2,
                    circle3: circle3,
                    circle2: circle2,
                    circle1: circle1,
                  });
                });

                pointSeriesHQ.bullets.push(function () {
                  var container2 = am5.Container.new(root, {
                    tooltipHTML: `{title}{caption}`,
                    cursorOverStyle: "pointer",
                    fill: am5.color("#fff"),
                  });

                  var circle3 = container2.children.push(
                    am5.Circle.new(root, {
                      radius: 5,
                      tooltipY: 0,
                      fill: am5.color("#fff"),
                      interactive: true,
                      strokeOpacity: 0,
                    })
                  );

                  var circle2 = container2.children.push(
                    am5.Circle.new(root, {
                      radius: 3,
                      tooltipY: 0,
                      fill: am5.color("#fff"),
                      interactive: true,
                      strokeOpacity: 0,
                    })
                  );

                  var circle1 = container2.children.push(
                    am5.Circle.new(root, {
                      radius: 5,
                      tooltipY: 0,
                      fill: am5.color("#fff"),
                      interactive: true,
                      strokeOpacity: 0,
                    })
                  );

                  circle3.animate({
                    key: "scale",
                    from: 1,
                    to: 5,
                    duration: 2000,
                    easing: am5.ease.out(am5.ease.cubic),
                    loops: Infinity,
                  });

                  circle3.animate({
                    key: "opacity",
                    from: 1,
                    to: 0.1,
                    duration: 2000,
                    easing: am5.ease.out(am5.ease.cubic),
                    loops: Infinity,
                  });

                  circle2.animate({
                    key: "scale",
                    from: 1,
                    to: 5,
                    duration: 2000,
                    easing: am5.ease.out(am5.ease.cubic),
                    loops: Infinity,
                  });

                  circle2.animate({
                    key: "opacity",
                    from: 1,
                    to: 0.3,
                    duration: 2000,
                    easing: am5.ease.out(am5.ease.cubic),
                    loops: Infinity,
                  });

                  container2.events.on("click", (e) => {
                    changeBg(e.target.dataItem.dataContext.wilayah);
                  });

                  return am5.Bullet.new(root, {
                    sprite: container2,
                    circle3: circle3,
                    circle2: circle2,
                    circle1: circle1,
                  });
                });

                map.appear(1000, 100);

                function changeBg(selected) {
                  const s = selected.replaceAll(" ", "_").toLowerCase();
                  const wilayah = {
                    sumatra: [0, 2, 3, 6, 11, 17, 24, 26, 30, 32],
                    nusa_tenggara: [1, 20, 21, 34],
                    jawa: [4, 7, 8, 9, 10, 33],
                    sulawesi: [5, 25, 27, 28, 29, 31],
                    kalimantan: [12, 13, 14, 15, 16, 35, 36, 37],
                    kepulauan_maluku: [18, 19],
                    papua: [22, 23],
                  };

                  for (var i = 0; i < 38; i++) {
                    polygonSeries.mapPolygons.values[i].set(
                      "fill",
                      am5.color("#808285")
                    );
                  }

                  for (var j = 0; j < wilayah[s].length; j++) {
                    polygonSeries.mapPolygons.values[wilayah[s][j]].set(
                      "fill",
                      am5.color("#e31b23")
                    );
                  }

                  for (var k = 0; k < pointSeries.data.values.length; k++) {
                    if (
                      s ==
                      pointSeries.data.values[k].wilayah
                        .replaceAll(" ", "_")
                        .toLowerCase()
                    ) {
                      pointSeries.dataItems[k].bullets[0]._settings.circle1.set(
                        "fill",
                        am5.color("#fff")
                      );
                      pointSeries.dataItems[k].bullets[0]._settings.circle2.set(
                        "fill",
                        am5.color("#fff")
                      );
                      pointSeries.dataItems[k].bullets[0]._settings.circle3.set(
                        "fill",
                        am5.color("#fff")
                      );
                    } else {
                      pointSeries.dataItems[k].bullets[0]._settings.circle1.set(
                        "fill",
                        am5.color("#e31b23")
                      );
                      pointSeries.dataItems[k].bullets[0]._settings.circle2.set(
                        "fill",
                        am5.color("#e31b23")
                      );
                      pointSeries.dataItems[k].bullets[0]._settings.circle3.set(
                        "fill",
                        am5.color("#e31b23")
                      );
                    }
                  }
                }

                // var slideUp = {
                // 	distance: '150%',
                // 	origin: 'bottom',
                // 	opacity: null,
                // 	easing: 'ease-in',
                // }
                // ScrollReveal().reveal('.slide-up', slideUp)
                gsap.registerPlugin(ScrollTrigger);
                // REVEAL //
                gsap.utils.toArray(".slide-up").forEach(function (elem) {
                  ScrollTrigger.create({
                    trigger: elem,
                    start: "top 80%",
                    end: "bottom 20%",
                    markers: false,
                    onEnter: function () {
                      gsap.fromTo(
                        elem,
                        { y: 100, autoAlpha: 0 },
                        {
                          duration: 1.25,
                          y: 0,
                          autoAlpha: 1,
                          ease: "back",
                          overwrite: "auto",
                        }
                      );
                    },
                    onLeave: function () {
                      gsap.fromTo(
                        elem,
                        { autoAlpha: 1 },
                        { autoAlpha: 0, overwrite: "auto" }
                      );
                    },
                    onEnterBack: function () {
                      gsap.fromTo(
                        elem,
                        { y: -100, autoAlpha: 0 },
                        {
                          duration: 1.25,
                          y: 0,
                          autoAlpha: 1,
                          ease: "back",
                          overwrite: "auto",
                        }
                      );
                    },
                    onLeaveBack: function () {
                      gsap.fromTo(
                        elem,
                        { autoAlpha: 1 },
                        { autoAlpha: 0, overwrite: "auto" }
                      );
                    },
                  });
                });

                function send(btn) {
                  if (
                    !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
                      $("[name=email]").val()
                    )
                  ) {
                    toastr.error("Email tidak valid");
                    return false;
                  }
                  $.ajax({
                    url: "https://telkomakses.co.id/data/kirim",
                    data: {
                      id_info: $("[name=id_info]").val(),
                      ref: $("[name=ref]").val(),
                      pengirim: $("[name=pengirim]").val(),
                      email: $("[name=email]").val(),
                      telp: $("[name=telp]").val(),
                      pesan: $("[name=pesan]").val(),
                    },
                    type: "POST",
                    beforeSend: function (xhr, settings) {
                      $(btn).prop("disabled", true);
                    },
                    success: function (response) {
                      if (response["status"]) {
                        // RESPON PAKAI MODAL SWEETALERT
                        Swal.fire({
                          text: response["msg"],
                          icon: "success",
                          customClass: {
                            confirmButton: "btn btn-success",
                          },
                        });
                        $("[name=id_info]").val("");
                        $("[name=ref]").val("");
                        $("[name=pengirim]").val("");
                        $("[name=email]").val("");
                        $("[name=telp]").val("");
                        $("[name=pesan]").val("");
                      } else {
                        // RESPON PAKAI MODAL SWEETALERT
                        Swal.fire({
                          text: response["msg"],
                          icon: "error",
                          customClass: {
                            confirmButton: "btn btn-danger",
                          },
                        });
                      }
                      $(btn).prop("disabled", false);
                    },
                    error: function (error) {
                      // RESPON PAKAI MODAL SWEETALERT
                      Swal.fire({
                        text: error.statusText,
                        icon: "error",
                        customClass: {
                          confirmButton: "btn btn-danger",
                        },
                      });
                      $(btn).prop("disabled", false);
                    },
                  });
                }

                document.addEventListener("DOMContentLoaded", () => {
                  var owl = $(".owl-carousel");
                  owl.owlCarousel({
                    center: true,
                    items: 1,
                    loop: true,
                    margin: 20,
                    responsive: {
                      600: {
                        items: 4,
                      },
                    },
                  });

                  $("#kt_team_slider_prev").click(function () {
                    owl.trigger("prev.owl");
                  });
                  $("#kt_team_slider_next").click(function () {
                    owl.trigger("next.owl");
                  });
                  // FILTER TELEPON HANYA NOMOR
                  $("#telp").on("input", (e) => {
                    const value = e.target.value.replace(/[^0-9]/g, "");
                    if (value >= 0) {
                      $(e.target).val(value);
                    } else {
                      $(e.target).val(0).trigger("input");
                    }
                  });

                  $.ajax({
                    url: "https://telkomakses.co.id/data/map",
                    type: "POST",
                    beforeSend: function (xhr, settings) {},
                    success: function (response) {
                      let points = response["data"];
                      for (var i = 0; i < points.length; i++) {
                        var point = points[i];

                        if (point["is_pusat"] == "1") {
                          pointSeriesHQ.data.push({
                            wilayah: point.wilayah,
                            geometry: {
                              type: "Point",
                              coordinates: [
                                parseFloat(point.longitude),
                                parseFloat(point.latitude),
                              ],
                            },
                            title: point.judul,
                            caption: point.caption,
                          });
                        } else {
                          pointSeries.data.push({
                            wilayah: point.wilayah,
                            geometry: {
                              type: "Point",
                              coordinates: [
                                parseFloat(point.longitude),
                                parseFloat(point.latitude),
                              ],
                            },
                            title: point.judul,
                            caption: point.caption,
                          });
                        }
                      }
                    },
                    error: function (error) {
                      toastr.error(error.statusText);
                    },
                  });
                });
              </script>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div
      id="kt_scrolltop"
      class="scrolltop"
      data-kt-scrolltop="true"
      style="background-color: white; color: red"
    >
      <i class="fa fa-chevron-up text-primary"></i>
    </div>

    <script>
      var hostUrl = "assets/";
    </script>
    <script src="https://telkomakses.co.id/assets/plugins/global/plugins.bundle.js"></script>
    <script src="https://telkomakses.co.id/assets/js/scripts.bundle.js"></script>
    <script src="https://telkomakses.co.id/assets/plugins/custom/formrepeater/formrepeater.bundle.js"></script>
    <script src="https://telkomakses.co.id/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script src="https://telkomakses.co.id/assets/plugins/custom/fslightbox/fslightbox.bundle.js"></script>
    <script src="https://telkomakses.co.id/assets/plugins/custom/flatpickr.id.js"></script>
    <script src="https://telkomakses.co.id/assets/owl/owl.carousel.min.js"></script>
    <script type="text/javascript">
      function localDate(date) {
        const m = moment(date, "YYYY-MM-DD");
        return m.isValid() ? m.format("DD/MM/YYYY") : "-";
      }

      function localDatetime(datetime) {
        const m = moment(datetime, "YYYY-MM-DD H:mm");
        return m.isValid() ? m.format("DD/MM/YYYY H:mm") : "-";
      }

      $(".local-date").each((i, j) => {
        const m = moment($(j).text(), "YYYY-MM-DD");
        if (m.isValid()) {
          $(j).text(m.format("DD/MM/YYYY"));
        } else {
          $(j).text("-");
        }
      });

      $(".local-date-month").each((i, j) => {
        const m = moment($(j).text(), "YYYY-MM-DD");
        if (m.isValid()) {
          $(j).text(m.locale("id").format("DD MMM YYYY"));
        } else {
          $(j).text("-");
        }
      });

      $(".local-datetime").each((i, j) => {
        const m = moment($(j).text(), "YYYY-MM-DD H:mm");
        if (m.isValid()) {
          $(j).text(m.format("DD/MM/YYYY H:mm"));
        } else {
          $(j).text("-");
        }
      });

      // $('#modal_cari').on('shown.bs.modal', function() {
      // 	$('.modal-backdrop').addClass('modal-backdrop-og')
      // })

      $(document).ready(function () {});
    </script>
  </body>
</html>
