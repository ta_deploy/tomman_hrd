<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1024">
    <link rel="icon" href="/image/icon.png" type="image/png">
    <meta name="description" content="Mari Bergabung Bersama Kami Putra Putri Terbaik di Banua Kalimantan untuk Berkesempatan Berkarir pada PT Telkom Akses dan Mitra - Mitra Kami di Seluruh Area Kalimantan | PERWIRA">
    <meta name="keywords" content="rekrutmen telkom akses kalsel,rekrutmen indihome kalsel,perwira tomman,info loker telkom,info loker telkomakses,info loker kalimantan,info loker kalsel,personalia warrior akses,tomman,perwira telkomakses,perwira telkom akses,warrior akses,loker,indihome,indihome banjarmasin">
    <meta name="copyright" content="tomman.app"/>
    <meta name="author" content="Personalia Warior Akses - PERWIRA">
    <meta name="google-site-verification" content="JtYYY_8Z43BZrb7vjCMsOyo8QM2SfnqJNWldfFhhk1k" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rekrutmen Telkom Akses Kalsel | INDIHOME - PERWIRA TOMMAN</title>

    {{-- Global site tag (gtag.js) - Google Analytics --}}
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-204467533-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date() );

    gtag('config', 'UA-204467533-2');
    </script>
    {{-- Google Tag Manager --}}
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T7VJRQX');</script>
    {{-- End Google Tag Manager --}}
    {{-- Font Icon --}}
    <link rel="stylesheet" href="/library_recruitment/fonts/material-icon/css/material-design-iconic-font.min.css">
    {{-- Main css --}}
    <link rel="stylesheet" href="/library_recruitment/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
    .container {
        max-width: 990px;
    }
    .steps {
        padding-top: 10px;
        padding-right: 5px;
        padding-bottom: 1px;
        padding-left: 50px;
    }
    </style>
</head>
<body>
    {{-- Google Tag Manager (noscript) --}}
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T7VJRQX"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    {{-- End Google Tag Manager (noscript) --}}
    <div class="main">
        <div class="container">
        {{-- Modal --}}
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">📣 Pengumuman</h5>
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> --}}
                {{-- <span aria-hidden="true">&times;</span> --}}
                </button>
            </div>
            <div class="modal-body">
            Untuk informasi terbaru terkait perusahaan bisa rekan cek di sosial media kami dibawah ini
            <br/><br/>
            <i class="fab fa-instagram"></i> Instagram : <a href="https://instagram.com/kalsel_hibat" style="color: black;">@kalsel_hibat</a>
            <br/>
            <i class="fab fa-instagram"></i> Instagram : <a href="https://www.instagram.com/infotelkomaksesbalikpapan" style="color: black;">@infotelkomaksesbalikpapan</a>
            <br/><br/>
            <i class="fab fa-youtube"></i> Youtube : <a href="https://www.youtube.com/@kalselhibat9382" style="color: black;">Telkom Akses Banjarmasin</a>
            <br/>
            <i class="fab fa-youtube"></i> Youtube : <a href="https://www.youtube.com/@performancekalimantanbersa8320" style="color: black;">Telkom Akses Balikpapan</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>
            <h2>JOIN US!<br><b>PERSONALIA</b> <b style="color: red !important">WARIOR AKSES</b></h2><br>
            @include('Partial.alerts')
            <form method="POST" enctype="multipart/form-data" autocomplete="off" id="signup-form" class="signup-form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h3>
                    <span class="title_text">Data Pribadi</span>
                </h3>
                <fieldset>
                    <div class="fieldset-content">
                        <div class="form-group">
                            <label for="noktp" class="form-label">Nomor KTP *</label>
                            <input type="number" minlength="16" name="noktp" class="numbers" id="noktp" placeholder="Nomor KTP" required/>

                            {!! $errors->first('noktp','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="nama" class="form-label">Nama *</label>
                            <input type="text" class="caps" name="nama" id="nama" placeholder="Nama Lengkap" required/>

                            {!! $errors->first('nama','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-radio">
                            <label for="jk" class="form-label">Jenis Kelamin *</label>
                            <div class="form-radio-item checkbox-group required">
                                <input type="radio" name="jk" value="Laki-Laki" id="Laki-Laki" checked="checked" />
                                <label for="Laki-Laki">Laki-Laki</label>

                                <input type="radio" name="jk" value="Perempuan" id="Perempuan" />
                                <label for="Perempuan">Perempuan</label>

                                {!! $errors->first('jk','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-select">
                            <label for="agama" class="form-label">Agama *</label>
                            <select name="agama" id="agama" required>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Khatolik">Khatolik</option>
                                <option value="Buddha">Buddha</option>
                                <option value="Konghucu">Konghucu</option>
                            </select>

                            {!! $errors->first('agama','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="tempat_lahir" class="form-label">Tempat Lahir *</label>
                            <input type="text" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" required/>

                            {!! $errors->first('tempat_lahir','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-date">
                            <label for="tgl_lahir" class="form-label">Tanggal Lahir *</label>
                            <div class="form-date-item">
                                <input type="date" name="tgl_lahir" id="tgl_lahir" required/>
                                <span style="display: none;" class="warning_sgn"><code style="color: red">* Minimal Umur Adalah 18 Tahun!</code></span>

                                {!! $errors->first('tgl_lahir','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-select">
                            <label for="goldar" class="form-label">Golongan Darah *</label>
                            <select name="goldar" id="goldar" required>
                                @foreach($get_golongan_darah as $key => $val)
                                    <option value="{{ $val->id }}">{{ $val->text }}</option>
                                @endforeach
                            </select>

                            {!! $errors->first('goldar','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="form-label">Email *</label>
                            <input type="email" name="email" id="email" placeholder="Email" required/>

                            {!! $errors->first('email','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="npwp" class="form-label">NPWP</label>
                            <input type="text" name="npwp" id="npwp" data-inputmask="'mask': '9{2}.9{3}.9{3}.9{1}-9{3}.9{3}'" />

                            {!! $errors->first('npwp','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-textarea">
                            <label for="alamat" class="form-label">Alamat *</label>
                            <textarea style="resize: none" name="alamat" id="alamat" required placeholder="Alamat Lengkap ..." ></textarea>

                            {!! $errors->first('alamat','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-select">
                            <label for="provinsi" class="form-label">Provinsi *</label>
                            <select name="provinsi" id="provinsi" required>
                                @foreach($get_area as $key => $val)
                                    <option value="{{ $key }}">{{ $val }}</option>
                                @endforeach
                            </select>

                            {!! $errors->first('provinsi','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-select">
                            <label for="kota" class="form-label">Kabupaten / Kota *</label>
                            <select name="kota" id="kota" required></select>

                            {!! $errors->first('kota','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-select">
                            <label for="kecamatan" class="form-label">Kecamatan *</label>
                            <select name="kecamatan" id="kecamatan" required></select>

                            {!! $errors->first('kecamatan','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-select">
                            <label for="kelurahan" class="form-label">Kelurahan *</label>
                            <select name="kelurahan" id="kelurahan" required></select>

                            {!! $errors->first('kelurahan','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-select">
                            <label for="lokasi_kerja" class="form-label">Lokasi Kerja yang Diinginkan *</label>
                            <select name="lokasi_kerja" id="lokasi_kerja" required>
                                @foreach($get_lokasi_kerja as $key => $val)
                                    <option value="{{ $val->id }}">{{ $val->text }}</option>
                                @endforeach
                            </select>

                            {!! $errors->first('goldar','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="no_telp" class="form-label">No Handphone *</label>
                            <input type="number" minlength="11" name="no_telp" id="no_telp" placeholder="No Handphone yang Aktif" class="numbers" required/>

                            {!! $errors->first('no_telp','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="no_wa" class="form-label">No WhatsApp *</label>
                            <input type="number" minlength="11" class="numbers" name="no_wa" id="no_wa" placeholder="No WhatsApp yang Aktif" required/>

                            {!! $errors->first('no_wa','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group checkbox-group required">
                            <label>Cek Kelengkapan *</label>
                              <input type="checkbox" class="check" id="smartphone" name="ket[smartphone]" value="1">
                              <label for="smartphone">&nbsp;SmartPhone</label>
                              <input type="checkbox" class="check" id="laptop" name="ket[laptop]" value="1">
                              <label for="laptop">&nbsp;Laptop</label>
                              <input type="checkbox" class="check" id="sim" name="ket[sim]" value="1">
                              <label for="sim">&nbsp;SIM</label>
                              <input type="checkbox" class="check" id="motor" name="ket[motor]" value="1">
                              <label for="motor">&nbsp;Motor</label>
                        </div>
                        <div class="form-group checkbox-group">
                            <label>Keahlian (Silahkan Pilih Salah Satu)</label>
                            <input type="checkbox" class="check" id="microsoft_office" name="skill[microsoft_office]" value="1">
                            <label for="microsoft_office">&nbsp;Microsoft Office</label>
                            <input type="checkbox" class="check" id="autocad" name="skill[autocad]" value="1">
                            <label for="autocad">&nbsp;AutoCAD</label>
                            <input type="checkbox" class="check" id="web_dev" name="skill[web_dev]" value="1">
                            <label for="web_dev">&nbsp;Web Developer</label>
                            <input type="checkbox" class="check" id="design_grafis" name="skill[design_grafis]" value="1">
                            <label for="design_grafis">&nbsp;Design Grafis</label>
                        </div>
                    </div>
                    <div class="fieldset-footer">
                        <span>Step 1 of 4</span>
                    </div>
                </fieldset>
                <h3>
                    <span class="title_text">Data Keluarga</span>
                </h3>
                <fieldset>
                    <div class="fieldset-content">
                        <div class="form-group">
                            <label for="nokk" class="form-label">Nomor Kartu Keluarga *</label>
                            <input type="number" minlength="14" name="nokk" id="nokk" class="numbers" placeholder="Nomor Kartu Keluarga" required/>

                            {!! $errors->first('nokk','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-select">
                            <label for="status_perkawinan" class="form-label">Status Perkawinan *</label>
                            <select name="status_perkawinan" id="status_perkawinan" required>
                                <option value="Belum Menikah">Belum Menikah</option>
                                <option value="Sudah Menikah">Sudah Menikah</option>
                                <option value="Bercerai">Bercerai</option>
                            </select>

                            {!! $errors->first('status_perkawinan','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="ibu" class="form-label">Nama Ibu Kandung *</label>
                            <input type="text" name="ibu" id="ibu" class="caps" placeholder="Nama Ibu Kandung" required/>

                            {!! $errors->first('ibu','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="telpon_2_nm" class="form-label">Nama Keluarga yang Bisa Dihubungi *</label>
                            <input type="text" name="telpon_2_nm" id="telpon_2_nm" class="caps" placeholder="Nama Keluarga yang Bisa Dihubungi" required/>

                            {!! $errors->first('telpon_2_nm','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="telpon_2" class="form-label">Kontak Keluarga yang Bisa Dihubungi *</label>
                            <input type="number" minlength="11" name="telpon_2" id="telpon_2" class="numbers" placeholder="Kontak Keluarga yang Bisa Dihubungi" required/>

                            {!! $errors->first('telpon_2','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="relasi" class="form-label">Rekomendasi / Relasi Keluarga Telkom Group</label>
                            <input type="text" name="relasi" id="relasi" class="caps" placeholder="Diisi Nama Orang atau NIK Pegawai"/>

                            {!! $errors->first('relasi','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                    </div>
                    <div class="fieldset-footer">
                        <span>Step 2 of 4</span>
                    </div>
                </fieldset>
                <h3>
                    <span class="title_text">Pendidikan</span>
                </h3>
                <fieldset>
                    <div class="fieldset-content">
                        <div class="form-select">
                            <label for="lvl_pendidikan" class="form-label">Jenjang *</label>
                            <select name="lvl_pendidikan" id="lvl_pendidikan" required>
                                @foreach ($get_level_pendidikan as $lvl_pendidikan)
                                {{-- <option value="{{ $lvl_pendidikan->id_level_pendidikan }}">{{ $lvl_pendidikan->level_pendidikan }}</option> --}}
                                <option value="{{ $lvl_pendidikan->level_pendidikan }}">{{ $lvl_pendidikan->level_pendidikan }}</option>
                                @endforeach
                            </select>

                            {!! $errors->first('lvl_pendidikan','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="last_study" class="form-label">Nama Institusi Pendidikan Terakhir *</label>
                            <input type="text" name="last_study" id="last_study" placeholder="Nama Institusi Pendidikan Terakhir" required/>

                            {!! $errors->first('last_study','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-group">
                            <label for="jurusan" class="form-label">Jurusan *</label>
                            <input type="text" name="jurusan" id="jurusan" placeholder="Nama Jurusan (Diisi untuk Jenjang SMA/SMK Keatas)" required/>

                            {!! $errors->first('jurusan','<label><code style="color: red">:message</code></label>') !!}
                        </div>
                        <div class="form-date">
                        <label for="tgllulus" class="form-label">Tanggal Lulus *</label>
                            <div class="form-date-item">
                                <input type="date" name="tgllulus" id="tgllulus" required/>

                                {!! $errors->first('tgllulus','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="fieldset-footer">
                        <span>Step 3 of 4</span>
                    </div>
                </fieldset>
                <h3>
                    <span class="title_text">Dokumen</span>
                </h3>
                <fieldset>
                    <div class="fieldset-content">
                        <div class="form-group">
                            <label for="file_ktp" class="form-label">Upload KTP (.img) *</label>
                            <div class="form-file">
                                <input type="file" name="file_ktp" id="file_ktp" class="custom-file-input" required/>
                                <span id='val'></span>
                                <span class='button'>Select File</span>

                                {!! $errors->first('file_ktp','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_kk" class="form-label">Upload Kartu Keluarga (.img) *</label>
                            <div class="form-file">
                                <input type="file" name="file_kk" id="file_kk" class="custom-file-input" required/>
                                <span id='val'></span>
                                <span class='button'>Select File</span>

                                {!! $errors->first('file_kk','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_sim" class="form-label">Upload Kartu SIM C (.img)</label>
                            <div class="form-file">
                                <input type="file" name="file_sim" id="file_sim" class="custom-file-input" />
                                <span id='val'></span>
                                <span class='button'>Select File</span>

                                {!! $errors->first('file_sim','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_sim_b" class="form-label">Upload Kartu SIM B (.img)</label>
                            <div class="form-file">
                                <input type="file" name="file_sim_b" id="file_sim_b" class="custom-file-input" />
                                <span id='val'></span>
                                <span class='button'>Select File</span>

                                {!! $errors->first('file_sim_b','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_sim_b" class="form-label">Upload Sertifikat Multimedia (.img)</label>
                            <div class="form-file">
                                <input type="file" name="file_sim_b" id="file_sim_b" class="custom-file-input" />
                                <span id='val'></span>
                                <span class='button'>Select File</span>

                                {!! $errors->first('file_sim_b','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_surat_keterangan_sehat" class="form-label">Upload Surat Keterangan Sehat (.pdf) *</label>
                            <div class="form-file">
                                <input type="file" name="file_surat_keterangan_sehat" id="file_surat_keterangan_sehat" class="custom-file-input" required/>
                                <span id='val'></span>
                                <span class='button'>Select File</span>

                                {!! $errors->first('file_surat_keterangan_sehat','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_surat_berkelakuan_baik" class="form-label">Upload Surat Berkelakuan Baik (.pdf) *</label>
                            <div class="form-file">
                                <input type="file" name="file_surat_berkelakuan_baik" id="file_surat_berkelakuan_baik" class="custom-file-input" required/>
                                <span id='val'></span>
                                <span class='button'>Select File</span>

                                {!! $errors->first('file_surat_berkelakuan_baik','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_cv" class="form-label">Upload CV (.pdf) *</label>
                            <div class="form-file">
                                <input type="file" name="file_cv" id="file_cv" class="custom-file-input" required/>
                                <span id='val'></span>
                                <span class='button'>Select File</span>

                                {!! $errors->first('file_cv','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_ijazah" class="form-label">Upload Ijazah Terakhir (.pdf) *</label>
                            <div class="form-file">
                                <input type="file" name="file_ijazah" id="file_ijazah" class="custom-file-input" required/>
                                <span id='val'></span>
                                <span class='button'>Select File</span>

                                {!! $errors->first('file_ijazah','<label><code style="color: red">:message</code></label>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_sertifikat" class="form-label">Upload Pengalaman Kerja / Sertifikat / Piagam (.pdf)</label>
                            <div class="form-file">
                                <input type="file" name="file_sertifikat" id="file_sertifikat" class="custom-file-input" />
                                <span id='val'></span>
                                <span class='button'>Select File</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_sertifikat_vaksin" class="form-label">Upload Sertifikat Vaksin Terakhir (.pdf) *</label>
                            <div class="form-file">
                                <input type="file" name="file_sertifikat_vaksin" id="file_sertifikat_vaksin" class="custom-file-input" required/>
                                <span id='val'></span>
                                <span class='button'>Select File</span>
                            </div>
                        </div>
                    </div>

                    <div class="fieldset-footer">
                        <span>Step 4 of 4</span>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    {{-- JS --}}
    {{-- <script src="/library_recruitment/vendor/jquery/jquery.min.js"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/library_recruitment/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="/library_recruitment/vendor/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="/library_recruitment/vendor/jquery-steps/jquery.steps.min.js"></script>
    <script src="/library_recruitment/vendor/minimalist-picker/dobpicker.js"></script>
    <script src="/library_recruitment/vendor/jquery.pwstrength/jquery.pwstrength.js"></script>
    <script src="/library_recruitment/js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js" integrity="sha512-QFzu4/E9RKcqiUkfuaCtPW/9oOPdVtvpo35J8w7eoVDEhP/Boy+T50AnJw34a0gvWPJLcAli8HLMbrppmZBFsA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        $( function(){
            $('div.checkbox-group.required :checkbox:checked').length > 0

            $(window).on('load', function() {
                $('#myModal').modal('show');
            });

            $('select').select2({
				placeholder: "- Silahkan Pilih -",
            });

            $('#noktp').focusout( function(){
                console.log($(this).val().length)
                if($(this).val().length == 16)
                {
                    // console.log('mantap')
                }
            })

            $(":input").inputmask();

            $('#provinsi').val(null).change();

            $('#provinsi').on('select2:select', function(){
                $.ajax({
                    type: "GET",
                    url: '/rec/get_region',
                    dataType: 'JSON',
                    data: {
                        data: $(this).val(),
                        jenis: 'kota'
                    }
                }).done( function(e){
                    $('#kota').empty();


                    $('#kota').select2({
                        placeholder: "- Silahkan Pilih -",
                        data: e
                    });

                    $('#kota').val(null).change();
                });
            });

            $('#kota').on('select2:select', function(){
                $.ajax({
                    type: "GET",
                    url: '/rec/get_region',
                    dataType: 'JSON',
                    data: {
                        data: $(this).val(),
                        jenis: 'kecamatan'
                    }
                }).done( function(e){
                    $('#kecamatan').empty();

                    $('#kecamatan').select2({
                        placeholder: "- Silahkan Pilih -",
                        data: e
                    });

                    $('#kecamatan').val(null).change();

                });
            });

            $('#kecamatan').on('select2:select', function(){
                $.ajax({
                    type: "GET",
                    url: '/rec/get_region',
                    dataType: 'JSON',
                    data: {
                        data: $(this).val(),
                        jenis: 'kelurahan'
                    }
                }).done( function(e){
                    $('#kelurahan').empty();

                    $('#kelurahan').select2({
                        placeholder: "- Silahkan Pilih -",
                        data: e
                    });

                    $('#kelurahan').val(null).change();
                });
            });

            $('#tgl_lahir').on('change', function(e){
                var x2 = new Date(),
                x1 = new Date($(this).val() ),
                cals = new Date(x2.getTime() - x1.getTime() );

                var final = cals.getUTCFullYear() - 1970;
                if(final < 18){
                    $('.warning_sgn').css({
                        'display' : 'block'
                    });
                    $(this).val(' ');
                }else{
                    $('.warning_sgn').css({
                        'display' : 'none'
                    });
                }
            });

			$('.numbers').on('keyup', function(e){
				if (e.which != 8 && e.which != 0 && e.which < 48 || e.which > 57) {
					$(this).val(function (index, value) {
						return value.replace(/\D/g, "");
					});
				}
			});

			$('.caps').on('keyup', function(e){
                $(this).val(function (index, value) {
                    return value.replace(/^(.)|\s+(.)/g, c => c.toUpperCase() );
                });
			});
        });
    </script>
</body>
</html>