@extends('layout')
@section('title')
Event
@endsection
@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<style>
	/* List image */
	@import url('https://fonts.googleapis.com/css?family=Reenie+Beanie');

	figure, figcaption{
		display: block;
	}

	#polaroid{
		width:100%;
		padding:0px 10px;
		margin:auto;
		text-align: center;
	}

	#polaroid img{
		max-width: 100%;
		width: 100%;
		height: auto;
	}

	#polaroid figure{
		position:relative;
		width: auto;
		max-width: 600px; /* Lebar maksimal gambar */
		margin: 20px auto 0px;
		padding: 6px 8px 10px 8px;
		display:inline-block;
		-webkit-box-shadow: 4px 4px 8px -4px rgba(0, 0, 0, .75);
		-moz-box-shadow: 4px 4px 8px -4px rgba(0, 0, 0, .75);
		box-shadow: 4px 4px 8px -4px rgba(0, 0, 0, .75);
		background-color: #eee6d8;
		-webkit-transform:rotate(-1deg);
		-moz-transform: rotate(-1deg);
		-o-transform: rotate(-1deg);
		-ms-transform: rotate(-1deg);
		transform: rotate(-1deg);
		-webkit-backface-visibility:hidden;
	}

	#polaroid figure:nth-child(even){
		margin:20px 18px 20px 25px;
		-webkit-transform:rotate(2deg);
		-moz-transform: rotate(2deg);
		-o-transform: rotate(2deg);
		-ms-transform: rotate(2deg);
		transform: rotate(2deg);
		-webkit-backface-visibility:hidden;
		-webkit-box-shadow: 4px 4px 8px -4px rgba(0, 0, 0, .75);
		-moz-box-shadow: 4px 4px 8px -4px rgba(0, 0, 0, .75);
		box-shadow: -4px 4px 8px -4px rgba(0, 0, 0, .75);
	}

	#polaroid figure:before{
		content: '';
		display: block;
		position: absolute;
		left: 5px;
		top: -10px;
		width: 75px;
		height: 25px;
		z-index: 1;
		background-color: rgba(222,220,198,0.7);
		-webkit-transform: rotate(-12deg);
		-moz-transform: rotate(-12deg);
		-o-transform: rotate(-12deg);
		-ms-transform: rotate(-12deg);
	}

	#polaroid figure:nth-child(even):before{
		left:unset;
		right:10px;
		top:-10px;
		width: 55px;
		height: 25px;
		z-index: 1;
		-webkit-transform: rotate(12deg);
		-moz-transform: rotate(12deg);
		-o-transform: rotate(12deg);
		-ms-transform: rotate(12deg);
	}

	#polaroid figcaption{
		text-align:center;
		font-family: 'Reenie Beanie', cursive;
		font-size:1.75em;
		color:#454f40;
		letter-spacing:0.09em;
		margin-top:10px;
	}
	/* List image */

	/*SPARKLING BUTTON  */
	.glow-on-hover{
    width: 220px;
    height: 50px;
    border: none;
    outline: none;
    color: #000000;
    background: #eee6d8;
    cursor: pointer;
    position: relative;
    z-index: 0;
    border-radius: 10px;
	}

	.glow-on-hover:before{
		content: '';
		background: linear-gradient(45deg, #ff0000, #ff7300, #fffb00, #48ff00, #00ffd5, #002bff, #7a00ff, #ff00c8, #ff0000);
		position: absolute;
		top: -2px;
		left:-2px;
		background-size: 400%;
		z-index: -1;
		filter: blur(5px);
		width: calc(100% + 4px);
		height: calc(100% + 4px);
		animation: glowing 20s linear infinite;
		opacity: 0;
		transition: opacity .3s ease-in-out;
		border-radius: 10px;
	}

	.glow-on-hover:active{
		color: #000
	}

	.glow-on-hover:active:after{
		background: transparent;
	}

	.glow-on-hover:hover:before{
		opacity: 1;
	}

	.glow-on-hover:after{
		z-index: -1;
		content: '';
		position: absolute;
		width: 100%;
		height: 100%;
		background: #eee6d8;
		left: 0;
		top: 0;
		border-radius: 10px;
	}

	@keyframes glowing{
		0%{ background-position: 0 0; }
		50%{ background-position: 400% 0; }
		100%{ background-position: 0 0; }
	}
	/*SPARKLING BUTTON  */

	/* Rotate Images */
	.circle img{
		width:110px;
		height:110px;
		margin:3px;
		-moz-border-radius: 100px 100px 100px 100px;
		-webkit-border-radius: 100px 100px 100px 100px;
		border-radius: 100px 100px 100px 100px;
		-webkit-transition: width 2s, height 2s, -webkit-transform 2s;
		transition: width 2s, height 2s, transform 2s;
	}

	/* Rotate Images */

	/* Custom font */
	@import url(https://fonts.googleapis.com/css?family=Amatic+SC);
	.output{
		/* The command I know best. */
		text-transform: uppercase;
		font-family: 'Amatic SC';
		color:#000000;
		font-size:3em;
		font-weight:bold;
		text-align:center;
		background-image: repeating-linear-gradient(to right, #a2682a 0%, #be8c3c 8%, #be8c3c 18%, #d3b15f 27%, #faf0a0 35%, #ffffc2 40%, #faf0a0 50%, #d3b15f 58%, #be8c3c 67%, #b17b32 77%, #bb8332 83%, #d4a245 88%, #e1b453 93%, #a4692a 100%);
		background-size: 150%;
		-webkit-background-clip: text;
		-webkit-text-fill-color: transparent;
		filter: drop-shadow(0 0 1px rgba(255, 200, 0, .3) );
		animation: MoveBackgroundPosition 6s ease-in-out infinite;
	}

	@keyframes MoveBackgroundPosition{
		0%{
			background-position: 0% 50%
		}

		50%{
		background-position: 100% 50%
		}

		100%{
		background-position: 0% 50%
		}
	}
	/* Custom font */

	/* badge 123 */

	.quiz-medal{
	 margin: 30px 0 0 30px;
	}
	.quiz-medal{
		position: relative;
		margin-bottom: 16px;
	}
	.quiz-medal__circle{
		font-family: 'Roboto', sans-serif;
		font-size: 28px;
		font-weight: 500;
		width: 56px;
		height: 56px;
		border-radius: 100%;
		color: white;
		text-align: center;
		line-height: 46px;
		vertical-align: middle;
		position: relative;
		border-width: 0.2em;
		border-style: solid;
		z-index: 1;
		box-shadow: inset 0 0 0 #a7b2b8, 2px 2px 0 rgba(0, 0, 0, 0.08);
		border-color: #edeff1;
		text-shadow: 2px 2px 0 #98a6ad;
		background: linear-gradient(to bottom right, #d1d7da 50%, #c3cbcf 50%);
	}
	.quiz-medal__circle.quiz-medal__circle--gold{
		box-shadow: inset 0 0 0 #b67d05, 2px 2px 0 rgba(0, 0, 0, 0.08);
		border-color: #fadd40;
		text-shadow: 0 0 4px #9d6c04;
		background: linear-gradient(to bottom right, #f9ad0e 50%, #e89f06 50%);
	}
	.quiz-medal__circle.quiz-medal__circle--silver{
		box-shadow: inset 0 0 0 #a7b2b8, 2px 2px 0 rgba(0, 0, 0, 0.08);
		border-color: #edeff1;
		text-shadow: 0px 0px 4px #98a6ad;
		background: linear-gradient(to bottom right, #d1d7da 50%, #c3cbcf 50%);
	}
	.quiz-medal__circle.quiz-medal__circle--bronze{
		box-shadow: inset 0 0 0 #955405, 2px 2px 0 rgba(0, 0, 0, 0.08);
		border-color: #f7bb23;
		text-shadow: 0 0 4px #7d4604;
		background: linear-gradient(to bottom right, #df7e08 50%, #c67007 50%);
	}
	.quiz-medal__ribbon{
		content: "";
		display: block;
		position: absolute;
		border-style: solid;
		border-width: 6px 10px;
		width: 0;
		height: 20px;
		top: 50px;
	}
	.quiz-medal__ribbon--left{
		border-color: #fc402d #fc402d transparent #fc402d;
		left: 8px;
		transform: rotate(20deg) translateZ(-32px);
	}
	.quiz-medal__ribbon--right{
		left: 28px;
		border-color: #f31903 #f31903 transparent #f31903;
		transform: rotate(-20deg) translateZ(-48px);
	}
	/* badge 123 */

	/* stars */
	.blob{
		height: 50px;
		width: 50px;
		color: #ffcc00;
		position: absolute;
		top: 45%;
		left: 45%;
		z-index: 1;
		font-size: 30px;
		display: none;
	}
	/* stars */

	/* mini icon prize */
	.rolling_back{
		width: 110px;
		height: 110px;
		margin: 3px;
		-moz-border-radius: 100px 100px 100px 100px;
		-webkit-border-radius: 100px 100px 100px 100px;
		border-radius: 100px 100px 100px 100px;
		-webkit-transition: width 2s; height 2s; -webkit-transform 2s;
		transition: width 2s; height 2s; transform 2s;
	}

	.rolling{
		width:180px;
		height:180px;
		border:4px #F1F1F1 solid;
		-moz-border-radius: 100px 100px 100px 100px ;
		-webkit-border-radius: 100px 100px 100px 100px ;
		border-radius: 100px 100px 100px 100px ;
		-webkit-transform: rotate(30deg);
		transform: rotate(720deg);
		filter: alpha(opacity=40);
	}
	/* mini icon prize */

	/* Golden conffeti */
	.confetti_particle {
		background: rgb(166,124,0);
		background: linear-gradient(0deg, rgba(166,124,0,1) 0%, rgba(191,155,48,1) 10%, rgba(255,191,0,1) 20%, rgba(255,207,64,1) 30%, rgba(255,220,115,1) 40%, rgba(255,216,99,1) 50%, rgba(255,220,115,1) 60%, rgba(255,207,64,1) 70%, rgba(255,191,0,1) 80%, rgba(191,155,48,1) 90%, rgba(166,124,0,1) 100%);
			border: 1px solid #A57C01;
			position: absolute;
			display: flex;
			width: 10px;
			height: 25px;
			top: -100px;
		}

		.confetti_particle:nth-child(1) {
			animation: fall 2.5s linear infinite;
			left: 10%;
		}

		.confetti_particle:nth-child(2) {
			animation: fall 2.3s linear infinite .2s;
			left: 20%;
		}

		.confetti_particle:nth-child(3) {
			animation: fall 2.4s linear infinite .4s;
			left: 30%;
		}

		.confetti_particle:nth-child(4) {
			animation: fall 2.7s linear infinite .1s;
			left: 40%;
		}

		.confetti_particle:nth-child(5) {
			animation: fall 2.6s linear infinite .7s;
			left: 50%;
		}

		.confetti_particle:nth-child(6) {
			animation: fall 2.4s linear infinite .2s;
			left: 60%;
		}

		.confetti_particle:nth-child(7) {
			animation: fall 2.1s linear infinite .7s;
			left: 70%;
		}

		.confetti_particle:nth-child(8) {
			animation: fall 2.4s linear infinite .9s;
			left: 80%;
		}

		.confetti_particle:nth-child(9) {
			animation: fall 2.9s linear infinite .9s;
			left: 90%;
		}

		.confetti_particle:nth-child(10) {
			animation: fall 2.2s linear infinite 1.1s;
			left: 100%;
		}

		.confetti_particle:nth-child(11) {
			animation: fall2 2.5s linear infinite;
			left: 95%;
		}

		.confetti_particle:nth-child(12) {
			animation: fall2 2.3s linear infinite 1.1s;
			left: 85%;
		}

		.confetti_particle:nth-child(13) {
			animation: fall2 2.4s linear infinite 1.2s;
			left: 75%;
		}

		.confetti_particle:nth-child(14) {
			animation: fall2 2.7s linear infinite 1.3s;
			left: 65%;
		}

		.confetti_particle:nth-child(15) {
			animation: fall2 2.6s linear infinite 1.4s;
			left: 55%;
		}

		.confetti_particle:nth-child(16) {
			animation: fall2 2.4s linear infinite 1.5s;
			left: 45%;
		}

		.confetti_particle:nth-child(17) {
			animation: fall2 2.1s linear infinite 1.6s;
			left: 35%;
		}

		.confetti_particle:nth-child(18) {
			animation: fall2 2.4s linear infinite 1.7s;
			left: 25%;
		}

		.confetti_particle:nth-child(19) {
			animation: fall2 2.9s linear infinite 1.8s;
			left: 15%;
		}

		.confetti_particle:nth-child(20) {
			animation: fall2 2.2s linear infinite 1.9s;
			left: 5%;
		}

		.confetti_particle:nth-child(21) {
			animation: fall 2.5s linear infinite;
			left: 3%;
		}

		.confetti_particle:nth-child(22) {
			animation: fall 2.3s linear infinite 1.3s;
			left: 13%;
		}

		.confetti_particle:nth-child(23) {
			animation: fall 2.4s linear infinite 1.4s;
			left: 23%;
		}

		.confetti_particle:nth-child(24) {
			animation: fall 2.7s linear infinite 1.5s;
			left: 33%;
		}

		.confetti_particle:nth-child(25) {
			animation: fall 2.6s linear infinite 1.6s;
			left: 43%;
		}

		.confetti_particle:nth-child(26) {
			animation: fall 2.4s linear infinite 1.2s;
			left: 53%;
		}

		.confetti_particle:nth-child(27) {
			animation: fall 2.1s linear infinite 1.7s;
			left: 63%;
		}

		.confetti_particle:nth-child(28) {
			animation: fall 2.4s linear infinite 1.8s;
			left: 73%;
		}

		.confetti_particle:nth-child(29) {
			animation: fall 2.9s linear infinite 1.9s;
			left: 83%;
		}

		.confetti_particle:nth-child(30) {
			animation: fall 2.2s linear infinite 1.1s;
			left: 93%;
		}

		.confetti_particle:nth-child(31) {
			animation: fall2 2.2s linear infinite 1.1s;
			left: 7%;
		}

		.confetti_particle:nth-child(32) {
			animation: fall2 2.9s linear infinite .9s;
			left: 17%;
		}

		.confetti_particle:nth-child(33) {
			animation: fall2 2.4s linear infinite .1s;
			left: 27%;
		}

		.confetti_particle:nth-child(34) {
			animation: fall2 2.1s linear infinite .7s;
			left: 37%;
		}

		.confetti_particle:nth-child(35) {
			animation: fall2 2.4s linear infinite .2s;
			left: 47%;
		}

		.confetti_particle:nth-child(36) {
			animation: fall2 2.6s linear infinite .7s;
			left: 57%;
		}

		.confetti_particle:nth-child(37) {
			animation: fall2 2.7s linear infinite .9s;
			left: 67%;
		}

		.confetti_particle:nth-child(38) {
			animation: fall2 2.4s linear infinite .4s;
			left: 77%;
		}

		.confetti_particle:nth-child(39) {
			animation: fall2 2.3s linear infinite .2s;
			left: 87%;
		}

		.confetti_particle:nth-child(40) {
			animation: fall2 2.5s linear infinite .3s;
			left: 97%;
		}

		@keyframes fall {
			0% {top:-110px; transform: rotate(0deg) rotateY(-90deg); opacity:1;}
			100% {top:900px; transform: rotate(360deg) rotateY(180deg); opacity:0.7;}
		}

		@keyframes fall2 {
			0% {top:-110px; transform: rotate(0deg) rotateY(90deg); opacity:1;}
			100% {top:900px; transform: rotate(-360deg) rotateY(-180deg); opacity:0.5;}
		}
	/* Golden conffeti */

	/* Portofolio */
	.portfolio-grid figure {
		position: relative;
		float: left;
		overflow: hidden;
		width: 100%;
		background: #464dee;
		text-align: center;
		cursor: pointer;
	}

	.portfolio-grid figure img {
		position: relative;
		display: block;
		min-height: 100%;
		max-width: 100%;
		width: 100%;
		opacity: 0.8;
	}

	.portfolio-grid figure figcaption {
		padding: 2em;
		color: #ffffff;
		text-transform: uppercase;
		font-size: 1.25em;
		-webkit-backface-visibility: hidden;
		backface-visibility: hidden;
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}

	.portfolio-grid figure figcaption:after, .portfolio-grid figure figcaption:before {
		pointer-events: none;
	}

	.portfolio-grid figure.effect-text-in {
		border-radius: 0.25rem;
	}

	.portfolio-grid figure.effect-text-in h4,
	.portfolio-grid figure.effect-text-in img {
		-webkit-transition: -webkit-transform 0.35s;
		transition: transform 0.35s;
	}

	.portfolio-grid figure.effect-text-in img {
		-webkit-backface-visibility: hidden;
		backface-visibility: hidden;
	}

	.portfolio-grid figure.effect-text-in h4,
	.portfolio-grid figure.effect-text-in p {
		position: absolute;
		bottom: 10px;
		left: 0;
		padding: 10px;
		margin-bottom: 0;
		width: 100%;
	}

	.portfolio-grid figure.effect-text-in p {
		text-transform: none;
		opacity: 0;
		-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
		transition: opacity 0.35s, transform 0.35s;
		-webkit-transform: translate3d(0, 50px, 0);
		transform: translate3d(0, 50px, 0);
	}

	.portfolio-grid figure.effect-text-in:hover img {
		-webkit-transform: translate3d(0, -80px, 0);
		transform: translate3d(0, -80px, 0);
	}

	.portfolio-grid figure.effect-text-in:hover h4 {
		-webkit-transform: translate3d(0, -100px, 0);
		transform: translate3d(0, -100px, 0);
	}

	.portfolio-grid figure.effect-text-in:hover p {
		opacity: 1;
		-webkit-transform: translate3d(0, 0, 0);
		transform: translate3d(0, 0, 0);
	}
	/* Portofolio */

	@media (min-width: 768px) {
		.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	/* Animate fading 1-by-1 */
		@import "compass/css3";

		.block_b {
			display: block;
			width: 60px;
			height: 60px;
			margin: 5px;
			float: left;
		}

		.animate_t {
			animation: fade 1s forwards;
		}

		.portfolio-grid {
			display: block;
			width: 720px;
			margin: 10px auto 0 auto;
			overflow: hidden;
		}

		@keyframes fade {
			0%   { opacity: 1; }
			100% { opacity: 0; }
		}
	/* Animate fading 1-by-1 */

	/* Reveal animation */
		.play_reveal {
			margin: 0;
			text-align: center;
			font-size: 200px;
			overflow: hidden;
			line-height: 1;
			display: block;
			animation: reveal 1.5s cubic-bezier(0.77, 0, 0.175, 1) 0.5s;
		}

		@keyframes reveal {
			0% {
				transform: translate(0,100%);
			}
			100% {
				transform: translate(0,0);
			}
		}
	/* Reveal animation */
</style>
@endsection
@section('content')
<div class="modal modalAnimate fade" id="modal_bronze_prize" tabindex="-1" role="dialog" aria-labelledby="modal_bronze_prize" aria-hidden="true" data-animation-in="fadeInLeft" data-animation-out="bounceOut">
  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modal Heading</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div align="center">
					<div class="circle">
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN0AAADkCAMAAAArb9FNAAAA21BMVEX////t7e0AAADj4+MZ4/q9vb3g4OAuLi7u7u7o6OgA4vvl5eWioqL27uxLS0scHBxA5PmsrKzk7O4/Pz/s4+HPz8/y8vIa6//ExMRTU1MKXWaNjY3O6/BxcXEkJCSu6fJs5vcLCwsb2/EvEgmZmZk2NjZcXFwPiZcWxdmDg4Pb4+S66vHa7O8UFBQvGhZ2dnaL4u8EJysMbXgTr8AguMp95vYb8f/Lw8JIqren2eFVRkM80+aSh4UwjJhAODYAGRytvL4AVF5Uv84YAAAJZW8nHh2h6PMSoLAwAAB4gO8uAAAJgUlEQVR4nO2dCXvTOBCG6yQkdnO0JXEOmoaUTRtKCi3QsAewwHLs/v9ftPV84zLTyHEO5yrz8fD0sS2N9EaKRtHlvT2TyWQymUwmk8lkMplMJtNqdHRQmq6DFSWcmu5RBolUcmnKIBGXUtOtZJCI0a1ERre0fgm6s3qSzpqBUHOJpLShKSlmThcGSfLUB/poiaQeKUtNL0F+mD1dvpigsspTe4mk2spSkE9Q2eiMzujWQleGVkwXkopB+aeCi2OhylEkTrh5lCZ2IBxLGrrwZMPMCa+Y7hEp9H3f4/++L0jL3gmCI24xl6YiAuLixJdAvnICSLi4HjqZsqymwTJ0V6oyekZndEaXSOcp96Do8ql0eUl3so10Af7hT9gmnZOu2kK/P4d+lzevEBCxQi9JG6RT2YDD5d8NHVkYjcNCpMOGuBd0EBBeJUg2uy10+PrFdKLS3tHJrynTeXDT2192Rmd0RrcaOq1mGl1Ane0q01XpKkija7qS2ko6enRHR1dGZ3RGtxwdNRN+Zq0KzG2CDsMbbZ0kKe8n0nVHJKbDRTeRzufxKZVGey3jKlROTVcRhol0jet9UgHCxXUjkS50FRoSLq+WDghz0xXua246tm50Rvdr0fEQKslJx997SVdrkK5bJLQnBVxc41FN0gUwMYUOE4ZZ0HlXF5EGGBK/Ool05WE4H222cgzteBCeMoB7n/vjSH+8If1JruBPXPxBT/qfEdCTw+naLPsbGtv3OBfI0oDyd+UtRMcDWgPYrdPFARxYGf5Wuz35IbNH7kZltd8acvGXRbUatvajsuzKnoCrSrTRV8AAjXdAcetUht4AlvJL0VFdC0DXi+koN1PooC594RLo6OvXlb04Jx3sMV0JdFSRA6MzugdIx636Mehg9xRLSrzUVqVKuqTG/x3T0SOf5xaG7+jZJQKmtirgb55S3BLojmEpmJMLLVONWv88BojRcc/zB3/BXg2PHHRV+OznXPxodrGI5ozMxh/8cwSsOuiYCJ7wgj8gzIGy86P5Zg8uc/Z1lJrOS6bzNkKXL8e1dSk6GBZ0xUXp6EsT08kvzdx01BVT30WjM7rF6agpYDp/vu8dllKsmA5mi/XOrWoDNoxHcraxren+QnDMPcKplDATOahFj/7SdMoSFx3iHlPwOreZXsZ03C2OF5SIvu+tF5Jid4dM9ynXTXZMSsc0TljtCzrPV5bwk49XNVbIrk43QzpWUfRVpiima0i3q+kobw1Fp+RrOllbjM7ojG4aXZWaICcdNVDV7aKLV3rPRvf+Ay1GHJAqPTLbq+CSnnx4n0jHSYUIHXqT6WZPN6PueXMPS/DpAgv8PYc3d8lnSM9RW7aFbraemFv5xFwYndEZnabDJB3TVahP2eQ2Exc8gP8cAZPpgi2k8y5Jo/eRnn38O9qbVMJYfLsUXfz98Rk9GyHgQmlsjo7k3x/xi3RvxG9Kndw1uvzkeGZar8fojM7ovMfQjOl3C4e3Kgw/vb7VafgiUngaXXwa4lE33UhiupnTPYZ7Gs/a0mGW4AcmkxF3Hxc/xATCDHbGiKvwVkBHXYvrudrx6nVhUguZMDqj+7XpMD2px2nlSKrOmms1jovOTzK7Vrr+U9JnucfzM+45OsT+aHwdKS61SOOR43O4TDbbXyMdVg21vuSEvqHFd+XaR+sfrz4luYKNYPabNPulJVY/romO1Hois/EGnQ8XHaRXDjt0S0dm30izT1qyPhud0RmdpsMClFFi/3EaHSKNsLxlG+lef4v05J8+aU46RPrnCdl4vYV0rLfIxpx0MPvWYc/ojM7oHgId7J5hb5Yv+/7pdDeRY3jXqk46Bicdh2tRrJtZ6WSeeFpw3lPNeEf8sZzTddKdyZMRv+IehkEUiYuueoiAiPRVGjqbQodKxRPWS65pT6NT5z6+xD2E6KfS8bw5Ir1UllLpMlk5bHRGt510nlhWNx/dfr8q2tt7dBgv6e8vQoccLbem/Zw23QywWrJMSyseo7uLfBaG6PV+70XiPB28jJRDuHE30ujSQXc5omdjBMxRrPikWrL3Hb3yIZI6RLjHtLqjjCwNKH/n865pV4r3cOHgFLinp1yE+MXi+OBPEa6L9v5HdYKu+gOPugh46ih+/KLiQnuKcJSHVexQE6ta7+iA6KKj6shzQAl09IjmgHzfRaeq5FOsT13Z/jujM7pdoJMbyBHga2Z0X6XL5O3pa6SLh8TZMfxLGn7PiO77EAZh/FAO4K+HLnbFsn7evMqI7tWNrJOqQ7AmOtWNgjKkKyg6kaLRGd2vQsc98VDuI/fjDr5wDDf//fZTOY/puJ85cXrMXT+Twnk5Efc/pkMAblXkSEp81vcgA7o9mt/d61DHve7JQ2pfQIz3TCoeZSJhk+PdeWLYkimX1/gqLsOxdXUqbp1y0eE8ZSeMq/V4qatUY3JW/N6IH37/6jMY9RIml4nGZFIeFrDWMuSSdKUN05WMzuicdD119DWUSuc76fxUusmk/FV9707qtVt1QocOpZYqO2XJlVQnykT9JHM6Vs6hKo65wcjHWNGpGuw4tzav6MYwweZcSa0KC2q6kpSHEi1JRxfqYF+tLH2c0RndA6LbX5huf9N0e4OKUH2Szh/RAsXrOekQidcpKbq6TDCTXwWz6mSSLl6tOCedWtWo6Fbm2xai05qVTsnojM7opuicm2lkyrUwc046uTSGnc/5xuhwQk4eJ8ocu97oNh8d3qEU8EkxOI2nvDE6FlehDOjoln5xy8ZldEZndOuX0Rmd0a1fRvcg6criPZJ3dOJeeYfp/Lx6P+hbSN3L+ztL1+Qp4CkKm0a3SRmd0e0OHfb5z0JH2i265hWJ83nQm9QBx0LA5m7RqQLqlSbVUyGMbkMyOqPbaro9cdrL3HRyq+f5VtFB7NuOeEMb3sqkEJhL3cMboHjL2RHf2zSKQ5rOn4vO3zE6V0/MTUchjW6jeth07Zju50bSe3Tcw3TRYeKO6dqbRnEo7JzfqlORL6YIaZ4x1jkkb9VCBESkCkxsY9mx+AUiGM8L1LlnfNS6usdHCiPSxaYznyqmK09uzsBWCde9ctnotkFGt/N08Zt10ug43K7QhRhBpzdXHFc8MWfAzWNe3vIqCIhIW+wKtFAYNcfmDCU+tme7fu2kC5nupNJ1jG7rZHS7TldzzcdKNXezVeGXWdN7LKaoxG8x2nR2F1Ixl6biprO4hIxud2V0u6uiOhXGodNdpjOZTCaTyWQymUwmk8lkerj6H9ZlVd315wzuAAAAAElFTkSuQmCC" alt="">
					</div>
					<Strong>JUDUL_HADIAH</Strong>
				</div>
				<div class="congrats congrats_0">
					<div class="output output_0"></div>
				</div>
				<div class="congrats congrats_1">
					<div class="output output_1"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info random_ppl">Acak Orang</button>
				{{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> --}}
			</div>
		</div>
	</div>
</div>
<div class="modal modalAnimate fade" id="modal_silver_prize" tabindex="-1" role="dialog" aria-labelledby="modal_silver_prize" aria-hidden="true" data-animation-in="fadeInLeft" data-animation-out="bounceOut">
  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modal Heading silver</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div align="center">
					<div class="circle">
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN0AAADkCAMAAAArb9FNAAAA21BMVEX////t7e0AAADj4+MZ4/q9vb3g4OAuLi7u7u7o6OgA4vvl5eWioqL27uxLS0scHBxA5PmsrKzk7O4/Pz/s4+HPz8/y8vIa6//ExMRTU1MKXWaNjY3O6/BxcXEkJCSu6fJs5vcLCwsb2/EvEgmZmZk2NjZcXFwPiZcWxdmDg4Pb4+S66vHa7O8UFBQvGhZ2dnaL4u8EJysMbXgTr8AguMp95vYb8f/Lw8JIqren2eFVRkM80+aSh4UwjJhAODYAGRytvL4AVF5Uv84YAAAJZW8nHh2h6PMSoLAwAAB4gO8uAAAJgUlEQVR4nO2dCXvTOBCG6yQkdnO0JXEOmoaUTRtKCi3QsAewwHLs/v9ftPV84zLTyHEO5yrz8fD0sS2N9EaKRtHlvT2TyWQymUwmk8lkMplMJtNqdHRQmq6DFSWcmu5RBolUcmnKIBGXUtOtZJCI0a1ERre0fgm6s3qSzpqBUHOJpLShKSlmThcGSfLUB/poiaQeKUtNL0F+mD1dvpigsspTe4mk2spSkE9Q2eiMzujWQleGVkwXkopB+aeCi2OhylEkTrh5lCZ2IBxLGrrwZMPMCa+Y7hEp9H3f4/++L0jL3gmCI24xl6YiAuLixJdAvnICSLi4HjqZsqymwTJ0V6oyekZndEaXSOcp96Do8ql0eUl3so10Af7hT9gmnZOu2kK/P4d+lzevEBCxQi9JG6RT2YDD5d8NHVkYjcNCpMOGuBd0EBBeJUg2uy10+PrFdKLS3tHJrynTeXDT2192Rmd0RrcaOq1mGl1Ane0q01XpKkija7qS2ko6enRHR1dGZ3RGtxwdNRN+Zq0KzG2CDsMbbZ0kKe8n0nVHJKbDRTeRzufxKZVGey3jKlROTVcRhol0jet9UgHCxXUjkS50FRoSLq+WDghz0xXua246tm50Rvdr0fEQKslJx997SVdrkK5bJLQnBVxc41FN0gUwMYUOE4ZZ0HlXF5EGGBK/Ool05WE4H222cgzteBCeMoB7n/vjSH+8If1JruBPXPxBT/qfEdCTw+naLPsbGtv3OBfI0oDyd+UtRMcDWgPYrdPFARxYGf5Wuz35IbNH7kZltd8acvGXRbUatvajsuzKnoCrSrTRV8AAjXdAcetUht4AlvJL0VFdC0DXi+koN1PooC594RLo6OvXlb04Jx3sMV0JdFSRA6MzugdIx636Mehg9xRLSrzUVqVKuqTG/x3T0SOf5xaG7+jZJQKmtirgb55S3BLojmEpmJMLLVONWv88BojRcc/zB3/BXg2PHHRV+OznXPxodrGI5ozMxh/8cwSsOuiYCJ7wgj8gzIGy86P5Zg8uc/Z1lJrOS6bzNkKXL8e1dSk6GBZ0xUXp6EsT08kvzdx01BVT30WjM7rF6agpYDp/vu8dllKsmA5mi/XOrWoDNoxHcraxren+QnDMPcKplDATOahFj/7SdMoSFx3iHlPwOreZXsZ03C2OF5SIvu+tF5Jid4dM9ynXTXZMSsc0TljtCzrPV5bwk49XNVbIrk43QzpWUfRVpiima0i3q+kobw1Fp+RrOllbjM7ojG4aXZWaICcdNVDV7aKLV3rPRvf+Ay1GHJAqPTLbq+CSnnx4n0jHSYUIHXqT6WZPN6PueXMPS/DpAgv8PYc3d8lnSM9RW7aFbraemFv5xFwYndEZnabDJB3TVahP2eQ2Exc8gP8cAZPpgi2k8y5Jo/eRnn38O9qbVMJYfLsUXfz98Rk9GyHgQmlsjo7k3x/xi3RvxG9Kndw1uvzkeGZar8fojM7ovMfQjOl3C4e3Kgw/vb7VafgiUngaXXwa4lE33UhiupnTPYZ7Gs/a0mGW4AcmkxF3Hxc/xATCDHbGiKvwVkBHXYvrudrx6nVhUguZMDqj+7XpMD2px2nlSKrOmms1jovOTzK7Vrr+U9JnucfzM+45OsT+aHwdKS61SOOR43O4TDbbXyMdVg21vuSEvqHFd+XaR+sfrz4luYKNYPabNPulJVY/romO1Hois/EGnQ8XHaRXDjt0S0dm30izT1qyPhud0RmdpsMClFFi/3EaHSKNsLxlG+lef4v05J8+aU46RPrnCdl4vYV0rLfIxpx0MPvWYc/ojM7oHgId7J5hb5Yv+/7pdDeRY3jXqk46Bicdh2tRrJtZ6WSeeFpw3lPNeEf8sZzTddKdyZMRv+IehkEUiYuueoiAiPRVGjqbQodKxRPWS65pT6NT5z6+xD2E6KfS8bw5Ir1UllLpMlk5bHRGt510nlhWNx/dfr8q2tt7dBgv6e8vQoccLbem/Zw23QywWrJMSyseo7uLfBaG6PV+70XiPB28jJRDuHE30ujSQXc5omdjBMxRrPikWrL3Hb3yIZI6RLjHtLqjjCwNKH/n865pV4r3cOHgFLinp1yE+MXi+OBPEa6L9v5HdYKu+gOPugh46ih+/KLiQnuKcJSHVexQE6ta7+iA6KKj6shzQAl09IjmgHzfRaeq5FOsT13Z/jujM7pdoJMbyBHga2Z0X6XL5O3pa6SLh8TZMfxLGn7PiO77EAZh/FAO4K+HLnbFsn7evMqI7tWNrJOqQ7AmOtWNgjKkKyg6kaLRGd2vQsc98VDuI/fjDr5wDDf//fZTOY/puJ85cXrMXT+Twnk5Efc/pkMAblXkSEp81vcgA7o9mt/d61DHve7JQ2pfQIz3TCoeZSJhk+PdeWLYkimX1/gqLsOxdXUqbp1y0eE8ZSeMq/V4qatUY3JW/N6IH37/6jMY9RIml4nGZFIeFrDWMuSSdKUN05WMzuicdD119DWUSuc76fxUusmk/FV9707qtVt1QocOpZYqO2XJlVQnykT9JHM6Vs6hKo65wcjHWNGpGuw4tzav6MYwweZcSa0KC2q6kpSHEi1JRxfqYF+tLH2c0RndA6LbX5huf9N0e4OKUH2Szh/RAsXrOekQidcpKbq6TDCTXwWz6mSSLl6tOCedWtWo6Fbm2xai05qVTsnojM7opuicm2lkyrUwc046uTSGnc/5xuhwQk4eJ8ocu97oNh8d3qEU8EkxOI2nvDE6FlehDOjoln5xy8ZldEZndOuX0Rmd0a1fRvcg6criPZJ3dOJeeYfp/Lx6P+hbSN3L+ztL1+Qp4CkKm0a3SRmd0e0OHfb5z0JH2i265hWJ83nQm9QBx0LA5m7RqQLqlSbVUyGMbkMyOqPbaro9cdrL3HRyq+f5VtFB7NuOeEMb3sqkEJhL3cMboHjL2RHf2zSKQ5rOn4vO3zE6V0/MTUchjW6jeth07Zju50bSe3Tcw3TRYeKO6dqbRnEo7JzfqlORL6YIaZ4x1jkkb9VCBESkCkxsY9mx+AUiGM8L1LlnfNS6usdHCiPSxaYznyqmK09uzsBWCde9ctnotkFGt/N08Zt10ug43K7QhRhBpzdXHFc8MWfAzWNe3vIqCIhIW+wKtFAYNcfmDCU+tme7fu2kC5nupNJ1jG7rZHS7TldzzcdKNXezVeGXWdN7LKaoxG8x2nR2F1Ixl6biprO4hIxud2V0u6uiOhXGodNdpjOZTCaTyWQymUwmk8lkerj6H9ZlVd315wzuAAAAAElFTkSuQmCC" alt="">
					</div>
					<Strong>JUDUL_HADIAH</Strong>
				</div>
				<div class="congrats congrats_0">
					<div class="output output_0"></div>
				</div>
				<div class="congrats congrats_1">
					<div class="output output_1"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info random_ppl_silver">Acak Orang</button>
				{{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> --}}
			</div>
		</div>
	</div>
</div>
<div class="modal modalAnimate fade" id="modal_gold_prize" aria-labelledby="modal_gold_prize" data-animation-out="bounceOut">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modal Heading Gold</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="row portfolio-grid">
											<div class="block_b">
												<figure class="effect-text-in">
													<img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="image">
													<figcaption>
														<h4>Photography</h4>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
													</figcaption>
												</figure>
											</div>
											<div class="block_b">
												<figure class="effect-text-in">
													<img src="https://bootdey.com/img/Content/avatar/avatar2.png" alt="image">
													<figcaption>
														<h4>Lifestyle</h4>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
													</figcaption>
												</figure>
											</div>
											<div class="block_b">
												<figure class="effect-text-in">
													<img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="image">
													<figcaption>
														<h4>Tech Geeks</h4>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
													</figcaption>
												</figure>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info random_ppl_silver">Acak Orang</button>
				{{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> --}}
			</div>
		</div>
	</div>
</div>
<div class="px-content">
	@include('Partial.alerts')
	<div class="col-md-12">
		<div class="page-header">
		<h1>Single Image</h1>
		<div id="polaroid">
			<figure>
				<div class="quiz-medal">
					<div class="quiz-medal__circle quiz-medal__circle--gold">
						<span>
							1
						</span>
					</div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--left"></div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--right"></div>
				</div>
				<img src="https://4.bp.blogspot.com/-sTwXnTVk8L8/XBY9G7VlBII/AAAAAAAAG_w/alIfdUrZqU4oslH96bzbshnu7FR6E4WWACLcBGAs/s600/cat-3846780_1280.jpg" alt="This cat is so cute" title="This cat is so cute"/>
				<figcaption>
					<button type="button" class="glow-on-hover" data-toggle="modal" data-target="#modal_gold_prize">This cat is so cute</button>
				</figcaption>
			</figure>
		</div>
		<br/><br/>
		<h1>Hadiah Silver</h1>
		<div id="polaroid">
			<figure>
				<div class="quiz-medal">
					<div class="quiz-medal__circle quiz-medal__circle--silver">
						<span>
							2
						</span>
					</div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--left"></div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--right"></div>
				</div>
				<img src="https://4.bp.blogspot.com/-sTwXnTVk8L8/XBY9G7VlBII/AAAAAAAAG_w/alIfdUrZqU4oslH96bzbshnu7FR6E4WWACLcBGAs/s250/cat-3846780_1280.jpg" alt="This cat is so cute" title="This cat is so cute"/>
				<figcaption>
					<button type="button" class="glow-on-hover" data-toggle="modal" data-target="#modal_silver_prize">This cat is so cute</button>
				</figcaption>
			</figure>
			<figure>
				<div class="quiz-medal">
					<div class="quiz-medal__circle quiz-medal__circle--silver">
						<span>
							3
						</span>
					</div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--left"></div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--right"></div>
				</div>
				<img src="https://1.bp.blogspot.com/-DI7zmCmceHs/XBY9G7-N1lI/AAAAAAAAG_0/Us-KcjUzSIE4u0YSWxe_6FgiVz3BH55MQCLcBGAs/s250/grimace-388987_1280.jpg" alt="You are so funny!" title="You are so funny!"/>
				<figcaption>
					<button type="button" class="glow-on-hover" data-toggle="modal" data-target="#modal_silver_prize">You are so funny</button>
				</figcaption>
			</figure>
			<figure>
				<div class="quiz-medal">
					<div class="quiz-medal__circle quiz-medal__circle--silver">
						<span>
							3
						</span>
					</div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--left"></div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--right"></div>
				</div>
				<img src="https://2.bp.blogspot.com/-6tJBGSg9aPw/W68kNOiq0zI/AAAAAAAAGbc/O75Yv2U-DnoCEtrWeOPeDfduquQzr_C7ACPcBGAYYCw/s250/Fiksioner%2B01.jpg" alt="Be happy :)" title="Be happy :)"/>
				<figcaption>
					<button type="button" class="glow-on-hover" data-toggle="modal" data-target="#modal_silver_prize">Be happy :)</button>
				</figcaption>
			</figure>
			<figure>
				<div class="quiz-medal">
					<div class="quiz-medal__circle quiz-medal__circle--silver">
						<span>
							3
						</span>
					</div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--left"></div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--right"></div>
				</div>
				<img src="https://1.bp.blogspot.com/-Y1-qTfoRlrU/W68o92NKUyI/AAAAAAAAGb4/2fKXpiRnMDMlWt2APz7Gbxsqac6dvAc5ACLcBGAs/s250/Fiksioner%2B04.jpg" alt="My camera" title="My camera"/>
				<figcaption>
					<button type="button" class="glow-on-hover" data-toggle="modal" data-target="#modal_silver_prize">My camera</button>
				</figcaption>
			</figure>
		</div>
		<h1>Hadiah Bronze</h1>
		<div id="polaroid">
			<figure>
				<div class="quiz-medal">
					<div class="quiz-medal__circle quiz-medal__circle--bronze">
						<span>
							2
						</span>
					</div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--left"></div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--right"></div>
				</div>
				<img src="https://4.bp.blogspot.com/-sTwXnTVk8L8/XBY9G7VlBII/AAAAAAAAG_w/alIfdUrZqU4oslH96bzbshnu7FR6E4WWACLcBGAs/s250/cat-3846780_1280.jpg" alt="This cat is so cute" title="This cat is so cute"/>
				<figcaption>
					<button type="button" class="glow-on-hover" data-toggle="modal" data-target="#modal_bronze_prize">This cat is so cute</button>
				</figcaption>
			</figure>
			<figure>
				<div class="quiz-medal">
					<div class="quiz-medal__circle quiz-medal__circle--bronze">
						<span>
							3
						</span>
					</div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--left"></div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--right"></div>
				</div>
				<img src="https://1.bp.blogspot.com/-DI7zmCmceHs/XBY9G7-N1lI/AAAAAAAAG_0/Us-KcjUzSIE4u0YSWxe_6FgiVz3BH55MQCLcBGAs/s250/grimace-388987_1280.jpg" alt="You are so funny!" title="You are so funny!"/>
				<figcaption>
					<button type="button" class="glow-on-hover" data-toggle="modal" data-target="#modal_bronze_prize">You are so funny</button>
				</figcaption>
			</figure>
			<figure>
				<div class="quiz-medal">
					<div class="quiz-medal__circle quiz-medal__circle--bronze">
						<span>
							2
						</span>
					</div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--left"></div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--right"></div>
				</div>
				<img src="https://2.bp.blogspot.com/-6tJBGSg9aPw/W68kNOiq0zI/AAAAAAAAGbc/O75Yv2U-DnoCEtrWeOPeDfduquQzr_C7ACPcBGAYYCw/s250/Fiksioner%2B01.jpg" alt="Be happy :)" title="Be happy :)"/>
				<figcaption>
					<button type="button" class="glow-on-hover" data-toggle="modal" data-target="#modal_bronze_prize">Be happy :)</button>
				</figcaption>
			</figure>
			<figure>
				<div class="quiz-medal">
					<div class="quiz-medal__circle quiz-medal__circle--bronze">
						<span>
							2
						</span>
					</div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--left"></div>
					<div class="quiz-medal__ribbon quiz-medal__ribbon--right"></div>
				</div>
				<img src="https://1.bp.blogspot.com/-Y1-qTfoRlrU/W68o92NKUyI/AAAAAAAAGb4/2fKXpiRnMDMlWt2APz7Gbxsqac6dvAc5ACLcBGAs/s250/Fiksioner%2B04.jpg" alt="My camera" title="My camera"/>
				<figcaption>
					<button type="button" class="glow-on-hover" data-toggle="modal" data-target="#modal_bronze_prize">My camera</button>
				</figcaption>
			</figure>
		</div>
    </div>
	</div>
</div>
@endsection
@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.2/underscore-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
<script type="module" src="https://cdn.jsdelivr.net/npm/tsparticles-confetti/+esm"></script>
<script>
	$(function(){

		$('.modal').on('show.bs.modal', function(){
			setTimeout(function(){
				$('.circle > img').toggleClass('rolling');
			}, 1000);
		});

		var time_fade_prize;

		$('#modal_gold_prize').on('show.bs.modal', function(){
			var randoNumo = function(value) {
				return Math.floor(Math.random() * value);
			};

			var list = $('.block_b');

			(function go() {
				if( (list.length - $('.block_b.animate_t').length) != 1){
					list.eq(randoNumo(list.length) ).addClass('animate_t');
					time_fade_prize = setTimeout(function() {
						go();
					}, 200);
				}else{
					console.log('tes')
					$.each($('.block_b'), function(k, v) {
						if(!$(this).hasClass("animate_t") ){
							$(this).toggleClass('play_reveal');
							$(this).removeClass('block_b');
						}
					});
				}
			})();
		});

		$('#modal_gold_prize').on('hide.bs.modal', function(){
			clearTimeout(time_fade_prize);
			$('.block_b').removeClass('animate_t');
			$('.play_reveal').addClass('block_b');
			$('.block_b').removeClass('play_reveal');
		});

		function generate_text(k, ctnt, no){
			var theLetters = "abcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()_+<>?[]{}|;:", //You can customize what letters it will cycle through
			speed = 50, // ms per frame
			increment = 8, // frames per step. Must be >2
			si = 0,
			stri = 0,
			block = "",
			fixed = "",
			timer = null;

			function nextFrame(pos, ctnt, no){
				for (var i = 0; i < ctnt.length - stri; i++){
					//Random number
					var num = Math.floor(theLetters.length * Math.random() ) ;
					//Get random letter
					var letter = theLetters.charAt(num);
					block = block + letter;
				}

				if (si == (increment-1) ){
					stri++;
				}

				if (si == increment){
					// Add a letter;
					// every speed*10 ms
					fixed = fixed + ctnt.charAt(stri - 1);
					si = 0;
				}

				$(".output_"+k).html(no + '. ' + fixed + block);

				if(block.length == 0 && k <= 3){
					reset(k);

					animateText(k);

					animateBlobs(k);
				}

				if($("#modal_silver_prize").hasClass("in") && block.length == 0 && k == 0){
					for (let x = 0; x < 41; x++) {
						$('#modal_silver_prize').append("<div class='confetti_particle'></div>");
					}
				}
				block = "";
			}

			(function rustle (i){
				timer = setTimeout(function (){
					if (--i){
						rustle(i);
					}
					nextFrame(i, ctnt, no);
					si = si + 1;
				}, speed);

				$('.modal').on('hide.bs.modal', function(){
					$('.output').text('');
					clearInterval(timer);
				});

			})(ctnt.length * increment + 1);
			si = 0;
			stri = 0;
			fixed = "";
		}

		$('.modal').on('hide.bs.modal', function(){
			$('.circle > img').toggleClass('rolling')
			$('.circle > img').toggleClass('rolling_back')
		});

		$('#modal_silver_prize').on('hide.bs.modal', function(){
			$('.confetti_particle').remove();
		});

		$('.random_ppl').click(function(){
			var ctnt = ["12345", "67890123456"],
			no = 0;

			$.each(ctnt, function(k, v){
				var numberOfStars = 200;

				for (var i = 0; i < numberOfStars; i++){
					$('.congrats_' + k).append('<div class="blob blob_' + k +' fa fa-star ' + i + '"></div>');
				}
				++no;
				generate_text(k, v, no)
			})
			no = 0;

		});

		$('.random_ppl_silver').click(function(){
			var ctnt = ["12345", "67890123456"],
			no = 0;

			$.each(ctnt, function(k, v){
				var numberOfStars = 200;

				for (var i = 0; i < numberOfStars; i++){
					$('.congrats_' + k).append('<div class="blob blob_' + k +' fa fa-star ' + i + '"></div>');
				}
				++no;
				generate_text(k, v, no)
			});

			no = 0;
		});

		function reset(k){
			$.each($('.blob_'+k), function(i){
				TweenMax.set($(this),{ x: 0, y: 0, opacity: 1 });
			});

			TweenMax.set($('.output_' + k),{ scale: 1, opacity: 1, rotation: 0 });
		}

		function animateText(k){
			TweenMax.from($('.output_' + k), 0.8,{
				scale: 0.4,
				opacity: 0,
				rotation: 15,
				ease: Back.easeOut.config(4),
			});
		}

		function animateBlobs(k){
			var xSeed = _.random(350, 380),
			ySeed = _.random(120, 170);

			$.each($('.blob_'+k), function(i){
				var $blob = $(this),
				speed = _.random(1, 5),
				rotation = _.random(5, 100),
				scale = _.random(0.8, 1.5),
				x = _.random(-xSeed, xSeed),
				y = _.random(-ySeed, ySeed);

				TweenMax.to($blob, speed,{
					x: x,
					y: y,
					ease: Power1.easeOut,
					opacity: 0,
					rotation: rotation,
					scale: scale,
					onStartParams: [$blob],
					onStart: function($element){
						$element.css('display', 'block');
					},
					onCompleteParams: [$blob],
					onComplete: function($element){
						$element.css('display', 'none');
						$element.remove();
					}
				});
			});
		}

	})
</script>
@endsection