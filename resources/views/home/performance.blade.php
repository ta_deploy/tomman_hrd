@extends('layout')
@section('title')
Performansi
@endsection
@section('css')
<style>
  th {
    text-align: center;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
  .upper {
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
  <div class="col-md-12">
      <canvas style="height: 300px" id="chart_line"></canvas>
  </div>
  @php
    if(!in_array(session('auth')->perwira_level, [100]) )
    {
      $cl = "col-md-6";
    }
    else
    {
      $cl = "col-md-12";
    }
  @endphp
  <div class="col-md-12" style="padding: 11px;">
      <canvas id="dough" style="height: 500px"></canvas>
  </div>
  <div class="col-md-6">
    <div class="page-header">
      <div class="table-primary">
        <table class="table table-bordered table-striped table-small-font" id="table_ku">
          <thead>
            <tr>
              <th>RANK</th>
              <th>NIK</th>
              <th>NAMA</th>
              <th>MITRA</th>
              <th>TOTAL ORDER</th>
              <th>TOTAL CLOSE</th>
            </tr>
          </thead>
          <tbody class="middle-align">
          @foreach ($get_performansi as $rank => $performansi)
            <tr>
              <td>{{ ++$rank }}</td>
              <td>{{ $performansi->nik }}</td>
              <td>{{ $performansi->nama }}</td>
              <td>{{ $performansi->mitra_amija_pt }}</td>
              <td>{{ $performansi->jml_order }}</td>
              <td>{{ $performansi->jml_close }}</td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="{{ $cl }}">
    <div class="page-header">
      <div class="table-primary">
        <table class="table table-bordered table-striped table-small-font" id="table_bintang">
          <thead>
            <tr>
              <th>RANK</th>
              <th>NAMA</th>
              <th>NIK</th>
              <th class="no-sort">STARS</th>
            </tr>
          </thead>
          <tbody class="middle-align">
            <tr>
              <td>1</td>
              <td>Rendy</td>
              <td>18940469</td>
              <td><div class="rating"></div><span class="live-rating"></span></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
  <script src="https://cdn.jsdelivr.net/npm/rater-js@1.0.1/index.min.js"></script>
	<script>
		$( function() {
      var auth = {!! json_encode(session('auth') ) !!};

      var start_rate = raterJs( {
        starSize:18,
        max:9,
        readOnly:true,
        rating:4.4,
        element:document.querySelector(".rating"),
        rateCallback:function rateCallback(rating, done) {
          this.setRating(rating);
          done();
        },
      });

			$("#table_ku").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }],
        "order": [[ 5, "desc" ]]
			});

			$('#table_ku_wrapper .table-caption').text('PERFORMANSI TEKNISI MITRA');
			$('#table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#table_bintang_wrapper .table-caption').text('List Teknisi Bintang');
			$('#table_bintang_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

      var randomScalingFactor = function() {
        return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
      }

      var main_line = {!! json_encode($get_orders) !!},
      main_pie = {!! json_encode($get_karyawan) !!},
      MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      data_line_order = [],
      data_line_close = [],
      data_pie_label = [],
      data_pie_jml = [],
      dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ", 0.8)";
      },
      data_month = [],
      data_color = [],
      title = '';

      if(!$.isEmptyObject(main_line) ){
        $.each(main_line, function (key, value){
          data_line_order.push(value.jml_order);
          data_line_close.push(value.jml_close);
          data_month.push(MONTHS[value.month - 1]);
          data_color.push(dynamicColors() );
        });
      }

      if(!$.isEmptyObject(main_pie) ){
        title = main_pie[0].title;
        $.each(main_pie, function (key, value){
          data_pie_label.push(value.judul);
          data_pie_jml.push(value.jml);
          data_color.push(dynamicColors() );
        });
      }

      Chart.register(ChartDataLabels);

			var ctx_line = $('#chart_line'),
      chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(231,233,237)'
      },
      config = {
        type: 'line',
        data: {
          labels: data_month,
          datasets: [{
            label: "TOTAL ORDER",
            backgroundColor: chartColors.blue,
            borderColor: chartColors.blue,
            data: data_line_order,
            fill: false,
            datalabels: {
              align: function(context) {
                return context.active ? 'start' : 'center';
              }
            }
          },
          {
            label: "TOTAL CLOSE",
            backgroundColor: chartColors.green,
            borderColor: chartColors.green,
            data: data_line_close,
            fill: false,
            datalabels: {
              align: function(context) {
                return context.active ? 'end' : 'center';
              }
            }
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          plugins: {
						title: {
							display: true,
							text: 'DATA ORDER PER-BULAN'
						},
            datalabels: {
              backgroundColor: function(context) {
                return context.active ? context.dataset.backgroundColor : 'white';
              },
              borderColor: function(context) {
                return context.dataset.backgroundColor;
              },
              borderRadius: function(context) {
                return context.active ? 0 : 32;
              },
              borderWidth: 3,
              color: function(context) {
                return context.active ? 'white' : context.dataset.backgroundColor;
              },
              font: {
                weight: 'bold'
              },
              formatter: function(value, context) {
                value = Math.round(value * 100) / 100;
                return context.active
                  ? context.dataset.label + '\n' + value + ' Buah'
                  : Math.round(value);
              },
              offset: 8,
              padding: 5,
              textAlign: 'center'
            }
					},
          tooltips: {
            mode: 'label',
          },
          hover: {
            mode: 'index',
            intersect: false
          },
          scales: {
            xAxes: [{
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Month'
              }
            }],
            yAxes: [{
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Value'
              }
            }]
          }
        }
      };
      window.myLine = new Chart(ctx_line, config);

      var ctx_pie = $('#dough'),
      doughGraph,
      config_line = {
      type: 'pie',
      data: {
        labels: data_pie_label,
        datasets: [{
          label: '# Data',
          data: data_pie_jml,
          backgroundColor: data_color,
          borderColor: data_color,
          borderWidth: 1,
          datalabels: {
            anchor: 'end'
          }
        }]
      },
        options: {
          tooltips: {
            enabled: false
          },
          responsive: true,
          maintainAspectRatio: false,
		      scaleBeginAtZero: true,
          onHover: (event, chartElement) => {
            event.native.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
          },
          plugins: {
						title: {
							display: true,
              align: 'center',
							text: title
						},
            datalabels: {
              backgroundColor: function(context) {
                return context.dataset.backgroundColor;
              },
              borderColor: 'white',
              borderRadius: 25,
              borderWidth: 2,
              color: 'black',
              display: function(context) {
                var dataset = context.dataset;
                var count = dataset.data.length;
                var value = dataset.data[context.dataIndex];
                return value > count * 1.5;
              },
              font: {
                weight: 'bold'
              },
              padding: 6,
              formatter: Math.round
            },
            legend: {
							position: 'right'
						}
					},

        }
      };

    doughGraph = new Chart(ctx_pie, config_line);

		});
  </script>
@endsection