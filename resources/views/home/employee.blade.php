@extends('layout')
@section('title')
Beranda
@endsection
@section('css')
<style>
  th {
    text-align: center;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
  .upper {
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
@include('Partial.alerts')
  <div class="row">
    <div class="col-md-3">
      <canvas style="width: 350px;" id="pie_karyawan"></canvas>
    </div>
    <div class="col-md-3">
      <canvas style="width: 350px;" id="pie_scmt"></canvas>
    </div>
    <div class="col-md-3">
      <canvas style="width: 350px;" id="pie_scmt_mytech"></canvas>
    </div>
    <div class="col-md-3">
      <canvas style="width: 350px;" id="pie_brevert"></canvas>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
  <script>
		$( function() {
      var kar_active = {!! json_encode($monolog_mytech->karyawan_active ?? '') !!},
      unregister_monolog = {!! json_encode($monolog_mytech->tidak_terdaftar ?? '') !!},
      kar_nonactive = {!! json_encode($monolog_mytech->karyawan_nonactive ?? '') !!},
      scmt_active = {!! json_encode($monolog_mytech->mytech_scmt_active ?? '') !!},
      scmt_blank = {!! json_encode($monolog_mytech->mytech_scmt_blanks ?? '') !!},
      scmt_mytech_active = {!! json_encode($monolog_mytech->scmt_mytech_active ?? '') !!},
      scmt_mytech_blank = {!! json_encode($monolog_mytech->scmt_mytech_blanks ?? '') !!},
      not_brevert = {!! json_encode($get_belum_brevert ?? '') !!},
      pass_brevert = {!! json_encode($get_lulus_brevert ?? '') !!};

      var chartColors = {
        red: 'rgb(255, 99, 132, 0.5)',
        orange: 'rgb(255, 159, 64, 0.5)',
        yellow: 'rgb(255, 205, 86, 0.5)',
        green: 'rgb(75, 192, 192, 0.5)',
        blue: 'rgb(54, 162, 235, 0.5)',
        purple: 'rgb(153, 102, 255, 0.5)',
        grey: 'rgb(231,233,237, 0.5)'
      };

      Chart.register(ChartDataLabels);

			var ctx_bar = $('#pie_karyawan'),
      config = {
        type: 'pie',
        data: {
          labels: ['Aktif', 'Tidak Aktif', 'Tidak Terdaftar Monolog'],
          jenis_data: ['karyawan_aktif', 'karyawan_nonaktif', 'tidak_terdaftar'],
          datasets: [{
            label: 'karyawan',
            data: [kar_active, kar_nonactive, unregister_monolog],
            backgroundColor: [chartColors.green, chartColors.grey, chartColors.blue],
            datalabels: {
              anchor: 'center'
            }
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          onHover: (event, chartElement) => {
            event.native.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
          },
          onClick: (evt) => {
            var activePoint = evt.chart.getElementsAtEventForMode(evt, 'nearest', {}, true)[0],
            label = evt.chart.data.labels[activePoint.index],
            jenis_data = evt.chart.data.jenis_data[activePoint.index],
            value = evt.chart.data.datasets[activePoint.datasetIndex].data[activePoint.index];

            if(typeof activePoint === 'undefined'){
              console.log('tidak ada')
              return false;
            }
            window.open('/hr/monolog#'+ jenis_data)
          },
          plugins: {
            title: {
							display: true,
              align: 'center',
							text: 'List Aktif Karyawan'
						},
            datalabels: {
              backgroundColor: function(context) {
                return context.dataset.backgroundColor;
              },
              borderColor: 'white',
              borderRadius: 25,
              borderWidth: 2,
              color: 'black',
              display: function(context) {
                var dataset = context.dataset;
                var count = dataset.data.length;
                var value = dataset.data[context.dataIndex];
                return value > count * 1.5;
              },
              font: {
                weight: 'bold'
              },
              padding: 6,
              formatter: Math.round
            }
          }
        }
      };

      barGraph = new Chart(ctx_bar, config);

			var ctx_bar = $('#pie_scmt'),
      config = {
        type: 'pie',
        data: {
          labels: ['Aktif', 'Blank'],
          jenis_data: ['mytech_scmt_active', 'mytech_scmt_blanks'],
          datasets: [{
            label: 'scmt',
            data: [scmt_active, scmt_blank],
            backgroundColor: [chartColors.green, chartColors.grey],
            datalabels: {
              anchor: 'center'
            }
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          onHover: (event, chartElement) => {
            event.native.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
          },
          onClick: (evt) => {
            var activePoint = evt.chart.getElementsAtEventForMode(evt, 'nearest', {}, true)[0],
            label = evt.chart.data.labels[activePoint.index],
            value = evt.chart.data.datasets[activePoint.datasetIndex].data[activePoint.index],
            jenis_data = evt.chart.data.jenis_data[activePoint.index];
            if(typeof activePoint === 'undefined'){
              console.log('tidak ada')
              return false;
            }
            console.log(value)
            window.open('/hr/monolog#'+ jenis_data)

          },
          plugins: {
            title: {
							display: true,
              align: 'center',
							text: 'List Aktif SCMT'
						},
            datalabels: {
              backgroundColor: function(context) {
                return context.dataset.backgroundColor;
              },
              borderColor: 'white',
              borderRadius: 25,
              borderWidth: 2,
              color: 'black',
              display: function(context) {
                var dataset = context.dataset;
                var count = dataset.data.length;
                var value = dataset.data[context.dataIndex];
                return value > count * 1.5;
              },
              font: {
                weight: 'bold'
              },
              padding: 6,
              formatter: Math.round
            }
          }
        }
      };

      barGraph = new Chart(ctx_bar, config);

			var ctx_bar = $('#pie_scmt_mytech'),
      config = {
        type: 'pie',
        data: {
          labels: ['Aktif', 'Blank'],
          jenis_data: ['scmt_mytech_active', 'scmt_mytech_blanks'],
          datasets: [{
            label: 'scmt',
            data: [scmt_mytech_active, scmt_mytech_blank],
            backgroundColor: [chartColors.green, chartColors.grey],
            datalabels: {
              anchor: 'center'
            }
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          onHover: (event, chartElement) => {
            event.native.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
          },
          onClick: (evt) => {
            var activePoint = evt.chart.getElementsAtEventForMode(evt, 'nearest', {}, true)[0],
            label = evt.chart.data.labels[activePoint.index],
            value = evt.chart.data.datasets[activePoint.datasetIndex].data[activePoint.index],
            jenis_data = evt.chart.data.jenis_data[activePoint.index];
            if(typeof activePoint === 'undefined'){
              console.log('tidak ada')
              return false;
            }
            console.log(value)
            window.open('/hr/monolog#'+ jenis_data)
          },
          plugins: {
            title: {
							display: true,
              align: 'center',
							text: 'List Aktif SCMT MYTECH'
						},
            datalabels: {
              backgroundColor: function(context) {
                return context.dataset.backgroundColor;
              },
              borderColor: 'white',
              borderRadius: 25,
              borderWidth: 2,
              color: 'black',
              display: function(context) {
                var dataset = context.dataset;
                var count = dataset.data.length;
                var value = dataset.data[context.dataIndex];
                return value > count * 1.5;
              },
              font: {
                weight: 'bold'
              },
              padding: 6,
              formatter: Math.round
            }
          }
        }
      };

      barGraph = new Chart(ctx_bar, config);

			var ctx_bar = $('#pie_brevert'),
      config = {
        type: 'pie',
        data: {
          labels: ['Aktif', 'Blank'],
          datasets: [{
            label: 'scmt',
            data: [not_brevert, pass_brevert],
            backgroundColor: [chartColors.green, chartColors.grey],
            datalabels: {
              anchor: 'center'
            }
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          onHover: (event, chartElement) => {
            event.native.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
          },
          onClick: (evt) => {
            var activePoint = evt.chart.getElementsAtEventForMode(evt, 'nearest', {}, true)[0];
            var label = evt.chart.data.labels[activePoint.index];
            var value = evt.chart.data.datasets[activePoint.datasetIndex].data[activePoint.index];
            if(typeof activePoint === 'undefined'){
              console.log('tidak ada')
              return false;
            }
            console.log(value)
            // // window.location = 'google.com'
            // console.log(label)
          },
          plugins: {
            title: {
							display: true,
              align: 'center',
							text: 'List Aktif Brevert'
						},
            datalabels: {
              backgroundColor: function(context) {
                return context.dataset.backgroundColor;
              },
              borderColor: 'white',
              borderRadius: 25,
              borderWidth: 2,
              color: 'black',
              display: function(context) {
                var dataset = context.dataset;
                var count = dataset.data.length;
                var value = dataset.data[context.dataIndex];
                return value > count * 1.5;
              },
              font: {
                weight: 'bold'
              },
              padding: 6,
              formatter: Math.round
            }
          }
        }
      };

      barGraph = new Chart(ctx_bar, config);
		});
  </script>
@endsection
