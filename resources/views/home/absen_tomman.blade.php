@extends('layout')
@section('title')
Form Absen Teknisi
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="col-md-12">	
			<div class="panel panel-color panel-info panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Absen Tomman Teknisi</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary btn-block"><span class="fa fa-check"></span>&nbsp;Absen</button>
            </div>
          </div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

		});
	</script>
@endsection