@extends('layout')
@section('title')
Event Yang Ada
@endsection
@section('css')
<style>
	#qrcode {
	width: 128px;
	height: 128px;
	margin: 0 auto;
	text-align: center;
	margin-bottom: 11px;
	}
</style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div class="col-md-12">
		<div class="page-header">
			<div class="table-success">
				<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="detail_evet">
					<thead>
						<tr>
							<th>Jenis Event</th>
							<th>Nama Event</th>
						</tr>
					</thead>
					<tbody class="middle-align">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')

@endsection