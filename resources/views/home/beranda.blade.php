@extends('layout')
@section('title')
Beranda
@endsection
@section('css')
<style>
  th {
    text-align: center;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
  .upper {
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="modal modal_pelamar fade modal-large">
  <div class="modal-dialog" style="width:1250px;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title_pelamar"></div>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#tabs-pelamar_month" data-toggle="tab">
              List Pelamar Bulan <span class="month_tab"></span>
            </a>
          </li>
          <li>
            <a href="#tabs-pelamar_now" data-toggle="tab">
             List Pelamar (Hari Ini) <span class="rec_now"></span>
            </a>
          </li><li>
            <a href="#tabs-pelamar_all" data-toggle="tab">
             List Pelamar (Tahun Ini) <span class="rec_all"></span>
            </a>
          </li>
        </ul>
        <div class="tab-content tab-content-bordered">
          <div class="tab-pane fade in active" id="tabs-pelamar_month">
            <div class="page-header">
              <div class="table-default">
                <table class="table table_ada table-bordered table-striped table-small-font" style="width: 100%;" id="pelamar_month">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Pendidikan Terakhir</th>
                      <th>Kabupaten / Kota</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tabs-pelamar_now">
            <div class="page-header">
              <div class="table-default">
                <table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="pelamar_now">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Pendidikan Terakhir</th>
                      <th>Kabupaten / Kota</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tabs-pelamar_all">
            <div class="page-header">
              <div class="table-default">
                <table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="pelamar_all">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Pendidikan Terakhir</th>
                      <th>Kabupaten / Kota</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal_rekrut fade modal-large">
  <div class="modal-dialog" style="width:1250px;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title_rekrut"></div>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#tabs-rekrut_month" data-toggle="tab">
              Daftar Rekrutmen Bulan <span class="recruit_month"></span>
            </a>
          </li>
          <li>
            <a href="#tabs-rekrut_now" data-toggle="tab">
             Daftar Rekrutmen (Hari Ini) <span class="recruit_now"></span>
            </a>
          </li><li>
            <a href="#tabs-rekrut_all" data-toggle="tab">
             Daftar Rekrutmen (Tahun Ini) <span class="recruit_all"></span>
            </a>
          </li>
        </ul>
        <div class="tab-content tab-content-bordered">
          <div class="tab-pane fade in active" id="tabs-rekrut_month">
            <div class="page-header">
              <div class="table-default">
                <table class="table table_ada table-bordered table-striped table-small-font" style="width: 100%;" id="rekrut_month">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tabs-rekrut_now">
            <div class="page-header">
              <div class="table-default">
                <table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="rekrut_now">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tabs-rekrut_all">
            <div class="page-header">
              <div class="table-default">
                <table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="rekrut_all">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal_rejected fade modal-large">
  <div class="modal-dialog" style="width:1250px;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title_rejected"></div>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#tabs-rejected_month" data-toggle="tab">
              Daftar Tidak Lolos Bulan <span class="rejected_month"></span>
            </a>
          </li>
          <li>
            <a href="#tabs-rejected_now" data-toggle="tab">
             Daftar Tidak Lolos (Hari Ini) <span class="rejected_now"></span>
            </a>
          </li><li>
            <a href="#tabs-rejected_all" data-toggle="tab">
             Daftar Tidak Lolos (Tahun Ini) <span class="rejected_all"></span>
            </a>
          </li>
        </ul>
        <div class="tab-content tab-content-bordered">
          <div class="tab-pane fade in active" id="tabs-rejected_month">
            <div class="page-header">
              <div class="table-default">
                <table class="table table_ada table-bordered table-striped table-small-font" style="width: 100%;" id="rejected_month">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tabs-rejected_now">
            <div class="page-header">
              <div class="table-default">
                <table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="rejected_now">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tabs-rejected_all">
            <div class="page-header">
              <div class="table-default">
                <table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="rejected_all">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal_accept fade modal-large">
  <div class="modal-dialog" style="width:1250px;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title_accept"></div>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#tabs-accept_month" data-toggle="tab">
              Daftar Di Terima / Lolos Bulan <span class="accept_month"></span>
            </a>
          </li>
          <li>
            <a href="#tabs-accept_now" data-toggle="tab">
             Daftar Di Terima / Lolos (Hari Ini) <span class="accept_now"></span>
            </a>
          </li><li>
            <a href="#tabs-accept_all" data-toggle="tab">
            Daftar Di Terima / Lolos (Tahun Ini) <span class="accept_all"></span>
            </a>
          </li>
        </ul>
        <div class="tab-content tab-content-bordered">
          <div class="tab-pane fade in active" id="tabs-accept_month">
            <div class="page-header">
              <div class="table-default">
                <table class="table table_ada table-bordered table-striped table-small-font" style="width: 100%;" id="accept_month">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tabs-accept_now">
            <div class="page-header">
              <div class="table-default">
                <table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="accept_now">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tabs-accept_all">
            <div class="page-header">
              <div class="table-default">
                <table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="accept_all">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nomor KTP</th>
                      <th>Tanggal Melamar</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody class="middle-align">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal_rekrut_all fade modal-large">
  <div class="modal-dialog" style="width:1250px;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title_rekrut_all"></div>
      </div>
      <div class="modal-body">
        <div class="page-header">
          <div class="table-default">
            <table class="table table_rekrut table-bordered table-striped table-small-font" style="width: 100%;" id="rekrut">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Nomor KTP</th>
                  <th>Tanggal Melamar</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody class="middle-align">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal_accepted fade modal-large">
  <div class="modal-dialog" style="width:1250px;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title_accepted"></div>
      </div>
      <div class="modal-body">
        <div class="page-header">
          <div class="table-default">
            <table class="table table_accepted table-bordered table-striped table-small-font" style="width: 100%;" id="accepted">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Nomor KTP</th>
                  <th>Tanggal Melamar</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody class="middle-align">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">close</button>
      </div>
    </div>
  </div>
</div>
<div class="px-content">
@include('Partial.alerts')
<h3 style="text-align: center">DASHBOARD REKRUTMEN</h3>
  <form method="get" class="form-horizontal validate" enctype="multipart/form-data">
    <div class="form-group">
      <div class="col-md-3">
        <select class="form-control" id="jk" name="jk[]" multiple>
          <option value="Laki-Laki">Laki-Laki</option>
          <option value="Perempuan">Perempuan</option>
        </select>
      </div>
      <div class="col-md-4">
        <select class="form-control" id="tahun" name="tahun">
          @for ($min = $minim_year->thn; $min <= date('Y'); $min++)
            <option value="{{ $min }}">{{ $min }}</option>
          @endfor
        </select>
      </div>
      <div class="col-md-3">
        <select class="form-control" id="umur" name="umur[]" multiple>
          <option value="18_25">18 Sampai 25</option>
          <option value="25_">25+</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-2">
        <select name="provinsi" class="form-control" id="provinsi" >
          {{-- <option value="all">Semua</option> --}}
          <option value="all">Semua</option>
          @foreach($get_area as $key => $val)
              <option value="{{ $key }}">{{ $val }}</option>
          @endforeach
        </select>
      </div>
      <div class="col-md-3">
        <select name="kota" class="form-control" id="kota"></select>
      </div>
      <div class="col-md-3">
        <select name="kecamatan" class="form-control" id="kecamatan"></select>
      </div>
      <div class="col-md-3">
        <select name="kelurahan" class="form-control" id="kelurahan"></select>
      </div>
    </div>
    <button class="btn btn-block btn-primary" style="margin-bottom: 11px;"><span class="fa fa-search-plus"></span>Cari</button>
  </form>
  <div class="row">
		<div class="col-md-12">
			<canvas style="height: 300px" id="chart_line_kar"></canvas>
		</div>
  </div>
	<div class="row">
    <div class="col-md-12">
      <canvas style="height: 300px" id="recruitment_complete"></canvas>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3">
      <div class="box bg-success darken">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">PELAMAR</div>
          <div class="font-weight-bold font-size-20">{{ count($total_pelamar) }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-arrow-graph-up-right"></i>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="box bg-default darken">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">REKRUTMEN</div>
          <div class="font-weight-bold font-size-20">{{ count($total_rekrutmen) }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-ios-box"></i>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="box bg-warning darken">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">TIDAK LOLOS</div>
          <div class="font-weight-bold font-size-20">{{ count($total_rejected) }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-ios-box"></i>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="box bg-info darken">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">DITERIMA / LOLOS</div>
          <div class="font-weight-bold font-size-20">{{ count($total_diterima) }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-ios-people"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="page-header">
        <div class="table-success">
          <table class="table table-bordered table-striped table-small-font" style="font-size: 0.9em" id="table_kota" style="width: 100%;">
            <thead>
              <tr>
                <th>No</th>
                <th>Kota</th>
                <th class="no-sort">Jumlah</th>
              </tr>
            </thead>
            <tbody class="middle-align">
              @php
                $total = 0;
              @endphp
              @foreach ($get_pelamar as $num => $p)
                <tr>
                  <td>{{ ++$num }}</td>
                  <td>{{ $p->nama }}</td>
                  <td><a href="/hr/list/pelamar/perC/{{ $p->id_kota }}" target="_blank" class="badge badge-info">{{ $p->jumlah }}</a></td>
                </tr>
                @php
                  $total = $total + $p->jumlah;
                @endphp
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <td colspan="2">TOTAL</td>
                <td><a href="/hr/list/pelamar/perC/all" target="_blank" class="badge badge-info">{{ $total }}</a></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="page-header">
        <div class="table-warning">
          <table class="table table-bordered table-striped table-small-font" style="font-size: 0.9em" id="table_ditolak" style="width: 100%;">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
              </tr>
            </thead>
            <tbody class="middle-align" id="pelamar_perkota">
              @foreach ($total_rejected as $num => $r)
                <tr>
                  <td>{{ ++$num }}</td>
                  <td>{{ $r->nama }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function() {
      $('#jk, #tahun, #umur, #provinsi').val('').trigger('change');

      var data_sub = {!! json_encode($data_sub) !!};

      $('#jk').select2({
        placeholder: 'Jenis Kelamin'
      });

      $('#tahun').select2({
        placeholder: 'Tahun'
      });

      $('#umur').select2({
        placeholder: 'Umur'
      });

      $('#provinsi').select2({
        placeholder: 'Provinsi'
      });

      $('#kota').select2({
        placeholder: "Kota",
        data: [{'id': 'all', 'text': 'Semua'}]
      });

      $('#kecamatan').select2({
        placeholder: "Kecamatan",
        data: [{'id': 'all', 'text': 'Semua'}]
      });

      $('#kelurahan').select2({
        placeholder: "Kelurahan",
        data: [{'id': 'all', 'text': 'Semua'}]
      });

      function loadProv(provinsiValue) {
        return new Promise(function(resolve, reject) {
          $.ajax({
            type: "GET",
            url: '/rec/get_region',
            dataType: 'JSON',
            data: {
              data: provinsiValue,
              jenis: 'kota'
            }
          }).done( function(e){
            $('#kota, #kecamatan, #kelurahan').empty();

            if($('#kota').hasClass('select2-hidden-accessible') ){
              $('#kota').select2('destroy');
            }

            e.unshift({
              id: 'all',
              text: 'Semua'
            });

            $('#kota').select2({
              placeholder: "Kota",
              data: e
            });

            if(typeof data_sub.kota != 'undefined' && data_sub.kota != 'all'){
              $('#kota').val(data_sub.kota).trigger('change').trigger('select2:select');
            }else{
              $('#kota').val(null).change();
            }

            $('#kecamatan').select2({
              placeholder: "Kecamatan",
              data: [{'id': 'all', 'text': 'Semua'}]
            });

            $('#kecamatan').val(null).change();

            $('#kelurahan').select2({
              placeholder: "Kelurahan",
              data: [{'id': 'all', 'text': 'Semua'}]
            });

            $('#kelurahan').val(null).change();

            resolve();
          }).fail(function() {
            reject();
          })
        });
      }

      function KotaProv(kotaValue) {
        return new Promise(function(resolve, reject) {
          $.ajax({
            type: "GET",
            url: '/rec/get_region',
            dataType: 'JSON',
            data: {
              data: kotaValue,
              jenis: 'kecamatan'
            }
          }).done( function(e){
            if($('#kecamatan').hasClass('select2-hidden-accessible') ) {
              $('#kecamatan').select2('destroy');
            }

            e.unshift({
              id: 'all',
              text: 'Semua'
            });

            $('#kecamatan').select2({
              placeholder: "Kecamatan",
              data: e
            });

            if(typeof data_sub.kecamatan != 'undefined' && data_sub.kecamatan != 'all'){
              $('#kecamatan').val(data_sub.kecamatan).trigger('change').trigger('select2:select');
            }else{
              $('#kecamatan').val(null).change();
            }

            $('#kecamatan').val(null).change();

            $('#kelurahan').select2({
              placeholder: "Kelurahan",
              data: [{'id': 'all', 'text': 'Semua'}]
            });

            $('#kelurahan').val(null).change();

            resolve();
          }).fail(function() {
            reject();
          })
        });
      }

      function KecamatanProv(KecamatanValue) {
        return new Promise(function(resolve, reject) {
          $.ajax({
            type: "GET",
            url: '/rec/get_region',
            dataType: 'JSON',
            data: {
              data: KecamatanValue,
              jenis: 'kelurahan'
            }
          }).done( function(e){
            $('#kelurahan').empty();

            if($('#kelurahan').hasClass('select2-hidden-accessible') ) {
              $('#kelurahan').select2('destroy');
            }

            e.unshift({
              id: 'all',
              text: 'Semua'
            });

            $('#kelurahan').select2({
              placeholder: "Kelurahan",
              data: e
            });

            if(typeof data_sub.kelurahan != 'undefined' && data_sub.kelurahan != 'all'){
              $('#kelurahan').val(data_sub.kelurahan).trigger('change').trigger('select2:select');
            }else{
              $('#kelurahan').val(null).change();
            }

            $('#kelurahan').val(null).change();

            resolve();
          }).fail(function() {
            reject();
          })
        });
      }

      $('#provinsi').on('select2:select', async function(e) {
        var provinsiValue = $(this).val();

        try {
          await loadProv(provinsiValue);
        } catch (error) {
          console.error('Error loading Provinsi data:', error);
        }
      });

      $('#kota').on('select2:select', async function(e) {
        var kotaValue = $(this).val();

        try {
          await KotaProv(kotaValue);
        } catch (error) {
          console.error('Error loading Kota data:', error);
        }
      });

      $('#kecamatan').on('select2:select', async function(e) {
        var KecamatanValue = $(this).val();

        try {
          await KecamatanProv(KecamatanValue);
        } catch (error) {
          console.error('Error loading Kecamatan data:', error);
        }
      });

      if($.isEmptyObject(data_sub) ){
        $("#jk").val(['Laki-Laki', 'Perempuan']).change();

        let date = new Date();
        $("#tahun").val(date.getFullYear() ).change()

        $("#umur").val(['18_25', '25_']).change();
      }else{
        if(data_sub.jk.length != 0){
          $("#jk").val(data_sub.jk).change();
        }

        if(data_sub.umur.length != 0){
          $("#umur").val(data_sub.umur).change();
        }

        let date = new Date();
        $("#tahun").val(data_sub.tahun ?? date.getFullYear() ).change();

        if(data_sub.provinsi != 'all'){
          $('#provinsi').val(data_sub.provinsi).trigger('change').trigger('select2:select');
        }
      }

      $("#table_kota").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
        }],
        "lengthChange": false
			});

			$('#table_kota_wrapper .table-caption').text('Total Pelamar Per Kota');
			$('#table_kota_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

      $("#table_ditolak").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
        }],
        "lengthChange": false
			});

			$('#table_ditolak_wrapper .table-caption').text('Daftar Pelamar Yang Ditolak');
			$('#table_ditolak_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

      var randomScalingFactor = function() {
        return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
      },
      bulan = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ", 0.5)";
      },
      chartColors = {
        red: 'rgb(255, 99, 132, 0.5)',
        orange: 'rgb(255, 159, 64, 0.5)',
        yellow: 'rgb(255, 205, 86, 0.5)',
        green: 'rgb(75, 192, 192, 0.5)',
        blue: 'rgb(54, 162, 235, 0.5)',
        purple: 'rgb(153, 102, 255, 0.5)',
        grey: 'rgb(231,233,237, 0.5)'
      };

      $(".modal").on('shown.bs.modal', function(){
				$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
			});

      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    	} );

      var chart_rec = {!! json_encode($total_rekrutmen_per_mit) !!},
      label = [],
      isi = [],
      color = [];

      $.each(chart_rec, function(k, v){
        label.push(v.mitra)
      })

      $.each(chart_rec, function(k, v){
        isi.push(v.jml)
        color.push(dynamicColors() )
      })

      Chart.register(ChartDataLabels);

			var data = {!! json_encode($get_data_tot ?? '') !!},
      bulanku = [],
      total_kar = [];

      $.each(data, function (key, value){
        //pelamar
        total_kar.push(value.data);
        bulanku.push(bulan[value.tgl - 1]);
      });

			var ctx_line = $('#chart_line_kar'),
      totalDuration = 3000
      delayBetweenPoints = totalDuration / data.length,
      previousY = (ctx) => ctx.index === 0 ? ctx.chart.scales.y.getPixelForValue(100) : ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index - 1].getProps(['y'], true).y,
      animation = {
        x: {
          type: 'number',
          easing: 'linear',
          duration: delayBetweenPoints,
          from: NaN, // the point is initially skipped
          delay(ctx) {
            if (ctx.type !== 'data' || ctx.xStarted) {
              return 0;
            }
            ctx.xStarted = true;
            return ctx.index * delayBetweenPoints;
          }
        },
        y: {
          type: 'number',
          easing: 'linear',
          duration: delayBetweenPoints,
          from: previousY,
          delay(ctx) {
            if (ctx.type !== 'data' || ctx.yStarted) {
              return 0;
            }
            ctx.yStarted = true;
            return ctx.index * delayBetweenPoints;
          }
        }
      },
      lineGraph = {},
      config = {
        type: 'line',
        data: {
          labels: bulanku,
          datasets: [{
            label: "Karyawan",
            backgroundColor: chartColors.blue,
            borderColor: chartColors.blue,
            fill: false,
            data: total_kar,
						datalabels: {
							align: 'end',
							anchor: 'end'
						}
          }]
        },
        options: {
          responsive: true,
          animation,
          maintainAspectRatio: false,
          plugins: {
						title: {
							display: true,
							text: 'Total Karyawan Tahun ' + $('#tahun').val()
						},
						datalabels: {
              backgroundColor: function(context) {
                return context.active ? context.dataset.backgroundColor : 'white';
              },
              borderColor: function(context) {
                return context.dataset.backgroundColor;
              },
              borderRadius: function(context) {
                return context.active ? 0 : 32;
              },
              borderWidth: 3,
              color: function(context) {
                return context.active ? 'white' : context.dataset.backgroundColor;
              },
              font: {
                weight: 'bold'
              },
              formatter: function(value, context) {
                value = Math.round(value * 100) / 100;
                return context.active
                  ? context.dataset.label + '\n' + value + ' Orang'
                  : Math.round(value);
              },
              offset: 8,
              padding: 5,
              textAlign: 'center'
            }
					},
          tooltips: {
            mode: 'label',
          },
          hover: {
            mode: 'index',
            intersect: false
          },

        }
      };

      lineGraph = new Chart(ctx_line, config);

      var chart_accepted = {!! json_encode($total_diterima) !!},
      label = [],
      isi = [],
      color = [];

      $.each(chart_accepted, function(k, v){
        label.push(v.mitra)
        isi.push(v.jml)
        color.push(dynamicColors() )
      })

      var pelamar = [],
      rekrut = [],
      rejected = [],
      accept = [],
      month_pelamar_a = [],
      month_rekrut = [],
      month_rejected = [],
      month_accept = [],
      data_new = {!! json_encode($pelamar) !!},
      data_rekrut = {!! json_encode($rekrut) !!},
      data_rejected = {!! json_encode($rejected) !!},
      data_accept = {!! json_encode($accept ?? '') !!},
      data_tot = {!! json_encode($get_data_tot_lamar) !!};

      $.each(data_new, function (key, value){
        //pelamar
        pelamar.push(value.data);
        month_pelamar_a.push(value.tgl - 1);
      });

      $.each(data_rekrut, function (key, value){
        //rekrut
        rekrut.push(value.data);
        month_rekrut.push(value.tgl - 1);
      });

      $.each(data_rejected, function (key, value){
        //rejected
        rejected.push(value.data);
        month_rejected.push(value.tgl - 1);
      });

      $.each(data_accept, function (key, value){
        //diterima
        accept.push(value.data);
        month_accept.push(value.tgl - 1);
      });

      var month_hs = [...new Set([...month_pelamar_a, ...month_rekrut, ...month_rejected, ...month_accept])],
      bulanku = [],
      raw_bulanku = [];

      month_hs.sort(function(a, b){
        return parseInt(a)- parseInt(b);
      });

      $.each(month_hs, function (key, valu){
        bulanku.push(bulan[valu]);
        raw_bulanku.push(valu);
      });

      var data_bar = []

      if(pelamar.length != 0){
        data_bar.push({
          label: "Pelamar",
          jenis: "pelamar",
          backgroundColor: chartColors.blue,
          borderColor: chartColors.blue,
          data: pelamar,
          fill: false,
        })
      }

      if(rekrut.length != 0){
        data_bar.push({
          label: "Rekrutmen",
          jenis: "rekrut",
          backgroundColor: chartColors.grey,
          borderColor: chartColors.grey,
          data: rekrut,
          fill: false,
        })
      }

      if(rejected.length != 0){
        data_bar.push({
          label: "Tidak Lolos",
          jenis: "rejected",
          backgroundColor: chartColors.yellow,
          borderColor: chartColors.yellow,
          data: rejected,
          fill: false,
        })
      }

      if(accept.length != 0){
        data_bar.push({
          label: "Di Terima / Lolos",
          jenis: "accept",
          backgroundColor: chartColors.green,
          borderColor: chartColors.green,
          data: accept,
          fill: false,
          datalabels: {
            align: 'center',
            anchor: 'center'
          }
        })
      }

      var ctx_bar = $('#recruitment_complete'),
      delayed,
      barGraph = {},
      config = {
        type: 'bar',
        data: {
          labels: bulanku,
          raw_bln: raw_bulanku,
          datasets: data_bar
        },
        options: {
          animation: {
            onComplete: () => {
              delayed = true;
            },
            delay: (context) => {
              let delay = 0;
              if (context.type === 'data' && context.mode === 'default' && !delayed) {
                delay = context.dataIndex * 300 + context.datasetIndex * 100;
              }
              return delay;
            }
          },
          responsive: true,
          maintainAspectRatio: false,
          plugins: {
						title: {
							display: true,
							text: 'Grafik Data Tahun ' + $('#tahun').val()
						},
            datalabels: {
              color: 'black',
              display: function(context) {
                return context.dataset.data[context.dataIndex];
              },
              font: {
                weight: 'bold'
              },
              formatter: Math.round
            },
					},
          tooltips: {
            mode: 'label',
          },
          interaction: {
            intersect: false,
            mode: 'nearest',
          },
          onHover: (event, chartElement) => {
            event.native.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
          },
          onClick: (evt) => {
            var activePoint = evt.chart.getElementsAtEventForMode(evt, 'nearest', {}, true)[0];
            if(typeof activePoint === 'undefined'){
              console.log('tidak ada')
              return false;
            }
            var label = evt.chart.data.labels[activePoint.index];
            var buln_num = evt.chart.data.raw_bln[activePoint.index] + 1;
            var value = evt.chart.data.datasets[activePoint.datasetIndex].data[activePoint.index];
            var jenis = evt.chart.data.datasets[activePoint.datasetIndex].jenis;
            // console.log(jenis)
            if(activePoint.datasetIndex == 0){
              $('.modal_pelamar').modal('toggle');
              $('.modal-title_pelamar').text('Detail Pelamar Bulan '+ label);
              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: buln_num,
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.month_tab').html(label+"<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.lvl_pendidikan,
                    val.nama_kota,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=pelamar' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#pelamar_month').DataTable().destroy();

                $('#pelamar_month').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#pelamar_month_wrapper .table-caption').text('Daftar Pelamar Bulan '+label);
                $('#pelamar_month_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });

              var d = new Date();
              var m = d.getMonth() + 1;
              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: 'today',
                  tahun: $("#tahun").val(),
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.rec_now').html("<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.lvl_pendidikan,
                    val.nama_kota,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=pelamar' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#pelamar_now').DataTable().destroy();

                $('#pelamar_now').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#pelamar_now_wrapper .table-caption').text('Daftar Pelamar (Hari Ini)');
                $('#pelamar_now_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });

              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: 'all',
                  tahun: $("#tahun").val(),
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.rec_all').html("<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.lvl_pendidikan,
                    val.nama_kota,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=pelamar' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#pelamar_all').DataTable().destroy();

                $('#pelamar_all').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#pelamar_all_wrapper .table-caption').text('Daftar Pelamar (Tahun Ini)');
                $('#pelamar_all_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });

            }
            if(activePoint.datasetIndex == 1){
              $('.modal_rekrut').modal('toggle');

              $('.modal-title').text('Detail Recruit Bulan '+ label);

              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: buln_num,
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.recruit_month').html(label+"<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=recruit' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#rekrut_month').DataTable().destroy();

                $('#rekrut_month').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#rekrut_month_wrapper .table-caption').text('Daftar Recruitment Bulan '+label);
                $('#rekrut_month_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });

              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: 'today',
                  tahun: $("#tahun").val(),
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.recruit_now').html("<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=recruit' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#rekrut_now').DataTable().destroy();

                $('#rekrut_now').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#rekrut_now_wrapper .table-caption').text('Daftar Recruitment (Hari Ini)');
                $('#rekrut_now_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });

              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: 'all',
                  tahun: $("#tahun").val(),
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.recruit_all').html("<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=recruit' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#rekrut_all').DataTable().destroy();

                $('#rekrut_all').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#rekrut_all_wrapper .table-caption').text('Daftar Recruitment (Tahun Ini)');
                $('#rekrut_all_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });
            }
            if(activePoint.datasetIndex == 2){
              $('.modal_rejected').modal('toggle');

              $('.modal-title').text('Detail Tidak Lolos Bulan '+ label);

              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: buln_num,
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.rejected_month').html(label+"<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=rejected' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#rejected_month').DataTable().destroy();

                $('#rejected_month').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#rejected_month_wrapper .table-caption').text('Daftar Tidak Lolos Bulan '+label);
                $('#rejected_month_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });

              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: 'today',
                  tahun: $("#tahun").val(),
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.rejected_now').html("<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=rejected' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#rejected_now').DataTable().destroy();

                $('#rejected_now').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#rejected_now_wrapper .table-caption').text('Daftar Tidak Lolos (Hari Ini)');
                $('#rejected_now_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });

              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: 'all',
                  tahun: $("#tahun").val(),
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.rejected_all').html("<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=rejected' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#rejected_all').DataTable().destroy();

                $('#rejected_all').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#rejected_all_wrapper .table-caption').text('Daftar Recruitment (Tahun Ini)');
                $('#rejected_all_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });
            }
            if(activePoint.datasetIndex == 3){
              $('.modal_accept').modal('toggle');

              $('.modal-title').text('Detail Di Terima Bulan '+ label);

              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: buln_num,
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.accept_month').html(label+"<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=accept' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#accept_month').DataTable().destroy();

                $('#accept_month').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#accept_month_wrapper .table-caption').text('Daftar Di Terima / Lolos Bulan '+label);
                $('#accept_month_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });

              var d = new Date();
              var m = d.getMonth() + 1;
              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: 'today',
                  tahun: $("#tahun").val(),
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.accept_now').html("<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=accept' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#accept_now').DataTable().destroy();

                $('#accept_now').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#accept_now_wrapper .table-caption').text('Daftar Di Terima / Lolos (Hari Ini)');
                $('#accept_now_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });

              $.ajax({
                url: '/jx_dng/get_table_beranda',
                data: {
                  data: jenis,
                  bulan: 'all',
                  tahun: $("#tahun").val(),
                  jk : $('#jk').val(),
                  tahun : $('#tahun').val(),
                  umur : $('#umur').val(),
                  provinsi : $('#provinsi').val(),
                  kota : $('#kota').val(),
                  kecamatan : $('#kecamatan').val(),
                  kelurahan : $('#kelurahan').val()
                },
                dataType: 'JSON'
              }).done( function(e){
                var kolom = [],
                final_data = {};
                $('.accept_all').html("<span class='label label-primary'>"+e.length+"</span>");
                $.each(e, function(key, val){
                  kolom.push([
                    ++key,
                    val.nama,
                    val.noktp,
                    val.created_at,
                    "<a target='_blank' href='/profile/view?nik="+val.noktp+"&grafik=accept' class='btn btn-info'>View</a>"
                  ]);
                });

                final_dta= {
                  BO: kolom,
                };

                $('#accept_all').DataTable().destroy();

                $('#accept_all').DataTable({
                  drawCallback: function () {
                    $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
                  },
                  fixedHeader: {
                    header: true,
                    footer: true
                  },
                  order:	[
                    [0, "DESC" ]
                  ],
                  data: final_dta.BO,
                  deferRender: true,
                  scrollCollapse: true,
                  scroller: true,
                }).columns.adjust().draw();

                $('#accept_all_wrapper .table-caption').text('Daftar Di Terima / Lolos (Tahun Ini)');
                $('#accept_all_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
              });
            }
          },
          scales: {
            x: {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Month'
              }
            },
            y: {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Value'
              }
            }
          }
        }
      };

      barGraph = new Chart(ctx_bar, config);
		});
  </script>
@endsection
