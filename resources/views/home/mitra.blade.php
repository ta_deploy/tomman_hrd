@extends('layout')
@section('title')
Beranda
@endsection
@section('css')
<style>
  th {
    text-align: center;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
  .upper {
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
  <div class="col-md-12">
    <div class="page-header">
      <div class="table-info">
        <table class="table table-bordered table-striped table-small-font" id="recruitment">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Kartu Keluarga</th>
              <th>Tanggal Melamar</th>
              <th class="no-sort">Detail</th>
            </tr>
          </thead>
          <tbody class="middle-align">
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="page-header">
      <div class="table-success">
        <table class="table table-bordered table-striped table-small-font" id="table_siap_train_fa">
          <thead>
            <tr>
              <th>No</th>
              <th>Nik</th>
              <th>Mitra</th>
              <th>Kartu Keluarga</th>
              <th class="no-sort">Detail</th>
            </tr>
          </thead>
          <tbody class="middle-align">
            <tr>
              <td>1</td>
              <td>Rendy</td>
              <td>18940469</td>
              <td>18940469</td>
              <td>tes</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="page-header">
      <div class="table-warning">
        <table class="table table-bordered table-striped table-small-font" id="table_not_mytech">
          <thead>
            <tr>
              <th>No</th>
              <th>Nik</th>
              <th>Mitra</th>
              <th>Kartu Keluarga</th>
              <th class="no-sort">Detail</th>
            </tr>
          </thead>
          <tbody class="middle-align">
            <tr>
              <td>1</td>
              <td>Rendy</td>
              <td>18940469</td>
              <td>18940469</td>
              <td>tes</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <canvas style="height: 300px" id="chart_line_kar"></canvas>
  </div>
</div>
@endsection

@section('js')
	<script>
		$( function() {

      $(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});
			
			$('#recruitment_wrapper .table-caption').text('List Karyawan Recruitment');
			$('#recruitment_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
			
			$('#table_not_mytech_wrapper .table-caption').text('List Tidak Punya My-tech');
			$('#table_not_mytech_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
      
			$('#table_siap_train_fa_wrapper .table-caption').text('List Siap Training FA');
			$('#table_siap_train_fa_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
    });
  </script>
@endsection