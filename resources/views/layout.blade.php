<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <link rel="icon" href="/image/icon.png" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=0.6, user-scalable=no, minimum-scale=1.0, maximum-scale=0.7">
  <meta name="description" content="Mari Bergabung Bersama Kami Putra Putri Terbaik di Banua Kalimantan untuk Berkesempatan Berkarir pada PT Telkom Akses dan Mitra - Mitra Kami di Seluruh Area Kalimantan | PERWIRA">
  <meta name="keywords" content="personalia warior akses,perwira,perwira tomman,tomman,perwira telkomakses,perwira telkom akses,warior akses,info loker telkom,info loker telkomakses,info loker kalimantan,loker,indihome">
  <meta name="author" content="Personalia Warior Akses - PERWIRA">
  <meta name="google-site-verification" content="JtYYY_8Z43BZrb7vjCMsOyo8QM2SfnqJNWldfFhhk1k" />

  <title>@yield('title') | PERWIRA</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <?php
      $auth = session('auth') ?: '';
      $path = Request::path();
      $theme = 'mint-dark';

      if(isset($auth->pixeltheme) )
      {
        if($auth->pixeltheme){
          $theme = $auth->pixeltheme ?? 'light';
        }
      }

      $color = '';

      if (str_contains($theme, 'dark') )
      {
        $color = '-dark';
      }
    ?>
    <link href="/css/bootstrap{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/pixeladmin{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/widgets{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/themes/{{ $theme }}.min.css" rel="stylesheet" type="text/css">

  <!-- Pace.js -->
  <script src="/pace/pace.min.js"></script>
  @yield('css')
</head>
<body>
  <!-- Nav -->
  <nav class="px-nav px-nav-left">
    <button type="button" class="px-nav-toggle" data-toggle="px-nav">
      <span class="px-nav-toggle-arrow"></span>
      <span class="navbar-toggle-icon"></span>
      <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
    </button>
    @if(@!is_null(session('auth')->perwira_level) )
      <ul class="px-nav-content">
        @if(session('auth')->perwira_level != 33)
          @if (in_array(@session('auth')->perwira_level, [1, 69]) )
            <li class="px-nav-item px-nav-dropdown">
              <a href="#"><i class="px-nav-icon ion-android-contacts"></i><span class="px-nav-label">Human Resources Management</span></a>
              <ul class="px-nav-dropdown-menu">
                <!-- <li class="px-nav-item"><a href="/hr/list/nik"><span class="px-nav-label">List Karyawan</span></a></li> -->
                <li class="px-nav-item"><a href="/event_list"><span class="px-nav-label">Event Absen</span></a></li>
                <li class="px-nav-item"><a href="/hr/rec_init"><span class="px-nav-label">Data Pelamar dan Rekrutmen</span></a></li>
                <li class="px-nav-item"><a href="/hr/monolog?witel=WITEL BANJARMASIN"><span class="px-nav-label">Data Monolog</span></a></li>
                <li class="px-nav-item"><a href="/hr/teknisi?witel=WITEL BANJARMASIN"><span class="px-nav-label">Data Teknisi</span></a></li>
                <li class="px-nav-item"><a href="/hr/teknisi_tomman?witel=WITEL BANJARMASIN"><span class="px-nav-label">Data Tomman</span></a></li>
                <li class="px-nav-item"><a href="/hr/list/position"><span class="px-nav-label">Posisi</span></a></li>
                <li class="px-nav-item"><a href="/hr/list/direktorat"><span class="px-nav-label">Direktorat</span></a></li>
                <li class="px-nav-item"><a href="/hr/list/mutasi"><span class="px-nav-label">Mutasi</span></a></li>
                <li class="px-nav-item"><a href="/hr/list/ttd"><span class="px-nav-label">Tanda Tangan</span></a></li>
                <li class="px-nav-item"><a href="/hr/list/archivement"><span class="px-nav-label">Penghargaan</span></a></li>
                <li class="px-nav-item"><a href="/hr/calendar/cuti"><span class="px-nav-label">Kalender Cuti</span></a></li>
              </ul>
            </li>
          @endif
          @if (in_array(@session('auth')->perwira_level, [100]) )
            <li class="px-nav-item">
              <a href="/absent_tomman"><i class="px-nav-icon ion-ios-alarm-outline"></i><span class="px-nav-label">Absen</span></a>
            </li>
          @endif
          @if (in_array(@session('auth')->perwira_level, [1, 12]) )
            <li class="px-nav-item px-nav-dropdown">
              <a href="#"><i class="px-nav-icon ion-ribbon-a"></i><span class="px-nav-label">Fiber Academy</span></a>
              <ul class="px-nav-dropdown-menu">
                <li class="px-nav-item"><a href="/FA/list/breving"><span class="px-nav-label">Brevert / Training</span></a></li>
              </ul>
            </li>
          @endif
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-hammer"></i><span class="px-nav-label">WareHouse</span></a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item"><a href="/report/alker"><span class="px-nav-label">Alat Kerja</span></a></li>
            </ul>
          </li>
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-nuclear"></i><span class="px-nav-label">HSE</span></a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item"><a href="/check_data"><span class="px-nav-label">Check Tiket<span class="badge badge-info">{{ $countMenu->check_first ?? 0 }}</span></span></a></li>
              <li class="px-nav-item"><a href="/wadah_potensi"><span class="px-nav-label">Pengaduan<span class="badge badge-primary">{{ $countMenu->pengaduan ?? 0 }}</span></span></a></li>
              <li class="px-nav-item"><a href="/potensi_finished"><span class="px-nav-label"></span>Sudah Dikerjakan<span class="badge badge-success">{{ $countMenu->sdh_dikerjakan ?? 0 }}</span></a></li>
              <li class="px-nav-item"><a href="/get_potensi/approve"><span class="px-nav-label">Finish</span></a></li>
              <li class="px-nav-item"><a href="/get_potensi/reject"><span class="px-nav-label">Reject<span class="badge badge-danger">{{ $countMenu->reject ?? 0 }}</span></span></a></li>
              <li class="px-nav-item"><a href="/map_rawan_bahaya"><span class="px-nav-label">Map</span></a></li>
            </ul>
          </li>
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-ios-list-outline"></i><span class="px-nav-label">Report</span></a>
            <ul class="px-nav-dropdown-menu">
              {{-- hr --}}
              <li class="px-nav-item"><a href="/performance"><span class="px-nav-label">Performansi</span></a></li>
              <li class="px-nav-item"><a href="/report/current_brevert"><span class="px-nav-label">Brevert Yang Berlangsung</span></a></li>
              <li class="px-nav-item"><a href="/report/monitor_brevert"><span class="px-nav-label">Monitor Brevert</span></a></li>
              <li class="px-nav-item"><a href="/report/absensi"><span class="px-nav-label">Absensi</span></a></li>
              <li class="px-nav-item"><a href="/report/briefingList/{{ date('Y-m-d') }}"><span class="px-nav-label">Briefing</span></a></li>
              <li class="px-nav-item"><a href="/report/resling"><span class="px-nav-label">Resign / Konseling</span></a></li>
              <li class="px-nav-item"><a href="/report/accident"><span class="px-nav-label">Kecelakaan Kerja</span></a></li>
              <li class="px-nav-item"><a href="/report/cuti"><span class="px-nav-label">Cuti Kerja</span></a></li>
            </ul>
          </li>
          @if (in_array(@session('auth')->perwira_level, [1]) )
            <li class="px-nav-item px-nav-dropdown">
              <a href="#"><i class="px-nav-icon ion-android-settings"></i><span class="px-nav-label">Master</span></a>
              <ul class="px-nav-dropdown-menu">
                <li class="px-nav-item"><a href="/hr/list/created_user"><span class="px-nav-label">User</span></a></li>
                <li class="px-nav-item"><a href="/hr/list/sto"><span class="px-nav-label">Sto</span></a></li>
                <li class="px-nav-item"><a href="/hr/list/mitra"><span class="px-nav-label">Mitra</span></a></li>
                <li class="px-nav-item"><a href="/hr/list/level"><span class="px-nav-label">Level</span></a></li>
                <li class="px-nav-item"><a href="/hr/list/witel"><span class="px-nav-label">Witel</span></a></li>
              </ul>
            </li>
          @endif
          {{-- <li class="px-nav-item">
            <a href="/organisasi"><i class="px-nav-icon ion-network"></i><span class="px-nav-label">Organisasi</span></a>
          </li> --}}
        @endif
        {{-- <li class="px-nav-item">
          <a href="/event_doorprize"><i class="px-nav-icon ion-network"></i><span class="px-nav-label">Event</span></a>
        </li> --}}
      </ul>
    @endif
  </nav>

  <!-- Navbar -->
  <nav class="navbar px-navbar">
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><b>P E R</b> <b style="color: red !important">W I R A</b></a>
    </div>
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

    <div class="collapse navbar-collapse" id="px-navbar-collapse">
    <ul class="nav navbar-nav navbar-right">
        <li>
          <form class="navbar-form">
            <div class="form-group">
              <span class="timestampt"></span>
            </div>
          </form>
        </li>
        @if(@session('auth')->perwira_level)
          <li>
            <form class="navbar-form" role="search" id="search_me" method="GET" action="/search/result">
              <div class="form-group">
                {{-- {{ csrf_field() }} --}}
                <input type="text" class="form-control" name='data_search' id="data_search" placeholder="Masukkan Nomor KTP/NIK/Nama untuk Mencari Karyawan" style="width: 400px;" autocomplete="off">
              </div>
            </form>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              {{-- <img src="https://apps.telkomakses.co.id/wimata/photo/crop_{{ session('auth')->id_nik }}.jpg" alt="" class="px-navbar-image"> --}}
              <span class="hidden-md">{{ @session('auth')->nama }}</span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="/profile"><i class="fa fa-user"></i>&nbsp;&nbsp;Profile</a></li>
              <li><a href="/theme"><i class="px-nav-icon ion-ios-pie"></i>Theme</a></li>
              <li class="divider"></li>
              <li><a href="/logout"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
            </ul>
          </li>
        @endif
      </ul>
    </div>
  </nav>

   <!-- Content -->
   @yield('content')

   <!-- Footer -->
   <footer class="px-footer px-footer-bottom">
    <div style="text-align: center">
      PERSONALIA WARIOR AKSES © {{ date('Y') }}
    </div>
  </footer>

  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- Load jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Core scripts -->
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>

  <!-- Your scripts -->
  <script src="/js/app.js"></script>
  @yield('js')
  <script>
  function time() {
      var d = new Date(),
      s = d.getSeconds(),
      m = d.getMinutes(),
      h = d.getHours(),
      today = new Date(),
      dd = String(today.getDate() ).padStart(2, '0'),
      hh = String(h).padStart(2, '0'),
      months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      digit_month = months[today.getMonth()],
      yyyy = today.getFullYear(),
      today = dd + ' ' + digit_month + ' ' + yyyy;
      $('.timestampt').html(today + " " + hh + ":" + m + ":" + s);
    }

    $(function () {
      var file = String(document.location).split('/');
      file.splice(0, 3);
      // file = file.replace(/(\.html).*/i, '$1');

      // if (!/.html$/i.test(file) ) {
      //   file = 'index.html';
      // }

      if($('body > .px-nav').find('.px-nav-item > a[href="/' + file.join('/') + '"]').parent().parent().parent().hasClass('px-nav-dropdown') ){
        $('body > .px-nav').find('.px-nav-item > a[href="/' + file.join('/') + '"]').parent().parent().parent().toggleClass('px-open')
      }

      // Activate current nav item
      $('body > .px-nav')
        .find('.px-nav-item > a[href="/' + file.join('/') + '"]')
        .parent()
        .addClass('active');

      $('body > .px-nav').pxNav();
      $('body > .px-footer').pxFooter();

      $('#navbar-notifications').perfectScrollbar();
      $('#navbar-messages').perfectScrollbar();

      setInterval(time, 1000);

      $('#search_me').submit( function(e){

        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": true,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "timeOut": "6000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        };

        var filter = /^[0-9-+]+$/;
        if (filter.test($('#data_search').val() ) ) {
          if($('#data_search').val().length <= 5){
            toastr.warning('Minimal Nik Harus 5 Karakter!', "warning");
            return false;
          }
          if($('#data_search').val().length > 8 && $('#data_search').val().length <= 10){
            toastr.warning('Minimal KTP Harus 10 Karakter!', "warning");
            return false;
          }
        }
        else {
          if($('#data_search').val().length == 0){
            toastr.warning('Masukkan Nik/KTP/Nama Naker!', "danger");
            return false;
          }
          if($('#data_search').val().length <= 4){
            toastr.warning('Minimal Nama Harus 4 Karakter!', "warning");
            return false;
          }
        }
      });

    });
  </script>
</body>
</html>
