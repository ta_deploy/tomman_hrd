@extends('layout')
@section('css')
<style>
	.input-photos img {
		width: 100px;
		height: 150px;
		margin-bottom: 5px;
	}
</style>
@endsection
@section('title')
Form Organisasi
@endsection
@section('content')
<div class="px-content">
	<div class="partial_alert"></div>
  <div class="page-header">
		<form method="post" class="form-horizontal validate" enctype="multipart/form-data">
			<div class="col-md-12">
			<div class="panel panel-color panel-info panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Organisasi</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="nik_jabatan">Nik Jabatan :</label>
						<div class="col-md-9">
							<select name="nik_jabatan" class="form-control choose_nik">
							</select>
							<code>*Tidak Usah Diisi Jika Tidak Ada</code>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="nik_user">Nik User :</label>
						<div class="col-md-9">
							<select name="nik_user" class="form-control choose_nik">
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="text-center input-photos">
							<?php
								$path = "/upload4/perwira/organisasi/".Request::segment(2)."/profile_".Request::segment(2);
								$th = '';
								$img = '';
								$src="/image/placeholder.gif";
								$flag = '';

								if(file_exists(public_path().$path) )
								{
									$th   = "$path-th.jpg";
									$img  = "$path.jpg";
									$src  = $th;
									$flag = 1;
								}
							?>
							<a href="{{ $img }}">
								<img src="{{ $src }}" alt="profile" id="img-profile" class="photo_valid_dis" style="margin-bottom: 7px;"/>
							</a>
							<br />
							<input type="text" class="hidden" name="flag_profile" value="{{ $flag }}"/>
							<input type="file" class="hidden photo_profile" name="photo-profile" accept="image/jpeg" />
							<button type="button" class="btn btn-sm btn_up btn-info">
								<i class="ion ion-camera"></i>
							</button>
							<p style="font-size: 9px;">Photo Profile</p>
							{!! $errors->first('profile', '<span class="label label-danger">:message</span>') !!}
						</div>
					</div>
					<div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
            </div>
          </div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('.choose_nik').select2({
				width: '100%',
				placeholder: "Ketik Nik Karyawan",
				allowClear: true,
				minimumInputLength:2,
				ajax: {
					url: '/jx_dng/get_user_with_pos',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('.photo_profile').change(function() {
        /*console.log(this.name);*/
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
					/*$(inputEl).parent().find('input[type=text]').val(1);*/
					var reader = new FileReader();
					reader.onload = function(e) {
						$(inputEl).parent().find('img').attr('src', e.target.result);
					}
					reader.readAsDataURL(inputEl.files[0]);
        }
    	});

			$('.input-photos').on('click', 'button', function() {
				$(this).parent().find('input[type=file]').click();
			});

			$("select[name='nik_user']").on('select2:select, change', function(){
				var isi = $(this).val();

				function checkImage(url) {
					var request = new XMLHttpRequest();
					request.open("GET", url, true);
					request.send();
					request.onload = function() {
						status = request.status;
						if(request.status == 200){
							// $('#src_gmbr').attr('src', 'https://apps.telkomakses.co.id/wimata/photo/crop_'+isi+'.jpg')
							$('.btn_up').hide();
						}else{
							$('.btn_up').show();
						}
					}
				}

				// checkImage('https://apps.telkomakses.co.id/wimata/photo/crop_'+isi+'.jpg');
			})

			$('.modal_submit').on('click', function(){
				var fd = new FormData(),
				token = <?= json_encode(csrf_token() ) ?>;
				fd.append( 'new_alker', $('#new_alker').val() );
				fd.append( '_token', token );

				$.ajax({
					url: '/report/tools/addNewAlker',
					data: fd,
					processData: false,
					contentType: false,
					dataType: 'JSON',
					type: 'POST',
					success: function(data){
						$('.partial_alert').html("<div class='alert alert-"+ data.msg.type+ "'>"+ data.msg.text, "</div>");
					}
				});
			});
		});
	</script>
@endsection