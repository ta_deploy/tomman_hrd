@extends('layout')
@section('title')
List Pelamar {{ $data[0]->nama_kota }}
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div class="page-header">
		<div class="table-info">
			<table class="table table-bordered table-striped table-small-font" id="table_ku">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th>Nama</th>
						<th>Jenis Kelamin</th>
						<th class="no-sort">Nomor KTP</th>
						<th>Agama</th>
						<th>Pendidikan Terakhir</th>
						<th>Kota</th>
						<th class="no-sort">Action</th>
					</tr>
				</thead>
				<tbody class="middle-align">
					@foreach ($data as $key => $d )
						<tr>
							<td>{{ ++$key }}</td>
							<td>{{ $d->nama }}</td>
							<td>{{ $d->jk }}</td>
							<td>{{ $d->noktp }}</td>
							<td>{{ $d->agama }}</td>
							<td>{{ $d->lvl_pendidikan }}</td>
							<td>{{ $d->nama_kota }}</td>
							<td><a target="_blank" href="/profile/view?nik={{ $d->noktp }}" class="btn btn-info">View</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});

			var data = {!! json_encode($data[0]) !!};

			$('#table_ku_wrapper .table-caption').text('List Semua Pelamar ' + data.nama_kota);
			$('#table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection