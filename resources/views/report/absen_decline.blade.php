@extends('layout')
@section('title')
Absen Decline
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                DECLINE INFORMATION
            </div>
            <div class="panel-body">
              <form method="post">
                <input type="hidden" name="absen_id" value="{{ $data->absen_id }}" />
              <table class="table">
                <tr>
                  <td>NIK</td>
                  <td>{{ $data->nik }}</td>
                </tr>

                <tr>
                  <td>NAME</td>
                  <td>{{ $data->nama }}</td>
                </tr>

                <tr>
                  <td>TIME</td>
                  <td>{{ $data->date_created }}</td>
                </tr>

                <tr>
                  <td>CATATAN</td>
                  <td>
                    <textarea name="keterangan" rows="8" cols="80"></textarea>
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td><button class="btn btn-danger">DECLINE</button></td>
                </tr>
              </table>
            </form>
            </div>
            </div>
        </div>
    </div>

</div>
@endsection