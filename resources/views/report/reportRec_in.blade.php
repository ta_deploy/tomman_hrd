@extends('layout')
@section('title')
Data Pelamar dan Rekrutmen
@endsection
@section('css')
<style>
  th {
    text-align: center;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    /* text-transform: uppercase; */
  }
  .upper {
    text-transform: uppercase;
  }
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tabs-pelamar" data-toggle="tab">
				Pelamar <span class="badge badge-success">{{ count($pelamar) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-diterima_mitra" data-toggle="tab">
				Diterim Mitra <span class="badge badge-warning">{{ count($diterima_mitra) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-diterima_ta" data-toggle="tab">
				Di Terima TA <span class="badge badge-primary">{{ count($diterima_ta) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-blacklist" data-toggle="tab">
				Blacklist <span class="badge badge-danger">{{ count($blacklist) }}</span>
			</a>
		</li>
	</ul>
	<div class="tab-content tab-content-bordered">
		<div class="tab-pane fade in active" id="tabs-pelamar">
			<div class="page-header">
				<div class="table-success table-responsive">
					<table class="table table-bordered table-striped table-small-font" id="pelamar">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Umur</th>
								<th>Sertifikat Vaksin</th>
								<th>Jenis Kelamin</th>
								<th>Agama</th>
								<th>Golongan Darah</th>
								<th>Tempat Lahir</th>
								<th>Tanggal Lahir</th>
								<th>Email</th>
								<th>NPWP</th>
								<th>Alamat</th>
								<th>Provinsi</th>
								<th>Kabupaten / Kota</th>
								<th>Kecamatan</th>
								<th>Kelurahan</th>
								<th>Lokasi Kerja</th>
								<th>Nomor Handphone</th>
								<th>Nomor WhatsApp</th>
								<th>Nomor KTP</th>
								<th>Nomor Kartu Keluarga</th>
								<th>Status Perkawinan</th>
								<th>Nama Ibu</th>
								<th>Nama Keluarga yang Bisa Dihubungi</th>
								<th>Kontak Keluarga yang Bisa Dihubungi</th>
								<th>Tingat Pendidikan Terakhir</th>
								<th>Nama Institusi Pendidikan Terakhir</th>
								<th>Jurusan</th>
								<th>Tanggal Lulus</th>
								<th>Gadget ( SmartPhone )</th>
								<th>Gadget ( Laptop )</th>
								<th>SIM</th>
								<th>Motor Pribadi</th>
								<th>Relasi</th>
								<th>Tanggal Daftar</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse($pelamar as $key => $val)
							<?php
							$public_path = '/upload4/perwira/recruitment/'.$val->noktp;
							$files = "$public_path/$val->fileName.$val->originalExt";
							$file = '';
							if (@file_exists(public_path().$files) )
							{
								$file = "$public_path/$val->fileName.$val->originalExt";
							}
							?>
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val->nama }}</td>
									<td>{{ $val->umur }}</td>
									<td><a href="{{ $file }}" target="_blank" id="{{ $val->fileName }}">Liat File ({{ $val->originalExt }})</a></td>
									<td>{{ $val->jk }}</td>
									<td>{{ $val->agama }}</td>
									<td>{{ $val->goldar }}</td>
									<td>{{ $val->tempat_lahir }}</td>
									<td>{{ $val->tgl_lahir }}</td>
									<td>{{ $val->email }}</td>
									<td>{{ $val->npwp }}</td>
									<td>{{ $val->alamat }}</td>
									<td>{{ $val->nama_provinsi }}</td>
									<td>{{ $val->nama_kota }}</td>
									<td>{{ $val->nama_kecamatan }}</td>
									<td>{{ $val->nama_kelurahan }}</td>
									<td>{{ $val->lokasi_kerja }}</td>
									<td>{{ $val->no_telp }}</td>
									<td>{{ $val->no_wa }}</td>
									<td>'{{ $val->noktp }}</td>
									<td>'{{ $val->nokk }}</td>
									<td>{{ $val->status_perkawinan }}</td>
									<td>{{ $val->ibu }}</td>
									<td>{{ $val->telpon_2_nm }}</td>
									<td>{{ $val->telpon_2 }}</td>
									<td>{{ $val->lvl_pendidikan }}</td>
									<td>{{ $val->last_study }}</td>
									<td>{{ $val->jurusan }}</td>
									<td>{{ $val->tgllulus }}</td>
									<td>
										@if($val->ket_smartphone == '1')
										Ada
										@else
										Tidak Ada
										@endif
									</td>
									<td>
										@if($val->ket_laptop == '1')
										Ada
										@else
										Tidak Ada
										@endif
									</td>
									<td>
										@if($val->ket_sim == '1')
										Ada
										@else
										Tidak Ada
										@endif
									</td>
									<td>
										@if($val->ket_motor == '1')
										Ada
										@else
										Tidak Ada
										@endif
									</td>
									<td>{{ $val->relasi }}</td>
									<td>{{ $val->created_at }}</td>
									<td><a target='_blank' href='/profile/view?nik={{ $val->noktp }}' class='btn btn-info'>View</a></td>
								</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-diterima_mitra">
			<div class="page-header">
				<div class="table-warning table-responsive">
					<table class="table table-bordered table-striped table-small-font" id="diterima_mitra">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Umur</th>
								<th>Jenis Kelamin</th>
								<th>Agama</th>
								<th>Golongan Darah</th>
								<th>Tempat Lahir</th>
								<th>Tanggal Lahir</th>
								<th>Email</th>
								<th>NPWP</th>
								<th>Alamat</th>
								<th>Provinsi</th>
								<th>Kabupaten / Kota</th>
								<th>Kecamatan</th>
								<th>Kelurahan</th>
								<th>Nomor Handphone</th>
								<th>Nomor WhatsApp</th>
								<th>Nomor KTP</th>
								<th>Nomor Kartu Keluarga</th>
								<th>Status Perkawinan</th>
								<th>Nama Ibu</th>
								<th>Nama Keluarga yang Bisa Dihubungi</th>
								<th>Kontak Keluarga yang Bisa Dihubungi</th>
								<th>Tingat Pendidikan Terakhir</th>
								<th>Nama Institusi Pendidikan Terakhir</th>
								<th>Jurusan</th>
								<th>Tanggal Lulus</th>
								<th>Gadget ( SmartPhone )</th>
								<th>Gadget ( Laptop )</th>
								<th>SIM</th>
								<th>Motor Pribadi</th>
								<th>Relasi</th>
								<th>Diperbaharui Oleh</th>
								<th>Mitra</th>
								<th>Tanggal Di Diperbaharui</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse($diterima_mitra as $key => $val)
							<tr>
								<td>{{ ++$key }}</td>
								<td>{{ $val->nama }}</td>
								<td>{{ $val->umur }}</td>
								<td>{{ $val->jk }}</td>
								<td>{{ $val->agama }}</td>
								<td>{{ $val->goldar }}</td>
								<td>{{ $val->tempat_lahir }}</td>
								<td>{{ $val->tgl_lahir }}</td>
								<td>{{ $val->email }}</td>
								<td>{{ $val->npwp }}</td>
								<td>{{ $val->alamat }}</td>
								<td>{{ $val->nama_provinsi }}</td>
								<td>{{ $val->nama_kota }}</td>
								<td>{{ $val->nama_kecamatan }}</td>
								<td>{{ $val->nama_kelurahan }}</td>
								<td>{{ $val->no_telp }}</td>
								<td>{{ $val->no_wa }}</td>
								<td>'{{ $val->noktp }}</td>
								<td>'{{ $val->nokk }}</td>
								<td>{{ $val->status_perkawinan }}</td>
								<td>{{ $val->ibu }}</td>
								<td>{{ $val->telpon_2_nm }}</td>
								<td>{{ $val->telpon_2 }}</td>
								<td>{{ $val->lvl_pendidikan }}</td>
								<td>{{ $val->last_study }}</td>
								<td>{{ $val->jurusan }}</td>
								<td>{{ $val->tgllulus }}</td>
								<td>
									@if($val->ket_smartphone == '1')
									Ada
									@else
									Tidak Ada
									@endif
								</td>
								<td>
									@if($val->ket_laptop == '1')
									Ada
									@else
									Tidak Ada
									@endif
								</td>
								<td>
									@if($val->ket_sim == '1')
									Ada
									@else
									Tidak Ada
									@endif
								</td>
								<td>
									@if($val->ket_motor == '1')
									Ada
									@else
									Tidak Ada
									@endif
								</td>
								<td>{{ $val->relasi }}</td>
								<td>{{ $val->modified_by }}</td>
								<td>{{ $val->mitra }}</td>
								<td>{{ $val->modified_at }}</td>
								<td><a target='_blank' href='/profile/view?nik={{ $val->noktp }}' class='btn btn-info'>View</a></td>
							</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-diterima_ta">
			<div class="page-header">
				<div class="table-primary table-responsive">
					<table class="table table-bordered table-striped table-small-font" id="diterima_ta">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Umur</th>
								<th>Jenis Kelamin</th>
								<th>Agama</th>
								<th>Golongan Darah</th>
								<th>Tempat Lahir</th>
								<th>Tanggal Lahir</th>
								<th>Email</th>
								<th>NPWP</th>
								<th>Alamat</th>
								<th>Provinsi</th>
								<th>Kabupaten / Kota</th>
								<th>Kecamatan</th>
								<th>Kelurahan</th>
								<th>Nomor Handphone</th>
								<th>Nomor WhatsApp</th>
								<th>Nomor KTP</th>
								<th>Nomor Kartu Keluarga</th>
								<th>Status Perkawinan</th>
								<th>Nama Ibu</th>
								<th>Nama Keluarga yang Bisa Dihubungi</th>
								<th>Kontak Keluarga yang Bisa Dihubungi</th>
								<th>Tingat Pendidikan Terakhir</th>
								<th>Nama Institusi Pendidikan Terakhir</th>
								<th>Jurusan</th>
								<th>Tanggal Lulus</th>
								<th>SmartPhone</th>
								<th>Gadget ( SmartPhone )</th>
								<th>Gadget ( Laptop )</th>
								<th>Motor Pribadi</th>
								<th>Relasi</th>
								<th>Rekrut Oleh</th>
								<th>Mitra</th>
								<th>Tanggal Di Rekrut</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse($diterima_ta as $key => $val)
							<tr>
								<td>{{ ++$key }}</td>
								<td>{{ $val->nama }}</td>
								<td>{{ $val->umur }}</td>
								<td>{{ $val->jk }}</td>
								<td>{{ $val->agama }}</td>
								<td>{{ $val->goldar }}</td>
								<td>{{ $val->tempat_lahir }}</td>
								<td>{{ $val->tgl_lahir }}</td>
								<td>{{ $val->email }}</td>
								<td>{{ $val->npwp }}</td>
								<td>{{ $val->alamat }}</td>
								<td>{{ $val->nama_provinsi }}</td>
								<td>{{ $val->nama_kota }}</td>
								<td>{{ $val->nama_kecamatan }}</td>
								<td>{{ $val->nama_kelurahan }}</td>
								<td>{{ $val->no_telp }}</td>
								<td>{{ $val->no_wa }}</td>
								<td>'{{ $val->noktp }}</td>
								<td>{{ $val->nokk }}</td>
								<td>{{ $val->status_perkawinan }}</td>
								<td>{{ $val->ibu }}</td>
								<td>{{ $val->telpon_2_nm }}</td>
								<td>{{ $val->telpon_2 }}</td>
								<td>{{ $val->lvl_pendidikan }}</td>
								<td>{{ $val->last_study }}</td>
								<td>{{ $val->jurusan }}</td>
								<td>{{ $val->tgllulus }}</td>
								<td>
									@if($val->ket_smartphone == '1')
									Ada
									@else
									Tidak Ada
									@endif
								</td>
								<td>
									@if($val->ket_laptop == '1')
									Ada
									@else
									Tidak Ada
									@endif
								</td>
								<td>
									@if($val->ket_sim == '1')
									Ada
									@else
									Tidak Ada
									@endif
								</td>
								<td>
									@if($val->ket_motor == '1')
									Ada
									@else
									Tidak Ada
									@endif
								</td>
								<td>{{ $val->relasi }}</td>
								<td>{{ $val->modified_by }}</td>
								<td>{{ $val->mitra }}</td>
								<td>{{ $val->modified_at }}</td>
								<td><a target='_blank' href='/profile/view?nik={{ $val->noktp }}' class='btn btn-info'>View</a></td>
							</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-blacklist">
			<div class="page-header">
				<div class="table-danger table-responsive">
					<table class="table table-bordered table-striped table-small-font" id="blacklist">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Nomor KTP</th>
								<th>Diblacklist Oleh</th>
								<th>Tanggal Blacklist</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse($blacklist as $key => $val)
							<tr>
								<td>{{ ++$key }}</td>
								<td>{{ $val->nama }}</td>
								<td>'{{ $val->noktp }}</td>
								<td>{{ $val->modified_by }}</td>
								<td>{{ $val->modified_at }}</td>
							</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script>
	$( function() {
		$('#pelamar').DataTable({
			select: true,
            dom: 'Blfrtip',
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
                extend: 'excel',
                text: 'Download to Excel',
                title: 'DATA SELURUH PELAMAR PERWIRA'
            }]
        });

		$('#diterima_ta').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA SELURUH REKRUTMEN TA YANG DITERIMA PERWIRA'
			}]
		});

		$('#diterima_mitras').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA SELURUH REKRUTMEN MITRA YANG DITERIMA PERWIRA'
			}]
		});

		$('#blacklist').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA SELURUH BLACKLIST PERWIRA'
			}]
		});

		$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
			$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
		} );

		// $(".table").DataTable({
		// 	"columnDefs": [{
		// 		"targets": 'no-sort',
		// 		"orderable": false,
		// 	}]
		// });

		$('#pelamar_wrapper .table-caption').text('Daftar Pelamar');
		$('#pelamar_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

		$('#diterima_ta__wrapper .table-caption').text('Daftar Di Terima TA');
		$('#diterima_ta__wrapper .dataTables_filter input').attr('placeholder', 'Search...');

		$('#diterima_mitra__wrapper .table-caption').text('Daftar Di Terima');
		$('#diterima_mitra__wrapper .dataTables_filter input').attr('placeholder', 'Search...');

		$('#blacklist_wrapper .table-caption').text('Daftar Blacklist');
		$('#blacklist_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
	});
</script>
@endsection