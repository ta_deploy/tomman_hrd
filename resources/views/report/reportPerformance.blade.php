@extends('layout')
@section('title')
Performance
@endsection
@section('content')
<div class="px-content">
	<div>
		<canvas id="myChart"></canvas>
	</div>
  <div class="page-header">
		<div class="table-primary">
			<table class="table table-bordered table-striped table-small-font" id="table_ku">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th>Nama</th>
						<th>Nik</th>
						<th class="no-sort">Action</th>
					</tr>
				</thead>
				<tbody class="middle-align">
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
	<script>
		$( function() {
			$("#table_ku").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});
			
			$('#table_ku_wrapper .table-caption').text('Performansi Karyawan');
			$('#table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			var ctx = document.getElementById("myChart").getContext('2d'),
			myChart = new Chart(ctx, {
				type: 'line',
				data: {
					labels: ["Tokyo",	"Mumbai",	"Mexico City",	"Shanghai",	"Sao Paulo",	"New York",	"Karachi","Buenos Aires",	"Delhi","Moscow"],
					datasets: [{
							label: 'Series 1', // Name the series
							data: [500,	50,	2424,	14040,	14141,	4111,	4544,	47,	5555, 6811], // Specify the data values array
							fill: false,
							borderColor: '#2196f3', // Add custom color border (Line)
							backgroundColor: '#2196f3', // Add custom color background (Points and Fill)
							borderWidth: 1 // Specify bar border width
					}]
				},
				options: {
					responsive: true, // Instruct chart js to respond nicely.
					maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
				}
			});

		});
  </script>
@endsection