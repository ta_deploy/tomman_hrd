@extends('layout')
@section('title')
Laporan Resign / Konseling
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div>
		<canvas id="myChart"></canvas>
	</div>
	<a href="/report/add/resling" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;">Input User Resign / Konseling</a>
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tabs-res_respect" data-toggle="tab">
				Resign Terhormat <span class="label label-warning">{{ count($resign_ter) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-resign_woresp" data-toggle="tab">
				Resign Tidak Terhormat <span class="badge badge-danger">{{ count($resign_black) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-kons" data-toggle="tab">
				Konseling <span class="badge badge-info">{{ count($kons) }}</span>
			</a>
		</li>
	</ul>
	<div class="tab-content tab-content-bordered">
		<div class="tab-pane fade in active" id="tabs-res_respect">
			<div class="page-header">
				<div class="table-warning">
					<table class="table table-bordered table-striped table-small-font" id="table_res" style="width:100%">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Nama</th>
								<th>Nik</th>
								<th>Jabatan</th>
								<th class="no-sort">Alasan Resign</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@foreach($resign_ter as $key => $val)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val->nama }}</td>
									<td>{{ $val->id_nik }}</td>
									<td>{{ $val->pos_name }}</td>
									<td>{{ $val->detail_langgar }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-resign_woresp">
			<div class="page-header">
				<div class="table-danger">
					<table class="table table-bordered table-striped table-small-font" id="table_res_not" style="width:100%">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Nama</th>
								<th>Nik</th>
								<th>Jabatan</th>
								<th class="no-sort">Alasan Resign</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@foreach($resign_black as $key => $val)
							<tr>
								<td>{{ ++$key }}</td>
								<td>{{ $val->nama }}</td>
								<td>{{ $val->id_nik }}</td>
								<td>{{ $val->pos_name }}</td>
								<td>{{ $val->detail_langgar }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-kons">
			<div class="page-header">
				<div class="table-info">
					<table class="table table-bordered table-striped table-small-font" id="table_kons" style="width:100%">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Nama</th>
								<th>Nik</th>
								<th>Jabatan</th>
								<th>Konseling</th>
								<th class="no-sort">Alasan</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@foreach($kons as $key => $val)
							<tr>
								<td>{{ ++$key }}</td>
								<td>{{ $val->nama }}</td>
								<td>{{ $val->id_nik }}</td>
								<td>{{ $val->pos_name }}</td>
								<td>{{ $val->jenis_jns }}</td>
								<td>{{ $val->detail_langgar }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
	<script>
		$( function() {
			$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    	} );
			
			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
				}]
			});

			$('#table_res_wrapper .table-caption').text('List Karyawan Resign Terhormat');
			$('#table_res_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#table_res_not_wrapper .table-caption').text('List Karyawan Resign Tidak Terhormat');
			$('#table_res_not_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#table_kons_wrapper .table-caption').text('List Karyawan Konseling');
			$('#table_kons_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ", 1)";
      };

			var data =<?= json_encode($data_chart) ?>,
			main_data = [],
			raw_month = [];

			$.each(data, function(key, val){
				$.each(val, function(key_c1, val_c1){
					var isi_data = $.map(val_c1, function(value, index) {
						return [value];
					});

					$.each(val_c1, function(key_c2, val_c2){
						raw_month.push(key_c2);
					});
					
					main_data.push({
						label: key+' '+key_c1,
						data: isi_data,
						fill: false,
						borderColor: dynamicColors(),
						backgroundColor: dynamicColors(),
						borderWidth: 1 // Specify bar border width
					});
				});
			});

			var month_hs = [...new Set([...raw_month])],
      bulanku = [];

      month_hs.sort(function(a, b){
        return parseInt(a)- parseInt(b);
      });

      $.each(month_hs, function (key, valu){
        bulanku.push(MONTHS[valu]);
      });

			// console.log(main_data)
			
			var ctx = $("#myChart"),
			myChart = new Chart(ctx, {
				type: 'line',
				data: {
					labels: bulanku,
					datasets: main_data
				},
				options: {
					responsive: true,
					maintainAspectRatio: false,
				}
			});
			
		});
  </script>
@endsection