@extends('layout')
@section('title')
Absensi
@endsection
@section('content')
<style>
	th {
		text-align: center;
	}
	td {
		text-align: center;
		vertical-align: middle;
		text-transform: uppercase;
	}
</style>
<div class="px-content">
@include('Partial.alerts')
	<div class="row">
		<div class="col-md-6">
			<div class="panel">
				<div class="panel-body text-xs-center">
					<div class="easy-pie-chart" data-percent='{{ $p_ba }}'>
						<span></span>
					</div>
					<br>
					<div class="label">Belum Absen</div>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel">
				<div class="panel-body text-xs-center">
					<div class="easy-pie-chart" data-percent='{{ $p_sa }}'>
						<span></span>
					</div>
					<br>
					<div class="label">Sudah Absen</div>
				</div>
			</div>
		</div>
	</div>
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tabs-blum_absn" data-toggle="tab">
				Belum Absen <span class="badge badge-default">{{ count($belum_absen) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-waiting" data-toggle="tab">
				Waiting <span class="badge badge-primary">{{ count($menunggu_approve) }}</span>
			</a>
		</li>
		<li>
			<a href="#table-sdh_abs" data-toggle="tab">
				Sudah Absen <span class="badge badge-success">{{ count($sudah_absen) }}</span>
			</a>
		</li>
	</ul>
	<form method="POST" action="/report/approval_massal">
	{{csrf_field()}}
	<div class="tab-content tab-content-bordered">
		<div class="tab-pane fade in active" id="tabs-blum_absn">
			<div class="page-header">
				<div class="table-default">
					<table class="table table-bordered table-striped table-small-font" id="table_blum_abs">
						<thead>
							<tr>
								<th>NO</th>
								<th>NAMA</th>
								<th>NIK</th>
								<th>SERTIFIKAT VAKSIN</th>
								<th>TANGGAL TERAKHIR ABSEN</th>
								<th>TANGGAL TERAKHIR APPROVE</th>
							</tr>
						</thead>
						<tbody class="middle-align">
						@forelse($belum_absen as $numb => $ba)
							<tr>
								<td>{{ ++$numb }}</td>
								<td>{{ $ba->nama }}</td>
								<td>{{ $ba->nik }}</td>
								<?php
								$foto	= "photo_hasil_screenshot";
								$pathx	= "/upload4/profile/{$ba->nik}";
								$thx	= "$pathx/$foto-th.jpg";
								$path	= null;

								if(file_exists(public_path().$thx) ){
									$path = "$pathx/$foto";
								}

								if($path){
									$th    = "$path-th.jpg";
									$img   = "$path.jpg";
								} else {
									$th    = null;
									$img   = null;
								}
								?>
								<td>
									@if(!empty($th) && file_exists(public_path().$th) )
									<center>
										<a href="{{ $img }}"><img src="{{ $th }}"></a>
									</center>
									@else
									<center>
										<img src="/image/placeholder.gif" style="width: 100px; height: 100px" alt="" /><br />
									</center>
									@endif
								</td>
								<td>{{ $ba->waktu_absen }}</td>
								<td>{{ $ba->date_approval }}</td>
							</tr>
						@empty
						@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-waiting">
			<div class="page-header">
				<div class="table-info">
					<table class="table table-bordered table-striped table-small-font" id="table_waiting">
						<thead>
							<tr>
								<th>NO</th>
								<th><input type="submit" class="btn btn-primary btn-xs" value="Approve Cheklist"></th>
								<th>NAMA</th>
								<th>NIK</th>
								<th>SERTIFIKAT VAKSIN</th>
								<th>TANGGAL ABSEN</th>
								<th>ACTION</th>
							</tr>
						</thead>
						<tbody class="middle-align">
						@forelse($menunggu_approve as $numb => $ma)
							<tr>
								<td>{{ ++$numb }}</td>
								<td>
									@if ($ma->status_kehadiran == "HADIR")
									@php
										$disable = 'disabled';
										if ($ma->approval == 0){
											$disable = '';
										}
									@endphp
									<input type="checkbox" name="approveMassal[]" value="{{ $ma->nik }}" {{ $disable }}>
									@endif
								</td>
								<td>{{ $ma->nama }}</td>
								<td>{{ $ma->nik }}</td>
								<?php
								$foto	= "photo_hasil_screenshot";
								$pathx	= "/upload4/profile/{$ma->nik}";
								$thx	= "$pathx/$foto-th.jpg";
								$path	= null;

								if(file_exists(public_path().$thx) ){
									$path = "$pathx/$foto";
								}

								if($path){
									$th    = "$path-th.jpg";
									$img   = "$path.jpg";
								} else {
									$th    = null;
									$img   = null;
								}
								?>
								<td>
									@if(!empty($th) && file_exists(public_path().$th) )
									<center>
										<a href="{{ $img }}"><img src="{{ $th }}"></a>
									</center>
									@else
									<center>
										<img src="/image/placeholder.gif" style="width: 100px; height: 100px" alt="" /><br />
									</center>
									@endif
								</td>
								<td>{{ $ma->waktu_absen }}</td>
								<td>
									@if ($ma->status_kehadiran == "HADIR")
										@if ($ma->approval == 0)
										<a href="/report/absen_approve/{{ $ma->nik }}" class="label label-default">APPROVE</a>
										<a href="/report/absen_decline/{{ $ma->absen_id }}" class="label label-danger">DECLINE</a>
										@elseif ($ma->approval == 3)
										<span class="label label-warning">DECLINED</span>
										@else
										<span class="label label-success">APPROVED</span>
										@endif
									@endif
								</td>
							</tr>
						@empty
						@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="table-sdh_abs">
			<div class="page-header">
				<div class="table-success">
					<table class="table table-bordered table-striped table-small-font" id="table_sdh_abs">
						<thead>
							<tr>
								<th>NO</th>
								<th>NAMA</th>
								<th>NIK</th>
								<th>SERTIFIKAT VAKSIN</th>
								<th>STATUS</th>
								<th>TANGGAL ABSEN</th>
								<th>TANGGAL APPROVE</th>
							</tr>
						</thead>
						<tbody class="middle-align">
						@forelse($sudah_absen as $numb => $sa)
							<tr>
								<td>{{ ++$numb }}</td>
								<td>{{ $sa->nama }}</td>
								<td>{{ $sa->nik }}</td>
								<?php
								$foto	= "photo_hasil_screenshot";
								$pathx	= "/upload4/profile/{$sa->nik}";
								$thx	= "$pathx/$foto-th.jpg";
								$path	= null;

								if(file_exists(public_path().$thx) ){
									$path = "$pathx/$foto";
								}

								if($path){
									$th    = "$path-th.jpg";
									$img   = "$path.jpg";
								} else {
									$th    = null;
									$img   = null;
								}
								?>
								<td>
									@if(!empty($th) && file_exists(public_path().$th) )
									<center>
										<a href="{{ $img }}"><img src="{{ $th }}"></a>
									</center>
									@else
									<center>
										<img src="/image/placeholder.gif" style="width: 100px; height: 100px" alt="" /><br />
									</center>
									@endif
								</td>
								<td>{{ $sa->status_kehadiran }}</td>
								<td>{{ $sa->waktu_absen }}</td>
								<td>{{ $sa->date_approval }}</td>
							</tr>
						@empty
						@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>
@endsection

@section('js')
	<script>
		$( function() {

			$(".table").DataTable({
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});

			$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
				$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
			} );

			$('#table_blum_abs_wrapper .table-caption').text('List Karyawan Belum Absen');
			$('#table_blum_abs_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#table_waiting_wrapper .table-caption').text('List Karyawan Menunggu Approve');
			$('#table_waiting_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#table_sdh_abs_wrapper .table-caption').text('List Karyawan Sudah Absen');
			$('#table_sdh_abs_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection