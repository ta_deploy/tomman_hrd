@extends('layout')
@section('title')
List Briefing
@endsection
@section('content')
<style>
	th {
		text-align: center;
	}
	td {
		text-align: center;
		text-transform: uppercase;
	}
</style>
<div class="px-content">
@include('Partial.alerts')
    <div class="row">
        <div class="col-md-12">
            @if(count($dataNow)==0 || $dataNow->status==0)
                <a href="/report/briefing" class="btn btn-primary"><span class="fa fa-plus"></span> Inputkan Laporan</a>
            @else
                <a href="#" class="btn btn-success"><span class="fa fa-check-circle"></span> Sudah Laporan Hari Ini</a>
            @endif
        </div>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table table-bordered">
        <thead>
            <tr>
                <th>NO</th>
                <th>ID LAPORAN</th>
                <th>TANGGAL</th>
                <th>CATATAN</th>
                <th>FOTO</th>
            </tr>
        <thead>
        <tbody>
            @foreach($listData as $no=>$data)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $data->id }}</td>
                <td>{{ $data->createdAt }}</td>
                <td>{{ $data->catatan }}</td>
                <td>
                    @foreach($getFoto as $foto)
                        @if($data->id==$foto->idFoto)
                            <a href="/upload/briefing/{{ $data->id }}/{{ $foto->foto }}"><img src="/upload4/briefing/{{ $data->id }}/{{ $foto->foto }}" width="80px" height="120px"></a>
                        @endif
                    @endforeach
                </td>
            </tr>
            @endforeach
        <tbody>
        </table>
    </div>
</div>
@endsection