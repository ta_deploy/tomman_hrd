@extends('layout')
@section('title')
Briefing Input
@endsection
@section('css')
<style>
.dropzone {
    background: white;
    border-radius: 5px;
    border: 2px dashed rgb(0, 135, 247);
    border-image: none;
    max-width: 500px;
    margin-left: auto;
    margin-right: auto;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css" type='text/css'>
@endsection
@section('content')
<div class="px-content">
@include('Partial.alerts')
<div class="panel">
  <div class="panel-heading">
    <div class="panel-title">Upload File</div>
  </div>
  <div class="panel-body">
    <form action="/report/briefing" method="POST" id="upload-dropzone" class="dropzone-box">
      <section>
        <div class="form-group">
          <div class="dz-default dz-message">
            <div class="dz-upload-icon"></div>
            Tarik Foto Kesini<br>
            <span class="dz-text-small">Atau Pilih Manual</span>
          </div>
        </div>
        <div class="fallback">
          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
          <input name="file" type="file" id="file_ny" multiple>

        </div>
      </section>
    </form>
      <div class="form-group">
        <label>Send To Grub Sektor</label>
        <input type="hidden" id="sektor" name="sektor" class="form-control" />
        {!! $errors->first('sektor', '<span class="label label-danger">:message</span>') !!}
      </div>
      <div class="form-group">
        <label>Catatan TL</label>
        <textarea class="form-control" id="catatan" name="catatan" rows="5"></textarea>
        {!! $errors->first('catatan', '<span class="label label-danger">:message</span>') !!}
      </div>
      <input type="hidden" name="idLaporan" id="idLaporan" value="{{ $id }}">
      <button type="submit" class="btn btn-primary pencet">Simpan Laporan</button>
    </div>
</div>
@endsection
@section('js')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script type="text/javascript">
Dropzone.autoDiscover = false;
  $(function() {
    var data_sektor= <?= json_encode($sektor) ?>;
    var sektor = function() {
      return {
        data: data_sektor,
        placeholder: 'Pilih Sektor',
        formatResult: function(data) {
        return  data.text;
        }
      }
    }
    $('#sektor').select2(sektor() );

    var myDropzone = new Dropzone("#upload-dropzone", {
        autoProcessQueue: false,
        parallelUploads: 2,
        url: '/report/briefing',
        maxFilesize:     1000,
        uploadMultiple: true,
        filesizeBase:    1000,
        resize: function(file) {
          return {
            srcX:      0,
            srcY:      0,
            srcWidth:  file.width,
            srcHeight: file.height,
            trgWidth:  file.width,
            trgHeight: file.height,
          };
        },
        init: function() {
          var mydropzone = this;
          $('.pencet').on("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            mydropzone.processQueue();
          });

          this.on("sendingmultiple", function(data, xhr, formData) {
            formData.append("sektor", $("#sektor").val() );
            formData.append("idLaporan", $("#idLaporan").val() );
            formData.append("catatan",  $("#catatan").val() );
          });

          this.on("successmultiple", function(data, xhr, formData) {
            console.log('tes')
          });
        },
        success: function(file, response){
          window.location = response.url;
        }

      });

      // Dropzone.uploadFiles = function(files) {
      //   var minSteps         = 6;
      //   var maxSteps         = 60;
      //   var timeBetweenSteps = 100;
      //   var bytesPerStep     = 100000;

      //   var self = this;

      //   for (var i = 0; i < files.length; i++) {

      //     var file = files[i];
      //     var totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep) ) );

      //     for (var step = 0; step < totalSteps; step++) {
      //       var duration = timeBetweenSteps * (step + 1);

      //       setTimeout(function(file, totalSteps, step) {
      //         return function() {
      //           file.upload = {
      //             progress: 100 * (step + 1) / totalSteps,
      //             total: file.size,
      //             bytesSent: (step + 1) * file.size / totalSteps
      //           };

      //           self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
      //           console.log(file.upload.progress)
      //           // if (file.upload.progress == 100) {
      //           //   console.log(file)
      //           //   // file.status =  Dropzone.SUCCESS;
      //           //   // self.emit('success', file, 'success', null);
      //           //   // self.emit('complete', file);
      //           //   // self.processQueue();
      //           // }

      //         }
      //       }(file, totalSteps, step), duration);
      //     }
      //   }
      // };
  });
</script>
@endsection