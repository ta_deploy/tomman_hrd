<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <link rel="icon" href="/image/icon.png" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=0.6, user-scalable=no, minimum-scale=1.0, maximum-scale=0.7">
  <meta name="description" content="Mari Bergabung Bersama Kami Putra Putri Terbaik di Banua Kalimantan untuk Berkesempatan Berkarir pada PT Telkom Akses dan Mitra - Mitra Kami di Seluruh Area Kalimantan | PERWIRA">
  <meta name="keywords" content="personalia warior akses,perwira,perwira tomman,tomman,perwira telkomakses,perwira telkom akses,warior akses,info loker telkom,info loker telkomakses,info loker kalimantan,loker,indihome">
  <meta name="author" content="Personalia Warior Akses - PERWIRA">
  <meta name="google-site-verification" content="JtYYY_8Z43BZrb7vjCMsOyo8QM2SfnqJNWldfFhhk1k" />

  <title>@yield('title') | PERWIRA</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <!-- Core stylesheets -->
  <link href="/css/bootstrap-dark.min.css" rel="stylesheet" type="text/css">
  <link href="/css/pixeladmin-dark.min.css" rel="stylesheet" type="text/css">
  <link href="/css/widgets-dark.min.css" rel="stylesheet" type="text/css">

  <!-- Theme -->
  <link href="/css/themes/mint-dark.min.css" rel="stylesheet" type="text/css">

  <!-- Pace.js -->
  <script src="/pace/pace.min.js"></script>
  @yield('css')
</head>
<body>


   <!-- Content -->
   @yield('content')

   <!-- Footer -->
  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- Load jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Core scripts -->
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>

  <!-- Your scripts -->
  <script src="/js/app.js"></script>
  @yield('js')
  <script>
  function time() {
      var d = new Date(),
      s = d.getSeconds(),
      m = d.getMinutes(),
      h = d.getHours(),
      today = new Date(),
      dd = String(today.getDate() ).padStart(2, '0'),
      hh = String(h).padStart(2, '0'),
      months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      digit_month = months[today.getMonth()],
      yyyy = today.getFullYear(),
      today = dd + ' ' + digit_month + ' ' + yyyy;
      $('.timestampt').html(today + " " + hh + ":" + m + ":" + s);
    }

    $(function () {
      var file = String(document.location).split('/');
      file.splice(0, 3);
      // file = file.replace(/(\.html).*/i, '$1');

      // if (!/.html$/i.test(file) ) {
      //   file = 'index.html';
      // }

      if($('body > .px-nav').find('.px-nav-item > a[href="/' + file.join('/') + '"]').parent().parent().parent().hasClass('px-nav-dropdown') ){
        $('body > .px-nav').find('.px-nav-item > a[href="/' + file.join('/') + '"]').parent().parent().parent().toggleClass('px-open')
      }

      // Activate current nav item
      $('body > .px-nav')
        .find('.px-nav-item > a[href="/' + file.join('/') + '"]')
        .parent()
        .addClass('active');

      $('body > .px-nav').pxNav();
      $('body > .px-footer').pxFooter();

      $('#navbar-notifications').perfectScrollbar();
      $('#navbar-messages').perfectScrollbar();

      setInterval(time, 1000);

      $('#search_me').submit( function(e){

        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": true,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "timeOut": "6000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        };

        var filter = /^[0-9-+]+$/;
        if (filter.test($('#data_search').val() ) ) {
          if($('#data_search').val().length <= 5){
            toastr.warning('Minimal Nik Harus 5 Karakter!', "warning");
            return false;
          }
          if($('#data_search').val().length > 8 && $('#data_search').val().length <= 10){
            toastr.warning('Minimal KTP Harus 10 Karakter!', "warning");
            return false;
          }
        }
        else {
          if($('#data_search').val().length == 0){
            toastr.warning('Masukkan Nik/KTP/Nama Naker!', "danger");
            return false;
          }
          if($('#data_search').val().length <= 4){
            toastr.warning('Minimal Nama Harus 4 Karakter!', "warning");
            return false;
          }
        }
      });

    });
  </script>
</body>
</html>
