@extends('layout')
@section('title')
Form Alat Kerja Teknisi
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="col-md-12">	
			<div class="panel panel-color panel-info panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Alat Kerja Teknisi</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="alker">Alat Kerja :</label>
						<div class="col-md-10">
							<select name="alker[]" class="form-control" id="alker" multiple>
							</select>
						</div>
					</div>
					<div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
            </div>
          </div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

		});
	</script>
@endsection