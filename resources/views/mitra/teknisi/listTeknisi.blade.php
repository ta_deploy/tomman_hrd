@extends('layout')
@section('title')
List Teknisi
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<a href="/mitra/addNew_teknisi" type="button" class="btn btn-primary btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Teknisi</a>
  <div class="page-header">
		<div class="table-primary">
			<table class="table table-bordered table-striped table-small-font" id="table_ku">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th>Nama</th>
						<th>Nik</th>
						<th class="no-sort">Action</th>
					</tr>
				</thead>
				<tbody class="middle-align">
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$("#table_ku").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});
			
			$('#table_ku_wrapper .table-caption').text('List Teknisi Dengan Nik');
			$('#table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection