@extends('layout')
@section('title')
Tambah Teknisi Baru
@endsection
@section('css')
{{-- <link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" /> --}}
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate" enctype="multipart/form-data">
			<div class="col-md-12">	
			<div class="panel panel-color panel-danger panel-border">
				<div class="panel-heading">
					<div class="panel-title">Tambah Teknisi</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<div class="col-sm-3">
							<label class="control-label" for="nama">Nama Teknisi</label>
							<select name="nama" class="form-control" id="nama"></select>
						</div>
						<div class="detail_orang"></div>
					</div>
					<div class="form-group">	
						<div class=" col-sm-2">
							<button class="btn bt-block btn-primary" type="submit">
								<span class="fa fa-cloud-download"></span>
								<span>Simpan</span>
							</button>
						</div>
					</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
	$( function(){
			$('select').select2();

			$('#nama').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: { 
					url: "/jx_dng/get_user",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Nama Tidak Terdaftar!";
						}
					},
					cache: true,
					success: function(value) {

					}
				}
			});
		});
	</script>
@endsection