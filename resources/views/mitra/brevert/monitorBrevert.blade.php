@extends('layout')
@section('title')
Monitoring Brevert
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tabs-not_br" data-toggle="tab">
				Belum Brevert <span class="badge badge-primary">{{ count($log_main) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-log_br" data-toggle="tab">
				Log Brevert <span class="badge badge-success">{{ count($log_data) }}</span>
			</a>
		</li>
	</ul>
	<div class="tab-content tab-content-bordered">
		<div class="tab-pane fade in active" id="tabs-not_br">
			<div class="page-header">
				<div class="table-info">
					<table class="table table-bordered table-striped table-small-font" id="not_br" style="width:100%">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Nama</th>
								<th>NIK</th>
								<th>Nomor KTP</th>
								<th>Daftar Brevert</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse($log_main as $key => $val)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val['nama'] }}</td>
									<td>{{ $val['id_nik'] }}</td>
									<td>{{ $val['noktp'] }}</td>
									<td>
										@foreach ($val['event'] as $val_c1)
											<ul>
												<li><span class="label label-primary">{{ $val_c1->title }}</span> / <span class="label label-info label-pill">{{ $val_c1->tgl_start }}</span> / <span class="label label-{{ $val_c1->hadir ? 'primary' : 'danger' }} label-tag">{{ $val_c1->hadir ?? 'Tidak Hadir' }}</span></li>
											</ul>
										@endforeach
									</td>
								</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-log_br">
			<div class="page-header">
				<div class="table-success">
					<table class="table table-bordered table-striped table-small-font" id="log_br" style="width:100%">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Nama</th>
								<th>NIK</th>
								<th>Nomor KTP</th>
								<th>Daftar Brevert</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse($log_data as $key => $val)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val['nama'] }}</td>
									<td>{{ $val['id_nik'] }}</td>
									<td>{{ $val['noktp'] }}</td>
									<td>
										@foreach ($val['event'] as $val_c1)
											<ul>
												<li><span class="label label-primary">{{ $val_c1->title }}</span> / <span class="label label-info label-pill">{{ $val_c1->tgl_start }}</span> - <span class="label label-default label-pill">{{ $val_c1->tgl_end }}</span> / <span class="label label-{{ $val_c1->hadir ? 'primary' : 'danger' }} label-tag">{{ $val_c1->hadir ?? 'Tidak Hadir' }}</span></li>
											</ul>
										@endforeach
									</td>
								</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    	} );
			
			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
				}]
			});

			$('#not_br_wrapper .table-caption').text('List Teknisi Belum Absen Brevert');
			$('#not_br_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#log_br_wrapper .table-caption').text('List Brever Yang Dijalani');
			$('#log_br_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection