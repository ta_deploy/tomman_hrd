<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Tomman</title>
  @yield('header')
  <script src="/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="/bower_components/vue/dist/vue.min.js"></script>
  <link rel="stylesheet" href="/bower_components/bootswatch-dist/css/bootstrap.min.css" />
  <!-- <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" media="screen" title="no title" charset="utf-8"> -->
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />

	<script src="/bower_components/devexpress-web-14.1/js/globalize.min.js"></script>
	
  <style media="screen">
    h3 {
      font-size: 24px;
    }
    .footer {
      padding: 10px 20px;
      background-color: #eee;
    }

    .list-group-item span, .form-group p.form-control-static {
      word-wrap: break-word;
    }

    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
    @media (max-width: 900px) {
        #menu-main .navbar-right li {
            border-top: 1px solid #E7E7E7;
        }
    }
    .navbar {
		    box-shadow: 0 1px 10px rgba(0,0,0,0.3);
		}
    .nav-tabs {
	    border : 1px solid #D6D6D6;
    }
    .btn-info {
	    background-color: #FF5442;
    }
    .label-info {
		    background-color: #2196f3;
		}
		.footer {
			position: fixed;
			bottom: 0px;
			width: 100%;
			background-color: #404040;
			color : #FFF;
		}
		.color_UP {
			color: #FFF;
			background-color : #1cd43e;
		}
		.color_OGP {
			color: #FFF;
			background-color : #d4841c;
		}
		.color_KENDALA {
			color: #FFF;
			background-color : #ff0000;
		}
		.color_BELUM {
			color: #FFF;
			background-color : #527ee6;
		}
		.report {
		}
		#tanggal {
			padding-left: 10px;
		}
    .pengumuman {
      padding: 0px;
      font-weight: bold;
    }


  </style>
</head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/workorder">Tomman</a>
        </div>

        <div id="menu-main" class="collapse navbar-collapse">
          
        </div>
    </div>
  </nav>

  <div class="container-fluid" style="margin-bottom : 350px">
    @yield('content')
  </div>

  <script>
    // $('.container').css('min-height', window.innerHeight - 66);
  </script>

<script src="/bower_components/select2/select2.min.js"></script>
<div class="footer">
<div class="pengumuman">
  <!--<marquee>• Perhatian Kepada Seluruh Teknisi PT1, STB yang usage di TV Portable harap dibawa kembali ke Logistik. Jika pelanggan ingin menggunakan STBnya kembali silahkan lapor ke 147 atau ke Plasa Telkom terdekat. • Jangan kada ingat absen wal... •  </marquee>
  -->
</div>
    <a href="/logout" style="margin-top:10px" class="btn btn-sm btn-warning pull-right">Logout</a>
    <span>Login Sebagai</span>
    <strong>[{{ session('auth')->name }}] {{ session('auth')->nama }}</strong>
  </div>
  @yield('plugins')
</body>
</html>
