@extends('layout')

@section('html-title')
	Project Dashboard
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Homepage
@endsection

@section('page-title')
	<link rel="stylesheet" href="/lib/xenon/js//uikit/uikit.css">
	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home active"></i>TOMMAN</a></li>
	</ol>
@endsection

@section('body')
	<div class="container">
		<div class="content">
			<div class="title"></div>
			<div class="panel panel-default panel-tabs"><!-- Add class "collapsed" to minimize the panel -->
				<div class="panel-heading">
					<div class="panel-options">
						<ul class="nav nav-tabs">
							<li>
								<a href="#tab-1" data-toggle="tab">Nota</a>
							</li>
							<li class="active">
								<a href="#tab-2" data-toggle="tab">Panjar</a>
							</li>
						</ul>
								
					</div>
				</div>
						
				<div class="panel-body">
					<div class="tab-content">
						<div class="tab-pane" id="tab-1">
							@foreach($nota as $no => $list)
								<div class="col-sm-3">
									<div class="xe-widget xe-counter-block" data-count=".num" data-from="0" data-to="{{$list->nilai}}" data-suffix="%" data-duration="2">
										<div class="xe-upper">
											<div class="xe-label">
												<strong class="num">{{number_format($list->nilai)}}</strong>
												<span>{{$list->tgl}}</span>
											</div>
															
										</div>
										<div class="xe-lower">
											<div class="border"></div>
											<span>{{$list->keperluan}}</span>
										</div>
									</div>
								</div>
						  @endforeach
						</div>
					<div class="tab-pane active" id="tab-2">
						<ul id="nestable-list-1" class="uk-nestable" data-uk-nestable>
							<?php $last = 0;$sisa = 0;?>
							@foreach($panjar as $p)
								<?php 
								$tambah = 0;$potongpanjar = 0;$kembali = 0;
								if($p->status == 1){
						      $tambah = $p->tpnilai;$sisa+=$tambah;
								}
						    else if($p->status == 2){
						      $kembali = $p->tpnilai;$sisa-=$kembali;
						    }
						    else{
						      $potongpanjar = $p->tpnilai;$sisa-=$potongpanjar;
						    }
					    	?>
								@if($last != $p->panjar_id)
									@if($last)
											</ul>
										</li>
									@endif
									<li data-item="a" data-item-id="b">
										<div class="uk-nestable-item">
											<div data-nestable-action="toggle"></div>
											  <div class="list-label">
											   	<div class="label label-default">Tgl:{{$p->ptgl}}</div>
													<div class="label label-blue">Project</div>
													<div class="label label-warning">Sisa:{{$p->psisa}}</div>
													<div class="label label-danger">Keperluan:{{$p->pkeperluan}}</div>
												</div>
										</div>
										<ul>
								@else
									<li data-item="Item 7" data-item-id="a">
										<div class="uk-nestable-item">
											<div class="list-label">
												<div class="label label-default">Tgl:{{$p->tptgl}}</div>
												<div class="label label-primary">Tambahan:{{$tambah}}</div>
												<div class="label label-secondary">Kembali:{{$kembali}}</div>
												<div class="label label-info">PotongPanjar:{{$potongpanjar}}</div>
												<div class="label label-warning">Sisa:{{$sisa}}</div>
												<div class="label label-danger">Keperluan:{{$p->tpkeperluan}}</div>
											</div>
										</div>
									</li>
								@endif
									<?php $last = $p->panjar_id; ?>
							@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
  	</div>
	</div>
@endsection
@section('plugins')
  <script src="/lib/xenon/js/uikit/js/uikit.min.js"></script>
	<script src="/lib/xenon/js/uikit/js/addons/nestable.min.js"></script>
@endsection
@section('footer')
<script type="text/javascript">
								jQuery(document).ready(function($)
								{
									$("#nestable-list-1").on('nestable-stop', function(ev)
									{
										var serialized = $(this).data('nestable').serialize(),
											str = '';
										
										//console.log( $(this).data('nestable').list() );
										str = iterateList(serialized, 0);
										//iterateList(serialized);
										
										$("#nestable-list-1-ev").val(str);
									});
								});
								
								function iterateList(items, depth)
								{
									var str = '';
									
									if( ! depth)
										depth = 0;
									
									console.log(items);
									
									jQuery.each(items, function(i, obj)
									{
										str += '[ID: ' + obj.itemId + ']\t' + repeat('—', depth+1) + ' ' + obj.item;
										str += '\n';
										
										if(obj.children)
										{
											str += iterateList(obj.children, depth+1);
										}
									});
									
									return str;
								}
								
								function repeat(s, n)
								{
									var a = [];
									while(a.length < n)
									{
										a.push(s);
									}
									return a.join('');
								}
								
													
							</script>
								
@endsection
