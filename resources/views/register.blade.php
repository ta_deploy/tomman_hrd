<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <link rel="icon" href="/image/icon.png" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Mari Bergabung Bersama Kami Putra Putri Terbaik di Banua Kalimantan untuk Berkesempatan Berkarir pada PT Telkom Akses dan Mitra - Mitra Kami di Seluruh Area Kalimantan | PERWIRA">
  <meta name="keywords" content="personalia warior akses,perwira,perwira tomman,tomman,perwira telkomakses,perwira telkom akses,warior akses,info loker telkom,info loker telkomakses,info loker kalimantan,loker,indihome">
  <meta name="author" content="Personalia Warior Akses - PERWIRA">
  <meta name="google-site-verification" content="JtYYY_8Z43BZrb7vjCMsOyo8QM2SfnqJNWldfFhhk1k" />

  <title>P E R W I R A</title>

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/css/pixeladmin.min.css" rel="stylesheet" type="text/css">
  <link href="/css/widgets.min.css" rel="stylesheet" type="text/css">
  <link href="/css/themes/clean.min.css" rel="stylesheet" type="text/css">

  <style>
    body {
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url('/image/bg-perwira.png');
    }
    .page-signin-header {
      box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
    }

    .page-signin-header .btn {
      position: absolute;
      top: 12px;
      right: 15px;
    }

    .page-signup-container {
      width: auto;
      margin: 30px 10px;
    }

    .page-signup-container form {
      border: 0;
      background-color: transparent;
    }

    @media (min-width: 544px) {
      .page-signup-container {
        width: 550px;
        margin: 60px auto;
        background-color: transparent;
      }
    }

    #page-signin-forgot-form { display: none; }
  </style>
</head>
<body>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" style="vertical-align: middle;">📣 Pengumuman</h5>
            </button>
        </div>
        <div class="modal-body">
          Untuk informasi terbaru terkait perusahaan bisa rekan cek di sosial media kami dibawah ini
          <br/><br/>
          <i class="fab fa-instagram"></i> Instagram : <a href="https://instagram.com/kalsel_hibat" style="color: black;">@kalsel_hibat</a>
          <br/>
          <i class="fab fa-instagram"></i> Instagram : <a href="https://www.instagram.com/infotelkomaksesbalikpapan" style="color: black;">@infotelkomaksesbalikpapan</a>
          <br/><br/>
          <i class="fab fa-youtube"></i> Youtube : <a href="https://www.youtube.com/@kalselhibat9382" style="color: black;">Telkom Akses Banjarmasin</a>
          <br/>
          <i class="fab fa-youtube"></i> Youtube : <a href="https://www.youtube.com/@performancekalimantanbersa8320" style="color: black;">Telkom Akses Balikpapan</a>  
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>

  <div class="page-signin-header p-a-2 text-sm-center bg-white">
    <b style="color: black !important; font-size: 30px">PERSONALIA</b> <b style="color: red; font-size: 30px">WARIOR AKSES</b>
  </div>


  <div class="page-signup-container" id="page-signin-form">

    @if ($message = Session::has('alerts') )
    <div class="alert alert-{{ Session::get('alerts')[0]['type'] }}">
        <strong>{{ Session::get('alerts')[0]['text'] }}</strong>
    </div>
    @endif
    <h2 class="m-t-0 m-b-4 text-xs-center font-weight-semibold font-size-20">Form Pendaftaran User Tomman!</h2>

    <form method="POST" class="panel p-a-12">
        <div class="row">
            <fieldset class="col-md-6 form-group form-group-lg">
                <label for="username">Username / NIK</label>
                <input type="text" class="form-control valid" name="username" placeholder="Username" required>
            </fieldset>

            <fieldset class="col-md-6 form-group form-group-lg">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="pass" placeholder="Password" minlength="8" required>
                <small class="text-muted">Minimum 8 Characters</small>
            </fieldset>

            <fieldset class="col-md-6 form-group form-group-lg">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Name" required>
            </fieldset>

            <fieldset class="col-md-6 form-group form-group-lg">
                <label for="atasan1">NIK Leader Relation</label>
                <input type="text" class="form-control valid_leader" name="atasan1" placeholder="NIK Leader Relation" required>
            </fieldset>

            <fieldset class="col-md-6 form-group form-group-lg">
                <label for="no_telp">No Handphone</label>
                <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" name="no_telp" minlength="10" placeholder="No Handphone">
            </fieldset>

            <fieldset class="col-md-6 form-group form-group-lg">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" placeholder="Email">
            </fieldset>

            <fieldset class="col-md-12 form-group form-group-lg">
                <label for="witel">Witel</label>
                <select class="form-control" id="select2-witel" name="reg_psb" style="width: 100%" data-allow-clear="true" required>
                <option></option>
                @foreach ($get_witel as $witel)
                <option value="{{ $witel->id }}"> {{ $witel->text }}</option>
                @endforeach
              </select>
            </fieldset>

            <fieldset class="col-md-6 form-group form-group-lg">
                <label for="level">Position</label>
                <select class="form-control" id="select2-level" name="level" style="width: 100%" data-allow-clear="true" required>
                <option></option>
                <option value="2"> Manager / Site Manager </option>
                <option value="15"> Team Leader </option>
                <option value="15"> Helpdesk / Admin </option>
                <option value="10"> Technician </option>
                <option value="9"> Jointer </option>
                <option value="5"> Waspang </option>
                <option value="59"> Customer Service Plasa </option>
                <option value="61"> Sales Forces </option>
                <option value="66"> HERO </option>
                {{-- <option value="9_0"> Pengawas Jointer </option> --}}
              </select>
            </fieldset>

            <fieldset class="col-md-6 form-group form-group-lg">
                <label for="mitra">Mitra</label>
                <select class="form-control" id="select2-mitra" name="mitra" style="width: 100%" data-allow-clear="true" required>
                <option></option>
                @foreach ($get_mitra as $mitra)
                  <option value="{{ $mitra->id }}"> {{ preg_replace('/^PT/', 'PT.', $mitra->text) }}</option>
                @endforeach
              </select>
            </fieldset>
        </div>

      <button type="submit" class="btn btn-block btn-lg btn-primary m-t-3">Register Now</button>
    </form>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="/pace/pace.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>
  <script src="/js/app.js"></script>
  <script type="text/javascript">
      $( function(){

        $(".valid").on('keypress, keyup', function(){
          var value = $(this).val();
          if(value.length >= 10){
            $(this).val(value.substring(0, 10) )
          }

          if(event.which >= 37 && event.which <= 40) return;
          $(this).val(function(index, value) {
            return value.replace(/[^a-zA-Z0-9]+/ig, "");
          });
        })

        $(".valid_leader").on('keypress, keyup', function(){
          var value = $(this).val();
          if(value.length >= 10){
            $(this).val(value.substring(0, 10) )
          }

          if(event.which >= 37 && event.which <= 40) return;
          $(this).val(function(index, value) {
            return value.replace(/[^0-9]+/ig, "");
          });
        })

        $(window).on('load', function() {
            $('#myModal').modal('show');
        });

        $('#select2-witel').select2({
            placeholder: '- Choice a Witel -',
        });

        $('#select2-level').select2({
            placeholder: '- Choice a Work Position -',
        });

        $('#select2-mitra').select2({
            placeholder: '- Choice a Mitra -',
        });
      });
  </script>

</body>
</html>