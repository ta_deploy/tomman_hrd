@extends('layout')
@section('css')
<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.css" rel="stylesheet" />
<style>
	.input-photos img {
		width: 100px;
		height: 150px;
		margin-bottom: 5px;
	}

	#map {
		top: 0; bottom: 0; height: 100%;
	}

	.mapboxgl-canvas {
			left: 0;
	}

	.modal-xl{
			width: 100%;
			max-width:1300px;
	}

	.modal-body{
			height: 400px;
	}

	.coordinates {
		background: rgba(0, 0, 0, 0.5);
		color: #fff;
		position: absolute;
		bottom: 40px;
		left: 10px;
		padding: 5px 10px;
		margin: 0;
		font-size: 11px;
		line-height: 18px;
		border-radius: 3px;
		display: none;
	}
</style>
@endsection
@section('title', 'Wadah Rawan')
@section('content')
<div class="modal fade" id="modal-large" tabindex="-1">
	<div class="modal-dialog modal-xl">
			<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Find Me</h4>
					</div>
					<div class="modal-body">
						<div id="map"></div>
						<pre id="coordinates" class="coordinates"></pre>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" required>Close</button>
					</div>
			</div>
	</div>
</div>
<div class="px-content">
	<div class="partial_alert"></div>
  <div class="page-header">
		<form method="post" class="form-horizontal validate" enctype="multipart/form-data">
			<div class="col-md-12">
				<div class="panel panel-color panel-info panel-border">
					<div class="panel-heading">
						<div class="panel-title title_kar">Input Rawan Kecelakaan</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="koor">Koordinat: </label>
							<div class="col-md-6">
								<input type="text" required id="koor" name="koor" class="form-control">
							</div>
							<div class="col-md-3">
								<button type="button" class="btn btn-primary koorkuh" style="margin-bottom: 10px;" data-toggle="modal" data-target="#modal-large"><i class="px-nav-icon ion-map"></i>Tampilkan Peta</button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="odp_near">ODP Terdekat: </label>
							<div class="col-md-9">
								<input type="text" required name="odp_near" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="alamat_manual">Alamat Lokasi: </label>
							<div class="col-md-9">
								<input type="text" required name="alamat_manual" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="note">Catatan Tambahan: </label>
							<div class="col-md-9">
								<textarea type="text" required name="note" class="form-control" style="resize: none;"></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-top: 50px;">
							@foreach ($list_foto as $v)
								<div class="text-center col-sm-3 input-photos">
									<a href="/image/placeholder.gif">
										<img src="/image/placeholder.gif" alt="profile" id="img-profile" class="photo_valid_dis" style="margin-bottom: 7px;"/>
									</a>
									<br/>
									<input type="file" class="hidden photo_profile" name="{{ $v }}" accept="image/jpeg" />
									<button type="button" class="btn btn-sm btn_up btn-info">
										<i class="ion ion-camera"></i>
									</button>
									<p style="font-size: 12px;">{{ str_replace('_', ' ', $v) }}</p>
									{!! $errors->first('profile', '<span class="label label-danger">:message</span>') !!}
								</div>
							@endforeach
						</div>
						<div class="form-group" style="margin-top: 100px;">
							<div class="col-md-12">
								<button type="submit" class="btn btn-block"><span class="fa fa-cloud-download"></span> Simpan</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.js"></script>
	<script>
		$( function(){

			$('.koorkuh').click(function() {
					mapboxgl.accessToken = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ';
					var map = new mapboxgl.Map({
						container: 'map',
						style: 'mapbox://styles/mapbox/streets-v11',
						center: [114.717711, -3.443288],
						zoom: 15
					});

					var marker = new mapboxgl.Marker({
						draggable: true
					}).setLngLat([114.717711, -3.443288])
					.addTo(map);

					var geolocate = new mapboxgl.GeolocateControl({
						positionOptions: {
							enableHighAccuracy: true
						},
						trackUserLocation: true
					});

					function onDragEnd() {
						var lngLat = marker.getLngLat();
						coordinates.style.display = 'block';
						coordinates.innerHTML =
						'Longitude: ' + parseFloat(lngLat.lng).toFixed(8) + '<br />Latitude: ' + parseFloat(lngLat.lat).toFixed(8);
						$('#koor').val([marker._lngLat.lat, marker._lngLat.lng]);
					}

					map.addControl(geolocate);

					geolocate.on('geolocate', function(e) {
						var lon = e.coords.longitude;
						var lat = e.coords.latitude;
						marker._lngLat.lng = parseFloat(lon).toFixed(8);
						marker._lngLat.lat = parseFloat(lat).toFixed(8);
						onDragEnd()
					})

					marker.on('dragend', onDragEnd);

					map.on('load', function () {
						geolocate.trigger();
						map.resize();
					});
        });

			$('.choose_nik').select2({
				width: '100%',
				placeholder: "Ketik Nik Karyawan",
				allowClear: true,
				minimumInputLength:2,
				ajax: {
					url: '/jx_dng/get_user_with_pos',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('.photo_profile').change(function() {
        /*console.log(this.name);*/
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
					/*$(inputEl).parent().find('input[type=text]').val(1);*/
					var reader = new FileReader();
					reader.onload = function(e) {
						$(inputEl).parent().find('img').attr('src', e.target.result);
					}
					reader.readAsDataURL(inputEl.files[0]);
        }
    	});

			$('.input-photos').on('click', 'button', function() {
				$(this).parent().find('input[type=file]').click();
			});

			$("select[name='nik_user']").on('select2:select, change', function(){
				var isi = $(this).val();

				function checkImage(url) {
					var request = new XMLHttpRequest();
					request.open("GET", url, true);
					request.send();
					request.onload = function() {
						status = request.status;
						if(request.status == 200){
							// $('#src_gmbr').attr('src', 'https://apps.telkomakses.co.id/wimata/photo/crop_'+isi+'.jpg')
							$('.btn_up').hide();
						}else{
							$('.btn_up').show();
						}
					}
				}

				// checkImage('https://apps.telkomakses.co.id/wimata/photo/crop_'+isi+'.jpg');
			})
		});
	</script>
@endsection