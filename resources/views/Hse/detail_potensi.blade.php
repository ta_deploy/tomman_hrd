@extends('layout')
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
<style>
	.input-photos img {
		width: 100px;
		height: 150px;
		margin-bottom: 5px;
	}

	.coordinates {
		background: rgba(0, 0, 0, 0.5);
		color: #fff;
		position: absolute;
		bottom: 40px;
		left: 10px;
		padding: 5px 10px;
		margin: 0;
		font-size: 11px;
		line-height: 18px;
		border-radius: 3px;
		display: none;
	}

	.css-icon {
    animation: pulsate 1s ease-out;
    -webkit-animation: pulsate 1s ease-out;
    -webkit-animation-iteration-count: infinite;
    opacity: 0.0
	}

	@-webkit-keyframes pulsate {
    0% {transform: opacity: 0.0;}
    50% {opacity: 1.0;}
    100% {transform: opacity: 0.0;}
	}
</style>
@endsection
@section('title', 'Wadah Rawan')
@section('content')
<div class="px-content">
	<div class="partial_alert"></div>
  <div class="page-header">
		<form method="post" class="form-horizontal validate" {!! in_array($data->update_hse, [1, 4]) ? 'enctype="multipart/form-data" ' : 'Action="/check_hse/'.$id.' " ' !!}>
			<div class="col-md-6">
				<div class="panel panel-color panel-info panel-border">
					<div class="panel-heading">
						<div class="panel-title title_kar">Input Rawan Kecelakaan <b><u>{{ $data->nama_created }} ({{ $data->created_by }})</u></b></div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="odp_near">No Telp PIC: </label>
							<div class="col-md-9">
								<input type="text" required name="odp_near" class="form-control" value="{{ $data->created_no_telp }}" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="koor">Koordinat: </label>
							<div class="col-md-9">
								<input type="text" required id="koor" name="koor" class="form-control" value="{{ $data->koordinat }}" readonly>
							</div>
						</div>
						<div class="form-group">
							<div id="map" style="height: 600px; margin-top: 10px;"></div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="odp_near">ODP Terdekat: </label>
							<div class="col-md-9">
								<input type="text" required name="odp_near" class="form-control" value="{{ $data->odp_near }}" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="alamat_manual">Alamat Lokasi: </label>
							<div class="col-md-9">
								<input type="text" required name="alamat_manual" class="form-control" value="{{ $data->alamat_manual }}" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="note">Catatan Tambahan: </label>
							<div class="col-md-9">
								<textarea type="text" name="note" class="form-control" value="{{ $data->note }}" readonly style="resize: none;"></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-top: 50px;">
							@foreach ($list_foto as $v)
								<div class="text-center col-sm-3 input-photos">
									@php
										// $path = '/upload/tomman_hrd/rawan_celaka/potensi_bahaya/'.Request::segment(2);
										$path = '/upload4/perwira/rawan_celaka/potensi_bahaya/'.Request::segment(2);
										$src  = "/image/placeholder.gif";

										if(file_exists(public_path(). $path ."/$v.jpg") )
										{
											$img  = "$path/$v.jpg";
											$src  = $img;
										}
									@endphp
									<a href="{{ $src }}">
										<img src="{{ $src }}" alt="profile" id="img-profile" class="photo_valid_dis" style="margin-bottom: 7px;"/>
									</a>
									<br/>
									<input type="file" class="hidden photo_profile" name="{{ $v }}" accept="image/jpeg" />
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-md-12">
					<div class="panel panel-color panel-info panel-border">
						<div class="panel-heading">
							<div class="panel-title title_kar">Input Hasil Lapangan</div>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="note_perbaikan">Catatan Tambahan: </label>
								<div class="col-md-9">
									<textarea type="text" {{ in_array($data->update_hse, [0, 1, 2, 4]) ? 'required' : 'readonly' }} name="note_perbaikan" class="form-control" style="resize: none;">{{ in_array($data->update_hse, [3]) ? $data->note_perbaikan : '' }}</textarea>
								</div>
							</div>
							<div class="form-group" style="margin-top: 50px;">
								@foreach ($list_foto_update as $v)
									<div class="text-center col-sm-3 input-photos">
										@php
											// $path = '/upload/tomman_hrd/rawan_celaka/tindak_lanjut/'.Request::segment(2);
											$path = '/upload4/perwira/rawan_celaka/tindak_lanjut/'.Request::segment(2);
											$src  = "/image/placeholder.gif";

											if(file_exists(public_path(). $path ."/$v.jpg") )
											{
												$img  = "$path/$v.jpg";
												$src  = $img;
											}
										@endphp
										<a href="{{ $src }}">
											<img src="{{ $src }}" alt="profile" id="img-profile" class="photo_valid_dis" style="margin-bottom: 7px;"/>
										</a>
										<br/>
										<input type="file" class="hidden photo_profile" name="{{ $v }}" accept="image/jpeg" />
										@if(in_array($data->update_hse, [1, 4]) )
											<button type="button" class="btn btn-sm btn_up btn-info">
												<i class="ion ion-camera"></i>
											</button>
										@endif
										<p style="font-size: 12px;">{{ str_replace('_', ' ', $v) }}</p>
										{!! $errors->first('profile', '<span class="label label-danger">:message</span>') !!}
									</div>
								@endforeach
							</div>
							<div class="form-group" style="margin-top: 100px;">
								@if(in_array($data->update_hse, [1, 4]) )
									<div class="col-md-12">
										<button type="submit" class="btn btn-block"><span class="fa fa-cloud-download"></span> Simpan</button>
									</div>
								@elseif($data->update_hse != 3)
								<div class="col-md-6">
									<button type="submit" class="btn btn-block btn-success" name="submit_info" value='approve'><span class="fa fa-check"></span> Approve</button>
								</div>
								<div class="col-md-6">
									<button type="submit" class="btn btn-block btn-danger" name="submit_info" value='reject'><span class="fa fa-ban"></span> Reject</button>
								</div>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="page-header">
						<div class="panel-heading">
							<div class="panel-title title_kar">Log HSE</div>
						</div>
						<div class="panel-body">
							<div class="table-primary">
								<table class="table table-bordered table-striped" style="width: 100%;">
									<thead>
										<tr>
											<th>No</th>
											<th>Status</th>
											<th>Keterangan</th>
											<th>Tanggal</th>
											<th>User</th>
										</tr>
									</thead>
									<tbody class="middle-align">
										@foreach ($data_log as $k => $v)
											<tr>
												<td>{{ ++$k }}</td>
												<td>{{ $v->status }}</td>
												<td>{{ $v->detail }}</td>
												<td>{{ $v->created_at }}</td>
												<td>{{ $v->nama_created }} ({{ $v->created_by }})</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
	<script src="https://unpkg.com/@turf/turf@6/turf.min.js"></script>
	<script>
		$( function(){
			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});

      var data_load = {!! json_encode($data) !!},
			mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			token = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ';
			googleStreets = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleSat = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleTerrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleAlteredRoad = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleTraffic = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
					maxZoom: 20,
					subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
			}),
			locate_now = L.layerGroup(),
			map = L.map('map', {
					center: data_load.koordinat.split(',').map(x => x.trim() ),
					zoom: 15,
					maxZoom: 19,
					layers: [googleStreets, googleHybrid, googleSat, googleTerrain, googleTraffic, googleAlteredRoad, locate_now]
			}),
			baseLayers = {
					'Jalan': googleStreets,
					'Satelit': googleSat,
					'Satelit Dan Jalan': googleHybrid,
					'Jalur': googleTerrain,
					'Jalur 2': googleAlteredRoad,
					'Lalu Lintas': googleTraffic,
			},
			overlays = {
					'Koordinat': locate_now,
			},
      marker = '',
			layerControl = L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map);

			var koor_now = data_load.koordinat.split(',').map(x => x.trim() );

			map.flyTo(koor_now, 13);

			if (map.hasLayer(marker) ){
				map.removeLayer(marker);
			}

			$('#show_map_latText').html(koor_now[0]);
			$('#show_map_lngText').html(koor_now[1]);

			var circle = L.circleMarker(koor_now, {
				className: 'css-icon',
				radius: 20,
				color: 'red',
			}).addTo(map);

			L.marker(koor_now).addTo(map);

			// $('#show_map').on('shown.bs.modal', function(){
      //   map.invalidateSize();
      // })

      // $('#map').css({
      //   height: (window.innerHeight * .30) + 'px'
      // })

			$('#show_map_gpsBtn').on('click', function(){
				if(marker.length != 0){
					map.removeLayer(marker);
				}

        navigator.geolocation.getCurrentPosition(position => {
          const { coords: { latitude, longitude } } = position;
					console.log(latitude, longitude)
          marker = new L.marker([latitude, longitude], {
            autoPan: true,
            draggable: 'true'
          }).addTo(map);

          $('#show_map_latText').html(latitude);
          $('#show_map_lngText').html(longitude);

          $('#koor').val(`${latitude}, ${longitude}`);

          map.flyTo([latitude, longitude], 16);
        })
      })

			$('.choose_nik').select2({
				width: '100%',
				placeholder: "Ketik Nik Karyawan",
				allowClear: true,
				minimumInputLength:2,
				ajax: {
					url: '/jx_dng/get_user_with_pos',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('.photo_profile').change(function() {
        /*console.log(this.name);*/
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
					/*$(inputEl).parent().find('input[type=text]').val(1);*/
					var reader = new FileReader();
					reader.onload = function(e) {
						$(inputEl).parent().find('img').attr('src', e.target.result);
					}
					reader.readAsDataURL(inputEl.files[0]);
        }
    	});

			$('.input-photos').on('click', 'button', function() {
				$(this).parent().find('input[type=file]').click();
			});
    });
	</script>
@endsection