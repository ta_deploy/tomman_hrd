@extends('layout')
@section('title', 'List Potensi Bahaya')
@section('css')
<style>
  th {
    text-align: center;
	text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
@include('Partial.alerts')
  <div class="page-header">
		<div class="table-{{ $status_hse == 3 ? 'success' : 'danger' }} table-responsive">
			<table class="table table-bordered table-striped" id="table_ku">
				<thead>
					<tr>
            <th>No</th>
            <th>Koordinat</th>
            <th>ODP Terdekat</th>
            <th>Alamat</th>
            <th>Catatan</th>
            <th>PIC</th>
            <th>Telepon PIC</th>
            <th>User Update</th>
            <th>Terakhir Update</th>
            <th>Action</th>
					</tr>
				</thead>
				<tbody class="middle-align">
					@foreach ($data as $num => $d)
            <tr>
              <td>{{ ++$num }}</td>
              <td>{{ $d->koordinat }}</td>
              <td>{{ $d->odp_near }}</td>
              <td>{{ $d->alamat_manual }}</td>
              <td>{{ $d->note }}</td>
              <td>{{ $d->nama_created }} ({{ $d->created_by }})</td>
              <td>{{ $d->created_no_telp }}</td>
              <td>{{ $d->modified_by ? $d->nama_updated .' ('.$d->modified_by.')' : '-' }}</td>
              <td>{{ $d->modified_at }}</td>
              <td>
                <a type='button' href='/detail_potensi/{{ $d->id }}' style="margin-bottom: 10px;" class='btn btn-sm btn-info'><span class='ion-wrench'></span> Detail</a>
              </td>
            </tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
@section('js')
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script>
		$( function() {
      $('.btn_delete').on('click', function(e){
      e.preventDefault()
      var href = $(this).attr('href');

      Swal.fire({
        title: 'Delete Potensi',
        text: 'Laporan Potensi Bahaya Akan Dihapus!!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Saya Mengerti!!'
      }).then( (result) => {
        if(result.isConfirmed){
          window.location.href = href;
        }
      });
    });

			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});

			$('#table_ku_wrapper .table-caption').text('List Potensi Bahaya');
			$('#table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection