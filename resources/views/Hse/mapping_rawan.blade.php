@extends('layout')
@section('title', 'List Potensi Bahaya')
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
<style>
  .css-icon {
    animation: pulsate 1s ease-out;
    -webkit-animation: pulsate 1s ease-out;
    -webkit-animation-iteration-count: infinite;
    opacity: 0.0
	}

	@-webkit-keyframes pulsate {
    0% {transform: opacity: 0.0;}
    50% {opacity: 1.0;}
    100% {transform: opacity: 0.0;}
	}
</style>
@endsection
@section('content')
<div class="px-content">
  <div id="map" style="height: 750px; margin-top: 10px;"></div>
</div>
@endsection
@section('js')
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
	<script>
		$( function() {
      var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      token = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ';
      googleStreets = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleSat = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTerrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleAlteredRoad = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTraffic = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
      }),
      rawan_bahaya = L.layerGroup(),
      map = L.map('map', {
        center: ['-3.319000', '114.584100'],
        zoom: 15,
        maxZoom: 19,
        layers: [googleStreets, googleHybrid, googleSat, googleTerrain, googleTraffic, googleAlteredRoad, rawan_bahaya]
      }),
      baseLayers = {
        'Jalan': googleStreets,
        'Satelit': googleSat,
        'Satelit Dan Jalan': googleHybrid,
        'Jalur': googleTerrain,
        'Jalur 2': googleAlteredRoad,
        'Lalu Lintas': googleTraffic,
      },
      overlays = {
        'Rawan Bahaya': rawan_bahaya,
      },
      layerControl = L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map),
      odp_green = new L.Icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      }),
      odp_blue = new L.Icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      }),
      odp_yellow = new L.Icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-yellow.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      }),
      odp_grey = new L.Icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-grey.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      })

      var data = {!! json_encode($data) !!};

      console.log(data, odp_green);

      $.each(data, function(k, v){
        if(v.update_hse == 2){
          icon = {icon: odp_blue};
        }else if(v.update_hse == 3){
          icon = {icon: odp_green};
        }else if(v.update_hse == 4){
          icon = {icon: odp_yellow};
        }else{
          icon = {icon: odp_grey};
        }

        var koor_rawan = v.koordinat.split(',').map(x => x.trim() );

        if(koor_rawan.length == 2){
          if($.inArray(v.update_hse, [3, 4]) === -1 ){
            var circle = L.circleMarker(koor_rawan, {
              className: 'css-icon',
              radius: 20,
              color: 'red',
            }).addTo(map);
          }

          L.marker(koor_rawan, icon)
          .bindPopup(`<strong>${v.alamat_manual}</strong><br>${v.status_hse}<br>${v.created_by}`)
          .addTo(rawan_bahaya);
        }
      })
		});
  </script>
@endsection