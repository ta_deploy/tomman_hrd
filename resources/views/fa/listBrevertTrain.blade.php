@extends('layout')
@section('title')
List Brevert / Training
@endsection
@section('css')
<style>
  th {
    text-align: center;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
  .upper {
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div class="col-md-12" style="padding-bottom: 11px;">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#tabs-event_all" data-toggle="tab">
					Event Tersedia <span class="badge badge-primary">{{ count($all_event) }}</span>
				</a>
			</li>
			<li>
				<a href="#tabs-event_still" data-toggle="tab">
					Event Yang Berlangsung <span class="badge badge-primary">{{ count($current_event) }}</span>
				</a>
			</li>
			<li>
				<a href="#tabs-event_done" data-toggle="tab">
					Sudah Selesai <span class="badge badge-primary">{{ count($done_event) }}</span>
				</a>
			</li>
		</ul>
		<div class="tab-content tab-content-bordered">
			<div class="tab-pane fade in active" id="tabs-event_all">
				<div class="page-header">
					<div class="table-info">
						@if(session('auth')->perwira_level != 12)
							<a href="/FA/submission/breving" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Brevert / Training Baru</a>
						@endif
						<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="event_all">
							<thead>
								<tr>
									<th>No</th>
									<th>Jenis Event</th>
									<th>Nama Event</th>
									<th>Tanggal Mulai</th>
									<th>Tanggal Berakhir</th>
									<th>Peserta</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($all_event as $key => $val)
								{{-- {{dd($all_event)}} --}}
									<tr>
										<td>{{ ++$key }}</td>
										<td>{{ $val->jenis }}</td>
										<td>{{ $val->title }}</td>
										<td>{{ $val->tgl_start }}</td>
										<td>{{ $val->tgl_end }}</td>
										<td>
											<ul>
												@foreach ($val->data_nama as $val)
													<li>{{ $val }}</li>
												@endforeach
											</ul>
										</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-event_still">
				<div class="page-header">
					<div class="table-info">
						<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="masih_event">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Event</th>
									<th>Tanggal Berakhir</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($current_event as $key => $val)
									<tr>
										<td>{{ ++$key }}</td>
										<td>{{ $val->title }}</td>
										<td>{{ $val->tgl_end }}</td>
										<td><a href="/FA/detail_brevert/{{ $val->id }}" target="_blank" type="button" class="btn btn-block btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-eye"></span> Lihat Detail</a></td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-event_done">
				<div class="page-header">
					<div class="table-info">
						<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="event_done">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Event</th>
									<th>Tanggal Mulai</th>
									<th>Tanggal Selesai</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($done_event as $key => $val)
									<tr>
										<td>{{ ++$key }}</td>
										<td>{{ $val->title }}</td>
										<td>{{ $val->tgl_start }}</td>
										<td>{{ $val->tgl_end }}</td>
										<td><a href="#" type="button" class="btn btn-block btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Sertifikat</a>
										</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@if(session('auth')->perwira_level != 12)
		<div class="col-md-12">
			<div class="page-header">
				<div class="table-info">
					<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="new_recruit">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>No KTP</th>
								<th>No Handphone / WhatsApp</th>
								<th>Mitra</th>
								<th>Tanggal Diterima</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse ($data_rec as $k => $data)
								<tr>
									<td>{{ ++$k }}</td>
									<td>{{ $data->nama }}</td>
									<td>{{ $data->noktp }}</td>
									<td>{{ $data->no_telp }} / {{ $data->no_wa }}</td>
									<td></td>
									<td>{{ $data->last_update }}</td>
								</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@endif
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    	} );

			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});

			$('#event_all_wrapper .table-caption').text('List Brevert / Training Tersedia');
			$('#event_all_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#masih_event_wrapper .table-caption').text('List Brevert / Training Yang Berlangsung');
			$('#masih_event_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#event_done_wrapper .table-caption').text('List Brevert / Training Selesai');
			$('#event_done_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#new_recruit_wrapper .table-caption').text('List Karyawan Siap Training');
			$('#new_recruit_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

		});
  </script>
@endsection