<!DOCTYPE html>
<html>
<head>
<title>WhatsApp API</title>
<style>
.client {
	border: 1px solid #ccc;
	padding: 10px;
	box-sizing: border-box;
	display: inline-block;
	margin: 5px;
}
.hide {
	display: none;
}
body {
	background-image: url('https://images.unsplash.com/photo-1499989545599-0800ab969152?ixid=MnwxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8MTV8MjAxMzMxMnx8ZW58MHx8fHw%3D&ixlib=rb-1.2.1&w=1000&q=80');
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
	color: white;
}
</style>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link id="favicon" rel="shortcut icon" type="image/png" href="https://web.whatsapp.com/img/favicon_c5088e888c97ad440a61d247596f88e5.png"/>
<body>

<div class="container" id="app">
	<center>

	<div class="form-container">
		<label for="client-id">ID</label><br>
		<input type="text" id="client-id" placeholder="Masukkan ID Sender">
		<br><br>
		<label for="client-description">Description</label><br>
		<textarea rows="4" id="client-description" placeholder="Masukkan Deskripsi"></textarea>
		<br><br>
		<button class="add-client-btn">Add Client</button>
		<br><br>
		<p>Contact & Support</p>
		<a href="tel:+6285248804480" target="_blank"><img src="https://1.bp.blogspot.com/-GjCpjdW8Hrs/XkXUvE0RseI/AAAAAAAABmk/u5e1zr7RGHQN2TFwPu1IoN8QJBtwXLH5QCLcBGAsYHQ/s1600/Logo%2BLink%2BAja%2521.png" width="50" height="40"/>
		<br>
		<a href="https://t.me/bigb00s" target="_blank"><i class="fa fa-telegram" style="font-size:36px"></i></a>
		<a href="https://wa.me/6285248804480" target="_blank" style="color: #2ECC71"><i class="fa fa-whatsapp" style="font-size:36px"></i></a>

	</div>
	</center>
	<hr>
	<div class="client-container">
		<div class="client hide">
			<h3 class="title"></h3>
			<p class="description"></p>
			<img src="" id="qrcode">
			<br>
			<h7>Log Activites :</h7>
			<p class="logs"></p>
		</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/3.1.1/socket.io.min.js"></script>

<script>
	$(document).ready(function() {
		var socket = io.connect('0.0.0.0:8031');
		// var socket = io.connect('https://sacrifc.herokuapp.com');

		$('.add-client-btn').click(function() {
			var clientId = $('#client-id').val(),
			clientDescription = $('#client-description').val(),
			template = $('.client').first().clone()
			.removeClass('hide')
			.addClass(clientId);

			template.find('.title').html(clientId);
			template.find('.description').html(clientDescription);
			$('.client-container').html(template);

			socket.emit('create-session', {
				id: clientId,
				description: clientDescription
			});
		});

		socket.on('init', function(data) {
			$('.client-container .client').not(':first').remove();
			// console.log(data);
			for (var i = 0; i < data.length; i++) {
				var session = data[i],
				clientId = session.id,
				clientDescription = session.description,
				template = $('.client').first().clone()
				.removeClass('hide')
				.addClass(clientId);

				template.find('.title').html(clientId);
				template.find('.description').html(clientDescription);
				$('.client-container').append(template);

				if (session.ready) {
					('.client.'+session.id+' .logs').append($('<li>').text('Whatsapp is Ready!') );
				} else {
					$('.client.'+session.id+' .logs').append($('<li>').text('Connecting...') );
				}
			}
		});

		socket.on('remove-session', function(id) {
			$('.client.'+id).remove();
		});

		socket.on('message', function(data) {
			$('.client.'+data.id+' .logs').append($('<li>').text(data.text) );
		});

		socket.on('qr', function(data) {
			// console.log(data.src)
			$('.client.'+data.id+' #qrcode').attr('src', data.src);
			$('.client.'+data.id+' #qrcode').show();
		});

		socket.on('ready', function(data) {
			$('.client.'+data.id+' #qrcode').hide();
		});

		socket.on('authenticated', function(data) {
			$('.client.'+data.id+' #qrcode').hide();
		});
	});
</script>
</body>
</html>