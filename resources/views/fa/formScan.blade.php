@extends('layout')
@section('title')
Scan
@endsection
@section('css')
<style>
	#preview{
		 width:100%;
		 height: 500px;
		 outline: 1px solid red;
	}
</style>
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<div class="alert alert-success" style="display: none;"></div>
		<video id="preview"></video>
	</div>
</div>
@endsection
@section('js')
	<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
	<script>
		$.fn.removeClassRegex = function(regex) {
			return $(this).removeClass(function(index, classes) {
				return classes.split(/\s+/).filter(function(c) {
					return regex.test(c);
				}).join(' ');
			});
		};
		document.addEventListener("DOMContentLoaded", event => {
			let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
			Instascan.Camera.getCameras().then(cameras => {
				console.log(cameras)
				scanner.camera = cameras[cameras.length - 1];
				scanner.start();
			}).catch(e => console.error(e) );

			scanner.addListener('scan', content => {
				console.log(content);
				// var socket = io('http://0.0.0.0:8031');
				var socket = io('https://sacrifc.herokuapp.com');

				var d = new Date,
				dformat = [d.getFullYear(), d.getMonth()+1, d.getDate()].join('-')+' '+ [d.getHours(), d.getMinutes(), d.getSeconds()].join(':');
				socket.emit("send", {
					ktp: "{{ session('auth')->noktp }}",
					date: dformat,
				});

				$.ajax({
					method: "GET",
					url: content,
					data: {
						date: dformat
					}
				}).done(function( msg ) {

					$('.alert').text(msg.msg.text);

					$('.alert').removeClassRegex(/^alert-/)
					$('.alert').addClass('alert-'+msg.msg.type)
					$('.alert').css({
						'display': 'block'
					})
				});
			});
		});

		// let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    //   scanner.addListener('scan', function (content) {
    //     console.log(content);

    //   });

    //   Instascan.Camera.getCameras().then(function (cameras) {
    //     if (cameras.length > 0) {
    //       scanner.start(cameras[0]);
    //     } else {
    //       console.error('No cameras found.');
    //     }
    //   }).catch(function (e) {
    //     console.error(e);
    //   });
	</script>
@endsection