@extends('layout')
@section('title')
Input Brevert / Training
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="panel-group">
				<div class="panel panel-color panel-info panel-border">
					<div class="panel-heading">
						<div class="panel-title">Form Brevert / Training</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="jenis">Jenis :</label>
							<div class="col-md-10">
								<select name="jenis" class="form-control" id="jenis">
									<option value="Brevert">Brevert</option>
									<option value="Training">Training</option>
									<option value="Training Karyawan Baru">Training Karyawan Baru</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="tgl">Tanggal :</label>
							<div class="col-md-10">
								<input type="text" name="tgl" class="form-control" id="tgl">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="title">Judul :</label>
							<div class="col-md-10">
								<textarea name="title" rows="4" style="resize: none" class="form-control" id="title"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="title">Nik :</label>
							<div class="col-md-10">
								<select class="form-control" name="Nik[]" id="Nik" multiple></select>
							</div>
						</div>
						<div class="form-group">
							<div class=" col-sm-12">
								<button class="btn btn-block btn-primary" type="submit">
									<span class="fa fa-cloud-download"></span>
									Simpan
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			$('#Nik').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: {
					url: "/jx_dng/get_user",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Nama Karyawan / KK / Nik Sementara Tidak Terdaftar!";
						}
					},
					cache: true,
					success: function(value) {
					}
				}
			});

			$("#jenis").on('select2:select', function(){
				if($(this).val() == 'Training Karyawan Baru'){
					$.ajax({
						type: 'GET',
						url: '/jx_dng/get_user_rekrut',
						dataType: 'JSON'
					}).done( function(e){
						$('#Nik').empty().trigger('change');
						$.each(e, function(key, value){
							$('#Nik').append(new Option(value.text , value.id, true, true) ).trigger('change')
						});
					});
				}
			});

			$('#tgl').daterangepicker({
				locale: {
					format: 'YYYY/MM/DD'
				},
				minDate: new Date(new Date().setDate(new Date().getDate() ) ),
				maxDate: new Date(new Date().setDate(new Date().getDate() + 12) )
			});
		});
	</script>
@endsection