@extends('layout')
@section('title')
Detail Brevert / Training
@endsection
@section('css')
<style>
	#qrcode {
	width: 128px;
	height: 128px;
	margin: 0 auto;
	text-align: center;
	margin-bottom: 11px;
	}
</style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div class="col-md-12">
		<div id="qrcode"></div>
		<code>*Ketuk Barcode Untuk scan!</code>
		<div class="page-header">
			<div class="table-success">
				<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="detail_evet">
					<thead>
						<tr>
							<th>Jenis Event</th>
							<th>Nama Event</th>
							<th>Tanggal Mulai</th>
							<th>TAnggal Berakhir</th>
						</tr>
					</thead>
					<tbody class="middle-align">
							<tr>
								<td>{{ $brevert->jenis }}</td>
								<td>{{ $brevert->title }}</td>
								<td>{{ $brevert->tgl_start }}</td>
								<td>{{ $brevert->tgl_end }}</td>
							</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="page-header">
			<div class="table-info">
				<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="new_recruit">
					<thead>
						<tr>
							<th class="text-center">No</th>
							<th>Nama</th>
							<th class="no-sort">Kartu Keluarga</th>
							<th class="no-sort">No KTP</th>
							<th>Jam Kehadiran</th>
						</tr>
					</thead>
					<tbody class="middle-align">
						@foreach ($data as $k => $data)
							<tr>
								<td>{{ ++$k }}</td>
								<td>{{ $data->nama }}</td>
								<td>{{ $data->nokk }}</td>
								<td>{{ $data->noktp }}</td>
								<td class="ktp" data-ktp="{{ $data->noktp }}">{{ $data->hadir }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js" integrity="sha512-NFUcDlm4V+a2sjPX7gREIXgCSFja9cHtKPOL1zj6QhnE0vcY695MODehqkaGYTLyL2wxe/wtr4Z49SvqXq12UQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
	<script>
		$( function() {

			// var socket = io.connect('0.0.0.0:8031');
      var socket = io.connect('https://sacrifc.herokuapp.com');

      socket.on('receive', function (data) {
        $('.ktp').each(function(val){
					if($(this).attr('data-ktp') == data.ktp){
						$(this).html(data.date)
					}
				})
      });

      socket.on('error', function() { console.error(arguments) });
      socket.on('message', function() { console.log(arguments) });

			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});

			$('#qrcode').on('click', function(){
				window.location.replace("/scan");
			});

			$('#detail_evet_wrapper .table-caption').text('Detail Brevert / Training');
			$('#detail_evet_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#new_recruit_wrapper .table-caption').text('Detail User Mengikuti Latihan');
			$('#new_recruit_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#qrcode').qrcode({
				width: 129,
				height: 129,
				text: '/FA/check_breving/'+window.location.href.split('/')[5]
			});

		});
  </script>
@endsection