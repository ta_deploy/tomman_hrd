@extends('layout')
@section('title')
Input Sertifikat
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="panel-group">	
				<div class="panel panel-color panel-info panel-border">
					<div class="panel-heading">
						<div class="panel-title">Form Sertifikat</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="title">Judul :</label>
							<div class="col-md-10">
								<select name="title" class="form-control" id="title"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="nik">nik :</label>
							<div class="col-md-10">
								<select name="nik" class="form-control" id="nik" multiple></select>
							</div>
						</div>
						<div class="form-group">
							<div class=" col-sm-12">
								<button class="btn btn-block btn-primary" type="submit">
									<span class="fa fa-cloud-download"></span>
									Simpan
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>	
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();
		});
	</script>
@endsection