@extends('layout')
@section('title')
Hasil Pencarian
@endsection
@section('css')
<style>
  th {
    text-align: center;
	text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<div class="row">
			<div class="clearfix">
				@forelse ($data as $k => $v )
					<div class="widget-products-item col-xs-12 col-sm-6 col-md-4 col-xl-3">
						<a href="#" class="widget-products-image">
							{{-- <img src="https://apps.telkomakses.co.id/wimata/photo/crop_{{ $v['nik'] }}.jpg"> --}}
							<span class="widget-products-overlay"></span>
						</a>
						<a href="/info_search/{{ $v['id_people'] }}" class="widget-products-title">
							{{ $v['nama'] }} / {{ $v['nik'] }}
							<span class="widget-products-price pull-xs-right label label-tag label-info">Detail</span>
						</a>
						<div class="widget-products-footer text-muted">
							<i class="fa fa-shopping-basket"></i> {{ $v['jml_order'] }} Total WO
							<i class="fa fa-check p-l-1"></i> {{ $v['jml_close'] }} WO Close
						</div>
					</div>
				@empty

				@endforelse
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>

  </script>
@endsection