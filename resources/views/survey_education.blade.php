<!doctype html>
<html>
  <head>
   <meta charset="utf-8" name="viewport content="width=device-width, initial-scale=1.0" />
   <title>Survey Form</title>
  </head>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Ubuntu&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap');
    @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css');

    @media only screen and (max-width: 767px) {
      html {
      background-image: url("/image/background-survey.jpg");
      background-repeat: no-repeat;
      background-position: center center;
      background-size: cover;
      }
    }

    html {
      min-height: 100%;
      background-image: url("/image/background-survey.jpg");
      background-repeat: no-repeat;
      background-position: center center;
      background-size: cover;
      /*background-attachment: fixed;*/
    }

    .header {
      color: #F8F8FF;
      text-align: center;
    }

    #title{
      padding-top: 30px;
      padding-bottom: -50px;
      font-family: 'Ubuntu', sans-serif;
      font-size: 36px;
      text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.4);
      font-weight: 100;
    }

    #description {
      font-family: 'Ubuntu', sans-serif;
      font-size: 24px;
      text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.4);
      padding-bottom: 40px;
    }

    /* ------- survey -----------*/

    #survey-form {
      text-align: left;
      line-height: 1.6;
      max-width: 550px;
      margin: 0 auto 50px auto;
      background-color: rgb(38 49 85 / 90%);
      border: 5px solid ghostwhite;
      border-radius: 10px;
      color: #F8F8FF;
      font-family: 'Ubuntu', sans-serif;
      font-size: 18px;
      font-weight: 200;
      padding: 30px 20px 50px 20px;
    }

    input[type=text] {
      width: 100%;
      padding: 12px 20px;
      margin: 8px 0;
      box-sizing: border-box;
      border: 2px;
      border-radius: 4px;
    }
    .dropdown {
      width: 100%;
      padding: 12px 20px;
      margin: 8px 0;
      box-sizing: border-box;
      border: 2px;
      border-radius: 4px;
    }
    input[type=submit] {
      width: 100%;
      padding: 12px 20px;
      margin: 8px 0;
      box-sizing: border-box;
      border: 2px;
      background-color: #808080;
      border-radius: 4px;
      color: #F8F8FF;
      font-family: 'Ubuntu', sans-serif;
      font-size: 18px;
      font-weight: 200;
      cursor: pointer;
    }
    input[type=submit]:hover {
      background-color: #666666;
    }

    :root {
      --infoBlue: #2e86de;
      --infoBlueLight: #eff6fc;
      --successGreen: #329f5d;
      --successGreenLight: #eff7f2;
      --errorRed: #c2160a;
      --errorRedLight: #faedec;
      --warningOrange: #fa7f05;
      --warningOrangeLight: #fff5eb;
      --bodyTextColour: #212121;
    }

    .alert {
      margin-block: 2.5rem;
      padding: 1.25rem;
      display: grid;
      grid-gap: 1.25rem;
      grid-template-columns: max-content auto;
      border-radius: 4px;
      border-width: 4px;
      border-left-style: solid;
      transition: 0.12s ease;
      position: relative;
      overflow: hidden;
    }
    .alert:before {
      content: "";
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      mix-blend-mode: soft-light;
      background: linear-gradient(90deg, rgba(255, 255, 255, 0) 30%, white 56%, rgba(2, 0, 36, 0.1) 82%);
      z-index: 1;
    }
    .alert .icon,
    .alert .content {
      z-index: 2;
    }
    .alert .icon {
      line-height: 1;
    }
    .alert .title {
      font-weight: 700;
      margin-bottom: 0.75rem;
    }
    .alert .content {
      max-width: 60ch;
    }
    .alert.alert--info {
      background-color: var(--infoBlueLight);
      border-left-color: var(--infoBlue);
    }
    .alert.alert--info .icon {
      color: var(--infoBlue);
    }
    .alert.alert--success {
      background-color: var(--successGreenLight);
      border-left-color: var(--successGreen);
    }
    .alert.alert--success .icon {
      color: var(--successGreen);
    }
    .alert.alert--error {
      background-color: var(--errorRedLight);
      border-left-color: var(--errorRed);
    }
    .alert.alert--error .icon {
      color: var(--errorRed);
    }
    .alert.alert--warning {
      background-color: var(--warningOrangeLight);
      border-left-color: var(--warningOrange);
    }
    .alert.alert--warning .icon {
      color: var(--warningOrange);
    }

    @media (max-width: 767px) {
      .alert {
        grid-template-columns: auto;
        padding: 1rem;
        grid-gap: 0.75rem;
      }
      .alert .icon {
        font-size: 1.5rem;
      }
      .alert .title {
        margin-bottom: 0.5rem;
      }
    }
  </style>
  <body>
    @if (Session::has('alerts') )
      @foreach(Session::get('alerts') as $alert)
        <div class="alert alert--info">
          <i class="fa fa-info-circle fa-2xl icon"></i>
          <div class="content">
            <div class="title">Berhasil!</div>
            <div class="body">{!! $alert['text'] !!}</div>
          </div>
        </div>
      @endforeach
    @endif
    <header class="header">
     <h1 id="title">Formulir Survey Pendidikan</h1>
     <p id="description">Terima kasih sudah meluangkan waktu untuk mengisi survey dibawah ini
     </p>
    </header>

    <form id="survey-form"method="post">
      <div class="form-group select_nik" style='padding-bottom: 10px;'>
        <label for="nik_nama">Nik/Nama</label><br>
        <select id="nik_nama" name="nik_nama" class="dropdown"></select>
      </div>

      <div class="form-group" style='padding-bottom: 10px;'>
        <p>Apakah Anda Menemukan Nik/Nama Anda?</p>
        <input type="radio" id="yes" name="select_opsi_avail" class="select_opsi_avail" value="yes">
        <label for="yes">Ya</label><br>
        <input type="radio" id="no" name="select_opsi_avail" class="select_opsi_avail" value="no">
        <label for="no">Tidak</label>
      </div>

      <div class="no_avail form-group" style='padding-bottom: 10px;'>
        <label id="nama-label" for="nama">Nama</label><br/>
        <input type="text" id="nama" name="nama" placeholder="Masukkan Nama">
      </div>

      <div class="no_avail form-group" style='padding-bottom: 10px;'>
        <label id="nik-label" for="nik">Nik</label><br/>
        <input type="text" id="nik" name="nik" placeholder="Masukkan Nik">
      </div>

    <div class="form-group" style='padding-bottom: 10px;'>
      <p>Jabatan</p>
      <input type="radio" id="Off 1 / Ast.Man" name="level" value="Off 1 / Ast.Man">
      <label for="Off 1 / Ast.Man">Off 1 / Ast.Man</label><br>
      <input type="radio" id="Off 2 / SPV" name="level" value="Off 2 / SPV">
      <label for="Off 2 / SPV">Off 2 / SPV</label><br>
      <input type="radio" id="Off 3 / Team Leader" name="level" value="Off 3 / Team Leader">
      <label for="Off 3 / Team Leader">Off 3 / Team Leader</label><br>
      <input type="radio" id="Teknisi" name="level" value="Teknisi">
      <label for="Teknisi">Teknisi</label>
    </div>

     <div class="form-group" style='padding-bottom: 10px;'>
      <label for="pendidikan">Level Pendidikan</label><br>
      <select id="pendidikan" class="dropdown" name="level_pendidikan" required>
        <option disabled selected value>Pilih Pendidikan Sekarang</option>
        <option value="SMA/SMK">SMA / SMK</option>
        <option value="D1">D1</option>
        <option value="D2">D2</option>
        <option value="D3">D3</option>
        <option value="progress_S1">Sedang Melanjutkan Pendidikan S1</option>
        <option value="S1">S1</option>
        <option value="progress_S2">Sedang Melanjutkan Pendidikan S2</option>
        <option value="S2">S2</option>
        <option value="S3">S3</option>
      </select>
    </div>
    <input type="submit" id="submit" name="submit" value="SUBMIT">
    </form>
  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
    $(function(){
      $('#nik_nama').select2({
        placeholder: 'Pilih Nik/Nama',
        data: {!! json_encode($data_naker) !!}
      });

      $('.select_opsi_avail').on('change', function(){
        if($(this).val() == 'yes'){
          $('.select_nik').show();
          $('.no_avail').hide();
        }else{
          $('.no_avail').show();
          $('.select_nik').hide();
        }
      });

      $("#yes").prop("checked", true).trigger('change');
    })
  </script>
</html>