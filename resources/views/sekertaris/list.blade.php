@extends('layout_nologin')
@section('title')
List Laporan Ring 3
@endsection
@section('content')
<div class="px-content">
    @include('Partial.alerts')
    <div class="panel box">
      <div class="box-row">
        <div class="box-container valign-middle">
          <div class="box-cell p-y-1 p-x-1">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Provisioning
                </div>
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $d)
                    @if($d->posision == 'Site Manager Helpdesk' || $d->posision == 'Team Leader Provisioning & Migrasi' || $d->posision == 'Site Manager Provisioning & Migration')
                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                        @if($d->posision == 'Team Leader Provisioning & Migrasi')
                            <span class="label {{ $d->QC_PSB?'label-success':'label-danger' }} label-tag pull-right">QC PSB</span>

                        @elseif($d->posision == 'Site Manager Helpdesk')
                            <span class="label {{ $d->Rekap_Reason_Pelanggan_Cabut?'label-success':'label-danger' }} label-tag pull-right">Pelanggan Cabut</span>
                            <span class="label {{ $d->Progress_Uji_Petik_IXSA?'label-success':'label-danger' }} label-tag pull-right">Uji Petik IXSA</span>
                        @else
                            <span class="label {{ $d->CABUTAN_NTE?'label-success':'label-danger' }} label-tag pull-right">Cabutan NTE</span>
                        @endif
                          <br/>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->tgl_update,11,5) }}</span>
                        </td>
                      </tr>
                    @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="box-cell p-y-1 p-x-1 valign-top">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Assurance
                </div>
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $d)
                    @if($d->posision == 'Team Leader Sektor IOAN' || $d->posision == 'Team Leader Helpdesk IOAN')
                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                        @if($d->posision == 'Team Leader Sektor IOAN')
                            <span class="label {{ $d->UNSPEC?'label-success':'label-danger' }} label-tag pull-right">UNSPEC</span>
                            <span class="label {{ $d->VALINS?'label-success':'label-danger' }} label-tag pull-right">VALINS</span>
                        @else
                            <span class="label {{ $d->Q?'label-success':'label-danger' }} label-tag pull-right">Q</span>
                            <span class="label {{ $d->SUGAR?'label-success':'label-danger' }} label-tag pull-right">SUGAR</span>
                            <span class="label {{ $d->Dashboard_NONATERO?'label-success':'label-danger' }} label-tag pull-right">NONATERO</span>
                        @endif
                        <br/>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->tgl_update,11,5) }}</span>
                        </td>
                      </tr>
                    @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div> <!-- / .box-container -->
      </div>
      <div class="box-row">
        <div class="box-container">
          <div class="box-cell p-y-1 p-x-1">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Corporate Service
                </div>
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $d)
                    @if($d->posision == 'Team Leader TSEL & OLO Services' || $d->posision == 'Team Leader MO SPBU' || $d->posision == 'Team Leader BGES')
                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                        @if($d->posision == 'Team Leader BGES')
                            <span class="label {{ $d->UNSPEC?'label-success':'label-danger' }} label-tag pull-right">UNSPEC</span>
                            <span class="label {{ $d->VALINS?'label-success':'label-danger' }} label-tag pull-right">VALINS</span>
                        @elseif($d->posision == 'Team Leader MO SPBU')
                            <span class="label {{ $d->Excelent_SPBU?'label-success':'label-danger' }} label-tag pull-right">Excelent SPBU</span>
                            <span class="label {{ $d->Mytracker?'label-success':'label-danger' }} label-tag pull-right">Mytracker</span>
                        @else
                            <span class="label {{ $d->MTTR?'label-success':'label-danger' }} label-tag pull-right">MTTR</span>
                            <span class="label {{ $d->Q?'label-success':'label-danger' }} label-tag pull-right">Q</span>
                        @endif
                        <br/>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->tgl_update,11,5) }}</span>
                        </td>
                      </tr>
                    @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="box-cell p-y-1 p-x-1 valign-top">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Support
                </div>
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $d)
                    @if($d->posision == 'Team Leader Inventory & Asset Management')
                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                        @if($d->posision == 'Team Leader Inventory & Asset Management')
                            <span class="label {{ $d->STOK_OTP_Prekso?'label-success':'label-danger' }} label-tag pull-right">STOK OTP & Prekso</span>
                        @endif
                        <br/>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->tgl_update,11,5) }}</span>
                        </td>
                      </tr>
                    @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div> <!-- / .box-container -->
      </div>
    </div>
</div>
@endsection