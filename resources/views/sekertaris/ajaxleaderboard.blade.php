@foreach($arraysektor as $as)
<div class="widget-profile-counters">
  <a href="#" class="col-xs-2 detil" data-toggle="modal" data-target="#modal-base" data-sektor="{{ $as['sektor'] }}" data-tgl_load="{{ $tgl_load }}" data-tahun="{{ date('Y') }}">
    <span class="widget-profile-counter">{{ explode(' - ', $as['sektor'])[2] ?? $as['sektor'] }}</span>
  </a>
  <a href="#" class="col-xs-2 detil" data-toggle="modal" data-target="#modal-base" data-sektor="{{ $as['sektor'] }}" data-tgl_load="{{ $tgl_load }}" data-tahun="{{ date('Y') }}">
    <span class="widget-profile-counter">{{ $as['witel'] }}</span>
  </a>
  <a href="#" class="col-xs-2 detil" data-toggle="modal" data-target="#modal-base" data-sektor="{{ $as['sektor'] }}" data-tgl_load="{{ $tgl_load }}" data-tahun="{{ date('Y') }}">
    <span class="widget-profile-counter">{{ $as['last_nilai'] }}</span>
  </a>
  <a href="#" class="col-xs-4 detil" data-toggle="modal" data-target="#modal-base" data-sektor="{{ $as['sektor'] }}" data-tgl_load="{{ $tgl_load }}" data-tahun="{{ date('Y') }}">
    <span class="widget-profile-counter">
      <?php
        $sum_tropy = $sum_bintang = $sum_skull = $sum_quest = 0;

        foreach($as['result'] as $r){

          if($r['result'] === 2){
            echo "🏆";
            $sum_tropy++;
          }else if($r['result'] === 1){
            echo "⭐";
            $sum_bintang++;
          }else if($r['result'] === 0){
            echo "💀";
            $sum_skull++;
          }else{
            echo "❓";
            $sum_quest++;
          }
        }
      ?>
    </span>
  </a>
  <a href="#" class="col-xs-2 detil" data-toggle="modal" data-target="#modal-base" data-sektor="{{ $as['sektor'] }}" data-tgl_load="{{ $tgl_load }}" data-tahun="{{ date('Y') }}">
    <span class="widget-profile-counter">🏆{{ $sum_tropy }} ⭐{{ $sum_bintang }} 💀{{ $sum_skull }} ❓{{ $sum_quest }}</span>
  </a>
</div>
@endforeach