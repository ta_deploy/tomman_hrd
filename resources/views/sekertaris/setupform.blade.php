@extends('layout_nologin')
@section('title')
Laporan Ring 3
@endsection
@section('css')
  <style type="text/css">

    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
    .no-search .select2-search {
      display:none
    }
  </style>
@endsection
@section('content')
<div class="px-content">
	 @include('Partial.alerts')
   <div class="panel">
    <div class="panel-title">FORM ASSIGN LAPORAN <span class="label label-success badge pull-right">{{ $data->nama or 'Register' }}</span></div>
  <div class="panel-body">
  <form method="post" id="setupform" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-4 form-group form-message-dark">
            <label for="nik">NIK</label>
            <input type="text" name="nik" id="nik" class="form-control border-round" required autocomplete="off" placeholder="Input nik" value="{{ $data->nik or old('nik') }}" {{ Request::segment(2)=='new'?'':'disabled' }}/>
        </div>
        <div class="col-md-4 form-group form-message-dark">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control border-round" autocomplete="off" required placeholder="Input Nama"  value="{{ $data->nama or old('nama') }}"/>
        </div>
        <div class="col-md-4 form-group form-message-dark">
          <label for="sektor">Sektor</label>
          <input type="text" name="sektor" id="sektor" class="form-control border-round" autocomplete="off" value="{{ $data->sektor or old('sektor') }}" placeholder="Input Sektor (opsional)" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 form-group form-message-dark">
          <label for="posision">Position</label>
          <input type="text" name="posision" id="posision" class="form-control border-round" autocomplete="off" value="{{ $data->posision or old('posision') }}" required placeholder="Input Position"/>
        </div>
        <div class="col-md-4 form-group form-message-dark">
            <label for="witel">Witel</label>
            <input type="text" name="witel" id="witel" class="form-control border-round" autocomplete="off" value="{{ $data->witel or old('witel') }}" required/>
        </div>
        <div class="col-md-4 form-group form-message-dark">
          <label for="grouping">Grouping</label>
          <input type="text" name="grouping" id="grouping" class="form-control border-round" autocomplete="off" value="{{ $data->grouping or old('grouping') }}" required/>
        </div>
    </div>
    <div class="row">
      <div class="col-md-4 form-group form-message-dark">
        <label for="cc">CC Laporan</label>
        <input type="text" name="cc" id="cc" class="form-control border-round" autocomplete="off" value="{{ $data->cc or old('cc') }}"/>
      </div>

      <div class="col-md-6 form-group form-message-dark">
        <label for="jenis_laporan">Jenis Laporan</label>
        <div class="input-group">
          <?php
            $p = array();
            if(isset($data->payload) ){
              $p = json_decode(@$data->payload);
            }
          ?>
          <select name="jenis_laporan[]" id="jenis_laporan" class="form-control border-round" multiple required>
            @foreach($jenis_laporan as $jl)
              <option value="{{ $jl->id }}" {{ in_array($jl->id, $p)?'selected':'' }}>{{ $jl->text }}({{ $jl->required?'Required':'Optional' }})</option>
            @endforeach
          </select>
          <div class="input-group-btn">
            <button type="button" class="btn btn-success border-round" data-toggle="modal" data-target="#modal-tematik"><i class="ion-plus"></i> Register Tematik</button>
          </div>
        </div>
      </div>
      <div class="col-md-2 form-group form-message-dark">
        <label for="a">&nbsp;</label>
        <button type="submit" class="btn btn-primary form-control border-round"><i class="ion-soup-can"></i> Simpan</button>
      </div>
    </div>
  </form>
</div>
</div>
</div>

<div id="modal-tematik" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="formTematik">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Form Register Tematik laporan</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-7 form-group form-message-dark">
            <label for="jenis_tematik">Nama Tematik</label>
            <input type="text" name="jenis_tematik" placeholder="Input Tematik" id="jenis_tematik" class="form-control border-round" autocomplete="off" required/>
          </div>
          <div class="col-md-3 form-group form-message-dark">
            <label for="switcher">Required</label>
            <label for="switcher" class="switcher switcher-success m-t-2">
              <input type="checkbox" id="switcher" name="required" checked>
              <div class="switcher-indicator">
                <div class="switcher-yes">YES</div>
                <div class="switcher-no">NO</div>
              </div>
            </label>
          </div>

          <div class="col-md-2 form-group form-message-dark">
            <label for="a">&nbsp;</label>
            <button type="submit" id="saveInstansi" class="btn btn-primary form-control border-round"><i class="ion-soup-can"></i> Simpan</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('js')
  <script type="text/javascript">
    $('#witel').select2({
      data:[{"id":"BANJARMASIN", "text":"BANJARMASIN"},{"id":"BALIKPAPAN", "text":"BALIKPAPAN"}],
      placeholder:"Select Witel",
      containerCssClass :'border-round'
    });
    $('#grouping').select2({
      data:<?= json_encode($group); ?>,
      placeholder:"Select Grouping",
      containerCssClass :'border-round'
    });
    $('#cc').select2({
      data:<?= json_encode($cc); ?>,
      placeholder:"Select CC Laporan",
      containerCssClass :'border-round'
    });
    var jl = $('#jenis_laporan').select2({
      placeholder:"Select Jenis Laporan atau register",
      containerCssClass :'border-round'
    });
    $('#setupform').pxValidate();

    $("#formTematik").submit(function(e){
      e.preventDefault();
      var formdata = new FormData(this);
      $.getJSON({
        url: "/formSaveTematik",
        type: "POST",
        data: formdata,
        contentType: false,
        cache: false,
        processData: false,
        success: function(e){
          console.log(e);
          var required = e.text;
          if(e.required){
            required = e.text+"(Required)";
          }else{
            required = e.text+"(Optional)";
          }
          console.log(required);
          var newState = new Option(required, e.id, true, true);
          // Append it to the select
          $("#jenis_laporan").append(newState).trigger('change');
          $('#modal-tematik').modal('toggle');
        },error: function(){
          alert("okey");
        }
      });
    });
  </script>

@endsection