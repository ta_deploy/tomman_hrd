@extends('layout_nologin')
@section('title')
List Setup Laporan Ring 3
@endsection
@section('content')
<div class="px-content">
    @include('Partial.alerts')
    <div class="panel box">
      <div class="box-row">
        <div class="box-container">
          <div class="box-cell p-y-1 p-x-1 font-weight-bold font-size-20">
            List TL/SM <a href="/formSetupLaporanRing3/new" class="btn btn-info btn-outline btn-rounded pull-right"><i class="ion ion-plus"></i>&nbsp;&nbsp;Register</a>
          </div>
        </div> <!-- / .box-container -->
      </div>
      <div class="box-row">
        <div class="box-container valign-middle">
          <div class="box-cell p-y-1 p-x-1">
            <div class="table-success">
              <div class="table-responsive table-primary">
                <table class="table" id="datatables">
                <thead>
                  <tr>
                    <th>Nik</th>
                    <th>Nama</th>
                    <th>Witel</th>
                    <th>Grouping</th>
                    <th>Set Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($reporter as $d)
                    <tr>
                      <td>{{ $d->nik }}</td>
                      <td>{{ $d->nama }}</td>
                      <td>{{ $d->witel }}</td>
                      <td>{{ $d->grouping }}</td>
                      <td>
                        @if(isset($d->payload) )
                          <a href="/hapusLaporanRing3/{{ $d->nik }}"><i class="ion ion-trash-a"></i>&nbsp;&nbsp;</a>
                        @endif
                        <a href="/formSetupLaporanRing3/{{ $d->nik }}">
                          @if(isset($d->payload) )
                            <span class="label label-success label-tag pull-right">&nbsp;OK</span>
                            @else
                            <span class="label label-danger label-tag pull-right">&nbsp;NOK</span>
                          @endif
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
          </div>

        </div> <!-- / .box-container -->
      </div>
      <div class="box-row">
        <div class="box-container">
          <div class="box-cell p-y-1 p-x-1 text-center font-weight-bold font-size-20">
            Grouping Preview
          </div>
        </div> <!-- / .box-container -->
      </div>
      <div class="box-row">
        <div class="box-container valign-middle">
          <div class="box-cell p-y-1 p-x-1">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Provisioning
                </div>
              </div>
              @if(isset($rg['PROVISIONING']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik</th>
                    <th>Nama</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($rg['PROVISIONING'] as $d)
                    <tr>
                      <td>{{ $d->nik }}</td>
                      <td>{{ $d->nama }}</td>
                      <td><a href="/formSetupLaporanRing3/{{ $d->nik }}">
                        @foreach(json_decode($d->payload) as $l)
                          <?php
                            if($laporan[$l]['required']=='on'){
                              $label = 'label-info';
                            }else{
                              $label = 'label-warning';
                            }
                          ?>
                          <span class="label {{$label}} label-tag pull-right">&nbsp;{{ str_replace('_',' ',(@$laporan[$l]['text']) ) }}</span>
                        @endforeach
                      </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
          </div>
          <div class="box-cell p-y-1 p-x-1 valign-top">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Assurance
                </div>
              </div>
              @if(isset($rg['ASSURANCE']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik</th>
                    <th>Nama</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($rg['ASSURANCE'] as $d)
                    <tr>
                      <td>{{ $d->nik }}</td>
                      <td>{{ $d->nama }}</td>
                      <td><a href="/formSetupLaporanRing3/{{ $d->nik }}">
                        @foreach(json_decode($d->payload) as $l)
                          <?php
                            if($laporan[$l]['required']=='on'){
                              $label = 'label-info';
                            }else{
                              $label = 'label-warning';
                            }
                          ?>
                          <span class="label {{$label}} label-tag pull-right">&nbsp;{{ str_replace('_',' ',(@$laporan[$l]['text']) ) }}</span>
                        @endforeach
                      </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
          </div>
        </div> <!-- / .box-container -->
      </div>
      <div class="box-row">
        <div class="box-container">
          <div class="box-cell p-y-1 p-x-1">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Corporate Service
                </div>
              </div>
              @if(isset($rg['CORPORATE SERVICE']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik</th>
                    <th>Nama</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($rg['CORPORATE SERVICE'] as $d)
                    <tr>
                      <td>{{ $d->nik }}</td>
                      <td>{{ $d->nama }}</td>
                      <td><a href="/formSetupLaporanRing3/{{ $d->nik }}">
                        @foreach(json_decode($d->payload) as $l)
                          <?php
                            if($laporan[$l]['required']=='on'){
                              $label = 'label-info';
                            }else{
                              $label = 'label-warning';
                            }
                          ?>
                          <span class="label {{$label}} label-tag pull-right">&nbsp;{{ str_replace('_',' ',(@$laporan[$l]['text']) ) }}</span>
                        @endforeach
                      </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
          </div>
          <div class="box-cell p-y-1 p-x-1 valign-top">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Support
                </div>
              </div>
              @if(isset($rg['SUPPORT']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik</th>
                    <th>Nama</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($rg['SUPPORT'] as $d)
                    <tr>
                      <td>{{ $d->nik }}</td>
                      <td>{{ $d->nama }}</td>
                      <td><a href="/formSetupLaporanRing3/{{ $d->nik }}">
                        @foreach(json_decode($d->payload) as $l)
                          <?php
                            if($laporan[$l]['required']){
                              $label = 'label-info';
                            }else{
                              $label = 'label-warning';
                            }
                          ?>
                          <span class="label {{$label}} label-tag pull-right">&nbsp;{{ str_replace('_',' ',(@$laporan[$l]['text']) ) }}</span>
                        @endforeach
                      </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
          </div>
        </div> <!-- / .box-container -->
      </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
  $('#datatables').dataTable();
});
</script>
@endsection