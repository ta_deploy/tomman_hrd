@extends('layout_nologin')
@section('title')
List Laporan Ring 3
@endsection
@section('content')
<div class="px-content">
    @include('Partial.alerts')
    <div class="panel box">
      <div class="box-row">
        <div class="box-container valign-middle">
          <div class="box-cell p-y-1 p-x-1">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Provisioning
                </div>
              </div>
              @if(isset($data['PROVISIONING']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['PROVISIONING'] as $d)
                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          @foreach(json_decode($d->payload) as $p)
                            <?php
                              if($laporan[$p]['required']){
                                $label = 'label-danger';
                              }else{
                                $label = 'label-warning';
                              }

                              if($d->jenis_laporan){
                                if (in_array($p, json_decode($d->jenis_laporan) ) ){
                                  $label = 'label-success';
                                }
                              }
                            ?>
                            <span class="label {{ $label }} label-tag pull-right">{{ str_replace('_',' ',(@$laporan[$p]['text']) ) }}</span>
                          @endforeach
                          <br/>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->tgl_update,11,5) }}</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
          </div>
          <div class="box-cell p-y-1 p-x-1 valign-top">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Assurance
                </div>
              </div>
              @if(isset($data['ASSURANCE']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['ASSURANCE'] as $d)

                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          @foreach(json_decode($d->payload) as $p)
                            <?php
                              if($laporan[$p]['required']){
                                $label = 'label-danger';
                              }else{
                                $label = 'label-warning';
                              }

                              if($d->jenis_laporan){
                                if (in_array($p, json_decode($d->jenis_laporan) ) ){
                                  $label = 'label-success';
                                }
                              }
                            ?>
                            <span class="label {{ $label }} label-tag pull-right">{{ str_replace('_',' ',(@$laporan[$p]['text']) ) }}</span>
                          @endforeach
                          <br/>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->tgl_update,11,5) }}</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
          </div>


          <div class="box-cell p-y-1 p-x-1 valign-top">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Corporate Service
                </div>
              </div>
              @if(isset($data['CORPORATE SERVICE']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['CORPORATE SERVICE'] as $d)
                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          @foreach(json_decode($d->payload) as $p)
                            <?php
                              if($laporan[$p]['required']){
                                $label = 'label-danger';
                              }else{
                                $label = 'label-warning';
                              }

                              if($d->jenis_laporan){
                                if (in_array($p, json_decode($d->jenis_laporan) ) ){
                                  $label = 'label-success';
                                }
                              }
                            ?>
                            <span class="label {{ $label }} label-tag pull-right">{{ str_replace('_',' ',(@$laporan[$p]['text']) ) }}</span>
                          @endforeach
                          <br/>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->tgl_update,11,5) }}</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>

            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Support
                </div>
              </div>
              @if(isset($data['SUPPORT']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['SUPPORT'] as $d)
                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          @foreach(json_decode($d->payload) as $p)
                            <?php
                              if($laporan[$p]['required']){
                                $label = 'label-danger';
                              }else{
                                $label = 'label-warning';
                              }

                              if($d->jenis_laporan){
                                if (in_array($p, json_decode($d->jenis_laporan) ) ){
                                  $label = 'label-success';
                                }
                              }
                            ?>
                            <span class="label {{ $label }} label-tag pull-right">{{ str_replace('_',' ',(@$laporan[$p]['text']) ) }}</span>
                          @endforeach
                          <br/>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->tgl_update,11,5) }}</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>

            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Deployment
                </div>
              </div>
              @if(isset($data['DEPLOYMENT']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['DEPLOYMENT'] as $d)
                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          @foreach(json_decode($d->payload) as $p)
                            <?php
                              if($laporan[$p]['required']){
                                $label = 'label-danger';
                              }else{
                                $label = 'label-warning';
                              }

                              if($d->jenis_laporan){
                                if (in_array($p, json_decode($d->jenis_laporan) ) ){
                                  $label = 'label-success';
                                }
                              }
                            ?>
                            <span class="label {{ $label }} label-tag pull-right">{{ str_replace('_',' ',(@$laporan[$p]['text']) ) }}</span>
                          @endforeach
                          <br/>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->tgl_update,11,5) }}</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>

          </div>
        </div> <!-- / .box-container -->
      </div>
    </div>
</div>
@endsection