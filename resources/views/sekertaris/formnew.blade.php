@extends('layout_nologin')
@section('title')
Laporan Ring 3
@endsection
@section('css')
  <style type="text/css">
    
    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
    .no-search .select2-search {
      display:none
    }
  </style>
@endsection
@section('content')
<div class="px-content">
	 @include('Partial.alerts')
   <form method="post" id="laporanring3" enctype="multipart/form-data">

      <div class="panel panel-color {{ @$data->tgl_update?'panel-success':'panel-danger' }} panel-border">
        <div class="panel-heading">
          <div class="panel-title">Laporan Ring 3</div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-4 form-group form-message-dark">
                <label for="nik">NIK</label>
                <input type="text" name="nik" id="nik" class="form-control border-round" autocomplete="off" placeholder="Input nik" value="{{ $data->nik or old('nik') }}" disabled/>
            </div>
            <div class="col-md-4 form-group form-message-dark">
                <label for="nama">Nama</label>
                <input type="text" name="nama" id="nama" class="form-control border-round" autocomplete="off" placeholder="Input Nama"  value="{{ $data->nama or old('nama') }}" disabled/>
            </div>
            <div class="col-md-4 form-group form-message-dark">
              <label for="sektor">Sektor</label>
              <input type="text" name="sektor" id="sektor" class="form-control border-round" autocomplete="off" value="{{ $data->sektor or old('sektor') }}" placeholder="Input Sektor (opsional)" disabled />
            </div>
          </div>
          <div class="row">
              <div class="col-md-4 form-group form-message-dark">
                <label for="posision">Position</label>
                <input type="text" name="posision" id="posision" class="form-control border-round" autocomplete="off" value="{{ $data->posision or old('posision') }}" placeholder="Input Position" disabled/>
              </div>
              <div class="col-md-4 form-group form-message-dark">
                  <label for="witel">Witel</label>
                  <input type="text" name="witel" id="witel" class="form-control border-round" autocomplete="off" value="{{ $data->witel or old('witel') }}" disabled/>
              </div>
              <div class="col-md-4 form-group form-message-dark">
                <label for="grouping">Grouping</label>
                <input type="text" name="grouping" id="grouping" class="form-control border-round" autocomplete="off" value="{{ $data->grouping or old('grouping') }}" disabled/>
              </div>
          </div>
                
          <div class="row">
            @foreach($poto as $input)
              <?php
              // dd($poto);
                if($laporan[$input]['required']){
                  $label = 'label-info';
                  $textnya = 'Required';
                  $requirenya = 'required';
                }else{
                  $label = 'label-warning';
                  $textnya = 'Opsional';
                  $requirenya = '';
                }
              ?>
              <div class="form-group col-xs-6 col-sm-6 col-md-6 input-photos form-message-dark" >
                <label for="{{ $input }}" class="control-label">{{ str_replace('_',' ',@$laporan[$input]['text'])  }} <span class="label label-pill font-size-11 {{ $label }}">{{ $textnya }}</span></label>
                <label id="{{$input}}-label" class="custom-file px-file border-round" for="{{ $input }}">
                  <input type="file" id="{{ $input }}" {{$requirenya}} name="{{ $input }}" class="custom-file-input border-round">
                  <span class="custom-file-control form-control border-round">Choose JPG...</span>
                  <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                  </div>
                </label>
              </div>
            @endforeach
          </div>
          <div class="row">
            <div class="col-sm-12">
              <button class="btn {{ @$data->tgl_update?'btn-primary':'btn-danger' }} btn-lg btn-block btn-rounded m-t-4" type="submit">
                <span class="fa fa-cloud-upload"></span>
                <span>Lapor</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </form> 

</div>
@endsection
@section('js')
  <script type="text/javascript">
    $(function() {

      $('.px-file').pxFile();
      $('#laporanring3').pxValidate();
    });
  </script>

@endsection