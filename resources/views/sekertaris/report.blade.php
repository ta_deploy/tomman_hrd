@extends('layout_nologin')
@section('title')
Report MTTR
@endsection
@section('content')
<div class="px-content">
    @include('Partial.alerts')

    <?php
      $date = date('Y-m-01');
      $terhitung_date = date('Y-m-d');
      $datetime1 = new DateTime($date);
      $datetime2 = new DateTime($terhitung_date);
      $interval = $datetime1->diff($datetime2);
      $count_wajib = $interval->format('%d');
    ?>
    <div class="panel box">
      <div class="box-row">
        <div class="box-container">
          <div class="box-cell p-y-1 p-x-1 text-center font-weight-bold font-size-20">
            MTTR (Mean Time To Report) Leaders
          </div>
        </div> <!-- / .box-container -->
      </div>
      <div class="box-row">
        <div class="box-container">
          <div class="box-cell p-y-1 p-x-1">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Provisioning
                </div>
              </div>
              @if(isset($data['PROVISIONING']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>AVG(time) Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['PROVISIONING'] as $d)
                      <tr class="font-weight-bold">
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->avg_tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->avg_tgl_update,0,5) }}</span><br/>
                          <span class="label label-info">Count Lapor</span>
                          <span class="label label-{{ $d->count_lapor<$count_wajib?'danger':'success' }}">{{ $d->count_lapor }}X</span> /
                          <span class="label label-success">{{ $count_wajib }} Hari</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
          </div>
          <div class="box-cell p-y-1 p-x-1 valign-top">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Assurance
                </div>
              </div>
              @if(isset($data['ASSURANCE']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>AVG(time) Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['ASSURANCE'] as $d)
                      <tr class="font-weight-bold">
                        <td>{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td>{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->avg_tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->avg_tgl_update,0,5) }}</span><br/>
                          <span class="label label-info">Count Lapor</span>
                          <span class="label label-{{ $d->count_lapor<$count_wajib?'danger':'success' }}">{{ $d->count_lapor }}X</span> /
                          <span class="label label-success">{{ $count_wajib }} Hari</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
          </div>


          <div class="box-cell p-y-1 p-x-1">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Corporate Service
                </div>
              </div>
              @if(isset($data['CORPORATE SERVICE']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>AVG(time) Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['CORPORATE SERVICE'] as $d)
                      <tr class="font-weight-bold">
                        <td>{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td>{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->avg_tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->avg_tgl_update,0,5) }}</span><br/>
                          <span class="label label-info">Count Lapor</span>
                          <span class="label label-{{ $d->count_lapor<$count_wajib?'danger':'success' }}">{{ $d->count_lapor }}X</span> /
                          <span class="label label-success">{{ $count_wajib }} Hari</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>

            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Support
                </div>
              </div>
              @if(isset($data['SUPPORT']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>AVG(time) Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['SUPPORT'] as $d)
                      <tr class="font-weight-bold">
                        <td>{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td>{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->avg_tgl_update?'show':'hide' }}">&nbsp;{{ substr($d->avg_tgl_update,0,5) }}</span><br/>

                          <span class="label label-info">Count Lapor</span>
                          <span class="label label-{{ $d->count_lapor<$count_wajib?'danger':'success' }}">{{ $d->count_lapor }}X</span> /
                          <span class="label label-success">{{ $count_wajib }} Hari</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>

            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  Deployment
                </div>
              </div>
              @if(isset($data['DEPLOYMENT']) )
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>AVG(time) Laporan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['DEPLOYMENT'] as $d)
                      <tr class="font-weight-bold">
                        <td>{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td>{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->avg_tgl_update?'show':'hide' }}">&nbsp;{{ substr($d->avg_tgl_update,0,5) }}</span><br/>

                          <span class="label label-info">Count Lapor</span>
                          <span class="label label-{{ $d->count_lapor<$count_wajib?'danger':'success' }}">{{ $d->count_lapor }}X</span> /
                          <span class="label label-success">{{ $count_wajib }} Hari</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
          </div>
        </div> <!-- / .box-container -->
      </div>
    </div>
</div>
@endsection