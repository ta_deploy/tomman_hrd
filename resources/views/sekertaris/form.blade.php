@extends('layout_nologin')
@section('title')
Laporan Ring 3
@endsection
@section('css')
  <style type="text/css">
    
    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
    .no-search .select2-search {
      display:none
    }
  </style>
@endsection
@section('content')
<div class="px-content">
	 @include('Partial.alerts')
   <form method="post" id="laporanring3" class="form-horizontal validate" enctype="multipart/form-data">
      <div class="col-md-12"> 
      <div class="panel panel-color panel-success panel-border">
        <div class="panel-heading">
          <div class="panel-title">Laporan Ring 3</div>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <div class="col-sm-6">
              <label class="control-label" for="nama">Nik</label>
              <input name="nik" class="form-control border-round" id="nik" required/>
            </div>
            <div class="col-sm-6">
              <label class="control-label" for="nama">Position</label>
              <input name="position" class="form-control border-round" id="position" required/>
            </div>
          </div>
          
          <div class="form-group">
            @foreach($poto as $input)
              <?php
                $photoclass = '';
                foreach($class[$input] as $c){
                  $photoclass.= ' '.$c;
                }



              ?>
              <div class="col-xs-6 col-sm-6 col-md-6 input-photos {{ $photoclass }}" >
                
                <label for="{{$input}}" class="control-label">{{ str_replace('_',' ',$input)  }}</label>
                <label id="{{$input}}-label" class="custom-file px-file  border-round" for="{{$input}}">
                  <input type="file" id="{{$input}}" name="{{$input}}" class="custom-file-input {{ $photoclass }} border-round" required="true">
                  <span class="custom-file-control form-control border-round">Choose JPG...</span>
                  <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                  </div>
                </label>
                
              </div>
            @endforeach

          </div>
          <div class="form-group">  
            <div class=" col-sm-2">
              <button class="btn bt-block btn-primary" type="submit">
                <span class="fa fa-cloud-download"></span>
                <span>Lapor</span>
              </button>
            </div>
          </div>
      </div>
    </form> 

</div>
@endsection
@section('js')
  <script type="text/javascript">
    $('.input-photos').hide();
    $('#position').select2({
      data:[{"id":"TL_Assurance", "text":"TL Assurance"},{"id":"TL_Hdesk_Assurance", "text":"TL Hdesk Assurance"},{"id":"TL_Provisioning", "text":"TL Provisioning"},{"id":"SM_Provisioning", "text":"SM Provisioning"},{"id":"Team_Leader_BGES", "text":"Team Leader BGES"},{"id":"Team_Leader_MO_SPBU", "text":"Team Leader MO SPBU"},{"id":"Team_Leader_TSEL", "text":"Team Leader TSEL"},{"id":"Site_Manager_Helpdesk", "text":"Site Manager Helpdesk"},{"id":"Team_Leader_Inventory_Asset_Management", "text":"Team Leader Inventory & Asset Management"}],
      placeholder:"select Position",
      containerCssClass :'border-round'
    });
    $('#position').change(function(){
      $('.input-photos').hide();
      $('.'+this.value).show();
      $('.custom-file-input').removeAttr('required');
      $('.'+this.value).attr('required','true');

    })
    $('.input-photos').on('click', 'button', function() {
      $(this).parent().find('input[type=file]').click();
    });
    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);
            
          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });
    $('.px-file').pxFile();
    $('#laporanring3').pxValidate();
  </script>

@endsection