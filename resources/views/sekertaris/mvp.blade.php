@extends('layout_nologin')
@section('title')
MVP Laporan Ring 3
@endsection
@section('content')
<div class="px-content">
    @include('Partial.alerts')
    <button id="daterange-4" class="btn dropdown-toggle pull-right m-b-2"></button>
    @if(count($data) )
      <?php
        if(null !==Request::segment(2) ){
          $date = explode(':', Request::segment(2) )[0] ." 00:00:01";
          $terhitung_date = explode(':', Request::segment(2) )[1] ." 23:59:59";
        }else{
          $date = date('Y-m-d 00:00:01',strtotime("monday last week") );
          $terhitung_date   =  date('Y-m-d 23:59:59',strtotime("monday this week - 1 second") );
        }

        $datetime1 = new DateTime($date);
        $datetime2 = new DateTime($terhitung_date);
        $interval = $datetime1->diff($datetime2);
        $count_wajib = $interval->format('%d');
      ?>
    <div class="panel box">
      <div class="box-row">
        <div class="box-container valign-middle">
          <div class="box-cell p-y-1 p-x-1">
            <div class="table-success">
              <div class="table-header">
                <div class="table-caption text-center">
                  AVG Laporan Tercepat terhitung dari {{$date}} sampai {{$terhitung_date}}
                </div>
              </div>

              <input type="hidden" name="start" id="start" value="{{ $date }}">
              <input type="hidden" name="end" id="end" value="{{ $terhitung_date }}">
              <table class="table">
                <thead>
                  <tr>
                    <th>Nik/Witel</th>
                    <th>Nama/Position</th>
                    <th>Jam</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $d)
                      <tr>
                        <td class="font-weight-bold">{{ $d->nik }}<br/>{{ $d->witel }}</td>
                        <td class="font-weight-bold">{{ $d->nama }}<br/>{{ $d->posision }}</td>
                        <td>
                          <span class="ion ion-clock font-size-20 font-weight-bold {{ $d->avg_tgl_update?'show':'hide' }} pull-right">&nbsp;{{ substr($d->avg_tgl_update,0,5) }}</span><br/>
                          <span class="label label-info">Count Lapor</span>
                          <span class="label label-{{ $d->count_lapor<$count_wajib?'danger':'success' }}">{{ $d->count_lapor }}X</span> /
                          <span class="label label-success">{{ $count_wajib+1 }} Hari</span>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>

            </div>
          </div>

        </div> <!-- / .box-container -->
      </div>
    </div>
    @endif
</div>
@endsection
@section('js')
  <script type="text/javascript">
    $(function() {
      var start = moment($('#start').val() );
      var end = moment($('#end').val() );

      function cb(start, end) {
        $('#daterange-4').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY') );
      }

      $('#daterange-4').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
         'Hari Ini': [moment(), moment()],
         'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
         '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
         'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
         'Bulan Sebelumnya': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);

      cb(start, end);
      $('#daterange-4').on('apply.daterangepicker', function(ev, picker) {
          var s = picker.startDate.format('YYYY-MM-DD');
          var e = picker.endDate.format('YYYY-MM-DD');
          var mitrapid = $(location).attr('href').split("/")[5]+'/'+$(location).attr('href').split("/")[6];
          // alert()
          window.location.href = document.location.origin+"/mvpLaporanRing3/"+s+":"+e;
      });
    });
  </script>
@endsection