@extends('layout_nologin')
@section('title')
Laporan Ring 3
@endsection
@section('css')
  <style type="text/css">
  </style>
@endsection
@section('content')
<!-- ion-trophy
ion-ios-star
ion-arrow-graph-up-right
ion-arrow-graph-down-right
 -->
<div class="px-content">
  <select class="form-control" id="witel_select2" name="witel_select2" style="width: 100%;" required>
    <option value="all">Semua</option>
    @foreach ($witel as $v)
      <option value="{{ $v }}">{{ $v }}</option>
    @endforeach
  </select>
  <div class="panel panel-info panel-dark widget-profile widget-profile-centered" style="margin-top: 15px;">
    <div class="panel-heading">
      <img src="/image/trophy_provisioning.png" alt="" height="300px">
      <h3 style="color:black;">
        LEADERBOARD PROVISIONING KALIMANTAN 1 PERIODE {{ $tgl_param }} {{-- <br/><br/> LAST UPDATE {{ $tgl  }} --}}
      </h3>
      <i class="widget-profile-bg-icon ion-arrow-graph-up-right"></i>
      <div class="widget-profile-counters">
        <a href="#" class="col-xs-2">
          <span class="widget-profile-counter">SEKTOR</span>
        </a>
        <a href="#" class="col-xs-2">
          <span class="widget-profile-counter">WITEL</span>
        </a>
        <a href="#" class="col-xs-1">
          <span class="widget-profile-counter">KPI</span>
        </a>
        <a href="#" class="col-xs-1">
          <span class="widget-profile-counter">PS/RE</span>
        </a>
        <a href="#" class="col-xs-4">
          <span class="widget-profile-counter">RESULT</span>
        </a>
        <a href="#" class="col-xs-2">
          <span class="widget-profile-counter">SUMMARY</span>
        </a>
      </div>
    </div>
    <div class='content_isi'></div>
  </div>
</div>
<div class="modal" id="modal-base">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
  <script type="text/javascript">
    $('#witel_select2').select2({
      placeholder: 'Pilih Witel',
    });

    $('#witel_select2').on('select2:select change', function(){
      var isi = $(this).val();
      $.ajax({
        type: "GET",
        url: '/jx_prov_get',
        data: {
          witel: isi,
          tgl: {!! json_encode($tgl_param) !!}
        }
      }).done( function(e){
        $('.content_isi').html(e);
      });
    });
    $('#witel_select2').val('all').change();

    $('#modal-base').on('shown.bs.modal', function (e) {
      console.log($(e.relatedTarget).data('sektor') );
      $(".modal-title").html($(e.relatedTarget).data('sektor')+" TAHUN "+$(e.relatedTarget).data('tahun') );
      $.ajax({
        type: "GET",
        url: '/detail_leaderboardprov/'+$(e.relatedTarget).data('sektor')+'/'+$(e.relatedTarget).data('tahun')+'/'+$(e.relatedTarget).data('tgl_load')
      }).done( function(r){
        $('.modal-body').html(r);
      });
    });
  </script>
@endsection