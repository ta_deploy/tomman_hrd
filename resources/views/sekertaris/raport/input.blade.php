@extends('layout_nologin')
@section('title')
input
@endsection
@section('css')
  <style type="text/css">
  </style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
  <div class="panel panel-color panel-success panel-border">
    <div class="panel-heading">
      <div class="panel-title">Input KPI MANUAL</div>
    </div>
    <div class="panel-body">
      <form method="post">
        <div class="form-group">
          <label for="paste">TGL DATA</label>
          <div class="input-group col-sm-12">

            <input name="tgl_data" class="form-control border-round" id="tgl_data" placeholder="Input tgl_data ex:YYYY-MM" />
          </div>  
        </div>
        <div class="form-group">
          <label for="paste">PASTE</label>
          <div class="input-group col-sm-12">

            <textarea name="paste" class="form-control" id="paste" placeholder="PASTE. pastikan kolom terblock sesuai standart."></textarea>
            <div class="input-group-btn">
              <button type="submit" class="btn btn-success border-round" id="btnnext">save</button>
            </div>
          </div>  
        </div>
        
      </form>
    </div>
  </div>
</div>
@endsection
@section('js')

@endsection