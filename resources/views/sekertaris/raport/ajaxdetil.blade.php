<div class="table-light">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th rowspan="2" class="text-center valign-middle">KPI</th>
        <th colspan="12" class="text-center">BULAN</th>
      </tr>
      <tr>
        <th class="text-center">1</th>
        <th class="text-center">2</th>
        <th class="text-center">3</th>
        <th class="text-center">4</th>
        <th class="text-center">5</th>
        <th class="text-center">6</th>
        <th class="text-center">7</th>
        <th class="text-center">8</th>
        <th class="text-center">9</th>
        <th class="text-center">10</th>
        <th class="text-center">11</th>
        <th class="text-center">12</th>
      </tr>
    </thead>
    <tbody>
        @foreach($kpi as $key=> $k)
        @if($key)
      <tr>
        <td>{{ $key }}</td>
        @foreach($bln as $b=>$bl)
        <td class="text-right">{{ $k[$b] }}</td>
        <?php
          $bln[$b]+=$k[$b];
        ?>
        @endforeach
      </tr>
      @endif
        @endforeach
      <tr style="background-color: rgba(224,224,224,.05);">
        <td class="text-center">TOTAL</td>
        @foreach($total as $b)
          <td class="text-right">{{ $b }}</td>
        @endforeach
      </tr>
      <tr style="background-color: rgba(224,224,224,.05);" class="text-center">
        <td>RESULT</td>
        <?php
          $sum_tropy = $sum_bintang = $sum_skull = $sum_quest = 0;
          foreach($result as $b)
          {
            if($b === 2){
              echo "<td>🏆</td>";
              $sum_tropy++;
            }else if($b === 1){
              echo "<td>⭐</td>";
              $sum_bintang++;
            }else if($b === 0){
              echo "<td>💀</td>";
              $sum_skull++;
            }else{
              echo "<td>❓</td>";
              $sum_quest++;
            }
          }
        ?>
      </tr>
      <tr style="background-color: rgba(224,224,224,.05);" class="text-center">
        <td>SUMMARY</td>
        <td colspan="12">🏆{{ $sum_tropy }} ⭐{{ $sum_bintang }} 💀{{ $sum_skull }} ❓{{ $sum_quest }}</td>
      </tr>
    </tbody>
  </table>
</div>