@extends('layout_nologin')
@section('title')
Laporan Ring 3
@endsection
@section('css')
  <style type="text/css">
  </style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
  <div class="panel panel-color panel-success panel-border">
    <div class="panel-heading">
      <div class="panel-title">Laporan Ring 3</div>
    </div>
    <div class="panel-body">
      <div class="form-group">
        <label for="nik">NIK</label>
        <div class="input-group col-sm-6">

          <input name="nik" class="form-control border-round" id="nik" placeholder="Input NIK" />
          <div class="input-group-btn">
            <button type="button" class="btn btn-success border-round" id="btnnext">Next Form</button>
          </div>
        </div>  
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
  <script type="text/javascript">
    $('#btnnext').on('click', function() {
      window.location.href = $(location).attr('href')+'/'+$('#nik').val();
    });
  </script>

@endsection