@extends('layout')
@section('title')
List Reward
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<a href="/hr/new_arc/input" type="button" class="btn btn-primary btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Reward</a>
  <div class="page-header">
		<div class="table-info">
			<table class="table table-bordered table-striped table-small-font" id="table_ku">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th>NIK</th>
						<th>Nama</th>
						<th>Tanggal</th>
						<th class="no-sort">Reward / Inovasi</th>
						<th class="no-sort">Dokumen</th>
					</tr>
				</thead>
				<tbody class="middle-align">
					@foreach ($data as $key => $d)
						<tr>
							<td>{{ ++$key }}</td>
							<td>{{ $d->id_nik }}</td>
							<td>{{ $d->nama }}</td>
							<td>{{ $d->tgl }}</td>
							<td>{{ $d->title_archive }}</td>
							<td><a href="/download/file_reward/{{ $d->id }}/{{ $d->people_id }}">tes</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});

			$('#table_ku_wrapper .table-caption').text('List Penghargagan');
			$('#table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection