@extends('layout')
@section('title')
Input Penghargaan
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="panel-group">	
				<div class="panel panel-color panel-info panel-border">
					<div class="panel-heading">
						<div class="panel-title">Form Pengisian Reward</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="nik">Nik :</label>
							<div class="col-md-10">
								<select name="nik[]" class="form-control" id="nik" multiple></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="tgl">Tanggal :</label>
							<div class="col-md-10">
								<input type="text" name="tgl" class="form-control" id="tgl">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="title_archive">Judul Reward / Inovasi :</label>
							<div class="col-md-10">
								<input type="text" name="title_archive" class="form-control" id="title_archive">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="berkas">Berkas :</label>
							<div class="col-md-10">
								<input type="file" name="berkas" class="form-control" id="berkas">
							</div>
						</div>
						<div class="form-group">
							<div class=" col-sm-12">
								<button class="btn btn-block btn-primary" type="submit">
									<span class="fa fa-cloud-download"></span>
									<span>Simpan</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>	
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			$('#nik').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: { 
						url: "/jx_dng/get_user",
						dataType: 'json',
						delay: 250,
						data: function (params) {
								return {
										searchTerm: params.term
								};
						},
						processResults: function (response) {
								return {
										results: response
								};
						},
						language: {
								noResults: function(){
										return "Nama Karyawan / KK / Nik Sementara Tidak Terdaftar!";
								}
						},
						cache: true,
						success: function(value) {
						}
				}
			});

			$('#tgl').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',
        daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',

        beforeShowMonth: function(date) {
          if (date.getMonth() === 8) {
            return false;
          }
        },

        beforeShowYear: function(date){
          if (date.getFullYear() === 2014) {
            return false;
          }
        }
      });
		});
	</script>
@endsection