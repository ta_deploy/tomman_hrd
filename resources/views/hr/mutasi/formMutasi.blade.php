@extends('layout')
@section('title')
Form Mutasi
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate" enctype="multipart/form-data">
			<div class="col-md-12">
				<div class="panel panel-color panel-info panel-border">
					<div class="panel-heading">
						<div class="panel-title title_kar">Input Mutasi Karyawan</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="tgl">Tanggal Pergantian :</label>
							<div class="col-md-10">
								<input type="text" name="tgl" class="form-control" id="tgl">
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label" for="nik_atasan">Nik Atasan :</label>
								<input type="text" readonly name="nik_atasan" class="form-control" id="nik_atasan">
							</div>
							<div class="detail_data_nik"></div>
							<div class="col-md-6">
								<label class="control-label" for="nama">Nik Karyawan :</label>
								<select name="nama" class="form-control" id="nama"></select>
							</div>
							<div class="detail_data_nik"></div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="jenis">Jenis :</label>
							<div class="col-sm-10">
								<select class="form-control" name="jenis" id="jenis">
									<option value="Mutasi">Mutasi</option>
									<option value="Demosi">Demosi</option>
									<option value="Promosi">Promosi</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-color panel-primary panel-border">
					<div class="panel-heading">
						<div class="panel-title">Posisi Baru</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="witel">Witel :</label>
							<div class="col-md-10">
								<select name="witel" class="form-control" id="witel"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sto">STO :</label>
							<div class="col-md-10">
								<select name="sto" class="form-control" id="sto"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="jabatan">Jabatan :</label>
							<div class="col-md-10">
								<select name="jabatan" class="form-control" id="jabatan"></select>
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label" for="unit">Unit :</label>
								<input type="text" name="unit" disabled class="form-control" id="unit">
							</div>
							<div class="col-sm-6">
								<label class="control-label" for="sub_unit">Sub Unit :</label>
								<input type="text" name="sub_unit" disabled class="form-control" id="sub_unit">
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label" for="group">Group :</label>
								<input type="text" name="group" disabled class="form-control" id="group">
							</div>
							<div class="col-sm-4">
								<label class="control-label" for="sub_grup">Sub Group :</label>
								<input type="text" name="sub_grup" disabled class="form-control" id="sub_grup">
							</div>
							<div class="col-md-4">
								<label class="control-label" for="grup_fun">Group Fungsi :</label>
								<input type="text" name="grup_fun" disabled class="form-control" id="grup_fun">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="bizpart">Bizpart ID :</label>
							<div class="col-md-9">
								<input type="text" name="bizpart" disabled class="form-control" id="bizpart">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="direktorat">Direktorat :</label>
							<div class="col-md-9">
								<input type="text" name="direktorat" disabled class="form-control" id="direktorat">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-color panel-default panel-border">
					<div class="panel-heading">
						<div class="panel-title">Posisi Lama</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="prev_witel">Witel :</label>
							<div class="col-md-10">
								<select name="prev_witel" class="form-control" id="prev_witel" readonly></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="prev_sto">STO :</label>
							<div class="col-md-10">
								<select name="prev_sto" class="form-control" id="prev_sto" readonly></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="prev_jabatan">Jabatan :</label>
							<div class="col-md-10">
								<select name="prev_jabatan" class="form-control" id="prev_jabatan" readonly></select>
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label" for="prev_unit">Unit :</label>
								<input type="text" name="prev_unit" class="form-control" readonly id="prev_unit">
							</div>
							<div class="col-md-6">
								<label class="control-label" for="prev_sub_unit">Sub Unit :</label>
								<input type="text" name="prev_sub_unit" class="form-control" readonly id="prev_sub_unit">
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-4">
								<label class="control-label" for="prev_group">Group :</label>
								<input type="text" name="prev_group" class="form-control" readonly id="prev_group">
							</div>
							<div class="col-md-4">
								<label class="control-label" for="prev_sub_grup">Sub Group :</label>
								<input type="text" name="prev_sub_grup" class="form-control" readonly id="prev_sub_grup">
							</div>
							<div class="col-md-4">
								<label class="control-label" for="prev_grup_fun">Group Fungsi :</label>
								<input type="text" name="prev_grup_fun" class="form-control" readonly id="prev_grup_fun">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="prev_bizpart">Bizpart ID :</label>
							<div class="col-md-9">
								<input type="text" name="prev_bizpart" class="form-control" readonly id="prev_bizpart">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="prev_direktorat">Direktorat :</label>
							<div class="col-md-9">
								<input type="text" name="prev_direktorat" class="form-control" readonly id="prev_direktorat">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-color panel-warning panel-border">
					<div class="panel-heading">
						<div class="panel-title">Tanda Tangan</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-6">
								<label class="control-label" for="create_by">Dibuat Oleh :</label>
								<select name="create_by" class="form-control" id="create_by"></select>
							</div>
							<div class="col-sm-6">
								<label class="control-label" for="created_position">Jabatan :</label>
								<input type="text" name="created_position" class="form-control" disabled id="created_position">
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label" for="approve_by">Disetujui Oleh :</label>
								<select name="approve_by" class="form-control" id="approve_by"></select>
							</div>
							<div class="col-md-6">
								<label class="control-label" for="approve_position">Jabatan :</label>
								<input type="text" name="approve_position" class="form-control" disabled id="approve_position">
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label" for="known_by">Diketahui Oleh :</label>
								<select name="known_by" class="form-control" id="known_by"></select>
							</div>
							<div class="col-md-6">
								<label class="control-label" for="known_position">Jabatan :</label>
								<input type="text" name="known_position" class="form-control" disabled id="known_position">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			$('#tgl').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',
        daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',

        beforeShowMonth: function(date) {
          if (date.getMonth() === 8) {
            return false;
          }
        },

        beforeShowYear: function(date){
          if (date.getFullYear() === 2014) {
            return false;
          }
        }
      });

			$('#nama').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: {
						url: "/jx_dng/get_user",
						dataType: 'json',
						delay: 250,
						data: function (params) {
								return {
										searchTerm: params.term
								};
						},
						processResults: function (response) {
								return {
										results: response
								};
						},
						language: {
								noResults: function(){
										return "Nama Karyawan / KK / Nik Sementara Tidak Terdaftar!";
								}
						},
						cache: true,
						success: function(value) {
						}
				}
			});

			$('#witel').select2({
				width: '100%',
				placeholder: "Ketik Nama Witel",
				allowClear: true,
				ajax: {
					url: '/jx_dng/get_witel',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('#create_by').select2({
				width: '100%',
				placeholder: "Ketik Nama Pembuat",
				minimumInputLength: 7,
				allowClear: true,
				ajax: {
						url: "/jx_dng/get_user",
						dataType: 'json',
						delay: 250,
						data: function (params) {
								return {
										searchTerm: params.term
								};
						},
						processResults: function (response) {
								return {
										results: response
								};
						},
						language: {
								noResults: function(){
										return "Nama Karyawan / KK / Nik Sementara Tidak Terdaftar!";
								}
						},
						cache: true,
						success: function(value) {
						}
				}
			});

			$('#approve_by').select2({
				width: '100%',
				placeholder: "Ketik Nama Penyetuju",
				minimumInputLength: 7,
				allowClear: true,
				ajax: {
						url: "/jx_dng/get_user",
						dataType: 'json',
						delay: 250,
						data: function (params) {
								return {
										searchTerm: params.term
								};
						},
						processResults: function (response) {
								return {
										results: response
								};
						},
						language: {
								noResults: function(){
										return "Nama Karyawan / KK / Nik Sementara Tidak Terdaftar!";
								}
						},
						cache: true,
						success: function(value) {
						}
				}
			});

			$('#known_by').select2({
				width: '100%',
				placeholder: "Ketik Nama Saksi",
				minimumInputLength: 7,
				allowClear: true,
				ajax: {
						url: "/jx_dng/get_user",
						dataType: 'json',
						delay: 250,
						data: function (params) {
								return {
										searchTerm: params.term
								};
						},
						processResults: function (response) {
								return {
										results: response
								};
						},
						language: {
								noResults: function(){
										return "Nama Karyawan / KK / Nik Sementara Tidak Terdaftar!";
								}
						},
						cache: true,
						success: function(value) {
						}
				}
			});

			var sto = $('#sto').select2({
				width: '100%',
				placeholder: "Pilih STO",
			}),
			jabatan = $('#jabatan').select2({
				width: '100%',
				placeholder: "Pilih Jabatan",
			});


			$('#witel').on("select2:select change", function(e) {
				$('#sto').empty().change();
				sto.select2({
					width: '100%',
					placeholder: "Pilih STO",
					ajax: {
					url: "/jx_dng/get_sto_mut",
					dataType: 'json',
					delay: 50,
					data: function (params) {
						return {
							searchTerm: params.term,
							witel: $('#witel').val()
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Jabatan Tidak Terdaftar!";
						}
					},
					cache: true,
				}
				});
			});

			$('#sto').on("select2:select change", function(e) {
				$('#jabatan').empty().change();
				jabatan.select2({
					width: '100%',
					placeholder: "Pilih Jabatan",
					minimumInputLength: 1,
					ajax: {
					url: "/jx_dng/get_jabatan",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term,
							witel: $('#witel').val()
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Jabatan Tidak Terdaftar!";
						}
					},
					cache: true,
				}
				});
			});

			$('#jabatan').on('select2:select change', function(e){
					$.ajax({
						type: 'GET',
						url: '/jx_dng/get_posisi',
						dataType: 'JSON',
						data: {
							data: $(this).val()
						}
					}).done( function(e){
						$('#unit').val(e.unit);
						$('#sub_unit').val(e.sub_unit);
						$('#group').val(e.grup);
						$('#sub_grup').val(e.sub_grup);
						$('#grup_fun').val(e.fungsi_grup);
						$('#bizpart').val(e.bizpart_id);
						$('#direktorat').val(e.direktorat);
					})
			});

			$('#witel').append('<option value=1 selected>KALSEL</option>').trigger('change');

			$('#nama').on('select2:select change', function(){
				$.ajax({
					type: 'GET',
					url: '/jx_dng/get_user_mutasi',
					data: {
						data: $(this).val()
					},
					dataType: 'JSON'
				}).done( function(e){
					console.log(e)
					$('#prev_unit').val(e.p_unit ?? 'Kosong');
					$('#prev_sub_unit').val(e.p_sub_unit ?? 'Kosong');
					$('#prev_group').val(e.p_grup ?? 'Kosong');
					$('#prev_sub_grup').val(e.p_sub_grup ?? 'Kosong');
					$('#prev_grup_fun').val(e.p_fungsi_grup ?? 'Kosong');
					$('#prev_bizpart').val(e.p_bizpart_id ?? 'Kosong');
					$("#prev_witel, #prev_sto, #prev_jabatan").empty().change();
					$('#prev_witel').append(new Option(e.p_nama_witel ?? 'Kosong', e.p_nama_witel_id ?? 'Kosong', true, true) ).trigger('change');
					$('#prev_sto').append(new Option(e.p_kode_area ?? 'Kosong', e.p_kode_area_id ?? 'Kosong', true, true) ).trigger('change');
					$('#prev_jabatan').append(new Option(e.p_direktorat ?? 'Kosong', e.p_direktorat_id ?? 'Kosong', true, true) ).trigger('change');
					$('#prev_direktorat').val(e.p_direktorat ?? 'Kosong');
				});
			});

		});
	</script>
@endsection