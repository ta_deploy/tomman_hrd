@extends('layout')

@section('html-title')
	 HR :: TOMMAN
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Project : 
@endsection

@section('page-title')
	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
		<li><i class="linecons-calendar"></i>HR</li>
		<li class="active"><i class="linecons-tag">Detail Pelurusan Data Position Name</i> </li>
	</ol>
@endsection

@section('body')
<input name="id" type="hidden"  value="{{ $jobbaru->id or ''}}" />	
	<div class="col-md-12">	
	<div class="panel panel-color panel-blue">
		<div class="panel-heading">
			<div class="panel-title">{{$jobbaru->jenis}} Atas Nama : {{$jobbaru->nama}} - NIK : {{$jobbaru->nik}} - Status : <?php if ($jobbaru->status_pengajuan=='2') {echo" Disetujui";}
												else {echo "Tidak Disetujui";}?></div>
		</div>
	</div>
	</div>			
		
	<div class="col-md-6">
	<div class="panel panel-color panel-warning panel-border">
		<div class="panel-heading">
			<div class="panel-title">Position Name Lama</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-objectid">Object ID</label>
						<input name="objectid" type="text" id="objectid" class="form-control" value="{{ $joblama->object_id}}" disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="position">Position Name</label>
						<input name="position" type="text" id="position" class="form-control " value="{{ $joblama->position_name}}" disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="witel">Witel</label>
						<input name="witel" type="text" id="witel" class="form-control" value="{{ $joblama->witel }}" disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="input-psa">PSA</label>
						<input name="psa" type="text" id="psa" class="form-control " value="{{ $joblama->loker }}" disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-regional">Regional</label>
						<input name="regional" type="text" id="regional" class="form-control" value="{{ $joblama->regional}}"disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="bizpart">Bizpart ID</label>
						<input name="bizpart" type="text" id="bizpart" class="form-control" value="{{ $joblama->bizpart_id}}"disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-direktorat">Direktorat</label>
						<input name="direktorat" type="text" id="direktorat" class="form-control" value="{{ $joblama->direktorat }}" disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="unit">Unit</label>
						<input name="unit" type="text" id="unit" class="form-control " value="{{ $joblama->unit }}" disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-subunit">Sub Unit</label>
						<input name="subunit" type="text" id="subunit" class="form-control " value="{{ $joblama->sub_unit}}" disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="group">Group</label>
						<input name="group" type="text" id="group" class="form-control " value="{{ $joblama->group }}" disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-subgroup">Sub Group</label>
						<input name="subgroup" type="text" id="subgroup" class="form-control " value="{{ $joblama->sub_group}}"disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="groupfungsi">Group Fungsi</label>
						<input name="groupfungsi" type="text" id="groupfungsi" class="form-control " value="{{ $joblama->group_fungsi }}" disabled/>
					</div>
				</div>
		</div>
	</div>
	</div>
	<div class="col-md-6">
	<div class="panel panel-color panel-success panel-border">
		<div class="panel-heading">
			<div class="panel-title">Position Name Baru</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-objectid">Object ID</label>
						<input name="objectid" type="text" id="objectid" class="form-control" value="{{ $jobbaru->object_id}}" disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="position">Position Name</label>
						<input name="position" type="text" id="position" class="form-control " value="{{ $jobbaru->position_name}}" disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="witel">Witel</label>
						<input name="witel" type="text" id="witel" class="form-control" value="{{ $jobbaru->witel }}" disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="input-psa">PSA</label>
						<input name="psa" type="text" id="psa" class="form-control " value="{{ $jobbaru->loker }}" disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-regional">Regional</label>
						<input name="regional" type="text" id="regional" class="form-control" value="{{ $jobbaru->regional}}"disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="bizpart">Bizpart ID</label>
						<input name="bizpart" type="text" id="bizpart" class="form-control" value="{{ $jobbaru->bizpart_id }}"disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-direktorat">Direktorat</label>
						<input name="direktorat" type="text" id="direktorat2" class="form-control " value="{{ $jobbaru->direktorat}}" disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="unit">Unit</label>
						<input name="unit" type="text" id="unit2" class="form-control " value="{{ $jobbaru->unit}}" disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-subunit">Sub Unit</label>
						<input name="subunit" type="text" id="subunit2" class="form-control " value="{{ $jobbaru->sub_unit}}" disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="group">Group</label>
						<input name="group" type="text" id="group2" class="form-control " value="{{ $jobbaru->group}}" disabled/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-subgroup">Sub Group</label>
						<input name="subgroup" type="text" id="subgroup2" class="form-control " value="{{ $jobbaru->sub_group}}" disabled/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="groupfungsi">Group Fungsi</label>
						<input name="groupfungsi" type="text" id="groupfungsi2" class="form-control " value="{{ $jobbaru->group_fungsi}}"div disabled/>
					</div>
				</div>
		</div>
	</div>
	</div>

@endsection

@section('plugins')
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/inputmask/jquery.inputmask.bundle.js"></script>
@endsection

@section('footer')
<script>
		
</script>

@endsection