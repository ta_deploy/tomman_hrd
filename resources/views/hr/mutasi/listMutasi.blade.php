@extends('layout')
@section('title')
List Mutasi
@endsection
@section('css')
<style>
</style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div id="detail_mutasi" class="modal fade modal-large detail_mutasi">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title">Detail Mutasi</div>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="table-warning">
							<table class="table table-bordered table-striped table-small-font" id="detail_mut" style="width: 100%;">
								<thead>
									<tr>
										<th>Dokumen</th>
										<th>Tanggal Persetujuan</th>
										<th>Jenis</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tabs-mutasi" data-toggle="tab">
				Mutasi <span class="label label-warning">12</span>
			</a>
		</li>
		<li>
			<a href="#tabs-mutas_appr" data-toggle="tab">
				Persetujuan Mutasi <span class="badge badge-danger">12</span>
			</a>
		</li>
	</ul>
	<div class="tab-content tab-content-bordered">
		<div class="tab-pane fade in active" id="tabs-mutasi">
			<div class="page-header">
				<a href="/hr/submission/mutasi" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Mutasi</a>
				<div class="table-warning">
					<table class="table table-bordered table-striped table-small-font" id="mutasi" style="width: 100%;">
						<thead>
							<tr>
								<th rowspan="2">No</th>
								<th rowspan="2">BA</th>
								<th rowspan="2">Jenis</th>
								<th rowspan="2">NIK</th>
								<th rowspan="2" >Nama</th>
								<th colspan="2" class="text-center">Position Name Sebelumnya</th>
								<th colspan="2" class="text-center">Position Name Baru</th>
							</tr>
							<tr>
								<td>Position Name</td>
								<td>PSA</td>
								<td>Position Name</td>
								<td>PSA</td>
							</tr>
						</thead>
						<tbody class="middle-align">
							<tr>
								<td>1</td>
								<td>ba</td>
								<td>jenis</td>
								<td>nik</td>
								<td>nama</td>
								<td>PN before <button type='button' class='btn btn-sm btn-info' data-toggle='modal' data-target='#detail_mutasi'><span class='fa fa-question'></span> (lihat Detail)</button></td>
								<td>witel</td>
								<td>PN After</td>
								<td>witel</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-mutas_appr">
			<div class="page-header">
				<div class="table-danger">
					<table class="table table-bordered table-striped table-small-font" id="mutasi_appr" style="width: 100%;">
						<thead>
							<tr>
								<th rowspan="2">No</th>
								<th rowspan="2">NIK</th>
								<th rowspan="2">Nama</th>
								<th colspan="2" class="text-center">Position Name</th>
								<th rowspan="2" class="no-sort">Action</th>
							</tr>
							<tr>
								<td>Position Name</td>
								<td>PSA</td>
							</tr>
						</thead>
						<tbody class="middle-align">
							<tr>
								<td>1</td>
								<td>tes</td>
								<td>tas</td>
								<td>tos</td>
								<td>tus</td>
								<td><a href="/hr/approve/mutasi/1" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-edit"></span> Status Mutasi</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    	});

			$(".modal").on('shown.bs.modal', function(){
				console.log('tes')
				$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
			});

			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }],
				"scrollX": true,
			});

			$('#mutasi_wrapper .table-caption').text('List Mutasi Karyawan');
			$('#mutasi_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#mutasi_appr_wrapper .table-caption').text('List Persetujuan Mutasi');
			$('#mutasi_appr_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection