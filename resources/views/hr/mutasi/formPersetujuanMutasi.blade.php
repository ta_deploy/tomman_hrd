@extends('layout')
@section('title')
Form Persetujuan Mutasi
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate" enctype="multipart/form-data">
			<div class="col-md-12">	
				<div class="panel panel-color panel-info panel-border">
					<div class="panel-heading">
						<div class="panel-title title_kar">Input Persetujuan Mutasi Karyawan</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="tgl">Tanggal Pergantian :</label>
							<div class="col-md-10">
								<input type="text" name="tgl" class="form-control" id="tgl">
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label" for="nik_atasan">Nik Atasan :</label>
								<select name="nik_atasan" class="form-control" id="nik_atasan"></select>
							</div>
							<div class="detail_data_nik"></div>
							<div class="col-md-6">
								<label class="control-label" for="nama">Nik Karyawan :</label>
								<select name="nama" class="form-control" id="nama"></select>
							</div>
							<div class="detail_data_nik"></div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="status">Status :</label>
							<div class="col-sm-10">
								<select class="form-control" name="status" id="status">
									<option value="1">Disetujui</option>
									<option value="2">Tidak Disetujui</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="berkas">File Berkas :</label>
							<div class="col-sm-10">
								<input type="File" class="form-control" id="berkas" name="berkas">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">	
				<div class="panel panel-color panel-default panel-border">
					<div class="panel-heading">
						<div class="panel-title">Posisi Lama</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="prev_witel">Witel :</label>
							<div class="col-md-10">
								<select name="prev_witel" class="form-control" id="prev_witel"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="prev_sto">STO :</label>
							<div class="col-md-10">
								<select name="prev_sto" class="form-control" id="prev_sto"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="prev_jabatan">Jabatan :</label>
							<div class="col-md-10">
								<select name="prev_jabatan" class="form-control" id="prev_jabatan"></select>
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label" for="prev_unit">Unit :</label>
								<input type="text" name="prev_unit" class="form-control" id="prev_unit">
							</div>
							<div class="col-md-6">
								<label class="control-label" for="prev_sub_unit">Sub Unit :</label>
								<input type="text" name="prev_sub_unit" class="form-control" id="prev_sub_unit">
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-4">
								<label class="control-label" for="prev_group">Group :</label>
								<input type="text" name="prev_group" class="form-control" id="prev_group">
							</div>
							<div class="col-md-4">
								<label class="control-label" for="prev_sub_grup">Sub Group :</label>
								<input type="text" name="prev_sub_grup" class="form-control" id="prev_sub_grup">
							</div>
							<div class="col-md-4">
								<label class="control-label" for="prev_grup_fun">Group Fungsi :</label>
								<input type="text" name="prev_grup_fun" class="form-control" id="prev_grup_fun">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="prev_bizpart">Bizpart ID :</label>
							<div class="col-md-9">
								<input type="text" name="prev_bizpart" class="form-control" id="prev_bizpart">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="prev_direktorat">Direktorat :</label>
							<div class="col-md-9">
								<input type="text" name="prev_direktorat" class="form-control" id="prev_direktorat">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">	
				<div class="panel panel-color panel-primary panel-border">
					<div class="panel-heading">
						<div class="panel-title">Posisi Baru</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="witel">Witel :</label>
							<div class="col-md-10">
								<select name="witel" class="form-control" id="witel"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sto">STO :</label>
							<div class="col-md-10">
								<select name="sto" class="form-control" id="sto"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="jabatan">Jabatan :</label>
							<div class="col-md-10">
								<select name="jabatan" class="form-control" id="jabatan"></select>
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label" for="unit">Unit :</label>
								<input type="text" name="unit" class="form-control" id="unit">
							</div>
							<div class="col-sm-6">
								<label class="control-label" for="sub_unit">Sub Unit :</label>
								<input type="text" name="sub_unit" class="form-control" id="sub_unit">
							</div>
						</div>
						<hr class="page-wide-block">
						<div class="form-group">
							<div class="col-md-4">
								<label class="control-label" for="group">Group :</label>
								<input type="text" name="group" class="form-control" id="group">
							</div>
							<div class="col-md-4">
								<label class="control-label" for="sub_grup">Sub Group :</label>
								<input type="text" name="sub_grup" class="form-control" id="sub_grup">
							</div>
							<div class="col-md-4">
								<label class="control-label" for="grup_fun">Group Fungsi :</label>
								<input type="text" name="grup_fun" class="form-control" id="grup_fun">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="bizpart">Bizpart ID :</label>
							<div class="col-md-9">
								<input type="text" name="bizpart" class="form-control" id="bizpart">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="direktorat">Direktorat :</label>
							<div class="col-md-9">
								<input type="text" name="direktorat" class="form-control" id="direktorat">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();
			
			$('#tgl').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',
        daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',

        beforeShowMonth: function(date) {
          if (date.getMonth() === 8) {
            return false;
          }
        },

        beforeShowYear: function(date){
          if (date.getFullYear() === 2014) {
            return false;
          }
        }
      });

			$('#nama').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: { 
					url: "/jx_dng/get_user",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Nik Tidak Terdaftar!";
						}
					},
					cache: true,
					success: function(value) {
						// $('.title_kar').html('Resign Karyawan', value);
					}
				}
			});

			$('#nik_atasan').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: { 
						url: "/jx_dng/get_user",
						dataType: 'json',
						delay: 250,
						data: function (params) {
								return {
										searchTerm: params.term
								};
						},
						processResults: function (response) {
								return {
										results: response
								};
						},
						language: {
								noResults: function(){
										return "Nama Karyawan / KK / Nik Sementara Tidak Terdaftar!";
								}
						},
						cache: true,
						success: function(value) {
						}
				}
			});

			$('#witel').select2({
				width: '100%',
				placeholder: "Ketik Nama Witel",
				minimumInputLength: 7,
				allowClear: true,
				language: {
					noResults: function(){
						return "Witel Tidak Terdaftar!";
					}
				}
			});

			$('#created_by').select2({
				width: '100%',
				placeholder: "Ketik Nama Pembuat",
				minimumInputLength: 7,
				allowClear: true,
				language: {
					noResults: function(){
						return "Nama Tidak Terdaftar!";
					}
				}
			});

			$('#approve_by').select2({
				width: '100%',
				placeholder: "Ketik Nama Penyetuju",
				minimumInputLength: 7,
				allowClear: true,
				language: {
					noResults: function(){
						return "Nama Tidak Terdaftar!";
					}
				}
			});

			$('#known_by').select2({
				width: '100%',
				placeholder: "Ketik Nama Saksi",
				minimumInputLength: 7,
				allowClear: true,
				language: {
					noResults: function(){
						return "Nama Tidak Terdaftar!";
					}
				}
			});

			var sto = $('#sto').select2({
				width: '100%',
				placeholder: "Pilih STO",
			}),
			jabatan = $('#jabatan').select2({
				width: '100%',
				placeholder: "Pilih STO",
			});


			$('#witel').on("select2:selecting", function(e) { 	
				$('#sto').empty().change();
				sto.select2({
					width: '100%',
					placeholder: "Pilih STO",
					data: data_pr,
				});
			});
			
			$('#sto').on("select2:selecting", function(e) { 	
				$('#jabatan').empty().change();
				jabatan.select2({
					width: '100%',
					placeholder: "Pilih Jabatan",
					data: data_pr,
					ajax: { 
					url: "/jx_dng/get_user",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Jabatan Tidak Terdaftar!";
						}
					},
					cache: true,
					success: function(value) {
						// $('#unit2').val(data.unit);
						// $('#subunit2').val(data.sub_unit);
						// $('#group2').val(data.group);
						// $('#subgroup2').val(data.sub_group);
						// $('#groupfungsi2').val(data.group_fungsi);
						// $('#regional2').val(data.regional);
						// $('#bizpart2').val(data.bizpart_id);
					}
				}
				});
			});
		});
	</script>
@endsection