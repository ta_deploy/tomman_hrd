@include('partial.confirm')
@extends('layout')

@section('html-title')
	 HR :: TOMMAN
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Project :
@endsection

@section('page-title')
	<style>
	.right{
		text-align: right;
	}
	</style>
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2.css">
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="/lib/xenon/js/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/lib/xenon/css/fonts/meteocons/css/meteocons.css">
	<script src="/lib/xenon/js/jquery-1.11.1.min.js"></script>

	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
		<li><i class="linecons-calendar"></i>HR</li>
		<li class="active"><i class="linecons-tag">Mapping Position</i></li>
	</ol>
@endsection


@section('body')
<div class="panel panel-color panel-blue">
	<div class="panel-heading">
		<div class="panel-title">Position Name</div>
		<div class="panel-options">
				<a href="#" data-toggle="panel">
					<span class="collapse-icon klik">&ndash;</span>
					<span class="expand-icon">+</span>
				</a>
		</div>
	</div>
		<div class="panel-body">
			<form method="post" class="form-horizontal">
			<div class="input-group">
				<div class="col-sm-1">
					<label class="control-label" for="input-status">Filter By</label>
				</div>
				<div class="col-sm-2">
					<select name="cari" id="input-cari" class="form-control selectboxit">
						<option value="1">WITEL</option>
						<option value="2">PSA</option>
					</select>
				</div>
				<div id="divwitel" >
					<div class="col-sm-3">
						<select name="witel" id="input-witel" class="form-control">
							@foreach($witel as $p)
								<option value="{{ $p->id }}">{{ $p->nama_witel }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div id="divpsa">
					<div class="col-sm-3">
						<input name="psa" id="psa" class="form-control"/>
					</div>
				</div>

				<input name="status" type="hidden"  value="0,1" />
				<input name="status_tersedia" type="hidden"  value="0,1" />

		      <span class="input-group-btn">
		      	<button class="btn btn-blue btn-icon btn-icon-standalone btn-icon-standalone-right" type="submit">
					<i class="fa-search"></i>
					<span>Filter</span>
				</button>
		      </span>
		    </div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3">
			<div class="xe-widget xe-counter" data-count=".num" data-from="0" data-to="{{$data[0] or ''}}" data-duration="3">
				<div class="xe-icon">
					<button id="all" class="btn btn-success btn-sm btn-lg btn-icon icon-left fa-bar-chart"></button>
				</div>
				<div class="xe-label">
					<strong class="num">0</strong>
					<span>ALL</span>
				</div>
			</div>
		</div>

		<div class="col-sm-3">
			<div class="xe-widget xe-counter xe-counter-blue " data-count=".num" data-from="0" data-to="{{$data[1] or ''}}" data-duration="3" data-easing="false">
				<div class="xe-icon">
					<button id="terisi" class="btn btn-blue btn-sm btn-lg btn-icon icon-left fa-bar-chart"></button>
				</div>
				<div class="xe-label">
					<strong class="num">0</strong>
					<span>Terisi</span>
				</div>
			</div>
		</div>

		<div class="col-sm-3">
			<div class="xe-widget xe-counter xe-counter-info" data-count=".num" data-from="0" data-to="{{$data[2] or ''}}" data-duration="3" data-easing="true">
				<div class="xe-icon">
					<button id="kosong" class="btn btn-info btn-sm btn-lg btn-icon icon-left fa-bar-chart"></button>
				</div>
				<div class="xe-label">
					<strong class="num">0</strong>
					<span>Kosong</span>
				</div>
			</div>
		</div>

		<div class="col-sm-3">
			<div class="xe-widget xe-counter xe-counter-warning"  data-count=".num" data-from="0" data-to="{{$data[4] or ''}}" data-duration="3" data-easing="true">
				<div class="xe-icon">
					<button id="unmap" class="btn btn-warning btn-sm btn-lg btn-icon icon-left fa-bar-chart"></button>
				</div>
				<div class="xe-label">
					<strong class="num">0</strong>
					<span>Naker Unmapping</span>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default" id="divjob">
		<div class="panel-heading">
	      	<div class="panel-options">
	        <a href="#" id="copy-button" data-clipboard-target="#example-2">
	          Copy
	        </a>
	          <i class="fa-copy"></i>
	      	</div>
	    </div>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-small-font" id="example-2">
			<thead>
				<tr>
					<th>Nomor</th>
					<th>Aksi</th>
					<th>Object ID</th>
					<th>NIK</th>
					<th>Nama</th>
					<th>Position Name </th>
					<th>PSA</th>
					<th>Witel</th>
					<th>Regional</th>
					<th>Level</th>
					<th>Bizzpart ID</th>
					<th>Direktorat</th>
					<th>Unit</th>
					<th>Sub Unit</th>
					<th>Group</th>
					<th>Sub Group</th>
					<th>Group Fungsi</th>
					<th>Status</th>
					<th>Status Tersedia</th>
				</tr>
			</thead>
			<tbody class="middle-align">
				<?php $no=1; ?>
				@foreach($list as $r)
				<?php $status='';
						$status_tersedia='';
					if ($r->status==0){$status="Kosong";} else if ($r->status==1){$status="Terisi";}
					if ($r->status_tersedia==0){$status_tersedia="Tidak Aktif";} else if ($r->status_tersedia==1){$status_tersedia="Aktif";}
				?>
					<tr>
						<td>{{$no}}</td>
						<td><a href="/hr/formMutasi/input/{{$r->id_naker}}" class="btn btn-blue btn-sm btn-icon icon-left fa-arrow-right tooltip-blue"
							data-toggle="tooltip" data-placement="top" title="" data-original-title="Pelurusan Position"
							style="display:{{ ($r->status==0) ? 'none':'inline'  }}">
							</a>
							<a href="/hr/formMutasi/unmap/{{$r->id}}" class="btn btn-blue btn-sm btn-icon icon-left fa-arrow-right tooltip-blue"
							data-toggle="tooltip" data-placement="top" title="" data-original-title="Pelurusan Position"
							style="display:{{ ($r->status==0) ? 'inline':'none'  }}">
							</a>
							<form method="POST" action="/hr/unmap/{{$r->id}}/{{$r->id_naker}}/{{$r->id_loker}}" accept-charset="UTF-8" style="display:{{ ($r->status==0) ? 'none':'inline'  }}"
								class="tooltip-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Unmap">
			                  <button class="btn btn-warning btn-sm btn-icon icon-left fa-puzzle-piece tooltip-warning"
			                  type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Unmap {{$r->nama}} ?"
			                  data-message="Are you sure?">
			                  </button>
			                </form>
			            </td>
						<td>{{$r->object_id}}</td>

						<td>{{ ($r->status==0) ? '':$r->nik  }}</td>
						<td>{{ ($r->status==0) ? '':$r->nama  }}</td>

						<td>{{$r->position_name}}</td>
						<td>{{$r->loker}}</td>
						<td>{{$r->witel}}</td>
						<td>{{$r->regional}}</td>
						<td>{{$r->level}}</td>
						<td>{{$r->bizpart_id}}</td>

						<td>{{$r->direktorat}}</td>
						<td>{{$r->unit}}</td>
						<td>{{$r->sub_unit}}</td>
						<td>{{$r->group}}</td>
						<td>{{$r->sub_group}}</td>
						<td>{{$r->group_fungsi}}</td>
						<td>{{$status}}</td>
						<td>{{$status_tersedia}}</td>
					</tr>
					<?php $no++;?>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>

	<div class="panel panel-default" id="divunmap">
		<div class="panel-heading">
	      	<div class="panel-options">
	        <a href="#" id="copy-button" data-clipboard-target="#example-3">
	          Copy
	        </a>
	          <i class="fa-copy"></i>
	      	</div>
	    </div>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-small-font" id="example-3">
			<thead>
				<tr>
					<th>Nomor</th>
					<th>Aksi</th>
					<th>NIK</th>
					<th>Nama</th>
					<th>PSA</th>
					<th>Position Sebelumnya</th>
				</tr>
			</thead>
			<tbody class="middle-align">
				<?php $no=1; ?>
				@foreach($listUnmap as $r)
					<tr>
						<td>{{$no}}</td>
						<td><a href="/hr/formMutasi/input/{{$r->id_naker}}" class="btn btn-blue btn-sm btn-icon icon-left fa-arrow-right tooltip-blue"
							data-toggle="tooltip" data-placement="top" title="" data-original-title="Pelurusan Position"
							style="display:inline">
							</a>
							<form method="POST" action="/hr/cancelUnmap/{{$r->id}}/{{$r->idjob}}" accept-charset="UTF-8"
								style="display:{{ ($r->status==0) ? 'inline':'none'  }}"
								class="tooltip-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cancel Unmap">
			                  	<button class="btn btn-warning btn-sm btn-icon icon-left fa-puzzle-piece tooltip-warning"
			                  	type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Cancel Unmap {{$r->nama}} ?"
			                  	data-message="Are you sure?">
			                  </button>
			                </form>
						</td>
						<td>{{$r->nik}}</td>
						<td>{{$r->nama}}</td>
						<td>{{$r->loker}}</td>
						<td>{{$r->position_name}}</td>
					</tr>
					<?php $no++;?>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>

@endsection

@section('plugins')
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/lib/xenon/js/datatables/dataTables.bootstrap.js"></script>
	<script src="/lib/xenon/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="/lib/xenon/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
	<script src="/lib/xenon/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="/lib/xenon/js/select2/select2.min.js"></script>
	<script src="/lib/xenon/js/rwd-table/js/rwd-table.min.js"></script>
  	<script src="//cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.4.0/clipboard.min.js"></script>
	<script src="/lib/xenon/js/inputmask/jquery.inputmask.bundle.js"></script>
	<script src="/lib/xenon/js/xenon-widgets.js"></script>

@endsection

@section('footer')
	<script>
	$('#divpsa').hide();
	$('#input-cari').change( function (){
		if($( this ).val() == 1){
			$('#divwitel').show();
			$('#divpsa').hide();
		}
		else if($( this ).val() == 2){
			$('#divpsa').show();
			$('#divwitel').hide();
		}
	});
	var data = <?= json_encode($psa) ?>;
			$('#psa').select2({
				data: data,
				placeholder: 'Input PSA',
				allowClear: true,
				minimumInputLength: 1,
				formatSelection: function(data) { return data.text },
				formatResult: function(data) {
				return  '<span class="label label-info">'+data.id+'</span>'+
								'<strong style="margin-left:5px">'+data.text+'</strong>';
				}
			});

	$("#input-witel").select2();

	$('#divunmap').hide(); $('#divjob').show();
	$("#example-3").dataTable();

    var dataTables = $("#example-2").DataTable({
	   	"scrollY": 1000,
	   	"scrollX": true,
	   	"scrollCollapse": true,
	   	"autoWidth": true,
	   });

   $('#all').on('click', function () {
        dataTables.columns().search("").draw();
        $('#divunmap').hide(); $('#divjob').show();
    });
   $('#terisi').on('click', function () {
        dataTables.columns(17).search("Terisi").draw();
        $('#divunmap').hide(); $('#divjob').show();
    });
   $('#kosong').on('click', function () {
        dataTables.columns(17).search("Kosong").draw();
        $('#divunmap').hide(); $('#divjob').show();
    });
   $('#unmap').on('click', function () {
        $('#divjob').hide(); $('#divunmap').show();
    });

    (function(){
    var copyCode = new Clipboard('#copy-button');
    copyCode.on('success', function(event) {
      event.clearSelection();
      event.trigger.textContent = 'Copied';
      window.setTimeout(function() {
          event.trigger.textContent = 'Copy';
      }, 2000);
    });
    copyCode.on('error', function(event) {
      event.trigger.textContent = 'Press "Ctrl + C" to copy';
      window.setTimeout(function() {
          event.trigger.textContent = 'Copy';
      }, 2000);
    });
	})();

	$('#confirmDelete').on('show.bs.modal', function (e) {
      $message = $(e.relatedTarget).attr('data-message');
      $(this).find('.modal-body p').text($message);
      $title = $(e.relatedTarget).attr('data-title');
      $(this).find('.modal-title').text($title);
      $(this).find('#confirm').text('Yes');

      	$(this).find('#confirm').addClass('btn-warning');
		$(this).find('#confirm').removeClass('btn-danger');
      // Pass form reference to modal for submission on yes/ok
      var form = $(e.relatedTarget).closest('form');
      $(this).find('.modal-footer #confirm').data('form', form);
    });
    $('#confirm').on('click', function(){
        $(this).data('form').submit();
    });

  </script>
@endsection