@extends('layout')
@section('title')
List Level
@endsection
@section('css')
<style>
  th {
    text-align: center;
	text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<div class="table-warning">
			<table class="table table-bordered table-striped table-small-font" id="table_mitra">
				<thead>
					<tr>
						<th>ID Level</th>
						<th>Level</th>
					</tr>
				</thead>
				<tbody class="middle-align">
				@foreach($data as $num => $data)
  					<tr>
						<td>{{ $data->level_id }}</td>
						<td>{{ $data->level_name }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
	$(".table").DataTable({
			"columnDefs": [{
				"targets": 'no-sort',
				"orderable": false,
				}]
		});

		$('#table_mitra_wrapper .table-caption').text('LIST LEVEL');
		$('#table_mitra_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
  </script>
@endsection