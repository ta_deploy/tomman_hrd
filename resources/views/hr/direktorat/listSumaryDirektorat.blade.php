@extends('layout')
@section('title')
List Direktorat
@endsection
@section('css')
<style>
</style>
@endsection
@section('content')
<div id="edit_direktorat" class="modal fade modal-large edit_direktorat">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">Edit Direktorat <span class="direktorat_prev"></span></div>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="panel panel-color panel-warning panel-border">
						<div class="panel-heading">
							<div class="panel-title title_kar">Ubah Direktorat <span class="direktorat_prev"></span></div>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="new_alker">Direktorat Baru</label>
								<div class="col-md-9">
									<input type="text" name="new_alker" class="form-control" id="new_alker">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" data-dismiss="modal"><span class="fa fa-cloud-download"></span> Simpan</button>
			</div>
		</div>
	</div>
</div>
<div class="px-content">
	@include('Partial.alerts')
  <div class="page-header">
    <div class="panel panel-info input_dir">
      <div class="panel-heading">
        <div class="panel-title">Input Data Direktorat</div>
      </div>
      <div class="panel-body">
        <form class="form-inline" id="add_dir" method="POST">
					{{ csrf_field() }}
          <div class="form-group">
            <label for="direktorat_nm">Nama Direktorat :</label>
            <input type="text" class="form-control" name="direktorat_nm" id="direktorat_nm" placeholder="Nama Direktorat">
          </div>
          <button type="submit" class="btn btn-default find_me">Tambah</button>
					<code id="cuss" style="display: none;"></code>
        </form>
      </div>
    </div>
		<div class="table-info">
			<table class="table table-bordered table-striped table-small-font" id="table_direktorat">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th>Direktorat</th>
						<th>Posisi</th>
						<th>Isi</th>
					</tr>
				</thead>
				<tbody class="middle-align">
					@foreach ($data as $key => $val)
						<tr>
							<td>{{ ++$key }}</td>
							<td>{{ $val->direktorat }}</td>
							<td>{{ $val->pos_name }}</td>
							<td>{{ $val->jml }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function(){

			$('.find_me').on('click', function(e){
				e.preventDefault();
				$.ajax({
					type: 'GET',
					url: '/jx_dng/validate_dirkt',
					data: {
						data: $.trim($('#direktorat_nm').val() )
					},
					context: this,
					datatype: 'JSON',
					beforeSend: function(){
						$('.input_dir').toggleClass('form-loading');
					}
				}).done( function(val){
					$(this).removeClass(function (index, className){
						return (className.match (/(^|\s)btn-\S+/g) || []).join(' ');
					});

					$('.input_dir').toggleClass('form-loading');

					if($.isEmptyObject(val) ){

						if($(this).text() == 'Submit'){
							$('#add_dir').submit();
							$(this).attr('disabled', true);
						}

						$(this).toggleClass('find_me');
						$(this).text('Submit');

						$('#cuss').css({
							'display': 'inline',
							'color': 'green'
						});
						$(this).addClass('btn-primary');
						$('#cuss').text('*Nama Direktorat Tidak Ada, Bisa Diinput!');
					}else{
						e.preventDefault();
						$('#cuss').css({
							'display': 'inline',
						});
						$(this).addClass('btn-danger');
						$('#cuss').text('*Nama Direktorat Sudah Ada, Tidak Bisa Input Dua Kali!');
					}
				});
			});

			$(".table").DataTable({
				"columnDefs": [
					{
					"targets": 'no-sort',
					"orderable": false,
					}
				],
			});

			$('#table_direktorat_wrapper .table-caption').text('List Direktorat');
			$('#table_direktorat_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		})
  </script>
@endsection