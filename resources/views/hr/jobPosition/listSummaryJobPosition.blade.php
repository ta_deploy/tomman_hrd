@extends('layout')
@section('title')
List Posisi
@endsection
@section('css')

@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div id="detail_job" class="modal fade modal-large detail_job">
		<div class="modal-dialog" style="width:1250px;">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title">Detail Mutasi</div>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="table-info">
							<table class="table table-bordered table-striped table-small-font" id="detail_mut" style="width: 100%;">
								<thead>
									<tr>
										<th>Object ID</th>
										<th>Position Name </th>
										<th>PSA</th>
										<th>Witel</th>
										<th>Regional</th>
										<th>Level</th>
										<th>Teritory</th>
										<th>Bizzpart ID</th>
										<th>Direktorat</th>
										<th>Unit</th>
										<th>Sub Unit</th>
										<th>Group</th>
										<th>Sub Group</th>
										<th>Group Fungsi</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">close</button>
				</div>
			</div>
		</div>
	</div>
  <div class="page-header">
		<a href="/hr/new_jobPos/input" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Job Position</a>
		<div class="table-info">
			<table class="table table-bordered table-striped table-small-font" id="table_posisi">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th>Direktorat</th>
						<th>Posisi</th>
						<th>Isi</th>
						<th class="no-sort">Action</th>
					</tr>
				</thead>
				<tbody class="middle-align">
					@foreach ($data as $key => $d)
						<tr>
							 <td>{{ ++$key }}</td>
							 <td>{{ $d->direktorat }}</td>
							 <td>{{ $d->pos_name }}</td>
							 <td>{{ $d->total_pd }}</td>
							 <td><button type='button' class='btn btn-sm btn-info show_modal' data-toggle='modal' data-target='#detail_job' data-pos ="{{ $d->pos_name }}" data-id_dir="{{ $d->direktorat_id }}"><span class='fa fa-question'></span> (lihat Detail)</button></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function(){
			$(".table").DataTable({
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}],
			});

			$(".modal").on('shown.bs.modal', function(){
				$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
			});

			$('.show_modal').on('click', function(){
				$.ajax({
					type: 'GET',
					url: '/jx_dng/detail_job_pos',
					data:{
						id_dir: $(this).attr('data-id_dir'),
						pos_name: $(this).attr('data-pos')
					},
					dataType:'JSON'
				}).done( function(e){
					var main_table = $.map(e, function(value, key){
						return [value]
					});

					var main_data =[];

					$.each(main_table, function(k, v){
						main_data.push( Object.values(v) )
					})

					$('#detail_mut').DataTable().clear().draw();

					$.each(main_data, function(k, v){
						$('#detail_mut').DataTable().row.add(v).draw();
					})
				});
			});

			$('#table_posisi_wrapper .table-caption').text('List Posisi Pekerjaan');
			$('#table_posisi_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		})
  </script>
@endsection