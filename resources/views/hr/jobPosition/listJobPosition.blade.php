@include('partial.confirm')
@extends('layout')

@section('html-title')
	 HR :: TOMMAN
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Project : 
@endsection

@section('page-title')
	<style>
	.right{
		text-align: right;
	}
	</style>
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2.css">
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="/lib/xenon/js/datatables/dataTables.bootstrap.css">
	<script src="/lib/xenon/js/jquery-1.11.1.min.js"></script>

	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
		<li><i class="linecons-calendar"></i>HR</li>
		<li class="active"><i class="linecons-tag">List Position Name</i></li>
		<a href="/hr/formJobPosition/input" class="btn btn-info btn-sm btn-icon icon-left fa-plus tooltip-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Tambah Data">
		</a>
	</ol>
@endsection


@section('body')
<div class="panel panel-default panel-noheading">
		<div class="panel-body">
			<form method="post" class="form-horizontal">
			<div class="input-group">
				<div class="col-sm-1">
					<label class="control-label" for="input-status">Filter By</label>
				</div>	
				<div class="col-sm-2">	
					<select name="cari" id="input-cari" class="form-control selectboxit">
						<option value="1">WITEL</option>
						<option value="2">PSA</option>
					</select>
				</div>
				<div id="divwitel" >	
					<div class="col-sm-3">	
						<select name="witel" id="input-witel" class="form-control">
							@foreach($witel as $p)
								<option value="{{ $p->id }}">{{ $p->nama_witel }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div id="divpsa">
					<div class="col-sm-3">	
						<input name="psa" id="psa" class="form-control"/>
					</div>
				</div>

				<div class="col-sm-1">
					<label class="control-label" for="input-kategori">Status</label>
				</div>	
				<div class="col-sm-2">	
					<select name="status" id="input-status" class="form-control selectboxit">
							<option value="0,1">ALL</option>
							<option value="1">Terisi</option>
							<option value="0">Kosong</option>
					</select>				
				</div>
				<div class="col-sm-1">
					<label class="control-label" for="input-kategori">Status Tersedia</label>
				</div>	
				<div class="col-sm-2">	
					<select name="status_tersedia" id="input-status" class="form-control selectboxit">
							<option value="0,1">ALL</option>
							<option value="1">Aktif</option>
							<option value="0">Tidak Aktif</option>
					</select>				
				</div>
		      <span class="input-group-btn">
		      	<button class="btn btn-blue btn-icon btn-icon-standalone btn-icon-standalone-right" type="submit">
					<i class="fa-search"></i>
					<span>Search</span>
				</button>
		      </span>
		    </div>
			</form>
		</div>
	</div>

	<div class="panel panel-default">	
		<div class="panel-heading">
	      <div class="panel-options">
	        <a href="#" id="copy-button" data-clipboard-target="#example-2">
	          Copy
	        </a>
	          <i class="fa-copy"></i>
	      </div>
	    </div>	
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-small-font" id="example-2">
			<thead>
				<tr>
					<th>Nomor</th>
					<th>Aksi</th>
					<th>Object ID</th>
					<th>Position Name </th>
					<th>PSA</th>
					<th>Witel</th>
					<th>Regional</th>
					<th>Level</th>
					<th>Teritory</th>
					<th>Bizzpart ID</th>
					<th>Direktorat</th>
					<th>Unit</th>
					<th>Sub Unit</th>
					<th>Group</th>
					<th>Sub Group</th>
					<th>Group Fungsi</th>
					<th>Status</th>
					<th>Status Tersedia</th>
				</tr>
			</thead>
			<tbody class="middle-align">
				<?php $no=1; ?>
				@foreach($list as $r)
				<?php $status='';
						$status_tersedia='';
					if ($r->status==0){$status="Kosong";} else if ($r->status==1){$status="Terisi";}
					if ($r->status_tersedia==0){$status_tersedia="Tidak Aktif";} else if ($r->status_tersedia==1){$status_tersedia="Aktif";}
				?>				
					<tr>
						<td>{{$no}}</td>
						<td><a href="/hr/formJobPosition/{{$r->id}}" class="btn btn-secondary btn-sm btn-icon icon-left fa-edit tooltip-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" >					
							</a>
							<form method="POST" action="/hr/deleteJobPosition/{{$r->id}}" accept-charset="UTF-8" style="display:inline"
								class="tooltip-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Job Position">
			                  <button class="btn btn-danger btn-sm btn-icon icon-left fa-trash tooltip-danger" 
			                  type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Hapus Job Position {{ $r->position_name }} ?" 
			                  data-message="Are you sure you want to delete this ?" <?php if($r->status==1){echo "disabled";} ?>>
			                
			                  </button>
			                </form>
						</td>
						<td>{{$r->object_id}}</td>
						<td>{{$r->position_name}}</td>

						<td>{{$r->loker}}</td>
						<td>{{$r->witel}}</td>
						<td>{{$r->regional}}</td>
						<td>{{$r->level}}</td>
						<td>{{$r->teritory}}</td>
						<td>{{$r->bizpart_id}}</td>

						<td>{{$r->direktorat}}</td>
						<td>{{$r->unit}}</td>
						<td>{{$r->sub_unit}}</td>
						<td>{{$r->group}}</td>
						<td>{{$r->sub_group}}</td>
						<td>{{$r->group_fungsi}}</td>
						<td>{{$status}}</td>
						<td>{{$status_tersedia}}</td>
					</tr>
					<?php $no++;?>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>

@endsection

@section('plugins')
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/lib/xenon/js/datatables/dataTables.bootstrap.js"></script>
	<script src="/lib/xenon/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="/lib/xenon/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
	<script src="/lib/xenon/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="/lib/xenon/js/select2/select2.min.js"></script>
	<script src="/lib/xenon/js/rwd-table/js/rwd-table.min.js"></script>
  	<script src="//cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.4.0/clipboard.min.js"></script>
	
@endsection

@section('footer')
	<script>
	$('#divpsa').hide();
	$('#input-cari').change( function (){
		if($( this ).val() == 1){
			$('#divwitel').show();
			$('#divpsa').hide();
		}
		else if($( this ).val() == 2){
			$('#divpsa').show();
			$('#divwitel').hide();
		}
	});
	var data = <?= json_encode($psa) ?>;
			$('#psa').select2({
				data: data,
				placeholder: 'Input PSA',
				allowClear: true,
				minimumInputLength: 1,
				formatSelection: function(data) { return data.text },
				formatResult: function(data) {
				return  '<span class="label label-info">'+data.id+'</span>'+
								'<strong style="margin-left:5px">'+data.text+'</strong>';
				}
			});

	$("#input-witel").select2();		

    $("#example-2").DataTable({
	   	scrollY: 1000,
	   	"scrollX": true,
	   	"scrollCollapse": true,
	   	"autoWidth": true
	   });
    $('#confirmDelete').on('show.bs.modal', function (e) {
      $message = $(e.relatedTarget).attr('data-message');
      $(this).find('.modal-body p').text($message);
      $title = $(e.relatedTarget).attr('data-title');
      $(this).find('.modal-title').text($title);
      // Pass form reference to modal for submission on yes/ok
      var form = $(e.relatedTarget).closest('form');
      $(this).find('.modal-footer #confirm').data('form', form);
    });
    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
        $(this).data('form').submit();
    });

    (function(){
    var copyCode = new Clipboard('#copy-button');
    copyCode.on('success', function(event) {
      event.clearSelection();
      event.trigger.textContent = 'Copied';
      window.setTimeout(function() {
          event.trigger.textContent = 'Copy';
      }, 2000);
    });
    copyCode.on('error', function(event) { 
      event.trigger.textContent = 'Press "Ctrl + C" to copy';
      window.setTimeout(function() {
          event.trigger.textContent = 'Copy';
      }, 2000);
    });
	})();
  </script>
@endsection