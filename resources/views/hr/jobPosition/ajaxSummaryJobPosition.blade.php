<table class="table table-bordered table-striped table-small-font" id="example-2">
	<thead>
		<tr>
			<th class="text-center">Nomor</th>
			<th class="text-center">Aksi</th>
			<th class="text-center">Posisi</th>
			<th class="text-center">Kuota</th>
			<th class="text-center">Isi</th>
			<th class="text-center">Kekurangan</th>
		</tr>
	</thead>
	<tbody class="middle-align">
	<?php $no=1; ?>
	@foreach($list as $r)				
		<tr>
			<td class="text-center">{{$no}}</td>
			<td >
				<button value="{{$r->position_name}}" class="tambah btn btn-info btn-sm btn-icon icon-left fa-plus ">					
				</button>
				<button value="{{$r->position_name}}" class="kurang btn btn-danger btn-sm btn-icon icon-left fa-minus" 
				{{ ($r->kekurangan==0) ? 'disabled':'enabled'  }}>					
				</button>
			</td>
			<td>{{$r->position_name}}</td>
			<td class="text-center">{{$r->kuota}}</td>
			<td class="text-center">{{$r->isi}}</td>
			<td class="text-center">{{$r->kekurangan}}</td>
		</tr>
		<?php $no++;?>
	@endforeach
	</tbody>
</table>

	