@extends('layout')
@section('title')
Form Posisi Pekerjaan
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="col-md-12">
			<div class="panel panel-color panel-info panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Pembuatan Job Position</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="posisi_id">ID :</label>
						<div class="col-md-10">
							<input type="text" name="posisi_id" class="form-control" id="posisi_id">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="nama_posisi">Position Name :</label>
						<div class="col-md-10">
							<input type="text" name="nama_posisi" class="form-control" id="nama_posisi">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="title_posisi">Position Title :</label>
						<div class="col-md-10">
							<select name="title_posisi" class="form-control" id="title_posisi">
								@foreach ($posisi_job_title as $v)
									<option value="{{ $v->text }}">{{ $v->text }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="direktorat">Direktorat :</label>
						<div class="col-md-10">
							<select name="direktorat" class="form-control" id="direktorat">
							</select>
						</div>
					</div>
					<hr class="page-wide-block">
					<div class="form-group">
						<div class="col-md-6">
							<label class="control-label" for="unit">Unit :</label>
							<input type="text" name="unit" class="form-control" id="unit">
						</div>
						<div class="col-md-6">
							<label class="control-label" for="sub_unit">Sub Unit :</label>
							<input type="text" name="sub_unit" class="form-control" id="sub_unit">
						</div>
					</div>
					<hr class="page-wide-block">
					<div class="form-group">
						<div class="col-md-4">
							<label class="control-label" for="grup">Group :</label>
							<input type="text" name="grup" class="form-control" id="grup">
						</div>
						<div class="col-md-4">
							<label class="control-label" for="sub_grup">Sub Group :</label>
							<select name="sub_grup" class="form-control" id="sub_grup">
								@foreach ($posisi_job_sub as $v)
									<option value="{{ $v->text }}">{{ $v->text }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-4">
							<label class=" control-label" for="grup_fungsi">Fungsi Group :</label>
							<select name="grup_fungsi" class="form-control" id="grup_fungsi">
								@foreach ($posisi_job_fungsi as $v)
									<option value="{{ $v->text }}">{{ $v->text }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<hr class="page-wide-block">
					<div class="form-group">
						<div class="col-md-4">
							<label class="control-label" for="witel">Witel</label>
							<select name="witel" class="form-control" id="witel">
								<option value="Regional KALIMANTAN">Regional KALIMANTAN</option>
								@foreach ($get_witel as $witel)
									<option value="WITEL {{ strtoupper($witel->City) }}">WITEL {{ strtoupper($witel->City) }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-4">
							<label class="control-label" for="teritory">Teritory :</label>
							<select name="teritory" class="form-control" id="teritory">
								@foreach ($get_witel as $teritor)
									<option value="TA {{ strtoupper($teritor->City) }}">TA {{ strtoupper($teritor->City) }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-4">
							<label class="control-label" for="regional">Regional :</label>
							<select name="regional" class="form-control" id="regional">
								@foreach ($get_witel as $reg)
									<option value="{{ strtoupper($reg->Reg_TA_Alias) }}">{{ strtoupper($reg->Reg_TA_Alias) }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<hr class="page-wide-block">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="bizpart_id">Bizpart Id :</label>
						<div class="col-md-10">
							<select name="bizpart_id" class="form-control" id="bizpart_id">
								<option value="Witel">Witel</option>
								<option value="Regional">Regional</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="level">Level :</label>
						<div class="col-md-10">
							<select name="level" class="form-control" id="level">
								@foreach ($posisi_job_level as $v)
									<option value="{{ $v->text }}">{{ $v->text }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="status">Status :</label>
						<div class="col-md-10">
							<select name="status" class="form-control" id="status">
								<option value="Active">Active</option>
								<option value="Non-Active">Non-Active</option>
							</select>
						</div>
					</div>
					<div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
            </div>
          </div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$("#regional option").each(function() {
				$(this).siblings('[value="'+ this.value +'"]').remove();
			});

			$('select').select2();

			$('#direktorat').select2({
				width: '100%',
				placeholder: "Ketik Nama Direktorat",
				allowClear: true,
				minimumInputLength: 3,
				ajax: {
					url: '/jx_dng/get_dir',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				language: {
					noResults: function(){
						return "Direktorat Tidak Ditemukan? <a type='button' href='/hr/list/direktorat' target='_blank' class='btn btn-sm btn-warning'><span class='ion-plus-circled'></span> Buka Halaman Direktorat</a>";
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});
		});
	</script>
@endsection