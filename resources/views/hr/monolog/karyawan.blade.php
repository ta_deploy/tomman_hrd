@extends('layout')
@section('title')
Data Monolog
@endsection
@section('css')
<style>
  th {
    text-align: center;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    /* text-transform: uppercase; */
  }
  .upper {
    text-transform: uppercase;
  }
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div class="page-header">
	<div class="col-md-12">
		<div class="panel panel-info">
			<div class="panel-body">
				<a href="/hr/uploadData" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Upload Data</a>
				<form method="get">
					<div class="form-group">
						<div class="col-sm-12">
							<label for="input-witel">Witel</label>
							<select class="form-select" name="witel" id="input-witel">
								<option value="" selected disabled>Pilih Witel</option>
								@foreach ($get_witel as $witel)
								<option value="{{ $witel->id ? : $input_witel }}">{{ $witel->text }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<br/>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<ul class="nav nav-tabs">
			<li class="dropdown active">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">KARYAWAN</a>
				<ul class="dropdown-menu">
					<li class="active">
						<a href="#tabs-tidak-terdaftar" data-toggle="tab">
							TIDAK TERDAFTAR <span class="badge badge-default">{{ count($get_tidak_terdaftar) }}</span>
						</a>
					</li>
					<li>
						<a href="#tabs-karyawan-aktif" data-toggle="tab">
							KARYAWAN AKTIF <span class="badge badge-default">{{ count($get_karyawan_aktif) }}</span>
						</a>
					</li>
					<li>
						<a href="#tabs-karyawan-nonaktif" data-toggle="tab">
							KARYAWAN NON AKTIF <span class="badge badge-default">{{ count($get_karyawan_nonaktif) }}</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">MY-TECH SCMT</a>
				<ul class="dropdown-menu">
					<li>
						<a href="#tabs-mytech-scmt-active" data-toggle="tab">
						ACTIVE <span class="badge badge-default">{{ count($get_mytech_scmt_active) }}</span>
						</a>
					</li>
					<li>
						<a href="#tabs-mytech-scmt-blanks" data-toggle="tab">
							BLANKS <span class="badge badge-default">{{ count($get_mytech_scmt_blanks) }}</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">SCMT MY-TECH </a>
				<ul class="dropdown-menu">
					<li>
						<a href="#tabs-scmt-mytech-blanks" data-toggle="tab">
							BLANKS <span class="badge badge-default">{{ count($get_scmt_mytech_blanks) }}</span>
						</a>
					</li>
					<li>
						<a href="#tabs-scmt-mytech-active" data-toggle="tab">
							ACTIVE <span class="badge badge-default">{{ count($get_scmt_mytech_active) }}</span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
		<div class="tab-content tab-content-bordered">
			<div class="tab-pane fade in active" id="tabs-tidak-terdaftar">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="tidak_terdaftar" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>NIK</th>
									<th>Laborcode</th>
									<th>Nama</th>
									<th>Witel</th>
									<th>WH</th>
									<th>Status Karyawan</th>
									<th>NTE</th>
									<th>My-Tech</th>
									<th>SCMT</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_tidak_terdaftar as $key => $val)
									<tr>
										<td>{{ ++$key }}</td>
										<td>{{ $val->NIK }}</td>
										<td>{{ $val->LABORCODE }}</td>
										<td>{{ $val->NAMA_HR }}</td>
										<td>{{ $val->WITEL_HR }}</td>
										<td>{{ $val->WH }}</td>
										<td>{{ $val->STATUS_KARYAWAN }}</td>
										<td>{{ $val->NTE }}</td>
										<td>{{ $val->MY_TECH }}</td>
										<td>{{ $val->SCMT }}</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-karyawan-aktif">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="karyawan_aktif" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>NIK</th>
									<th>Laborcode</th>
									<th>Nama</th>
									<th>Witel</th>
									<th>WH</th>
									<th>Status Karyawan</th>
									<th>NTE</th>
									<th>My-Tech</th>
									<th>SCMT</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_karyawan_aktif as $key => $val)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val->NIK }}</td>
									<td>{{ $val->LABORCODE }}</td>
									<td>{{ $val->NAMA_HR }}</td>
									<td>{{ $val->WITEL_HR }}</td>
									<td>{{ $val->WH }}</td>
									<td>{{ $val->STATUS_KARYAWAN }}</td>
									<td>{{ $val->NTE }}</td>
									<td>{{ $val->MY_TECH }}</td>
									<td>{{ $val->SCMT }}</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-karyawan-nonaktif">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="karyawan_nonaktif" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>NIK</th>
									<th>Laborcode</th>
									<th>Nama</th>
									<th>Witel</th>
									<th>WH</th>
									<th>Status Karyawan</th>
									<th>NTE</th>
									<th>My-Tech</th>
									<th>SCMT</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_karyawan_nonaktif as $key => $val)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val->NIK }}</td>
									<td>{{ $val->LABORCODE }}</td>
									<td>{{ $val->NAMA_HR }}</td>
									<td>{{ $val->WITEL_HR }}</td>
									<td>{{ $val->WH }}</td>
									<td>{{ $val->STATUS_KARYAWAN }}</td>
									<td>{{ $val->NTE }}</td>
									<td>{{ $val->MY_TECH }}</td>
									<td>{{ $val->SCMT }}</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-mytech-scmt-active">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="mytech_scmt_active" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>NIK</th>
									<th>Laborcode</th>
									<th>Nama</th>
									<th>Witel</th>
									<th>WH</th>
									<th>Status Karyawan</th>
									<th>NTE</th>
									<th>My-Tech</th>
									<th>SCMT</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_mytech_scmt_active as $key => $val)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val->NIK }}</td>
									<td>{{ $val->LABORCODE }}</td>
									<td>{{ $val->NAMA_HR }}</td>
									<td>{{ $val->WITEL_HR }}</td>
									<td>{{ $val->WH }}</td>
									<td>{{ $val->STATUS_KARYAWAN }}</td>
									<td>{{ $val->NTE }}</td>
									<td>{{ $val->MY_TECH }}</td>
									<td>{{ $val->SCMT }}</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-mytech-scmt-blanks">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="mytech_scmt_blanks" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>NIK</th>
									<th>Laborcode</th>
									<th>Nama</th>
									<th>Witel</th>
									<th>WH</th>
									<th>Status Karyawan</th>
									<th>NTE</th>
									<th>My-Tech</th>
									<th>SCMT</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_mytech_scmt_blanks as $key => $val)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val->NIK }}</td>
									<td>{{ $val->LABORCODE }}</td>
									<td>{{ $val->NAMA_HR }}</td>
									<td>{{ $val->WITEL_HR }}</td>
									<td>{{ $val->WH }}</td>
									<td>{{ $val->STATUS_KARYAWAN }}</td>
									<td>{{ $val->NTE }}</td>
									<td>{{ $val->MY_TECH }}</td>
									<td>{{ $val->SCMT }}</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-scmt-mytech-blanks">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="scmt_mytech_blanks" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>NIK</th>
									<th>Laborcode</th>
									<th>Nama</th>
									<th>Witel</th>
									<th>WH</th>
									<th>Status Karyawan</th>
									<th>NTE</th>
									<th>My-Tech</th>
									<th>SCMT</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_scmt_mytech_blanks as $key => $val)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val->NIK }}</td>
									<td>{{ $val->LABORCODE }}</td>
									<td>{{ $val->NAMA_HR }}</td>
									<td>{{ $val->WITEL_HR }}</td>
									<td>{{ $val->WH }}</td>
									<td>{{ $val->STATUS_KARYAWAN }}</td>
									<td>{{ $val->NTE }}</td>
									<td>{{ $val->MY_TECH }}</td>
									<td>{{ $val->SCMT }}</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-scmt-mytech-active">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="scmt_mytech_active" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>NIK</th>
									<th>Laborcode</th>
									<th>Nama</th>
									<th>Witel</th>
									<th>WH</th>
									<th>Status Karyawan</th>
									<th>NTE</th>
									<th>My-Tech</th>
									<th>SCMT</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_scmt_mytech_active as $key => $val)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $val->NIK }}</td>
									<td>{{ $val->LABORCODE }}</td>
									<td>{{ $val->NAMA_HR }}</td>
									<td>{{ $val->WITEL_HR }}</td>
									<td>{{ $val->WH }}</td>
									<td>{{ $val->STATUS_KARYAWAN }}</td>
									<td>{{ $val->NTE }}</td>
									<td>{{ $val->MY_TECH }}</td>
									<td>{{ $val->SCMT }}</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js" integrity="sha512-taB73dM5L3JgciedVi4zU6fYBnX1tbhnWUtc/hdBTSgKIWgQnlNvkJYT4uJiin5GiJ2oXiIxrxqjD0Fy5orCxA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
	$( function() {

		$('select').select2();

		var file = String(document.location).split('#');
		console.log(file[1])

		if(typeof file[1] != 'undefined'){
			$('.nav-tabs a[href="#'+ file[1] +'"]').tab('show');
		}
		
		$('#tidak_terdaftar').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA KARYAWAN TIDAK TERDAFTAR MONOLOG PERWIRA'
			}]
		});

		$('#karyawan_aktif').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA KARYAWAN AKTIF PERWIRA'
			}]
		});

		$('#karyawan_nonaktif').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA KARYAWAN NONAKTIF PERWIRA'
			}]
		});

		$('#mytech_scmt_active').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA MYTECH SCMT ACTIVE PERWIRA'
			}]
		});

		$('#mytech_scmt_blanks').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA MYTECH SCMT BLANKS PERWIRA'
			}]
		});

		$('#scmt_mytech_blanks').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA SCMT MYTECH BLANKS PERWIRA'
			}]
		});

		$('#scmt_mytech_active').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA SCMT MYTECH ACTIVE PERWIRA'
			}]
		});

		$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
			$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
		} );

		$('#tidak_terdaftar').append('<caption style="caption-side: top; color: #000;">Karyawan Tidak Terdaftar</caption>');

		$('#karyawan_aktif').append('<caption style="caption-side: top; color: #000;">Karyawan Aktif</caption>');

		$('#karyawan_nonaktif').append('<caption style="caption-side: top; color: #000;">Karyawan Non Aktif</caption>');

		$('#mytech_scmt_active').append('<caption style="caption-side: top; color: #000;">MY-TECH SCMT Active</caption>');

		$('#mytech_scmt_blanks').append('<caption style="caption-side: top; color: #000;">MY-TECH SCMT Blanks</caption>');

		$('#scmt_mytech_blanks').append('<caption style="caption-side: top; color: #000;">SCMT MY-TECH Blanks</caption>');

		$('#scmt_mytech_active').append('<caption style="caption-side: top; color: #000;">MY-TECH Inactive SCMT Active</caption>');
	});
</script>
@endsection