@extends('layout')
@section('title')
Data Teknisi Tomman
@endsection
@section('css')
<style>
  th {
    text-align: center;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    /* text-transform: uppercase; */
  }
  .upper {
    text-transform: uppercase;
  }
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div class="page-header">
	<div class="col-md-12">
		<div class="panel panel-info">
			<div class="panel-body">
				<form method="get">
					<div class="form-group">
						<div class="col-sm-12">
							<label for="input-witel">Witel</label>
							<select class="form-select" name="witel" id="input-witel">
								<option value="" selected disabled>Pilih Witel</option>
								@foreach ($get_witel as $witel)
								<option value="{{ $witel->id ? : $input_witel }}">{{ $witel->text }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<br/>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<ul class="nav nav-tabs nav-tabs-simple">
			<li class="active">
				<a href="#tabs-get_aktif_tomman" data-toggle="tab">
					AKTIF <span class="badge badge-default">{{ count($get_aktif_tomman) }}</span>
				</a>
			</li>
			<li>
				<a href="#tabs-get_non_aktif_reg" data-toggle="tab">
					NONAKTIF <span class="badge badge-default">{{ count($get_non_aktif_reg) }}</span>
				</a>
			</li>
			<li>
				<a href="#tabs-get_no_profil" data-toggle="tab">
					TIDAK ADA PROFIL <span class="badge badge-default">{{ count($get_no_profil) }}</span>
				</a>
			</li>
		</ul>
		<div class="tab-content tab-content-bordered">
			<div class="tab-pane fade in active" id="tabs-get_aktif_tomman">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="tidak_terdaftar" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>ID User</th>
									<th>Nama</th>
									<th>KTP</th>
									<th>Nomor Telepon</th>
									<th>ID Telegram</th>
									<th>Jenis Kelamin</th>
									<th>Witel</th>
									<th>Mitra</th>
									<th>Posisi Lapangan</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_aktif_tomman as $k => $v)
									<tr>
										<td>{{ ++$k }}</td>
										<td>{{ $v->id_user }}</td>
										<td>{{ $v->nama }}</td>
										<td>{{ $v->noktp }}</td>
										<td>{{ $v->no_telp }}</td>
										<td>{{ $v->username_telegram }}</td>
										<td>{{ $v->jk }}</td>
										<td>{{ $v->Witel_New }}</td>
										<td>{{ $v->mitra_amija ?? $v->mitra_pt }}</td>
										<td>{{ $v->position }}</td>
										<td>
											@if($v->id_people)
												<a href="/info_search/{{ $v->id_people }}" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-tool"></span> Edit Data</a>
											@else
												Tidak Ada Data Profil!
											@endif
										</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-get_non_aktif_reg">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="karyawan_aktif" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>ID User</th>
									<th>Nama</th>
									<th>KTP</th>
									<th>Nomor Telepon</th>
									<th>ID Telegram</th>
									<th>Jenis Kelamin</th>
									<th>Witel</th>
									<th>Mitra</th>
									<th>Posisi Lapangan</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_non_aktif_reg as $k => $v)
									<tr>
										<td>{{ ++$k }}</td>
										<td>{{ $v->id_user }}</td>
										<td>{{ $v->nama }}</td>
										<td>{{ $v->noktp }}</td>
										<td>{{ $v->no_telp }}</td>
										<td>{{ $v->username_telegram }}</td>
										<td>{{ $v->jk }}</td>
										<td>{{ $v->Witel_New }}</td>
										<td>{{ $v->mitra_amija ?? $v->mitra_pt }}</td>
										<td>{{ $v->position }}</td>
										<td>
											@if($v->id_people)
												<a href="/info_search/{{ $v->id_people }}" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-tool"></span> Edit Data</a>
											@else
												Tidak Ada Data Profil!
											@endif
										</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-get_no_profil">
				<div class="page-header">
					<div class="table-default table-responsive">
						<table class="table table-bordered table-striped table-small-font" id="karyawan_nonaktif" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>ID User</th>
									<th>Nama</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse($get_no_profil as $k => $v)
									<tr>
										<td>{{ ++$k }}</td>
										<td>{{ $v->id_user }}</td>
										<td>{{ $v->nama ?? '-' }}</td>
										<td>{{ $v->psb_remember_token ? 'AKTIF' : 'NONAKTIF' }}</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js" integrity="sha512-taB73dM5L3JgciedVi4zU6fYBnX1tbhnWUtc/hdBTSgKIWgQnlNvkJYT4uJiin5GiJ2oXiIxrxqjD0Fy5orCxA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
	$( function() {

		$('select').select2();

		var file = String(document.location).split('#');
		console.log(file[1])

		if(typeof file[1] != 'undefined'){
			$('.nav-tabs a[href="#'+ file[1] +'"]').tab('show');
		}

		$('#tidak_terdaftar').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA KARYAWAN TIDAK TERDAFTAR MONOLOG PERWIRA'
			}]
		});

		$('#karyawan_aktif').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA KARYAWAN AKTIF PERWIRA'
			}]
		});

		$('#karyawan_nonaktif').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA KARYAWAN NONAKTIF PERWIRA'
			}]
		});

		$('#mytech_scmt_active').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA MYTECH SCMT ACTIVE PERWIRA'
			}]
		});

		$('#mytech_scmt_blanks').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA MYTECH SCMT BLANKS PERWIRA'
			}]
		});

		$('#scmt_mytech_blanks').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA SCMT MYTECH BLANKS PERWIRA'
			}]
		});

		$('#scmt_mytech_active').DataTable({
			select: true,
			dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			buttons: [{
				extend: 'excel',
				text: 'Download to Excel',
				title: 'DATA SCMT MYTECH ACTIVE PERWIRA'
			}]
		});

		$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
			$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
		} );

		$('#tidak_terdaftar').append('<caption style="caption-side: top; color: #000;">Karyawan Tidak Terdaftar</caption>');

		$('#karyawan_aktif').append('<caption style="caption-side: top; color: #000;">Karyawan Aktif</caption>');

		$('#karyawan_nonaktif').append('<caption style="caption-side: top; color: #000;">Karyawan Non Aktif</caption>');

		$('#mytech_scmt_active').append('<caption style="caption-side: top; color: #000;">MY-TECH SCMT Active</caption>');

		$('#mytech_scmt_blanks').append('<caption style="caption-side: top; color: #000;">MY-TECH SCMT Blanks</caption>');

		$('#scmt_mytech_blanks').append('<caption style="caption-side: top; color: #000;">SCMT MY-TECH Blanks</caption>');

		$('#scmt_mytech_active').append('<caption style="caption-side: top; color: #000;">MY-TECH Inactive SCMT Active</caption>');
	});
</script>
@endsection