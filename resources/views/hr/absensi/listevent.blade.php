@extends('layout')
@section('title')
	List Event
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<div class="col-md-12">
		<div class="panel panel-default ">
			<div class="table-info">
				<div class="table-header">
			    <div class="table-caption">
			      List of Event
			      <a href="/event_list/input" class="btn btn-xs btn-success pull-right"><i class="ion ion-plus"></i> Create</a>
			    </div>
			  </div>
				<table class="table table-bordered table-striped table-small-font" id="table_acc">
					<thead>
						<tr>
							<th class="text-center">No</th>
							<th>Nama Event</th>
							<th>Tanggal</th>
							<th>Set By</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody class="middle-align">
						@foreach ($data as $key => $d)
							<tr>
								<td>{{ ++$key }}</td>
								<td>{{ $d->nama_event }}</td>
								<td>{{ $d->tgl }}</td>
								<td>{{ $d->user }}</td>
								<td><a href="/scanrfid/{{ $d->id }}" class="btn btn-xs btn-primary"><i class="ion ion-camera"></i> Scan</a> |
									<a href="/event_list/{{ $d->id }}" class="btn btn-xs btn-info"><i class="ion ion-edit"></i> Edit</a> |
									<a href="/event_download/{{ $d->id }}" class="btn btn-xs btn-warning"><i class="ion ion-android-download"></i> Download Excel</a> |
									<a href="/event_delete/{{ $d->id }}" class="btn btn-xs btn-danger"><i class="ion ion-trash-a"></i> Delete</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')

@endsection