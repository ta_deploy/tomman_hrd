@extends('layout_nologin')
@section('title')
Absensi
@endsection
@section('content')
<link rel="stylesheet" type="text/css" href="https://jeremyfagis.github.io/dropify/dist/css/dropify.min.css">

<div class="px-content">
@include('Partial.alerts')

    <div class="panel">
        <div class="panel-heading">
          <div class="panel-title">Kehadiran Apel Disiplin Operasi Warrior (DOW)</div>
        </div>
        <div class="panel-body" id="form_absen">

          <form method="post" action="/absen_dow_save" enctype="multipart/form-data" autocomplete="off">

            <input type="hidden" name="nik" value="{{ $nik }} ">
            <input type="hidden" name="nama" value="{{ $nama }} ">
            <input type="hidden" name="gps_lat" id="gps_lat">
            <input type="hidden" name="gps_long" id="gps_long">

            <fieldset class="form-group">
              <div class="col-md-12">
                @php
                  $name   = date('Ymd');
                  $path4  = "/upload4/absen_dow/".$nik;
                  $th4    = "$path4/$name-th.jpg";
                  $path   = null;

                  if (file_exists(public_path().$th4) )
                  {
                    $path = "$path4/$name";
                  }

                  if($path)
                  {
                      $img    = "$path.jpg";
                      $th     = "$path-th.jpg";
                  } else {
                      $img    = null;
                      $th     = null;
                  }
                @endphp
                @if ($th == null)
                  @php
                    $img_file = '';
                  @endphp
                @else
                  @php
                    $img_file = $img;
                  @endphp
                @endif
                <input type="file" accept="image/*" name="img_dow" class="dropify" data-default-file="{{ $img_file }}" data-height="500" required/>
                <br /><br />
                <center><small class="text-muted">Harap foto yang di upload menggunakan aplikasi TimeStamp Camera.</small></center>
              </div>
            </fieldset>

            <fieldset class="form-group">
              <div class="col-md-12">
                <label for="switcher-basic" class="switcher switcher-success">
                  <input type="checkbox" id="switcher-basic" required>
                  <div class="switcher-indicator" onclick="getLocation()">
                    <div class="switcher-yes">YES</div>
                    <div class="switcher-no">NO</div>
                  </div>
                  Use Current My Location
                </label>
              </div>
            </fieldset>

            <fieldset class="form-group">
              <div class="col-md-12">
                <button class="btn btn-primary btn-outline btn-rounded btn-block">Upload!</button>
              </div>
            </fieldset>

          </form>
        </div>
      </div>
    </div>

</div>
@endsection

@section('js')
<script>
  var x = document.getElementById("form_absen");

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
      x.innerHTML = "Geolocation is not supported by this browser.";
    }
  }

  function showPosition(position) {
    console.log(position.coords.latitude + ',' + position.coords.longitude)
  }

  function showError(error) {
    switch(error.code) {
      case error.PERMISSION_DENIED:
        x.innerHTML = "User denied the request for Geolocation."
        break;
      case error.POSITION_UNAVAILABLE:
        x.innerHTML = "Location information is unavailable."
        break;
      case error.TIMEOUT:
        x.innerHTML = "The request to get user location timed out."
        break;
      case error.UNKNOWN_ERROR:
        x.innerHTML = "An unknown error occurred."
        break;
    }
  }
</script>
<script type="text/javascript">
  window.onload = function() {
      var startPos;
      var geoOptions = {
        enableHighAccuracy: true
      }

      var geoSuccess = function(position) {
        startPos = position;
        document.getElementById('gps_lat').value = startPos.coords.latitude;
        document.getElementById('gps_long').value = startPos.coords.longitude;
      };
      var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    }
</script>
<script type="text/javascript">
  $(function() {
    $('.select2').select2({
        placeholder: 'Pilih Status Kehadiran',
    });

    $('.dropify').dropify();
  });
</script>
<script type="text/javascript" src="https://jeremyfagis.github.io/dropify/dist/js/dropify.min.js"></script>
@endsection