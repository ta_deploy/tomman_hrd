@extends('layout')
@section('title')
Absensi
@endsection
@section('content')

<div class="px-content">
@include('Partial.alerts')
  <input type="hidden" name="event_id" id="event_id" value="{{ Request::segment(2) }}">
  <div class="panel">
    <div class="panel-heading">
      <div class="panel-title">ABSENSI KEHADIRAN {{ $data->nama_event }} TANGGAL {{ $data->tgl }} </div>
    </div>
    <div class="panel-body">
      {!! $html !!}
    </div>
  </div>
  <div class="modal fade modal-alert modal-warning" id="modal_regist" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"><i class="fa fa-warning"></i></div>
        <div class="modal-title">Kartu Tidak Dikenali</div>
        <div class="modal-body">Silahkan Register atau Close.</div>
        <div class="modal-footer">
          <a href="/" class="btn btn-primary" id="regist_link">Register</a>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-alert modal-primary" id="modal_wait" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"><i class="fa fa-warning"></i></div>
        <div class="modal-title">LOADING....</div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="/lib/rfid/src/jquery.rfid.js"></script>
<script type="text/javascript">
    var rfidParser = function (rawData) {
      console.log(rawData, rawData.length);
        if (rawData.length != 11) return null;
      else return rawData;

    };

    // Called on a good scan (company card recognized)
    var goodScan = function (cardData) {
      $('#modal_wait').modal('show');
      $.post("/scanned_rfid", { rfid: cardData.substr(0,10),event_id: $('#event_id').val()})
        .done(function( data ) {
          $('#modal_wait').modal('hide');
          if(data.code==1){
            $('#regist_link').attr("href", "/rfid_register/"+cardData.substr(0,10)+"/"+$('#event_id').val() )
            $('#modal_regist').modal('show');
          }else{
            if(data.code==3){
              $('.panel-body').html(data.html)
            }
            toastr.warning(data.msg+' Attended!', "Berhasil");
            // alert( "Data Loaded: " + data.msg );
            $('body').focus();
          }

        });
    };

    // Called on a bad scan (company card not recognized)
    var badScan = function() {

    };

    // Initialize the plugin.
    $.rfidscan({
      parser: rfidParser,
      success: goodScan,
      error: badScan
    });
    $("#modal_regist").on("hidden.bs.modal", function(){
      $('body').focus();
    });
  </script>
@endsection