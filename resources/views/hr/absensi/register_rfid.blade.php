@extends('layout_nologin')
@section('title')
Register
@endsection
@section('content')

<div class="px-content">
@include('Partial.alerts')

  <div class="panel">
    <div class="panel-heading">
      <div class="panel-title">Register RFID {{ Request::segment(2) }}</div>
      <div class="panel-body">
        <form class="form-horizontal" method="post" action="/rfid_register">
          <input type="hidden" name="event_id" value="{{ Request::segment(3) }}"/>
          <div class="form-group">
            <label for="grid-input-5" class="col-md-3 control-label">RFID</label>
            <div class="col-md-9">
              <input class="form-control" name="rfid" value="{{ Request::segment(2) }}" id="rfid" readonly />
            </div>
          </div>
          <div class="form-group">
            <label for="grid-input-5" class="col-md-3 control-label">Nik</label>
            <div class="col-md-9">
              <input class="form-control" name="nik" id="nik"/>
            </div>
          </div>
          <div class="form-group">
            <label for="grid-input-5" class="col-md-3 control-label">Jenis Kartu</label>
            <div class="col-md-9">
              <select class="custom-select form-control" name="card_type" id="card_type">
                <option value="PASSCARD">PASSCARD</option>
                <option value="KTP">KTP</option>
                <option value="SIM">SIM</option>
                <option value="RFID CARD">RFID CARD</option>
                <option value="RFID KEY CHAIN">RFID KEY CHAIN</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn">Submit</button>
          </div>
        </div>
      </form>
    </div>
      
  </div>
</div>
@endsection

@section('js')

@endsection