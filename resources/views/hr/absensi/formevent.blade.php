@extends('layout')
@section('title')
	Form Event
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="col-md-12">
			<div class="panel panel-color panel-danger panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Form Event</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="tgl">Tanggal :</label>
						<div class="col-md-9">
							<input type="text" name="tgl" class="form-control" id="tgl" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="nama_event">Nama Event :</label>
						<div class="col-md-9">
							<input type="text" name="nama_event" class="form-control" id="nama_event">
						</div>
					</div>
					<div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
            </div>
          </div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script>
		$( function(){
			$('#tgl').datepicker({
        todayBtn:              'linked',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',

        beforeShowMonth: function(date) {
          if (date.getMonth() === 8) {
            return false;
          }
        },

        beforeShowYear: function(date){
          if (date.getFullYear() === 2014) {
            return false;
          }
        }
      });
		});
	</script>
@endsection