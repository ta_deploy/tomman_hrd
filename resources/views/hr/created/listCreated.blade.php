@extends('layout')
@section('title')
List Pembuatan Akun
@endsection
@section('css')
<style>
  th {
    text-align: center;
	text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
@include('Partial.alerts')
	<a href="/hr/createNew_user/input" type="button" class="btn btn-primary btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Pembuatan User</a>
  <div class="page-header">
		<div class="table-primary table-responsive">
			<table class="table table-bordered table-striped" id="table_ku">
				<thead>
					<tr>
						<th rowspan="2">NO</th>
						<th rowspan="2">NAMA</th>
						<th rowspan="2">NIK</th>
						<th rowspan="2">WITEL</th>
						<th rowspan="2">MITRA</th>
						<th colspan="6">JENIS USER</th>
					</tr>
					<tr>
						<th>PSB</th>
						<th>MARINA</th>
						<th>PT-2</th>
						<th>PERWIRA</th>
						<th>PROMISE</th>
						<th>PROJECT</th>
					</tr>
				</thead>
				<tbody class="middle-align">
					@foreach ($get_data as $num => $data)
					<tr>
						<td>{{ ++$num }}</td>
						<td>{{ $data->nama }}</td>
						<td>{{ $data->id_nik }}</td>
						<td>{{ $data->witel }}</td>
						<td>{{ $data->mitra }}</td>
						<td>{{ $data->psb_level }}</td>
						<td>{{ $data->maintenance_level }}</td>
						<td>{{ $data->pt2_level }}</td>
						<td>{{ $data->perwira_level }}</td>
						<td>{{ $data->proc_level }}</td>
						<td>{{ $data->promise_level ? : '0' }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});

			$('#table_ku_wrapper .table-caption').text('List User Dengan Akun');
			$('#table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection