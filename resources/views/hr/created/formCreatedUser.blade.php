@extends('layout')
@section('title')
Form Pembuatan User
@endsection
@section('content')
<div class="px-content">
@include('Partial.alerts')
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="col-md-12">
			<div class="panel panel-color panel-info panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Pembuatan User</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="nik">NIK :</label>
						<div class="col-md-9">
							<select name="nik" class="form-control" id="nik">
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="jenis">Jenis :</label>
						<div class="col-md-9">
							<select name="jenis" class="form-control" id="jenis">
								<option value="MYIT">MYI-Technition</option>
								<option value="Tomman">Tomman</option>
							</select>
						</div>
					</div>
					<div class="form-group stts_tomman">
						<label class="col-sm-3 control-label" for="tomman">Tomman :</label>
						<div class="col-md-9">
							<select name="tomman" class="form-control" id="tomman">
								@foreach ($only_tomman as $v)
									<option value="{{ $v }}">{{ $v }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group tomman_lvl">
						<label class="col-sm-3 control-label" for="level">Level :</label>
						<div class="col-md-9">
							<select name="level" class="form-control" id="level"></select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="status">Status :</label>
						<div class="col-md-9">
							<select name="status" class="form-control" id="status">
								<option value="Active">Active</option>
								<option value="Non-Active">Non-Active</option>
							</select>
						</div>
					</div>
					<div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
            </div>
          </div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			if($('#jenis').select2().val() == 'MYIT'){
				$('.stts_tomman, .tomman_lvl').css({
					'display': 'none'
				});
			}

			$('#jenis').on('select2:select', function() {
				if($(this).val() == 'MYIT'){
					$('.stts_tomman').css({
						'display': 'none'
					});
				}else{
					$('.stts_tomman').css({
						'display': 'block'
					});
				}
			});

			$('#tomman').val(null).trigger('change');
			$('#tomman').select2({
				allowClear: true,
				width: '100%',
				placeholder: 'Pilih Jenis Tomman',
			});

			var data_tomman = {!! json_encode($get_data) !!};
			// console.log(data_tomman);
			$('#tomman').on('select2:select', function() {

			var data = [];
				$('.tomman_lvl').css({
					'display': 'block'
				});
				$('#level').empty().trigger('change');

				$.each(data_tomman, function(k, v){
					if($('#tomman').val() == v.tomman)
					{
						data.push({
							id: v.level,
							text: v.aktor
						});
					}
				});
				console.log(data)
				$('#level').select2({
					'data': data
				});
			});

			$('#nik').select2({
				width: '100%',
				placeholder: "Masukan Username / NIK",
				allowClear: true,
				minimumInputLength: 3,
				ajax: {
					url: '/jx_dng/get_user',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				language: {
					noResults: function(){
						return "NIK Tidak Ditemukan :(";
					}
				}
			});
		});
	</script>
@endsection