@extends('layout')
@section('title')
List Resign
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
  <div class="page-header">
			<form method="post" class="form-horizontal">
			<div class="input-group" >
				<div class="col-sm-1">
					<label class="control-label" for="input-status">Filter Per</label>
				</div>	
				<div class="col-sm-3">	
					<select name="cari" id="input-cari" class="form-control selectboxit">
						<option value="1">Bulan</option>
						<option value="2">Tahun</option>
					</select>
				</div>	
				<div class="col-sm-4">	
					<input  name="tgl" type="text" class="form-control datepicker" >				
				</div>
		      <span class="input-group-btn">
		      	<button class="btn btn-blue btn-icon btn-icon-standalone btn-icon-standalone-right" type="submit">
					<i class="fa-search"></i>
					<span>Search</span>
				</button>
		      </span>
		    </div>
			</form>
		</div>
	</div>

	<div class="panel panel-default ">
		<table class="table table-bordered table-striped table-small-font" id="example-2">
			<thead>
				<tr>
					<th>No</th>
					<th>NIK</th>
					<th>Nama</th>
					<th>PSA</th>
					<th>Position Name</th>
					<th>Tanggal</th>
					<th>Kronologi Kejadian</th>
					<th>Detil Cedera</th>
					<th>Alamat Kejadian</th>
					<th>Tindakan</th>
					<th>No BPJS</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody class="middle-align">
				<?php $no=1; ?>
				@foreach($list as $r)
				<tr>
					<td >{{$no}}</td>
					<td >{{$r->nik}}</td>
					<td >{{$r->nama}}</td>
					<td >{{$r->psa}}</td>
					<td >{{$r->position_name}}</td>
					<td >{{$r->tgl}}</td>
					<td >{{$r->kronologi_kejadian}}</td>
					<td >{{$r->detail_cedera}}</td>
					<td >{{$r->alamat}}</td>
					<td >{{$r->tindakan}}</td>
					<td >{{$r->bpjs}}</td>
					<td >
						<a href="/hr/formAccident/{{$r->id}}" class="btn btn-secondary btn-sm btn-icon icon-left fa-edit tooltip-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" >					
						</a>
						<form method="POST" action="/hr/deleteAccident/{{$r->id}}" accept-charset="UTF-8" style="display:inline"
							class="tooltip-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Kecelakaan Kerja">
		                  <button class="btn btn-danger btn-sm btn-icon icon-left fa-trash tooltip-danger" 
		                  type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Hapus Data ?" data-message="Are you sure you want to delete this ?">
		                  </button>
		                </form>	
					</td>
				</tr>
				<?php $no++; ?>
				@endforeach
			</tbody>
		</table>
	</div>

@endsection

@section('js')
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="/lib/xenon/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/lib/xenon/js/datatables/dataTables.bootstrap.js"></script>
	<script src="/lib/xenon/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="/lib/xenon/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
	<script src="/lib/xenon/js/datepicker/bootstrap-datepicker.js"></script>
	<script src="/lib/xenon/js/timepicker/bootstrap-timepicker.min.js"></script>

	<script src="/lib/xenon/js/rwd-table/js/rwd-table.min.js"></script>
  	<script src="//cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.4.0/clipboard.min.js"></script>

	<script>
	var tanggal =$(".datepicker").datepicker( {
	    format: "yyyy-mm",
	    startView: "months", 
	    minViewMode: "months",
	    autoclose: true
	});
	//$("#input-cari").selectboxit();
	$('#input-cari').change( function (){
		if($( this ).val() == 1){
			$(tanggal).datepicker("remove");
			$(tanggal).datepicker( {
			    format: "yyyy-mm",
			    startView: "months", 
			    minViewMode: "months",
			    autoclose: true
			});
		}
		else if($( this ).val() == 2){
			$(tanggal).datepicker("remove");
			$(tanggal).datepicker( {
			    format: "yyyy",
			    startView: "years", 
			    minViewMode: "years",
			    autoclose: true
			});
		}
	});

	$("#example-2").DataTable({
	   	scrollY: 1000,
	   	"scrollX": true,
	   	"scrollCollapse": true,
	   	"autoWidth": true
	   });

	$('#confirmDelete').on('show.bs.modal', function (e) {
      $message = $(e.relatedTarget).attr('data-message');
      $(this).find('.modal-body p').text($message);
      $title = $(e.relatedTarget).attr('data-title');
      $(this).find('.modal-title').text($title);
      // Pass form reference to modal for submission on yes/ok
      var form = $(e.relatedTarget).closest('form');
      $(this).find('.modal-footer #confirm').data('form', form);
    });
    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
        $(this).data('form').submit();
    });	
  </script>
@endsection