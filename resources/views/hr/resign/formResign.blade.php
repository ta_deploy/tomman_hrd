@extends('layout')
@section('title')
Form Resign Karyawan
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="col-md-12">
			<div class="panel panel-color panel-danger panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Resign Karyawan</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="tgl">Tanggal :</label>
						<div class="col-md-9">
							<input type="text" name="tgl" class="form-control" id="tgl">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="nama">Nik Karyawan :</label>
						<div class="col-md-9">
							<select name="nama" class="form-control" id="nama"></select>
						</div>
						<div class="detail_data"></div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="jenis">Jenis :</label>
						<div class="col-sm-9">
							<select class="form-control" name="jenis" id="jenis">
								@if (strtolower(Request::segment(1) ) != 'mitra')
									<option value="Konseling">Konseling</option>
								@endif
								<option value="Resign">Resign</option>
							</select>
						</div>
					</div>
					@if (strtolower(Request::segment(1) ) != 'mitra')
						<div class="form-group konseling">
							<label class="col-sm-3 control-label" for="jenis_kon">Konseling</label>
							<div class="col-sm-9">
								<select class="form-control" name="jenis_kon" id="jenis_kon">
									<option value="SP 1">SP 1</option>
									<option value="SP 2">SP 2</option>
									<option value="SP 3">SP 3</option>
								</select>
							</div>
						</div>
						<div class="form-group konseling">
							<label class="col-sm-3 control-label" for="detail_langgar">Keterangan Pelanggaran :</label>
							<div class="col-md-9">
								<textarea style="resize: none" rows="4" name="detail_langgar" class="form-control" id="detail_langgar"></textarea>
							</div>
						</div>
						<div class="form-group konseling">
							<label class="col-sm-3 control-label" for="tndk_lanjut">Tindak Lanjut :</label>
							<div class="col-md-9">
								<textarea style="resize: none" rows="4" name="tndk_lanjut" class="form-control" id="tndk_lanjut"></textarea>
							</div>
						</div>
					@endif
					<div class="form-group resign">
						<label class="col-md-3 control-label" for="jenis_res">Resign :</label>
						<div class="col-sm-9">
							<select class="form-control" name="jenis_res" id="jenis_res">
								<option value="Terhormat">Terhormat</option>
								<option value="BLACKLIST">Blacklist</option>
							</select>
						</div>
					</div>
					<div class="form-group resign">
						<label class="col-sm-3 control-label" for="detail_blacklst">Keterangan :</label>
						<div class="col-md-9">
							<textarea style="resize: none" rows="4" name="detail_blacklst" class="form-control" id="detail_blacklst"></textarea>
						</div>
					</div>
					<div class="form-group resign">
						<label class="col-md-3 control-label" for="bukti">Bukti Resign :</label>
						<div class="col-sm-9">
							<input type="file" class="form-control" name="bukti" id="bukti">
						</div>
					</div>
					<div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
            </div>
          </div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			$('#jenis').val(null).trigger('change');

			$('#jenis').select2({
				allowClear: true,
				width: '100%',
				placeholder: 'Ketik Jenisnya!',
			});

			$('.konseling, .resign').css({
				'display': 'none'
			});

			var raw_class= ['.konseling', '.resign'];

			$('#jenis').on('select2:select', function(){
				if($(this).val() == 'Konseling'){
					var value = '.konseling';
					kelas = raw_class.filter(function(item) {
							return item !== value
					});
					console.log(kelas)

					$('.konseling').css({
						'display': 'block'
					});


					$(kelas.join(',') ).css({'display': 'none'});
				}else{
					var value = '.resign';

					$('.resign').css({
						'display': 'block'
					});

					kelas = raw_class.filter(function(item) {
							return item !== value
					});
					$(kelas.join(',') ).css({'display': 'none'});
				}
			});

			$('#tgl').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',
        daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',

        beforeShowMonth: function(date) {
          if (date.getMonth() === 8) {
            return false;
          }
        },

        beforeShowYear: function(date){
          if (date.getFullYear() === 2014) {
            return false;
          }
        }
      });

			$('#nama').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: {
					url: "/jx_dng/get_user",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Nik Tidak Terdaftar!";
						}
					},
					cache: true,
					success: function(value) {
						$('.title_kar').html('Resign Karyawan', value);
					}
				}
			});
		});
	</script>
@endsection