@extends('layout')
@section('title')
Upload Data
@endsection
@section('css')
<style>
.dropzone {
    background: white;
    border-radius: 5px;
    border: 2px dashed rgb(0, 135, 247);
    border-image: none;
    max-width: 500px;
    margin-left: auto;
    margin-right: auto;
}
</style>
@endsection
@section('content')
<div class="px-content">
@include('Partial.alerts')
<div class="panel">
  <div class="panel-heading">
    <div class="panel-title">Upload File</div>
  </div>
  <div class="panel-body">
    <form action="/hr/uploadData" method="POST" enctype="multipart/form-data" autocomplete="off">
      <div class="form-group">
      <select class="form-control" name="keterangan" id="input-keterangan">
						<option value="" selected disabled>Pilih Keterangan</option>
							<option value="MONOLOG">MONOLOG</option>
							<option value="BREVERT_TEKNISI">BREVERT TEKNISI</option>
							<option value="EMPLOYEE_TA">EMPLOYEE TA</option>
              <option value="EMPLOYEE_MITRA">EMPLOYEE MITRA AKTIF</option>
              <option value="EMPLOYEE_MITRA_NONAKTIF">EMPLOYEE MITRA NONAKTIF</option>
						</select>
        {!! $errors->first('keterangan', '<span class="label label-danger">:message</span>') !!}
      </div>
      <div class="form-group">
        <label class="control-label" for="upload">Import Excel</label>
        <input type="file" id="upload" name="upload" class="form-control"/>
        {!! $errors->first('upload', '<span class="label label-danger">:message</span>') !!}
      </div>

      <button type="submit" class="btn btn-primary pencet">Simpan Laporan</button>
    </form>
</div>
</div>
@endsection