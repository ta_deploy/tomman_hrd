@extends('layout')
@section('title')
Input Mitra
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="panel panel-color panel-danger panel-border">
				<div class="panel-heading">
					Form Pengisian Mitra
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="mitra_nm">Nama Mitra :</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="mitra_nm" id="mitra_nm">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="mitra_alias">Nama Alias :</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="mitra_alias" id="mitra_alias">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="kategori">Kategori :</label>
						<div class="col-sm-10">
							<select class="form-control" name="kategori" id="kategori">
								<option value="IOAN">IOAN</option>
								<option value="DELTA">DELTA</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="status">Status :</label>
						<div class="col-sm-10">
							<select class="form-control" name="status" id="status">
								<option value="Active">Active</option>
								<option value="Non-Active">Non-Active</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class=" col-sm-12">
							<button class="btn btn-block btn-primary" type="submit">
								<span class="fa fa-cloud-download"></span>
								<span>Simpan</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>	
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();
		});
	</script>
@endsection