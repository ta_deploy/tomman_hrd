@extends('layout')
@section('title')
List Mitra
@endsection
@section('css')
<style>
  th {
    text-align: center;
	text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
	<a href="/hr/created/mitra/input" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Mitra Baru</a>
  <div class="page-header">
		<div class="table-info">
			<table class="table table-bordered table-striped table-small-font" id="table_mitra">
				<thead>
					<tr>
						<th>ID MITRA</th>
						<th>ALIAS</th>
						<th>KATEGORI</th>
						<th>NAMA MITRA</th>
					</tr>
				</thead>
				<tbody class="middle-align">
				@foreach($get_mitra as $num => $mitra)
  					<tr>
						<td>{{ $mitra->mitra_amija_id }}</td>
						<td>{{ $mitra->mitra_amija }}</td>
						<td>{{ $mitra->kat }}</td>
						<td>{{ $mitra->mitra_amija_pt }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
	$(".table").DataTable({
			"columnDefs": [{
				"targets": 'no-sort',
				"orderable": false,
				}]
		});

		$('#table_mitra_wrapper .table-caption').text('LIST MITRA');
		$('#table_mitra_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
  </script>
@endsection