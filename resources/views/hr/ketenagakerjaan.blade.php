@extends('layout')

@section('html-title')
	 HR :: TOMMAN
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Project : 
@endsection

@section('page-title')
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2.css">
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2-bootstrap.css">
	<style>
		#keperluan{
			height : 98px;
			resize : none;
		}
	</style>
	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
		<li><i class="linecons-calendar"></i>HR</li>
		<li class="active"><i class="linecons-tag">Ketenagakerjaan</i> </li>
	</ol>
@endsection

@section('body')
	<div class="row">
  		<div class="col-sm-4">
			<a href="/hr/listKonseling">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-pencil"></i>
				</div>
				<div class="xe-label">
					<strong >Konseling & SP</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="/hr/listReward">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-star"></i>
				</div>
				<div class="xe-label">
					<strong >Reward & Inovasi</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="/hr/listAccident">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-beaker"></i>
				</div>
				<div class="xe-label">
					<strong >Kecelakaan Kerja</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
	</div>
@endsection

@section('plugins')
	<script src="/lib/xenon/js/datepicker/bootstrap-datepicker.js"></script>
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/select2/select2.min.js"></script>
	<script src="/lib/xenon/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="/lib/xenon/js/inputmask/jquery.inputmask.bundle.js"></script>
@endsection

@section('footer')
<script>
		
</script>

@endsection