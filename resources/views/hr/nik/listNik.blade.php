@extends('layout')
@section('title')
List Karyawan
@endsection
@section('css')
<style>
  th {
    text-align: center;
	text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<a href="/hr/submission/karyawan_new/input" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Karyawan Baru</a>
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tabs-ada_nik" data-toggle="tab">
				Memiliki NIK <span class="label label-success">{{ count($get_ada) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-kdd_nik" data-toggle="tab">
				Belum Punya NIK <span class="badge badge-warning">{{ count($get_request) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-proses_aju" data-toggle="tab">
				Proses Pengajuan <span class="badge badge-info">{{ count($get_process) }}</span>
			</a>
		</li>
	</ul>
	<div class="tab-content tab-content-bordered">
		<div class="tab-pane fade in active" id="tabs-ada_nik">
			<div class="page-header">
				<div class="table-success">
					<table class="table table_ada table-bordered table-striped table-small-font" style="width: 100%;" id="ada_nik">
						<thead>
							<tr>
								<th>NO</th>
								<th>NAMA</th>
								<th>NIK</th>
								<th>MITRA</th>
								<th>UPDATED AT</th>
							</tr>
						</thead>
						<tbody class="middle-align">
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-kdd_nik">
			<div class="page-header">
				<div class="table-warning">
					<table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="kdd_nik">
						<thead>
							<tr>
								<th>NO</th>
								<th>NAMA</th>
								<th>NO KTP</th>
								<th>MITRA</th>
								<th>CREATED AT</th>
								<th class="no-sort">ACTION</th>
							</tr>
						</thead>
						<tbody class="middle-align">
						@foreach($get_request as $num => $request)
							<tr>
								<td>{{ ++$num }}</td>
								<td>{{ $request->nama }}</td>
								<td>{{ $request->noktp }}</td>
								<td>{{ $request->mitra }}</td>
								<td>{{ $request->dateUpdate }}</td>
								<td><a href="/hr/submission/nik/{{ $request->id_people }}" type="button" class="btn btn-info btn-block btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> PROCESS</a></td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-proses_aju">
			<div class="page-header">
				<div class="table-info">
					<table class="table next_tbl table-bordered table-striped table-small-font" style="width: 100%;" id="proses_aju">
						<thead>
							<tr>
								<th>NO</th>
								<th>NAMA</th>
								<th>NO KTP</th>
								<th>MITRA</th>
								<th>TANGGAL PENGAJUAN</th>
								<th class="no-sort">ACTION</th>
							</tr>
						</thead>
						<tbody class="middle-align">
						@foreach($get_process as $num => $process)
							<tr>
								<td>{{ ++$num }}</td>
								<td>{{ $process->nama }}</td>
								<td>{{ $process->noktp }}</td>
								<td>{{ $process->mitra }}</td>
								<td>{{ $process->tanggal_usulan }}</td>
								<td><a href="/hr/submission/nik_update/{{ $process->id_people }}" type="button" class="btn btn-info btn-block btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> PROCESS</a></td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    	} );
			
			var data_ada = <?= json_encode($get_ada) ?>,
			kolom = [],
			final_data = {};
			$.each(data_ada, function(key, val){
				kolom.push([
					++key,
					val.nama,
					val.id_nik,
					val.mitra,
					val.dateUpdate
				]);
			});

			final_dta= {
				BO: kolom,
			};

			$('#ada_nik').DataTable({
				drawCallback: function () {
					$( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
				},
				fixedHeader: {
					header: true,
					footer: true
				},
				order:	[
					[ 4, "DESC" ]
				],
				data: final_dta.BO,
				deferRender: true,
				scrollCollapse: true,
				scroller: true,
			}).columns.adjust().draw();
			
			$(".next_tbl").DataTable({
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
   			 	}]
			});

			$('#ada_nik_wrapper .table-caption').text('KARYAWAN YANG MEMILIKI NIK');
			$('#ada_nik_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#kdd_nik_wrapper .table-caption').text('KARYAWAN BELUM PUNYA NIK');
			$('#kdd_nik_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#proses_aju_wrapper .table-caption').text('PROSES PENGAJUAN NIK');
			$('#proses_aju_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection