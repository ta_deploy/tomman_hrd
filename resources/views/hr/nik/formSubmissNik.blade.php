@extends('layout')
@section('title')
Pengajuan NIK
@endsection
@section('css')
<style>
	/* */

	.panel-default>.panel-heading {
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-color>.panel-heading a:after {
  content: "";
  position: relative;
  top: 1px;
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-color>.panel-heading a[aria-expanded="false"]:after {
  content: "\f067";
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.panel-color>.panel-heading a[aria-expanded="true"]:after {
  content: "\f068";
  -webkit-transform: rotate(0deg);
  transform: rotate(0deg);
}
</style>
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
  		@if (Request::segment(3)=='nik_update')
			<form method="post" action="hr/submission/nik/{{ Request::Segment(4) }}/update" class="form-horizontal validate" enctype="multipart/form-data">
		@else
			<form method="post" action="hr/submission/nik/{{ Request::Segment(4) }}/create" class="form-horizontal validate" enctype="multipart/form-data">
		@endif
			<div class="col-md-12">	
				<div class="form-group">
					<label class="col-sm-3 control-label" for="tgl_aju">Tanggal Pengajuan :</label>
					<div class="col-md-9">
						<input type="text" name="tgl_aju" class="form-control tgl" id="tgl_aju" value="{{ date('Y-m-d') }}">
					</div>
				</div>
				@if (Request::segment(3)=='nik_update')
				<div class="form-group">
					<label class="col-sm-3 control-label" for="tgl_selesai">Tanggal Selesai :</label>
					<div class="col-md-9">
						<input type="text" name="tgl_selesai" class="form-control tgl" id="tgl_selesai">
					</div>
					<div class="detail_data"></div>
				</div>
				@endif
				<div class="form-group">
					<label class="col-sm-3 control-label" for="nama">Karyawan:</label>
					<div class="col-md-9">
						<select name="nama" class="form-control" id="nama" value="{{ $data->noktp ? : '' }}"></select>
					</div>
				</div>
				<div class="form-group detail_data">
					{{-- GOLONGAN DARAH (YG MITRA AJA HAPUS, TA JANGAN
					NAMA PASANGAN HAPUS (KHUSUS MITRA DIHAPUS, TA JANGAN)
					NAMA ANAK 1 SAMPAI 3 (KHUSUS MITRA DIHAPUS, TA JANGAN)
					HAPUS
					PASANGAN DAN ANAK DIHAPUS DI MIRA ( TA JANGAN)
					MITRA DITAMPILKAN ( TA DIHAPUS) --}}
					<div class="col-md-12">	
						<div class="panel-group panel-group-success panel-group-dark" id="accordion_self_data">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_self_data" href="#self_data" aria-expanded="true">Data Pribadi</a>
								</div>
								<div id="self_data" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-3">
												<label class="control-label" for="nik_prev">NIK</label>
												<input disabled name="nik_prev" type="text" id="nik_prev" class="form-control" value="{{ $data->noktp ? : '' }}"/>
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="nama_prev">Nama</label>
												<input disabled name="nama_prev" type="text" id="nama_prev" class="form-control" value="{{ $data->nama ? : '' }}"/>
											</div>
											<div class="col-sm-2">
												<label class="control-label" for="jk">Jenis Kelamin</label>
												<input disabled name="kota" type="text" id="jk" class="form-control" value="{{ $data->jk ? : '' }}"/>
											</div>
											<div class="col-sm-2 hide_mitra">
												<label class="control-label" for="goldar">Golongan Darah</label>
												<input disabled name="goldar" type="text" id="goldar" class="form-control" value="{{ $data->goldar ? : '' }}"/>
											</div>
											<div class="col-sm-2">
												<label class="control-label" for="tgllahir">Tanggal Lahir</label>
												<input disabled name="tgllahir" type="text" id="tgllahir" class="form-control tgl" value="{{ $data->tgl_lahir ? : '' }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="kota_lahir">Tempat Lahir</label>
												<input disabled name="kota_lahir" type="text" id="kota_lahir" class="form-control" value="{{ $data->tempat_lahir ? : '' }}"/>
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="agama">Agama</label>
												<input disabled name="agama" type="text" id="agama" class="form-control" value="{{ $data->agama ? : '' }}"/>
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="status">Status</label>
												<input disabled name="status" type="text" id="status" class="form-control" value="{{ $data->status ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-8">
												<label class="control-label" for="alamat">Alamat</label>
												<textarea style="resize: none" rows="4" disabled name="alamat" type="text" id="alamat" class="form-control" >{{ $data->alamat ? : '' }}</textarea>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="kota">Kota</label>
												<input disabled name="kota" type="text" id="kota" class="form-control" value="{{ $data->kota ? : '' }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="noktp">Nomor KTP</label>
												<input disabled name="noktp" type="text" id="noktp" class="form-control" value="{{ $data->noktp ? : '' }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="telpon">Telepon</label>
												<input disabled name="telpon" type="text" id="telpon" class="form-control" value="{{ $data->no_telp ? : '' }}"/>
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="email">Email</label>
												<input disabled name="email" type="text" id="email" class="form-control" value="{{ $data->email ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-4">
												<label class="control-label" for="maiden">Nama Ibu Kandung</label>
												<input disabled name="maiden" type="text" id="maiden" class="form-control" value="{{ $data->ibu ? : '' }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="telpon_2">Telepon Yang Bisa Dihubungi</label>
												<input disabled name="telpon_2" type="text" id="telpon_2" class="form-control" value="{{ $data->telpon_2 ? : '' }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="telpon_2_nm">Nama Telepon Yang Bisa Dihubungi</label>
												<input disabled name="telpon_2_nm" type="text" id="telpon_2_nm" class="form-control" value="{{ $data->telpon_2_nm ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-12">
												<label class="control-label" for="npwp">NPWP</label>
												<input disabled name="npwp" type="text" id="npwp" class="form-control" value="{{ $data->npwp ? : '' }}"/>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="panel-group panel-group-warning panel-group-dark" id="accordion_status_kerja">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_status_kerja" href="#status_kerja" aria-expanded="true">
										STATUS KERJA
									</a>
								</div>
								<div id="status_kerja" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-12">
												<label class="control-label" for="waspang_nik">NIK Atasan</label>
												<input disabled name="waspang_nik" type="text" id="waspang_nik" class="form-control" value="{{ $data->waspang_nik ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="tanggal_aju">Tanggal Pengajuan</label>
												<input disabled name="tanggal_aju" type="text" id="tanggal_aju" class="form-control tgl" value="{{ $data->tanggal_aju ? : '' }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="tglmulaikerja">Tanggal Mulai Kerja</label>
												<input disabled name="tglmulaikerja" type="text" id="tglmulaikerja" class="form-control tgl" value="{{ $data->tglmulaikerja ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-12">
												<label class="control-label" for="status_kerja">Status Kerja</label>
												<input disabled name="status_kerja" type="text" id="status_kerja" class="form-control" value="{{ $data->status_kerja ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="psa">PSA</label>
												<input disabled name="psa" type="text" id="psa" class="form-control" value="{{ $data->psa ? : '' }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="witel">Witel</label>
												<input disabled name="witel" type="text" id="witel" class="form-control" value="{{ $data->witel ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="tglawalkerja">Tanggal Awal</label>
												<input disabled name="tglawalkerja" type="text" id="tglawalkerja" class="form-control tgl" value="{{ $data->tglawalkerja ? : '' }}">
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="tglakhirkerja">Tanggal Akhir</label>
												<input disabled name="tglakhirkerja" type="text" id="tglakhirkerja" class="form-control tgl" value="{{ $data->tglakhirkerja ? : '' }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel-group panel-group-info panel-group-dark" id="accordion_pendidikan">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_pendidikan" href="#pendidikan" aria-expanded="true">
										PENDIDIKAN
									</a>
								</div>
								<div id="pendidikan" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="last_study">Pendidikan Terakhir</label>
												<input disabled name="last_study" type="text" id="last_study" class="form-control" value="{{ $data->last_study }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="lvl_pendidikan">Level Pendidikan</label>
												<input disabled name="lvl_pendidikan" type="text" id="lvl_pendidikan" class="form-control" value="{{ $data->lvl_pendidikan }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">	
											<div class="col-sm-6">
												<label class="control-label" for="tgllulus">Tanggal Lulus</label>
												<input disabled name="tgllulus" type="text" id="tgllulus" class="form-control tgl" value="{{ $data->tgllulus }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="jurusan">Jurusan</label>
												<input disabled name="jurusan" type="text" id="jurusan" class="form-control" value="{{ $data->jurusan }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel-group panel-group-info panel-group-dark" id="accordion_keluarga">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_keluarga" href="#keluarga" aria-expanded="true">
										DATA KELUARGA
									</a>
								</div>
								<div id="keluarga" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="nokk">Nomor Kartu Keluarga</label>
												<input disabled name="nokk" type="text" id="nokk" class="form-control" value="{{ $data->nokk ? : '' }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="status_keluarga">Status Dalam Keluarga</label>
												<input disabled name="status_keluarga" type="text" id="status_keluarga" class="form-control" value="{{ $data->status_keluarga ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block hide_mitra">
										<div class="form-group hide_mitra">
											<div class="col-sm-12">
												<label class="control-label" for="namapasangan">Nama Pasangan</label>
												<input disabled name="namapasangan" type="text" id="namapasangan" class="form-control" value="{{ $data->namapasangan ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block hide_mitra">
										<div class="form-group hide_mitra">
											<div class="col-sm-12">
												<label class="control-label" for="anak1">Nama Anak I</label>
												<input disabled name="anak1" type="text" id="anak1" class="form-control" value="{{ $data->anak1 ? : '' }}"/>
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="anak2">Nama Anak II</label>
												<input disabled name="anak2" type="text" id="anak2" class="form-control" value="{{ $data->anak2 ? : '' }}"/>
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="anak3">Nama Anak III</label>
												<input disabled name="anak3" type="text" id="anak3" class="form-control" value="{{ $data->anak3 ? : '' }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="panel-group panel-group-warning panel-group-dark" id="accordion_rekening">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_rekening" href="#rekening" aria-expanded="true">
										REKENING
									</a>
								</div>
								<div id="rekening" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-4">
												<label class="control-label" for="norek">Nomor Rekening</label>
												<input disabled name="norek" type="text" id="norek" class="form-control" value="{{ $data->norek ? : '' }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="bank">Bank</label>
												<input disabled name="bank" type="text" id="bank" class="form-control" value="{{ $data->bank ? : '' }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="kantor_cabang">Kantor Cabang</label>
												<input disabled name="kantor_cabang" type="text" id="kantor_cabang" class="form-control" value="{{ $data->kantor_cabang ? : '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-12">
												<label class="control-label" for="namarek">Nama Di Rekening</label>
												<input disabled name="namarek" type="text" id="namarek" class="form-control" value="{{ $data->namarek ? : '' }}"/>
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="link_aj">Nomor Link Aja</label>
												<input disabled name="link_aj" type="text" id="link_aj" class="form-control" value="{{ $data->link_aj ? : '' }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="panel-group panel-group-success panel-group-dark" id="accordion_jamsos">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_jamsos" href="#jamsos" aria-expanded="true">
										JAMSOSTEK & BPJS
									</a>
								</div>
								<div id="jamsos" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-3">
												<label class="control-label" for="jamsostek">Nomor BPJS Ketenaga Kerjaan / Jamsostek</label>
												<input disabled name="jamsostek" type="text" id="jamsostek" class="form-control" />
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="tgl-jamsostek">Tanggal BPJS Ketenaga Kerjaan / Jamsostek</label>
												<input disabled name="tgl_jamsostek" type="text" id="tgl-jamsostek" class="form-control tgl" />
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="bpjs">Nomor BPJS</label>
												<input disabled name="bpjs" type="text" id="bpjs" class="form-control" />
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="tgl-bpjs">Tanggal BPJS</label>
												<input disabled name="tgl_bpjs" type="text" id="tgl-bpjs" class="form-control tgl" />
											</div>
										</div>
										<hr class="page-wide-block hide_mitra">
										<div class="form-group hide_mitra">
											<div class="col-sm-12">
												<label class="control-label" for="no-bpjs-istri">Nomor BPJS Istri</label>
												<input disabled name="bpjs_istri" type="text" id="no-bpjs-istri" class="form-control" />
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="tgl-bpjs-istri">Tanggal BPJS Istri</label>
												<input disabled name="tgl_bpjs_istri" type="text" id="tgl-bpjs-istri" class="form-control tgl" />
											</div>
										</div>
										<hr class="page-wide-block hide_mitra">
										<div class="form-group hide_mitra">
											<div class="col-sm-6">
												<label class="control-label" for="no-bpjs-anak1">Nomor BPJS Anak I</label>
												<input disabled name="bpjs_anak1" type="text" id="no-bpjs-anak1" class="form-control" />
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="tgl-bpjs-anak1">Tanggal BPJS Anak I</label>
												<input disabled name="tgl_bpjs_anak1" type="text" id="tgl-bpjs-anak1" class="form-control tgl" />
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="no-bpjs-anak2">Nomor BPJS Anak II</label>
												<input disabled name="bpjs_anak2" type="text" id="no-bpjs-anak2" class="form-control" />
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="tgl-bpjs-anak2">Tanggal BPJS Anak II</label>
												<input disabled name="tgl_bpjs_anak2" type="text" id="tgl-bpjs-anak2" class="form-control tgl" />
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="no-bpjs-anak3">Nomor BPJS Anak III</label>
												<input disabled name="bpjs_anak3" type="text" id="no-bpjs-anak3" class="form-control " />
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="tgl-bpjs-anak3">Tanggal BPJS Anak III</label>
												<input disabled name="tgl_bpjs_anak3" type="text" id="tgl-bpjs-anak3" class="form-control tgl" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel-group panel-group-danger panel-group-dark" id="accordion_etc">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_etc" href="#etc" aria-expanded="true">
										LAIN - LAIN
									</a>
								</div>
								<div id="etc" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="gadas">Gadas</label>
												<input disabled name="gadas" type="text" id="gadas" class="form-control" value="{{ $data->gadas or ''}}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="pelatihan">Pelatihan Ala</label>
												<input disabled name="pelatihan" type="text" id="pelatihan" class="form-control" value="{{ $data->pelatihan_ala or ''}}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel-group panel-group-danger panel-group-dark" id="accordion_apd">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_apd" href="#apd" aria-expanded="true">
										APD
									</a>
								</div>
								<div id="apd" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="seragam">Ukuran Baju</label>
												<input disabled name="seragam" type="text" id="seragam" class="form-control" value="{{ $data->penerimaan_seragam or ''}}"/>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label class="control-label" for="alker">Alker</label>
												<select class="form-control" id="alker" disabled name="alker" multiple></select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 hide_ta">
						<div class="panel-group panel-group-default panel-group-dark" id="accordion_mitra">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_mitra" href="#mitra" aria-expanded="true">
										DATA MITRA
									</a>
								</div>
								<div id="mitra" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="mitra_name">Nama Mitra</label>
												<select class="form-control" id="mitra_name" disabled name="mitra_name"></select>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="posisi">Posisi</label>
												<input disabled name="posisi" type="text" id="posisi" class="form-control" value="{{ $data->penerimaan_seragam or ''}}"/>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label class="control-label" for="alker">Alker (OTOMATIS TEISI SESUAI PUNYA MITRA)</label>
												<select class="form-control" id="alker" disabled name="alker" multiple></select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			$('.tgl').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',
        daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',

        beforeShowMonth: function(date) {
          if (date.getMonth() === 8) {
            return false;
          }
        },

        beforeShowYear: function(date){
          if (date.getFullYear() === 2014) {
            return false;
          }
        }
      });

			$('#nama').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / NIK Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: { 
					url: "/jx_dng/get_user",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Nama Tidak Terdaftar!";
						}
					},
					cache: true,
					success: function(value) {
					}
				}
			});

			$('#witel').select2({
				width: '100%',
				placeholder: "Ketik Nama Witel",
				allowClear: true,
				ajax: {
					url: '/jx_dng/get_witel',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('#mitra_name').select2({
				width: '100%',
				placeholder: "Ketik Nama Mitra",
				allowClear: true,
				ajax: {
					url: '/jx_dng/get_mitra',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

		});
	</script>
@endsection