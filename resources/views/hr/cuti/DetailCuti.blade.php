@extends('layout')
@section('title')
Detail
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<div class="alert alert-warning"><span id="sisa_cut"></span></div>
		<div class="col-md-8">
			<div class="panel panel-info">
				<div class="panel-heading">
					<span class="panel-title">
						Detail Cuti
					</span>
				</div>
				<div class="panel-body">
					<div class="form-horizontal validate">
						<div class="form-group">
							<label class="control-label col-md-3">Nama</label>
							<div class="col-sm-9">
								<input class="form-control" value="{{ $cuti->nama }}" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">NIK</label>
							<div class="col-sm-9">
								<input class="form-control" value="{{ $cuti->id_nik }}" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">Tanggal Awal Cuti</label>
							<div class="col-sm-9">
								<input class="form-control" value="{{ $cuti->tgl_cuti_awal }}" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">Tanggal Berakhir Cuti</label>
							<div class="col-sm-9">
								<input class="form-control" value="{{ $cuti->tgl_cuti_akhir }}" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">Jenis Cuti</label>
							<div class="col-sm-9">
								<input class="form-control" value="{{ $cuti->cuti_nm }}" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">Keterangan Cuti</label>
							<div class="col-sm-9">
								<textarea style="resize: none" rows="4" class="form-control"disabled>{{ $cuti->alasan_cuti }}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<span class="panel-title">
						Foto
					</span>
				</div>
				<div class="panel-body">
					<div class="widget-products-item col-md-12">
						<a href="#" class="widget-products-image">
							{{-- <img style="width: 265px; height: 286px;" src="https://apps.telkomakses.co.id/wimata/photo/crop_{{ $cuti->id_nik }}.jpg"> --}}
							<span class="widget-products-overlay"></span>
						</a>
						<a href="#" class="widget-products-title">
							{{ $cuti->nama }} / {{ $cuti->id_nik }}
							<span class="widget-products-price pull-xs-right label label-tag label-info">Detail</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js" integrity="sha512-taB73dM5L3JgciedVi4zU6fYBnX1tbhnWUtc/hdBTSgKIWgQnlNvkJYT4uJiin5GiJ2oXiIxrxqjD0Fy5orCxA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script>
		$( function(){
			var data = <?= json_encode ($cuti) ?>;

			console.log(data);

			$.ajax({
				type: 'GET',
				url: '/jx_dng/get_sisa_cuti',
				data: {
					data: data.people_id
				}
			}).done( function(e){
				console.log(e)
				var sisa = parseInt(e);

				if(sisa > 0){
					$('#sisa_cut').html('Sisa Cuti Tahun '+ new Date().getFullYear() +' Adalah '+ sisa +' Hari');
				}else{
					$('#sisa_cut').html('Tidak Memiliki Sisa Cuti Lagi Untuk Tahun '+ new Date().getFullYear() );
				}
			});
		});
	</script>
@endsection