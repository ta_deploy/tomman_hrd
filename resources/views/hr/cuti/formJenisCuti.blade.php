@extends('layout')
@section('title')
List Jenis Cuti
@endsection
@section('css')
<style>
  th {
    text-align: center;
	text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="col-md-12">
	<div id="add_witel_new" class="modal fade modal-large add_witel_new">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title">Ubah Jenis Cuti</div>
				</div>
				<div class="modal-body">
					<div class="col-md-12">	
						<div class="panel panel-color panel-info panel-border">
							<div class="panel-heading">
								<div class="panel-title title_kar">Jenis Cuti</div>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="edit_cuti">Nama Cuti Yang Baru :</label>
									<div class="col-md-9">
										<input type="text" name="edit_cuti" class="form-control" id="edit_cuti">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal"><span class="fa fa-cloud-download"></span> Simpan</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="px-content">
	@include('Partial.alerts')
	<div class="page-header">
		<div class="panel panel-info" style="margin-bottom: 11px;">
			<div class="panel-heading">
				<div class="panel-title">Input Jenis Cuti</div>
			</div>
			<div class="panel-body">
				<form class="form-inline" method="POST">
					<div class="form-group">
						<label for="cuti_nm">Nama Cuti</label>
						<input type="text" class="form-control" name="cuti_nm" id="cuti_nm" placeholder="Nama Cuti">
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
		<a href="/hr/input/cuti" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Karyawan Cuti</a>
		<div class="table-info">
			<table class="table table-bordered table-striped table-small-font" id="jenis_cuti">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th>Nama Jenis Cuti</th>
					</tr>
				</thead>
				<tbody class="middle-align">
					@foreach($data as $key => $d)
						<tr>
							<td>{{ ++$key }}</td>
							<td>{{ $d->cuti_nm }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){

			$(".table").DataTable({
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});
	
			$('#jenis_cuti_wrapper .table-caption').text('List Jenis Cuti');
			$('#jenis_cuti_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection