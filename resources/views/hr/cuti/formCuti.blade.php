@extends('layout')
@section('title')
Form Cuti
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="col-md-12">
			<div class="panel panel-color panel-info panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Pembuatan Cuti</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-1 control-label" for="nik">Nik :</label>
						<div class="col-md-11">
							<select name="nik" class="form-control" id="nik"></select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-1 control-label" for="position_nm">Posisi :</label>
						<div class="col-md-11">
							<input type="text" name="position_nm" class="form-control" disabled id="position_nm">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-1 control-label" for="unit">Unit :</label>
						<div class="col-md-11">
							<input type="text" name="unit" class="form-control" disabled id="unit">
						</div>
					</div>
					<hr class="page-wide-block">
					<div class="form-group">
						<div class="col-md-3">
							<label class=" control-label" for="tgl_cuti">Tanggal Cuti :</label>
							<input type="text" name="tgl_cuti" class="form-control" id="tgl_cuti">
						</div>
						<div class="col-md-1">
							<label class="control-label" for="cuti_left">Sisa Cuti :</label>
							<input type="text" name="cuti_left" class="form-control" id="cuti_left" readonly>
						</div>
						<div class="col-md-3">
							<label class="control-label" for="jenis_cuti">Jenis Cuti :</label>
							<select name="jenis_cuti" class="form-control" id="jenis_cuti"></select>
						</div>
						<div class="col-md-5">
							<label class=" control-label" for="alasan_cuti">Alasan Cuti :</label>
							<textarea name="alasan_cuti" style="resize: none" rows="4" class="form-control" id="alasan_cuti"></textarea>
						</div>
					</div>
					<div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
            </div>
          </div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			$('#tgl_cuti').daterangepicker({
				locale: {
					format: 'YYYY/MM/DD'
				},
				minDate: new Date(new Date().setDate(new Date().getDate() ) ),
				maxDate: new Date(new Date().setDate(new Date().getDate() + 12) )
			});

			$('#nik').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: {
					url: "/jx_dng/get_user",
					dataType: 'json',
					delay: 250,
					data: function (params) {
							return {
									searchTerm: params.term
							};
					},
					processResults: function (response) {
							return {
									results: response
							};
					},
					language: {
						noResults: function(){
							return "Nik Tidak Ditemukan? <a type='button' class='btn btn-sm btn-info'><span class='ion-plus-circled'></span> Tambah User Baru</a>";
						}
					},
					cache: true,
					success: function(value) {
					}
				}
			});

			$('#nik').on('select2:select', function(){
				$.ajax({
					type: 'GET',
					url: '/jx_dng/get_user_mutasi',
					data: {
						data: $(this).val()
					},
					dataType: 'JSON'
				}).done( function(e){
					console.log(e)
					$('#position_nm').val(e.c_pos_name);
					$('#unit').val(e.c_unit);
				});

				$.ajax({
					type: 'GET',
					url: '/jx_dng/get_sisa_cuti',
					data: {
						data: $(this).val()
					}
				}).done( function(e){
					$("#tgl_cuti").val('');

					var sisa = parseInt(e);

					if(sisa > 0){

						$('#tgl_cuti').daterangepicker({
							locale: {
								format: 'YYYY/MM/DD'
							},
							minDate: new Date(new Date().setDate(new Date().getDate() ) ),
							maxDate: new Date(new Date().setDate(new Date().getDate() + sisa) )
						});

						$('#cuti_left').val(sisa);
					}else{
						$('#cuti_left').val(0);
						$('#tgl_cuti').daterangepicker().empty();
					}

				});
			});

			$('#jenis_cuti').select2({
				width: '100%',
				placeholder: "Ketik Jenis Cuti",
				allowClear: true,
				minimumInputLength: 3,
				ajax: {
					url: '/jx_dng/jenis_cuti',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				language: {
					noResults: function(){
						return "Jenis Cuti Tidak Ditemukan? <a type='button' href='/report/cuti' target='_blank' class='btn btn-sm btn-warning'><span class='ion-plus-circled'></span> Buka Halaman Direktorat</a>";
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

		});
	</script>
@endsection