<table class="table table-bordered table-striped table-small-font" id="example-2">
	<thead>
		<tr>
			<th class="text-center">Nomor</th>
			<th class="text-center">Tanggal</th>
			<th class="text-center">Jenis Cuti</th>
		</tr>
	</thead>
	<tbody class="middle-align">
	<?php $no=1; ?>
	@foreach($list as $r)				
		<tr>
			<td class="text-center">{{$no}}</td>
			<td class="text-center">{{$r->tgl}}</td>
			<td class="text-center">{{$r->jenis_cuti}}</td>
		</tr>
		<?php $no++;?>
	@endforeach
	</tbody>
</table>