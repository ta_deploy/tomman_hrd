@extends('layout')
@section('title')
Kalender Cuti
@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.css">
@endsection
@section('content')
<div id="calendarModal" class="modal fade">
	<div class="modal-dialog">
			<div class="modal-content">
					<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
							<h4 id="modalTitle" class="modal-title"></h4>
					</div>
					<div id="modalBody" class="modal-body"> </div>
					<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
			</div>
	</div>
</div>
<div class="px-content">
	<div class="col-md-12">
		<div id="calendar"></div>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js"></script>
	<script>
		$( function() {

			var data = <?= json_encode($data) ?>,
			event_dt = [];
			console.log(data)
			$.each(data, function(key, val){
				event_dt.push({
					title: val.nama,
					start: val.tgl_cuti_awal,
					end: val.tgl_cuti_akhir,
					color: val.jk == 'Laki-Laki' ? 'blue' : '#ff6384',
					id: data.id,
					url: '/report/detail_cuti/'+val.id
				});
			});

			console.log(event_dt)

			var calendar = new FullCalendar.Calendar(document.getElementById('calendar'), {
				initialDate: new Date(), //new Date() / 2020-01-02
				editable: true,
				datesSet:  (data => {
					console.log('datesSet: ' , data);
					var todayDate = data.start.toISOString().slice(0, 10);
					console.log(todayDate);
				}),
				eventClick:  function(info) {
				// 	console.log(info.event.title, info.event, info.event.extendedProps.id)
				// 	$('#modalTitle').html('Detail Cuti '+info.event.title);
				// 	// $('#modalBody').html(event.description);
				// 	// $('#eventUrl').attr('href',event.url);
				// 	// $('#calendarModal').modal();
				// console.log(info.event, info)
					if (info.event.url) {
						info.jsEvent.preventDefault();
						window.open(info.event.url, "_blank");
					}
        },
				dayMaxEventRows: true,
				events: event_dt
			});
			calendar.render();

			// setTimeout(function(){
			// 	calendar.gotoDate('2021-06');
			// 	calendar.removeAllEvents();
			// 	calendar.addEvent(
			// 		{
			// 			title: 'as Hari Ini',
			// 			start: '2021-06-04',
			// 			color:	'blue',
			// 			url: '/'
			// 		}
			// 	);
			// 	calendar.addEvent(
			// 		{
			// 			title: 'as Hari Ini',
			// 			start: '2021-06-05',
			// 			color:	'blue',
			// 			url: '/'
			// 		}
			// 	);
			// }, 3000);
		});
  </script>
@endsection