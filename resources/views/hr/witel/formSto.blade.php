@extends('layout')
@section('title')
List STO
@endsection
@section('css')
<style>
  th {
    text-align: center;
	text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
  <div class="page-header">
    <div class="panel panel-info">
      <div class="panel-heading">
        <div class="panel-title">Input Data STO</div>
      </div>
      <div class="panel-body">
        <form class="form-inline row" method="post">
			<div class="form-group" style="width: 100%;">
				<label for="witel_nm">Nama Witel</label>
				<select class="form-control" name="witel_nm" id="witel_nm" style="width: 250px;">
				<option value="" selected disabled>Pilih Witel</option>
				@foreach ($get_witel as $witel)
				<option value="{{ $witel->id_witel }}">{{ $witel->nama_witel }}</option>
				@endforeach
				</select>
			</div>
			<hr class="page-wide-block">
			<div class="form-group">
				<label for="sto">STO</label>&nbsp;
				<input type="text" class="form-control" name="sto" id="sto" placeholder="Nama STO">
			</div>&nbsp;
			<div class="form-group">
				<label for="kode_area">Kode STO</label>&nbsp;
				<input type="text" class="form-control" name="kode_area" id="kode_area" placeholder="Kode STO">
			</div>&nbsp;
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
		<div class="table-info">
			<table class="table table-bordered table-striped table-small-font" id="kode_sto">
				<thead>
					<tr>
						<th>NO</th>
						<th>NAMA WITEL</th>
						<th>STO</th>
						<th>KODE STO</th>
					</tr>
				</thead>
				<tbody class="middle-align">
				@foreach($get_sto as $num => $sto)
  					<tr>
					  <td>{{ ++$num }}</td>
					  <td>{{ $sto->nama_witel }}</td>
					  <td>{{ $sto->area_alamat }}</td>
					  <td>{{ $sto->kode_area }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('#witel_nm').select2({
				placeholder: "Ketik Nama Witel",
				allowClear: true,
				language: {
					noResults: function(){
						return "Nama Witel Tidak Ada? <button type='button' class='btn btn-sm btn-info' data-toggle='modal' data-target='#add_witel_new'><span class='ion-plus-circled'></span> Tambah Witel Baru</button>";
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$(".table").DataTable({
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});

			$('#kode_sto_wrapper .table-caption').text('LIST STO');
			$('#kode_sto_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection