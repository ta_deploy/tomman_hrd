@extends('layout')
@section('title')
List Witel
@endsection
@section('css')
<style>
  th {
    text-align: center;
	text-transform: uppercase;
  }
  td {
    text-align: center;
    text-transform: uppercase;
  }
</style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
  <div class="page-header">
		<div class="table-info">
			<table class="table table-bordered table-striped table-small-font" id="table_witel">
				<thead>
					<tr>
						<th>ID WITEL</th>
						<th>NAMA WITEL</th>
						<th>PSA</th>
					</tr>
				</thead>
				<tbody class="middle-align">
				@foreach($get_witel as $num => $witel)
  					<tr>
  						<td>{{ $witel->id_witel }}</td>
						<td>{{ $witel->nama_witel }}</td>
						<td>{{ $witel->psa_witel }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function(){
			$(".table").DataTable({
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});
	
			$('#table_witel_wrapper .table-caption').text('LIST WITEL');
			$('#table_witel_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		})
  </script>
@endsection