@include('partial.confirm')
@extends('layout')

@section('html-title')
	 HR :: TOMMAN
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Project :
@endsection

@section('page-title')
	<style>
	.right{
		text-align: right;
	}
	</style>
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2.css">
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="/lib/xenon/js/datatables/dataTables.bootstrap.css">
	<script src="/lib/xenon/js/jquery-1.11.1.min.js"></script>

	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
		<li><i class="linecons-calendar"></i>HR</li>
	</ol>
@endsection

@section('body')
	<div class="panel panel-default panel-noheading">
		<div class="panel-body">
			<form method="post" class="form-horizontal validate" class="form-horizontal">
			<input name="id" type="hidden"  value="{{ $data->id or ''}}" />
			<div class="input-group">
				<div class="form-group col-sm-11">
					<div class="col-sm-6">
						<label class="control-label" for="input-witel">Witel</label>
						<select name="witel" id="input-witel" class="form-control">
							@foreach($witel as $w)
								<option value="{{ $w->id }}" <?php if(!empty($data->witel_id) ){if($w->id==$data->witel_id){echo "selected";}} ?> >{{ $w->nama_witel }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
						<div class="col-sm-12">
						<label class="control-label" for="ppn">Nama PSA</label>
						<input name="loker" type="text" id="loker" class="form-control" value="{{ $data->nama_loker or ''}}" data-validate="required" data-message-required="Loker Harus Diisi"/>
						</div></div>
					</div>
				</div>

		      <span class="input-group-btn">
		      	<button class="btn btn-blue btn-icon btn-icon-standalone btn-icon-standalone-right" type="submit">
		      		<i class="fa-save"></i>
					<span>Simpan</span>
				</button>
		      </span>
		    </div>
			</form>
		</div>
	</div>

	<div class="panel panel-default panel-noheading">
		<table class="table table-bordered table-striped table-small-font" id="example-2">
			<thead>
				<tr>
					<th>Nomor</th>
					<th>Witel</th>
					<th>Nama PSA</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody class="middle-align">
				<?php $no=1; ?>
				@foreach($list as $r)
				<tr>
					<td>{{$no}}</td>
					<td>{{$r->witel }}</td>
					<td>{{$r->nama_loker }}</td>
					<td>
						<a href="/hr/formLoker/{{$r->id}}" class="btn btn-secondary btn-sm btn-icon icon-left fa-edit tooltip-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit ">
						</a>
						<form method="POST" action="/hr/deleteLoker/{{$r->id}}" accept-charset="UTF-8" style="display:inline"
							class="tooltip-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Loker">
		                  <button class="btn btn-danger btn-sm btn-icon icon-left fa-trash tooltip-danger"
		                  type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Hapus PSA {{ $r->nama_loker }} ?" data-message="Are you sure you want to delete this ?">

		                  </button>
		                </form>
					</td>
				</tr>
				<?php $no++;?>
				@endforeach
			</tbody>
		</table>
	</div>

@endsection

@section('plugins')
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/lib/xenon/js/datatables/dataTables.bootstrap.js"></script>
	<script src="/lib/xenon/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="/lib/xenon/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
	<script src="/lib/xenon/js/select2/select2.min.js"></script>
	<script src="/lib/xenon/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="/lib/xenon/js/jquery-validate/jquery.validate.min.js"></script>

@endsection

@section('footer')
	<script>
	$("#input-witel").select2();
    $("#example-2").dataTable({
							dom: "t" + "<'row'<'col-xs-6'i><'col-xs-6'p>>",
							aoColumns: [
								null,
								null,
								null,
								null
							],
						});

	$('#confirmDelete').on('show.bs.modal', function (e) {
      $message = $(e.relatedTarget).attr('data-message');
      $(this).find('.modal-body p').text($message);
      $title = $(e.relatedTarget).attr('data-title');
      $(this).find('.modal-title').text($title);
      // Pass form reference to modal for submission on yes/ok
      var form = $(e.relatedTarget).closest('form');
      $(this).find('.modal-footer #confirm').data('form', form);
    });
    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
        $(this).data('form').submit();
    });

  </script>
@endsection