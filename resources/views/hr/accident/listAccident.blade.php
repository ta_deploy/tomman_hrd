@extends('layout')
@section('title')
List Kecelakaan Kerja
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	@if($jenis == 'all')
		<div class="col-md-12">
			<canvas id="chart_bar" style="height: 200px"></canvas>
		</div>
	@endif
	<div class="col-md-12">
		<canvas id="chart_line" style="height: 200px"></canvas>
	</div>
	<div class="col-md-12" style="padding-top: 11px;">
		<a href="/report/create_accident/input" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Kecelakaan Kerja</a>
	</div>
	<div class="col-md-12">
		<div class="panel panel-default ">
			<div class="page-header">
				<div class="table-info">
					<table class="table table-bordered table-striped table-small-font" id="table_acc">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>NIK</th>
								<th>Nama</th>
								<th>Kronologi Kejadian</th>
								<th>Detil Cedera</th>
								<th>No BPJS</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@foreach ($data as $key => $d)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $d->id_nik }}</td>
									<td>{{ $d->nama }}</td>
									<td>{{ $d->kronolog }}</td>
									<td>{{ $d->detail_acc }}</td>
									<td>{{ $d->bpjs }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
	<script>
		$( function(){

			$(".table").DataTable({
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});

			$('#table_acc_wrapper .table-caption').text('List Kecelakaan');
			$('#table_acc_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			var randomScalingFactor = function() {
				return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
			},
			dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ", 0.8)";
      },
			chartColors = {
				red: 'rgb(255, 99, 132, 0.8)',
				orange: 'rgb(255, 159, 64, 0.8)',
				yellow: 'rgb(255, 205, 86, 0.8)',
				green: 'rgb(75, 192, 192, 0.8)',
				blue: 'rgb(54, 162, 235, 0.8)',
				purple: 'rgb(153, 102, 255, 0.8)',
				grey: 'rgb(231,233,237, 0.8)'
			},
			MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

			var data = {!! json_encode($data_tahunan ?? '') !!},
			main_dt = [],
			raw_month= [];
			console.log(data)

			$.each(data, function(key, val){
				var isi_data = $.map(val, function(value, index) {
					return [value];
				});

				$.each(val, function(key_c1, val_c1){
					raw_month.push(key_c1);
				});

				main_dt.push({
					label: key,
					fill: false,
					backgroundColor: dynamicColors(),
					borderColor: dynamicColors(),
					data: isi_data,
					datalabels: {
						align: 'center',
						anchor: 'center'
					}
				});
			});

			var month_hs = [...new Set([...raw_month])],
      bulanku = [];

      month_hs.sort(function(a, b){
        return parseInt(a)- parseInt(b);
      });

      $.each(month_hs, function (key, valu){
        bulanku.push(MONTHS[valu]);
      });

			// console.log(main_dt, bulanku, )
			Chart.register(ChartDataLabels);

			if($('#chart_line').length){
				var ctx_line = $('#chart_line'),
				config = {
					type: 'bar',
					data: {
						labels: bulanku,
						datasets: main_dt
					},
					options: {
						responsive: true,
						maintainAspectRatio: false,
						plugins: {
							title: {
								display: true,
								text: 'Data Kecelakaan Tahun '
							},
							datalabels: {
								color: 'black',
								display: function(context) {
									return context.dataset.data[context.dataIndex];
								},
								font: {
									weight: 'bold'
								},
								formatter: Math.round
							}
						},
						tooltips: {
							mode: 'label',
						},
						hover: {
							mode: 'nearest',
							intersect: true
						},
						scales: {
							x: {
								stacked: true,
							},
							y: {
								stacked: true
							}
						}
					}
				};
				new Chart(ctx_line, config);
			}

			var data = {!! json_encode($data_hr ?? '') !!},
			labels = [],
			isi = [],
			color=[];

			$.each(data, function(key, val){
				labels.push(val.mitra);
				isi.push(val.jml_acc);
				color.push(dynamicColors() );
			});

			if($('#chart_bar').length){
				var ctx_bar = $('#chart_bar');
				config = {
					type: 'bar',
					data: {
						labels: labels,
						datasets: [{
							label: "Total Kecelakaan",
							backgroundColor: color,
							borderColor: color,
							data: isi,
							fill: false,
							borderWidth: 1,
							datalabels: {
								align: 'center',
								anchor: 'center'
							}
						}],
					},
					options: {
						responsive: true,
						datalabels: {
							color: 'black',
							display: function(context) {
								return context.dataset.data[context.dataIndex];
							},
							font: {
								weight: 'bold'
							},
							formatter: Math.round
						},
						maintainAspectRatio: false,
						scales: {
							y: {
								beginAtZero: true
							}
						},
						plugins: {
							legend: {
								display: false,
							},
							title: {
								display: true,
								text: 'Data Kecelakaan 10 Besar'
							}
						}
					}
				};
				new Chart(ctx_bar, config);
			}
		});
  </script>
@endsection