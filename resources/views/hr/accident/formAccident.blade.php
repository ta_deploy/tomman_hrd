@extends('layout')
@section('title')
Form Kecelakaan Kerja
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate" enctype="multipart/form-data">
			<div class="col-md-12">
			<div class="panel panel-color panel-danger panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Kecelakaan Kerja</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="tgl">Tanggal Kecelakaan :</label>
						<div class="col-md-9">
							<input type="text" name="tgl" class="form-control" id="tgl">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="nama">Nik Karyawan :</label>
						<div class="col-md-9">
							<select name="nama" class="form-control" id="nama"></select>
						</div>
						<div class="detail_data"></div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="alamat_acc">Alamat Kejadian :</label>
						<div class="col-md-9">
							<textarea style="resize: none" name="alamat_acc" class="form-control" id="alamat_acc"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="detail_acc">Detail Cedera :</label>
						<div class="col-md-9">
							<input type="text" name="detail_acc" class="form-control" id="detail_acc">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="action">Tindakan :</label>
						<div class="col-md-9">
							<input type="text" name="action" class="form-control" id="action">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="kronolog">Kronologi Kejadian :</label>
						<div class="col-md-9">
							<textarea style="resize: none" rows="4" name="kronolog" class="form-control" id="kronolog"></textarea>
						</div>
					</div>
					<div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
            </div>
          </div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			$('#tgl').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',
        daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',

        beforeShowMonth: function(date) {
          if (date.getMonth() === 8) {
            return false;
          }
        },

        beforeShowYear: function(date){
          if (date.getFullYear() === 2014) {
            return false;
          }
        }
      });

			$('#nama').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: {
					url: "/jx_dng/get_user",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Nik Tidak Terdaftar!";
						}
					},
					cache: true,
					success: function(value) {
						$('.title_kar').html('Kecelakaan Karyawan', value);
					}
				}
			});
		});
	</script>
@endsection