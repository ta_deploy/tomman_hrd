@extends('layout')
@section('title')
My Profile
@endsection
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
<style>
	/* */

	.panel-default>.panel-heading {
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-color>.panel-heading a:after {
  content: "";
  position: relative;
  top: 1px;
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
	float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-color>.panel-heading a[aria-expanded="false"]:after {
  content: "\f067";
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.panel-color>.panel-heading a[aria-expanded="true"]:after {
  content: "\f068";
  -webkit-transform: rotate(0deg);
  transform: rotate(0deg);
}
</style>
@endsection
@section('content')
<div id="detail_job" class="modal fade modal-large detail_job">
	<div class="modal-dialog" style="width:1250px;">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">Detail Mutasi</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<div class="table-info">
						<table class="table table-bordered table-striped table-small-font" id="detail_mut" style="width: 100%;">
							<thead>
								<tr>
									<th>Object ID</th>
									<th>Position Name </th>
									<th>PSA</th>
									<th>Witel</th>
									<th>Regional</th>
									<th>Level</th>
									<th>Teritory</th>
									<th>Bizzpart ID</th>
									<th>Direktorat</th>
									<th>Unit</th>
									<th>Sub Unit</th>
									<th>Group</th>
									<th>Sub Group</th>
									<th>Group Fungsi</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">close</button>
			</div>
		</div>
	</div>
</div>
<div class="px-content">
@include('Partial.alerts')
	<ul class="nav nav-tabs nav-tabs-simple">
		<li class="active">
			<a href="#tabs-data_naker" data-toggle="tab">
				Data Naker
			</a>
		</li>
		<li>
			<a href="#tabs-fa" data-toggle="tab">
				History FA
			</a>
		</li>
		@if(in_array(session('auth')->perwira_level, [1, 69]) )
			<li>
				<a href="#tabs-posisi" data-toggle="tab">
					History Position
				</a>
			</li>
			<li>
				<a href="#tabs-cuti" data-toggle="tab">
					History Cuti
				</a>
			</li>
		@endif
		<li>
			<a href="#tabs-reward" data-toggle="tab">
				History Reward
			</a>
		</li>
		@if(in_array(session('auth')->perwira_level, [1, 69]) )
			<li>
				<a href="#tabs-doc" data-toggle="tab">
					History Dokumen
				</a>
			</li>
			<li>
				<a href="#tabs-kons" data-toggle="tab">
					History Konseling & SP
				</a>
			</li>
		@endif
		<li>
			<a href="#tabs-accident" data-toggle="tab">
				History Kecelakaan Kerja
			</a>
		</li>
	</ul>
	<div class="tab-content tab-content-bordered">
		<div class="tab-pane fade in active" id="tabs-data_naker">
			<div class="page-header">
			<form id="submit_me" method="POST" enctype="multipart/form-data" autocomplete="off" class="form-horizontal validate">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="nik_id" value="{{ $data->nik }}">
				<div class="form-group detail_data">
					{{-- GOLONGAN DARAH (YG MITRA AJA HAPUS, TA JANGAN
					NAMA PASANGAN HAPUS (KHUSUS MITRA DIHAPUS, TA JANGAN)
					NAMA ANAK 1 SAMPAI 3 (KHUSUS MITRA DIHAPUS, TA JANGAN)
					HAPUS
					PASANGAN DAN ANAK DIHAPUS DI MIRA ( TA JANGAN)
					MITRA DITAMPILKAN (TA DIHAPUS) --}}
					<div class="col-md-12">
						<div class="panel-group panel-group-success panel-group-dark" id="accordion_self_data">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_self_data" href="#self_data" aria-expanded="true">DATA PRIBADI</a>
								</div>
								<div id="self_data" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										{{-- @php
											$path = "/upload4/perwira/profil_user/{{ $data->nik }}";
											$th   = '';
											$img  = '';
											$src  = "/image/placeholder.gif";
											$flag = '';

											if(file_exists(public_path().$path) )
											{
												$th   = "$path-th.jpg";
												$img  = "$path.jpg";
												$src  = $th;
												$flag = 1;
											}
										@endphp

										<a href="{{ $img }}">
											<img src="{{ $src }}" alt="profile" id="img-profile" class="photo_valid_dis" style="margin-bottom: 7px;"/>
										</a>
										<br />
										<input type="text" class="hidden" name="flag_profile" value="{{ $flag }}"/>
										<input type="file" class="hidden photo_profile" name="photo-profile" accept="image/jpeg" />
										<button type="button" class="btn btn-sm btn_up btn-info">
											<i class="ion ion-camera"></i>
										</button>
										<p style="font-size: 9px;">Photo Profile</p>
										{!! $errors->first('profile', '<span class="label label-danger">:message</span>') !!} --}}
										<div class="form-group">
											<div class="col-sm-3">
												<label class="control-label title_nik" for="nik">NIK</label>
												<input name="nik" type="text" id="nik" class="form-control" value="{{ $data->nik ? : '' }}" required/>
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="nama">Nama</label>
												<input name="nama" type="text" id="nama" class="form-control" value="{{ $data->nama ? : '' }}" required/>
											</div>
											<div class="col-sm-2">
												<label class="control-label" for="jk">Jenis Kelamin</label>
												<select class="form-control" id="jk" name="jk" required>
												@if($data->jk <> "")
													<option value="{{ $data->jk }}">{{ $data->jk }}</option>
												@else
													<option value="Laki-Laki">Laki-Laki</option>
													<option value="Perempuan">Perempuan</option>
												@endif
												</select>
											</div>
											<div class="col-sm-2">
												<label class="control-label" for="goldar">Golongan Darah</label>
												<select class="form-control" id="goldar" name="goldar" required>
												@if($data->goldar <> "")
													<option value="{{ $data->goldar }}">{{ $data->goldar }}</option>
												@else
													<option value="O">O</option>
													<option value="A">A</option>
													<option value="B">B</option>
													<option value="AB">AB</option>
												@endif
												</select>
											</div>
											<div class="col-sm-2">
												<label class="control-label" for="rhesus">Rhesus</label>
												<select class="form-control" id="rhesus" name="rhesus" required>
												@if(@$data->rhesus <> "")
													<option value="{{ $data->rhesus }}">{{ $data->rhesus }}</option>
												@else
													<option value="Negatif">Negatif</option>
													<option value="Positif">Positif</option>
												@endif
												</select>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-2">
												<label class="control-label" for="tgllahir">Tanggal Lahir</label>
												<input name="tgllahir" type="text" id="tgllahir" class="form-control tgl" value="{{ $data->tgl_lahir ? : '' }}" required/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="kota_lahir">Tempat Lahir</label>
												<input name="kota_lahir" type="text" id="kota_lahir" class="form-control" value="{{ $data->tempat_lahir ? : '' }}" required/>
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="agama">Agama</label>
												<select class="form-control" id="agama" name="agama" required>
													@if($data->agama <> "")
														<option value="{{ $data->agama }}">{{ $data->agama }}</option>
													@else
														<option value="Islam">Islam</option>
														<option value="Kristen">Kristen</option>
														<option value="Khatolik">Khatolik</option>
														<option value="Buddha">Buddha</option>
														<option value="Konghucu">Konghucu</option>
													@endif
												</select>
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="status_perkawinan">Status Perkawinan</label>
												<select name="status_perkawinan" id="status_perkawinan" class="form-control" required>
													@if($data->status_perkawinan <> "")
														<option value="{{ $data->status_perkawinan }}">{{ $data->status_perkawinan }}</option>
													@else
														<option value="Belum Menikah">Belum Menikah</option>
														<option value="Sudah Menikah">Sudah Menikah</option>
														<option value="Bercerai">Bercerai</option>
													@endif
												</select>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-8">
												<label class="control-label" for="alamat">Alamat</label>
												<textarea name="alamat" style="resize: none" rows="4" type="text" id="alamat" class="form-control" required>{{ $data->alamat ? : '' }}</textarea>
												<div id="map" style="height: 600px; margin-top: 10px;"></div>
												<button type="button" class="btn btn-primary gps_button" style="margin-bottom: 10px;" data-alamat="default"><i class="px-nav-icon ion-map"></i>Koordinat Saya</button>
												<input type="hidden" name="koor_alamat" id="koor_alamat">
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="telpon">No Handphone</label>
												<input name="telpon" type="number" id="telpon" class="form-control number" value="{{ $data->no_telp ? : '' }}" required/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="no_wa">No WhatsApp</label>
												<input name="no_wa" type="number" id="no_wa" class="form-control number" value="{{ $data->no_wa ? : '' }}" required/>
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="noktp">Nomor KTP</label>
												<input name="noktp" type="number" id="noktp" class="form-control number" value="{{ $data->noktp ? : '' }}" required/>
											</div>
											<div class="col-sm-6">
												<label for="provinsi" class="form-label">Provinsi</label>
												<select name="provinsi" id="provinsi" required>
													@foreach($get_area as $key => $val)
														<option value="{{ $key }}">{{ $val }}</option>
													@endforeach
												</select>
											</div>
											<div class="col-sm-6">
												<label for="kota" class="form-label">Kabupaten / Kota</label>
												<select name="kota" id="kota" required></select>
											</div>
											<div class="col-sm-6">
												<label for="kecamatan" class="form-label">Kecamatan</label>
												<select name="kecamatan" id="kecamatan" required></select>
											</div>
											<div class="col-sm-6">
												<label for="kelurahan" class="form-label">Kelurahan</label>
												<select name="kelurahan" id="kelurahan" required></select>
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="email">Email</label>
												<input name="email" type="text" id="email" class="form-control" value="{{ $data->email ? : '' }}" required/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-4">
												<label class="control-label" for="ibu">Nama Ibu Kandung</label>
												<input name="ibu" type="text" id="ibu" class="form-control" value="{{ $data->ibu ? : '' }}" required/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="telpon_2_nm">Nama Keluarga yang Bisa Dihubungi</label>
												<input name="telpon_2_nm" type="text" id="telpon_2_nm" class="form-control" value="{{ $data->telpon_2_nm ? : '' }}" required/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="telpon_2">Kontak Keluarga yang Bisa Dihubungi</label>
												<input name="telpon_2" type="number" id="telpon_2" class="form-control number" value="{{ $data->telpon_2 ? : '' }}" required/>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-8">
												<label class="control-label" for="alamat_urgent">Alamat Darurat</label>
												<textarea name="alamat_urgent" style="resize: none" rows="4" type="text" id="alamat_urgent" class="form-control" required>{{ $data->alamat_urgent ? : '' }}</textarea>
												<div id="map2" style="height: 600px; margin-top: 10px;"></div>
												<button type="button" class="btn btn-primary gps_button" style="margin-bottom: 10px;" data-alamat="urgent"><i class="px-nav-icon ion-map"></i>Koordinat Saya</button>
												<input type="hidden" name="koor_urgent" id="koor_urgent">
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-12">
												<label class="control-label" for="npwp">NPWP</label>
												<input name="npwp" type="text" id="npwp" data-inputmask="'mask': '9{2}.9{3}.9{3}.9{1}-9{3}.9{3}'" class="form-control" value="{{ $data->npwp ? : '' }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="panel-group panel-group-warning panel-group-dark" id="accordion_status_kerja">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_status_kerja" href="#status_kerja" aria-expanded="true">
										STATUS KERJA
									</a>
								</div>
								<div id="status_kerja" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-4">
												<label class="control-label" for="tanggal_aju">Tanggal Pengajuan <small style="color: red">*Naker Baru</small></label>
												<input name="tanggal_aju" type="text" id="tanggal_aju" class="form-control tgl" value="{{ $data->tanggal_aju }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="tglmulaikerja">Tanggal Mulai Kerja <small style="color: red">*Naker Baru</small></label>
												<input name="tglmulaikerja" type="text" id="tglmulaikerja" class="form-control tgl" value="{{ $data->tglmulaikerja }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="jobdesk_hr">Jobdesk HR</label>
												<select name="jobdesk_hr" id="jobdesk_hr" class="form-control">
													@foreach($get_jobdes as $v)
														<option value="{{ $v->text }}" {{ ($v->text == @$data->jobdesk_hr) ? "Selected" : "" }} >{{ $v->text }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-4">
												<label class="control-label" for="status_kerja">Status Kerja</label>
												<select name="status_kerja" id="status_kerja" class="form-control">
												@if($data->status_kerja <> "")
													<option value="{{ $data->status_kerja }}">{{ $data->status_kerja }}</option>
												@else
													<option value="PKS">PKS</option>
													<option value="Karyawan Tetap">Karyawan Tetap</option>
												@endif
												</select>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="direktorat">Direktorat</label>
												<select name="direktorat" id="direktorat" class="form-control">
													<option value="" selected disabled>Pilih Direktorat</option>
													@foreach($get_direktorat as $direktorat)
													<option value="{{ $direktorat->id }}" {{ ($direktorat->id == $data->direktorat_id) ? "Selected" : "" }} >{{ $direktorat->text }}</option>
													@endforeach
												</select>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="sto">STO</label>
												<select name="sto" id="sto" class="form-control">
													<option value="" selected disabled>Pilih STO</option>
													@foreach($get_sto as $v)
													<option value="{{ $v->kode_area }}" {{ ($v->kode_area == @$data->sto) ? "Selected" : "" }} >{{ $v->kode_area }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-4">
												<label class="control-label" for="psa">PSA</label>
												<input name="psa" type="text" id="psa" class="form-control" value="{{ $data->psa }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="witel">Witel</label>
												<select name="witel" id="witel" class="form-control"></select>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="region">Regional</label>
												<input name="region" type="text" id="region" class="form-control" value="6"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="tglawalkerja">Tanggal Awal <small style="color: red">*Naker Baru</small></label>
												<input name="tglawalkerja" type="text" id="tglawalkerja" class="form-control tgl" value="{{ $data->tglawalkerja }}">
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="tglakhirkerja">Tanggal Akhir <small style="color: red">*Naker Baru</small></label>
												<input name="tglakhirkerja" type="text" id="tglakhirkerja" class="form-control tgl" value="{{ $data->tglakhirkerja }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel-group panel-group-info panel-group-dark" id="accordion_pendidikan">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_pendidikan" href="#pendidikan" aria-expanded="true">
										PENDIDIKAN
									</a>
								</div>
								<div id="pendidikan" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="lvl_pendidikan">Jenjang</label>
												<select name="lvl_pendidikan" id="lvl_pendidikan" class="form-control">
													<option value="" selected disabled>Pilih Jenjang Pendidikan Terakhir</option>
													<option value="SD">Sekolah Dasar (SD) / Sederajat</option>
													<option value="SMP">Sekolah Menengah Pertama (SMP) / Sederajat</option>
													<option value="SMA/SMK">SMA / SMK</option>
													<option value="D1">D1</option>
													<option value="D2">D2</option>
													<option value="D3">D3</option>
													<option value="progress_S1">Lanjut Pendidikan S1</option>
													<option value="S1">S1</option>
													<option value="progress_S2">Lanjut Pendidikan S2</option>
													<option value="S2">S2</option>
													<option value="S3">S3</option>
												</select>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="last_study">Pendidikan Terakhir</label>
												<input name="last_study" type="text" id="last_study" class="form-control" value="{{ $data->last_study }}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="tgllulus">Tanggal Lulus</label>
												<input name="tgllulus" type="text" id="tgllulus" class="form-control tgl" value="{{ $data->tgllulus }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="jurusan">Jurusan</label>
												<input name="jurusan" type="text" id="jurusan" class="form-control" value="{{ $data->jurusan }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel-group panel-group-info panel-group-dark" id="accordion_keluarga">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_keluarga" href="#keluarga" aria-expanded="true">
										DATA KELUARGA
									</a>
								</div>
								<div id="keluarga" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="nokk">Nomor Kartu Keluarga</label>
												<input name="nokk" type="text" id="nokk" class="form-control number" value="{{ $data->nokk }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="status_keluarga">Status Dalam Keluarga</label>
												<input name="status_keluarga" type="text" id="status_keluarga" class="form-control" value="{{ $data->status_keluarga }}"/>
											</div>
										</div>
										<hr class="page-wide-block hide_mitra">
										<div class="form-group hide_mitra">
											<div class="col-sm-12">
												<label class="control-label" for="namapasangan">Nama Pasangan</label>
												<input name="namapasangan" type="text" id="namapasangan" class="form-control" value="{{ $data->namapasangan }}"/>
											</div>
										</div>
										<hr class="page-wide-block hide_mitra">
										<div class="form-group hide_mitra">
											<div class="col-sm-12">
												<label class="control-label" for="anak1">Nama Anak I</label>
												<input name="anak1" type="text" id="anak1" class="form-control" value="{{ $data->anak1 }}"/>
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="anak2">Nama Anak II</label>
												<input name="anak2" type="text" id="anak2" class="form-control" value="{{ $data->anak2 }}"/>
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="anak3">Nama Anak III</label>
												<input name="anak3" type="text" id="anak3" class="form-control" value="{{ $data->anak3 }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="panel-group panel-group-warning panel-group-dark" id="accordion_rekening">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_rekening" href="#rekening" aria-expanded="true">
										REKENING
									</a>
								</div>
								<div id="rekening" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-4">
												<label class="control-label" for="norek">Nomor Rekening</label>
												<input name="norek" type="text" id="norek" class="form-control number" value="{{ $data->no_rekening or ''}}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="bank">Bank</label>
												<input name="bank" type="text" id="bank" class="form-control" value="{{ $data->bank or ''}}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="kantor_cabang">Kantor Cabang</label>
												<input name="kantor_cabang" type="text" id="kantor_cabang" class="form-control" value="{{ $data->kantor_cabang_bank or ''}}"/>
											</div>
										</div>
										<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="namarek">Nama Di Rekening</label>
												<input name="namarek" type="text" id="namarek" class="form-control" value="{{ $data->nama_rekening or ''}}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="link_aj">Nomor Link Aja</label>
												<input name="link_aj" type="text" id="link_aj" class="form-control number" value="{{ $data->bank or ''}}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="panel-group panel-group-success panel-group-dark" id="accordion_jamsos">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_jamsos" href="#jamsos" aria-expanded="true">
										JAMSOSTEK & BPJS
									</a>
								</div>
								<div id="jamsos" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-3">
												<label class="control-label" for="jamsostek">Nomor BPJS Ketenaga Kerjaan / Jamsostek</label>
												<input name="jamsostek" type="text" id="jamsostek" class="form-control" value="{{ $data->jamsostek or '' }}"/>
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="tgl-jamsostek">Tanggal BPJS Ketenaga Kerjaan / Jamsostek</label>
												<input name="tgl_jamsostek" type="text" id="tgl-jamsostek" class="form-control tgl" value="{{ $data->tgl_jamsostek or '' }}"/>
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="bpjs">Nomor BPJS</label>
												<input name="bpjs" type="text" id="bpjs" class="form-control number" value="{{ $data->bpjs or '' }}"/>
											</div>
											<div class="col-sm-3">
												<label class="control-label" for="tgl-bpjs">Tanggal BPJS</label>
												<input name="tgl_bpjs" type="text" id="tgl-bpjs" class="form-control tgl" value="{{ $data->tgl_bpjs or '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block hide_mitra">
										<div class="form-group hide_mitra">
											<div class="col-sm-12">
												<label class="control-label" for="no-bpjs-istri">Nomor BPJS Istri</label>
												<input name="bpjs_istri" type="text" id="no-bpjs-istri" class="form-control number" value="{{ $data->bpjs_istri or '' }}"/>
											</div>
											<div class="col-sm-12">
												<label class="control-label" for="tgl-bpjs-istri">Tanggal BPJS Istri</label>
												<input name="tgl_bpjs_istri" type="text" id="tgl-bpjs-istri" class="form-control tgl" value="{{ $data->tgl_bpjs_istri or '' }}"/>
											</div>
										</div>
										<hr class="page-wide-block hide_mitra">
										<div class="form-group hide_mitra">
											<div class="col-sm-6">
												<label class="control-label" for="no-bpjs-anak1">Nomor BPJS Anak I</label>
												<input name="bpjs_anak1" type="text" id="no-bpjs-anak1" class="form-control" value="{{ $data->bpjs_anak1 or '' }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="tgl-bpjs-anak1">Tanggal BPJS Anak I</label>
												<input name="tgl_bpjs_anak1" type="text" id="tgl-bpjs-anak1" class="form-control tgl" value="{{ $data->tgl_bpjs_anak1 or '' }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="no-bpjs-anak2">Nomor BPJS Anak II</label>
												<input name="bpjs_anak2" type="text" id="no-bpjs-anak2" class="form-control" value="{{ $data->bpjs_anak2 or '' }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="tgl-bpjs-anak2">Tanggal BPJS Anak II</label>
												<input name="tgl_bpjs_anak2" type="text" id="tgl-bpjs-anak2" class="form-control tgl" value="{{ $data->tgl_bpjs_anak2 or '' }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="no-bpjs-anak3">Nomor BPJS Anak III</label>
												<input name="bpjs_anak3" type="text" id="no-bpjs-anak3" class="form-control" value="{{ $data->bpjs_anak3 or '' }}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="tgl-bpjs-anak3">Tanggal BPJS Anak III</label>
												<input name="tgl_bpjs_anak3" type="text" id="tgl-bpjs-anak3" class="form-control tgl" value="{{ $data->tgl_bpjs_anak3 or '' }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="panel-group panel-group-default panel-group-dark" id="accordion_mitra">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_mitra" href="#mitra" aria-expanded="true">
										DATA MITRA
									</a>
								</div>
								<div id="mitra" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-12">
												<label class="control-label" for="waspang_nik">NIK Atasan</label>
												<input name="waspang_nik" type="text" id="waspang_nik" class="form-control" value="{{ $data->waspang_nik }}"/>
											</div>
										</div>
											<hr class="page-wide-block">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="mitra_name">Mitra</label>
												<select class="form-control" id="mitra_name" name="mitra_name">
													<option value="" selected disabled>Pilih Nama Mitra</option>
												@foreach($get_mitra as $mitra)
													<option value="{{ $mitra->id }}" {{ ($mitra->id == $data->mitra_pt) ? "Selected" : "" }} >{{ $mitra->text }}</option>
												@endforeach
												</select>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="posisi">Posisi Lapangan</label>
												<select name="posisi" id="posisi" class="form-control">
													@foreach($get_jobdes as $v)
														<option value="{{ $v->text }}" {{ ($v->text == @$data->posisi) ? "Selected" : "" }} >{{ $v->text }}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel-group panel-group-danger panel-group-dark" id="accordion_etc">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_etc" href="#etc" aria-expanded="true">
										LAIN - LAIN
									</a>
								</div>
								<div id="etc" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-6">
												<label class="control-label" for="gadas">Gadas</label>
												<input name="gadas" type="text" id="gadas" class="form-control" value="{{ $data->gadas or ''}}"/>
											</div>
											<div class="col-sm-6">
												<label class="control-label" for="pelatihan">Pelatihan Ala</label>
												<input name="pelatihan" type="text" id="pelatihan" class="form-control" value="{{ $data->pelatihan_ala or ''}}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel-group panel-group-danger panel-group-dark" id="accordion_apd">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_apd" href="#apd" aria-expanded="true">
										APD
									</a>
								</div>
								<div id="apd" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											<div class="col-sm-3">
												<label class="control-label" for="seragam">Ukuran Baju</label>
												<select class="form-control" id="seragam" name="seragam">
													<option value="S">S</option>
													<option value="M">M</option>
													<option value="L">L</option>
													<option value="XL">XL</option>
													<option value="XXL">XXL</option>
												</select>
											</div>
											<div class="col-sm-5">
												<label class="control-label" for="celana">Ukuran Celana / Pinggang (cm)</label>
												<input name="celana" type="text" id="celana" class="form-control number" value="{{ $data->celana or '' }}"/>
											</div>
											<div class="col-sm-4">
												<label class="control-label" for="sepatu">Ukuran Sepatu</label>
												<input name="sepatu" type="text" id="sepatu" class="form-control number" value="{{ $data->sepatu }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="panel-group panel-group-info panel-group-dark" id="accordion_dokumen">
							<div class="panel panel-color panel-blue">
								<div class="panel-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_dokumen" href="#dokumen" aria-expanded="true">
										DOKUMEN
									</a>
								</div>
								<div id="dokumen" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="form-group">
											@foreach ($file as $val => $key)
											<?php
											$check_path = public_path().'/upload4/perwira/recruitment/'.$data->nik;
											$files = @preg_grep('~^'.$key.'.*$~', @scandir($check_path) );


											$filex = NULL;
											if(count($files) != 0)
											{
												$public_path = '/upload4/perwira/recruitment/'.$data->noktp;
												$files = array_values($files);
												$filex = $public_path.'/'.$files[0];
											}
											?>
											@if($filex)
											<div class="col-sm-4">
												<label class="control-label" for="{{ $key }}">{{ $val }}</label>&nbsp;
												<a href="{{ $filex }}" target="_blank" id="{{ $key }}">Liat File</a>
											</div>
											@else
											<div class="col-sm-4">
												<label class="control-label" for="{{ $key }}">{{ $val }}</label>&nbsp;
												<input name="{{ $key }}" type="file" id="{{ $key }}" class="form-control"/>
											</div>
											@endif
											@endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@if(session('auth')->perwira_level != 33)
						<div class="col-md-12">
							<div class="form-group">
								<div style="text-align: center; width: 100%">
									<button type="submit" class="btn btn-lg redirect_me" data-nik= "{{ $data->nik ? : '' }}"><span class="fa fa-cloud-download"></span> Simpan</button>
								</div>
							</div>
						</div>
					@endif
				</div>
			</form>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-fa">
			<div class="page-header">
				<div class="table-primary">
					<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="fa">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Jenis</th>
								<th>Tanggal Pelaksanaan</th>
								<th>Tanggal Selesai</th>
								<th>Judul Pelaksanaan</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse ($fa as $k => $v)
								<tr>
									<td>{{ ++$k }}</td>
									<td>{{ $v->jenis }}</td>
									<td>{{ $v->tgl_start }}</td>
									<td>{{ $v->tgl_end }}</td>
									<td>{{ $v->title }}</td>
								</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@if(in_array(session('auth')->perwira_level, [1, 69]) )
			<div class="tab-pane fade" id="tabs-posisi">
				<div class="page-header">
					<div class="table-info">
						<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="posisi">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th>ID Posisi</th>
									<th>Posisi</th>
									<th>Direktorat</th>
									<th>Witel</th>
									<th>Regional</th>
									<th>Level</th>
									<th class="no-sort">Action</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse ($mutasi as $k => $v)
									<tr>
										<td>{{ ++$k }}</td>
										<td>{{ $v->position_id }}</td>
										<td>{{ $v->pos_name }}</td>
										<td>{{ $v->direktorat }}</td>
										<td>{{ $v->nama_witel }}</td>
										<td>{{ $v->regional }}</td>
										<td>{{ $v->level }}</td>
										<td><button type='button' class='btn btn-sm btn-info show_modal' data-toggle='modal' data-target='#detail_job' data-id="{{ $v->id_p_pos }}"><span class='fa fa-question'></span> (lihat Detail)</button></td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-cuti">
				<div class="page-header">
					<div class="alert alert-warning"><span id="sisa_cut"></span></div>
					<div class="table-info">
						<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="cuti">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th>Jenis Cuti</th>
									<th>Keterangan</th>
									<th>Awal Cuti</th>
									<th>Akhir Cuti</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse ($cuti as $k => $v)
									<tr>
										<td>{{ ++$k }}</td>
										<td>{{ $v->cuti_nm }}</td>
										<td>{{ $v->alasan_cuti }}</td>
										<td>{{ $v->tgl_cuti_awal }}</td>
										<td>{{ $v->tgl_cuti_akhir }}</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		@endif
		<div class="tab-pane fade" id="tabs-reward">
			<div class="page-header">
				<div class="table-success">
					<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="reward">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Tanggal</th>
								<th>Judul Reward / Inovasi</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse ($reward as $k => $v)
								<tr>
									<td>{{ ++$k }}</td>
									<td>{{ $v->tgl }}</td>
									<td>{{ $v->title_archive }}</td>
								</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@if(in_array(session('auth')->perwira_level, [1, 69]) )
			<div class="tab-pane fade" id="tabs-doc">
				<div class="page-header">
				@foreach ($file as $val => $key)
					<?php
					$check_path = public_path().'/upload4/perwira/recruitment/'.session('auth')->noktp;
					$files = @preg_grep('~^'.$key.'.*$~', @scandir($check_path) );


					$filex = NULL;
					if(count($files) != 0)
					{
						$files = array_values($files);
						$filex = $check_path.'/'.$files[0];
					}
					?>
					@if($filex)
						<div class="col-md-4">
							<label class="control-label" for="{{ $key }}">Dokumen {{ $val }}</label>
							<a href="{{ $filex }}" id="{{ $key }}"></a>
						</div>
					@else
						<div class="col-md-4">
							<label class="control-label" for="{{ $key }}">Dokumen {{ $val }}</label><br/>
							File Belum di Upload
						</div>
					@endif
					@endforeach
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-kons">
				<div class="page-header">
					<div class="table-warning">
						<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="kons">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th>Tanggal</th>
									<th>Jenis</th>
									<th>Detail</th>
									<th>Keterangan</th>
									<th>Tindak Lanjut</th>
								</tr>
							</thead>
							<tbody class="middle-align">
								@forelse ($resling as $k => $v)
									<tr>
										<td>{{ ++$k }}</td>
										<td>{{ $v->tgl }}</td>
										<td>{{ $v->jenis }}</td>
										<td>{{ $v->jenis_jns }}</td>
										<td>{{ $v->detail_langgar }}</td>
										<td>{{ $v->tndk_lanjut }}</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		@endif
		<div class="tab-pane fade" id="tabs-accident">
			<div class="page-header">
				<div class="table-danger">
					<table class="table table-bordered table-striped table-small-font" style="width: 100%;" id="accident">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Tanggal</th>
								<th>Alamat Kejadian</th>
								<th>Detail</th>
								<th>Tindak Lanjut</th>
								<th class="no-sort">Kronologi</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@forelse ($accident as $k => $v)
								<tr>
									<td>{{ ++$k }}</td>
									<td>{{ $v->tgl }}</td>
									<td>{{ $v->alamat_acc }}</td>
									<td>{{ $v->detail_acc }}</td>
									<td>{{ $v->action }}</td>
									<td>{{ $v->kronolog }}</td>
								</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js" integrity="sha512-taB73dM5L3JgciedVi4zU6fYBnX1tbhnWUtc/hdBTSgKIWgQnlNvkJYT4uJiin5GiJ2oXiIxrxqjD0Fy5orCxA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
<script src="https://unpkg.com/@turf/turf@6/turf.min.js"></script>
	<script>
		$( function() {
			var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			token = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ',
			map = null,
			map2 = null,
			googleStreets = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleSat = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleTerrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleAlteredRoad = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleTraffic = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
					maxZoom: 20,
					subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
			}),
			locate_map = L.layerGroup(),
			baseLayers = {
				'Jalan': googleStreets,
				'Satelit': googleSat,
				'Satelit Dan Jalan': googleHybrid,
				'Jalur': googleTerrain,
				'Jalur 2': googleAlteredRoad,
				'Lalu Lintas': googleTraffic,
			},
			overlays = {
				'Titik Sekarang': locate_map,
			},
			koor_now = [-3.319000, 114.584100];

			map = L.map('map', {
				center: koor_now,
				zoom: 15,
				maxZoom: 19,
				layers: [googleStreets, googleHybrid, googleSat, googleTerrain, googleTraffic, googleAlteredRoad, locate_map]
			})
			marker = '',
			layerControl = L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map);

			map.flyTo(koor_now, 16);

			if (map.hasLayer(marker) ){
				map.removeLayer(marker);
			}

			$('#show_map_latText').html(koor_now[0]);
			$('#show_map_lngText').html(koor_now[1]);

			marker = new L.marker(koor_now, {
				draggable: 'true'
			});

			map.addLayer(marker);

			marker.on('dragend', function (e) {
				var lat = marker.getLatLng().lat,
				lng = marker.getLatLng().lng;

				$('#show_map_latText').html(lat);
				$('#show_map_lngText').html(lng);

				$("#koor_alamat").val(`${lat}, ${lng}`)
			});

			var googleStreets2 = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleHybrid2 = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleSat2 = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleTerrain2 = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleAlteredRoad2 = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
					maxZoom: 20,
					subdomains:['mt0','mt1','mt2','mt3']
			}),
			googleTraffic2 = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
					maxZoom: 20,
					subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
			}),
			baseLayers2 = {
				'Jalan': googleStreets2,
				'Satelit': googleSat2,
				'Satelit Dan Jalan': googleHybrid2,
				'Jalur': googleTerrain2,
				'Jalur 2': googleAlteredRoad2,
				'Lalu Lintas': googleTraffic2,
			},
			titik_koordinat_2 = L.layerGroup(),
			overlays2 = {
				'Koordinat': titik_koordinat_2,
			};

			map2 = L.map('map2', {
				center: koor_now,
				zoom: 15,
				maxZoom: 19,
				layers: [googleStreets2, googleHybrid2, googleSat2, googleTerrain2, googleTraffic2, googleAlteredRoad2, titik_koordinat_2]
			}),
			marker2 = '',
			layerControl2 = L.control.layers(baseLayers2, overlays2, {position: 'topleft'}).addTo(map2)

			map2.flyTo(koor_now, 16);

			if (map2.hasLayer(marker2) ){
				map2.removeLayer(marker2);
			}

			$('#show_map_latText').html(koor_now[0]);
			$('#show_map_lngText').html(koor_now[1]);

			marker2 = new L.marker(koor_now, {
				draggable: 'true'
			});

			map2.addLayer(marker2);

			marker2.on('dragend', function (e) {
				var lat = marker2.getLatLng().lat,
				lng = marker2.getLatLng().lng;

				$('#show_map_latText').html(lat);
				$('#show_map_lngText').html(lng);

				$("#koor_urgent").val(`${lat}, ${lng}`)
			});

			$('.gps_button').on('click', function(){
				var current_map = null,
				btn = 0,
				alamat_object = $(this).data('alamat');

				if (alamat_object == 'default') {
					current_map = map;
					btn = 1;

					if(marker.length != 0){
						current_map.removeLayer(marker);
					}
				}

				if (alamat_object == 'urgent') {
					current_map = map2;
					btn = 2;

					if(marker2.length != 0){
						current_map.removeLayer(marker2);
					}
				}

        navigator.geolocation.getCurrentPosition(position => {
          const { coords: { latitude, longitude } } = position;

					switch(btn) {
						case 1:
							marker = new L.marker([latitude, longitude], {
								autoPan: true,
								draggable: 'true'
							}).addTo(current_map);

							$("#koor_alamat").val(`${latitude}, ${longitude}`)
						break;
						case 2:
							marker2 = new L.marker([latitude, longitude], {
								autoPan: true,
								draggable: 'true'
							}).addTo(current_map);

							$("#koor_urgent").val(`${latitude}, ${longitude}`)
						break;
					}

          current_map.flyTo([latitude, longitude], 16);
        })
      });

			$('#submit_me').on('submit', function(e){
				var nik = $('.redirect_me').attr('data-nik');
				// window.open('https://t.me/Tperwira_bot?start='+nik, '_blank');
			});

			$('select').select2();

			$(":input").inputmask();

			$('.number').on('keyup', function(e){
				if (e.which != 8 && e.which != 0 && e.which < 48 || e.which > 57) {
					$(this).val(function (index, value) {
						return value.replace(/\D/g, "");
					});
				}
			});

			$('.tgl').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',
        daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',

        beforeShowMonth: function(date) {
          if (date.getMonth() === 8) {
            return false;
          }
        },

        beforeShowYear: function(date){
          if (date.getFullYear() === 2014) {
            return false;
          }
        }
      });

			$(".modal").on('shown.bs.modal', function(){
				$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
			});

			$('.show_modal').on('click', function(){
				$.ajax({
					type: 'GET',
					url: '/jx_dng/detail_job_pos',
					data:{
						data: $(this).attr('data-id')
					},
					dataType:'JSON'
				}).done( function(e){
					var main_table = $.map(e, function(value, key){
						return [value]
					});
					$('#detail_mut').DataTable().row.add(main_table).draw();
				});
			});

			$('#witel').select2({
				width: '100%',
				placeholder: "Ketik Nama Witel",
				allowClear: true,
				ajax: {
					url: '/jx_dng/get_witel',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('#witel').on('select2:select change', function(){
				$.ajax({
					type: 'GET',
					url: '/jx_dng/get_psa',
					dataType: 'JSON',
					data: {
						data: $(this).val()
					}
				}).done( function(e){
					$('#psa').val(e.psa_witel)
				})
			})

			$('#provinsi').val(null).change();

			$('#provinsi').select2({
				placeholder: 'Data Kosong'
			});

			var data_php = {!! json_encode($data) !!};

			// console.log(data_php);

			$('#provinsi').on('select2:select change', function(){
				$.ajax({
					type: "GET",
					url: '/rec/get_region',
					dataType: 'JSON',
					data: {
						data: $(this).val(),
						jenis: 'kota'
					}
				}).done( function(e){
					var data = [];
					$.each(e.kota_kabupaten, function(key, val){
						data.push({
							id: val.id,
							text: val.nama
						});
					});

					$('#kota').empty();

					$('#kota').select2({
						placeholder: "Data Kosong",
						data: data
					});

					$('#kota').val(null).change();

					if(!$.isEmptyObject(data_php) ){
						// console.log(data_php, data_php.kota)
						$('#kota').val(data_php.kota).change();
					}
				});
			});

			$('#kota').on('select2:select change', function(){
				// console.log('dirubah')

				$.ajax({
					type: "GET",
					url: '/rec/get_region',
					dataType: 'JSON',
					data: {
						data: $(this).val(),
						jenis: 'kecamatan'
					}
				}).done( function(e){
					// console.log(e)
					var data = [];
					$.each(e.kecamatan, function(key, val){
						data.push({
							id: val.id,
							text: val.nama
						});
					});
					$('#kecamatan').empty();

					$('#kecamatan').select2({
						placeholder: "Data Kosong",
						data: data
					});

					$('#kecamatan').val(null).change();
					if(!$.isEmptyObject(data_php) ){
						$('#kecamatan').val(data_php.kecamatan).change();
					}
				});
			});

			$('#kecamatan').on('select2:select change', function(){
				$.ajax({
					type: "GET",
					url: '/rec/get_region',
					dataType: 'JSON',
					data: {
						data: $(this).val(),
						jenis: 'kelurahan'
					}
				}).done( function(e){
					var data = [];
					$.each(e.kelurahan, function(key, val){
						data.push({
							id: val.id,
							text: val.nama
						});
					});
					$('#kelurahan').empty();

					$('#kelurahan').select2({
						placeholder: "Data Kosong",
						data: data
					});

					$('#kelurahan').val(null).change();
					if(!$.isEmptyObject(data_php) ){
						$('#kelurahan').val(data_php.kelurahan).change();
					}
				});
			});

			$(".table").DataTable({
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});

			$(".modal").on('shown.bs.modal', function(){
				$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
			});

			$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    	} );

			$(".table").DataTable();

			$('#posisi_wrapper .table-caption').text('List History Posisi Karyawan');
			$('#posisi_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#fa_wrapper .table-caption').text('List History Pelatihan FA');
			$('#fa_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#cuti_wrapper .table-caption').text('List History Cuti');
			$('#cuti_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#reward_wrapper .table-caption').text('List History Reward Karyawan');
			$('#reward_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#kons_wrapper .table-caption').text('List History SP dan Resign Karyawan');
			$('#kons_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#accident_wrapper .table-caption').text('List Kecelakaan Karyawan');
			$('#accident_wrapper .dataTables_filter input').attr('placeholder', 'Search...');


			$.ajax({
				type: 'GET',
				url: '/jx_dng/get_sisa_cuti',
				data: {
					data: data_php.id_people
				}
			}).done( function(e){
				// console.log(e)
				var sisa = parseInt(e);

				if(sisa > 0){
					$('#sisa_cut').html('Sisa Cuti Tahun '+ new Date().getFullYear() +' Adalah '+ sisa +' Hari');
				}else{
					$('#sisa_cut').html('Tidak Memiliki Sisa Cuti Lagi Untuk Tahun '+ new Date().getFullYear() );
				}

			});

			$('#witel').append(new Option(data_php.Witel_New, data_php.witel, true, true) ).change();

			$('#provinsi').val(data_php.provinsi).change();

			var raw_class= ['.hide_mitra', '.hide_ta'];

			if(data_php.mitra_amija.indexOf('TELKOM') !== -1 ){
				var value = '.hide_mitra';
				kelas = raw_class.filter(function(item) {
						return item !== value
				});

				$('.hide_mitra').css({
					'display': 'inline'
				});

				$(kelas.join(',') ).css({'display': 'none'});
			} else {
				var value = '.hide_ta';

				$('.hide_ta').css({
					'display': 'inline'
				});

				kelas = raw_class.filter(function(item) {
					return item !== value
				});

				$(kelas.join(',') ).css({'display': 'none'});
			}

		});
  </script>
@endsection