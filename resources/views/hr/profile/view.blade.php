@extends('layout')
@section('title')
View
@endsection
@section('css')
<style>
	/* */

	.panel-default>.panel-heading {
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-color>.panel-heading a:after {
  content: "";
  position: relative;
  top: 1px;
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
	float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-color>.panel-heading a[aria-expanded="false"]:after {
  content: "\f067";
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.panel-color>.panel-heading a[aria-expanded="true"]:after {
  content: "\f068";
  -webkit-transform: rotate(0deg);
  transform: rotate(0deg);
}
</style>
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
	  	@if(in_array(session('auth')->perwira_level,[69,1,30,46,71]) )
			<div class="col-md-12">
			<div class="panel panel-info">
			<div class="panel-heading">
				<div class="panel-title">Nama : {{ $data->nama }}</div>
			</div>
			<div class="panel-body">
				<form method="post">
				<input type="hidden" class="form-control" name="id" value="{{ $data->id_recruitment }}">
				<input type="hidden" class="form-control" name="noktp" value="{{ $data->noktp }}">
					<div class="form-group">
						<label for="input-status">Status</label>
						<select class="form-control" name="input_status" id="input-status">
						<option value="" selected disabled>Pilih Status</option>
						@if($grafik == "pelamar")
							<option value="REKRUT">RECRUITMENT</option>
						@else
							@if(in_array(session('auth')->perwira_level,[1,69]) )
							{{-- <option value="REJECTED">TIDAK LOLOS</option> --}}
							<option value="BLACKLIST">BLACKLIST</option>
							@endif
							<option value="ACCEPTED_TA">DI TERIMA / LOLOS (TA)</option>
							<option value="ACCEPTED_MITRA">DI TERIMA / LOLOS (MITRA)</option>
						@endif
						</select>
					</div>
					<div class="row accepted">
						<div class="form-group">
							<div class="col-sm-3">
								<label for="nik">NIK</label><br/>
								<input type="text" class="form-control" name="nik" id="nik" placeholder="Masukan NIK Karyawan">
							</div>
							<div class="col-sm-3">
								<label for="laborcode">Laborcode</label><br/>
								<input type="text" class="form-control" name="laborcode" id="laborcode" placeholder="Masukan Laborcode">
							</div>

							<div class="col-sm-3">
								<label for="tgl_usulan">Tanggal Usulan</label><br/>
								<input type="date" class="form-control" name="tgl_usulan" id="tgl_usulan">
							</div>
							<div class="col-sm-3">
								<label for="tgl_selesai">Tanggal Selesai / Mulai Bekerja</label><br/>
								<input type="date" class="form-control" name="tgl_selesai" id="tgl_selesai">
							</div>
						</div>
					</div>
					{{-- <div class="row accepted">
						<div class="form-group">
							<div class="col-sm-6">
								<label for="input-direktorat">Direktorat</label>
								<select class="form-select" name="direktorat" id="input-direktorat">
									<option value="" selected disabled>Pilih Direktorat</option>
									@foreach ($get_direktorat as $direktorat)
									<option value="{{ $direktorat->id }}">{{ $direktorat->direktorat }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-6">
								<label for="input-subgroup">Sub Group</label>
								<select class="form-select" name="subgroup" id="input-subgroup">
									<option value="" selected disabled>Pilih Sub Group</option>
									@foreach ($get_subgroup as $sub)
									<option value="{{ $sub->id }}">{{ $sub->text }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div> --}}
					<div class="row accepted">
						<div class="form-group">
							<div class="col-sm-3">
								<label for="input-subgroup">Sub Group</label>
								<select class="form-select" name="subgroup" id="input-subgroup">
									<option value="" selected disabled>Pilih Sub Group</option>
									@foreach ($get_subgroup as $sub)
									<option value="{{ $sub->id }}">{{ $sub->text }}</option>
									@endforeach
								</select>
							</div>

							<div class="col-sm-3">
								<label for="input-posisi-mitra">Posisi Mitra</label>
								<select class="form-select" name="posisi_mitra" id="input-posisi-mitra">
									<option value="" selected disabled>Pilih Posisi</option>
									@foreach ($get_posisi_mitra as $posisi_mitra)
									<option value="{{ $posisi_mitra->id_posisi_mitra }}">{{ $posisi_mitra->text }}</option>
									@endforeach
								</select>
							</div>

							<div class="col-sm-3">
								<label for="input-perusahaan-mitra">Perusahaan Mitra</label>
									<select class="form-select" name="perusahaan_mitra" id="input-perusahaan-mitra">
									<option value="" selected disabled>Pilih Perusahaan</option>
									@foreach ($get_perusahaan_mitra as $perusahaan_mitra)
									<option value="{{ $perusahaan_mitra->id_perusahaan_mitra }}">{{ $perusahaan_mitra->text_perusahaan_mitra }}</option>
									@endforeach
								</select>
							</div>

							<div class="col-sm-3">
								<label for="nik_waspang">NIK Waspang</label><br/>
								<input type="text" class="form-control" name="nik_waspang" id="nik_waspang" placeholder="Masukan NIK Waspang Aktif dari TA">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<br/>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
			</div>
			</div>
		@endif
			<div class="form-group detail_data">
				{{-- GOLONGAN DARAH (YG MITRA AJA HAPUS, TA JANGAN
				NAMA PASANGAN HAPUS (KHUSUS MITRA DIHAPUS, TA JANGAN)
				NAMA ANAK 1 SAMPAI 3 (KHUSUS MITRA DIHAPUS, TA JANGAN)
				HAPUS
				PASANGAN DAN ANAK DIHAPUS DI MIRA ( TA JANGAN)
				MITRA DITAMPILKAN ( TA DIHAPUS) --}}
				<div class="col-md-12">
					<div class="panel-group panel-group-info panel-group-dark" id="accordion_self_data">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_self_data" href="#self_data" aria-expanded="true">DATA PRIBADI</a>
							</div>
							<div id="self_data" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-3">
											<label class="control-label" for="nama">Nama</label>
											<input name="nama" type="text" id="nama" class="form-control" value="{{ $data->nama }}" readonly/>
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="jk">Jenis Kelamin</label>
											<input name="jk" type="text" id="jk" class="form-control" value="{{ $data->jk }}" readonly/>
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="goldar">Golongan Darah</label>
											<input name="goldar" type="text" id="goldar" class="form-control" value="{{ $data->goldar }}" readonly/>
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="tgllahir">Tanggal Lahir</label>
											<input name="tgllahir" type="text" id="tgllahir" class="form-control tgl" value="{{ $data->tgl_lahir }}" readonly/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="kota_lahir">Tempat Lahir</label>
											<input name="kota_lahir" type="text" id="kota_lahir" class="form-control" value="{{ $data->tempat_lahir }}" readonly/>
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="agama">Agama</label>
											<input name="agama" type="text" id="agama" class="form-control" value="{{ $data->agama }}" readonly/>
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="status_perkawinan">Status Perkawinan</label>
											<input name="status_perkawinan" type="text" id="status_perkawinan" class="form-control" value="{{ $data->status_perkawinan }}" readonly/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-8">
											<label class="control-label" for="alamat">Alamat</label>
											<textarea name="alamat" style="resize: none" rows="4" type="text" id="alamat" class="form-control" readonly>{{ $data->alamat }}</textarea>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="telpon">No Handphone</label>
											<input name="telpon" type="number" id="telpon" class="form-control number" value="{{ $data->no_telp }}" readonly/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="no_wa">No WhatsApp</label>
											<input name="no_wa" type="number" id="no_wa" class="form-control number" value="{{ $data->no_wa }}" readonly/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="noktp">Nomor KTP</label>
											<input name="noktp" type="text" id="noktp" class="form-control number" value="{{ $data->noktp }}" readonly/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="noktp">Nomor KK</label>
											<input name="nokk" type="text" id="nokk" class="form-control number" value="{{ $data->nokk }}" readonly/>
										</div>
										<div class="col-sm-6">
											<label for="provinsi" class="form-label">Provinsi</label>
											<input name="nama_provinsi" type="text" id="nama_provinsi" class="form-control" value="{{ $data->nama_provinsi }}" readonly/>
										</div>
										<div class="col-sm-6">
											<label for="kota" class="form-label">Kabupaten / Kota</label>
											<input name="nama_kota" type="text" id="nama_kota" class="form-control" value="{{ $data->nama_kota }}" readonly/>
										</div>
										<div class="col-sm-6">
											<label for="kecamatan" class="form-label">Kecamatan</label>
											<input name="nama_kecamatan" type="text" id="nama_kecamatan" class="form-control" value="{{ $data->nama_kecamatan }}" readonly/>
										</div>
										<div class="col-sm-6">
											<label for="kelurahan" class="form-label">Kelurahan</label>
											<input name="nama_kelurahan" type="text" id="nama_kelurahan" class="form-control" value="{{ $data->nama_kelurahan }}" readonly/>
										</div>
										<div class="col-sm-12">
											<label class="control-label" for="email">Email</label>
											<input name="email" type="text" id="email" class="form-control" value="{{ $data->email }}" readonly/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="ibu">Nama Ibu Kandung</label>
											<input name="ibu" type="text" id="ibu" class="form-control" value="{{ $data->ibu }}" readonly/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="telpon_2_nm">Nama Keluarga yang Bisa Dihubungi</label>
											<input name="telpon_2_nm" type="text" id="telpon_2_nm" class="form-control" value="{{ $data->telpon_2_nm }}" readonly/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="telpon_2">Kontak Keluarga yang Bisa Dihubungi</label>
											<input name="telpon_2" type="number" id="telpon_2" class="form-control number" value="{{ $data->telpon_2 }}" readonly/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-12">
											<label class="control-label" for="npwp">NPWP</label>
											<input name="npwp" type="text" id="npwp" data-inputmask="'mask': '9{2}.9{3}.9{3}.9{1}-9{3}.9{3}'" class="form-control" value="{{ $data->npwp }}" readonly/>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel-group panel-group-info panel-group-dark" id="accordion_pendidikan">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_pendidikan" href="#pendidikan" aria-expanded="true">
									PENDIDIKAN
								</a>
							</div>
							<div id="pendidikan" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="lvl_pendidikan">Jenjang</label>
											<input name="lvl_pendidikan" type="text" id="lvl_pendidikan" class="form-control" value="{{ $data->lvl_pendidikan }}" readonly/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="last_study">Pendidikan Terakhir</label>
											<input name="last_study" type="text" id="last_study" class="form-control" value="{{ $data->last_study }}" readonly/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="tgllulus">Tanggal Lulus</label>
											<input name="tgllulus" type="text" id="tgllulus" class="form-control tgl" value="{{ $data->tgllulus }}" readonly/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="jurusan">Jurusan</label>
											<input name="jurusan" type="text" id="jurusan" class="form-control" value="{{ $data->jurusan }}" readonly/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel-group panel-group-info panel-group-dark" id="accordion_dokumen">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_dokumen" href="#dokumen" aria-expanded="true">
									DOKUMEN
								</a>
							</div>
							<div id="dokumen" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										@foreach ($file as $val => $key)
											@php
												$filex = null;
							
												$check_path = public_path('/upload4/perwira/recruitment/'.$data->noktp);
												$files = @preg_grep('~^'.$key.'.*$~', @scandir($check_path));
												
												if (count($files) != 0) {
													$public_path = '/upload4/perwira/recruitment/'.$data->noktp;
													$files = array_values($files);
													$filex = $public_path.'/'.$files[0];
												}
							
												if ($filex === null) {
													$check_path = public_path('/upload5/perwira/recruitment/'.$data->noktp);
													$files = @preg_grep('~^'.$key.'.*$~', @scandir($check_path));
													
													if (count($files) != 0) {
														$public_path = '/upload5/perwira/recruitment/'.$data->noktp;
														$files = array_values($files);
														$filex = $public_path.'/'.$files[0];
													}
												}
											@endphp
											@if ($filex)
												<div class="col-sm-4">
													<label class="control-label" for="{{ $key }}">{{ $val }}</label>
													<a href="{{ $filex }}" target="_blank" id="{{ $key }}">Lihat File</a>
												</div>
											@else
												<div class="col-sm-4">
													<label class="control-label" for="{{ $key }}">{{ $val }}</label>
													File Belum di Upload
												</div>
											@endif
										@endforeach
										<div class="col-sm-4">
											<label class="control-label" for="all_file">Semua File</label>
											<a href="/download_all_file/{{$data->noktp}}" target="_blank" id="all_file">Download</a>
										</div>
									</div>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js" integrity="sha512-taB73dM5L3JgciedVi4zU6fYBnX1tbhnWUtc/hdBTSgKIWgQnlNvkJYT4uJiin5GiJ2oXiIxrxqjD0Fy5orCxA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script>
		$( function(){
			$('select').select2();

			$('.accepted').hide();

			$('#nik').on('keyup', function(){
				$('#laborcode').val($(this).val() )
			})

			$('#input-status').change(function(){
				if ($.inArray($(this).val(), ['ACCEPTED_TA', 'ACCEPTED_MITRA']) !== -1) {
					$('.accepted').show();
					if($(this).val() == 'ACCEPTED_TA'){
						$('#input-perusahaan-mitra').val(0).change();
						$('#input-perusahaan-mitra').prop('disabled', true);
					}else{
						$('#input-perusahaan-mitra').removeAttr('disabled');
					}
				} else {
					$('.accepted').hide();
				}
			});
			$('#input-status').val(null).change()
		});
	</script>
@endsection