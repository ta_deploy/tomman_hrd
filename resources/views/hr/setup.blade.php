@extends('layout')

@section('html-title')
	 HR :: TOMMAN
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Project : 
@endsection

@section('page-title')
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2.css">
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2-bootstrap.css">
	<style>
		#keperluan{
			height : 98px;
			resize : none;
		}
	</style>
	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
		<li><i class="linecons-calendar"></i>HR</li>
		<li class="active"><i class="linecons-tag">Setup HR</i> </li>
	</ol>
@endsection

@section('body')
	<div class="row">
  		<div class="col-sm-3">
			<a href="/hr/listJobPosition">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-cog"></i>
				</div>
				<div class="xe-label">
					<strong >Position Name</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a href="/hr/listSumaryJobPosition">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-cog"></i>
				</div>
				<div class="xe-label">
					<strong >Summary Position Name</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a href="/hr/formWitel">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-cog"></i>
				</div>
				<div class="xe-label">
					<strong >Witel</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a href="/hr/formLoker/input">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-cog"></i>
				</div>
				<div class="xe-label">
					<strong >PSA</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a href="/hr/formSto/input">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-cog"></i>
				</div>
				<div class="xe-label">
					<strong >STO</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a href="/hr/formJenisCuti">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-cog"></i>
				</div>
				<div class="xe-label">
					<strong >Jenis Cuti</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a href="/hr/listTtd">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-cog"></i>
				</div>
				<div class="xe-label">
					<strong >TTD dan Atasan</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a href="/hr/listMitra">
			<div class="xe-widget xe-counter xe-counter-gray">
				<div class="xe-icon">
					<i class="linecons-cog"></i>
				</div>
				<div class="xe-label">
					<strong >Mitra</strong>
					<span></span>
				</div>
			</div>
			</a>
		</div>
	</div>
@endsection

@section('plugins')
	<script src="/lib/xenon/js/datepicker/bootstrap-datepicker.js"></script>
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/select2/select2.min.js"></script>
	<script src="/lib/xenon/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="/lib/xenon/js/inputmask/jquery.inputmask.bundle.js"></script>
@endsection

@section('footer')
<script>
		
</script>

@endsection