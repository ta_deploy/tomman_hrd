@extends('layout')
@section('title')
Input Tanda Tangan
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="panel-group">
				<div class="panel panel-color panel-info panel-border">
					<div class="panel-heading">
						<div class="panel-title">Form Pengisian Tanda Tangan</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-3">
								<label class="control-label" for="Nik">Nik :</label>
								<select class="form-control" name="Nik" id="Nik"></select>
							</div>
							<div class="col-sm-4">
								<label class="control-label" for="nama">Nama :</label>
								<input type="text" class="form-control" name="nama" id="nama" disabled>
							</div>
							<div class="col-sm-3">
								<label class="control-label" for="jabatan">Posisi :</label>
								<input type="text" class="form-control" name="jabatan" id="jabatan" disabled>
							</div>
							<div class="col-sm-2">
								<label class="control-label" for="status">Status</label>
								<select class="form-control" name="status" id="status">
									<option value="Active">Active</option>
									<option value="Non-Active">Non-Active</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class=" col-sm-12">
								<button class="btn btn-block btn-primary" type="submit">
									<span class="fa fa-cloud-download"></span>
									<span>Simpan</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			$('#Nik').select2({
				width: '100%',
				placeholder: "Ketik Nama Karyawan / KK / Nik Sementara",
				minimumInputLength: 7,
				allowClear: true,
				ajax: {
					url: "/jx_dng/get_user",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					language: {
						noResults: function(){
							return "Nama Karyawan / KK / Nik Sementara Tidak Terdaftar!";
						}
					},
					cache: true,
					success: function(value) {
					}
				}
			});

			$('#Nik').on('select2:select change.select2', function(){
				console.log('tes')
				$.ajax({
					type: 'GET',
					url: '/jx_dng/get_user_mutasi',
					data: {
						data: $(this).val()
					},
					dataType: 'JSON'
				}).done( function(e){
					console.log(e)
					$('#nama').val(e.nama);
					$('#jabatan').val(e.c_pos_name);
				});
			});

			var data = <?= json_encode($data) ?>;
			if(data){
    		$('#Nik').append(new Option(data[0].nama +'('+ data[0].id_nik +')', data[0].people_id, true, true) ).trigger('change');
			}

		});
	</script>
@endsection