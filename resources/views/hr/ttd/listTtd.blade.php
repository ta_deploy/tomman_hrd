@extends('layout')
@section('title')
List Tanda Tangan
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	<a href="/hr/new_ttd/input" type="button" class="btn btn-primary btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Tanda Tangan</a>
  <div class="page-header">
		<div class="table-info">
			<table class="table table-bordered table-striped table-small-font" id="table_ku">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th>NIK</th>
						<th>Nama</th>
						<th>Jabatan</th>
						<th>Status</th>
						<th class="no-sort">Action</th>
					</tr>
				</thead>
				<tbody class="middle-align">
					@foreach($data as $key => $d)
						<tr>
							<td>{{ ++$key }}</td>
							<td>{{ $d->people_id }}</td>
							<td>{{ $d->people_id }}</td>
							<td>{{ $d->people_id }}</td>
							<td>{{ $d->status }}</td>
							<td><a type='button' href='/hr/update_ttd/{{ $d->id }}' class='btn btn-sm btn-info'><span class='fa fa-question'></span> Edit</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$(".table").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});

			$('#table_ku_wrapper .table-caption').text('List Tanda Tangan');
			$('#table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
		});
  </script>
@endsection