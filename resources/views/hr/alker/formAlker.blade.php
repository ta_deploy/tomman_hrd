@extends('layout')
@section('title')
Form Alat Kerja
@endsection
@section('content')
<div id="add_alker_new" class="modal fade modal-large add_alker_new">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">Input Alat Kerja Baru</div>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="panel panel-color panel-info panel-border">
						<div class="panel-heading">
							<div class="panel-title title_kar">Alat Kerja Baru</div>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="new_alker">Nama Alker :</label>
								<div class="col-md-9">
									<input type="text" name="new_alker" class="form-control" id="new_alker">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info modal_submit" data-dismiss="modal"><span class="fa fa-cloud-download"></span> Simpan</button>
			</div>
		</div>
	</div>
</div>
<div class="px-content">
	<div class="partial_alert"></div>
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="col-md-12">
			<div class="panel panel-color panel-info panel-border">
				<div class="panel-heading">
					<div class="panel-title title_kar">Alat Kerja</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="merk_alker">Merk Alat Kerja :</label>
						<div class="col-md-9">
							<input name="merk_alker" class="form-control" id="merk_alker">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="alker">Alat Kerja :</label>
						<div class="col-md-9">
							<select name="alker" class="form-control" id="alker">
							</select>
						</div>
					</div>
					<div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
            </div>
          </div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script>
		$( function(){
			$('select').select2();

			$('#alker').select2({
				width: '100%',
				placeholder: "Ketik Nama Alat Kerja",
				allowClear: true,
				minimumInputLength:2,
				ajax: {
					url: '/jx_dng/get_alk',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				language: {
					noResults: function(){
						return "Alat Kerja Tidak Ada? <button type='button' class='btn btn-sm btn-info' data-toggle='modal' data-target='#add_alker_new'><span class='ion-plus-circled'></span> Tambah Alker Baru</button>";
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('.modal_submit').on('click', function(){
				var fd = new FormData(),
				token = <?= json_encode(csrf_token() ) ?>;
				fd.append( 'new_alker', $('#new_alker').val() );
				fd.append( '_token', token );

				$.ajax({
					url: '/report/tools/addNewAlker',
					data: fd,
					processData: false,
					contentType: false,
					dataType: 'JSON',
					type: 'POST',
					success: function(data){
						$('.partial_alert').html("<div class='alert alert-"+ data.msg.type+ "'>"+ data.msg.text, "</div>");
					}
				});
			});
		});
	</script>
@endsection