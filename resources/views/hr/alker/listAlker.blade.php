@extends('layout')
@section('title')
List Alat Kerja
@endsection
@section('css')
<style>
	.badge {
		color: #000;
	}
</style>
@endsection
@section('content')
<div class="px-content">
	@include('Partial.alerts')
	@if(!in_array(session('auth')->perwira_level, [71]) )
		<a href="/report/tools/addAlker" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Alat Kerja</a>
	@else
		<a href="/mitra/tools/addAlker" type="button" class="btn btn-info btn-3d" style="margin-bottom: 10px;"><span class="fa fa-plus"></span> Alat Kerja (Kalau mitra yang login)</a>
	@endif
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tabs-teknisi_alker" data-toggle="tab">
				Teknisi <span class="label label-info count_user_alker"></span>
			</a>
		</li>
		<li>
			<a href="#tabs-alker" data-toggle="tab">
				Alat Kerja <span class="label label-info">{{ count($alker_detail) }}</span>
			</a>
		</li>
		<li>
			<a href="#tabs-jenis_alker" data-toggle="tab">
				Jenis Alat Kerja <span class="badge badge-default">{{ count($alker) }}</span>
			</a>
		</li>
	</ul>
	<div class="tab-content tab-content-bordered">
		<div class="tab-pane fade in active" id="tabs-teknisi_alker">
			<div class="page-header">
				<div class="table-info">
					<div class="panel-body">
						<div class="input-group col-md-12">
							<div class="col-sm-1">
								<label class="control-label" for="input-status">Filter By</label>
							</div>
							<div class="col-sm-2">
								<select name="witel" id="input-witel" class="form-control select_opsi">
								</select>
							</div>
							<div class="col-sm-3">
								<select name="unit" id="input-unit" class="form-control select_opsi">
								</select>
							</div>
							<div class="col-sm-3">
								<select name="posisi" id="input-posisi" class="form-control select_opsi">
								</select>
							</div>
							<div class="col-sm-3">
								<select name="umur" id="input-umur" class="form-control select_opsi">
									<option value="_6_bulan">> 6 Bulan</option>
									<option value="_1_tahun">> 1 Tahun</option>
								</select>
								<code>* {{ ucwords('1 tahun hanya untuk motor, tangga, laptop, splicer, OTDR, dan mini OTDR') }}</code>
							</div>
						</div>
					</div>
					<table class="table table-bordered table-striped table-small-font" id="nama_table_ku" style="width: 100%;">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Nama</th>
								<th>Unit</th>
								<th>Posisi</th>
								<th>Witel</th>
								<th>List Item</th>
							</tr>
						</thead>
						<tbody class="middle-align"></tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-alker">
			<div class="page-header">
				<div class="table-info">
					<table class="table table-bordered table-striped table-small-font" id="table_ku" style="width: 100%;">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Alat Kerja</th>
								<th>Merek</th>
								<th>Nama Barang</th>
								<th>Asset</th>
								<th>Spesifikasi</th>
								{{-- <th class="no-sort">Action</th> --}}
							</tr>
						</thead>
						<tbody class="middle-align">
							@foreach ($alker_detail as $key => $d )
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $d->nama_alker }}</td>
									<td>{{ $d->merk }}</td>
									<td>{{ $d->nama }}</td>
									<td>{{ $d->aset }}</td>
									<td>{{ $d->spek }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tabs-jenis_alker">
			<div class="page-header">
				<div class="table-default">
					<table class="table table-bordered table-striped table-small-font" id="jenis_table_ku" style="width: 100%;">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th>Alat Kerja</th>
								<th>Asset</th>
								<th>Spesifikasi</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							@foreach ($alker as $key => $d )
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $d->alker }}</td>
									<td>{{ $d->aset }}</td>
									<td>{{ $d->spek }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$( function() {
			$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    	} );

			$("#table_ku, #jenis_table_ku").DataTable({
				"columnDefs": [{
          "targets": 'no-sort',
          "orderable": false,
   			 }]
			});

			$('#nama_table_ku_wrapper .table-caption').text('List Teknisi Dengan Alker/Sarker');
			$('#nama_table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#table_ku_wrapper .table-caption').text('List Alat Kerja');
			$('#table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

			$('#jenis_table_ku_wrapper .table-caption').text('List Jenis Alat Kerja');
			$('#jenis_table_ku_wrapper .dataTables_filter input').attr('placeholder', 'Search...');


			function render_list_item(data, type, row, meta){
				if(type === 'display'){
					data = '<ol>';
						$.each(row.barang, function(k, v){
							var badge_alker_sarker = (v.spek.toUpperCase() == 'SARKER' ? 'info' : 'primary');
							data += `<li>${v.nama_brand} <span class='badge badge-${badge_alker_sarker}'>${v.merk_brand}</span> || <span class='badge badge-${badge_alker_sarker}'>${v.alker} (${v.aset} / ${v.spek})</span> || <span class='badge badge-warning'>${v.umur} (${v.tanggal})</span></li>`;
						})
					data += '</ol>';
				}
				return data;
			}

			var table_user_alker = $('#nama_table_ku').DataTable({
				processing: true,
				serverSide: true,
				ajax: {
					url: '/jx_dng/get_alker_teknisi',
					type: 'POST',
					data: function (d) {
						d.witel = $('#input-witel').val(),
						d.unit = $('#input-unit').val(),
						d.posisi = $('#input-posisi').val(),
						d.umur = $('#input-umur').val()
					},
				},
				select: true,
				lengthMenu: [
					[10, 25, 50, -1],
					[10, 25, 50, "All"]
				],
				columnDefs: [{
					targets: "datatable-nosort",
					orderable: false,
				}],
				columns: [
					{ data: 'no', name: 'no' },
					{ data: 'nama', name: 'nama' },
					{ data: 'unit', name: 'unit' },
					{ data: 'posisi', name: 'posisi' },
					{ data: 'witel', name: 'witel' },
					{ data: 'witel', name: 'witel', render: render_list_item },
				],
			});

			table_user_alker.ajax.reload(null, false);

			$('#input-witel').select2({
				placeholder: "Pilih Nama Witel",
				allowClear: true,
				ajax: {
					url: "/jx_dng/select_alker/witel",
					dataType: 'json',
					delay: 250,
					data: function (params){
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response){
						return {
							results: response
						};
					},
					cache: true,
				}
			});

			$('#input-unit').select2({
				placeholder: "Pilih Nama Unit",
				allowClear: true,
				multiple: true,
				ajax: {
					url: "/jx_dng/select_alker/unit",
					dataType: 'json',
					delay: 250,
					data: function (params){
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response){
						return {
							results: response
						};
					},
					cache: true,
				}
			});

			$('#input-posisi').select2({
				placeholder: "Pilih Nama Posisi",
				allowClear: true,
				ajax: {
					url: "/jx_dng/select_alker/posisi",
					dataType: 'json',
					delay: 250,
					data: function (params){
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response){
						return {
							results: response
						};
					},
					cache: true,
				}
			});

			$('#input-umur').select2({
				placeholder: "Pilih Umur",
				allowClear: true,
			});

			$('#input-umur').val(null).trigger('change');

			$('.select_opsi').on('change', function(){
				table_user_alker.ajax.reload(null, false);
			});
		});
  </script>
@endsection