@include('partial.confirm')
@extends('layout')

@section('html-title')
	 HR :: TOMMAN
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Project :
@endsection

@section('page-title')
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2.css">
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="/lib/xenon/js/datatables/dataTables.bootstrap.css">
	<script src="/lib/xenon/js/jquery-1.11.1.min.js"></script>

	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
		<li><i class="linecons-calendar"></i>HR</li>
		<li class="active"><i class="linecons-tag">Curriculum Vitae</i></li>
	</ol>
@endsection

@section('body')
	<div class="panel panel-default panel-noheading">
		<div class="panel-body">
			<form method="post" class="form-horizontal">
			<div class="input-group">
				<div class="col-sm-1">
					<label class="control-label" for="input-status">Karyawan</label>
				</div>
				<div class="col-sm-4">
					<input name="nama" type="hidden" id="input-an" />
				</div>

		      <span class="input-group-btn">
		      	<button class="btn btn-blue btn-icon btn-icon-standalone btn-icon-standalone-right" type="submit">
					<i class="fa-search"></i>
					<span>Search</span>
				</button>
		      </span>
		    </div>
			</form>
		</div>
	</div>

	<div class="panel panel-default panela">
		<ul class="nav nav-tabs nav-tabs-justified">
			<li class="active">
				<a href="#naker" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">Data Naker</span>
				</a>
			</li>
			<li>
				<a href="#positionname" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">Position Name</span>
				</a>
			</li>
			<li>
				<a href="#reward" data-toggle="tab">
					<span class="visible-xs"><i class="fa-user"></i></span>
					<span class="hidden-xs">Reward</span>
				</a>
			</li>
			<li>
				<a href="#history" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">History Jobdesk</span>
				</a>
			</li>
			<li>
				<a href="#konseling" data-toggle="tab">
					<span class="visible-xs"><i class="fa-envelope-o"></i></span>
					<span class="hidden-xs">Konseling & SP</span>
				</a>
			</li>
			<li>
				<a href="#kecelakaan" data-toggle="tab">
					<span class="visible-xs"><i class="fa-envelope-o"></i></span>
					<span class="hidden-xs">Kecelakaan Kerja</span>
				</a>
			</li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active" id="naker">
			<div>
				<div class="panel panel-color panel-blue panel-border">
					<div class="panel-heading">
						<div class="panel-title">DATA PRIBADI</div>
						<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon klik">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
						</div>
					</div>
					<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label" for="input-nik">NIK</label>
									<input name="nik" type="text" id="input-nik" class="form-control" value="{{ $data->nik or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="nama">Nama</label>
									<input name="nama" type="text" id="nama" class="form-control" value="{{ $data->nama or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="tgl-lahir">Tanggal Lahir</label>
									<input name="tgllahir" type="text" id="input-tgllahir" class="form-control datepicker" value="{{ $data->tgl_lahir or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="ppn">Kota Lahir</label>
									<input name="kota_lahir" type="text" id="kota-lahir" class="form-control" value="{{ $data->kota_lahir or ''}}" disabled/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label" for="alamat">Alamat</label>
									<input name="alamat" type="text" id="alamat" class="form-control" value="{{ $data->alamat or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="kota">Kota</label>
									<input name="kota" type="text" id="kota" class="form-control" value="{{ $data->kota or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="input-agama">Agama</label>
									<input name="agama" type="text" id="agama" class="form-control" value="{{ $data->agama or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="jk">Jenis Kelamin</label>
									<input name="jk" type="text" id="jk" class="form-control" value="{{ $data->jk or ''}}" disabled/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label" for="suku">Suku</label>
									<input name="suku" type="text" id="suku" class="form-control" value="{{ $data->suku or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="input-status">Status</label>
									<input name="status_nikah" type="text" id="status_nikah" class="form-control" value="{{ $data->status_nikah or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="maiden">Ibu Kandung</label>
									<input name="maiden" type="text" id="maiden" class="form-control" value="{{ $data->maiden or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="input-goldar">Golongan Darah</label>
									<input name="goldar" type="text" id="goldar" class="form-control" value="{{ $data->goldar or ''}}" disabled/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label" for="noktp">Nomor KTP</label>
									<input name="noktp" type="text" id="input-noktp" class="form-control" value="{{ $data->ktp or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="npwp">NPWP</label>
									<input name="npwp" type="text" id="npwp" class="form-control" value="{{ $data->npwp or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="input-telpon">Telpon</label>
									<input name="telpon" type="text" id="input-telpon" class="form-control" value="{{ $data->telpon or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="email">Email</label>
									<input name="email" type="text" id="email" class="form-control" value="{{ $data->email or ''}}" disabled/>
								</div>
							</div>
					</div>
				</div>
				<div class="panel panel-color panel-success panel-border">
					<div class="panel-heading">
						<div class="panel-title">STATUS KERJA</div>
						<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
						</div>
					</div>
					<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-4">
									<label class="control-label" for="input-tgl-mulai-kerja">Tanggal Mulai Kerja</label>
									<input name="tgl-mulai-kerja" type="text" id="input-tgl-mulai-kerja" class="form-control datepicker" value="{{ $data->tgl_mulai_kerja or ''}}" disabled/>
								</div>
								<div class="col-sm-4">
									<?php $statuskerja='';
									if(!empty($data) ){
										if($data->status=='1'){$statuskerja="OJT";}
										else if($data->status=='2'){$statuskerja="Kontrak I";}
										else if($data->status=='3'){$statuskerja="Kontrak II";}
										else if($data->status=='4'){$statuskerja="Pembaharuan";}
										else if($data->status=='5'){$statuskerja="Pegawai Tetap";}
										else if($data->status=='16'){$statuskerja="Resign";}
									}?>
									<label class="control-label" for="ojt">Status Kerja</label>
									<input name="statuskerja" type="text" id="statuskerja" class="form-control datepicker" value="{{ $statuskerja or ''}}" disabled/>
								</div>
								<div class="col-sm-4">
									<?php $tanggalstatus='';
									if(!empty($data) ){
										if($data->status=='1'){$tanggalstatus=$data->ojt;}
										else if($data->status=='2'){$tanggalstatus=$data->kontrak1;}
										else if($data->status=='3'){$tanggalstatus=$data->kontrak2;}
										else if($data->status=='4'){$tanggalstatus=$data->pembaharuan;}
										else if($data->status=='5'){$tanggalstatus=$data->pegawai_tetap;}
										else if($data->status=='6'){$tanggalstatus=$data->resign;}
									}
									?>
									<label class="control-label" for="kontrak1">Tanggal </label>
									<input name="kontrak1" type="text" id="kontrak1" class="form-control datepicker" value="{{ $tanggalstatus or ''}}" disabled/>
								</div>
							</div>
					</div>
				</div>
				<div class="panel panel-color panel-warning panel-border">
					<div class="panel-heading">
						<div class="panel-title">PENDIDIKAN</div>
						<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
						</div>
					</div>
					<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label" for="penyelenggara-pendidikan">Penyelenggara Pendidikan</label>
									<input name="penyelenggara-pendidikan" type="text" id="penyelenggara-pendidikan" class="form-control" value="{{ $data->penyelenggara_pendidikan or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="lvl-pendidikan">Level Pendidikan</label>
									<input name="lvl-pendidikan" type="text" id="lvl-pendidikan" class="form-control" value="{{ $data->lvl_pendidikan or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="tgl-lulus">Tanggal Lulus</label>
									<input name="tgl-lulus" type="text" id="tgl-lulus" class="form-control datepicker" value="{{ $data->tgl_lulus or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="jurusan">Jurusan</label>
									<input name="jurusan" type="text" id="jurusan" class="form-control" value="{{ $data->jurusan or ''}}" disabled/>
								</div>
							</div>
					</div>
				</div>
				<div class="panel panel-color panel-info panel-border">
					<div class="panel-heading">
						<div class="panel-title">DATA KELUARGA</div>
						<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
						</div>
					</div>
					<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-4">
									<label class="control-label" for="no-kk">Nomor Kartu Keluarga</label>
									<input name="no-kk" type="text" id="no-kk" class="form-control" value="{{ $data->nokk or ''}}" disabled/>
								</div>
								<div class="col-sm-4">
									<label class="control-label" for="status-keluarga">Status Dalam Keluarga</label>
									<input name="status-keluarga" type="text" id="status-keluarga" class="form-control" value="{{ $data->status_dalam_keluarga or ''}}" disabled/>
								</div>
								<div class="col-sm-4">
									<label class="control-label" for="namapasangan">Nama Pasangan</label>
									<input name="namapasangan" type="text" id="namapasangan" class="form-control" value="{{ $data->nama_pasangan or ''}}" disabled/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<label class="control-label" for="anak1">Nama Anak I</label>
									<input name="anak1" type="text" id="anak1" class="form-control" value="{{ $data->nama_anak1 or ''}}" disabled/>
								</div>
								<div class="col-sm-4">
									<label class="control-label" for="anak2">Nama Anak II</label>
									<input name="anak2" type="text" id="anak2" class="form-control" value="{{ $data->nama_anak2 or ''}}" disabled/>
								</div>
								<div class="col-sm-4">
									<label class="control-label" for="anak3">Nama Anak III</label>
									<input name="anak3" type="text" id="anak3" class="form-control" value="{{ $data->nama_anak3 or ''}}" disabled/>
								</div>
							</div>
					</div>
				</div>
				<div class="panel panel-color panel-purple panel-border">
					<div class="panel-heading">
						<div class="panel-title">REKENING</div>
						<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
						</div>
					</div>
					<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-4">
									<label class="control-label" for="norek">Nomor Rekening</label>
									<input name="norek" type="text" id="norek" class="form-control" value="{{ $data->no_rekening or ''}}" disabled/>
								</div>
								<div class="col-sm-4">
									<label class="control-label" for="namarek">Nama Di Rekening</label>
									<input name="namarek" type="text" id="namarek" class="form-control" value="{{ $data->nama_rekening or ''}}" disabled/>
								</div>
								<div class="col-sm-4">
									<label class="control-label" for="bank">Bank</label>
									<input name="bank" type="text" id="bank" class="form-control" value="{{ $data->bank or ''}}" disabled/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<label class="control-label" for="kantor-cabang">Kantor Cabang</label>
									<input name="kantor-cabang" type="text" id="kantor-cabang" class="form-control" value="{{ $data->kantor_cabang_bank or ''}}" disabled/>
								</div>
								<div class="col-sm-8">
									<label class="control-label" for="alamat-bank">Alamat Bank</label>
									<input name="alamat-bank" type="text" id="alamat-bank" class="form-control" value="{{ $data->alamat_bank or ''}}" disabled/>
								</div>
							</div>
					</div>
				</div>
				<div class="panel panel-color panel-success panel-border">
					<div class="panel-heading">
						<div class="panel-title">JAMSOSTEK & BPJS</div>
						<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
						</div>
					</div>
					<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label" for="no-jamsostek">Nomor BPJS Ketenaga Kerjaan / Jamsostek</label>
									<input name="no-jamsostek" type="text" id="no-jamsostek" class="form-control" value="{{ $data->jamsostek or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="tgl-jamsostek">Tanggal BPJS Ketenaga Kerjaan / Jamsostek</label>
									<input name="tgl-jamsostek" type="text" id="tgl-jamsostek" class="form-control datepicker" value="{{ $data->tgl_jamsostek or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="no-bpjs">Nomor BPJS</label>
									<input name="no-bpjs" type="text" id="no-bpjs" class="form-control" value="{{ $data->bpjs or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="tgl-bpjs">Tanggal BPJS</label>
									<input name="tgl-bpjs" type="text" id="tgl-bpjs" class="form-control datepicker" value="{{ $data->tgl_bpjs or ''}}" disabled/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label" for="no-bpjs-istri">Nomor BPJS Istri</label>
									<input name="no-bpjs-istri" type="text" id="no-bpjs-istri" class="form-control" value="{{ $data->bpjs_istri or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="tgl-bpjs-istri">Tanggal BPJS Istri</label>
									<input name="tgl-bpjs-istri" type="text" id="tgl-bpjs-istri" class="form-control datepicker" value="{{ $data->tgl_bpjs_istri or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="no-bpjs-anak1">Nomor BPJS Anak I</label>
									<input name="no-bpjs-anak1" type="text" id="no-bpjs-anak1" class="form-control" value="{{ $data->bpjs_anak1 or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="tgl-bpjs-anak1">Tanggal BPJS Anak I</label>
									<input name="tgl-bpjs-anak1" type="text" id="tgl-bpjs-anak1" class="form-control datepicker" value="{{ $data->tgl_bpjs_anak1 or ''}}" disabled/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label" for="no-bpjs-anak2">Nomor BPJS Anak II</label>
									<input name="no-bpjs-anak2" type="text" id="no-bpjs-anak2" class="form-control" value="{{ $data->bpjs_anak2 or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="tgl-bpjs-anak2">Tanggal BPJS Anak II</label>
									<input name="tgl-bpjs-anak2" type="text" id="tgl-bpjs-anak2" class="form-control datepicker" value="{{ $data->tgl_bpjs_anak2 or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="no-bpjs-anak3">Nomor BPJS Anak III</label>
									<input name="no-bpjs-anak3" type="text" id="no-bpjs-anak3" class="form-control " value="{{ $data->bpjs_anak3 or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="tgl-bpjs-anak3">Tanggal BPJS Anak III</label>
									<input name="tgl-bpjs-anak3" type="text" id="tgl-bpjs-anak3" class="form-control datepicker" value="{{ $data->tgl_bpjs_anak3 or ''}}" disabled/>
								</div>
							</div>
					</div>
				</div>
				<div class="panel panel-color panel-danger panel-border">
					<div class="panel-heading">
						<div class="panel-title">LAIN-LAIN</div>
						<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
						</div>
					</div>
					<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label" for="input-telkomedika">Nomor Telkomedika</label>
									<input name="telkomedika" type="text" id="input-telkomedika" class="form-control" value="{{ $data->telkomedika or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="nikatasan">NIK Atasan</label>
									<input name="nikatasan" type="text" id="nikatasan" class="form-control" value="{{ $data->nik_atasan or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="gadas">Gadas</label>
									<input name="gadas" type="text" id="gadas" class="form-control" value="{{ $data->gadas or ''}}" disabled/>
								</div>
								<div class="col-sm-3">
									<label class="control-label" for="pelatihan">Pelatihan Ala</label>
									<input name="pelatihan" type="text" id="pelatihan" class="form-control" value="{{ $data->pelatihan_ala or ''}}" disabled/>
								</div>
							</div>
					</div>
				</div>
			</div>
			</div>
			<div class="tab-pane" id="positionname">
					<div class="panel panel-color panel-success panel-border">
						<div class="panel-heading">
							<div class="panel-title">Position Name</div>
						</div>
						<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-3">
										<label class="control-label" for="input-objectid">Object ID</label>
										<input name="objectid" type="text" id="objectid" class="form-control"  value="{{ $job->object_id or ''}}" disabled/>
									</div>
									<div class="col-sm-3">
										<label class="control-label" for="position">Position Name</label>
										<input name="position" type="text" id="position" class="form-control" value="{{ $job->position_name or ''}}" disabled/>
									</div>
									<div class="col-sm-3">
										<label class="control-label" for="witel">Witel</label>
										<input name="witel" type="text" id="witel" class="form-control" value="{{ $job->witel or ''}}" disabled/>
									</div>
									<div class="col-sm-3">
										<label class="control-label" for="input-psa">PSA</label>
										<input name="psa" type="text" id="psa" class="form-control "  value="{{ $job->loker or ''}}" disabled/>
									</div>
									<div class="col-sm-3">
										<label class="control-label" for="input-regional">Regional</label>
										<input name="regional" type="text" id="regional" class="form-control" value="{{ $job->regional or ''}}" disabled/>
									</div>
									<div class="col-sm-3">
										<label class="control-label" for="bizpart">Bizpart ID</label>
										<input name="bizpart" type="text" id="bizpart" class="form-control" value="{{ $job->bizpart_id or ''}}" disabled/>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-3">
										<label class="control-label" for="input-direktorat">Direktorat</label>
										<input name="direktorat" type="text" id="direktorat" class="form-control" value="{{ $job->direktorat or ''}}" disabled/>
									</div>
									<div class="col-sm-3">
										<label class="control-label" for="unit">Unit</label>
										<input name="unit" type="text" id="unit" class="form-control" value="{{ $job->unit or ''}}" disabled/>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-3">
										<label class="control-label" for="input-subunit">Sub Unit</label>
										<input name="subunit" type="text" id="subunit" class="form-control" value="{{ $job->sub_unit or ''}}" disabled/>
									</div>
									<div class="col-sm-3">
										<label class="control-label" for="group">Group</label>
										<input name="group" type="text" id="group" class="form-control" value="{{ $job->group or ''}}" disabled/>
									</div>
									<div class="col-sm-3">
										<label class="control-label" for="input-subgroup">Sub Group</label>
										<input name="subgroup" type="text" id="subgroup" class="form-control" value="{{ $job->sub_group or ''}}" disabled/>
									</div>
									<div class="col-sm-3">
										<label class="control-label" for="groupfungsi">Group Fungsi</label>
										<input name="groupfungsi" type="text" id="groupfungsi" class="form-control" value="{{ $job->group_fungsi or ''}}" disabled/>
									</div>
								</div>
						</div>
					</div>
			</div>
			<div class="tab-pane " id="reward">
				<div>
					<table class="table table-bordered table-striped table-small-font" id="example-2">
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Reward / Inovasi</th>
								<th>Dokumen</th>
							</tr>
						</thead>
						<tbody class="middle-align">
							<?php $no=1; ?>
							@foreach($reward as $r)
								<?php
									$path = public_path()."/upload/Reward/".$r->id;
				        			$file = array_diff(scandir($path), array('.', '..') );
								?>
								<tr>
									<td >{{ $no }}</td>
									<td >{{ $r->tgl }}</td>
									<td >{{ $r->reward }}</td>
									<td ><a href="/upload/Reward/{{$r->id}}/{{$file[2]}}">{{$file[2]}}</a></td>
								</tr>
								<?php $no++; ?>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="tab-pane" id="history">
				<div class="panel panel-default">
				<table class="table table-bordered table-striped table-small-font" id="example-3">
					<thead>
						<tr>
							<th>Nomor</th>
							<th>Tanggal</th>
							<th>Jenis</th>
							<th>Object ID</th>
							<th>Position Name </th>
							<th>Witel</th>
							<th>PSA</th>
						</tr>
					</thead>
					<tbody class="middle-align">
					<?php $no=1; ?>
					@foreach($position as $r)
						<tr>
							<td>{{$no}}</td>
							<td>{{$r->tgl}}</td>
							<td>{{$r->jenis}}</td>
							<td>{{$r->object_id}}</td>
							<td>{{$r->position_name}}</td>
							<td>{{$r->witel}}</td>
							<td>{{$r->psa}}</td>
						</tr>
						<?php $no++;?>
					@endforeach
					</tbody>
				</table>
				</div>
			</div>
			<div class="tab-pane" id="konseling">
				<table class="table table-bordered table-striped table-small-font " id="example-2">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal</th>
							<th>Keterangan Konseling</th>
							<th>Tindak Lanjut</th>
							<th>SP</th>
							<th>Keterangan SP</th>
							<th>Dokumen</th>
						</tr>
					</thead>
					<tbody class="middle-align">
						<?php $no=1; ?>
						@foreach($konseling as $r)
							<?php
							$path = public_path()."/upload/Konseling/".$r->id;
		        			$file = array_diff(scandir($path), array('.', '..') );

							$sp='';
							if ($r->sp==0)  {$sp="Tidak SP";}
							else if ($r->sp==1)  {$sp="SP 1";}
							else if ($r->sp==2)  {$sp="SP 2";}
							else if ($r->sp==3)  {$sp="SP 3";}
							?>
							<tr>
								<td >{{ $no }}</td>
								<td >{{ $r->tgl }}</td>
								<td >{{ $r->pelanggaran }}</td>
								<td >{{ $r->tindak_lanjut }}</td>
								<td >{{ $sp }}</td>
								<td >{{ $r->ket_sp }}</td>
								<td ><a href="/upload/Konseling/{{$r->id}}/{{$file[2]}}">{{$file[2]}}</a></td>
							</tr>
							<?php $no++; ?>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="tab-pane " id="kecelakaan">
				<table class="table table-bordered table-striped table-small-font tabel" id="example-3">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal</th>
							<th>Kronologi Kejadian</th>
							<th>Detil Cedera</th>
							<th>Alamat Kejadian</th>
							<th>Tindakan</th>
						</tr>
					</thead>
					<tbody class="middle-align">
						<?php $no=1; ?>
						@foreach($accident as $r)
						<tr>
							<td >{{$no}}</td>
							<td >{{$r->tgl}}</td>
							<td >{{$r->kronologi_kejadian}}</td>
							<td >{{$r->detail_cedera}}</td>
							<td >{{$r->alamat}}</td>
							<td >{{$r->tindakan}}</td>
						</tr>
						<?php $no++; ?>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</div>


@endsection

@section('plugins')
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/select2/select2.min.js"></script>
	<script src="/lib/xenon/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="/lib/xenon/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/lib/xenon/js/datatables/dataTables.bootstrap.js"></script>
	<script src="/lib/xenon/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="/lib/xenon/js/datatables/tabletools/dataTables.tableTools.min.js"></script>

@endsection

@section('footer')
	<script>
	$("#example-3").dataTable();
	var data = <?= json_encode($karyawan) ?>;
			$('#input-an').select2({
				data: data,
				placeholder: 'Input Karyawan',
				allowClear: true,
				minimumInputLength: 1,
				formatSelection: function(data) { return data.text },
				formatResult: function(data) {
					return  '<span class="label label-info">'+data.id+'</span>'+
								'<span class="badge badge-purple pull-right">'+data.nik+'</span>'+
								'<strong style="margin-left:5px">'+data.text+'</strong>';

				}
			});

	$("#example-2").dataTable();

  </script>
@endsection