@extends('layout')
@section('title')
Form Pengajuan NIK
@endsection
@section('css')
<style>
	/* */

	.panel-default>.panel-heading {
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-color>.panel-heading a:after {
  content: "";
  position: relative;
  top: 1px;
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
	float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-color>.panel-heading a[aria-expanded="false"]:after {
  content: "\f067";
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.panel-color>.panel-heading a[aria-expanded="true"]:after {
  content: "\f068";
  -webkit-transform: rotate(0deg);
  transform: rotate(0deg);
}
</style>
@endsection
@section('content')
<div class="px-content">
  <div class="page-header">
		<form method="post" class="form-horizontal validate">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="jenis_stts">Jenis :</label>
				<div class="col-md-10">
					<select name="jenis_stts" class="form-control" id="jenis_stts">
						<option value="Mitra">Mitra</option>
						<option value="Telkom Akses">Telkom Akses</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="jenis_nik">Naker :</label>
				<div class="col-md-10">
					<select name="jenis_nik" class="form-control" id="jenis_nik">
						<option value="Ada">Sudah Ada</option>
						<option value="Request">Request</option>
					</select>
				</div>
			</div>
			<div class="form-group detail_data">
				{{-- GOLONGAN DARAH (YG MITRA AJA HAPUS, TA JANGAN
				NAMA PASANGAN HAPUS (KHUSUS MITRA DIHAPUS, TA JANGAN)
				NAMA ANAK 1 SAMPAI 3 (KHUSUS MITRA DIHAPUS, TA JANGAN)
				HAPUS
				PASANGAN DAN ANAK DIHAPUS DI MIRA ( TA JANGAN)
				MITRA DITAMPILKAN ( TA DIHAPUS) --}}
				<div class="col-md-12">
					<div class="panel-group panel-group-success panel-group-dark" id="accordion_self_data">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_self_data" href="#self_data" aria-expanded="true">Data Pribadi</a>
							</div>
							<div id="self_data" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-3">
											<label class="control-label title_nik" for="nik">NIK Resmi</label>
											<input name="nik" type="text" id="nik" class="form-control" value/>
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="nama">Nama</label>
											<input name="nama" type="text" id="nama" class="form-control" value/>
										</div>
										<div class="col-sm-2">
											<label class="control-label" for="jk">Jenis Kelamin</label>
											<select class="form-control" id="jk" name="jk">
												<option value="Laki-Laki">Laki-Laki</option>
												<option value="Perempuan">Perempuan</option>
											</select>
										</div>
										<div class="col-sm-2 hide_mitra">
											<label class="control-label" for="goldar">Golongan Darah</label>
											<select class="form-control" id="goldar" name="goldar">
												<option value="O">O</option>
												<option value="A">A</option>
												<option value="B">B</option>
												<option value="AB">AB</option>
											</select>
										</div>
										<div class="col-sm-2">
											<label class="control-label" for="tgllahir">Tanggal Lahir</label>
											<input name="tgllahir" type="text" id="tgllahir" class="form-control tgl" value/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="kota_lahir">Tempat Lahir</label>
											<input name="kota_lahir" type="text" id="kota_lahir" class="form-control" value/>
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="agama">Agama</label>
											<select class="form-control" id="agama" name="agama">
												<option value="Islam">Islam</option>
												<option value="Kristen">Kristen</option>
												<option value="Khatolik">Khatolik</option>
												<option value="Buddha">Buddha</option>
												<option value="Konghucu">Konghucu</option>
											</select>
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="status">Status</label>
											<select name="status" id="status" class="form-control">
											<option value="Belum Menikah">Belum Menikah</option>
											<option value="Sudah Menikah">Sudah Menikah</option>
											<option value="Bercerai">Bercerai</option>
											</select>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-8">
											<label class="control-label" for="alamat">Alamat</label>
											<textarea name="alamat" style="resize: none" rows="4" type="text" id="alamat" class="form-control"></textarea>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="kota">Kota</label>
											<input name="kota" type="text" id="kota" class="form-control" value/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="noktp">Nomor KTP</label>
											<input name="noktp" type="text" id="noktp" class="form-control number" value/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="telpon">Telepon</label>
											<input name="telpon" type="text" id="telpon" class="form-control number" value/>
										</div>
										<div class="col-sm-12">
											<label class="control-label" for="email">Email</label>
											<input name="email" type="text" id="email" class="form-control" value/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="maiden">Nama Ibu Kandung</label>
											<input name="maiden" type="text" id="maiden" class="form-control" value/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="telpon_2">Telepon Yang Bisa Dihubungi</label>
											<input name="telpon_2" type="text" id="telpon_2" class="form-control number" value/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="telpon_2_nm">Nama Telepon Yang Bisa Dihubungi</label>
											<input name="telpon_2_nm" type="text" id="telpon_2_nm" class="form-control" value/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-12">
											<label class="control-label" for="npwp">NPWP</label>
											<input name="npwp" type="text" id="npwp" data-inputmask="'mask': '9{2}.9{3}.9{3}.9{1}-9{3}.9{3}'" class="form-control" value/>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel-group panel-group-warning panel-group-dark" id="accordion_status_kerja">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_status_kerja" href="#status_kerja" aria-expanded="true">
									STATUS KERJA
								</a>
							</div>
							<div id="status_kerja" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-12">
											<label class="control-label" for="waspang_nik">NIK Atasan</label>
											<input name="waspang_nik" type="text" id="waspang_nik" class="form-control"/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="tanggal_aju">Tanggal Pengajuan</label>
											<input name="tanggal_aju" type="text" id="tanggal_aju" class="form-control tgl"/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="tglmulaikerja">Tanggal Mulai Kerja</label>
											<input name="tglmulaikerja" type="text" id="tglmulaikerja" class="form-control tgl"/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="status_kerja">Status Kerja</label>
											<select name="status_kerja" id="status_kerja" class="form-control">
											<option value="KEMITRAAN">KEMITRAAN</option>
											<option value="TELKOM AKSES">TELKOM AKSES</option>
											<option value="TELKOM">TELKOM</option>
											</select>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="direktorat">Direktorat</label>
											<select name="direktorat" id="direktorat" class="form-control"/></select>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="psa">PSA</label>
											<input name="psa" type="text" id="psa" class="form-control" readonly/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="witel">Witel</label>
											<select name="witel" id="witel" class="form-control"/></select>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="region">Regional</label>
											<input name="region" type="text" id="region" class="form-control" value="REGIONAL 6" readonly/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="tglawalkerja">Tanggal Awal</label>
											<input name="tglawalkerja" type="text" id="tglawalkerja" class="form-control tgl">
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="tglakhirkerja">Tanggal Akhir</label>
											<input name="tglakhirkerja" type="text" id="tglakhirkerja" class="form-control tgl" value="{{ $data->tgl_mulai_kerja or ''}}"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel-group panel-group-info panel-group-dark" id="accordion_pendidikan">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_pendidikan" href="#pendidikan" aria-expanded="true">
									PENDIDIKAN
								</a>
							</div>
							<div id="pendidikan" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="last_study">Pendidikan Terakhir</label>
											<input name="last_study" type="text" id="last_study" class="form-control"/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="lvl_pendidikan">Level Pendidikan</label>
											<input name="lvl_pendidikan" type="text" id="lvl_pendidikan" class="form-control"/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="tgllulus">Tanggal Lulus</label>
											<input name="tgllulus" type="text" id="tgllulus" class="form-control tgl"/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="jurusan">Jurusan</label>
											<input name="jurusan" type="text" id="jurusan" class="form-control"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel-group panel-group-info panel-group-dark" id="accordion_keluarga">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_keluarga" href="#keluarga" aria-expanded="true">
									DATA KELUARGA
								</a>
							</div>
							<div id="keluarga" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="nokk">Nomor Kartu Keluarga</label>
											<input name="nokk" type="text" id="nokk" class="form-control number"/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="status_keluarga">Status Dalam Keluarga</label>
											<input name="status_keluarga" type="text" id="status_keluarga" class="form-control"/>
										</div>
									</div>
									<hr class="page-wide-block hide_mitra">
									<div class="form-group hide_mitra">
										<div class="col-sm-12">
											<label class="control-label" for="namapasangan">Nama Pasangan</label>
											<input name="namapasangan" type="text" id="namapasangan" class="form-control"/>
										</div>
									</div>
									<hr class="page-wide-block hide_mitra">
									<div class="form-group hide_mitra">
										<div class="col-sm-12">
											<label class="control-label" for="anak1">Nama Anak I</label>
											<input name="anak1" type="text" id="anak1" class="form-control"/>
										</div>
										<div class="col-sm-12">
											<label class="control-label" for="anak2">Nama Anak II</label>
											<input name="anak2" type="text" id="anak2" class="form-control"/>
										</div>
										<div class="col-sm-12">
											<label class="control-label" for="anak3">Nama Anak III</label>
											<input name="anak3" type="text" id="anak3" class="form-control"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel-group panel-group-warning panel-group-dark" id="accordion_rekening">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_rekening" href="#rekening" aria-expanded="true">
									REKENING
								</a>
							</div>
							<div id="rekening" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="norek">Nomor Rekening</label>
											<input name="norek" type="text" id="norek" class="form-control number" value="{{ $data->no_rekening or ''}}"/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="bank">Bank</label>
											<input name="bank" type="text" id="bank" class="form-control" value="{{ $data->bank or ''}}"/>
										</div>
										<div class="col-sm-4">
											<label class="control-label" for="kantor_cabang">Kantor Cabang</label>
											<input name="kantor_cabang" type="text" id="kantor_cabang" class="form-control" value="{{ $data->kantor_cabang_bank or ''}}"/>
										</div>
									</div>
									<hr class="page-wide-block">
									<div class="form-group">
										<div class="col-sm-12">
											<label class="control-label" for="namarek">Nama Di Rekening</label>
											<input name="namarek" type="text" id="namarek" class="form-control" value="{{ $data->nama_rekening or ''}}"/>
										</div>
										<div class="col-sm-12">
											<label class="control-label" for="link_aj">Nomor Link Aja</label>
											<input name="link_aj" type="text" id="link_aj" class="form-control number" value="{{ $data->bank or ''}}"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel-group panel-group-success panel-group-dark" id="accordion_jamsos">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_jamsos" href="#jamsos" aria-expanded="true">
									JAMSOSTEK & BPJS
								</a>
							</div>
							<div id="jamsos" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-3">
											<label class="control-label" for="jamsostek">Nomor BPJS Ketenaga Kerjaan / Jamsostek</label>
											<input name="jamsostek" type="text" id="jamsostek" class="form-control" />
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="tgl-jamsostek">Tanggal BPJS Ketenaga Kerjaan / Jamsostek</label>
											<input name="tgl_jamsostek" type="text" id="tgl-jamsostek" class="form-control tgl" />
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="bpjs">Nomor BPJS</label>
											<input name="bpjs" type="text" id="bpjs" class="form-control number" />
										</div>
										<div class="col-sm-3">
											<label class="control-label" for="tgl-bpjs">Tanggal BPJS</label>
											<input name="tgl_bpjs" type="text" id="tgl-bpjs" class="form-control tgl" />
										</div>
									</div>
									<hr class="page-wide-block hide_mitra">
									<div class="form-group hide_mitra">
										<div class="col-sm-12">
											<label class="control-label" for="no-bpjs-istri">Nomor BPJS Istri</label>
											<input name="bpjs_istri" type="text" id="no-bpjs-istri" class="form-control number" />
										</div>
										<div class="col-sm-12">
											<label class="control-label" for="tgl-bpjs-istri">Tanggal BPJS Istri</label>
											<input name="tgl_bpjs_istri" type="text" id="tgl-bpjs-istri" class="form-control tgl" />
										</div>
									</div>
									<hr class="page-wide-block hide_mitra">
									<div class="form-group hide_mitra">
										<div class="col-sm-6">
											<label class="control-label" for="no-bpjs-anak1">Nomor BPJS Anak I</label>
											<input name="bpjs_anak1" type="text" id="no-bpjs-anak1" class="form-control" />
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="tgl-bpjs-anak1">Tanggal BPJS Anak I</label>
											<input name="tgl_bpjs_anak1" type="text" id="tgl-bpjs-anak1" class="form-control tgl" />
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="no-bpjs-anak2">Nomor BPJS Anak II</label>
											<input name="bpjs_anak2" type="text" id="no-bpjs-anak2" class="form-control" />
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="tgl-bpjs-anak2">Tanggal BPJS Anak II</label>
											<input name="tgl_bpjs_anak2" type="text" id="tgl-bpjs-anak2" class="form-control tgl" />
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="no-bpjs-anak3">Nomor BPJS Anak III</label>
											<input name="bpjs_anak3" type="text" id="no-bpjs-anak3" class="form-control " />
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="tgl-bpjs-anak3">Tanggal BPJS Anak III</label>
											<input name="tgl_bpjs_anak3" type="text" id="tgl-bpjs-anak3" class="form-control tgl" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel-group panel-group-danger panel-group-dark" id="accordion_etc">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_etc" href="#etc" aria-expanded="true">
									LAIN - LAIN
								</a>
							</div>
							<div id="etc" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="gadas">Gadas</label>
											<input name="gadas" type="text" id="gadas" class="form-control" value="{{ $data->gadas or ''}}"/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="pelatihan">Pelatihan Ala</label>
											<input name="pelatihan" type="text" id="pelatihan" class="form-control" value="{{ $data->pelatihan_ala or ''}}"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel-group panel-group-danger panel-group-dark" id="accordion_apd">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_apd" href="#apd" aria-expanded="true">
									APD
								</a>
							</div>
							<div id="apd" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="seragam">Ukuran Baju</label>
											<select class="form-control" id="seragam" name="seragam">
												<option value="S">S</option>
												<option value="M">M</option>
												<option value="L">L</option>
												<option value="XL">XL</option>
												<option value="XXL">XXL</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<label class="control-label" for="alker">Alker</label>
											<select class="form-control" id="alker" name="alker" multiple></select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel-group panel-group-info panel-group-dark" id="accordion_dokumen">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_dokumen" href="#dokumen" aria-expanded="true">
									DOKUMEN
								</a>
							</div>
							<div id="dokumen" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="file_ktp">KTP</label>
											<input name="file_ktp" type="file" id="file_ktp" class="form-control" value="{{ $data->file_ktp or ''}}"/>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="file_pakta_integritas">Pakta Integritas (MYI-Tech)</label>
											<input name="file_pakta_integritas" type="file" id="file_pakta_integritas" class="form-control" value="{{ $data->file_pakta_integritas or ''}}"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 hide_ta">
					<div class="panel-group panel-group-default panel-group-dark" id="accordion_mitra">
						<div class="panel panel-color panel-blue">
							<div class="panel-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_mitra" href="#mitra" aria-expanded="true">
									DATA MITRA
								</a>
							</div>
							<div id="mitra" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-6">
											<label class="control-label" for="mitra_name">Nama Mitra</label>
											<select class="form-control" id="mitra_name" name="mitra_name"></select>
										</div>
										<div class="col-sm-6">
											<label class="control-label" for="posisi">Posisi</label>
											<input name="posisi" type="text" id="posisi" class="form-control" value="{{ $data->penerimaan_seragam or ''}}"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn"><span class="fa fa-cloud-download"></span> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
	<script>
		$( function(){
			$(":input").inputmask();

			$('.number').on('keyup', function(e){
				if (e.which != 8 && e.which != 0 && e.which < 48 || e.which > 57) {
					$(this).val(function (index, value) {
						return value.replace(/\D/g, "");
					});
				}
			});

			$('select').select2();

			$('#jenis_nik').on('select2:select', function(){
				if($(this).val() == 'Ada'){
					$('.title_nik').text('NIK Resmi');
				}else{
					$('.title_nik').text('NIK Sementara');
				}
			});

			$('.tgl').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',
        daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',

        beforeShowMonth: function(date) {
          if (date.getMonth() === 8) {
            return false;
          }
        },

        beforeShowYear: function(date){
          if (date.getFullYear() === 2014) {
            return false;
          }
        }
      });

			$('#jenis_stts').select2({
				width: '100%',
				placeholder: 'Pilih Jenis Karyawan',
				allowClear: true
			})

			$('#jenis_stts').val(null).trigger('change');

			var raw_class= ['.hide_mitra', '.hide_ta'];

			$('#jenis_stts').on('select2:select', function(){
				if($(this).val().indexOf('TELKOM') !== -1 ){
					var value = '.hide_mitra';
					kelas = raw_class.filter(function(item) {
							return item !== value
					});

					$('.hide_mitra').css({
						'display': 'inline'
					});


					$(kelas.join(',') ).css({'display': 'none'});
				}else{
					var value = '.hide_ta';

					$('.hide_ta').css({
						'display': 'inline'
					});

					kelas = raw_class.filter(function(item) {
							return item !== value
					});
					$(kelas.join(',') ).css({'display': 'none'});
				}
			});

			$('#direktorat').select2({
				width: '100%',
				placeholder: "Ketik Nama Direktorat",
				allowClear: true,
				ajax: {
					url: '/jx_dng/get_dir',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				language: {
					noResults: function(){
						return "Direktorat Tidak Ditemukan? <a type='button' href='/hr/list/direktorat' target='_blank' class='btn btn-sm btn-warning'><span class='ion-plus-circled'></span> Buka Halaman Direktorat</a>";
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('#witel').select2({
				width: '100%',
				placeholder: "Ketik Nama Witel",
				allowClear: true,
				ajax: {
					url: '/jx_dng/get_witel',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('#witel').on('select2:select', function(){
				$.ajax({
					type: 'GET',
					url: '/jx_dng/get_psa',
					dataType: 'JSON',
					data: {
						data: $(this).val()
					}
				}).done( function(e){
					$('#psa').val(e.psa_witel)
				})
			})

			$('#mitra_name').select2({
				width: '100%',
				placeholder: "Ketik Nama Mitra",
				allowClear: true,
				ajax: {
					url: '/jx_dng/get_mitra',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});

			$('##alker').select2({
				width: '100%',
				placeholder: "Ketik Alker Yang Ada",
				allowClear: true,
				ajax: {
					url: '/jx_dng/get_mitra',
					dataType: 'json',
					type: "GET",
					delay: 50,
					data: function (term) {
						return {
							searchTerm: term.term
						};
					},
					processResults: function (data) {
						return {
							results: data
						};
					}
				},
				escapeMarkup: function (markup) {
					return markup;
				}
			});
		});
	</script>
@endsection