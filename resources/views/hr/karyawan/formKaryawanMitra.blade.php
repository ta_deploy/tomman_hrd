@extends('layout')

@section('html-title')
	 HR :: TOMMAN
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Project :
@endsection

@section('page-title')
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2.css">
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2-bootstrap.css">
	<style>
		#keperluan{
			height : 98px;
			resize : none;
		}
	</style>
	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
		<li><i class="linecons-calendar"></i>HR</li>
		<li class="active"><i class="linecons-tag">Insert Karyawan Mitra</i> </li>
	</ol>
@endsection

@section('body')
<div >
<form method="post" class="form-horizontal validate">
<input name="id" type="hidden"  value="{{ $data->id or ''}}" />
	<div class="col-md-12">
	<div class="panel panel-color panel-blue">
		<div class="panel-heading">
			<div class="panel-title">DATA PRIBADI</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-3">
						<label class="control-label" for="input-nik">NIK</label>
						<input name="nik" type="text" id="input-nik" class="form-control" value="{{ $data->nik or ''}}"/>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
						<div class="col-sm-12">
						<label class="control-label" for="nama">Nama</label>
						<input name="nama" type="text" id="nama" class="form-control" value="{{ $data->nama or ''}}" data-validate="required" data-message-required="Nama Harus Diisi"/>
						</div></div>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="tgl-lahir">Tanggal Lahir</label>
						<input name="tgllahir" type="text" id="input-tgllahir" class="form-control datepicker" value="{{ $data->tgl_lahir or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="ppn">Kota Lahir</label>
						<input name="kota_lahir" type="text" id="kota-lahir" class="form-control" value="{{ $data->kota_lahir or ''}}"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label class="control-label" for="alamat">Alamat</label>
						<input name="alamat" type="text" id="alamat" class="form-control" value="{{ $data->alamat or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="kota">Kota</label>
						<input name="kota" type="text" id="kota" class="form-control" value="{{ $data->kota or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="input-agama">Agama</label>
						<select name="agama" id="input-agama" class="form-control selectboxit">
							<option <?php if(!empty($data->agama) ){if ($data->agama=='Islam'){echo "selected";}} ?>>Islam</option>
							<option <?php if(!empty($data->agama) ){if ($data->agama=='Kristen'){echo "selected";}} ?>>Kristen</option>
							<option <?php if(!empty($data->agama) ){if ($data->agama=='Khatolik'){echo "selected";}} ?>>Khatolik</option>
							<option <?php if(!empty($data->agama) ){if ($data->agama=='Hindu'){echo "selected";}} ?>>Hindu</option>
							<option <?php if(!empty($data->agama) ){if ($data->agama=='Budha'){echo "selected";}} ?>>Budha</option>
						</select>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="jk">Jenis Kelamin</label>
						<select name="jk" id="input-jk" class="form-control selectboxit">
							<option <?php if(!empty($data->jk) ){if ($data->jk=='Laki-Laki'){echo "selected";}} ?>>Laki-Laki</option>
							<option <?php if(!empty($data->jk) ){if ($data->jk=='Perempuan'){echo "selected";}} ?>>Perempuan</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label class="control-label" for="suku">Suku</label>
						<input name="suku" type="text" id="suku" class="form-control" value="{{ $data->suku or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="input-status">Status</label>
						<select name="status" id="input-status" class="form-control selectboxit">
							<option <?php if(!empty($data->status_nikah) ){if ($data->status_nikah=='Belum Menikah'){echo "selected";}} ?>>Belum Menikah</option>
							<option <?php if(!empty($data->status_nikah) ){if ($data->status_nikah=='Menikah'){echo "selected";}} ?>>Menikah</option>
						</select>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="maiden">Ibu Kandung</label>
						<input name="maiden" type="text" id="maiden" class="form-control" value="{{ $data->maiden or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="input-goldar">Golongan Darah</label>
						<select name="goldar" id="input-goldar" class="form-control selectboxit">
							<option <?php if(empty($data->goldar) ){echo "selected";} ?>></option>
							<option <?php if(!empty($data->goldar) ){if ($data->goldar=='A'){echo "selected";}} ?>>A</option>
							<option <?php if(!empty($data->goldar) ){if ($data->goldar=='B'){echo "selected";}} ?>>B</option>
							<option <?php if(!empty($data->goldar) ){if ($data->goldar=='AB'){echo "selected";}} ?>>AB</option>
							<option <?php if(!empty($data->goldar) ){if ($data->goldar=='O'){echo "selected";}} ?>>O</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label class="control-label" for="noktp">Nomor KTP</label>
						<input name="noktp" type="text" id="input-noktp" class="form-control" value="{{ $data->ktp or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="npwp">NPWP</label>
						<input name="npwp" type="text" id="npwp" class="form-control" value="{{ $data->npwp or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="input-telpon">Telpon</label>
						<input name="telpon" type="text" id="input-telpon" class="form-control" value="{{ $data->telpon or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="email">Email</label>
						<input name="email" type="text" id="email" class="form-control" value="{{ $data->email or ''}}"/>
					</div>
				</div>
		</div>
	</div>
	</div>
	<div class="col-md-6">
	<div class="panel panel-color panel-warning">
		<div class="panel-heading">
			<div class="panel-title">PENDIDIKAN</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="penyelenggara-pendidikan">Penyelenggara Pendidikan</label>
						<input name="penyelenggara" type="text" id="penyelenggara" class="form-control" value="{{ $data->penyelenggara_pendidikan or ''}}"/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="lvl-pendidikan">Level Pendidikan</label>
						<select name="lvlpendidikan" id="input-lvlpendidikan" class="form-control selectboxit">
							<option <?php if(empty($data->lvl_pendidikan) ){echo "selected";} ?>></option>
							<option <?php if(!empty($data->lvl_pendidikan) ){if ($data->lvl_pendidikan=='S2'){echo "selected";}} ?>>S2</option>
							<option <?php if(!empty($data->lvl_pendidikan) ){if ($data->lvl_pendidikan=='S1'){echo "selected";}} ?>>S1</option>
							<option <?php if(!empty($data->lvl_pendidikan) ){if ($data->lvl_pendidikan=='D3'){echo "selected";}} ?>>D3</option>
							<option <?php if(!empty($data->lvl_pendidikan) ){if ($data->lvl_pendidikan=='SMA'){echo "selected";}} ?>>SMA</option>
							<option <?php if(!empty($data->lvl_pendidikan) ){if ($data->lvl_pendidikan=='SMK'){echo "selected";}} ?>>SMK</option>
							<option <?php if(!empty($data->lvl_pendidikan) ){if ($data->lvl_pendidikan=='MAN'){echo "selected";}} ?>>MAN</option>
							<option <?php if(!empty($data->lvl_pendidikan) ){if ($data->lvl_pendidikan=='SLTP'){echo "selected";}} ?>>SLTP</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="tgl-lulus">Tanggal Lulus</label>
						<input name="tgllulus" type="text" id="tgl-lulus" class="form-control datepicker" value="{{ $data->tgl_lulus or ''}}"/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="jurusan">Jurusan</label>
						<input name="jurusan" type="text" id="jurusan" class="form-control" value="{{ $data->jurusan or ''}}"/>
					</div>
				</div>
		</div>
	</div>
	</div>
	<div class="col-md-6">
	<div class="panel panel-color panel-success">
		<div class="panel-heading">
			<div class="panel-title">STATUS KERJA</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-4">
						<div class="form-group">
						<div class="col-sm-12">
						<label class="control-label" for="input-psa">PSA</label>
						<input name="psa" type="text" id="psa" class="form-control" value="{{ $data->id_psa or ''}}"data-validate="required" data-message-required="PSA Harus Dipilih"/>
						</div></div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
						<div class="col-sm-12">
						<label class="control-label" for="input-psa">Mitra</label>
						<input name="mitra" type="text" id="mitra" class="form-control" value="{{ $data->id_mitra or ''}}" data-validate="required" data-message-required="Mitra Harus Dipilih"/>
						</div></div>
					</div>
					<div class="col-sm-4">
						<label class="control-label" for="input-tgl-mulai-kerja">Tanggal Mulai Kerja</label>
						<input name="tglmulaikerja" type="text" id="input-tgl-mulai-kerja" class="form-control datepicker" value="{{ $data->tgl_mulai_kerja or ''}}"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<label class="control-label" for="status">Status Kerja</label>
						<select name="statuskerja" id="input-statuskerja" class="form-control selectboxit">
							<option value="1" <?php if(!empty($data->status) ){if ($data->status=='1'){echo "selected";}} ?>>OJT</option>
							<option value="2"<?php if(!empty($data->status) ){if ($data->status=='2'){echo "selected";}} ?>>Kontrak I</option>
							<option value="3"<?php if(!empty($data->status) ){if ($data->status=='3'){echo "selected";}} ?>>Kontrak II</option>
							<option value="4"<?php if(!empty($data->status) ){if ($data->status=='4'){echo "selected";}} ?>>Pembaharuan</option>
							<option value="5"<?php if(!empty($data->status) ){if ($data->status=='5'){echo "selected";}} ?>>Pegawai tetap</option>
							<option value="6" <?php if(!empty($data->status) ){if ($data->status=='6'){echo "selected";}} ?>>Resign</option>
						</select>
					</div>
					<div class="col-sm-4">
						<label class="control-label" for="kontrak1">Tanggal Awal</label>
						<input name="tgl" type="text" id="input-tgl" class="form-control datepicker"
						<?php if(empty($data) ){echo "value=''";}
							else { if ($data->status=='1'){echo "value=".$data->ojt; }
								else if ($data->status=='2'){echo "value=".$data->kontrak1; }
								else if ($data->status=='3'){echo "value=".$data->kontrak2; }
								else if ($data->status=='4'){echo "value=".$data->pembaharuan; }
								else if ($data->status=='5'){echo "value=".$data->pegawai_tetap; }
								else if ($data->status=='6'){echo "value=".$data->resign; }
								}	 ?>/>
					</div>
					<div class="col-sm-4">
						<label class="control-label" for="kontrak1">Tanggal Akhir</label>
						<input name="tgl2" type="text" id="input-tgl" class="form-control datepicker"
						<?php if(empty($data) ){echo "value=''";}
							else { if ($data->status=='1'){echo "value=".$data->ojt; }
								else if ($data->status=='2'){echo "value=".$data->kontrak1; }
								else if ($data->status=='3'){echo "value=".$data->kontrak2; }
								else if ($data->status=='4'){echo "value=".$data->pembaharuan; }
								else if ($data->status=='5'){echo "value=".$data->pegawai_tetap; }
								else if ($data->status=='6'){echo "value=".$data->resign; }
								}	 ?>/>
					</div>
				</div>
		</div>
	</div>
	</div>
	<div class="col-md-6">
	<div class="panel panel-color panel-info">
		<div class="panel-heading">
			<div class="panel-title">DATA KELUARGA</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-4">
						<label class="control-label" for="no-kk">Nomor Kartu Keluarga</label>
						<input name="nokk" type="text" id="no-kk" class="form-control" value="{{ $data->nokk or ''}}"/>
					</div>
					<div class="col-sm-4">
						<label class="control-label" for="status-keluarga">Status Dalam Keluarga</label>
						<input name="status_keluarga" type="text" id="status-keluarga" class="form-control" value="{{ $data->status_dalam_keluarga or ''}}"/>
					</div>
					<div class="col-sm-4">
						<label class="control-label" for="namapasangan">Nama Pasangan</label>
						<input name="namapasangan" type="text" id="namapasangan" class="form-control" value="{{ $data->nama_pasangan or ''}}"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<label class="control-label" for="anak1">Nama Anak I</label>
						<input name="anak1" type="text" id="anak1" class="form-control" value="{{ $data->nama_anak1 or ''}}"/>
					</div>
					<div class="col-sm-4">
						<label class="control-label" for="anak2">Nama Anak II</label>
						<input name="anak2" type="text" id="anak2" class="form-control" value="{{ $data->nama_anak2 or ''}}"/>
					</div>
					<div class="col-sm-4">
						<label class="control-label" for="anak3">Nama Anak III</label>
						<input name="anak3" type="text" id="anak3" class="form-control" value="{{ $data->nama_anak3 or ''}}"/>
					</div>
				</div>
		</div>
	</div>
	</div>
	<div class="col-md-6">
	<div class="panel panel-color panel-purple">
		<div class="panel-heading">
			<div class="panel-title">REKENING</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-4">
						<label class="control-label" for="norek">Nomor Rekening</label>
						<input name="norek" type="text" id="norek" class="form-control" value="{{ $data->no_rekening or ''}}"/>
					</div>
					<div class="col-sm-4">
						<label class="control-label" for="namarek">Nama Di Rekening</label>
						<input name="namarek" type="text" id="namarek" class="form-control" value="{{ $data->nama_rekening or ''}}"/>
					</div>
					<div class="col-sm-4">
						<label class="control-label" for="bank">Bank</label>
						<input name="bank" type="text" id="bank" class="form-control" value="{{ $data->bank or ''}}"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<label class="control-label" for="kantor-cabang">Kantor Cabang</label>
						<input name="kantor_cabang" type="text" id="kantor-cabang" class="form-control" value="{{ $data->kantor_cabang_bank or ''}}"/>
					</div>
					<div class="col-sm-8">
						<label class="control-label" for="alamat-bank">Alamat Bank</label>
						<input name="alamat_bank" type="text" id="alamat-bank" class="form-control" value="{{ $data->alamat_bank or ''}}"/>
					</div>
				</div>
		</div>
	</div>
	</div>
	<div class="col-md-12">
	<div class="panel panel-color panel-success">
		<div class="panel-heading">
			<div class="panel-title">JAMSOSTEK & BPJS</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-3">
						<label class="control-label" for="jamsostek">Nomor BPJS Ketenaga Kerjaan / Jamsostek</label>
						<input name="jamsostek" type="text" id="jamsostek" class="form-control" value="{{ $data->jamsostek or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="tgl-jamsostek">Tanggal BPJS Ketenaga Kerjaan / Jamsostek</label>
						<input name="tgl_jamsostek" type="text" id="tgl-jamsostek" class="form-control datepicker" value="{{ $data->tgl_jamsostek or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="bpjs">Nomor BPJS</label>
						<input name="bpjs" type="text" id="bpjs" class="form-control" value="{{ $data->bpjs or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="tgl-bpjs">Tanggal BPJS</label>
						<input name="tgl_bpjs" type="text" id="tgl_bpjs" class="form-control datepicker" value="{{ $data->tgl_bpjs or ''}}"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label class="control-label" for="no-bpjs-istri">Nomor BPJS Istri</label>
						<input name="bpjs_istri" type="text" id="bpjs_istri" class="form-control" value="{{ $data->bpjs_istri or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="tgl-bpjs-istri">Tanggal BPJS Istri</label>
						<input name="tgl_bpjs_istri" type="text" id="tgl_bpjs_istri" class="form-control datepicker" value="{{ $data->tgl_bpjs_istri or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="no-bpjs-anak1">Nomor BPJS Anak I</label>
						<input name="bpjs_anak1" type="text" id="bpjs_anak1" class="form-control" value="{{ $data->bpjs_anak1 or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="tgl-bpjs-anak1">Tanggal BPJS Anak I</label>
						<input name="tgl_bpjs_anak1" type="text" id="tgl_bpjs_anak1" class="form-control datepicker" value="{{ $data->tgl_bpjs_anak1 or ''}}"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label class="control-label" for="no-bpjs-anak2">Nomor BPJS Anak II</label>
						<input name="bpjs_anak2" type="text" id="bpjs_anak2" class="form-control" value="{{ $data->bpjs_anak2 or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="tgl-bpjs-anak2">Tanggal BPJS Anak II</label>
						<input name="tgl_bpjs_anak2" type="text" id="tgl_bpjs_anak2" class="form-control datepicker" value="{{ $data->tgl_bpjs_anak2 or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="no-bpjs-anak3">Nomor BPJS Anak III</label>
						<input name="bpjs_anak3" type="text" id="bpjs_anak3" class="form-control " value="{{ $data->bpjs_anak3 or ''}}"/>
					</div>
					<div class="col-sm-3">
						<label class="control-label" for="tgl-bpjs-anak3">Tanggal BPJS Anak III</label>
						<input name="tgl_bpjs_anak3" type="text" id="tgl_bpjs_anak3" class="form-control datepicker" value="{{ $data->tgl_bpjs_anak3 or ''}}"/>
					</div>
				</div>
		</div>
	</div>
	</div>
	<div class="col-md-6">
	<div class="panel panel-color panel-danger">
		<div class="panel-heading">
			<div class="panel-title">LAIN-LAIN</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="gadas">Gadas</label>
						<input name="gadas" type="text" id="gadas" class="form-control" value="{{ $data->gadas or ''}}"/>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="pelatihan">Pelatihan Ala</label>
						<input name="pelatihan" type="text" id="pelatihan" class="form-control" value="{{ $data->pelatihan_ala or ''}}"/>
					</div>
				</div>
		</div>
	</div>
	</div>
	<div class="col-md-6">
	<div class="panel panel-color panel-warning">
		<div class="panel-heading">
			<div class="panel-title">APD</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-6">
						<label class="control-label" for="input-ukuran">Ukuran Baju</label>
						<select name="ukuran" id="input-ukuran" class="form-control selectboxit">
							<option <?php if(!empty($data->ukuran_baju) ){if ($data->ukuran_baju=='S'){echo "selected";}} ?>>S</option>
							<option <?php if(!empty($data->ukuran_baju) ){if ($data->ukuran_baju=='M'){echo "selected";}} ?>>M</option>
							<option <?php if(!empty($data->ukuran_baju) ){if ($data->ukuran_baju=='L'){echo "selected";}} ?>>L</option>
							<option <?php if(!empty($data->ukuran_baju) ){if ($data->ukuran_baju=='XL'){echo "selected";}} ?>>XL</option>
							<option <?php if(!empty($data->ukuran_baju) ){if ($data->ukuran_baju=='XXL'){echo "selected";}} ?>>XXL</option>
						</select>
					</div>
					<div class="col-sm-6">
						<label class="control-label" for="seragam">Penerimaan Seragam Ke</label>
						<input name="seragam" type="text" id="seragam" class="form-control" value="{{ $data->penerimaan_seragam or ''}}"/>
					</div>
				</div>
				<div class="form-group"><br></div>
				<div class="form-group">
					<div class="col-sm-12">
						<label class="checkbox-inline">
							<input name="rompi" type="checkbox" value="1" <?php if(!empty($data->rompi) ){if ($data->rompi==1){echo "checked";}} ?>>Rompi
						</label>
						<label class="checkbox-inline">
							<input name="helm" type="checkbox" value="1" <?php if(!empty($data->helm) ){if ($data->helm==1){echo "checked";}} ?>>Helm
						</label>
						<label class="checkbox-inline">
							<input name="kacamata" type="checkbox" value="1" <?php if(!empty($data->kacamata) ){if ($data->rompi==1){echo "checked";}} ?>>Kacamata
						</label>
						<label class="checkbox-inline">
							<input name="sepatu_safety" type="checkbox" value="1" <?php if(!empty($data->sepatu_safety) ){if ($data->sepatu_safety==1){echo "checked";}} ?>>Sepatu Safety
						</label>
						<label class="checkbox-inline">
							<input name="sarung_tangan" type="checkbox" value="1" <?php if(!empty($data->sarung_tangan) ){if ($data->sarung_tangan==1){echo "checked";}} ?>>Sarung Tangan
						</label>
					</div>
				</div>
		</div>
	</div>
	</div>
	<div class="col-md-12">
	<div class="panel panel-color panel-blue">
		<div class="panel-heading">
			<div class="panel-title">AKSI</div>
		</div>
		<div class="panel-body">
			<div class="form-group">
				<div class=" col-sm-2">
					<button class="btn btn-primary" type="submit">
						<span class="glyphicon glyphicon-floppy-disk"></span>
						<span>Simpan</span>
					</button>
				</div>
			</div>
		</div>
	</div>
	</div>
</form>
</div>
@endsection

@section('plugins')
	<script src="/lib/xenon/js/datepicker/bootstrap-datepicker.js"></script>
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/select2/select2.min.js"></script>
	<script src="/lib/xenon/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="/lib/xenon/js/inputmask/jquery.inputmask.bundle.js"></script>
	<script src="/lib/xenon/js/jquery-validate/jquery.validate.min.js"></script>
@endsection

@section('footer')
<script>
	$(".datepicker").datepicker( {
    	format: "yyyy-mm-dd",
    	autoclose: true
	});

	$(".selectboxit").selectBoxIt();
	$("#input-agama").select2();
	$("#input-jk").select2();

	var data = <?= json_encode($psa) ?>;
	$('#psa').select2({
		data: data,
		placeholder: 'Input PSA',
		allowClear: true,
		minimumInputLength: 0,
		formatSelection: function(data) { return data.text },
		formatResult: function(data) {
		return  '<span class="label label-info">'+data.id+'</span>'+
						'<strong style="margin-left:5px">'+data.text+'</strong>';
		}
	});

	var data = <?= json_encode($mitra) ?>;
	$('#mitra').select2({
		data: data,
		placeholder: 'Input Mitra',
		allowClear: true,
		minimumInputLength: 0,
		formatSelection: function(data) { return data.text },
		formatResult: function(data) {
		return  '<span class="label label-info">'+data.id+'</span>'+
						'<strong style="margin-left:5px">'+data.text+'</strong>';
		}
	});
</script>

@endsection