@include('partial.confirm')
@extends('layout')

@section('html-title')
	 HR :: TOMMAN
@endsection

@section('xs-title')
	<i class="linecons-calendar"></i>
	Project :
@endsection

@section('page-title')
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2.css">
	<link rel="stylesheet" href="/lib/xenon/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="/lib/xenon/js/datatables/dataTables.bootstrap.css">
	<script src="/lib/xenon/js/jquery-1.11.1.min.js"></script>

	<ol class="breadcrumb">
		<li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
		<li><i class="linecons-calendar"></i>HR</li>
		<li class="active"><i class="linecons-tag">List Karyawan</i></li>
		<a href="/hr/formKaryawan/input" class="btn btn-info btn-sm btn-icon icon-left fa-plus tooltip-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Insert Karyawan">
		</a>
		<a href="/hr/formKaryawanMitra/input" class="btn btn-warning btn-sm btn-icon icon-left fa-plus tooltip-warning" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Insert Karyawan Mitra">
		</a>
		<a class="tooltip-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Cetak Data Karyawan ">
          	<button class="btn btn-secondary btn-sm btn-icon icon-left fa-file-excel-o"  id="button-excel"
          	type="button" data-toggle="modal" data-target="#modalExcel">
          	</button>
        </a>
	</ol>
@endsection

@section('modal')
<div class="modal fade" id="modalExcel">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Cetak Data Karyawan</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<form method="POST" action="/hr/cetakExcelKaryawan" accept-charset="UTF-8" style="display:inline" id="form_excel"
							class="form-horizontal">
							<div class="col-md-2">
								<label for="field-1" class="control-label">PSA</label>
							</div>
							<div class="col-sm-10">
								<input name="psa" id="psa" class="form-control"/>
							</div>
							<!-- tabel -->
							<div >
							<table class="table table-bordered table-striped table-small-font" id="example-3">
								<thead>
									<tr>
										<th>Check<br><input type="checkbox" class="iswitch-lg iswitch-info"></th>
										<th>Kolom</th>
									</tr>
								</thead>
								<tbody class="middle-align">
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.nik" class="iswitch-lg iswitch-info center"></td>
											<td >NIK</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.nama" class="iswitch-lg iswitch-info center"></td>
											<td >Nama</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.tgl_lahir" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Lahir</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.kota_lahir" class="iswitch-lg iswitch-info center"></td>
											<td >Kota Lahir</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="loker.nama_loker" class="iswitch-lg iswitch-info center"></td>
											<td >PSA</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="job_position.position_name" class="iswitch-lg iswitch-info center"></td>
											<td >Position Name</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.agama" class="iswitch-lg iswitch-info center"></td>
											<td >Agama</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.jk" class="iswitch-lg iswitch-info center"></td>
											<td >Jenis Kelamin</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.alamat" class="iswitch-lg iswitch-info center"></td>
											<td >Alamat</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.kota" class="iswitch-lg iswitch-info center"></td>
											<td >Kota</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.suku" class="iswitch-lg iswitch-info center"></td>
											<td >Suku</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.status_nikah" class="iswitch-lg iswitch-info center"></td>
											<td >Status Nikah</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.maiden" class="iswitch-lg iswitch-info center"></td>
											<td >Maiden</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.goldar" class="iswitch-lg iswitch-info center"></td>
											<td >Golongan Darah</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.ktp" class="iswitch-lg iswitch-info center"></td>
											<td >KTP</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.npwp" class="iswitch-lg iswitch-info center"></td>
											<td >NPWP</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.telpon" class="iswitch-lg iswitch-info center"></td>
											<td >Nomor Telpon</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.email" class="iswitch-lg iswitch-info center"></td>
											<td >Email</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.tgl_mulai_kerja" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Mulai Kerja</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.status" class="iswitch-lg iswitch-info center"></td>
											<td >Status Kerja</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.ojt" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Mulai OJT</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.ojt_akhir" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Berakhir OJT</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.kontrak1" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Mulai Kontrak I</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.kontrak1_akhir" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Berakhir Kontrak I</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.kontrak2" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Mulai Kontrak II</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.kontrak2_akhir" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Berakhir Kontrak II</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.pembaharuan" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Mulai Pembaharuan</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.pembaharuan_akhir" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Berakhir Pembaharuan</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.pegawai_tetap" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Mulai Pegawai Tetap</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.pegawai_tetap_akhir" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Berakhir Pegawai Tetap</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.resign" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Resign</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.lvl_pendidikan" class="iswitch-lg iswitch-info center"></td>
											<td >Level Pendidikan</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.penyelenggara_pendidikan" class="iswitch-lg iswitch-info center"></td>
											<td >Penyelenggara Pendidikan</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.tgl_lulus" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Lulus</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.jurusan" class="iswitch-lg iswitch-info center"></td>
											<td >Jurusan</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.nokk" class="iswitch-lg iswitch-info center"></td>
											<td >Nomor Kartu Keluarga</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.status_dalam_keluarga" class="iswitch-lg iswitch-info center"></td>
											<td >Status Dalam Keluarga</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.nama_pasangan" class="iswitch-lg iswitch-info center"></td>
											<td >Nama Pasangan</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.nama_anak1," class="iswitch-lg iswitch-info center"></td>
											<td >Nama Anak I</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.nama_anak2," class="iswitch-lg iswitch-info center"></td>
											<td >Nama Anak II</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.nama_anak3" class="iswitch-lg iswitch-info center"></td>
											<td >Nama Anak III</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.no_rekening" class="iswitch-lg iswitch-info center"></td>
											<td >Nomor Rekening</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.nama_rekening" class="iswitch-lg iswitch-info center"></td>
											<td >Nama Di Rekening</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.bank" class="iswitch-lg iswitch-info center"></td>
											<td >Nama Bank</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.kantor_cabang_bank" class="iswitch-lg iswitch-info center"></td>
											<td >Kantor Cabang Bank</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.alamat_bank" class="iswitch-lg iswitch-info center"></td>
											<td >Alamat Bank</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.jamsostek" class="iswitch-lg iswitch-info center"></td>
											<td >Nomor Jamsostek</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.tgl_jamsostek" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal Jamsostek</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.bpjs" class="iswitch-lg iswitch-info center"></td>
											<td >Nomor BPJS</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.tgl_bpjs" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal BPJS</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.bpjs_istri" class="iswitch-lg iswitch-info center"></td>
											<td >Nomor BPJS Istri</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.tgl_bpjs_istri" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal BPJS Istri</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.bpjs_anak1" class="iswitch-lg iswitch-info center"></td>
											<td >Nomor BPJS Anak I</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.tgl_bpjs_anak1" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal BPJS Anak I</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.bpjs_anak2" class="iswitch-lg iswitch-info center"></td>
											<td >Nomor BPJS Anak II</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.tgl_bpjs_anak2" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal BPJS  Anak II</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.bpjs_anak3" class="iswitch-lg iswitch-info center"></td>
											<td >Nomor BPJS Anak III</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.tgl_bpjs_anak3" class="iswitch-lg iswitch-info center"></td>
											<td >Tanggal BPJS Anak III</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.nik_atasan" class="iswitch-lg iswitch-info center"></td>
											<td >NIK Atasan</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.gadas" class="iswitch-lg iswitch-info center"></td>
											<td >Gadas</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.pelatihan_ala" class="iswitch-lg iswitch-info center"></td>
											<td >Pelatihan Ala</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.ukuran_baju" class="iswitch-lg iswitch-info center"></td>
											<td >Ukuran Baju</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.penerimaan_seragam" class="iswitch-lg iswitch-info center"></td>
											<td >Penerimaan Seragam Ke</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.rompi" class="iswitch-lg iswitch-info center"></td>
											<td >Rompi</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.helm" class="iswitch-lg iswitch-info center"></td>
											<td >Helm</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.kacamata" class="iswitch-lg iswitch-info center"></td>
											<td >Kacamata</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.sepatu_safety" class="iswitch-lg iswitch-info center"></td>
											<td >Sepatu Safety</td>
										</tr>
										<tr>
											<td ><input type="checkbox" name="cek[]" value="karyawan.sarung_tangan" class="iswitch-lg iswitch-info center"></td>
											<td >Sarung Tangan</td>
										</tr>
								</tbody>
							</table>
							</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-info" id="cetak">Cetak</button>
				</div>
				</form>
			</div>
		</div>
	</div>

@endsection

@section('body')

	<div class="panel panel-default panel-noheading">
		<div class="panel-body">
			<form method="post" class="form-horizontal">
			<div class="input-group">
				<div class="col-sm-1">
					<label class="control-label" for="input-status">Jenis</label>
				</div>
				<div class="col-sm-2">
					<select name="jenis" id="jenis" class="form-control selectboxit">
						<option value="TA">Telkom Akses</option>
						<option value="Mitra">Mitra</option>
					</select>
				</div>
				<div class="col-sm-1">
					<label class="control-label" for="input-status">PSA</label>
				</div>
				<div>
					<div class="col-sm-3">
						<input name="psa2" id="psa2" class="form-control"/>
					</div>
				</div>
		      <span class="input-group-btn">
		      	<button id="cari" class="btn btn-blue btn-icon btn-icon-standalone btn-icon-standalone-right" type="button">
					<i class="fa-search"></i>
					<span>Search</span>
				</button>
		      </span>
		    </div>
			</form>

		</div>
	</div>

	<div class="panel panel-default panel-noheading">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-small-font" id="example-2">
			<thead>
				<tr>
					<th>Nomor</th>
					<th>NIK</th>
					<th>Nama</th>
					<th>Kota Lahir</th>
					<th>Tanggal Lahir</th>
					<th>Status Kerja</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody class="middle-align">

			</tbody>
		</table>
		</div>
	</div>

@endsection

@section('plugins')
	<script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="/lib/xenon/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/lib/xenon/js/datatables/dataTables.bootstrap.js"></script>
	<script src="/lib/xenon/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="/lib/xenon/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
	<script src="/lib/xenon/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="/lib/xenon/js/select2/select2.min.js"></script>

@endsection

@section('footer')
	<script>
    $('#confirmDelete').on('show.bs.modal', function (e) {
      $message = $(e.relatedTarget).attr('data-message');
      $(this).find('.modal-body p').text($message);
      $title = $(e.relatedTarget).attr('data-title');
      $(this).find('.modal-title').text($title);
      // Pass form reference to modal for submission on yes/ok
      var form = $(e.relatedTarget).closest('form');
      $(this).find('.modal-footer #confirm').data('form', form);
    });
    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
        $(this).data('form').submit();
    });

    var data = <?= json_encode($psa) ?>;
	$('#psa').select2({
		data: data,
		placeholder: 'Input PSA',
		allowClear: true,
		minimumInputLength: 0,
		formatSelection: function(data) { return data.text },
		formatResult: function(data) {
		return  '<span class="label label-info">'+data.id+'</span>'+
						'<strong style="margin-left:5px">'+data.text+'</strong>';
		}
	});
	$('#psa2').select2({
		data: data,
		placeholder: 'Input PSA',
		allowClear: true,
		minimumInputLength: 0,
		formatSelection: function(data) { return data.text },
		formatResult: function(data) {
		return  '<span class="label label-info">'+data.id+'</span>'+
						'<strong style="margin-left:5px">'+data.text+'</strong>';
		}
	});

	$("#cari").on('click',function(){
		if ($("#jenis").val()=="TA"){
		    $.get("/hr/ajaxKaryawanTA/TA/"+$("#psa2").val(), function( data ) {
	          	$("#example-2").html(data);
	        });
		}
		else if ($("#jenis").val()=="Mitra"){
		    $.get("/hr/ajaxKaryawanMitra/Mitra/"+$("#psa2").val(), function( data ) {
	          	$("#example-2").html(data);
	        });
		}
	});

	//sDom : "t" +'r<"H"lf><"datatable-scroll"t><"F"ip>',
	$("#example-3").DataTable({

		"scrollY": "200px",
	   	"scrollCollapse": true,
	   	"paging" :false,
	   	"searching":false,
	   	"ordering" :false,
	   	"info" :false

   });

	// Replace checkboxes when they appear
	var $state = $("#example-3 thead input[type='checkbox']");

	$("#example-3").on('draw.dt', function()
	{
		cbr_replace();

		$state.trigger('change');
	});

	// Script to select all checkboxes
	$state.on('change', function(ev)
	{
		var $chcks = $("#example-3 tbody input[type='checkbox']");

		if($state.is(':checked') )
		{
			$chcks.prop('checked', true).trigger('change');
		}
		else
		{
			$chcks.prop('checked', false).trigger('change');
		}
	});



  </script>
@endsection