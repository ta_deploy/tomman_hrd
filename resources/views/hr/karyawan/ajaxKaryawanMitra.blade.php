<table class="table table-bordered table-striped table-small-font" id="example-2">
<?php
function TanggalIndo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

  $tahun = substr($date, 0, 4);
  $bulan = substr($date, 5, 2);
  $tgl   = substr($date, 8, 2);

  if ($bulan<1){ $result='';}
		else { $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;  }
  return($result);
}
?>   
<thead>
	<tr>
		<th>Nomor</th>
		<th>NIK</th>
		<th>Nama</th>
		<th>Kota Lahir</th>
		<th>Tanggal Lahir</th>
		<th>Mitra</th>
		<th>Aksi</th>
	</tr>
</thead>
<tbody class="middle-align">
	<?php $no=1; ?>
	@foreach($list as $r)
		<tr>
			<td>{{$no}}</td>
			<td>{{$r->nik}}</td>
			<td>{{$r->nama}}</td>
			<td>{{$r->kota_lahir}}</td>
			<td><?php echo TanggalIndo($r->tgl_lahir); ?></td>
			<td >{{$r->nama_mitra}}</td>
			<td><a href="/hr/formKaryawanMitra/{{$r->id}}" class="btn btn-secondary btn-sm btn-icon icon-left fa-edit tooltip-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" >					
				</a>
				<form method="POST" action="/hr/deleteKaryawanMitra/{{$r->id}}" accept-charset="UTF-8" style="display:inline"
					class="tooltip-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus">
                  <button class="btn btn-danger btn-sm btn-icon icon-left fa-trash tooltip-danger" 
                  type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Hapus Data Karyawan Mitra {{ $r->nama }} ?" data-message="Are you sure you want to delete this ?">
                
                  </button>
                </form>
			</td>
		</tr>
		<?php $no++;?>
	@endforeach
</tbody>
</table>