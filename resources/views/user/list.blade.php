@include('partial.confirm')
@extends('layout')

@section('html-title')
   Finance :: TOMMAN
@endsection

@section('xs-title')
  <i class="linecons-calendar"></i>
  Project : 
@endsection

@section('page-title')
  <style>
  .right{
    text-align: right;
  }
  </style>

  <ol class="breadcrumb">
    <li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
    <li><i class="linecons-calendar"></i>User</li>
    <li class="active"><i class="linecons-tag">List User</i></li>
  </ol>


@endsection

@section('body')
  <div class="panel panel-default panel-noheading">
      
              <table class="table table-hover members-table middle-align">
                <thead>
                  <tr>
                    
                    <th class="hidden-xs hidden-sm"></th>
                    <th>Name and Role</th>
                    <th>ID</th>
                    <th class="hidden-xs hidden-sm">Date</th>
                    <th>Settings</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($list as $r)
                    <tr>
                      <td class="user-image hidden-xs hidden-sm">
                        <a href="#">
                          <img src="/image/user-1.png" class="img-circle" alt="user-pic" />
                        </a>
                      </td>
                      <td class="user-name">
                        <a href="#" class="name">{{ $r->nama }}</a><br/>
                        <em>{{ $r->level_name }}</em>
                      </td>
                      <td class="user-id">
                        {{ $r->name }}
                      </td>
                      <td class="hidden-xs hidden-sm">
                        <em>{{ isset($r->created_at) ? 'Created :'.$r->created_at : '' }}</em><br/>
                        <em>{{ isset($r->updated_at) ? 'Updated :'.$r->updated_at : '' }}</em>
                      </td>
                      
                      <td class="action-links">
                        <a href="/user/{{ $r->id }}" class="btn btn-xs btn-blue" role="button">
                  <span class="glyphicon glyphicon-pencil"></span>
                Edit</a>
                <form method="POST" action="/delete-user/{{ $r->id }}" accept-charset="UTF-8" style="display:inline">
                  <button class="btn btn-xs btn-danger" type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Delete User" data-message="Are you sure you want to delete this user ?">
                  <i class="glyphicon glyphicon-trash"></i> 
                    Delete
                  </button>
                </form>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
@endsection

@section('plugins')
  <script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
  <script src="/lib/xenon/js/datatables/js/jquery.dataTables.min.js"></script>
  <script src="/lib/xenon/js/datatables/dataTables.bootstrap.js"></script>
  
@endsection

@section('footer')
  <script>
    $("#example-2").dataTable();
    $('#confirmDelete').on('show.bs.modal', function (e) {
      $message = $(e.relatedTarget).attr('data-message');
      $(this).find('.modal-body p').text($message);
      $title = $(e.relatedTarget).attr('data-title');
      $(this).find('.modal-title').text($title);
      // Pass form reference to modal for submission on yes/ok
      var form = $(e.relatedTarget).closest('form');
      $(this).find('.modal-footer #confirm').data('form', form);
    });
    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
        $(this).data('form').submit();
    });
  </script>
@endsection