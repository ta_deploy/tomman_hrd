@extends('layout')

@section('html-title')
   Finance :: TOMMAN
@endsection

@section('xs-title')
  <i class="linecons-calendar"></i>
  Project :
@endsection

@section('page-title')
  <link rel="stylesheet" href="/lib/xenon/js/select2/select2.css">
  <link rel="stylesheet" href="/lib/xenon/js/select2/select2-bootstrap.css">
  <style>
    #keperluan{
      height : 33px;
      resize : none;
    }
  </style>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa-home"></i>TOMMAN</a></li>
    <li><a href="/user"><i class="linecons-calendar"></i>List User</li></a>
    <li class="active"><i class="linecons-tag">Update</i> </li>
  </ol>
@endsection
@section('body')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="id" type="hidden" class="form-control" value="{{ $data->id or ''}}" />
    <div class="form-group">
      <input name="name" type="text" id="input-name" class="form-control" placeholder="Username" value="{{ $data->name or '' }}" />
    </div>
    <div class="form-group">
      <input name="password" type="password" id="input-pwd" class="form-control" placeholder="Password" />
    </div>
    <div class="form-group">
      <input name="karyawan_id" type="hidden" id="input-kar" class="form-control" value="{{ $data->karyawan_id or '' }}" />
    </div>
    <div class="form-group">
      <input name="level_id" type="hidden" id="input-lvl" class="form-control" value="{{ $data->level_id or '' }}" />
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary" type="submit">Simpan</button>
    </div>
  </form>

@endsection
@section('plugins')
  <script src="/lib/xenon/js/jquery-ui/jquery-ui.min.js"></script>
  <script src="/lib/xenon/js/select2/select2.min.js"></script>

@endsection

@section('footer')
<script>

  $(document).ready(function () {
      var data = <?= json_encode($karyawan) ?>;
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Pilih Karyawan',
          allowClear: true,
          escapeMarkup: function(m) { return m },
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-kar').select2(select2Options() );
      var lvl = <?= json_encode($level) ?>;
      var select2Options = function() {
        return {
          data: lvl,
          placeholder: 'Pilih Level',
          allowClear: true,
          escapeMarkup: function(m) { return m },
          formatSelection: function(data) { return data.level_name },
          formatResult: function(data) {
            return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.level_name+'</strong>';
          }
        }
      }
      $('#input-lvl').select2(select2Options() );

  });
  </script>
@endsection