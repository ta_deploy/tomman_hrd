<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <link rel="icon" href="/image/icon.png" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Mari Bergabung Bersama Kami Putra Putri Terbaik di Banua Kalimantan untuk Berkesempatan Berkarir pada PT Telkom Akses dan Mitra - Mitra Kami di Seluruh Area Kalimantan | PERWIRA">
  <meta name="keywords" content="personalia warior akses,perwira,perwira tomman,tomman,perwira telkomakses,perwira telkom akses,warior akses,info loker telkom,info loker telkomakses,info loker kalimantan,loker,indihome">
  <meta name="author" content="Personalia Warior Akses - PERWIRA">
  <meta name="google-site-verification" content="JtYYY_8Z43BZrb7vjCMsOyo8QM2SfnqJNWldfFhhk1k" />

  <title>P E R W I R A</title>

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/css/pixeladmin.min.css" rel="stylesheet" type="text/css">
  <link href="/css/widgets.min.css" rel="stylesheet" type="text/css">
  <link href="/css/themes/clean.min.css" rel="stylesheet" type="text/css">

  <style>
    body {
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url('/image/bg-perwira.png');
    }
    .page-signin-header {
      box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
    }

    .page-signin-header .btn {
      position: absolute;
      top: 12px;
      right: 15px;
    }

    .page-signin-container {
      width: auto;
      margin: 30px 10px;
    }

    .page-signin-container form {
      border: 0;
      box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
    }

    @media (min-width: 544px) {
      .page-signin-container {
        width: 350px;
        margin: 60px auto;
      }
    }

    #page-signin-forgot-form { display: none; }
  </style>
</head>
<body>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" style="vertical-align: middle;">📣 Pengumuman</h5>
            </button>
        </div>
        <div class="modal-body">
          Untuk informasi terbaru terkait perusahaan bisa rekan cek di sosial media kami dibawah ini
          <br/><br/>
          <i class="fab fa-instagram"></i> Instagram : <a href="https://instagram.com/kalsel_hibat" style="color: black;">@kalsel_hibat</a>
          <br/>
          <i class="fab fa-instagram"></i> Instagram : <a href="https://www.instagram.com/infotelkomaksesbalikpapan" style="color: black;">@infotelkomaksesbalikpapan</a>
          <br/><br/>
          <i class="fab fa-youtube"></i> Youtube : <a href="https://www.youtube.com/@kalselhibat9382" style="color: black;">Telkom Akses Banjarmasin</a>
          <br/>
          <i class="fab fa-youtube"></i> Youtube : <a href="https://www.youtube.com/@performancekalimantanbersa8320" style="color: black;">Telkom Akses Balikpapan</a>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>

  <div class="page-signin-header p-a-2 text-sm-center bg-white">
    <b style="color: black !important; font-size: 30px">PERSONALIA</b> <b style="color: red; font-size: 30px">WARIOR AKSES</b>
  </div>


  <div class="page-signin-container" id="page-signin-form">

    @if ($message = Session::has('alerts') )
    <div class="alert alert-{{ Session::get('alerts')[0]['type'] }}">
        <strong>{{ Session::get('alerts')[0]['text'] }}</strong>
    </div>
    @endif

    <h2 class="m-t-0 m-b-4 text-xs-center font-weight-semibold font-size-20">Sign In</h2>

    <form method="POST" class="panel p-a-4">
      <fieldset class=" form-group form-group-lg">
        <input type="text" class="form-control" name="user" placeholder="Username">
      </fieldset>

      <fieldset class=" form-group form-group-lg">
        <input type="password" class="form-control" name="pass" placeholder="Password">
      </fieldset>

      <button type="submit" class="btn btn-block btn-lg btn-primary m-t-3">Login</button>

      <br />
      <div style="text-align: center">
        <label class="custom-control custom-radio radio-inline">
          <input type="radio" name="type_login" class="custom-control-input" value="lokal" checked>
          <span class="custom-control-indicator"></span>
          Lokal
        </label>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <label class="custom-control custom-radio radio-inline">
          <input type="radio" name="type_login" class="custom-control-input" value="sso">
          <span class="custom-control-indicator"></span>
          SSO
        </label>
      </div>

      <center>
      <br /><br />
      <span class="text-muted">You already have an account ? <a href="/register">Register Here!</a></span>
      </center>
    </form>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="/pace/pace.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>
  <script src="/js/app.js"></script>
  <script type="text/javascript">
      $( function(){

          $(window).on('load', function() {
              $('#myModal').modal('show');
          });
      });
  </script>

</body>
</html>