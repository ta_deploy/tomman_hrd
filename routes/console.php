<?php

use Illuminate\Foundation\Inspiring;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SekertarisController;
use App\DA\PublicModel;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote() );
})->describe('Display an inspiring quote');

Artisan::command('tele_bot {id}', function ($id) {
	$HomeModel = new HomeController();
	$HomeModel->running_me_baby($id);
});

Artisan::command('updateAPILocate', function () {
	HomeController::updateAPILocate();
});

Artisan::command('get_laporan_performance', function () {
	$SekertarisController = new SekertarisController();
	$SekertarisController->get_laporan_performance();
});


Artisan::command('check_notif_recruitment', function () {
	PublicModel::check_notif_recruitment();
});

Artisan::command('sendWhatsapp {to_name} {number}', function ($to_name, $number) {
	PublicModel::sendWhatsapp($to_name, $number);
});