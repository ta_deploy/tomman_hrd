<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//recruitment

use App\Http\Controllers\HrController;

Route::get('/get_ioan_kpi', 'PerfController@get_perf_ioan');

Route::get('/absen_dow', 'HrController@absen_dow');
Route::post('/absen_dow_save', 'HrController@absen_dow_save');

// Route::get('/recruitment', function(){
// 	return view('recruitment.annoucement');
// });

Route::get('/recruitment', 'PublicController@recruitment');
Route::post('/recruitment', 'PublicController@recruitmentSave');

Route::get('/theme', 'LoginController@themes');
Route::post('/theme', 'LoginController@themeSave');
Route::get('/rec/get_region', 'PublicController@ajx_region');

Route::get('/try_me_bitch', function(){
	return view('fa.tes_api');
});

Route::get('/updateAPILocate', 'HomeController@updateAPILocate');

Route::get('/login', 'LoginController@index');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');
Route::get('/register', 'LoginController@register');
Route::post('/register', 'LoginController@registerSave');

Route::get('/survey_education', 'LoginController@survey_education');
Route::post('/survey_education', 'LoginController@save_survey_education');

Route::get('/enemy_spotted', 'HomeController@running_me_baby');

//HSE
Route::get('/input_potensi_b/{nik}', 'HseController@input');
Route::post('/input_potensi_b/{nik}', 'HseController@submit_teknisi');

//APK
Route::post('/APILogin', 'LoginController@getLoginResult');
Route::post('/APILoginByToken', 'LoginController@getLoginByToken');
Route::post('/APIAset', 'ApiController@getAset');
Route::post('/APISaveLaporanAset', 'ApiController@saveLaporanAset');
Route::post('/APIGetLaporanAset', 'ApiController@getLaporanAset');

//sekertaris
Route::get('/getGambarLaporan', 'SekertarisController@getGambar');
Route::get('/sendMttrGambarLaporan', 'SekertarisController@mttrGambar');
Route::get('/reportLaporanRing3/{date}', 'SekertarisController@report');
Route::get('/hapusLaporanRing3/{nik}', 'SekertarisController@hapusLaporan');

// Route::get('/listLaporanRing3', 'SekertarisController@list');
// Route::get('/formLaporanRing3', 'SekertarisController@form');
// Route::post('/formLaporanRing3', 'SekertarisController@save');

//leaderboard
Route::get('/leaderboard/{tgl?}', 'SekertarisController@leaderboard');
Route::get('/detail_leaderboard/{sektor}/{jenis}/{tgl_load}', 'SekertarisController@leaderboard_detail');

Route::get('/inputkpiprov', 'SekertarisController@inputkpiprov');
Route::post('/inputkpiprov', 'SekertarisController@saveinputkpiprov');

Route::get('/leaderboardprov/{tgl?}', 'SekertarisController@leaderboardprov');
Route::get('/detail_leaderboardprov/{sektor}/{jenis}/{tgl_load}', 'SekertarisController@leaderboard_detail_prov');
Route::get('/jx_prov_get', 'SekertarisController@leaderboard_ajax_prov');

//new
//TL
Route::get('/mvpLaporanRing3', 'SekertarisController@mvp');
Route::get('/mvpLaporanRing3/{date}', 'SekertarisController@mvp');
Route::get('/listLaporanRing3', 'SekertarisController@listnew');
Route::get('/formLaporanRing3/{nik}', 'SekertarisController@formnew');
Route::post('/formLaporanRing3/{nik}', 'SekertarisController@savenew');

Route::get('/formLaporanRing3', function(){
	return view('sekertaris.formnik');
});

Route::get('/cektidaklapor', function(){
	return view('sekertaris.tidaklapor');
});

Route::group(['prefix' => 'jx_dng'], function(){
	Route::get('lb_get', 'SekertarisController@leaderboard_ajax');
	Route::get('select_alker/{jenis_select}', 'AjaxController@jenis_select');
	Route::post('get_alker_teknisi', 'AjaxController@get_alker_teknisi');
});

// Route::post('/ajaxcektidaklapor', 'SekertarisController@ajaxcektidaklapor');

Route::get('/organisasi', 'HomeController@organisasi');
Route::post('/organisasi/{id}', 'HomeController@save_create');

//MGR
Route::get('/listSetupLaporanRing3', 'SekertarisController@setuplist');
Route::get('/formSetupLaporanRing3/{nik}', 'SekertarisController@setupform');
Route::post('/formSetupLaporanRing3/{nik}', 'SekertarisController@setupsave');
Route::post('/formSaveTematik', 'SekertarisController@tematiksave');

Route::group(['middleware' => 'tomman.auth'], function () {
	Route::get('/performance', 'HomeController@performance');
	Route::get('/', 'HomeController@home_beranda');
	Route::get('/Emp_dashboard', 'HomeController@home_employee');
	Route::get('/profile', 'HomeController@profile');
	Route::post('/profile', 'HomeController@profile_save');
	Route::get('/info_search/{id}', 'HomeController@info_search');
	Route::post('/info_search/{id}', 'HomeController@update_data_search');
	Route::get('/profile/view', 'HomeController@view_profile');
	Route::post('/profile/view', 'HomeController@view_profile_save');
	Route::get('/search/result', 'HomeController@result_search');
	Route::get('/updateEmployee', 'HomeController@updateEmployee');
	Route::get('/download_all_file/{nik}', 'HomeController@download_all_file');

	Route::get('/scan', function(){
		return view('fa.formScan');
	});

	Route::get('/event_list', 'HrController@event_list');
	Route::get('/event_list/{id}', 'HrController@event_form');
	Route::post('/event_list/{id}', 'HrController@event_save');
	Route::get('/event_delete/{id}', 'HrController@event_delete');
	Route::get('/event_download/{id}', 'HrController@event_download');
	Route::get('/scanrfid/{id}', 'HrController@scanrfid');
	Route::get('/rfid_register/{rfid}/{event_id}', 'HrController@register_rfid_form');
	Route::post('/rfid_register', 'HrController@register_rfid');
	Route::post('/scanned_rfid', 'HrController@scanned_rfid');

	Route::get('/hasil_test_hadir', function(){
		return view('home.test_hadir_result');
	});

	//HSE
	Route::group(['middleware' => 'checkrole:1,44'], function () {
		Route::get('/check_data', 'HseController@check_potensi');
		Route::get('/wadah_potensi', 'HseController@list_potensi');
		Route::get('/potensi_finished', 'HseController@potensi_finished');
		Route::get('/get_potensi/{jenis}', 'HseController@get_potensi');
		Route::get('/delete_potensi/{id}', 'HseController@delete_potensi');

		Route::get('/detail_potensi/{id}', 'HseController@detail_potensi');
		Route::post('/detail_potensi/{id}', 'HseController@update_perbaikan');

		Route::post('/check_hse/{id}', 'HseController@check_hse');

		Route::get('/map_rawan_bahaya', 'HseController@map_rawan_bahaya');
	});

	Route::get('/event_doorprize', 'HrController@event_dp');

	Route::get('report/alker','ReportController@list_alker');

	Route::group(['middleware' => 'checkrole:1,71'], function () {
		Route::group(['prefix' => 'mitra'], function(){
			//list mitra
			Route::get('list','MitraController@listKaryawan');

			//tambah teknisi
			Route::get('addNew_teknisi','MitraController@addNew_teknisi');

			//resign
			Route::get('resign/{id}','MitraController@resign');

			//alker
			Route::get('tools/addAlker','MitraController@addNew_alker');
		});
	});

	//hr checkrole:1,30,46,22,69,12,71
	Route::group(['middleware' => 'checkrole:1, 30, 69'], function () {
		Route::group(['prefix' => 'hr'], function(){

			Route::group(['prefix' => 'list'], function(){
				//pemakar
				Route::get('pelamar/perC/{id}', 'HrController@list_pelamar_country');
				//nik
				Route::get('nik', 'HrController@list_nik');

				//user
				Route::get('created_user', 'HrController@listcreated_user');

				//level
				Route::get('level', 'HrController@list_level');

				//mitra
				Route::get('mitra', 'HrController@list_mitra');

				//witel
				Route::get('witel', 'HrController@list_witel');
				Route::post('witel', 'HrController@save_witel');

				//sto
				Route::get('sto', 'HrController@list_sto');
				Route::post('sto', 'HrController@save_sto');

				//position
				Route::get('position', 'HrController@list_position');

				//direktorat
				Route::get('direktorat', 'HrController@list_direktorat');
				Route::post('direktorat', 'HrController@save_direktorat');

				//mutasi
				Route::get('mutasi', 'HrController@list_mutasi');

				//ttd
				Route::get('ttd', 'HrController@list_ttd');

				//penghargaan
				Route::get('archivement', 'HrController@list_archivement');
			});

			//recruitment
			Route::get('/rec_init', 'ReportController@list_recruit_inter');

			Route::get('/monolog', 'HrController@monolog_karyawan');
			Route::get('/teknisi', 'HrController@teknisi_list');
			Route::get('/teknisi_tomman', 'HrController@teknisiTomman_list');

			//nik
			Route::get('submission/nik/{id}', 'HrController@submit_nik');
			Route::get('submission/nik_update/{id}', 'HrController@submit_nik');
			Route::post('submission/nik/{id}/{kat}', 'HrController@save_aju_nik');

			//karyawan
			Route::get('submission/karyawan_new/{id}', 'HrController@submit_karyawan');
			Route::post('submission/karyawan_new/{id}', 'HrController@save_karyawan');

			//mitra
			Route::get('created/mitra/{id}', 'HrController@add_mitra');
			Route::post('created/mitra/{id}', 'HrController@save_mitra');

			//mutasi
			Route::get('submission/mutasi', 'HrController@submit_mutasi');
			Route::post('submission/mutasi', 'HrController@save_mutasi');

			Route::get('approve/mutasi/{id}', 'HrController@app_mutasi');
			Route::post('approve/mutasi/{id}', 'HrController@save_app_mutasi');

			//created user
			Route::get('createNew_user/{id}', 'HrController@created_user');
			Route::post('createNew_user/{id}', 'HrController@createdSave_user');

			//created job posisi
			Route::get('new_jobPos/{id}', 'HrController@created_position');
			Route::post('new_jobPos/{id}', 'HrController@save_position');

			//created ttd
			Route::get('new_ttd/{id}', 'HrController@created_ttd');
			Route::post('new_ttd/{id}', 'HrController@save_ttd');

			//update ttd
			Route::get('update_ttd/{id}', 'HrController@edit_ttd');
			Route::post('update_ttd/{id}', 'HrController@update_ttd');

			//created archivement
			Route::get('new_arc/{id}', 'HrController@created_archivement');
			Route::post('new_arc/{id}', 'HrController@save_archivement');

			//kalender
			Route::get('calendar/cuti', 'HrController@calendar_cuti');

			//Input Cuti
			Route::get('input/cuti', 'HrController@input_cuti');
			Route::post('input/cuti', 'HrController@save_cuti');

			// upload data
			Route::get('uploadData', 'HrController@uploadData');
			Route::post('uploadData', 'HrController@uploadDataSave');
		});
	});

	//report
	Route::group(['middleware' => 'checkrole:1,30,46,22,69,12,71,72'], function () {
		Route::group(['prefix' => 'report'], function(){
			//alker created
			Route::get('tools/addAlker','ReportController@addNew_alker');
			Route::post('tools/addAlker','ReportController@save_alker');
			Route::post('tools/addNewAlker','ReportController@save_Newalker');

			//resign / konseling
			Route::get('resling', 'ReportController@resling');
			Route::get('add/resling', 'ReportController@add_resling');
			Route::post('add/resling', 'ReportController@save_resling');

			//absensi
			Route::get('absensi', 'ReportController@absensi');
			Route::get('absen_approve/{nik}', 'ReportController@absen_approval');
			Route::get('absen_decline/{id}', 'ReportController@absen_decline');
			Route::post('absen_decline/{id}','ReportController@absen_declineSave');
			Route::post('approval_massal','ReportController@absen_approval_massal');

			//accident
			Route::get('accident', 'ReportController@list_accident');

			//created accident
			Route::get('create_accident/{id}', 'ReportController@created_accident');
			Route::post('create_accident/{id}', 'ReportController@save_accident');

			Route::get('detail_cuti/{id}', 'ReportController@detail_cuti');

			//created Cuti
			Route::get('cuti', 'ReportController@list_cuti');
			Route::post('cuti', 'ReportController@save_jenis_cuti');

			//briedfing
			Route::get('briefingList/{date}', 'ReportController@briefingList');
			Route::get('briefing','ReportController@briefingInput');
			Route::post('briefing','ReportController@briefingSave');

			Route::get('current_brevert', 'FAController@list_breving');
			//MONITOR BREVERT
			Route::get('monitor_brevert', 'ReportController@MonitorBrevert');
		});

		Route::group(['prefix' => 'jx_dng'], function(){
			Route::get('validate_dirkt', 'HrController@validate_direktorat');
			Route::get('get_dir', 'HrController@get_direktorat');
			Route::get('detail_job_pos', 'HrController@get_detail_jobs');
			Route::get('get_user', 'HrController@get_user');
			Route::get('get_user_with_pos', 'HrController@get_user_position');
			Route::get('get_user_mutasi', 'HrController@get_user_mutasi');
			Route::get('get_user_rekrut', 'HrController@get_user_rekrut');
			Route::get('get_witel', 'HrController@get_witel');
			Route::get('get_psa', 'HrController@get_psa');
			Route::get('get_sto_mut', 'HrController@get_sto_mut');
			Route::get('get_jabatan', 'HrController@get_jabatan');
			Route::get('get_mitra', 'HrController@get_mitra');
			Route::get('get_alk', 'HrController@get_alker');
			Route::get('get_posisi', 'HrController@get_posisi');
			Route::get('jenis_cuti', 'ReportController@jenis_cuti');
			Route::get('get_sisa_cuti', 'HrController@get_jml_cuti');
			Route::get('get_table_beranda', 'HrController@get_table_beranda');
			Route::get('get_recruit_comp', 'HrController@get_recruit_comp');
			Route::get('get_accepted_home', 'HrController@get_accepted_home');
		});

		Route::group(['middleware' => 'checkrole:1, 12'], function () {
			Route::group(['prefix' => 'FA'], function(){
				Route::get('list/breving', 'FAController@list_breving');
				Route::get('detail_brevert/{id}', 'FAController@detail_breving');
				Route::get('submission/breving', 'FAController@input_breving');
				Route::post('submission/breving', 'FAController@submit_breving');
				Route::get('check_breving/{id}', 'FAController@check_absen_breving');
			});
		});

	});

	Route::get('/download/file_reward/{id}/{id_org}', 'HrController@download_file');

});
